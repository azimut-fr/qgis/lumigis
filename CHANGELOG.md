# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->
## 1.2.3-b - 2025-02-17

- correction menu si rechargement extension
- correction template projet pour géométries sources
- correction setting pour "consultation"

## 1.2.2 - 2025-02-11

- correction identification entité depuis le formulaire Lumigis (treeview)
- ajout icône Lumigis dans le menu Extensions
- ajout mode "consultation"

## 1.2.1 - 2025-01-21

- correction dialogue manquant (show_alert)

## 1.2.0 - 2025-01-18

- correction du bug empêchant de choisir le fichier de configuration avec le bouton de navigation
- correction du bug interdisant le report si le nom de fichier contient "EXT"
- export des couches du groupe en Geopackage - 2024-xx-xx paramétrage du dossier du .gpkg (export_folder dans le fichier .yml) et utilisation du nom court de la couche si défini, sinon nettoyage du nom
- conservation du filtre de commune si on active une intervention "interne"

## 1.1.2 - 2024-11-19

- ajout colonnes manquantes à recol_chantier (2489)
- ajout valeurs listes déroulantes (2476)
- intégratiion champs détectin manquants (2481)
- export de groupe de couches (ex - 2024-xx-xx cadastre)

## 1.1.1 - 2024-09-10

- Correction import du geopackage

## 1.1.0 - 2024-xx-xx

- Evolution câble (outil fourreau <-> câble)
- Correction mise à jour/import attributs
- Amélioration Notes (affichage auteur)
- Ajout valeurs aux listes t_r_xxx
- Amélioration traduction française

## 1.0.3 - 2024-xx-xx

- amélioration des styles (JCA)

## 1.0.1 - 2024-xx-xx

- correction tri des identifiants du treeview
- refactorisation du code

## 1.0.0 - 2024-06-06

- sortie fonction navec  des pointant ESCAPE
- éclatement du fichier lumigis.yml en autant de fichiers que d'environnements
- amélioration symbole curseur
- amélioration symbole "armoire"
- mode report et import détection
- mise à jour documentation cahier des charges

## 0.9.8 - 2024-01-15

- intégration des points

## 0.9.6 - 2023-12-17

- commande de standardoisation kit existant

## 0.9.5 - 2024-12-14

- ajout tableau de bord

## 0.1.0 - 2021-10-26

- First release
- Generated with the [QGIS Plugins templater](https - 2024-xx-xx//oslandia.gitlab.io/qgis/template-qgis-plugin/)
