
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

# Guide d'utilisation du module de report (travaux)

## **Constitution des fichiers**

La livraison des dossiers d'un plan de recolement est réalisée d'une façon automatique et le nommage des fichiers ne doit en aucun cas être modfiée (exemple 2022-04-074-88098).

Pour chaque dossier de recolement, celui-ci comprend :

* un dossier data : contenant les données des attributs au format .gpkg (armoires, supports, foyers) et **deux fichiers csv correspondants aux points GPS du réseau et terrain naturel des éléments relevés (ces deux fichiers sont à compléter dans le cadre du chantier selon les modalités du cahier des charges)**
* un dossier dgn relatif au fond de plan de la commune au format microstation et qui doit être compléter le cas échéant
* un dossier styles relatif au style du recolement qui ne doit pas être modifié
* un dossier svg relatif aux cellules du recolement qui ne doit pas être modifié
* une touche de raccourci .qgs avec le nommage du numéro de chantier qui ne doit pas être modfiée. Cette touche sert au lancement du logiciel

![Dossier récolement](../_static/images/dossier_recol.png)

## **Utilisation graphique**

### **Ouverture du dossier recolement**

Dans le guide d'installation, une touche de raccourci avec le profil report a été créée. Ainsi, la combinaison de l'utilisation de ce profil et la touche de raccourci contenue dans chaque dossier de recolement permet l'ouverture du fichier de recolement. Une fenêtre de récapitulation de quelques données techniques et la version du module s'affiche.

### **Tableau de bord**

Sur la fenêtre principale, un tableau de bord des éléments (foyers, supports, armoires), des câbles, des fourreaux et des réseaux divers (fonçage, forages etc...) suivant leurs états (à poser, à remplacer) est disponible. Dés lors que l'état de ces éléments change ou un linéaire est créé le tableau évolue dynamiquement.

### **Couches**

Par défaut, le fichier contient un certain nombre de couches "armoires, câbles etc...". Des couches complémentaires peuvent être ajoutées comme dans cet exemple; celles pour les points GPS pour le réseau et les affleurants ainsi que le fond de plan. Nous verrons comment les ajouter et les personnaliser.

![Couches](../_static/images/couches.png)

### **Outils**

#### **Définition des icônes principaux**


![](../_static/images/boite_outils1.png)

* Cas A : utlisation de cette touche soit pour le placement des objets de linéaires (fourreaux et réseau divers, câbles, étiquettes RAS et réseau divers,)
et soit pour le placement des objets ponctuels (Note)
* Cas B : utilisation de cette touche pour les couches en mode edition qui permet de corriger les erreurs de position pour les objets ponctuels que     linéaires
* Cas C : visualisation des données enregistrées dans la base du recolement selon trois modes : soit visualisation des données en totalité (Ouvrir Table attributs), soit visualisation des données grâce à l'outil de sélection (voir description ci-dessous cas D) (Ouvrir Table attributs (entitées sélectionnées)); soit visualisation des entités dans le champ du dessin (Ouvrir Table attributs(entitées visibles))
* Cas D : la sélection des entités se réalise par polygone ou main levée; à la fin de la sélection terminer par un clic droit pour confirmer puis sélectionner Ouvrir Table attributs(entitées sélectionnées) (description du cas C)

#### **Utilisation Plugin 'lumigis' (module de report)**

##### **Définition des icônes principaux**


![](../_static/images/boite_outils2.png)

##### **Définition des données pour les fiches supports**


![](../_static/images/boite_outils3.png)

* Nota : lors de la livraion du recolement, la liste des supports fait apparaître en rouge les données du ou des chantiers à traiter. Il est nécessaire de faire attention car il est possible que dans la livraion du recolement un autre chantier apparaisse. Afin de retrouver rapidement l'endroit de l'emprise du recolement à traiter, un outil zoom sur le matériel courant (à droite dans la fiche support) est mis à disposition. Au préalable, selectionner un support à traiter (en rouge dans la liste)

##### **Définition des données pour les fiches foyers**


![](../_static/images/boite_outils5.png)

* Nota : explications identiques au point précédent

### **Accrochage des objets**

Le mode d'accrochage des objets linéaires (câbles, fourreaux etc...) ou ponctuels (supports, foyers etc..) doit être paramétrer afin de garantir une position parfaite du symbole ou du câble sur le levé GPS (points réseaux ou affleurants). Ainsi, pour chaque couche l'opérateur pourra définir la qualité et le type d'accrochage.


![](../_static/images/accr_objet.png)

### **Ajouter une couche de texte délimité (csv)**

#### **Paramètrage de la couche**

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/gps.mp4" type="video/mp4">
</video>
</p>

#### **Paramètrage du symbole de la couche**

Se positionner sur la couche puis faire un clic droit et sélectionner **Propriétés** et se rendre sur **Symbologie**. Il est possible de changer, augmenter la taille du synbole....


![](../_static/images/symb_csv.png)

#### **Paramètrage des étiquettes de la couche**

Se positionner sur la couche puis faire un clic droit et sélectionner **Propriétés** et se rendre sur **Etiquettes**. Il est possible de changer le style, augmenter la taille de létiquette... .Suivant la sélection de la colonne du fichier csv concerné dans le champ **Valeur**, l'opérateur aura la possibilité de visualiser cette données graphiquement.


![](../_static/images/eti_csv.png)

#### **Utilisation plugin Geo CSV**

![](../_static/images/util_geocsv.png)

L'utilisation de ce plugin peut se rélever utile dans le cas de traitement sur plusieurs données des points GPS pour définir la méthode de levé mixte soit en fouille ouverte ou détection. Il permet de pouvoir modifier les données attributaires.

Au préalable, il est nécessaire d'ajouter la ou les couches csv des points GPS d'origine comme décrite au point 6. De plus, les couches doivent respecter l'emplacement  comme sur la figuer au point **1** afin que le plugin puisse les identifier dans sa liste déroulante **point 2**.

Ensuite, il faudra créer le fichier un fichier csv temporaire relatif aux points GPS réseau dans lequel les données modifiées seront stockées : par 2019-04-086-88157-ep.def.csv. Ce fichier, une fois les modifications réalisées, sera nommé selon les préconisations du cahier des charges de recolement.

Aller dans le menu **Extensions** puis sélectionner Geocsv.

Dans la liste déroulante, sélectionner la couche du fichier csv à traiter; le sytéme va alors ajouter une couche temporaire du même nom que le fichier sélectionné portant la mention **Copy**.

Dorénavant, il est possible de pouvoir utiliser tous les outils d'édition pour cette couche : modification, copie etc... .Les modifications, une fois terminées, seront copiées puis collées dans le fichier csv définitif.

### **Utilisation du plugin AnotherDXFImporter**

L'utilisation de ce plugin peut s'avérer utile pour transformer le plan DXF du levé terrain du fond de plan, réseau et affleurants afin de faciliter le report du recolement au même titre que l'ajout de la couche des points GPS pour le réseau et les affleurants dont la configuration a été décrite dans la section concernée.

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/dxf.mp4" type="video/mp4">
</video>
</p>

### **Mise en oeuvre d'un plan de recolement**

#### **Représentation des câbles**

L'une des vidéos présente la suppression d'une élément d'une couche. **La suppression n'est à utiliser que pour les couches câbles, réseaux divers, fourreaux, étiquettes RAS et réseau divers**. Pour les autres couches, il est interdit de faire les suppressions au risque de perdre les données lors de l'intégration du recolement.

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/cable.mp4" type="video/mp4">
</video>
</p>

#### **Représentation des fourreaux**

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/fourreau.mp4" type="video/mp4">
</video>
</p>

#### **Représentation du réseau divers (fonçage, forage etc..)**

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/res div.mp4" type="video/mp4">
</video>
</p>

#### **Représentation étiquettes**

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/eti.mp4" type="video/mp4">
</video>
</p>

#### **Déplacement et orientation d'objet linéaire ou ponctuel**

- _Outil Déplacement_

Attention, pour le déplacement d'un objet ponctuel il est nécessaire de relâcher l'objet (support, foyer, armoire) pour le déplacer
<p align="center">

![](../_static/images/depla.png)
</p>

- _Outil Orientation_

<p align="center">

![](../_static/images/orient.png)
</p>

#### **Placement des accessoires**
<p align="center">
<video width="640" controls>
  <source src="../_static/videos/acce.mp4" type="video/mp4">
</video>
</p>

## **Utilisation de la Base de Données**

Pour accéder à la base de données, ouvrez le module lumigis et sélectionnez Report EP :

<p align="center">

![](../_static/images/boite_report.png)
</p>

Une boîte de dialogue apparaît avec deux onglets. L'un avec la liste des supports et les foyers rattachés (sous format d'arborescence) et l'autre pour les appareils armoires et départs rattachés à celles-ci. Pour chaque objet, les données techniques sont visuables et modifiables selon les droits donnés par le SDEV.

Pour chaque chantier, dans la liste les objets concernés sont identifiables grâce à une couleur différente (rouge). Par ailleurs, il se peut que le dossier de recolement puisse contenir plusiuers objets à traiter mais sur des chantiers différents. C'est pourquoi, il convient que le prestataire s'assure, avec le plan de numérotation fourni avec le dossier de recolement, de bien traiter les objets relatifs au chantier.

Afin, de se positionner correctement sur les objets du chantier il suffit de sélectionner l'un des identifiant dans la liste et à l'aide de l'icône prévue à cet effet (voir copie d'écran ci-dessous), le système se positionnera sur l'objet se centrera dessus.

![](../_static/images/rech_report.png)

### **Fiches supports**

<p align="center">

![](../_static/images/boite_outils3.png)
</p>

### **Fiches foyers**

<p align="center">

![](../_static/images/boite_outils5.png)
</p>

### **Exemples d'utilisation**

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/bd.mp4" type="video/mp4">
</video>
</p>

# Guide d'utilisation du module de report (détection)

Préalablement, le prestataire se reportera à la [section pour la création des profils](/usage/guide_installation.md#creation-des-profils) et à l'installation du module de report pour la détection. De même, il pourra installer les [plugins complémentaires](/usage/guide_installation.md#installation-plugins).

## **Convention de saisie des identifiants et des données**

### **Saisie des identifiants**

La saisie des identifiants des attributs (identifiants armoires, supports et foyers) est réaliée de la façon suivante :

- Armoire : **_*A1_** en lettre majuscule et précédé du caractère **_*_**. La numérotation se fait en continu **_*A1_**, **_*A2_**.... _**Si un identifiant existe déjà, le prestataire devra utiliser celui-ci**_.
- Départs : **_*1_** en chiffre. La numérotation se fait en continu **_*1_**, **_*2_**...._**Si un identifiant existe déjà, le prestataire devra utiliser celui-ci**_.
- Supports : **_*S1_** en lettre majuscule et précédé du caractère **_*_**. La numérotation se fait en continu **_*S1_**, **_*S2_**....
- Foyers : **_*F1_** en lettre majuscule et précédé du caractère **_*_**. La numérotation se fait en continu **_*F1_**, **_*F2_**....même en cas de changement d'armoire.

### **Saisie des données**

#### fiche armoire (à renseigner avec le module report)

- Armoire : identifiant de l'armoire **– exemple de saisie obligatoire :** ***A1**
- Etat : état de l’armoire (choix sur **liste**) **– saisie obligatoire : Actif**
- Vétusté : vétusté de l’armoire (choix sur **liste**)
- Fermeture : type de fermeture de l’armoire (choix sur **liste**)
- Type de pose : mode de pose de l’armoire (choix sur **liste**)

_Note importante : l'identifiant et l'état sont des champs à remplir correctement pour une bonne utilisation du module_

#### fiche départ (à renseigner avec le module report)

- Armoire : identifiant de l'armoire **– exemple de saisie obligatoire :** ***A1**
- N°de départ : identifiant du départ **– exemple de saisie obligatoire :** ***1**

_Note importante : l'identifiant et le numéro de départ sont des champs à remplir correctement pour une bonne utilisation du module_

#### fiche câble

- Etat : état du support (choix sur **liste**) **– saisie obligatoire : Actif**
- Type de pose : type de pose du câble (choix sur **liste**)
- Classe de précision (choix sur **liste**)

**Note importante : le prestataire doit renseigner un certain nombre de champs obligatoires pour valider la fiche câble. Parmi ceux-ci se trouve la classe de précision. Or, si celle-ci relève de la classe B ou C le prestaire devra indiquer dans l'onglet remarque la raison de cette classification. De plus, le type de pose sera renseigné par _souterrain incertain_ [(Annexe I du Catalogue des objets du patrimoine Eclairage Public)](/usage/cahierdescharges.md#annexe-i-catalogue-des-objets-du-patrimoine-eclairage-public)**

#### fiche support

- Support : identifiant du support **– exemple de saisie obligatoire :** ***S1**
- Etat : état du support (choix sur **liste**) **– saisie obligatoire : Actif**
- Nature : nature du support (choix sur **liste**)
- Symbole : symbole graphique (choix sur **liste**)
- Boitier : présence de boîtier classe II (boitier de raccordement à renseigner uniquement pour le réseau aérien)
- Prise_illumination : présence d’une prise illumination

_Note importante : l'identifiant, l'état et le symbole sont des champs à remplir correctement pour une bonne utilisation du module_

#### fiche foyer

- Foyer : identifiant du foyer **– exemple de saisie obligatoire :** ***F1**
- Etat : état du foyer (choix sur **liste**) **– saisie obligatoire : Actif**
- Armoire : identifiant de l'armoire **– exemple de saisie obligatoire :** ***A1**
- N°de départ : identifiant du départ **– exemple de saisie obligatoire :** ***1**
- Symbole : symbole graphique (choix sur **liste**)

_Note importante : l'identifiant, l'état et le symbole sont des champs à remplir correctement pour une bonne utilisation du module_

## Exemple de création des éléments

### Utilisation graphique

Le prestataire se reportera à la [section pour la mise en oeuvre d’un plan de recolement](/usage/guide_utilisation.md#mise-en-oeuvre-dun-plan-de-recolement); mise en oeuvre identique dans le cadre des travaux ou de la détection.

### Exemple de saisie des objets

<p align="center">
<video width="640" controls>
  <source src="../_static/videos/report_detec.mp4" type="video/mp4">
</video>
</p>
