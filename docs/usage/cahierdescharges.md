# Cahier des charges pour la mise en oeuvre des recolements (marché 2023/2027)

## **Objectifs**

Pour les communes ayant délégué la compétence relative au réseau d’éclairage public :

L’objet du présent cahier des charges porte sur la fourniture des plans de récolement. Cette prestation a deux objectifs :

Fournir sous forme de données numériques l’ensemble des éléments du plan de récolement

Récolement du patrimoine éclairage public :

1. Phase terrain
1. Etablissement des plans
1. Saisie/mise à jour des informations patrimoniales sur le réseau d’éclairage public

En application de ce cahier des charges, le plan de récolement numérique permettra de disposer d’une base de données géoréférencée et structurée afin d’être facilement intégrée dans un système d’information géographique.

De plus, dans le cadre de la réforme DT/DICT, l’arrêté du 15 Février 2012 impose de garantir une classe de précision géographique A et il oblige le gestionnaire ou l’exploitant du réseau à répondre aux déclarations réglementaires de travaux à proximité des réseaux sensibles pour la sécurité, avec des plans des réseaux géoréférencés avec la même précision (classe A) à partir de 2020 (pour des travaux situés en unités urbaines), et 2026 (pour des travaux en dehors de ces zones).

## **Contexte réglementaire**

**Système de géoréférencement :(X,Y) RGF93 – CC48 et (Z) NGF IGN69**

**Décret n°2006-272 du 3 mars 2006**: les coordonnées des données seront dans le système de projection Conique Conforme zone 48 et dans le système de référence RGF93 pour la planimétrie. Le nivellement sera rattaché au système d'altitude NGF/IGN69 (altitude normale).

**Arrêté du 15 février 2012** pris en application du chapitre IV du titre V du livre V du code de l'environnement relatif à l'exécution de travaux à proximité de certains ouvrages souterrains, aériens ou subaquatiques de transport ou de distribution.

**Arrêté du 16 septembre 2003** portant sur les classes de précision applicables aux catégories de travaux topographiques réalisés par l’Etat, les collectivités locales et leurs établissements publics ou exécutés pour leur compte.

**Norme AFNOR NF S70-003-2** portant sur les techniques de détection sans fouille (décembre 2019).

**Norme AFNOR NF S70-003-3** portant sur le géoréférencement des réseaux (décembre 2019).

**L'Arrêté du 27 décembre 2016** approuve le Guide d'application de la règlementation relative aux travaux à proximité des réseaux, composé de 3 fascicules :

**Fascicule 1 : Dispositions générales**

**Fascicule 2 : Guide technique des travaux**

**Fascicule 3 : Formulaires et autres documents pratiques**

**Plan de Corps de Rue Simplifié (PCRS) – géostandard d’échange version 2.0 du 21 septembre 2017**

## **Documents/données fournis par le maître d’ouvrage**

Le Syndicat Départemental d'Électricité des Vosges (désigné dans le présent document par le SDEV) fournira au prestataire :

***Le fichier graphique du chantier en 3D***

Le SDEV fournira au prestataire un fichier au format *geopackage* (*.gpkg*) (format SIG)^[**SIG : Système d'information géographique**] contenant l’ensemble des données suivantes :

- Le réseau d’éclairage public existant

- La base de données des armoires, départs, supports, foyers et sources de la commune

- Le cadastre de la commune concernée

- Le fond de plan pour la ou les parties souterraines de la commune concernée

Pour chaque couche, le paramétrage a été réalisé par le SDEV et il ne pourra en aucun cas être modifié

***Les fichiers prototypes des points à lever***

Relatifs aux levés du réseau et de ses accessoires ainsi qu’au levé du terrain naturel (TN) (voir le descriptif paragraphe 5 et exemple de nommage paragraphe 4.1)

***Le Catalogue des objets :***

Décrivant les éléments à lever, les attributs et les symboles (Annexe I).

***Un module logiciel pour le report des plans de recolement :***

pour cela le prestataire devra installer le logiciel QGIS téléchargeable sur le site :

[https://www.qgis.org/fr/site/](https://www.qgis.org/fr/site/) ainsi que le module
logiciel sous forme d'extension QGIS (plugin).

***Les mises à jour de ce module seront diffusées gracieusement dans le cadre de correctifs logiciels.***

***Le fonctionnement du plugin pour le report des plans de recolement fait l’objet d’une aide livrée avec le module.***

### **Règle de nommage des fichiers**

Pour chaque opération à réaliser, le SDEV fournit les fichiers vierges ou à compléter pour le report de réseau et le fond de plan vierge ou à compléter, au format *geopackage* (format SIG).

Pour chaque opération à réaliser, le SDEV fournit les fichiers des points à lever. Le nom
de ces fichiers est constitué de l’année, du numéro de lot, d’un numéro fixé par le SDEV et du
numéro INSEE de la commune auquel on ajoute le suffixe « -ep » suivi de l’extension *.csv*
(le tout en minuscules).

**Exemple** : pour Mazirot, le fichier du dossier *2020-A-012* s’appellera
**2020-A-012-88295-ep.csv**

**Nota : tout fichier ne respectant pas cette règle de nommage sera systématiquement refusé**

## **Documents à rendre par le prestataire**

### **Règles de nommage pour la restitution des fichiers**

Pour chaque chantier, le dossier «.zip » doit respecter la même règle de nommage et contenir :

1. Deux fichiers *.csv* des points levés: l’un contenant les points du terrain naturel et des accessoires et l’autre contenant les points réseaux de l’éclairage public et ses accessoires (voir description des fichiers paragraphe 5).
1. Dans le cas d’une rénovation ou d’une création, un fichier *.pdf* du ou des schémas d’armoire : le nom de ce fichier est constitué de l’année, du numéro de lot, d’un numéro fixé par le SDEV, du numéro INSEE de la commune, et de l’identifiant de l’armoire précédé du chiffre (**zéro**) auquel on ajoute le suffixe « -ep » suivi de l’extension *.pdf* (le tout en minuscules).

Exemple :

* **2020-A-016-88295-ep.zip** contiendra les fichiers suivants :

* les données réseau, armoires, départs, accesoires, supports, foyers et sources et fond de plan au format _geopackage_ :
  - **2020-A-016-88295-ep.gpkg**

* les points topographiques _levés_ du réseau et terrain naturel au format *csv*
  - **2020-A-016-88295-ep.res.csv**
  - **2020-A-016-88295-ep.tn.csv**

* si rénovation ou création d'armoire, le fichier des schémas d'armoire au format _pdf_
  - **2020-A-016-88295-0D-ep.pdf**

**Nota : Le respect de ces règles de nommage est impératif. Toute livraison ne respectant pas ces règles de nommage sera systématiquement et entièrement refusée.**

## **Description des fichiers de points levés**

Les levés seront conformes au système de géoréférencement et de précision de classe A (selon l’Arrêté du 15 février 2012 pris en application du chapitre IV du titre V du livre V du code de l'environnement relatif à l'exécution de travaux à proximité de certains ouvrages souterrains, aériens ou subaquatiques de transport ou de distribution et de l’Arrêté du 16 septembre 2003).

La position géographique des objets d’éclairage Public (les coordonnées des objets levés) seront dans le système de projection Conique Conforme zone 7 et dans le système de référence RGF93 pour la planimétrie. Le nivellement sera rattaché au système d'altitude NGF/IGN69 (altitude normale).

Afin d’assurer la traçabilité des données et faciliter le contrôle, les fichiers
*.csv* du chantier sont fournis par le prestataire qui recense ***uniquement***
les données brutes des points levés du réseau d’éclairage public, des accessoires et les points du terrain naturel.

Ces fichiers sont au nombre de deux : l’un recense les points levés du terrain naturel et des accessoires (candélabres, armoires, supports (bois, béton, bois contre-fiché, bois double, supports projecteurs, regards) (rrrr-r-rrr-rrrrr-ep.tn.csv) et l’autre recense les points du réseau de l’éclairage public (y compris les réseaux divers fonçage, forages, plaques de protection, tubes aciers, bétonnage) et ses accessoires (boites dérivation et jonction)(rrrr-r-rrr-rrrrr-ep.res.csv).

**Structure des données à renseigner (fichiers de points levés) : Afin d’intégrer dans son système d’information géographique les différentes données renseignées dans les deux fichiers csv des points levés du réseau et du terrain naturel et ses accessoires, le SDEV fourni une structure modèle pour chaque opération.**

**Le prestataire devra se conformer à la mise en forme des informations de la façon suivante (il est formellement demandé au prestataire de ne porter aucune modification sur les entêtes de lignes ou de colonnes ainsi que leurs mises en forme ; seules les données en texte libre seront au choix du prestataire)**

_#entreprise_georeferencement_ :

Nom de l'entreprise ayant réalisé le géoréférencement (texte libre) : **ex TOPDETEC**

_#Operateur_georeferencement_ :

Opérateur ayant effectué le géoréférencement (nom/prénom) (texte libre) : **ex Yann LASAUCE**

_#nature_ouvrage_ :

La nature de l'ouvrage objet du relevé, au sens de l'article R. 554-2 du code de l'environnement : dans notre cas ce sera toujours **EP**

_#appareil_gps_ : S/N

Le type, la marque, le modèle et le numéro de série de l'appareil de mesure (texte libre) : **ex GPS TRIMBLE R10 S/N1789234**

_#appareil_station_totale_ : S/N

Le type, la marque, le modèle et le numéro de série de l'appareil de mesure (texte libre) : **ex STATION TOTALE LEICA TS12 S/N301256**

_#methode_leve_ :

Méthode de relevé : _il est demandé d’inscrire le chiffre correspondant : **1** pour la détection, **2** pour la fouille ouverte, **3** pour un levé mixte  détection et fouille ouverte_

**Attention lorsque la méthode de levé est renseignée à 3, celle-ci implique de renseigner une colonne supplémentaire UNIQUEMENT pour le fichier réseau (voir page suivante colonne methode_leve)**

_#entreprise_detection_ :

Nom de l'entreprise ayant réalisé la détection (texte libre) : **ex RESEAU TOPO**

_#operateur_detection_ :

Opérateur ayant effectué la détection (nom/prénom) (texte libre) : **ex Karl THIRIET**

_#appareil_detection_ :

Le type, la marque, le modèle et le numéro de série de l'appareil de détection (texte libre) : **RADIODETECTION RD8100 S/N1879454**

_#technologie_mesure_ :

La technologie de mesure employée (texte libre) : ex: **Electro-magnétique**

Tous les points levés doivent être transmis dans le *.csv* correspondant, s'ils concernent :

- le réseau
- le réseau aérien, les affleurants et accessoires

Un enregistrement points contient **toujours** les colonnes suivantes :

- **Numero** : matricule alphanumérique du point
- **X** : coordonnée X du point dans le système de projection : **exemple : 1984599.21**
- **Y** : coordonnée Y du point dans le système de projection : **exemple : 7209195.09**
- **Z** : altitude du point dans le système de nivellement : **exemple : 801.34**
- **Profondeur** : profondeur du point de réseau souterrain en **mètres** (profondeur par rapport au point GPS TN) : **exemple : 0.72**
- **DateHeure** : horodatage du levé du point **(formats obligatoires acceptés) :**

>1. **2021/07/21 09:15:23+02 ou  2021/07/21 09:15:23 (mettre un espace entre la date et l’heure)**

> 2. **2021/07/21 09:15:23+02 ou  2021/07/21 09:15:23+02:00 (mettre un espace entre la date et l’heure)**

> 3. **2021-07-21 09:15:23+02 ou  2021-07-21 09:15:23 (mettre un espace entre la date et l’heure)**

> 4. **2021-07-21 09:15:23+02 ou  2021-07-21 09:15:23+02:00 (mettre un espace entre la date et l’heure)**

**Nota : l’espace entre la date et l’heure peut remplacer par la lettre ‘T’**

> 5. **2021/07/21T09:15:23+02**

- **Cod_Proj**: système de projection (texte libre) **exemple: CC48**
- **Commentaire** : toute information supplémentaire jugée nécessaire, notamment des informations aidant à l’identification sans ambigüité du point et/ou de l’ensemble de points définissant l’ouvrage auquel il appartient
- **HDOP** : dilution horizontale de précision Dilution de précision si GPS (précision planimétrique) : **exemple : 1.4**
- **PDOP** : dilution 3D de précision si GPS (précision 3D) : **exemple : 1.2**
- **methode_leve** : cette colonne est à remplir **UNIQUEMENT pour le fichier de points levés concernant le réseau et lorsque la ligne ‘#methode\_leve’ est renseignée à 3 :** il est nécessaire de différencier chaque point levé du réseau en fonction du levé soit en fouille ouverte (*code 2*) ou en détection (*code 1*) :

**exemple de saisie : 1 ou 2**

Suivant le contexte, certaines colonnes ne seront pas renseignées :

- **PROF** n'est renseignée que pour les points de réseau souterrain

- **HDOP/PDOP** ne sont renseignés que si un levé GPS est fait

**NB** : en cas de levé au théodolite (pas de GPS), il est indispensable que les points de référence prévus par la règlementation soient fournis et conformes à la description ci-dessus (**Le prestataire s’engage sur l’exactitude des éléments fournis**)

**Ces fichiers font l’objet d’un traitement informatique automatisé et doivent respecter le formalisme demandé; ils seront rejetés systématiquement en cas de :**

- [x] *Doublons des matricules pour les points levés*
- [x] *Non-respect des séparateurs de décimales (**obligatoirement un point**)*
- [x] *Non-respect ou changement de l’ordre ou de la dénomination des entêtes de colonnes des enregistrements*
- [x] *Non respect du format de la colonne d'horodatage « DateHeure »*

### **Méthodologie du relevé et nombre de points**

Concernant le réseau aérien, seul le relevé des supports ou candélabres sera pris en compte.

Concernant le réseau souterrain, il est demandé de lever :

- **5 points pour les traversées de chaussées,**

- **3 points minimum ou plus pour les courbes permettant la création de deux arcs de cercle contigus suivant le rayon de courbure afin de garantir la classe de précision A,**

- **1 point tous les 10 mètres dans le cas de réseaux rectilignes.**

**Important : dans le cas de charge atypique, le prestataire adaptera le nombre de points levés.**


### **Indications sur les points levés**

| **Type de cellules**                                       |   **Coordonnées**   |  **1er point lever**  |                       **2éme point lever**                        |
| :--------------------------------------------------------- | :-----------------: | :-------------------: | :---------------------------------------------------------------: |
| Poteau béton                                               | XYZ terrain naturel | Oui Centre de l’objet |                 Oui indication sur l’orientation                  |
|                                                            |                     |                       |                                                                   |  |
| Poteau bois                                                | XYZ terrain naturel | Oui Centre de l’objet |                                non                                |
|                                                            |                     |                       |                                                                   |  |
| Carré fer                                                  | XYZ terrain naturel | Oui Centre de l’objet |                                non                                |
|                                                            |                     |                       |                                                                   |  |
| Support façade                                             | XYZ terrain naturel | Oui Centre de l’objet |                                non                                |
|                                                            |                     |                       |                                                                   |  |
| Poteau bois double, contrefiche                            | XYZ terrain naturel | Oui Centre de l’objet |                 Oui indication sur l’orientation                  |
|                                                            |                     |                       |                                                                   |  |
| Support projecteur                                         | XYZ terrain naturel | Oui Centre de l’objet |                                non                                |
|                                                            |                     |                       |                                                                   |  |
| Armoire (posée au sol)                                     | XYZ terrain naturel | Oui Centre de l’objet |         Oui indication sur l’orientation (armoire au sol)         |
|                                                            |                     |                       |                                                                   |  |
| Armoire (en façade ou encastrée (poste de transformation)) | XYZ terrain naturel | Oui Centre de l’objet |                                non                                |
|                                                            |                     |                       |                                                                   |  |
| Armoire (sur poteau)                                       |    Pas de point     |     Pas de point      | Directement attaché à la cellule sur le support où elle se trouve |
|                                                            |                     |                       |                                                                   |  |
| Armoire (à l’intérieur d’un bâti)                          |    Pas de point     |     Pas de point      |                    Directement attaché au bâti                    |
|                                                            |                     |                       |                                                                   |  |
| Boites (dérivation, jonction)                              |      XYZ réel       | Oui Centre de l’objet |                                non                                |
|                                                            |                     |                       |                                                                   |  |
| Regard                                                     | XYZ terrain naturel | Oui Centre de l’objet |                                non                                |
|                                                            |                     |                       |                                                                   |  |
| Mât simple, double, triple, quadruple, boule               | XYZ terrain naturel | Oui Centre de l’objet |                                non                                |

**Nota : Le prestataire devra se conformer aux prescriptions suivantes pour les points levés des cellules décrites dans le tableau ci-dessus**


Pour les armoires : le deuxième point doit être pris soit à droite ou gauche de la petite face du coffret


![](../_static/images/point_armoire.png)

Pour les supports béton: le deuxième point doit être pris soit à droite ou gauche de la petite face du support


![](../_static/images/point_support.png)

Pour le support façade, celui-ci est orienté dans le prolongement du bâti


![](../_static/images/point_facade.png)
Pour les supports bois double et contrefiche: le deuxième point doit être pris soit à droite ou gauche mais dans le prolongement du support


![](../_static/images/point_boisdouble.png)


![](../_static/images/point_boiscontrefiche.png)

**Des contrôles des levés topographiques pourront être appliqués, sur décision du SDEV, pour vérifier le respect de la classe de précision conformément à l’arrêté du 16 septembre 2003 portant sur les classes de précision applicables aux catégories de travaux topographiques. En cas de non respect, les plans définitifs seront retournés au prestataire qui devra effectuer un nouveau géoréférencement à ses frais.**

## **Transmission des fichiers**

Les moyens informatiques peuvent être au choix :

1. soit l’utilisation du mail
2. soit la plateforme de téléchargement du Syndicat.

L’usage du mail sera réservé aux fichiers de petite taille (< 1 Mo).

Pour tous les supports utilisés par l’entreprise au cours des échanges de données, l’étiquetage du support doit comporter : le numéro de dossier, le nom de l’entreprise. Les pertes des données durant l’envoi n’engagent pas la responsabilité du SDEV. Pour les transmissions par mail, il est indispensable d’indiquer **en objet** le numéro de dossier, le nom de la commune **et** **sur la première ligne** du mail le numéro de dossier, le nom de la commune et la date de l’envoi.

Exemple :

Objet du mail : **2020-A-MAZIROT**

Première ligne du mail : **2020-A-MAZIROT/11-01-2020**

## **Règles générales de dessin**

Cette procédure est destinée à aborder la seule méthodologie des règles du dessin retenue par le SDEV dans la constitution du plan du réseau.

**Il est de la respnsabilité du titulaire de diffuser toutes ces informations auprès de ses sous-traitants éventuels.**
Seule la symbologie et de style de couche pour le réseau d'éclairage public fournie par le SDEV sera utilisée, sans modification de leurs caractéristiques. Si un élément n’est pas présent, le prestataire devra contacter le SDEV pour qu’il lui fournisse le modèle et style adéquat.

### **Orientation supports, candélabres, foyers, armoire**

Il est demandé pour les supports béton de les orienter soit en arrêt, alignement, ou bissectrice conformément à la réalité du terrain (cf. paragraphe 5.2 sur la méthologie du levé).

Il est demandé pour les candélabres simple, double, triple et quadruple de les orienter conformément à la réalité du terrain (cf. paragraphe 5.2 sur la méthodologie du levé).

Il est demandé pour les foyers dépendants des supports béton, carré fer, bois, bois double, bois contrefiché, façade et projecteur de les orienter conformément à la réalité du terrain.

Il est demandé pour les armoires les orienter conformément à la réalité du terrain (cf. paragraphe 5.2 sur la méthodologie du levé).

### **Placement accessoires**

Le boitier différentiel sera placé sur le câble souterrain concerné.

Les boîtes de jonction et de dérivation relevées dans un regard seront insérées sur le câble ou les câbles. Ainsi, le réseau souterrain sera constitué d’un tronçon de part et d’autre de la boîte.

Dans le cas où une boite souterraine (dérivation ou jonction et non accessible dans un regard) est détectée mais dont la position est incertaine il est nécessaire de l’identifier graphiquement au moyen de la cellule prévue à cet effet :


![](../_static/images/boite_incertaine.png)

### **Représentation des câbles, fourreau, réseaux divers**

#### **Câble souterrain**

Il est demandé de représenter les câbles par tronçons :

- d’une part un tronçon est considéré comme représentation du câble souterrain entre deux candélabres ou entre un candélabre et un accessoire (boîte par exemple)

- d’autre part en fonction de la classe de précision B ou C si besoin

Le prestataire utilisera le style de couche fourni par le SDEV et suivant le résultat de la précision du levé [(Annexe I du Catalogue des objets du patrimoine Eclairage Public)](/usage/cahierdescharges.md#annexe-i-catalogue-des-objets-du-patrimoine-eclairage-public).


![](../_static/images/csout.png)

#### **Câble aérien**

Il est demandé de représenter les câbles par tronçons :

- un tronçon est considéré comme représentation du câble aérien entre deux supports

Le prestataire utilisera le style de couche fourni par le SDEV pour les réseaux torsadés [(Annexe I du Catalogue des objets du patrimoine Eclairage Public)](/usage/cahierdescharges.md#annexe-i-catalogue-des-objets-du-patrimoine-eclairage-public).


![](../_static/images/caerien.png)

#### **Réseaux divers**

Le style de couche pour les réseaux divers est nécessaire dans le cas où la charge des réseaux n'est pas suffisante et/ou ne permet un relevé en classe A, ou à nécessiter la mise en place d'éléments de protection renforcée (fonçages, forages, tubes aciers, plaques de protection, bétonnage etc..).

Il sera recommandé d’indiquer la longueur du réseau divers dont le texte sera renseigné de la façon suivante et **uniquement de cette façon** : « *fonçage sur 3m »* ou « *forage sur 3m »* ou « *plaque de protection EP sur 3m »* ou « *tubes acier sur 15m »*. Le style de couche utilisé et prescrit dans l’annexe I est « *reseau divers* ».
#### -- **exemple 1**

![](../_static/images/et_resdivers.png)

#### -- **exemple 2**

![](../_static/images/et1_resdivers.png)

#### -- **exemple 3**

![](../_static/images/et2_resdivers.png)

#### -- **exemple 4**

![](../_static/images/et3_resdivers.png)

#### **Exemples de représentation câbles**

+ **Exemple de représentation d’un réseau entre une armoire desservant plusieurs candélabres** : le réseau souterrain est relié à chaque origine des symboles et fait l’objet de 5 tronçons minimum.


![](../_static/images/fig4.png)

+ **Exemple de représentation d’un réseau traversant deux boites (dérivation et jonction)** : le réseau doit être interrompu et fait l’objet, dans ce cas, de 4 tronçons distincts.


![](../_static/images/fig3.png)


+ **Dans le cas où, entre plusieurs supports, il est nécessaire de représenter plusieurs réseaux ceux-ci devront respecter le schéma représenté de cette façon** :


![](../_static/images/fig2.png)

Par ailleurs

Les segments S1, S3 et S4 constituent le tronçon T1 représenté par une seule polyligne et les segments S2, S6 et S5 constituent le tronçon T2 représenté par une seule polyligne. Ces deux tronçons feront l’objet de deux fiches.

+ **Exemple d’un réseau mixte aérien et souterrain** : celui-ci représente les tronçons associés au réseau souterrain (tronçon1) ainsi que ceux du réseau aérien (tronçons 2,3 et 4). Il met aussi en évidence les différents accrochages entre les natures des réseaux.


![](../_static/images/fig1.png)

### **Représentation des candélabres et foyers**

#### - Mât boule

![](../_static/images/rep_boule.png)

#### - Mât simple

![](../_static/images/rep_simple.png)

#### - Mât double

![](../_static/images/rep_double.png)

#### - Mât triple

![](../_static/images/rep_triple.png)

#### - Mât quadruple

![](../_static/images/rep_quadruple.png)

### Représentation des supports et foyers

#### - Poteau béton

![](../_static/images/betonfoyer.png)

#### - Poteau bois

![](../_static/images/boisfoyer.png)

#### - Poteau bois double

![](../_static/images/doublefoyer.png)

#### - Poteau bois contrefiché

![](../_static/images/contrefichefoyer.png)

#### - Projecteur

![](../_static/images/projecteurfoyer.png)

#### - Carré fer

![](../_static/images/ferfoyer.png)

#### - Support façade

![](../_static/images/facadefoyer.png)

### **Représentation des accessoires**

Sur cet exemple, le prestataire veillera pour ses rendus à se conformer :

>> - à accrocher le boitier différentiel sur le réseau souterrain concerné

![](../_static/images/rep_diff.png)

>> - à identifier les remontées aéro-souterraines

![](../_static/images/rep_ras.png)

### **Fiches de données techniques**

Seules les données techniques pour le câble de réseau d'éclairage public, les supports, armoires, départs, foyers et sources seront renseignées au moment de leur représentation. Les modalités de mise en oeuvre seront décrites au travers d'un guide fourni avec le module de report.

Pour les autres composants, ils feront l'objet d'une représentation seulement graphique ou complétée par une annotation prévue à cet effet et décrite dans l'annexe I du catalogue des objets du patrimoine Eclairage Public.

## **Information sur les identifiants**

Les armoires sont identifiées en majuscule sur un ou deux caractères et par une ou deux lettres (ex : A ou AA) ; cette identification est unique pour chaque commune.

Les supports sont identifiés sur 5 caractères commençant toujours par S et suivi de 4 chiffres (par ex S0011) ; cette identification est unique pour chaque commune.

Les foyers sont identifiés sur 6 caractères commençant toujours par F suivis de deux caractères, 2 lettres ou le *zéro* et la lettre identifiant l’armoire, et se terminant par trois chiffres (par ex F0A001 ou FAA001) ; cette identification est unique pour chaque commune.

La numérotation des supports se fait de manière continue, même dans le cas de rattachement à une armoire différente. Par contre, la numérotation des foyers se réinitialise dès lors qu’il y a changement d’armoire.

## **Qualité du rendu et contrôle des documents remis**

Les documents à fournir feront l’objet d’un contrôle par le SDEV qui se réserve le droit de faire modifier par le prestataire tout ou partie du plan pour toute non-conformité constatée.

Ce contrôle est effectué pour partie par voie informatique, et pour partie par visite sur le terrain

Nommage des fichiers :

Les fichiers fournis par le SDEV (fond de plan, réseau d’éclairage public etc…) sont transmis dans un fichier zippé et prédéfini dans son intitulé. Il sera demandé au prestataire de ne pas y apporter de modifications et de les garder dans le dossier dézippé. De plus, dans le cas de fourniture par celui-ci de fichiers complémentaires, le prestataire respectera nécessairement les règles de nommage imposées par le SDEV

## **Annexe I – Catalogue des objets du patrimoine Eclairage Public**

|  **Foyers**|  	|  	|
|---	|:---:	|---:	|
|  simple|  symbole FOYER SIMPLE	|  <img src="../_static/images/foyer_poteau.png">	|
|  projecteur	|  symbole PROJECTEUR	|  <img src="../_static/images/foyer_projecteur.png">	|
|  foyer mât	|  symbole FOYER MAT	| <img src="../_static/images/foyer_mat.png"> 	|
|  	|  	|  	|
|  **Supports**	|  	|  	|
|  mât boule	|  symbole MAT BOULE	|  <img src="../_static/images/top.png">	|
|  mât simple	|  symbole MAT SIMPLE	|  <img src="../_static/images/simple.png">	|
|  mât double	|  symbole MAT DOUBLE	|  <img src="../_static/images/double.png">	|
|  mât triple	|  symbole MAT TRIPLE	|  <img src="../_static/images/triple.png">	|
|  mât quadruple	|  symbole MAT QUADRUPLE	|  <img src="../_static/images/quadruple.png">	|
|  support projecteur	|  symbole SUPPORT PROJECTEUR	|  <img src="../_static/images/projecteur.png">	|
|  poteau bois	|  symbole POTEAU BOIS	|  <img src="../_static/images/bois.png">	|
|  poteau bois contrefiche	|  symbole POTEAU BOIS CONTREFICHE	|  <img src="../_static/images/bois_contrefiche.png">	|
|  poteau bois double	|  symbole POTEAU BOIS DOUBLE	|  <img src="../_static/images/bois_double.png">	|
|  poteau béton	|  symbole POTEAU BETON	|  <img src="../_static/images/beton.png">	|
|  carré fer	|  symbole CARRE FER	|  <img src="../_static/images/fer.png">	|
|  support façade	|  symbole SUPPORT FACADE	|  <img src="../_static/images/facade.png">	|
|  	|  	|  	|
|  **Accessoires**	|  	|  	|
|  boite de dérivation	|  symbole BOITE DE DERIVATION	|  <img src="../_static/images/boite_derivation.png">	|
|  boite de jonction	|  symbole BOITE DE JONCTION	|  <img src="../_static/images/boite_jonction.png">	|
|  boite incertaine	|  symbole BOITE INCERTAINE	|  <img src="../_static/images/boite_incertaine.png">	|
|  regard éclairage public	|  symbole REGARD EP	|  <img src="../_static/images/regard.png">	|
|  disjoncteur différentiel	|  symbole DISJONCTEUR DIFFERENTIEL	|  <img src="../_static/images/differentiel.png">	|
|  séparation de réseau	|  symbole SEPARATION DE RESEAU	|  <img src="../_static/images/separation.png">	|
|  	|  	|  	|
|  **Annotations**	|  	|  	|
|  remontée aéro-souterraine	|  	|  <img src="../_static/images/ras.png">	|
|  étiquettes réseaux divers	|  	|  <img src="../_static/images/annotation_resdivers.png">	|
|  	|  	|  	|
|  **Armoire**	|  	|  	|
|  armoire	|  symbole armoire	|  <img src="../_static/images/armoire.png">	|
|  	|  	|  	|
|  **Câbles**	|  	|  	|
|  câble souterrain EP	|  	|  <img src="../_static/images/csout.png">	|
|  câble souterrain incertain	|  	|  <img src="../_static/images/sout_incer.png">	|
|  câble torsadé EP	|  	|  <img src="../_static/images/caerien.png">	|
|  	|  	|  	|
|  **Fourreau**	|  	|  	|
|  fourreau EP	|  	|  <img src="../_static/images/fourreau.png">	|
|  	|  	|  	|
|  **Réseaux divers**	|  	|  	|
|  Réseaux divers	|  	|  <img src="../_static/images/reseau_divers.png">	|

## **Annexe II – Données de la base technique à renseigner**

### ***Fiche armoires – table « ARMOIRES » - désignation des champs***

- numero_voie : numéro de la voie où est située l’armoire (texte **libre**)
- type_voie : type de voie (dans le cas de nouveau lotissement, mentionner voie nouvelle) (choix sur **liste**) - **IMPORTANT : dans le cas où le nom de voie contient le type de voie (rue, chemin etc..) cette colonne n’est pas à remplir**
- nom_voie : nom de voie (dans le cas de nouveau lotissement, mentionner voie nouvelle) (choix sur **liste**)
- état : état de l’armoire (choix sur **liste**)
- enveloppe_pose : mode de pose de l’armoire (choix sur **liste**)
- nbre_départ : nombre de départs dans l’armoire (texte **libre**) **– exemple de saisie obligatoire : 1**
- fermeture : type de fermeture de l’armoire (choix sur **liste**)
- remarque : champ observations (texte **libre**)

### ***Fiche départs – table « DEPARTS » - désignation des champs***

- nature_depart : type de départ (choix sur **liste**)
- désignation : désignation (choix sur **liste**)
- nature_protection : nature de la protection utilisée (choix sur **liste**). **Lorsque la protection du départ est réalisée par un interrupteur différentiel et fusible il est recommandé de mettre ce choix.**
- calibrage_protection : calibre de la protection en A (texte **libre**) **– exemple de saisie obligatoire : 32. Lorsque le choix de la colonne ‘nature\_protection’ est ‘interdifférentiel et fusible’ indiquer le calibre du fusible (par ex : 16)**
- depart_sens_diff : sensibilité du différentiel en mA (texte **libre**) – **exemple de saisie obligatoire : 300. Lorsque le choix de la colonne ‘nature\_protection’ est ‘interdifférentiel et fusible’ indiquer la sensibilité de l’interrupteur différentiel (par ex : 30)**
- iphase1 : intensité entre phase1 et neutre en A (texte **libre**) – **exemple de saisie obligatoire : 20 – à renseigner dans le cas d’une prise de mesure ou d’équilibrage**
- iphase2 : intensité entre phase2 et neutre en A (texte **libre**) – **exemple de saisie obligatoire : 16 – à renseigner dans le cas d’une prise de mesure ou d’équilibrage** iphase3 : intensité entre phase3 et neutre en A (texte **libre**) – **exemple de saisie obligatoire : 25 – à renseigner dans le cas d’une prise de mesure ou d’équilibrage**
- heure_allumage : heure d’allumage de l’éclairage public (texte **libre**) **– exemple de saisie obligatoire : 15:00**
- heure_extinction : heure d’extinction de l’éclairage public (texte **libre**) **– exemple de saisie obligatoire : 15:00**
- régime_allumage : type de régime d’allumage (choix sur **liste**)
- pilote : indiquer le type de commande d’allumage par exemple pulsadis, récepteur télécommandé, horloge astronomique, cellule photoélectrique, sonde à fibre optique…etc (texte **libre**)
- remarque : champ observations (texte **libre**)

### ***Fiche câbles – table « CABLE » - désignation des champs***

- section_cable: identification de la section du câble en aérien ou souterrain selon le cahier des charges (texte **libre**) – **exemple de saisie obligatoire : 3G16mm²**
- classe : classe de précision – (choix sur **liste**)
- remarque : champ observations (texte **libre**)

### ***Fiche supports – table « SUPPORTS » - désignation des champs***

- numero_voie : numéro de la voie où est situé le support (texte **libre**)
- type_voie : type de voie où est situé le support (dans le cas de nouveau lotissement, mentionner voie nouvelle) (choix sur **liste**) - **IMPORTANT : dans le cas où le nom de voie contient le type de voie (rue, chemin etc..) cette colonne n’est pas à remplir**
- nom_voie : nom de voie où est situé le support (dans le cas de nouveau lotissement, mentionner voie nouvelle) (choix sur **liste**)
- état : état du support  (choix sur **liste**)
- terre : valeur de terre en ohms – **exemple de saisie obligatoire : 32**
- boitier : présence de boîtier classe II (boitier de raccordement à renseigner dans le cadre des travaux souterrains et/ou aériens) (choix sur **liste**)
- type_protection : type de protection en pied de mât du foyer (choix sur **liste**)
- sensibilité_diff : valeur de la sensibilité du différentiel en mA de la protection en pied de mât du foyer (texte **libre**) – **exemple de saisie obligatoire : 300**
- calibre_diff : valeur en A, fusible ou différentiel, de la protection en pied de mât du foyer (texte libre) – **exemple de saisie obligatoire : 20**
- entraxe_massif : entraxe du massif (choix sur **liste**)
- type_massif : type du massif (choix sur **liste**)
- prise_illumination : présence d’une prise illumination (choix sur **liste**)
- remarque : champ observations (texte **libre**)

### ***Fiche foyers – table « FOYERS » - désignation des champs***

- état : état du foyer  (choix sur **liste**)
- num_phase : numéro de la phase sur laquelle le foyer se trouve (1,2,3) dans le cas d’un équilibrage (choix sur **liste**)
- remarque : champ observations (texte **libre**)

### ***Fiche sources – table « SOURCES » - désignation des champs***

- état : état de la source  (choix sur **liste**)
- remarque : champ observations (texte **libre**)
