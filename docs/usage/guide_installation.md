# Guide d'installation du module de report

## **Téléchargement de QGIS**

Il est nécessaire de télécharger le logiciel QGIS pour bénéficier de l'utilisation du plugin de report des plans de recolement. Pour cela vous pouvez vous rendre sur [le site de QGIS utilisant ce lien ](https://www.qgis.org/en/site/forusers/alldownloads.html#osgeo4w-installer). Cliquez ensuite sur **OSGeo4W Installer** pour récupérer les fichiers d'installation.

Vous pouvez aussi utiliser directement [le lien suivant pour télécharger l&#39;installateur ](https://download.osgeo.org/osgeo4w/v2/osgeo4w-setup.exe)

### **Guide d'installation**

Aprés le téléchargement éxecuter le fichier et suivez les étapes suivantes :

* Etape 1


![](../_static/images/page_1.jpg)

* Etape 2


![](../_static/images/page_2.jpg)

* Etape 3


![](../_static/images/page_3.jpg)

* Etape 4


![](../_static/images/page_4.jpg)

Suivant le débit de votre connexion, la durée du téléchargement peut s'avérer plus ou moins longue.

## **Création des profils**

- profil  **report88 (travaux)** : dans le cadre de l'utilisation du module, un setup d'exécution est founi **(QGIS_Profil_report88.exe)** et demandé par l'entreprise. L'opérateur laissera les chemins d'installation proposés par défaut.

- profil  **detection88 (détection)** : dans le cadre de l'utilisation du module, un setup d'exécution est founi **(QGIS_Profil_detection88.exe)** et demandé par l'entreprise. L'opérateur laissera les chemins d'installation proposés par défaut.

### **Création des touches de raccourcis**

Afin de faciliter l'ouverture de QGIS suivant le profil enregistré une touche de raccouri sera mise en place. Pour se faire (QGIS doit être fermé), ouvrez une fenêtre d'explorateurs de fichiers puis tapez **C:\\OSGeo4W\\bin** et chercher qgis-bin.exe dans ce dossier.

![](../_static/images/expl_osgeo.jpg)

Sélectionnez ce fichier et à l'aide de la touche droite de votre souris, vous sélectionner **envoyers vers**/**Bureau (créer un raccourci)**. Vous pouvez le renommer en report_SDEV **pour le report (travaux)**.

L'opération est à renouveler pour la création de la touche de raccourci en la nommant report_detection **pour le report (détection)**.

Ensuite, il ne vous reste plus qu'à modifier les propriétés pour utiliser le profil du report. Pour cela, il suffit de sélectionner la touche de raccourci que vous venez de réaliser et à l'aide du clic droit de votre souris vous allez vous rendre dans le menu propriété du menu contextuel :

- modification des propriétés de la touche de raccourci **pour le report travaux**
![](../_static/images/chem_racc.jpg)
- modification des propriétés de la touche de raccourci **pour le report détection** : il suffit de remplacer la syntaxe **--profile report88** par **--profiledetection88**.

## **Paramètrage Icônes de travail et macros**

### **Macros**

A l'utilisation du plugin pour finaliser un recolement et lors de l'ouverture du document une fenêtre récapitulative des données de chantier doit s'ouvrir. Pour cela, QGIS utilise des macros. Si ces macros ne sont pas autorisées il ne vous est pas possible de voir cette fenêtre.

C'est pourquoi, il est recommandé en ouvrant QGIS (avec la touche de raccourci du report) d'aller dans le menu **Préférences\\options** et dans **général** (rendez vous en bas de la boite de dialogue) sélectionner **Toujours** pour le champ **activer les macros**.


![](../_static/images/para_macros.jpg)

### **Affichage couches**

Il peut s'avérer que certaines icônes n'apparaissent pas dans le menu. Vous devez alors contrôler si toutes les cases ont bien été cochées.


![](../_static/images/pan_couche.jpg)

## **Installation plugins**

### **Plugins complémentaires**

A toutes fins utiles certains plugins complémentaires seront nécessaires pour une bonne utilisation du report des recolements mais aussi de dépannage en cas de disfonctionnement.

En utilisant la touche de raccourci du profil report et après ouverture il suffit de sélectionner le menu **Extensions/installer_gérer les extensions** et de se positionner sur **toutes**


![](../_static/images/inst_ext_ttes.jpg)

**AnotherDXFImporter** : sélectionner le plugin et installer; le plugin est lancé depuis le menu **Vecteur**. Cette extension permet de transformer des fichiers dxf en shp ou gpkg.

**Plugin Reloader** : sélectionner le plugin et installer le plugin est lancé depuis le menu **Extensions**. Cette extension permet de recharger des extensions (selon sa configuration). Trés pratique pour Lumigis qui ne nécessitera pas de quitter QGIS.

**GeoCSV** : sélectionner le plugin et installer; le plugin est lancé depuis le menu **Extensions**. Cette extension permet de travailler sur un fichier csv (copier/coller des données)

**AdressesFr** : sélectionner le plugin et installer; le plugin est lancé depuis le menu **Extensions**. Cette extension permet de saisir des adresses postales et de se positionner sur la carte.

## **Mise à jour**

Plusieurs extensions ont été installées pour le fonctionnement et l'utilisation de fichiers. Lorsque des mises à jour sont disponibles un pop-pup signale les mises à jour
![](../_static/images/maj.png)
