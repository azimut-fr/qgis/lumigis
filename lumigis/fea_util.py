"""
/***************************************************************************
 FeaUtil
                                 A QGIS plugin
 Featue utils
        begin                : 2023-12-04
        copyright            : (C) 2023 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsExpression, QgsFeature, QgsFeatureRequest


class FeaUtil:
    def __init__(self): ...

    @staticmethod
    def feature_by_insee_and_idnom(layer, col_id, col_insee, insee, col_idnom, idnom):
        if insee and idnom:
            id = None
            feature = None
            expr = QgsExpression(
                f"\"{col_insee}\" = '{insee}' and \"{col_idnom}\" = '{idnom}'"
            )
            expr = f"\"{col_insee}\" = '{insee}' and \"{col_idnom}\" = '{idnom}'"
            fea_iter = layer.getFeatures(QgsFeatureRequest(QgsExpression(expr)))
            if True or fea_iter.isValid():
                feature = QgsFeature()
                fea_iter.nextFeature(feature)
                fea_iter.close()

            return feature

    @staticmethod
    def add_changed_attribute_value(
        layer, feature, feature_dict, old_dico, attribute_name, attribute_value
    ):
        if layer is not None and feature is not None:
            if attribute_value == "NULL":
                attribute_value = None
            i = layer.fields().indexFromName(attribute_name)
            if i >= 0 and feature.attribute(i) != attribute_value:
                old_dico.update({i: feature.attribute(i)})
                feature_dict.update({i: attribute_value})

    @staticmethod
    def prepare_changed_attribute_dict(layer, feature, attributes_dict):
        old_dico = dict()
        new_dico = dict()
        if layer is not None and feature is not None:
            # init dico with existing values
            for i in range(feature.fields().count()):
                old_dico[i] = feature[i]
            new_dico = old_dico.copy()
            for key in attributes_dict.keys():
                attribute_value = attributes_dict[key]
                if attribute_value == "NULL":
                    attribute_value = None
                i = layer.fields().indexOf(key)
                new_dico.update({i: attribute_value})
        return old_dico, new_dico

    @staticmethod
    def update_feature(layer, feature, dico, geom):
        old_dico, new_dico = FeaUtil.prepare_changed_attribute_dict(
            layer, feature, dico
        )
        geom_changed = True if geom != feature.geometry() else False
        if len(new_dico) or geom_changed:
            layer.startEditing()
            if geom_changed:
                layer.changeGeometry(feature.id(), geom)
            if len(new_dico):
                layer.changeAttributeValues(feature.id(), new_dico, old_dico)
            layer.commitChanges()
