from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QEvent, QObject, Qt
from qgis.PyQt.QtWidgets import QApplication


class EventFilter(QObject):
    def __init__(self, iface: QgisInterface):
        super(EventFilter, self).__init__()
        self.iface = iface
        self._current_tool = None

    def set_current_tool(self, action):
        self._current_tool = action

    def eventFilter(self, obj, event):
        """
        obj : QObject whose event is intercepted
        event: QEvent received

        returns:
            bool: True to intercept the event, False to forward it
        """
        if event.type() == QEvent.KeyRelease:
            if event.key() == Qt.Key_Escape:
                while QApplication.overrideCursor() is not None:
                    QApplication.restoreOverrideCursor()
                if self._current_tool:
                    self._current_tool.rubberBand.reset()
                    self._current_tool.reset()
                    self.iface.mapCanvas().unsetMapTool(self._current_tool)
                    self._current_tool = None
                return True
        return False
