<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyLocal="1" simplifyAlgorithm="0" maxScale="0" minScale="100000000" labelsEnabled="1" simplifyMaxScale="1" simplifyDrawingHints="0" symbologyReferenceScale="-1" version="3.28.5-Firenze" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" styleCategories="AllStyleCategories" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal accumulate="0" endExpression="" startExpression="" enabled="0" mode="0" startField="" durationUnit="min" limitMode="0" durationField="" endField="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation extrusionEnabled="0" binding="Centroid" clamping="Terrain" symbology="Line" showMarkerSymbolInSurfacePlots="0" type="IndividualFeatures" zscale="1" respectLayerSymbol="1" zoffset="0" extrusion="0">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" frame_rate="10" type="line" alpha="1" is_animated="0" force_rhr="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="152,125,183,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" frame_rate="10" type="fill" alpha="1" is_animated="0" force_rhr="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="152,125,183,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="109,89,131,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" frame_rate="10" type="marker" alpha="1" is_animated="0" force_rhr="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="152,125,183,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="109,89,131,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 forceraster="0" referencescale="-1" symbollevels="0" type="RuleRenderer" enableorderby="0">
    <rules key="{9ac1a0d0-2e77-43de-819e-7f85a55f26bc}">
      <rule filter=" @map_scale > 2500" symbol="0" scalemaxdenom="1000000" key="{d2a7d2c8-9b0c-46d0-b704-424422dc9480}" scalemindenom="2500"/>
      <rule filter="@map_scale &lt;= 2500" label="note" symbol="1" scalemaxdenom="2500" key="{258fb294-7aef-45ad-8427-cf6e5acebf9e}"/>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" frame_rate="10" type="marker" alpha="1" is_animated="0" force_rhr="0" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="255,255,255,255" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgd2lkdGg9IjMyLjg4NDkzbW0iCiAgIGhlaWdodD0iMzYuMTI0NDQzbW0iCiAgIHZpZXdCb3g9IjAgMCAxMTYuNTIxNCAxMjcuOTk5OTkiCiAgIGlkPSJzdmc0MjM3IgogICB2ZXJzaW9uPSIxLjEiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM0MjM5IiAvPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTQyNDIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTAiCiAgICAgc3R5bGU9ImZpbGw6I2JkYmNiYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDQxLjEgNjAuOCwzMCBjIDEuNiwwLjggMi41LDIuMiAyLjgsNCAwLjMsMS43IC0wLjIsMy4zIC0xLjQsNC42IGwgLTQxLjIsNDQuOSBjIC0xLjksMiAtNC44LDIuMSAtNywwLjQgTCA0LjYsODEuNiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDM2LjYgNjAuOCwzMC4xIGMgMS42LDAuNyAyLjUsMi4xIDIuOCwzLjkgMC4zLDEuNyAtMC4yLDMuNCAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC41IEwgNC42LDc3LjEgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU0IgogICAgIHN0eWxlPSJmaWxsOiNlOWU5ZTk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gNDkuMSwzMi4xIDYwLjgsMzAuMSBjIDEuNiwwLjggMi41LDIuMiAyLjgsMy45IDAuMywxLjggLTAuMiwzLjQgLTEuNCw0LjcgbCAtNDEuMiw0NC44IGMgLTEuOSwyLjEgLTQuOCwyLjIgLTcsMC41IEwgNC42LDcyLjYgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU2IgogICAgIGQ9Im0gNDkuMSwyNy43IDYwLjgsMzAgYyAxLjYsMC44IDIuNSwyLjIgMi44LDQgMC4zLDEuNyAtMC4yLDMuMyAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC40IEwgNC42LDY4LjIgWiIgZmlsbD0icGFyYW0oZmlsbCkiIGZpbGwtb3BhY2l0eT0icGFyYW0oZmlsbC1vcGFjaXR5KSIgc3Ryb2tlPSJwYXJhbShvdXRsaW5lKSIgc3Ryb2tlLW9wYWNpdHk9InBhcmFtKG91dGxpbmUtb3BhY2l0eSkiIHN0cm9rZS13aWR0aD0icGFyYW0ob3V0bGluZS13aWR0aCkiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ1OCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDM5LDM1LjYgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjEsLTIuNCA0LjMsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMSw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDI5LjUsNDQuNSBjIC0wLjgsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMSwtMS4xIDIuOCwxLjkgNC4yLDUuMyAzLjEsNy43IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjIiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxOS45LDUzLjUgYyAtMC43LC0xLjYgLTAuOCwtMy4yIC0wLjIsLTQuNSAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwLjQsNjIuNCBjIC0wLjcsLTEuNiAtMC44LC0zLjIgLTAuMiwtNC41IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMSAyLjgsMS44IDQuMSw1LjIgMyw3LjYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDQuNyw3NS42IEMgNC42LDc1LjUgNC41LDc1LjUgNC40LDc1LjQgMS41LDczLjYgMC4yLDcwLjEgMS4zLDY3LjcgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjgiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0My40LDMwLjcgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDMzLjksMzkuNiBjIC0wLjcsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMS4xIDIuOCwxLjkgNC4xLDUuMyAzLDcuNyIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDcyIgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMjQuNCw0OC42IGMgLTAuOCwtMS42IC0wLjgsLTMuMiAtMC4yLC00LjUgMS4xLC0yLjQgNC4zLC0yLjkgNy4xLC0xLjEgMi44LDEuOSA0LjIsNS4zIDMuMSw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDE0LjgsNTcuNSBDIDE0LjEsNTUuOSAxNCw1NC4zIDE0LjYsNTMgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NzYiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA2LDY3LjMgQyA1LjIsNjUuNyA1LjIsNjQuMSA1LjgsNjIuOCBjIDEuMSwtMi40IDQuMywtMi45IDcuMSwtMSAyLjgsMS44IDQuMiw1LjIgMy4xLDcuNiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDc4IgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gMjUuNiw3Ni40IDQ1LjIsNTYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDM0LjEsNzkuMyA0NC4yLDY4LjgiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDUwLjYsNjEuMyA2MS43LDQ5LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDY5LjIsNTYgNzksNTIuNSA5NS4yLDU5LjYgNjMuNyw5Ni45IFoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NiIKICAgICBzdHlsZT0iZmlsbDojZDNkMmQyO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTpub25lIgogICAgIGQ9Ik0gOTQuNCw1OS4yIDc0LjMsODMgYyAwLDAgLTMuMSwxLjMgLTIuNCwtMC4xIEwgODQuMSw1OS40IGMgNiwtMy4yIDguMiwtMy41IDEwLjMsLTAuMiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDg4IgogICAgIHN0eWxlPSJmaWxsOiM5MThmOTA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTEzLjUsMTUuMjEgLTExLjksMC41OSBjIDAsMCAtMTcuNjYsMzguNDggLTE4LjUsNDUuNTEgLTAuNjEsNS4wOCA4LjY4LC02LjcyIDExLjYsLTEuMTEgMC4zLDMuOCAxOC44LC00NC45OSAxOC44LC00NC45OSB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTAiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSAxMDEuNiwxNS44IDg4LjIsOS44IDY5LjIsNTYgYyAtMS40LDMuNCAxMi41LC02LjQ4IDEzLjksNS42MiAtMS4yLDIuOSAxMi44LC0zMS43MiAxOC41LC00NS44MiB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA5MC4zLDguMiA4Ny4xLDYuMDExIGMgMCwwIC03Ljk2LDE4Ljc0OSAtMTYuMDgsMzguNTE5IDAsMCAtMC45NSw1LjM2IC0xLjc4LDExLjE2IEMgNjguMzQsNjEuOTMgOTAuMyw4LjIgOTAuMyw4LjIgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDk0IgogICAgIHN0eWxlPSJmaWxsOiMyMzFmMjA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gNzUuNiw4Mi44IDYzLjcsOTYuOSBjIDAuNywtNS44IDEuNSwtMTEuNiAyLjMsLTE3LjQgMi44LDIuOSA2LjEsNCA5LjYsMy4zIHoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ5OCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwNC43LDQuMiBjIDcsMy4yIDEwLjksOC4zIDguNiwxMS42IC0yLjIsMy4yIC05LjcsMy4zIC0xNi43LDAuMSBDIDg5LjYsMTIuOCA4NS44LDcuNiA4OCw0LjMgOTAuMywxLjEgOTcuOCwxIDEwNC43LDQuMiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg1MDAiCiAgICAgc3R5bGU9ImZpbGw6IzIzMWYyMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMDEuOSw4LjMgYyAyLjEsMSAzLjIsMi41IDIuNiwzLjUgLTAuNywwLjkgLTIuOSwxIC01LDAgLTIuMSwtMC45IC0zLjMsLTIuNSAtMi42LC0zLjUgMC43LC0wLjkgMi45LC0wLjkgNSwwIHoiIC8+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255" name="outline_color"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="8" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" frame_rate="10" type="marker" alpha="1" is_animated="0" force_rhr="0" name="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="255,255,255,255" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgd2lkdGg9IjMyLjg4NDkzbW0iCiAgIGhlaWdodD0iMzYuMTI0NDQzbW0iCiAgIHZpZXdCb3g9IjAgMCAxMTYuNTIxNCAxMjcuOTk5OTkiCiAgIGlkPSJzdmc0MjM3IgogICB2ZXJzaW9uPSIxLjEiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM0MjM5IiAvPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTQyNDIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTAiCiAgICAgc3R5bGU9ImZpbGw6I2JkYmNiYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDQxLjEgNjAuOCwzMCBjIDEuNiwwLjggMi41LDIuMiAyLjgsNCAwLjMsMS43IC0wLjIsMy4zIC0xLjQsNC42IGwgLTQxLjIsNDQuOSBjIC0xLjksMiAtNC44LDIuMSAtNywwLjQgTCA0LjYsODEuNiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDM2LjYgNjAuOCwzMC4xIGMgMS42LDAuNyAyLjUsMi4xIDIuOCwzLjkgMC4zLDEuNyAtMC4yLDMuNCAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC41IEwgNC42LDc3LjEgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU0IgogICAgIHN0eWxlPSJmaWxsOiNlOWU5ZTk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gNDkuMSwzMi4xIDYwLjgsMzAuMSBjIDEuNiwwLjggMi41LDIuMiAyLjgsMy45IDAuMywxLjggLTAuMiwzLjQgLTEuNCw0LjcgbCAtNDEuMiw0NC44IGMgLTEuOSwyLjEgLTQuOCwyLjIgLTcsMC41IEwgNC42LDcyLjYgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU2IgogICAgIGQ9Im0gNDkuMSwyNy43IDYwLjgsMzAgYyAxLjYsMC44IDIuNSwyLjIgMi44LDQgMC4zLDEuNyAtMC4yLDMuMyAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC40IEwgNC42LDY4LjIgWiIgZmlsbD0icGFyYW0oZmlsbCkiIGZpbGwtb3BhY2l0eT0icGFyYW0oZmlsbC1vcGFjaXR5KSIgc3Ryb2tlPSJwYXJhbShvdXRsaW5lKSIgc3Ryb2tlLW9wYWNpdHk9InBhcmFtKG91dGxpbmUtb3BhY2l0eSkiIHN0cm9rZS13aWR0aD0icGFyYW0ob3V0bGluZS13aWR0aCkiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ1OCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDM5LDM1LjYgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjEsLTIuNCA0LjMsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMSw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDI5LjUsNDQuNSBjIC0wLjgsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMSwtMS4xIDIuOCwxLjkgNC4yLDUuMyAzLjEsNy43IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjIiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxOS45LDUzLjUgYyAtMC43LC0xLjYgLTAuOCwtMy4yIC0wLjIsLTQuNSAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwLjQsNjIuNCBjIC0wLjcsLTEuNiAtMC44LC0zLjIgLTAuMiwtNC41IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMSAyLjgsMS44IDQuMSw1LjIgMyw3LjYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDQuNyw3NS42IEMgNC42LDc1LjUgNC41LDc1LjUgNC40LDc1LjQgMS41LDczLjYgMC4yLDcwLjEgMS4zLDY3LjcgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjgiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0My40LDMwLjcgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDMzLjksMzkuNiBjIC0wLjcsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMS4xIDIuOCwxLjkgNC4xLDUuMyAzLDcuNyIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDcyIgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMjQuNCw0OC42IGMgLTAuOCwtMS42IC0wLjgsLTMuMiAtMC4yLC00LjUgMS4xLC0yLjQgNC4zLC0yLjkgNy4xLC0xLjEgMi44LDEuOSA0LjIsNS4zIDMuMSw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDE0LjgsNTcuNSBDIDE0LjEsNTUuOSAxNCw1NC4zIDE0LjYsNTMgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NzYiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA2LDY3LjMgQyA1LjIsNjUuNyA1LjIsNjQuMSA1LjgsNjIuOCBjIDEuMSwtMi40IDQuMywtMi45IDcuMSwtMSAyLjgsMS44IDQuMiw1LjIgMy4xLDcuNiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDc4IgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gMjUuNiw3Ni40IDQ1LjIsNTYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDM0LjEsNzkuMyA0NC4yLDY4LjgiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDUwLjYsNjEuMyA2MS43LDQ5LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDY5LjIsNTYgNzksNTIuNSA5NS4yLDU5LjYgNjMuNyw5Ni45IFoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NiIKICAgICBzdHlsZT0iZmlsbDojZDNkMmQyO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTpub25lIgogICAgIGQ9Ik0gOTQuNCw1OS4yIDc0LjMsODMgYyAwLDAgLTMuMSwxLjMgLTIuNCwtMC4xIEwgODQuMSw1OS40IGMgNiwtMy4yIDguMiwtMy41IDEwLjMsLTAuMiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDg4IgogICAgIHN0eWxlPSJmaWxsOiM5MThmOTA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTEzLjUsMTUuMjEgLTExLjksMC41OSBjIDAsMCAtMTcuNjYsMzguNDggLTE4LjUsNDUuNTEgLTAuNjEsNS4wOCA4LjY4LC02LjcyIDExLjYsLTEuMTEgMC4zLDMuOCAxOC44LC00NC45OSAxOC44LC00NC45OSB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTAiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSAxMDEuNiwxNS44IDg4LjIsOS44IDY5LjIsNTYgYyAtMS40LDMuNCAxMi41LC02LjQ4IDEzLjksNS42MiAtMS4yLDIuOSAxMi44LC0zMS43MiAxOC41LC00NS44MiB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA5MC4zLDguMiA4Ny4xLDYuMDExIGMgMCwwIC03Ljk2LDE4Ljc0OSAtMTYuMDgsMzguNTE5IDAsMCAtMC45NSw1LjM2IC0xLjc4LDExLjE2IEMgNjguMzQsNjEuOTMgOTAuMyw4LjIgOTAuMyw4LjIgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDk0IgogICAgIHN0eWxlPSJmaWxsOiMyMzFmMjA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gNzUuNiw4Mi44IDYzLjcsOTYuOSBjIDAuNywtNS44IDEuNSwtMTEuNiAyLjMsLTE3LjQgMi44LDIuOSA2LjEsNCA5LjYsMy4zIHoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ5OCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwNC43LDQuMiBjIDcsMy4yIDEwLjksOC4zIDguNiwxMS42IC0yLjIsMy4yIC05LjcsMy4zIC0xNi43LDAuMSBDIDg5LjYsMTIuOCA4NS44LDcuNiA4OCw0LjMgOTAuMywxLjEgOTcuOCwxIDEwNC43LDQuMiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg1MDAiCiAgICAgc3R5bGU9ImZpbGw6IzIzMWYyMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMDEuOSw4LjMgYyAyLjEsMSAzLjIsMi41IDIuNiwzLjUgLTAuNywwLjkgLTIuOSwxIC01LDAgLTIuMSwtMC45IC0zLjMsLTIuNSAtMi42LC0zLjUgMC43LC0wLjkgMi45LC0wLjkgNSwwIHoiIC8+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="0" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style forcedBold="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontUnderline="0" multilineHeight="1" fontWordSpacing="0.1875" allowHtml="0" fieldName="texte" legendString="Aa" fontLetterSpacing="0.1875" textColor="0,0,0,255" fontStrikeout="0" forcedItalic="0" textOpacity="1" previewBkgrdColor="255,255,255,255" isExpression="0" fontKerning="1" capitalization="0" multilineHeightUnit="Percentage" useSubstitutions="0" fontWeight="25" fontFamily="RobotoLight" namedStyle="" blendMode="0" fontSize="10" textOrientation="horizontal" fontSizeUnit="Point" fontItalic="0">
        <families/>
        <text-buffer bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferNoFill="1" bufferDraw="0" bufferJoinStyle="128" bufferColor="255,255,255,255" bufferSize="1" bufferSizeUnits="MM" bufferOpacity="1" bufferBlendMode="0"/>
        <text-mask maskType="0" maskOpacity="1" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskEnabled="0" maskSize="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeUnits="MM"/>
        <background shapeSizeUnit="MM" shapeOffsetX="0" shapeOffsetUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRotation="0" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="MM" shapeOffsetY="0" shapeBorderWidth="0.29999999999999999" shapeSizeX="1" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="1" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiX="1" shapeSizeY="1" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeSizeType="0" shapeBorderColor="227,26,28,255" shapeBlendMode="0" shapeRadiiUnit="MM" shapeType="0" shapeRadiiY="1" shapeSVGFile="" shapeJoinStyle="64" shapeOpacity="1">
          <symbol clip_to_extent="1" frame_rate="10" type="marker" alpha="1" is_animated="0" force_rhr="0" name="markerSymbol">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" enabled="1" pass="0" locked="0">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="square" name="cap_style"/>
                <Option type="QString" value="231,113,72,255" name="color"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="circle" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="2" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol clip_to_extent="1" frame_rate="10" type="fill" alpha="1" is_animated="0" force_rhr="0" name="fillSymbol">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleFill" enabled="1" pass="0" locked="0">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,255,255,255" name="color"/>
                <Option type="QString" value="round" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="227,26,28,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0.3" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetDist="1" shadowOffsetAngle="135" shadowOpacity="0.69999999999999996" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="1" shadowRadius="1.5" shadowUnder="0" shadowOffsetGlobal="1" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowColor="0,0,0,255" shadowBlendMode="6" shadowRadiusAlphaOnly="0" shadowScale="100" shadowRadiusUnit="MM"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format decimals="3" placeDirectionSymbol="0" wrapChar="" reverseDirectionSymbol="0" autoWrapLength="0" useMaxLineLengthForAutoWrap="1" formatNumbers="0" plussign="0" multilineAlign="3" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" addDirectionSymbol="0"/>
      <placement distMapUnitScale="3x:0,0,0,0,0,0" lineAnchorType="0" yOffset="0" lineAnchorTextPoint="CenterOfText" overlapHandling="PreventOverlap" rotationUnit="AngleDegrees" geometryGenerator="" polygonPlacementFlags="2" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" rotationAngle="0" repeatDistance="0" repeatDistanceUnits="MM" dist="0" preserveRotation="1" geometryGeneratorType="PointGeometry" overrunDistance="0" fitInPolygonOnly="0" maxCurvedCharAngleIn="25" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" centroidWhole="0" placement="0" centroidInside="0" lineAnchorClipping="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" quadOffset="4" geometryGeneratorEnabled="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" priority="5" lineAnchorPercent="0.5" allowDegraded="0" placementFlags="10" layerType="PointGeometry" distUnits="MM" xOffset="0" offsetUnits="MM" offsetType="0" maxCurvedCharAngleOut="-25"/>
      <rendering scaleVisibility="1" fontMinPixelSize="3" obstacle="1" labelPerPart="0" fontMaxPixelSize="10000" scaleMax="1501" limitNumLabels="0" zIndex="5" unplacedVisibility="0" drawLabels="1" upsidedownLabels="0" scaleMin="0" fontLimitPixelSize="0" minFeatureSize="0" mergeLines="0" obstacleType="0" obstacleFactor="1" maxNumLabels="2000"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" value="" name="name"/>
          <Option type="Map" name="properties">
            <Option type="Map" name="LabelRotation">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="rot_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="PositionX">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="x_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="PositionY">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="y_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="ShapeBorderColor">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FF0000', '#000000')" name="expression"/>
              <Option type="int" value="3" name="type"/>
            </Option>
            <Option type="Map" name="ShapeFillColor">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
              <Option type="int" value="3" name="type"/>
            </Option>
          </Option>
          <Option type="QString" value="collection" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
          <Option type="int" value="0" name="blendMode"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
          <Option type="bool" value="false" name="drawToAllParts"/>
          <Option type="QString" value="0" name="enabled"/>
          <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
          <Option type="QString" value="&lt;symbol clip_to_extent=&quot;1&quot; frame_rate=&quot;10&quot; type=&quot;line&quot; alpha=&quot;1&quot; is_animated=&quot;0&quot; force_rhr=&quot;0&quot; name=&quot;symbol&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;SimpleLine&quot; enabled=&quot;1&quot; pass=&quot;0&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;align_dash_pattern&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;square&quot; name=&quot;capstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;5;2&quot; name=&quot;customdash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;customdash_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;customdash_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;dash_pattern_offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;dash_pattern_offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;draw_inside_polygon&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;82,82,82,255&quot; name=&quot;line_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;line_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.3&quot; name=&quot;line_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;line_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_end&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_end_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_end_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_start&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_start_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_start_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;use_custom_dash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;width_map_unit_scale&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
          <Option type="double" value="0.2" name="minLength"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
          <Option type="QString" value="MM" name="minLengthUnit"/>
          <Option type="double" value="1" name="offsetFromAnchor"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
          <Option type="double" value="1.9999999999999998" name="offsetFromLabel"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="QString" value="texte" name="dualview/previewExpressions"/>
      <Option type="QString" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory spacingUnitScale="3x:0,0,0,0,0,0" diagramOrientation="Up" enabled="0" showAxis="0" spacing="0" spacingUnit="MM" penAlpha="255" penWidth="0" sizeType="MM" penColor="#000000" lineSizeType="MM" height="15" lineSizeScale="3x:0,0,0,0,0,0" direction="1" scaleBasedVisibility="0" barWidth="5" rotationOffset="270" width="15" scaleDependency="Area" opacity="1" labelPlacementMethod="XHeight" minimumSize="0" backgroundColor="#ffffff" sizeScale="3x:0,0,0,0,0,0" maxScaleDenominator="1e+08" minScaleDenominator="0" backgroundAlpha="255">
      <fontProperties underline="0" style="" description="Roboto,11,-1,5,25,0,0,0,0,0" bold="0" italic="0" strikethrough="0"/>
      <attribute field="" colorOpacity="1" label="" color="#000000"/>
      <axisSymbol>
        <symbol clip_to_extent="1" frame_rate="10" type="line" alpha="1" is_animated="0" force_rhr="0" name="">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" enabled="1" pass="0" locked="0">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" dist="0" zIndex="0" showAll="1" linePlacementFlags="18" obstacle="0" placement="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="id_note">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="texte">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="important">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_creation">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_modification">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="user_creation">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="user_modification">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="x_label">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="y_label">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rot_label">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id_note" index="0" name=""/>
    <alias field="texte" index="1" name=""/>
    <alias field="important" index="2" name=""/>
    <alias field="date_creation" index="3" name=""/>
    <alias field="date_modification" index="4" name=""/>
    <alias field="user_creation" index="5" name=""/>
    <alias field="user_modification" index="6" name=""/>
    <alias field="x_label" index="7" name=""/>
    <alias field="y_label" index="8" name=""/>
    <alias field="rot_label" index="9" name=""/>
  </aliases>
  <defaults>
    <default field="id_note" expression="" applyOnUpdate="0"/>
    <default field="texte" expression="" applyOnUpdate="0"/>
    <default field="important" expression="" applyOnUpdate="0"/>
    <default field="date_creation" expression="now()" applyOnUpdate="0"/>
    <default field="date_modification" expression="now()" applyOnUpdate="1"/>
    <default field="user_creation" expression=" @user_full_name " applyOnUpdate="0"/>
    <default field="user_modification" expression=" @user_full_name " applyOnUpdate="1"/>
    <default field="x_label" expression="" applyOnUpdate="0"/>
    <default field="y_label" expression="" applyOnUpdate="0"/>
    <default field="rot_label" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="id_note" unique_strength="1" exp_strength="0" constraints="3" notnull_strength="1"/>
    <constraint field="texte" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="important" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="date_creation" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="date_modification" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="user_creation" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="user_modification" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="x_label" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="y_label" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint field="rot_label" unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_note" exp="" desc=""/>
    <constraint field="texte" exp="" desc=""/>
    <constraint field="important" exp="" desc=""/>
    <constraint field="date_creation" exp="" desc=""/>
    <constraint field="date_modification" exp="" desc=""/>
    <constraint field="user_creation" exp="" desc=""/>
    <constraint field="user_modification" exp="" desc=""/>
    <constraint field="x_label" exp="" desc=""/>
    <constraint field="y_label" exp="" desc=""/>
    <constraint field="rot_label" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column hidden="0" type="field" name="id_note" width="-1"/>
      <column hidden="0" type="field" name="texte" width="674"/>
      <column hidden="0" type="field" name="important" width="82"/>
      <column hidden="0" type="field" name="date_creation" width="172"/>
      <column hidden="0" type="field" name="date_modification" width="-1"/>
      <column hidden="0" type="field" name="user_creation" width="-1"/>
      <column hidden="0" type="field" name="user_modification" width="-1"/>
      <column hidden="0" type="field" name="x_label" width="-1"/>
      <column hidden="0" type="field" name="y_label" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column hidden="0" type="field" name="rot_label" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="0" name="date_creation"/>
    <field editable="0" name="date_modification"/>
    <field editable="0" name="id_note"/>
    <field editable="1" name="important"/>
    <field editable="0" name="rot_label"/>
    <field editable="1" name="texte"/>
    <field editable="0" name="user_creation"/>
    <field editable="0" name="user_modification"/>
    <field editable="0" name="x_label"/>
    <field editable="0" name="y_label"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="date_creation"/>
    <field labelOnTop="0" name="date_modification"/>
    <field labelOnTop="0" name="id_note"/>
    <field labelOnTop="0" name="important"/>
    <field labelOnTop="0" name="rot_label"/>
    <field labelOnTop="0" name="texte"/>
    <field labelOnTop="0" name="user_creation"/>
    <field labelOnTop="0" name="user_modification"/>
    <field labelOnTop="0" name="x_label"/>
    <field labelOnTop="0" name="y_label"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="date_creation"/>
    <field reuseLastValue="0" name="date_modification"/>
    <field reuseLastValue="0" name="id_note"/>
    <field reuseLastValue="0" name="important"/>
    <field reuseLastValue="0" name="rot_label"/>
    <field reuseLastValue="0" name="texte"/>
    <field reuseLastValue="0" name="user_creation"/>
    <field reuseLastValue="0" name="user_modification"/>
    <field reuseLastValue="0" name="x_label"/>
    <field reuseLastValue="0" name="y_label"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"texte"</previewExpression>
  <mapTip>[% "texte" %]
&lt;/br>
modif par [% "user_modification" %] le [% "date_modification" %]</mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
