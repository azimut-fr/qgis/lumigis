<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" simplifyLocal="1" labelsEnabled="1" readOnly="0" hasScaleBasedVisibilityFlag="0" symbologyReferenceScale="200" version="3.30.1-'s-Hertogenbosch" maxScale="1" simplifyDrawingHints="0" simplifyAlgorithm="0" simplifyMaxScale="1" minScale="5000" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" durationField="fid" startExpression="" durationUnit="min" startField="date_pose" accumulate="0" enabled="0" mode="0" fixedDuration="0" limitMode="0" endField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation showMarkerSymbolInSurfacePlots="0" type="IndividualFeatures" extrusionEnabled="0" extrusion="0" respectLayerSymbol="1" clamping="Terrain" zoffset="0" binding="Centroid" symbology="Line" zscale="1">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" is_animated="0" type="line" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{fde3db75-5918-4103-923b-c8684328f670}" enabled="1" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="square"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="145,82,45,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.6"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{bd8438ed-f5e9-4416-9262-26d5215165be}" enabled="1" locked="0" class="SimpleFill">
          <Option type="Map">
            <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="color" value="145,82,45,255"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="104,59,32,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="style" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{ae981f9b-f82f-4eef-be30-dbde73a50706}" enabled="1" locked="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="145,82,45,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="diamond"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="104,59,32,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="3"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="200" type="RuleRenderer" symbollevels="0" forceraster="0" enableorderby="0">
    <rules key="{9584e0b9-7efd-4ac4-ad63-1d6ac48b5ccb}">
      <rule label="Actif" filter=" &quot;etat&quot; = 'Hors contrat' " key="{5a391dc9-9f01-435b-91fe-4d346f14cb9d}">
        <rule symbol="0" filter=" &quot;cellule&quot; = 'ARMOIRE'" key="{7a8c7280-bab7-423f-80da-b70057d3d9e5}"/>
      </rule>
    </rules>
    <symbols>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="0" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{553358bc-bef3-4c9c-93e7-4eeca723fcd2}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="171,187,254,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuMzkzOCAyNTE1Ljc0OCIKICAgaWQ9InN2ZzgzMTMiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9YXJtb2lyZS5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSmVhbiBDbGF1ZGVcRGVza3RvcFxwaG90b3MgY2VsbHVsZXNcc21kZXY9YXJtb2lyZS5zdmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczgzMTUiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJiYXNlIgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxLjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnpvb209IjAuMTg1MzU0MDciCiAgICAgaW5rc2NhcGU6Y3g9IjI4NzAuMTgyNCIKICAgICBpbmtzY2FwZTpjeT0iMTYwMi4zMzg3IgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnOTQ3IgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGJvcmRlcmxheWVyPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjQ1MjIuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjM3NTQuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgxMDYiCiAgICAgICBvcmlnaW54PSIwIgogICAgICAgb3JpZ2lueT0iMCIKICAgICAgIHNwYWNpbmd4PSIxIgogICAgICAgc3BhY2luZ3k9IjEiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhODMxOCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpbmtzY2FwZTpsYWJlbD0iQ2FscXVlIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsNDI2Mi41OTk0KSI+CiAgICA8ZwogICAgICAgaWQ9Imc5NDciCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5MjQ0OTQsMCwwLDAuOTk2NTcyNTMsLTQ2NzYuMjEzNSwtMzE3Ny4zMzYyKSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDAzIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLDAuOTk1ODYyNjcsLTEuMDA0MTU1MywwLDgzMjkuODE4NywxMTkzLjM2NzMpIj4KICAgICAgICA8cmVjdAogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwZmY7c3Ryb2tlLXdpZHRoOjEyMy45NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlkPSJyZWN0OTAwIgogICAgICAgICAgIHdpZHRoPSIyMzU2LjM3NDgiCiAgICAgICAgICAgaGVpZ2h0PSI1MTkxLjAyMDUiCiAgICAgICAgICAgeD0iLTIxOTkuNTI3OCIKICAgICAgICAgICB5PSItMTY4MC4xNTU4IiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDBmZjtmaWxsLW9wYWNpdHk6MC45ODgyMDg7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjIxODYuOTY7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiCiAgICAgICAgICAgZD0iTSAtNDMyLjI0Njg3LDM1MTAuODY0OCBIIC0xMDIxLjM0MDUgViAyMjEzLjEwOTcgOTE1LjM1NDQ5IGggNTg5LjA5MzYzIDU4OS4wOTM2OSBWIDIyMTMuMTA5NyAzNTEwLjg2NDggWiIKICAgICAgICAgICBpZD0icGF0aDQ4MzEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwZmY7ZmlsbC1vcGFjaXR5OjAuOTg4MjA4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyMTQ2LjIxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIgogICAgICAgICAgIGQ9Ik0gLTE2MDIuMDU3Miw5MTUuMzU0NSBIIC0yMTgyLjc3NCBWIC0zNzIuODcwNzcgLTE2NjEuMDk2NSBoIDU4MC43MTY4IDU4MC43MTY2IFYgLTM3Mi44NzA3NyA5MTUuMzU0NSBaIgogICAgICAgICAgIGlkPSJwYXRoNDgyOSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMTEuNjE3Njk5LDkuOTUxNzU1MSwwLC04MTkuNzc0Myw5OC42NjA0NCkiCiAgICAgICAgICAgaWQ9Imc0NzY2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiPgogICAgICAgICAgPGxpbmUKICAgICAgICAgICAgIGlkPSJsaW5lMiIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDQiCiAgICAgICAgICAgICBkPSJtIDIuOTk4LDc0LjM3NyBjIC0wLjUwNCwwIC0xLjAxNCwtMC4xMjkgLTEuNDgzLC0wLjM5NiAtMS40MzksLTAuODIgLTEuOTQsLTIuNjUyIC0xLjEyLC00LjA5MiBsIDM5LC02OC4zNzQgYyAwLjgyMSwtMS40MzkgMi42NTQsLTEuOTQxIDQuMDkyLC0xLjEyIDEuNDM5LDAuODIxIDEuOTQsMi42NTMgMS4xMTksNC4wOTIgbCAtMzksNjguMzc0IEMgNS4wNTMsNzMuODMzIDQuMDQsNzQuMzc3IDIuOTk4LDc0LjM3NyBaIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMDAwMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgaWQ9InBhdGg2IgogICAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgIGlkPSJwYXRoOCIKICAgICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIGlkPSJwb2x5Z29uMTAiCiAgICAgICAgICAgICBwb2ludHM9IjQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzICIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiIC8+CiAgICAgICAgPC9nPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMTEuNjE3Njk5LDkuOTUxNzU0NywwLC0xOTg4Ljc5MjMsMjY5NS43OTY1KSIKICAgICAgICAgICBpZD0iZzQ3NjYtOSIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIj4KICAgICAgICAgIDxsaW5lCiAgICAgICAgICAgICBpZD0ibGluZTItNCIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDQtMCIKICAgICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDYtOSIKICAgICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDgtOSIKICAgICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIGlkPSJwb2x5Z29uMTAtOCIKICAgICAgICAgICAgIHBvaW50cz0iNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMDAwMCIgLz4KICAgICAgICA8L2c+CiAgICAgIDwvZz4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="&quot;rot_z&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{545ca318-38e5-4550-a737-9ca01560616a}">
      <rule description="étiquettes armoire à remplacer" filter="&quot;etat&quot; = 'A remplacer'" key="{6e6bb33b-af43-4633-b4b7-cebb4f0234f3}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'armoire à remplacer : '  || id_armoire" fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="255,201,78,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0.20000000000000001" bufferNoFill="0" bufferSizeUnits="MapUnit" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MapUnit" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="1" shapeRadiiX="0" shapeRotationType="1" shapeBorderColor="255,201,78,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0.14999999999999999" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0.14999999999999999" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0.14999999999999999" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="255,201,78,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0.15"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="5" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="0" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MapUnit" rotationAngle="0" quadOffset="4" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="1" upsidedownLabels="0" scaleMin="1" maxNumLabels="2000" fontMinPixelSize="1" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="2" scaleMax="5000" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{ce7b08e3-b5db-4e9e-a5af-972ad206b97b}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{3c4e7c91-1d22-4940-b7d1-e7f0c710accb}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,201,78,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="étiquettes armmoire à poser" filter="&quot;etat&quot; = 'A poser'" key="{590b2b0d-8524-47ea-bf35-d7fef235f820}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'armoire à poser : '  ||  &quot;id_armoire&quot; " fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="255,0,252,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0.20000000000000001" bufferNoFill="0" bufferSizeUnits="MapUnit" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MapUnit" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="1" shapeRadiiX="0" shapeRotationType="1" shapeBorderColor="255,0,252,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0.14999999999999999" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0.14999999999999999" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0.14999999999999999" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="255,0,252,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0.15"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="5" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="0" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MapUnit" rotationAngle="0" quadOffset="4" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="1" upsidedownLabels="0" scaleMin="1" maxNumLabels="2000" fontMinPixelSize="1" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="2" scaleMax="5000" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{bb88df04-b538-4a4f-8c75-9d059110d696}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{cd7f9fde-ab18-412e-8997-e2eca0d67054}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,0,252,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="texte armoire" filter="&quot;etat&quot; = 'Actif'" key="{624f035a-7b59-40b8-a669-6d98e99b114e}">
        <settings calloutType="balloon">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold Italic" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="1" previewBkgrdColor="255,255,255,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'armoire : ' || &quot;id_armoire&quot;" fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="21,0,255,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0" bufferNoFill="0" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="Point" shapeOffsetUnit="Point" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="Point" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="0" shapeRadiiX="0" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeBorderWidthUnit="Point" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="Point"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="1" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="Point" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="Point"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="12" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MM" rotationAngle="0" quadOffset="4" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="0" maxNumLabels="2000" fontMinPixelSize="3" mergeLines="0" obstacleType="1" minFeatureSize="0" obstacle="1" obstacleFactor="1" scaleMax="0" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="PositionX">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
                <Option type="Map" name="PositionY">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="balloon">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="double" name="cornerRadius" value="0.5"/>
              <Option type="QString" name="cornerRadiusMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="cornerRadiusUnit" value="MM"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option type="Map" name="properties">
                  <Option type="Map" name="OriginX">
                    <Option type="bool" name="active" value="false"/>
                    <Option type="int" name="type" value="1"/>
                    <Option type="QString" name="val" value=""/>
                  </Option>
                  <Option type="Map" name="OriginY">
                    <Option type="bool" name="active" value="false"/>
                    <Option type="int" name="type" value="1"/>
                    <Option type="QString" name="val" value=""/>
                  </Option>
                </Option>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="fillSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{b494aca6-cf2d-4696-8178-ad4b2864d405}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;204,204,204,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;21,0,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="margins" value="0.5,0.5,0.5,0.5"/>
              <Option type="QString" name="marginsUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="wedgeWidth" value="1"/>
              <Option type="QString" name="wedgeWidthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="wedgeWidthUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="QString" name="QFieldSync/action" value="remove"/>
      <Option type="QString" name="QFieldSync/cloud_action" value="offline"/>
      <Option type="QString" name="QFieldSync/photo_naming" value="{&quot;photo&quot;: &quot;'DCIM/armoires_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;, &quot;schema_elec&quot;: &quot;'DCIM/armoires_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;}"/>
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )"/>
      </Option>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option type="QString" name="geopdf/includeFeatures" value="false"/>
      <Option type="QString" name="qgis2web/Interactive" value="true"/>
      <Option type="QString" name="qgis2web/Visible" value="true"/>
      <Option type="QString" name="qgis2web/popup/acteur" value="no label"/>
      <Option type="QString" name="qgis2web/popup/cellule" value="no label"/>
      <Option type="QString" name="qgis2web/popup/created_at" value="no label"/>
      <Option type="QString" name="qgis2web/popup/date_action" value="no label"/>
      <Option type="QString" name="qgis2web/popup/date_depose" value="no label"/>
      <Option type="QString" name="qgis2web/popup/date_pose" value="no label"/>
      <Option type="QString" name="qgis2web/popup/desc_action" value="no label"/>
      <Option type="QString" name="qgis2web/popup/ecp_intervention_id" value="no label"/>
      <Option type="QString" name="qgis2web/popup/enveloppe_pose" value="no label"/>
      <Option type="QString" name="qgis2web/popup/enveloppe_poste_amont" value="no label"/>
      <Option type="QString" name="qgis2web/popup/etat" value="no label"/>
      <Option type="QString" name="qgis2web/popup/fermeture" value="no label"/>
      <Option type="QString" name="qgis2web/popup/id_armoire" value="no label"/>
      <Option type="QString" name="qgis2web/popup/insee" value="no label"/>
      <Option type="QString" name="qgis2web/popup/intervention" value="no label"/>
      <Option type="QString" name="qgis2web/popup/mapid" value="no label"/>
      <Option type="QString" name="qgis2web/popup/mslink" value="no label"/>
      <Option type="QString" name="qgis2web/popup/nbre_depart" value="no label"/>
      <Option type="QString" name="qgis2web/popup/nom_voie" value="no label"/>
      <Option type="QString" name="qgis2web/popup/numero" value="no label"/>
      <Option type="QString" name="qgis2web/popup/numero_voie" value="no label"/>
      <Option type="QString" name="qgis2web/popup/photo" value="no label"/>
      <Option type="QString" name="qgis2web/popup/pkid" value="no label"/>
      <Option type="QString" name="qgis2web/popup/remarque" value="no label"/>
      <Option type="QString" name="qgis2web/popup/rot_z" value="no label"/>
      <Option type="QString" name="qgis2web/popup/schema_elec" value="no label"/>
      <Option type="QString" name="qgis2web/popup/type_voie" value="no label"/>
      <Option type="QString" name="qgis2web/popup/uid4" value="no label"/>
      <Option type="QString" name="qgis2web/popup/updated_at" value="no label"/>
      <Option type="QString" name="qgis2web/popup/vetuste" value="no label"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory minScaleDenominator="1" backgroundAlpha="255" enabled="0" maxScaleDenominator="1e+08" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" diagramOrientation="Up" penWidth="0" direction="1" spacingUnit="MM" scaleBasedVisibility="0" scaleDependency="Area" height="15" spacingUnitScale="3x:0,0,0,0,0,0" sizeType="MM" rotationOffset="270" penColor="#000000" opacity="1" labelPlacementMethod="XHeight" barWidth="5" backgroundColor="#ffffff" lineSizeScale="3x:0,0,0,0,0,0" penAlpha="255" width="15" showAxis="0" spacing="0">
      <fontProperties underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" italic="0"/>
      <attribute label="" color="#000000" colorOpacity="1" field=""/>
      <axisSymbol>
        <symbol frame_rate="10" is_animated="0" type="line" clip_to_extent="1" name="" force_rhr="0" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" id="{75399f43-5dd5-4b10-b635-f05a0cfbba4f}" enabled="1" locked="0" class="SimpleLine">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" dist="0" linePlacementFlags="18" showAll="1" priority="0" placement="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_armoire">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nom_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Actif" value="Actif"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="A déposer" value="A déposer"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="vetuste">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Bon" value="Bon"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Moyen" value="Moyen"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Vétuste" value="Vétuste"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="fermeture">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Clés" value="Clés"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Triangle" value="Triangle"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Plat" value="Plat"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="enveloppe_poste_amont">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="enveloppe_pose">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Autre" value="Autre"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Encastrée" value="Encastrée"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Saillie" value="Saillie"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Sur poteau de distribution" value="Sur poteau de distribution"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Sur façade" value="Sur façade"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nbre_depart">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cellule">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_pose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="dd/MM/yyyy"/>
            <Option type="QString" name="field_format" value="yyyy-MM-dd HH:mm:ss"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_depose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="yyyy-MM-dd"/>
            <Option type="QString" name="field_format" value="yyyy-MM-dd"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_action">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="desc_action">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="remarque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acteur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mslink">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mapid">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ecp_intervention_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photo">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option type="QString" name="DefaultRoot" value="P:/SIG/documents/photos"/>
            <Option type="int" name="DocumentViewer" value="1"/>
            <Option type="int" name="DocumentViewerHeight" value="0"/>
            <Option type="int" name="DocumentViewerWidth" value="0"/>
            <Option type="bool" name="FileWidget" value="false"/>
            <Option type="bool" name="FileWidgetButton" value="false"/>
            <Option type="QString" name="FileWidgetFilter" value=""/>
            <Option type="Map" name="PropertyCollection">
              <Option type="QString" name="name" value=""/>
              <Option type="invalid" name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
            <Option type="int" name="RelativeStorage" value="1"/>
            <Option type="int" name="StorageMode" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="schema_elec">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option type="QString" name="DefaultRoot" value="P:/SIG/documents/schemas"/>
            <Option type="int" name="DocumentViewer" value="0"/>
            <Option type="int" name="DocumentViewerHeight" value="0"/>
            <Option type="int" name="DocumentViewerWidth" value="0"/>
            <Option type="bool" name="FileWidget" value="true"/>
            <Option type="bool" name="FileWidgetButton" value="false"/>
            <Option type="QString" name="FileWidgetFilter" value=""/>
            <Option type="Map" name="PropertyCollection">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="documentViewerContent">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
            <Option type="int" name="RelativeStorage" value="0"/>
            <Option type="int" name="StorageMode" value="1"/>
            <Option type="bool" name="UseLink" value="true"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="created_at">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="yyyy-MM-dd HH:mm:ss"/>
            <Option type="QString" name="field_format" value="yyyy-MM-dd HH:mm:ss"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="updated_at">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pkid">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intervention">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rot_z">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="uid4">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="fid"/>
    <alias index="1" name="" field="numero"/>
    <alias index="2" name="numéro insee commune" field="insee"/>
    <alias index="3" name="identifiant" field="id_armoire"/>
    <alias index="4" name="Numéro rue" field="numero_voie"/>
    <alias index="5" name="Type (rue, route, chemin.....)" field="type_voie"/>
    <alias index="6" name="Nom de la rue" field="nom_voie"/>
    <alias index="7" name="Etat" field="etat"/>
    <alias index="8" name="Vétusté" field="vetuste"/>
    <alias index="9" name="Fermeture" field="fermeture"/>
    <alias index="10" name="Rattachement au poste de distribution publique" field="enveloppe_poste_amont"/>
    <alias index="11" name="Type de pose de l'enveloppe" field="enveloppe_pose"/>
    <alias index="12" name="Nombre de départ" field="nbre_depart"/>
    <alias index="13" name="" field="cellule"/>
    <alias index="14" name="Date de pose" field="date_pose"/>
    <alias index="15" name="Date de dépose" field="date_depose"/>
    <alias index="16" name="" field="date_action"/>
    <alias index="17" name="" field="desc_action"/>
    <alias index="18" name="Remarque" field="remarque"/>
    <alias index="19" name="chargé d'affaires" field="acteur"/>
    <alias index="20" name="" field="mslink"/>
    <alias index="21" name="" field="mapid"/>
    <alias index="22" name="identifiant interne intervention" field="ecp_intervention_id"/>
    <alias index="23" name="intérieur/extérieur" field="photo"/>
    <alias index="24" name="cliquez sur le lien" field="schema_elec"/>
    <alias index="25" name="" field="created_at"/>
    <alias index="26" name="" field="updated_at"/>
    <alias index="27" name="" field="pkid"/>
    <alias index="28" name="" field="intervention"/>
    <alias index="29" name="" field="rot_z"/>
    <alias index="30" name="" field="uid4"/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="fid"/>
    <policy policy="Duplicate" field="numero"/>
    <policy policy="Duplicate" field="insee"/>
    <policy policy="Duplicate" field="id_armoire"/>
    <policy policy="Duplicate" field="numero_voie"/>
    <policy policy="Duplicate" field="type_voie"/>
    <policy policy="Duplicate" field="nom_voie"/>
    <policy policy="Duplicate" field="etat"/>
    <policy policy="Duplicate" field="vetuste"/>
    <policy policy="Duplicate" field="fermeture"/>
    <policy policy="Duplicate" field="enveloppe_poste_amont"/>
    <policy policy="Duplicate" field="enveloppe_pose"/>
    <policy policy="Duplicate" field="nbre_depart"/>
    <policy policy="Duplicate" field="cellule"/>
    <policy policy="Duplicate" field="date_pose"/>
    <policy policy="Duplicate" field="date_depose"/>
    <policy policy="Duplicate" field="date_action"/>
    <policy policy="Duplicate" field="desc_action"/>
    <policy policy="Duplicate" field="remarque"/>
    <policy policy="Duplicate" field="acteur"/>
    <policy policy="Duplicate" field="mslink"/>
    <policy policy="Duplicate" field="mapid"/>
    <policy policy="Duplicate" field="ecp_intervention_id"/>
    <policy policy="Duplicate" field="photo"/>
    <policy policy="Duplicate" field="schema_elec"/>
    <policy policy="Duplicate" field="created_at"/>
    <policy policy="Duplicate" field="updated_at"/>
    <policy policy="Duplicate" field="pkid"/>
    <policy policy="Duplicate" field="intervention"/>
    <policy policy="Duplicate" field="rot_z"/>
    <policy policy="Duplicate" field="uid4"/>
  </splitPolicies>
  <defaults>
    <default expression="" applyOnUpdate="0" field="fid"/>
    <default expression="" applyOnUpdate="0" field="numero"/>
    <default expression=" @lumigis_insee " applyOnUpdate="1" field="insee"/>
    <default expression="" applyOnUpdate="0" field="id_armoire"/>
    <default expression="" applyOnUpdate="0" field="numero_voie"/>
    <default expression="" applyOnUpdate="0" field="type_voie"/>
    <default expression="" applyOnUpdate="0" field="nom_voie"/>
    <default expression="" applyOnUpdate="0" field="etat"/>
    <default expression="" applyOnUpdate="0" field="vetuste"/>
    <default expression="" applyOnUpdate="0" field="fermeture"/>
    <default expression="" applyOnUpdate="0" field="enveloppe_poste_amont"/>
    <default expression="" applyOnUpdate="0" field="enveloppe_pose"/>
    <default expression="" applyOnUpdate="0" field="nbre_depart"/>
    <default expression="" applyOnUpdate="0" field="cellule"/>
    <default expression="" applyOnUpdate="0" field="date_pose"/>
    <default expression="" applyOnUpdate="0" field="date_depose"/>
    <default expression="" applyOnUpdate="0" field="date_action"/>
    <default expression="" applyOnUpdate="0" field="desc_action"/>
    <default expression="" applyOnUpdate="0" field="remarque"/>
    <default expression=" @lumigis_technicien " applyOnUpdate="1" field="acteur"/>
    <default expression="" applyOnUpdate="0" field="mslink"/>
    <default expression="" applyOnUpdate="0" field="mapid"/>
    <default expression=" &quot;ecp_intervention_id&quot; " applyOnUpdate="1" field="ecp_intervention_id"/>
    <default expression="" applyOnUpdate="0" field="photo"/>
    <default expression="" applyOnUpdate="0" field="schema_elec"/>
    <default expression="" applyOnUpdate="0" field="created_at"/>
    <default expression="" applyOnUpdate="0" field="updated_at"/>
    <default expression="" applyOnUpdate="0" field="pkid"/>
    <default expression=" @lumigis_intervention " applyOnUpdate="1" field="intervention"/>
    <default expression="" applyOnUpdate="0" field="rot_z"/>
    <default expression="" applyOnUpdate="0" field="uid4"/>
  </defaults>
  <constraints>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" unique_strength="1" field="fid"/>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" unique_strength="1" field="numero"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="insee"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="id_armoire"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="numero_voie"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="type_voie"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="nom_voie"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="etat"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="vetuste"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="fermeture"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="enveloppe_poste_amont"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="enveloppe_pose"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="nbre_depart"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="cellule"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_pose"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_depose"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_action"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="desc_action"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="remarque"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="acteur"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="mslink"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="mapid"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="ecp_intervention_id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="photo"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="schema_elec"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="created_at"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="updated_at"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="pkid"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="intervention"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="rot_z"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="uid4"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" exp="" desc=""/>
    <constraint field="numero" exp="" desc=""/>
    <constraint field="insee" exp="" desc=""/>
    <constraint field="id_armoire" exp="" desc=""/>
    <constraint field="numero_voie" exp="" desc=""/>
    <constraint field="type_voie" exp="" desc=""/>
    <constraint field="nom_voie" exp="" desc=""/>
    <constraint field="etat" exp="" desc=""/>
    <constraint field="vetuste" exp="" desc=""/>
    <constraint field="fermeture" exp="" desc=""/>
    <constraint field="enveloppe_poste_amont" exp="" desc=""/>
    <constraint field="enveloppe_pose" exp="" desc=""/>
    <constraint field="nbre_depart" exp="" desc=""/>
    <constraint field="cellule" exp="" desc=""/>
    <constraint field="date_pose" exp="" desc=""/>
    <constraint field="date_depose" exp="" desc=""/>
    <constraint field="date_action" exp="" desc=""/>
    <constraint field="desc_action" exp="" desc=""/>
    <constraint field="remarque" exp="" desc=""/>
    <constraint field="acteur" exp="" desc=""/>
    <constraint field="mslink" exp="" desc=""/>
    <constraint field="mapid" exp="" desc=""/>
    <constraint field="ecp_intervention_id" exp="" desc=""/>
    <constraint field="photo" exp="" desc=""/>
    <constraint field="schema_elec" exp="" desc=""/>
    <constraint field="created_at" exp="" desc=""/>
    <constraint field="updated_at" exp="" desc=""/>
    <constraint field="pkid" exp="" desc=""/>
    <constraint field="intervention" exp="" desc=""/>
    <constraint field="rot_z" exp="" desc=""/>
    <constraint field="uid4" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{5c55e0f0-75c5-4988-87e5-e7f75979c44a}" key="Canvas"/>
    <actionsetting action="[%schema_elec%]" type="5" id="{82c346aa-d346-4561-968f-431458228657}" name="visualisation schéma" notificationMessage="" shortTitle="" icon="" isEnabledOnlyWhenEditable="0" capture="0">
      <actionScope id="Layer"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting action="[%photo%]" type="5" id="{5c55e0f0-75c5-4988-87e5-e7f75979c44a}" name="visualisaton photos extérieur/intérieur" notificationMessage="" shortTitle="" icon="" isEnabledOnlyWhenEditable="0" capture="0">
      <actionScope id="Layer"/>
      <actionScope id="Feature"/>
      <actionScope id="Canvas"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id_armoire&quot;" sortOrder="0" actionWidgetStyle="buttonList">
    <columns>
      <column type="field" name="intervention" width="-1" hidden="0"/>
      <column type="field" name="acteur" width="100" hidden="0"/>
      <column type="field" name="insee" width="-1" hidden="0"/>
      <column type="field" name="id_armoire" width="-1" hidden="0"/>
      <column type="field" name="numero_voie" width="-1" hidden="0"/>
      <column type="field" name="type_voie" width="-1" hidden="0"/>
      <column type="field" name="nom_voie" width="-1" hidden="0"/>
      <column type="field" name="etat" width="100" hidden="0"/>
      <column type="field" name="vetuste" width="100" hidden="0"/>
      <column type="field" name="fermeture" width="-1" hidden="0"/>
      <column type="field" name="enveloppe_poste_amont" width="252" hidden="0"/>
      <column type="field" name="enveloppe_pose" width="154" hidden="0"/>
      <column type="field" name="nbre_depart" width="-1" hidden="0"/>
      <column type="field" name="cellule" width="-1" hidden="0"/>
      <column type="field" name="date_pose" width="100" hidden="0"/>
      <column type="field" name="date_depose" width="100" hidden="0"/>
      <column type="field" name="mslink" width="-1" hidden="1"/>
      <column type="field" name="mapid" width="-1" hidden="1"/>
      <column type="field" name="ecp_intervention_id" width="143" hidden="1"/>
      <column type="field" name="photo" width="382" hidden="0"/>
      <column type="field" name="schema_elec" width="361" hidden="0"/>
      <column type="field" name="created_at" width="-1" hidden="1"/>
      <column type="field" name="updated_at" width="-1" hidden="1"/>
      <column type="field" name="remarque" width="-1" hidden="0"/>
      <column type="field" name="rot_z" width="-1" hidden="1"/>
      <column type="actions" width="-1" hidden="0"/>
      <column type="field" name="date_action" width="-1" hidden="0"/>
      <column type="field" name="desc_action" width="-1" hidden="0"/>
      <column type="field" name="uid4" width="-1" hidden="0"/>
      <column type="field" name="numero" width="-1" hidden="0"/>
      <column type="field" name="fid" width="-1" hidden="0"/>
      <column type="field" name="pkid" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">P:/SIG/gestion</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>X:/AGENTS/Jean-Claude/transfert_shape/201607014pairetgrandrupt/QGIS</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
      <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
    </labelStyle>
    <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Fiche descriptive" visibilityExpression="" groupBox="0">
      <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
        <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      </labelStyle>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Généralités" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="28" showLabel="1" name="intervention">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="19" showLabel="1" name="acteur">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="2" showLabel="1" name="insee">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Caractéristiques techniques" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Situation" visibilityExpression="" groupBox="1">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
          <attributeEditorField index="5" showLabel="1" name="type_voie">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField index="6" showLabel="1" name="nom_voie">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField index="4" showLabel="1" name="numero_voie">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
        </attributeEditorContainer>
        <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="identification" visibilityExpression="" groupBox="1">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
          <attributeEditorField index="3" showLabel="1" name="id_armoire">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField index="7" showLabel="1" name="etat">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField index="8" showLabel="1" name="vetuste">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField index="11" showLabel="1" name="enveloppe_pose">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField index="9" showLabel="1" name="fermeture">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField index="14" showLabel="1" name="date_pose">
            <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
              <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
            </labelStyle>
          </attributeEditorField>
        </attributeEditorContainer>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" visibilityExpressionEnabled="1" collapsed="0" columnCount="1" name="Remarques" visibilityExpression=" &quot;remarque&quot; &lt;> ''" groupBox="0">
      <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
        <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      </labelStyle>
      <attributeEditorField index="18" showLabel="1" name="remarque">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="acteur"/>
    <field editable="1" name="cellule"/>
    <field editable="1" name="created_at"/>
    <field editable="1" name="date_action"/>
    <field editable="1" name="date_depose"/>
    <field editable="1" name="date_maintenance"/>
    <field editable="1" name="date_pose"/>
    <field editable="0" name="departs_allumage"/>
    <field editable="0" name="departs_calibrage_protection"/>
    <field editable="0" name="departs_designation"/>
    <field editable="0" name="departs_heure_allumage"/>
    <field editable="0" name="departs_heure_extinction"/>
    <field editable="0" name="departs_id"/>
    <field editable="0" name="departs_id_armoire"/>
    <field editable="0" name="departs_id_depart"/>
    <field editable="0" name="departs_insee"/>
    <field editable="0" name="departs_nature_depart"/>
    <field editable="0" name="departs_nature_protection"/>
    <field editable="0" name="departs_pilote"/>
    <field editable="0" name="departs_remarque"/>
    <field editable="0" name="departs_sensibilite_diff"/>
    <field editable="1" name="desc_action"/>
    <field editable="1" name="desc_maintenance"/>
    <field editable="1" name="ecp_intervention_id"/>
    <field editable="1" name="enveloppe_pose"/>
    <field editable="1" name="enveloppe_poste_amont"/>
    <field editable="1" name="etat"/>
    <field editable="1" name="fermeture"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_armoire"/>
    <field editable="1" name="insee"/>
    <field editable="1" name="intervention"/>
    <field editable="1" name="mapid"/>
    <field editable="1" name="mslink"/>
    <field editable="1" name="nbre_depart"/>
    <field editable="1" name="nom_voie"/>
    <field editable="1" name="numero"/>
    <field editable="1" name="numero_voie"/>
    <field editable="0" name="photo"/>
    <field editable="1" name="pkid"/>
    <field editable="1" name="remarque"/>
    <field editable="1" name="rot_z"/>
    <field editable="0" name="schema_elec"/>
    <field editable="1" name="type_voie"/>
    <field editable="1" name="uid4"/>
    <field editable="1" name="updated_at"/>
    <field editable="1" name="vetuste"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="1" name="acteur"/>
    <field labelOnTop="0" name="cellule"/>
    <field labelOnTop="0" name="created_at"/>
    <field labelOnTop="0" name="date_action"/>
    <field labelOnTop="1" name="date_depose"/>
    <field labelOnTop="0" name="date_maintenance"/>
    <field labelOnTop="1" name="date_pose"/>
    <field labelOnTop="0" name="departs_allumage"/>
    <field labelOnTop="0" name="departs_calibrage_protection"/>
    <field labelOnTop="0" name="departs_designation"/>
    <field labelOnTop="0" name="departs_heure_allumage"/>
    <field labelOnTop="0" name="departs_heure_extinction"/>
    <field labelOnTop="0" name="departs_id"/>
    <field labelOnTop="0" name="departs_id_armoire"/>
    <field labelOnTop="0" name="departs_id_depart"/>
    <field labelOnTop="0" name="departs_insee"/>
    <field labelOnTop="0" name="departs_nature_depart"/>
    <field labelOnTop="0" name="departs_nature_protection"/>
    <field labelOnTop="0" name="departs_pilote"/>
    <field labelOnTop="0" name="departs_remarque"/>
    <field labelOnTop="0" name="departs_sensibilite_diff"/>
    <field labelOnTop="0" name="desc_action"/>
    <field labelOnTop="0" name="desc_maintenance"/>
    <field labelOnTop="0" name="ecp_intervention_id"/>
    <field labelOnTop="1" name="enveloppe_pose"/>
    <field labelOnTop="1" name="enveloppe_poste_amont"/>
    <field labelOnTop="1" name="etat"/>
    <field labelOnTop="1" name="fermeture"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="1" name="id_armoire"/>
    <field labelOnTop="1" name="insee"/>
    <field labelOnTop="1" name="intervention"/>
    <field labelOnTop="0" name="mapid"/>
    <field labelOnTop="0" name="mslink"/>
    <field labelOnTop="1" name="nbre_depart"/>
    <field labelOnTop="1" name="nom_voie"/>
    <field labelOnTop="0" name="numero"/>
    <field labelOnTop="1" name="numero_voie"/>
    <field labelOnTop="0" name="photo"/>
    <field labelOnTop="0" name="pkid"/>
    <field labelOnTop="1" name="remarque"/>
    <field labelOnTop="0" name="rot_z"/>
    <field labelOnTop="0" name="schema_elec"/>
    <field labelOnTop="1" name="type_voie"/>
    <field labelOnTop="0" name="uid4"/>
    <field labelOnTop="0" name="updated_at"/>
    <field labelOnTop="1" name="vetuste"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="acteur" reuseLastValue="0"/>
    <field name="cellule" reuseLastValue="0"/>
    <field name="created_at" reuseLastValue="0"/>
    <field name="date_action" reuseLastValue="0"/>
    <field name="date_depose" reuseLastValue="0"/>
    <field name="date_pose" reuseLastValue="0"/>
    <field name="desc_action" reuseLastValue="0"/>
    <field name="ecp_intervention_id" reuseLastValue="0"/>
    <field name="enveloppe_pose" reuseLastValue="0"/>
    <field name="enveloppe_poste_amont" reuseLastValue="0"/>
    <field name="etat" reuseLastValue="0"/>
    <field name="fermeture" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="id_armoire" reuseLastValue="0"/>
    <field name="insee" reuseLastValue="0"/>
    <field name="intervention" reuseLastValue="0"/>
    <field name="mapid" reuseLastValue="0"/>
    <field name="mslink" reuseLastValue="0"/>
    <field name="nbre_depart" reuseLastValue="0"/>
    <field name="nom_voie" reuseLastValue="0"/>
    <field name="numero" reuseLastValue="0"/>
    <field name="numero_voie" reuseLastValue="0"/>
    <field name="photo" reuseLastValue="0"/>
    <field name="pkid" reuseLastValue="0"/>
    <field name="remarque" reuseLastValue="0"/>
    <field name="rot_z" reuseLastValue="0"/>
    <field name="schema_elec" reuseLastValue="0"/>
    <field name="type_voie" reuseLastValue="0"/>
    <field name="uid4" reuseLastValue="0"/>
    <field name="updated_at" reuseLastValue="0"/>
    <field name="vetuste" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="intervention">
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option type="Map" name="properties">
          <Option type="Map" name="dataDefinedAlias">
            <Option type="bool" name="active" value="false"/>
            <Option type="int" name="type" value="1"/>
            <Option type="QString" name="val" value=""/>
          </Option>
        </Option>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </field>
    <field name="schema_elec">
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option type="Map" name="properties">
          <Option type="Map" name="dataDefinedAlias">
            <Option type="bool" name="active" value="false"/>
            <Option type="int" name="type" value="1"/>
            <Option type="QString" name="val" value=""/>
          </Option>
        </Option>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>COALESCE( "id_armoire", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
