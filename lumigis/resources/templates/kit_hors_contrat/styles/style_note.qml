<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" maxScale="0" symbologyReferenceScale="200" simplifyLocal="1" simplifyAlgorithm="0" simplifyDrawingTol="1" styleCategories="AllStyleCategories" minScale="100000000" version="3.28.5-Firenze" labelsEnabled="1" simplifyDrawingHints="0" readOnly="0" simplifyMaxScale="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startExpression="" limitMode="0" durationField="fid" fixedDuration="0" mode="0" durationUnit="min" accumulate="0" endExpression="" enabled="0" endField="" startField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation extrusion="0" zoffset="0" type="IndividualFeatures" zscale="1" symbology="Line" clamping="Terrain" binding="Centroid" showMarkerSymbolInSurfacePlots="0" extrusionEnabled="0" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol is_animated="0" name="" type="line" clip_to_extent="1" force_rhr="0" frame_rate="10" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="229,182,54,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.6"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol is_animated="0" name="" type="fill" clip_to_extent="1" force_rhr="0" frame_rate="10" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" locked="0" pass="0" enabled="1">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="229,182,54,255"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="164,130,39,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol is_animated="0" name="" type="marker" clip_to_extent="1" force_rhr="0" frame_rate="10" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" locked="0" pass="0" enabled="1">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="229,182,54,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="diamond"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="164,130,39,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="200" symbollevels="0" type="singleSymbol" forceraster="0" enableorderby="0">
    <symbols>
      <symbol is_animated="0" name="0" type="marker" clip_to_extent="1" force_rhr="0" frame_rate="10" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" locked="0" pass="0" enabled="1">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="51,160,44,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="51,160,44,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="balloon">
      <text-style forcedItalic="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontLetterSpacing="0" textColor="51,160,44,255" capitalization="0" fontKerning="1" fontStrikeout="0" fontFamily="Arial" forcedBold="0" blendMode="0" textOpacity="1" fieldName="manuelle" fontWeight="75" fontSize="4" multilineHeight="1" textOrientation="horizontal" fontSizeUnit="MM" fontWordSpacing="0" legendString="Aa" previewBkgrdColor="255,255,255,255" multilineHeightUnit="Percentage" useSubstitutions="0" fontItalic="1" isExpression="0" fontUnderline="0" namedStyle="Bold Italic" allowHtml="0">
        <families/>
        <text-buffer bufferDraw="0" bufferJoinStyle="128" bufferNoFill="0" bufferSizeUnits="MM" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSize="0" bufferColor="255,255,255,255" bufferBlendMode="0" bufferOpacity="1"/>
        <text-mask maskType="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskOpacity="1" maskJoinStyle="128" maskSize="0" maskSizeUnits="MM" maskEnabled="0" maskedSymbolLayers=""/>
        <background shapeRadiiX="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeFillColor="255,255,255,255" shapeSizeType="0" shapeOffsetUnit="Point" shapeSVGFile="" shapeOpacity="1" shapeType="0" shapeOffsetX="0" shapeRadiiUnit="Point" shapeBlendMode="0" shapeJoinStyle="64" shapeBorderWidthUnit="Point" shapeDraw="0" shapeRotation="0" shapeRadiiY="0" shapeSizeUnit="Point" shapeBorderColor="128,128,128,255" shapeOffsetY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidth="0">
          <symbol is_animated="0" name="markerSymbol" type="marker" clip_to_extent="1" force_rhr="0" frame_rate="10" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" locked="0" pass="0" enabled="1">
              <Option type="Map">
                <Option name="angle" type="QString" value="0"/>
                <Option name="cap_style" type="QString" value="square"/>
                <Option name="color" type="QString" value="152,125,183,255"/>
                <Option name="horizontal_anchor_point" type="QString" value="1"/>
                <Option name="joinstyle" type="QString" value="bevel"/>
                <Option name="name" type="QString" value="circle"/>
                <Option name="offset" type="QString" value="0,0"/>
                <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="offset_unit" type="QString" value="MM"/>
                <Option name="outline_color" type="QString" value="35,35,35,255"/>
                <Option name="outline_style" type="QString" value="solid"/>
                <Option name="outline_width" type="QString" value="0"/>
                <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="outline_width_unit" type="QString" value="MM"/>
                <Option name="scale_method" type="QString" value="diameter"/>
                <Option name="size" type="QString" value="2"/>
                <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="size_unit" type="QString" value="MM"/>
                <Option name="vertical_anchor_point" type="QString" value="1"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol is_animated="0" name="fillSymbol" type="fill" clip_to_extent="1" force_rhr="0" frame_rate="10" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleFill" locked="0" pass="0" enabled="1">
              <Option type="Map">
                <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="color" type="QString" value="255,255,255,255"/>
                <Option name="joinstyle" type="QString" value="bevel"/>
                <Option name="offset" type="QString" value="0,0"/>
                <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="offset_unit" type="QString" value="MM"/>
                <Option name="outline_color" type="QString" value="128,128,128,255"/>
                <Option name="outline_style" type="QString" value="no"/>
                <Option name="outline_width" type="QString" value="0"/>
                <Option name="outline_width_unit" type="QString" value="Point"/>
                <Option name="style" type="QString" value="solid"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOpacity="1" shadowUnder="0" shadowOffsetUnit="Point" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowOffsetGlobal="1" shadowRadiusUnit="Point" shadowRadiusAlphaOnly="0" shadowScale="100" shadowRadius="1.5" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowOffsetAngle="135" shadowColor="0,0,0,255" shadowOffsetDist="1"/>
        <dd_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format plussign="0" decimals="3" rightDirectionSymbol=">" placeDirectionSymbol="0" autoWrapLength="0" addDirectionSymbol="0" formatNumbers="0" multilineAlign="3" leftDirectionSymbol="&lt;" useMaxLineLengthForAutoWrap="1" reverseDirectionSymbol="0" wrapChar=""/>
      <placement dist="15" repeatDistanceUnits="MM" placement="6" repeatDistance="0" placementFlags="10" allowDegraded="0" geometryGeneratorEnabled="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" lineAnchorTextPoint="CenterOfText" rotationUnit="AngleDegrees" yOffset="0" offsetUnits="MM" preserveRotation="1" overrunDistance="0" layerType="PointGeometry" quadOffset="0" distMapUnitScale="3x:0,0,0,0,0,0" offsetType="1" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" priority="5" lineAnchorType="0" maxCurvedCharAngleIn="25" geometryGenerator="" maxCurvedCharAngleOut="-25" centroidWhole="0" rotationAngle="0" centroidInside="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" xOffset="1" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorClipping="0" overrunDistanceUnit="MM" geometryGeneratorType="PointGeometry" fitInPolygonOnly="0" polygonPlacementFlags="2" distUnits="MM" lineAnchorPercent="0.5"/>
      <rendering obstacleType="1" zIndex="0" obstacleFactor="1" minFeatureSize="0" limitNumLabels="0" mergeLines="0" scaleMin="0" scaleVisibility="0" fontMinPixelSize="3" unplacedVisibility="0" maxNumLabels="2000" scaleMax="0" fontMaxPixelSize="10000" obstacle="1" fontLimitPixelSize="0" drawLabels="1" labelPerPart="0" upsidedownLabels="0"/>
      <dd_properties>
        <Option type="Map">
          <Option name="name" type="QString" value=""/>
          <Option name="properties" type="Map">
            <Option name="PositionX" type="Map">
              <Option name="active" type="bool" value="false"/>
              <Option name="type" type="int" value="1"/>
              <Option name="val" type="QString" value=""/>
            </Option>
            <Option name="PositionY" type="Map">
              <Option name="active" type="bool" value="false"/>
              <Option name="type" type="int" value="1"/>
              <Option name="val" type="QString" value=""/>
            </Option>
          </Option>
          <Option name="type" type="QString" value="collection"/>
        </Option>
      </dd_properties>
      <callout type="balloon">
        <Option type="Map">
          <Option name="anchorPoint" type="QString" value="pole_of_inaccessibility"/>
          <Option name="blendMode" type="int" value="0"/>
          <Option name="cornerRadius" type="double" value="0.5"/>
          <Option name="cornerRadiusMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="cornerRadiusUnit" type="QString" value="MM"/>
          <Option name="ddProperties" type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties" type="Map">
              <Option name="OriginX" type="Map">
                <Option name="active" type="bool" value="false"/>
                <Option name="type" type="int" value="1"/>
                <Option name="val" type="QString" value=""/>
              </Option>
              <Option name="OriginY" type="Map">
                <Option name="active" type="bool" value="false"/>
                <Option name="type" type="int" value="1"/>
                <Option name="val" type="QString" value=""/>
              </Option>
            </Option>
            <Option name="type" type="QString" value="collection"/>
          </Option>
          <Option name="enabled" type="QString" value="1"/>
          <Option name="fillSymbol" type="QString" value="&lt;symbol is_animated=&quot;0&quot; name=&quot;symbol&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;SimpleFill&quot; locked=&quot;0&quot; pass=&quot;0&quot; enabled=&quot;1&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;border_width_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;color&quot; type=&quot;QString&quot; value=&quot;204,204,204,255&quot;/>&lt;Option name=&quot;joinstyle&quot; type=&quot;QString&quot; value=&quot;bevel&quot;/>&lt;Option name=&quot;offset&quot; type=&quot;QString&quot; value=&quot;0,0&quot;/>&lt;Option name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;offset_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;outline_color&quot; type=&quot;QString&quot; value=&quot;51,160,44,255&quot;/>&lt;Option name=&quot;outline_style&quot; type=&quot;QString&quot; value=&quot;solid&quot;/>&lt;Option name=&quot;outline_width&quot; type=&quot;QString&quot; value=&quot;0.26&quot;/>&lt;Option name=&quot;outline_width_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;style&quot; type=&quot;QString&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
          <Option name="labelAnchorPoint" type="QString" value="point_on_exterior"/>
          <Option name="margins" type="QString" value="1,1,1,1"/>
          <Option name="marginsUnit" type="QString" value="MM"/>
          <Option name="offsetFromAnchor" type="double" value="0"/>
          <Option name="offsetFromAnchorMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offsetFromAnchorUnit" type="QString" value="MM"/>
          <Option name="wedgeWidth" type="double" value="1"/>
          <Option name="wedgeWidthMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="wedgeWidthUnit" type="QString" value="MM"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option name="dualview/previewExpressions" type="List">
        <Option type="QString" value="&quot;date_creat&quot;"/>
      </Option>
      <Option name="embeddedWidgets/count" type="QString" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory direction="1" backgroundAlpha="255" lineSizeType="MM" sizeType="MM" penAlpha="255" minScaleDenominator="0" showAxis="0" width="15" opacity="1" spacing="0" minimumSize="0" penColor="#000000" height="15" lineSizeScale="3x:0,0,0,0,0,0" maxScaleDenominator="1e+08" sizeScale="3x:0,0,0,0,0,0" spacingUnit="MM" labelPlacementMethod="XHeight" diagramOrientation="Up" enabled="0" scaleBasedVisibility="0" spacingUnitScale="3x:0,0,0,0,0,0" penWidth="0" backgroundColor="#ffffff" scaleDependency="Area" barWidth="5" rotationOffset="270">
      <fontProperties strikethrough="0" bold="0" underline="0" style="" description="Roboto,11,-1,5,25,0,0,0,0,0" italic="0"/>
      <attribute colorOpacity="1" label="" color="#000000" field=""/>
      <axisSymbol>
        <symbol is_animated="0" name="" type="line" clip_to_extent="1" force_rhr="0" frame_rate="10" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" locked="0" pass="0" enabled="1">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" obstacle="0" dist="0" linePlacementFlags="18" showAll="1" placement="0" zIndex="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="date_creat" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option name="allow_null" type="bool" value="true"/>
            <Option name="calendar_popup" type="bool" value="true"/>
            <Option name="display_format" type="QString" value="dd-MM-yyyy"/>
            <Option name="field_format" type="QString" value="dd-MM-yyyy"/>
            <Option name="field_iso_format" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="manuelle" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="coord x" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="coord y" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="false"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="fid" index="0"/>
    <alias name="date de création" field="date_creat" index="1"/>
    <alias name="entrer votre texte" field="manuelle" index="2"/>
    <alias name="" field="coord x" index="3"/>
    <alias name="" field="coord y" index="4"/>
  </aliases>
  <defaults>
    <default expression="" field="fid" applyOnUpdate="0"/>
    <default expression=" now() " field="date_creat" applyOnUpdate="0"/>
    <default expression="" field="manuelle" applyOnUpdate="0"/>
    <default expression="" field="coord x" applyOnUpdate="0"/>
    <default expression="" field="coord y" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" constraints="3" exp_strength="0" notnull_strength="1" field="fid"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="date_creat"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="manuelle"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="coord x"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" notnull_strength="0" field="coord y"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="fid"/>
    <constraint desc="" exp="" field="date_creat"/>
    <constraint desc="" exp="" field="manuelle"/>
    <constraint desc="" exp="" field="coord x"/>
    <constraint desc="" exp="" field="coord y"/>
  </constraintExpressions>
  <expressionfields>
    <field comment="" expression="$x" name="coord x" length="0" type="2" subType="0" precision="0" typeName="integer"/>
    <field comment="" expression="$y" name="coord y" length="0" type="2" subType="0" precision="0" typeName="integer"/>
  </expressionfields>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="&quot;coord x&quot;">
    <columns>
      <column width="-1" type="actions" hidden="1"/>
      <column name="coord x" width="-1" type="field" hidden="0"/>
      <column name="coord y" width="-1" type="field" hidden="0"/>
      <column name="fid" width="-1" type="field" hidden="0"/>
      <column name="date_creat" width="-1" type="field" hidden="0"/>
      <column name="manuelle" width="355" type="field" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelFont="0" labelColor="0,0,0,255" overrideLabelColor="0">
      <labelFont strikethrough="0" bold="0" underline="0" style="" description="MS Shell Dlg 2,8,-1,5,50,0,0,0,0,0" italic="0"/>
    </labelStyle>
    <attributeEditorContainer columnCount="1" name="ajouter une note" collapsedExpressionEnabled="0" showLabel="1" visibilityExpression="" groupBox="0" collapsed="0" collapsedExpression="" visibilityExpressionEnabled="0" backgroundColor="#cccccc">
      <labelStyle overrideLabelFont="0" labelColor="0,0,0,255" overrideLabelColor="0">
        <labelFont strikethrough="0" bold="0" underline="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      </labelStyle>
      <attributeEditorField name="manuelle" showLabel="1" index="2">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255" overrideLabelColor="0">
          <labelFont strikethrough="0" bold="0" underline="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="date_creat" showLabel="1" index="1">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255" overrideLabelColor="0">
          <labelFont strikethrough="0" bold="0" underline="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="date_modif" showLabel="1" index="-1">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255" overrideLabelColor="0">
          <labelFont strikethrough="0" bold="0" underline="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="PI" editable="1"/>
    <field name="PI2" editable="1"/>
    <field name="armoire" editable="1"/>
    <field name="coord x" editable="0"/>
    <field name="coord y" editable="0"/>
    <field name="câble" editable="1"/>
    <field name="date_creat" editable="1"/>
    <field name="date_creation" editable="0"/>
    <field name="date_modif" editable="1"/>
    <field name="date_modification" editable="0"/>
    <field name="essai" editable="1"/>
    <field name="etat" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="foyer" editable="1"/>
    <field name="id_note" editable="0"/>
    <field name="identifiant A....." editable="1"/>
    <field name="identifiant F....." editable="1"/>
    <field name="identifiant S......" editable="1"/>
    <field name="important" editable="1"/>
    <field name="manuelle" editable="1"/>
    <field name="predef" editable="1"/>
    <field name="rot_label" editable="0"/>
    <field name="support" editable="1"/>
    <field name="terre" editable="1"/>
    <field name="text_PI" editable="1"/>
    <field name="texte" editable="1"/>
    <field name="txt_PI" editable="1"/>
    <field name="txt_armoir" editable="1"/>
    <field name="txt_cable" editable="1"/>
    <field name="txt_foyer" editable="1"/>
    <field name="txt_supp" editable="1"/>
    <field name="txt_terre" editable="1"/>
    <field name="type" editable="1"/>
    <field name="user_creat" editable="1"/>
    <field name="user_creation" editable="0"/>
    <field name="user_modif" editable="1"/>
    <field name="user_modification" editable="0"/>
    <field name="x_label" editable="0"/>
    <field name="y_label" editable="0"/>
  </editable>
  <labelOnTop>
    <field name="PI" labelOnTop="1"/>
    <field name="PI2" labelOnTop="0"/>
    <field name="armoire" labelOnTop="0"/>
    <field name="coord x" labelOnTop="0"/>
    <field name="coord y" labelOnTop="0"/>
    <field name="câble" labelOnTop="0"/>
    <field name="date_creat" labelOnTop="1"/>
    <field name="date_creation" labelOnTop="0"/>
    <field name="date_modif" labelOnTop="1"/>
    <field name="date_modification" labelOnTop="0"/>
    <field name="essai" labelOnTop="0"/>
    <field name="etat" labelOnTop="1"/>
    <field name="fid" labelOnTop="0"/>
    <field name="foyer" labelOnTop="0"/>
    <field name="id_note" labelOnTop="0"/>
    <field name="identifiant A....." labelOnTop="0"/>
    <field name="identifiant F....." labelOnTop="0"/>
    <field name="identifiant S......" labelOnTop="0"/>
    <field name="important" labelOnTop="0"/>
    <field name="manuelle" labelOnTop="1"/>
    <field name="predef" labelOnTop="1"/>
    <field name="rot_label" labelOnTop="0"/>
    <field name="support" labelOnTop="0"/>
    <field name="terre" labelOnTop="1"/>
    <field name="text_PI" labelOnTop="1"/>
    <field name="texte" labelOnTop="0"/>
    <field name="txt_PI" labelOnTop="1"/>
    <field name="txt_armoir" labelOnTop="0"/>
    <field name="txt_cable" labelOnTop="0"/>
    <field name="txt_foyer" labelOnTop="0"/>
    <field name="txt_supp" labelOnTop="0"/>
    <field name="txt_terre" labelOnTop="1"/>
    <field name="type" labelOnTop="1"/>
    <field name="user_creat" labelOnTop="0"/>
    <field name="user_creation" labelOnTop="0"/>
    <field name="user_modif" labelOnTop="0"/>
    <field name="user_modification" labelOnTop="0"/>
    <field name="x_label" labelOnTop="0"/>
    <field name="y_label" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="PI" reuseLastValue="0"/>
    <field name="coord x" reuseLastValue="0"/>
    <field name="coord y" reuseLastValue="0"/>
    <field name="câble" reuseLastValue="0"/>
    <field name="date_creat" reuseLastValue="0"/>
    <field name="date_modif" reuseLastValue="0"/>
    <field name="essai" reuseLastValue="0"/>
    <field name="etat" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="id_note" reuseLastValue="0"/>
    <field name="manuelle" reuseLastValue="0"/>
    <field name="predef" reuseLastValue="0"/>
    <field name="terre" reuseLastValue="0"/>
    <field name="text_PI" reuseLastValue="0"/>
    <field name="txt_armoir" reuseLastValue="0"/>
    <field name="txt_cable" reuseLastValue="0"/>
    <field name="txt_foyer" reuseLastValue="0"/>
    <field name="txt_supp" reuseLastValue="0"/>
    <field name="txt_terre" reuseLastValue="0"/>
    <field name="type" reuseLastValue="0"/>
    <field name="user_creat" reuseLastValue="0"/>
    <field name="user_modif" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="coord y">
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties" type="Map">
          <Option name="dataDefinedAlias" type="Map">
            <Option name="active" type="bool" value="false"/>
            <Option name="expression" type="QString" value="@form_mode"/>
            <Option name="type" type="int" value="3"/>
          </Option>
        </Option>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>"date_creat"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
