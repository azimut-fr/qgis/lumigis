<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" simplifyLocal="1" labelsEnabled="1" readOnly="0" hasScaleBasedVisibilityFlag="0" symbologyReferenceScale="200" version="3.30.1-'s-Hertogenbosch" maxScale="1" simplifyDrawingHints="0" simplifyAlgorithm="0" simplifyMaxScale="1" minScale="2500" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" durationField="fid" startExpression="" durationUnit="min" startField="date_pose" accumulate="0" enabled="0" mode="0" fixedDuration="0" limitMode="0" endField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation showMarkerSymbolInSurfacePlots="0" type="IndividualFeatures" extrusionEnabled="0" extrusion="0" respectLayerSymbol="1" clamping="Terrain" zoffset="0" binding="Centroid" symbology="Line" zscale="1">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" is_animated="0" type="line" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{13dce582-f415-4870-b1f3-4d2c48ecd369}" enabled="1" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="square"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="125,139,143,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.6"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{ceb130cd-5ef2-4aeb-a6a9-4f9d97ad9256}" enabled="1" locked="0" class="SimpleFill">
          <Option type="Map">
            <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="color" value="125,139,143,255"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="89,99,102,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="style" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{852abf01-e061-4266-a294-1e2b13b96f15}" enabled="1" locked="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="125,139,143,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="diamond"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="89,99,102,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="3"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="200" type="RuleRenderer" symbollevels="0" forceraster="0" enableorderby="0">
    <rules key="{32f5279c-1000-4b8e-af20-e49e291fdfd7}">
      <rule label="Actif" filter="&quot;etat&quot; =  'Hors contrat' " key="{16dd0af1-73d5-4cf1-9ec0-a2837f04135c}">
        <rule symbol="0" label="candélabre" filter="&quot;cellule&quot; ='FOYER MAT'" key="{829e0bbb-fcaa-4fbb-9c1b-f726264fe091}"/>
        <rule symbol="1" label="lanterne poteau, façade" description="Foyer imple" filter=" &quot;cellule&quot; = 'FOYER SIMPLE'" key="{02aba51b-f029-490a-8e61-f6559c301a96}"/>
        <rule symbol="2" label="projecteur" filter=" &quot;cellule&quot; = 'PROJECTEUR'" key="{8392ffdc-cc56-40cb-ab2f-4a0b1efb3642}"/>
      </rule>
    </rules>
    <symbols>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="0" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{41fb3e38-9c08-4f03-af51-34aef657f2e1}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="251,154,153,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAxMzAwIDEzMDAiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1mb3llcl9tYXQuc3ZnIgogICB3aWR0aD0iMTMwY20iCiAgIGhlaWdodD0iMTMwY20iCiAgIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXciPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE2Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MTY5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNTE2NyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJUYWlsIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iVGFpbCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDg2NyIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgtMS4yKSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg1NSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsLTMuOTU4NTIyNyAwLjU0MzUyMDk0LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTciCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLC0zLjk1ODUyMjcgMy4wNjE3MDUzLDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTkiCiAgICAgICAgICAgZD0iTSAxLjMwNTM1ODIsLTMuOTU4NTIyNyA1LjY1Mzc0NjYsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsNC4xNzc1ODM4IDAuNTQzNTIwOTQsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MyIKICAgICAgICAgICBkPSJNIC0xLjI4NjY4MzIsNC4xNzc1ODM4IDMuMDYxNzA1MywwLjIxOTc0MjI2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODAwMDAwMDE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0ODY1IgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLDQuMTc3NTgzOCA1LjY1Mzc0NjYsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4MTUiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODEzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDgwOSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4MDciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTEzIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTIxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTA4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjAuNzY0MTc5MTE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTAwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5MiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwOTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDg2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDc7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODAiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OC0yLTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg0NzIiPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICAgIG1vZGU9InNjcmVlbiIKICAgICAgICAgaW4yPSJCYWNrZ3JvdW5kSW1hZ2UiCiAgICAgICAgIGlkPSJmZUJsZW5kODQ3NCIgLz4KICAgIDwvZmlsdGVyPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJJbiBhbmQgb3V0IGdsb3cgd2l0aCBhIHBvc3NpYmxlIG9mZnNldCBhbmQgY29sb3JpemFibGUgZmxvb2QiCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJDdXRvdXQgR2xvdyIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlkPSJmaWx0ZXI4NDc5Ij4KICAgICAgPGZlT2Zmc2V0CiAgICAgICAgIGR5PSIzIgogICAgICAgICBkeD0iMyIKICAgICAgICAgaWQ9ImZlT2Zmc2V0ODQ4MSIgLz4KICAgICAgPGZlR2F1c3NpYW5CbHVyCiAgICAgICAgIHN0ZERldmlhdGlvbj0iMyIKICAgICAgICAgcmVzdWx0PSJibHVyIgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NDgzIiAvPgogICAgICA8ZmVGbG9vZAogICAgICAgICBmbG9vZC1jb2xvcj0icmdiKDAsMCwwKSIKICAgICAgICAgZmxvb2Qtb3BhY2l0eT0iMSIKICAgICAgICAgcmVzdWx0PSJmbG9vZCIKICAgICAgICAgaWQ9ImZlRmxvb2Q4NDg1IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49ImZsb29kIgogICAgICAgICBpbjI9IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIG9wZXJhdG9yPSJpbiIKICAgICAgICAgcmVzdWx0PSJjb21wb3NpdGUiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg0ODciIC8+CiAgICAgIDxmZUJsZW5kCiAgICAgICAgIGluPSJibHVyIgogICAgICAgICBpbjI9ImNvbXBvc2l0ZSIKICAgICAgICAgbW9kZT0ibm9ybWFsIgogICAgICAgICBpZD0iZmVCbGVuZDg0ODkiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIGlua3NjYXBlOm1lbnUtdG9vbHRpcD0iSW4gYW5kIG91dCBnbG93IHdpdGggYSBwb3NzaWJsZSBvZmZzZXQgYW5kIGNvbG9yaXphYmxlIGZsb29kIgogICAgICAgaW5rc2NhcGU6bWVudT0iU2hhZG93cyBhbmQgR2xvd3MiCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iQ3V0b3V0IEdsb3ciCiAgICAgICBzdHlsZT0iY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOnNSR0IiCiAgICAgICBpZD0iZmlsdGVyODQ5MSI+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeT0iMyIKICAgICAgICAgZHg9IjMiCiAgICAgICAgIGlkPSJmZU9mZnNldDg0OTMiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBzdGREZXZpYXRpb249IjMiCiAgICAgICAgIHJlc3VsdD0iYmx1ciIKICAgICAgICAgaWQ9ImZlR2F1c3NpYW5CbHVyODQ5NSIgLz4KICAgICAgPGZlRmxvb2QKICAgICAgICAgZmxvb2QtY29sb3I9InJnYigwLDAsMCkiCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjEiCiAgICAgICAgIHJlc3VsdD0iZmxvb2QiCiAgICAgICAgIGlkPSJmZUZsb29kODQ5NyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGluPSJmbG9vZCIKICAgICAgICAgaW4yPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iaW4iCiAgICAgICAgIHJlc3VsdD0iY29tcG9zaXRlIgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NDk5IiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0iYmx1ciIKICAgICAgICAgaW4yPSJjb21wb3NpdGUiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaWQ9ImZlQmxlbmQ4NTAxIiAvPgogICAgPC9maWx0ZXI+CiAgICA8ZmlsdGVyCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg1MDMiPgogICAgICA8ZmVHYXVzc2lhbkJsdXIKICAgICAgICAgc3RkRGV2aWF0aW9uPSI1IgogICAgICAgICByZXN1bHQ9InJlc3VsdDYiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1MDUiIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICByZXN1bHQ9InJlc3VsdDgiCiAgICAgICAgIGluPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NTA3IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ5IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgaW4yPSJTb3VyY2VBbHBoYSIKICAgICAgICAgaW49InJlc3VsdDgiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1MDkiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIKICAgICAgICAgaWQ9ImZlQ29sb3JNYXRyaXg4NTExIiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0icmVzdWx0MTAiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVCbGVuZDg1MTMiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgaWQ9ImZpbHRlcjg1NzUiCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyI+CiAgICAgIDxmZUZsb29kCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjAuMzI1NDkiCiAgICAgICAgIGZsb29kLWNvbG9yPSJyZ2IoMCwwLDApIgogICAgICAgICByZXN1bHQ9ImZsb29kIgogICAgICAgICBpZD0iZmVGbG9vZDg1NzciIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICBpbj0iZmxvb2QiCiAgICAgICAgIGluMj0iU291cmNlR3JhcGhpYyIKICAgICAgICAgb3BlcmF0b3I9ImluIgogICAgICAgICByZXN1bHQ9ImNvbXBvc2l0ZTEiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1NzkiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpbj0iY29tcG9zaXRlMSIKICAgICAgICAgc3RkRGV2aWF0aW9uPSIzIgogICAgICAgICByZXN1bHQ9ImJsdXIiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1ODEiIC8+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeD0iNiIKICAgICAgICAgZHk9IjYiCiAgICAgICAgIHJlc3VsdD0ib2Zmc2V0IgogICAgICAgICBpZD0iZmVPZmZzZXQ4NTgzIiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGluMj0ib2Zmc2V0IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgcmVzdWx0PSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1ODUiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHJlc3VsdD0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIGluPSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIHZhbHVlcz0iMCAwIDAgLTEgMCAwIDAgMCAtMSAwIDAgMCAwIC0xIDAgMCAwIDAgMSAwIgogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MjciIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NjI5IgogICAgICAgICBzdGREZXZpYXRpb249IjUiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0NiIKICAgICAgICAgaW49ImZiU291cmNlR3JhcGhpYyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzEiCiAgICAgICAgIGluMj0icmVzdWx0NiIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ4IgogICAgICAgICBpbj0iZmJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzMiCiAgICAgICAgIGluMj0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0OSIKICAgICAgICAgb3BlcmF0b3I9Im92ZXIiCiAgICAgICAgIGluPSJyZXN1bHQ4IiAvPgogICAgICA8ZmVDb2xvck1hdHJpeAogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MzUiCiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIgLz4KICAgICAgPGZlQmxlbmQKICAgICAgICAgaWQ9ImZlQmxlbmQ4NjM3IgogICAgICAgICBpbjI9InJlc3VsdDYiCiAgICAgICAgIGluPSJyZXN1bHQxMCIKICAgICAgICAgbW9kZT0ibm9ybWFsIiAvPgogICAgPC9maWx0ZXI+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzEyIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDYyNSIKICAgICBpbmtzY2FwZTpjeD0iNDIzNy4xODIyIgogICAgIGlua3NjYXBlOmN5PSIxNzk0LjM0NSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSI1NjE4LjY0LDIzODEuMzYiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjUxMzguOSwyNTYwIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODA3NiIKICAgICAgIGVuYWJsZWQ9InRydWUiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzg0NzYiCiAgICAgc3R5bGU9ImZpbHRlcjp1cmwoI2ZpbHRlcjg0OTEpIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM4NS40MTMxNiwyODU2LjQ4OTIpIiAvPgogIDxnCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIGlua3NjYXBlOmxhYmVsPSJMYXllciAxIgogICAgIHN0eWxlPSJkaXNwbGF5Om5vbmU7ZmlsdGVyOnVybCgjZmlsdGVyODQ3MikiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwzMDE3LjI4MzUpIiAvPgogIDxnCiAgICAgaWQ9Imc5NDciCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDA4NjcsMCwwLDAuOTkzMTA3ODgsMC4xMzY0ODYwNCwxMy4xOTM0OTMpIj4KICAgIDxwYXRoCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjY3NS44MjE5MiIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTMzNy45MDU3NCIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5OTk3MTcsLTAuMDAyMzc5NTcsMS45MzAxMjU2ZS00LDAuOTk5OTk5OTgsMCwwKSIKICAgICAgIGQ9Im0gMTE2MC40Njg0LDEyNTkuMDU0NiBhIDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgLTIxMS41OTAzNSw0MS44MDcyIDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgLTIxMS43NDM2NiwtNDEuNzc3IDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgMjExLjQzNzAzLC00MS44Mzc1IDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgMjExLjg5Njc4LDQxLjc0NjciCiAgICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgICAgc29kaXBvZGk6ZW5kPSI2LjI4MTczNjciCiAgICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICAgIHNvZGlwb2RpOnJ5PSI0MS44MDcyNzQiCiAgICAgICBzb2RpcG9kaTpyeD0iMjExLjY2NzA1IgogICAgICAgc29kaXBvZGk6Y3k9IjEyNTkuMDU0NiIKICAgICAgIHNvZGlwb2RpOmN4PSI5NDguODAxMzkiCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBpZD0icGF0aDUxNjEiCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45ODgyMDc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6NTkuNjI2MDk0ODI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5OjE3OC44NzgyODc0OCwgNTkuNjI2MDk1ODM7c3Ryb2tlLWRhc2hvZmZzZXQ6MTEzLjM4NTgyNjExO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIgLz4KICAgIDxlbGxpcHNlCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjYwLjI1MSIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTM4LjE2NDU3OCIKICAgICAgIHJ5PSI2MzEuNTAwMDYiCiAgICAgICByeD0iNjM5LjQ0NzM5IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTk5OTcxNywtMC4wMDIzNzk1NywxLjkzMDEyNTZlLTQsMC45OTk5OTk5OCwwLDApIgogICAgICAgY3k9IjY0Mi43NzA2OSIKICAgICAgIGN4PSI2NDkuMTc4MzQiCiAgICAgICBpZD0icGF0aDQ2NDciCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO2ZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTkuNTg0NzYwNjc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5OjU4Ljc1NDI3NjA0LCAxOS41ODQ3NTg2ODtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgPGVsbGlwc2UKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iNjAuMjUxIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItMzguMTY0NTc4IgogICAgICAgcnk9IjQ4NC4wOTgzIgogICAgICAgcng9IjQ5MC4xOTA2MSIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5OTk3MTcsLTAuMDAyMzc5NTcsMS45MzAxMjU2ZS00LDAuOTk5OTk5OTgsMCwwKSIKICAgICAgIGN5PSI2NDIuNzcwNjkiCiAgICAgICBjeD0iNjQ5LjE3ODM0IgogICAgICAgaWQ9InBhdGg5NDAiCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO2ZpbGw6I2ZmZmYwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTkuNDk4NDAxNjQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIgLz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="253,200,7,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="@project_folder || '/svg/smdev=' || replace( lower(&quot;cellule&quot;), ' ','_')   || '.svg'"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{94660daf-dffa-411c-aace-9dafe94c5105}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="103,99,139,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHdpZHRoPSI4NGNtIgogICBoZWlnaHQ9IjY0MGNtIgogICB2ZXJzaW9uPSIxLjEiCiAgIHZpZXdCb3g9IjAgMCA4NDAuMDAwMDMgNjQwMC4wMDAyIgogICBpZD0ic3ZnMTgiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PWZveWVyX3NpbXBsZS5zdmciCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMyMiI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NzQiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0NzcyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc2MSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NTkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzY2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc2NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0NzYyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDc2NiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NjgiCiAgICAgICB4MT0iLTI2NC40MjMwNyIKICAgICAgIHkxPSI4MzkuODY5MzgiCiAgICAgICB4Mj0iMTUyNC4xNjIyIgogICAgICAgeTI9IjgzOS44NjkzOCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09InRyYW5zbGF0ZSgtMTA1NjEuMDMsLTQyODMuMzA5NikiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwMDAwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzIwIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb3RoZXJzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1taWRwb2ludHM9InRydWUiCiAgICAgc2hvd2d1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMTEzMTM3MDkiCiAgICAgaW5rc2NhcGU6Y3g9IjY4NS45Nzg4MiIKICAgICBpbmtzY2FwZTpjeT0iMjIzNDAuNjMxIgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnNDkxMiIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSIxMzkuNTY1LDEyNDEuMzkiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjE2NjI4LjIsMTI4My40OCIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wZXJwZW5kaWN1bGFyPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRhbmdlbnRpYWw9ImZhbHNlIgogICAgIHNjYWxlLXg9IjEwIgogICAgIGZpdC1tYXJnaW4tdG9wPSIwIgogICAgIGZpdC1tYXJnaW4tbGVmdD0iMCIKICAgICBmaXQtbWFyZ2luLXJpZ2h0PSIwIgogICAgIGZpdC1tYXJnaW4tYm90dG9tPSIwIgogICAgIGlua3NjYXBlOm9iamVjdC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgc2hvd2JvcmRlcj0idHJ1ZSIKICAgICB2aWV3Ym94LXdpZHRoPSI3OC4yNDIiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkNDc3MSIKICAgICAgIG9yaWdpbng9IjkuODMxMDkzMWUtMDUiCiAgICAgICBvcmlnaW55PSIxLjg4Nzg4NDllLTA3IiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTYzOC4yNjU0LC05NjMuNjg1NjQiCiAgICAgICBvcmllbnRhdGlvbj0iMCwxIgogICAgICAgaWQ9Imd1aWRlODc2IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGEyIj4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICAgIDxjYzpsaWNlbnNlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIiAvPgogICAgICAgIDxkYzpjcmVhdG9yPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+am1hPC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpjcmVhdG9yPgogICAgICAgIDxkYzpwdWJsaXNoZXI+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5BemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnB1Ymxpc2hlcj4KICAgICAgICA8ZGM6cmlnaHRzPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+KGMpQXppbXV0PC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpyaWdodHM+CiAgICAgIDwvY2M6V29yaz4KICAgICAgPGNjOkxpY2Vuc2UKICAgICAgICAgcmRmOmFib3V0PSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIj4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjUmVwcm9kdWN0aW9uIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEaXN0cmlidXRpb24iIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNOb3RpY2UiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNBdHRyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cHJvaGliaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNDb21tZXJjaWFsVXNlIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEZXJpdmF0aXZlV29ya3MiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNTaGFyZUFsaWtlIiAvPgogICAgICA8L2NjOkxpY2Vuc2U+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlkPSJnNDkxMCIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1NTE3Ljk1NjIsOTM3MC44NTY2KSIKICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii03ODkuMDYwODkiCiAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI0MzMuMjUxMTYiPgogICAgPGcKICAgICAgIGlkPSJnNDkxMiIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iNTcuMTA3MTgzIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSItMTguNzEyOTMyIgogICAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lIj4KICAgICAgPGcKICAgICAgICAgaWQ9Imc4NTEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk2OTA2OSwwLDAsMC45OTk2ODU2NSwxMi45MzI1ODIsLTIwLjkzMzM5NSkiPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDAwMDAxLDAsMCwxLjAwMDA1MzYsLTIuNjM5MjUwNCwtMS44MjQ2MzkpIgogICAgICAgICAgIGlkPSJnODk3Ij4KICAgICAgICAgIDxnCiAgICAgICAgICAgICBpZD0iZzg4OCIKICAgICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDEuMDAyMTExNCwwLDAsMC45OTg2Mjk0OSwxMS42NDQ4NywtMTAuMzkxMDA2KSI+CiAgICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2NjY2NjY2NjIgogICAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgICBpZD0icGF0aDgiCiAgICAgICAgICAgICAgIGQ9Im0gLTU0ODUuMjM0NCwtODAyMi42MDk0IGggLTMwLjA4MiBsIDE3Ny42MTEzLDQ0MC40MzU2IDQyMy42MjUsMC4zNDE4IDE3OS41MzUyLC00NDIuODQ5OSAtMzAuMDgyMSwwLjA1MSAtMTY5LjY2MDEsNDEyLjY5OTMgLTM4My4wODIsLTAuMzA4NiB6IgogICAgICAgICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjY2NjY2NjY2NjIgogICAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgICBpZD0icGF0aDEwIgogICAgICAgICAgICAgICBkPSJtIC01NTAwLjI3NTQsLTkzMzIuOTMxNiBjIC04LjMwNjQsMCAtMTUuMDM5OSw2LjczNDYgLTE1LjA0MSwxNS4wNDEgdiAxMjk1LjI4MTIgaCAzMC4wODIgdiAtMTI4MC4yNDAyIGggNzE4LjQ3NjYgbCAyLjEzMDgsMTI3OC4yMTg3IDMwLjA4MjEsLTAuMDUxIC0yLjE1NDMsLTEyOTMuMjM0NCBjIC0wLjAxNSwtOC4yOTczIC02Ljc0NTcsLTE1LjAxNTYgLTE1LjA0MywtMTUuMDE1NiB6IgogICAgICAgICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNzY2NjY2NzY2NjY2NzYyIKICAgICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgICAgaWQ9InBhdGgxMiIKICAgICAgICAgICAgICAgZD0ibSAtNTEyNy4wNDMsLTkyMzIuOTQ1MyBjIC04NC44NzIzLDEuMjEwMyAtMTU2LjMwMTEsNzQuODA0IC0yMDcuOTUxMSwxNDQuOTkyMiAtNTEuNjUwMSw3MC4xODgyIC04My43NjU3LDEzOS44NTM1IC04My43NjU3LDEzOS44NTM1IGwgLTkuODU3NCwyMS4zNDc2IGggMjMuNTEzNyA1NzkuNDY2OCBsIC05LjExNzIsLTIxLjAyNTMgYyAwLDAgLTMwLjY2MTcsLTcwLjg1MDYgLTgxLjY0MjYsLTE0MS42OTkzIC01MC45ODA4LC03MC44NDg2IC0xMjIuOTU0NiwtMTQ0LjcxODkgLTIxMC42NDY1LC0xNDMuNDY4NyB6IG0gMC40Mjc4LDMwLjA4MDEgYyA2OS44NzIzLC0wLjk5NjIgMTM3LjAzNTUsNjMuMTg5NCAxODUuODAwNywxMzAuOTU5IDMyLjkwODUsNDUuNzMzMiA1Ni4xMTI4LDkwLjAwMDUgNjguMzg2OCwxMTUuMDcyMiBoIC01MDguMjExIGMgMTIuODQzLC0yNC44NTc4IDM2LjY4NzcsLTY4LjE5MDIgNjkuODc1LC0xMTMuMjg5IDQ5LjU0NDksLTY3LjMyNzQgMTE3LjI1MTgsLTEzMS43ODgzIDE4NC4xNDg1LC0xMzIuNzQyMiB6IgogICAgICAgICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjY2NjY2NjY2NjY2Njc3NzYyIKICAgICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgICAgaWQ9InBhdGg0Nzc5IgogICAgICAgICAgICAgICBkPSJtIC01MTI3LjAxMjQsLTg5MTguNzcxIGggMzE0LjEwOTkgbCAtMTIuMzAzLC0yNy41MjY0IGMgLTM5LjUyNTQsLTg4LjQzMDQgLTk0LjEwNjMsLTE2OS45MzE2IC0xNDguNjY4OSwtMjIxLjk5MzggLTM3LjY0MTgsLTM1LjkxNzQgLTc0LjQ2NDYsLTU3Ljc1NTEgLTExNC4xNzY4LC02Ny43MTMzIC0yMC4wODU2LC01LjAzNiAtNTUuNjc0NiwtNC42NjkgLTc1Ljg1MDgsMC43ODM2IC03MS41MTUsMTkuMzI3OCAtMTQxLjc2MjcsODIuODQ4NiAtMjA5LjQzODMsMTg5LjM4NDQgLTI0Ljk5NjcsMzkuMzUwMyAtNTMuMjE3Nyw5MS44NTIxIC02Ni4wNTE4LDEyMi44ODE1IGwgLTEuNzMwNSw0LjE4NCB6IG0gLTIzMC43NSwtNjMuMDcyNSBjIDc4Ljc1MjgsLTEzOS4wNTM3IDE2OS4wMzM4LC0yMTkuMjQwMiAyNDAuMDg0NiwtMjEzLjI0MSAzNi43NjYxLDMuMTAzNiA3Mi40MTA5LDIxLjkxNTYgMTEwLjQ3NDksNTguMzA0MyAzMC45NDg2LDI5LjU4NTUgNjMuMzkzNCw3MS40NDM5IDkxLjcwNjgsMTE4LjMxNDQgMTIuOTcyOSwyMS40NzU4IDI4LjUwMDUsNTAuNDQyOCAyOC41MDA1LDUzLjE2ODIgMCwxLjI3ODkgLTc4LjEzOTgsMS44OTY1IC0yMzkuOTAyNiwxLjg5NjUgLTEzMS45NDY0LDAgLTIzOS45MDI3LC0wLjU1ODYgLTIzOS45MDI3LC0xLjI0MTIgMCwtMC42ODM1IDQuMDY3MiwtOC40MjM1IDkuMDM4NSwtMTcuMjAxMiB6IgogICAgICAgICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45ODgyMDc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0ic3NjY2NjY3MiCiAgICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICAgIGlkPSJwYXRoNDc4MSIKICAgICAgICAgICAgICAgZD0ibSAtNTEyNi44OTgyLC04OTU4LjI4NTUgYyAxMzcuNTIzMiwwIDI1MC4wNDIsLTAuNTkwMyAyNTAuMDQyLC0xLjMxMzEgMCwtMy4xODMxIC0zMC4xMDM1LC01Ni4xNDc4IC00NS4wMzQ3LC03OS4yMzQ0IC01OC4yMTY3LC05MC4wMTM4IC0xMjMuNjYwMywtMTQ4LjMwODcgLTE3OS4yNTk3LC0xNTkuNjc2NiAtNDYuMzA4OSwtOS40NjgxIC05My4zMTM2LDkuOTgwNCAtMTQ2LjAwMDUsNjAuNDA2OSAtNDAuMjcxMSwzOC41NDI4IC04Mi41Nzk3LDk1LjgxMiAtMTIwLjA3NTQsMTYyLjUzMzggbCAtOS43MTMzLDE3LjI4MzQgeiIKICAgICAgICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNmZmZmMDA7ZmlsbC1vcGFjaXR5OjAuOTg4MjA3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjMwLjA4MzA4MjI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgICAgICAgPC9nPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOnVybCgjbGluZWFyR3JhZGllbnQ0NzY4KTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MzguMjE2MzUwNTY7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICAgIGQ9Ik0gLTMxNDcuMjc5OCwtMzI5OS4zMDk1IEggLTU0ODEuMDI5NiIKICAgICAgICAgICAgIGlkPSJwYXRoNDc2MCIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDc2MiIKICAgICAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gLTMxNDcuMjc5OCwtMzI5OS4zMDk1IEggLTU0ODEuMDI5NiIKICAgICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTAuNjEzNzMzMzYsLTEuMDAzMTcwNCwwLC04NDM0Ljg0MDIsLTk1MTMuNTkzMykiIC8+CiAgICAgICAgPC9nPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDg3NCIKICAgICAgICAgICBkPSJtIC01MTI3LjcwOTgsLTYxNTEuODU2OSA0ZS00LDMxODEiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC4wNDk5OTc5NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eTowIiAvPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K"/>
            <Option type="QString" name="offset" value="0.00000000000000006,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="@project_folder || '/svg/smdev=' || replace( lower(&quot;cellule&quot;), ' ','_')   || '.svg'"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="2" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{1ea54c83-c4f0-4731-af26-7a036bb5bc32}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="103,99,139,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIHg9IjBweCIKICAgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgNTI5MS4zMzg3IDEyMjgzLjQ2MyIKICAgaWQ9InN2ZzIiCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXByb2plY3RldXIuc3ZnIgogICB3aWR0aD0iMTQwY20iCiAgIGhlaWdodD0iMzI1Y20iPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE2Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ5NzEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzRkNGQ0ZDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NjkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTI3IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDkyNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTIzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkxNSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MDkiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0OTA3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg4MSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3ODEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0Nzc5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3NzciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0NzgxIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc4MyIKICAgICAgIHgxPSI5NTkuNDk5OTQiCiAgICAgICB5MT0iMjU5Ny43OTUyIgogICAgICAgeDI9Ijk2MC40OTk5NCIKICAgICAgIHkyPSIyNTk3Ljc5NTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkwOSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MTEiCiAgICAgICB4MT0iMjc5OS44ODg5IgogICAgICAgeTE9IjMxNTcuNzk1MiIKICAgICAgIHgyPSI2MzIwLjExMDQiCiAgICAgICB5Mj0iMzE1Ny43OTUyIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ5MjciCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTI5IgogICAgICAgeDE9IjI1NDEuMDczIgogICAgICAgeTE9IjQxNTcuNzk0OSIKICAgICAgIHgyPSI2NDE4LjkyNjMiCiAgICAgICB5Mj0iNDE1Ny43OTQ5IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ5MjciCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTI5LTgiCiAgICAgICB4MT0iMjU0MS4wNzMiCiAgICAgICB5MT0iNDE1Ny43OTQ5IgogICAgICAgeDI9IjY0MTguOTI2MyIKICAgICAgIHkyPSI0MTU3Ljc5NDkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MjMtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTE5LTEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkxNS05IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkyNyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MTEtMiIKICAgICAgIHgxPSIyNzk5Ljg4ODkiCiAgICAgICB5MT0iMzE1Ny43OTUyIgogICAgICAgeDI9IjYzMjAuMTEwNCIKICAgICAgIHkyPSIzMTU3Ljc5NTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MDUtNSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODgxLTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjAyIgogICAgIGlua3NjYXBlOmN4PSI2NTcuNDA5OTIiCiAgICAgaW5rc2NhcGU6Y3k9IjI3NDQuODgzMiIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9ImZhbHNlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iMTI0ODAsLTY0MCIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMTI0ODAsNDQ4MCIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNzYiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzEwMTkiPgogICAgPGcKICAgICAgIGlkPSJnOTk2Ij4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ3ODMpO3N0cm9rZS13aWR0aDowLjk4MjY3NzE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4NDI5LjU4MTEsNDM3My4wMzI0IFYgMTgxMy4wMzI5IgogICAgICAgICBpZD0icGF0aDQ3NzUiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzc3IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDg0MjkuNTgxMSw0MzczLjAzMjQgViAxODEzLjAzMjkiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTIuOTA0ODAwNDI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgICAgICBpZD0icGF0aDQ4MTMiCiAgICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgICAgc29kaXBvZGk6Y3g9Ii0yNzUwLjQ0ODUiCiAgICAgICAgIHNvZGlwb2RpOmN5PSIxMzc4LjQwOTgiCiAgICAgICAgIHNvZGlwb2RpOnJ4PSI5MjQuMDU3OTIiCiAgICAgICAgIHNvZGlwb2RpOnJ5PSIxNTkuMzYzMzkiCiAgICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgICBzb2RpcG9kaTplbmQ9IjAuNzQ0ODgwMzUiCiAgICAgICAgIGQ9Im0gLTE4MjYuMzkwNiwxMzc4LjQwOTggYSA5MjQuMDU3OTIsMTU5LjM2MzM5IDAgMCAxIC0yNDQuNzE5MSwxMDguMDI5OSBsIC02NzkuMzM4OCwtMTA4LjAyOTkgeiIKICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICAgICAgPGVsbGlwc2UKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGlkPSJwYXRoNDgzMyIKICAgICAgICAgY3g9Ii0yNTQ1LjE5MDkiCiAgICAgICAgIGN5PSIxNTI4LjU2NiIKICAgICAgICAgcng9IjI1OS42MzMxOCIKICAgICAgICAgcnk9IjMyLjI5MDQ5MyIKICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGlkPSJwYXRoNDgzNSIKICAgICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgICBzb2RpcG9kaTpjeD0iLTMxNzMuMzY0NyIKICAgICAgICAgc29kaXBvZGk6Y3k9IjE1MzEuNjM0NCIKICAgICAgICAgc29kaXBvZGk6cng9IjUwMS4xNDExNCIKICAgICAgICAgc29kaXBvZGk6cnk9IjYuMTM4MzQxOSIKICAgICAgICAgc29kaXBvZGk6c3RhcnQ9IjQuNzEzMTcwMSIKICAgICAgICAgc29kaXBvZGk6ZW5kPSI1LjAwMDcwNTYiCiAgICAgICAgIGQ9Im0gLTMxNzIuOTczMywxNTI1LjQ5NjEgYSA1MDEuMTQxMTQsNi4xMzgzNDE5IDAgMCAxIDE0Mi4xMDI0LDAuMjUzMyBsIC0xNDIuNDkzOCw1Ljg4NSB6IgogICAgICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTApIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmZmYwMDtmaWxsLW9wYWNpdHk6MC45MzcwNzg2ODtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTg4LjE3NDY4MjYyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICAgICAgaWQ9InBhdGg0ODYxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLC0xLDAsMCwwKSIKICAgICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgICBzb2RpcG9kaTpjeD0iLTYxNDEuNzMxOSIKICAgICAgICAgc29kaXBvZGk6Y3k9Ii0yNjMwLjg0MzMiCiAgICAgICAgIHNvZGlwb2RpOnJ4PSIyMjY5LjQ2MzYiCiAgICAgICAgIHNvZGlwb2RpOnJ5PSIxNzk5Ljc1NzYiCiAgICAgICAgIHNvZGlwb2RpOnN0YXJ0PSI0LjcxNjg0NzkiCiAgICAgICAgIHNvZGlwb2RpOmVuZD0iMS41NzAxMjA5IgogICAgICAgICBkPSJtIC02MTMxLjYxMjYsLTQ0MzAuNTgyOSBhIDIyNjkuNDYzNiwxNzk5Ljc1NzYgMCAwIDEgMjI1OS4zNDAyLDE4MDMuMTQ0MyAyMjY5LjQ2MzYsMTc5OS43NTc2IDAgMCAxIC0yMjY3LjkyNjcsMTc5Ni4zNTI1IGwgLTEuNTMyOCwtMTc5OS43NTcyIHoiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJtIDEyNTE4LjkxNSw1MTE2LjI0MTQgdiAwIgogICAgICAgICBpZD0icGF0aDQ4NzkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0ODgxIgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTE4LjkxNSw1MTE2LjI0MTQgYyAzMS44NDcsLTk1LjA3MSAzMS44NDcsLTk1LjA3MSAwLDAiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTExKTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gODkxOC43NDczLDM1MTYuMjQxNCAzNTYyLjcwNzcsODAwIgogICAgICAgICBpZD0icGF0aDQ5MDMiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTA1IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDg5MTguNzQ3MywzNTE2LjI0MTQgMzU2Mi43MDc3LDgwMCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCAtODgwLC00MDAiCiAgICAgICAgIGlkPSJwYXRoNDkxMyIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MTUiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCBjIC0yMDAuMzY3LC0yNjkuNzgyIC01NzAuMDIyLC0zMDQuNTg2IC04ODAsLTQwMCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCAtODAsLTgwIgogICAgICAgICBpZD0icGF0aDQ5MTciCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE5IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTE4LjkxNSw1NTk2LjI0MTQgYyAtMTcuMTM2LC0zMy41OTUgLTUzLjMzNCwtNTMuMzMzIC04MCwtODAiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTI5KTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gODY3OC45MTQ4LDQyMzYuMjQxNCAzODQwLjAwMDIsMTM2MCIKICAgICAgICAgaWQ9InBhdGg0OTIxIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkyMyIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSA4Njc4LjkxNDgsNDIzNi4yNDE0IDM4NDAuMDAwMiwxMzYwIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0ibSAxMjUxMS43OTYsOTU4Ljc3MzI5IHYgMCIKICAgICAgICAgaWQ9InBhdGg0ODc5LTQiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0ODgxLTgiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDk1OC43NzMyOSBjIDMxLjg0Nyw5NS4wNzE2MSAzMS44NDcsOTUuMDcxNjEgMCwwIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NDkxMS0yKTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gODkxMS42Mjg4LDI1NTguNzczMiAxMjQ3NC4zMzYsMTc1OC43NzMzIgogICAgICAgICBpZD0icGF0aDQ5MDMtOSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MDUtNSIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSA4OTExLjYyODgsMjU1OC43NzMyIDEyNDc0LjMzNiwxNzU4Ljc3MzMiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJtIDEyNTExLjc5Niw0NzguNzczNDkgLTg4MCwzOTkuOTk5OCIKICAgICAgICAgaWQ9InBhdGg0OTEzLTkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE1LTkiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDQ3OC43NzM0OSBjIC0yMDAuMzY3LDI2OS43ODE4IC01NzAuMDIyLDMwNC41ODYyIC04ODAsMzk5Ljk5OTgiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJtIDEyNTExLjc5Niw0NzguNzczNDkgLTgwLDgwIgogICAgICAgICBpZD0icGF0aDQ5MTctNCIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MTktMSIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IGMgLTE3LjEzNSwzMy41OTQ3IC01My4zMzMsNTMuMzMzNCAtODAsODAiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTI5LTgpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4NjcxLjc5NjMsMTgzOC43NzMzIDEyNTExLjc5Niw0NzguNzczNDkiCiAgICAgICAgIGlkPSJwYXRoNDkyMS0yIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkyMy0wIgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDg2NzEuNzk2MywxODM4Ljc3MzMgMTI1MTEuNzk2LDQ3OC43NzM0OSIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGQ9Im0gMzYwNS44MTgzLDYwNTcuMTIwNSBjIC0zOTEuMDY4LDguNzg5MyAtMTE3Mi4yNzIsMTUuOTgwNSAtMTczNi4wMTA3LDE1Ljk4MDUgSCA4NDQuODI5IGwgMTUuMzcyMywtMTg3LjI5MTkgYyA1My4xODU5LC02NDguMDAxMyAzNjcuNDQ4MSwtMTI4MS40MTI0IDgwMS44NTk4LC0xNjE2LjE4ODUgMzA1LjEwNjYsLTIzNS4xMjggNTc2LjI2NzQsLTMzMC43NTAxIDkzMy45NTQyLC0zMjkuMzUgODIzLjcwNCwzLjIyNDcgMTUzNC4yNTQsNzU2Ljk3OTIgMTY4NS45MDgsMTc4OC40MTgxIDE5LjA1OCwxMjkuNjE0MiAzNC43MTMsMjUyLjkzOTcgMzQuNzg4LDI3NC4wNTY5IDAuMTE5LDMzLjg4NDUgLTgzLjM4NCw0MC4yNzE3IC03MTAuODkzLDU0LjM3NDkgeiIKICAgICAgICAgaWQ9InBhdGg1MTM3IgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmZmYwMDtmaWxsLW9wYWNpdHk6MC45MzcwNzg2ODtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NzUuMjY5ODc0NTc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIHN0cm9rZSBmaWxsIgogICAgICAgICBkPSJtIDk0LjQ4ODIsMTM2LjM3ODEzIGMgLTAuNTgxNyw0Ny4xMjIwMyAxMjkwLjEwNjgsMzgxNC40NzY2NyAxMzE2LjA4ODIsMzg0MS4xMTIzNyA2LjEyODksNi4yODMyIDI3LjQ2NjUsNS4wMDEgNDcuNDE3LC0yLjg0NzQgMzAuODM0NywtMTIuMTMwMyAyMi43MjEyLC01My4zNDAyIC01NC4xMTg4LC0yNzQuODU5MSBDIDEzNTQuMTU4NCwzNTU2LjQ1OTEgMTA1NS4zNjEyLDI2OTEuNTM1OCA3MzkuODgwOSwxNzc3LjczMzYgMzg4LjU4MDQsNzYwLjE3Njg2IDE1NC42ODUxLDExNi4yNzUzIDEzNi4zNjE3LDExNi4yNzUzIGMgLTE2LjQ1NTEsMCAtMzUuMjY4LDguODcyMyAtNDEuODA2NCwxOS43MTg3IC0wLjA0MiwwLjA3MDUgLTAuMDY0LDAuMTk5NjIgLTAuMDY3LDAuMzg0MTMgeiBtIDEyMzcuODI4OCwzNS4yNTYzMiBjIC0zLjU1MDEsMy42Mzg2NiA3NjMuMzQ3OCwzNTMwLjIxODI1IDc3Ny4xMDA1LDM1NzMuNDk1NTUgMi4zMDIzLDcuMjQ1MyAxNi42NTEzLDYuMzgyOSA1My45MjY2LC0zLjI0MzIgMzkuNzM5MiwtMTAuMjYyNCA0OS45MTYzLC0xNi4xMjc0IDQ2Ljc4MzEsLTI2Ljk1OTkgLTMuOTc0NiwtMTMuNzQxMiAtNzY4LjcyOTcsLTM1MDUuMTg1ODkgLTc3Ny4wNzc0LC0zNTQ3LjcwMTM4IGwgLTQuMzU2NSwtMjIuMTg2ODEgLTQ1Ljg4MjMsMTAuOTMzMSBjIC0yNS4yMzU1LDYuMDE0MDEgLTQ3Ljk1NzYsMTMuMDYxNDUgLTUwLjQ5NCwxNS42NjI2NCB6IgogICAgICAgICBpZD0icGF0aDQ4ODMiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojZmZmZjAwO2ZpbGwtb3BhY2l0eTowLjkzNzA3ODY4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDo3NS4yNjk4NzQ1NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgICAgICBkPSJtIDM1NDYuNDc3MywxOTY2LjYwNzkgYyAtMjE0Ljc5Niw5NzcuNDU3IC0zOTAuNjQyLDE3NzcuODA5NCAtMzkwLjc3LDE3NzguNTYwNCAtMC40NTcsMi43MDEyIC05Mi43NjQsLTIwLjc1NDIgLTk2LjQxOCwtMjQuNTAwMiAtNC4zODMsLTQuNDk0MiA3NzUuMDE5LC0zNTY3Ljg1MzAxIDc4MS45MzcsLTM1NzQuOTQ0ODEgMi41NzMsLTIuNjM4NSAyNS42MDMsLTAuMzQzOCA1MS4xNzksNS4wOTk1NiAzNC45MDIsNy40Mjc1MyA0Ni4yNjQsMTMuNDczODEgNDUuNTU2LDI0LjI0MjU2IC0wLjUxOSw3Ljg5MDMxIC0xNzYuNjg3LDgxNC4wODQ0NSAtMzkxLjQ4NCwxNzkxLjU0MjQ5IHoiCiAgICAgICAgIGlkPSJwYXRoNDg4OCIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNmZmZmMDA7ZmlsbC1vcGFjaXR5OjAuOTM3MDc4Njg7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjc1LjI2OTg3NDU3O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGQ9Im0gNDU1OS41OTIzLDE5NzkuNDI0MyBjIC0zNTAuMTkxLDEwMTIuNzAxOCAtNjUwLjI2OSwxODgwLjMzNTYgLTY2Ni44NCwxOTI4LjA3NDMgbCAtMzAuMTI4LDg2Ljc5NzggLTQ5LjAwNCwtMTYuOTY1NyAtNDkuMDA0LC0xNi45NjU3IDM3LjIxNiwtMTA4LjI4MSBjIDIwLjQ2OSwtNTkuNTU0NyAzMjAuODQ5LC05MjkuMzc3MiA2NjcuNTExLC0xOTMyLjkzOSBsIDYzMC4yOTQsLTE4MjQuNjU3MjYgNDguODc3LDE3LjU1MzA1IGMgMjYuODgyLDkuNjU0NjkgNDguNjMyLDE5LjQ3ODc1IDQ4LjMzMywyMS44Mjk5MSAtMC4zLDIuMzUyMTYgLTI4Ny4wNjQsODMyLjg1MDM2IC02MzcuMjU1LDE4NDUuNTUzNiB6IgogICAgICAgICBpZD0icGF0aDQ4OTAiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L2c+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoMTAxMyIKICAgICAgIGQ9Im0gMjYzMC42ODY0LDYxNDEuNzMxOCAtMTYuNTk0MSw2MDQ3LjI0NDIiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTIuODUzNTY5MDM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="@project_folder || '/svg/smdev=' || replace( lower(&quot;cellule&quot;), ' ','_')   || '.svg'"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{5373aee4-0483-4e9f-b863-07eb3494fb5e}">
      <rule description="étiquettes foyers à poser" filter="&quot;etat&quot; = 'A poser'" key="{bd32c09e-2b13-4e35-9447-904918716b84}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'foyer à poser : '  ||  id_foyer ||  ' - (départ : ' ||  &quot;id_depart&quot;  || ')'||  '\n'  || 'Ph : ' ||  &quot;num_phase&quot; " fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="255,0,252,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0.20000000000000001" bufferNoFill="0" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="1" shapeRadiiX="0" shapeRotationType="1" shapeBorderColor="255,0,252,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0.14999999999999999" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0.14999999999999999" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0.14999999999999999" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="255,0,252,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0.15"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="20" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="0" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MM" rotationAngle="0" quadOffset="4" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="1" maxNumLabels="2000" fontMinPixelSize="1" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="2" scaleMax="5000" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{3e93a8ff-c0fb-4d36-b246-238db773b235}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{3e366c63-fed3-4e04-9e03-1072f622dda7}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,0,252,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="étiquettes foyers à remplacer" filter="&quot;etat&quot; =  'A remplacer'" key="{f2fd097a-5e28-41c4-b60c-4523d6d5571a}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'foyer à remplacer : '  ||  id_foyer || '- (départ : ' ||  &quot;id_depart&quot;  || ')'" fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="255,201,78,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0.20000000000000001" bufferNoFill="0" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="1" shapeRadiiX="0" shapeRotationType="1" shapeBorderColor="255,201,78,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0.14999999999999999" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0.14999999999999999" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="0.90000000000000002" shapeSizeX="0.14999999999999999" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="255,201,78,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0.15"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="10" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MM" rotationAngle="0" quadOffset="0" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="LineGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="1" maxNumLabels="2000" fontMinPixelSize="1" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="2" scaleMax="5000" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{46c55e7f-cb3d-4448-9269-8086352ebd48}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{27674211-05fb-40c3-a342-070d619e1544}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,201,78,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="étiquettes foyers à déposer" filter="&quot;etat&quot; = 'A déposer'" key="{6a0a9b20-7b06-4641-b9a4-322db0c1bacf}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Normal" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="50" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'foyer à déposer : '  ||  &quot;id_foyer&quot; " fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="165,165,165,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="1" bufferNoFill="1" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="0" shapeRadiiX="0" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="190,178,151,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="0" placement="0" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="5" distUnits="MM" rotationAngle="0" quadOffset="0" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="0" maxNumLabels="2000" fontMinPixelSize="3" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="1" scaleMax="0" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{c598bd81-6807-4b8a-aae8-472796858c09}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{9873a699-3b0a-4628-bcb4-ab22dcb0f99f}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;204,204,204,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="étiquettes foyers déposé" filter="&quot;etat&quot; = 'Déposé'" key="{44fdfc87-a904-467d-9d7f-742024150ca0}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Normal" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="50" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'foyer déposé : '  ||  &quot;id_foyer&quot; " fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="165,165,165,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="1" bufferNoFill="1" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="0" shapeRadiiX="0" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="190,178,151,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="0" placement="0" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="5" distUnits="MM" rotationAngle="0" quadOffset="0" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="0" maxNumLabels="2000" fontMinPixelSize="3" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="1" scaleMax="0" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{c869a814-4545-4f34-a399-da6488a5645b}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{61a8b52e-addc-4ace-a856-64d5a914f28e}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;204,204,204,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="texte foyer" filter="&quot;etat&quot; = 'Actif'" key="{0fe8f9bd-aebf-4371-8823-515884bf017a}">
        <settings calloutType="balloon">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold Italic" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="1" previewBkgrdColor="255,255,255,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="&quot;id_foyer&quot;  ||  '\n'  || '(départ : ' ||  &quot;id_depart&quot;  || ')'" fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="255,127,0,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0" bufferNoFill="0" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="Point" shapeOffsetUnit="Point" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="Point" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="0" shapeRadiiX="0" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeBorderWidthUnit="Point" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="Point"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="1" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="Point" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="Point"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MM" rotationAngle="0" quadOffset="4" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="0" maxNumLabels="2000" fontMinPixelSize="3" mergeLines="0" obstacleType="1" minFeatureSize="0" obstacle="1" obstacleFactor="1" scaleMax="0" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="PositionX">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
                <Option type="Map" name="PositionY">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="balloon">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="double" name="cornerRadius" value="0.5"/>
              <Option type="QString" name="cornerRadiusMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="cornerRadiusUnit" value="MM"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option type="Map" name="properties">
                  <Option type="Map" name="OriginX">
                    <Option type="bool" name="active" value="false"/>
                    <Option type="int" name="type" value="1"/>
                    <Option type="QString" name="val" value=""/>
                  </Option>
                  <Option type="Map" name="OriginY">
                    <Option type="bool" name="active" value="false"/>
                    <Option type="int" name="type" value="1"/>
                    <Option type="QString" name="val" value=""/>
                  </Option>
                </Option>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="fillSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{03baf7f9-3da0-4373-9b62-789bb61afa9f}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;204,204,204,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,127,0,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="margins" value="0.5,0.5,0.5,0.5"/>
              <Option type="QString" name="marginsUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="wedgeWidth" value="1"/>
              <Option type="QString" name="wedgeWidthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="wedgeWidthUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="QString" name="QFieldSync/action" value="remove"/>
      <Option type="QString" name="QFieldSync/cloud_action" value="offline"/>
      <Option type="QString" name="QFieldSync/photo_naming" value="{&quot;photo&quot;: &quot;'DCIM/foyers_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;}"/>
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
      </Option>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option type="QString" name="labeling/addDirectionSymbol" value="false"/>
      <Option type="QString" name="labeling/angleOffset" value="0"/>
      <Option type="QString" name="labeling/blendMode" value="0"/>
      <Option type="QString" name="labeling/bufferBlendMode" value="0"/>
      <Option type="QString" name="labeling/bufferColorA" value="255"/>
      <Option type="QString" name="labeling/bufferColorB" value="255"/>
      <Option type="QString" name="labeling/bufferColorG" value="255"/>
      <Option type="QString" name="labeling/bufferColorR" value="255"/>
      <Option type="QString" name="labeling/bufferDraw" value="false"/>
      <Option type="QString" name="labeling/bufferJoinStyle" value="128"/>
      <Option type="QString" name="labeling/bufferNoFill" value="false"/>
      <Option type="QString" name="labeling/bufferSize" value="1"/>
      <Option type="QString" name="labeling/bufferSizeInMapUnits" value="false"/>
      <Option type="QString" name="labeling/bufferSizeMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/bufferTransp" value="0"/>
      <Option type="QString" name="labeling/centroidInside" value="false"/>
      <Option type="QString" name="labeling/centroidWhole" value="false"/>
      <Option type="QString" name="labeling/decimals" value="3"/>
      <Option type="QString" name="labeling/displayAll" value="false"/>
      <Option type="QString" name="labeling/dist" value="0"/>
      <Option type="QString" name="labeling/distInMapUnits" value="false"/>
      <Option type="QString" name="labeling/distMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/drawLabels" value="true"/>
      <Option type="QString" name="labeling/enabled" value="true"/>
      <Option type="QString" name="labeling/fieldName" value="id_foyer"/>
      <Option type="QString" name="labeling/fitInPolygonOnly" value="false"/>
      <Option type="QString" name="labeling/fontCapitals" value="0"/>
      <Option type="QString" name="labeling/fontFamily" value="MS Shell Dlg 2"/>
      <Option type="QString" name="labeling/fontItalic" value="false"/>
      <Option type="QString" name="labeling/fontLetterSpacing" value="0"/>
      <Option type="QString" name="labeling/fontLimitPixelSize" value="false"/>
      <Option type="QString" name="labeling/fontMaxPixelSize" value="10000"/>
      <Option type="QString" name="labeling/fontMinPixelSize" value="3"/>
      <Option type="QString" name="labeling/fontSize" value="1.25"/>
      <Option type="QString" name="labeling/fontSizeInMapUnits" value="false"/>
      <Option type="QString" name="labeling/fontSizeMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/fontStrikeout" value="false"/>
      <Option type="QString" name="labeling/fontUnderline" value="false"/>
      <Option type="QString" name="labeling/fontWeight" value="50"/>
      <Option type="QString" name="labeling/fontWordSpacing" value="0"/>
      <Option type="QString" name="labeling/formatNumbers" value="false"/>
      <Option type="QString" name="labeling/isExpression" value="false"/>
      <Option type="QString" name="labeling/labelOffsetInMapUnits" value="true"/>
      <Option type="QString" name="labeling/labelOffsetMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/labelPerPart" value="false"/>
      <Option type="QString" name="labeling/leftDirectionSymbol" value="&lt;"/>
      <Option type="QString" name="labeling/limitNumLabels" value="false"/>
      <Option type="QString" name="labeling/maxCurvedCharAngleIn" value="25"/>
      <Option type="QString" name="labeling/maxCurvedCharAngleOut" value="-25"/>
      <Option type="QString" name="labeling/maxNumLabels" value="2000"/>
      <Option type="QString" name="labeling/mergeLines" value="false"/>
      <Option type="QString" name="labeling/minFeatureSize" value="0"/>
      <Option type="QString" name="labeling/multilineAlign" value="3"/>
      <Option type="QString" name="labeling/multilineHeight" value="1"/>
      <Option type="QString" name="labeling/namedStyle" value="Normal"/>
      <Option type="QString" name="labeling/obstacle" value="true"/>
      <Option type="QString" name="labeling/obstacleFactor" value="1"/>
      <Option type="QString" name="labeling/obstacleType" value="0"/>
      <Option type="QString" name="labeling/offsetType" value="0"/>
      <Option type="QString" name="labeling/placeDirectionSymbol" value="0"/>
      <Option type="QString" name="labeling/placement" value="6"/>
      <Option type="QString" name="labeling/placementFlags" value="10"/>
      <Option type="QString" name="labeling/plussign" value="false"/>
      <Option type="QString" name="labeling/predefinedPositionOrder" value="TR,TL,BR,BL,R,L,TSR,BSR"/>
      <Option type="QString" name="labeling/preserveRotation" value="true"/>
      <Option type="QString" name="labeling/previewBkgrdColor" value="#ffffff"/>
      <Option type="QString" name="labeling/priority" value="5"/>
      <Option type="QString" name="labeling/quadOffset" value="4"/>
      <Option type="QString" name="labeling/repeatDistance" value="0"/>
      <Option type="QString" name="labeling/repeatDistanceMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/repeatDistanceUnit" value="1"/>
      <Option type="QString" name="labeling/reverseDirectionSymbol" value="false"/>
      <Option type="QString" name="labeling/rightDirectionSymbol" value=">"/>
      <Option type="QString" name="labeling/scaleMax" value="10000000"/>
      <Option type="QString" name="labeling/scaleMin" value="1"/>
      <Option type="QString" name="labeling/scaleVisibility" value="false"/>
      <Option type="QString" name="labeling/shadowBlendMode" value="6"/>
      <Option type="QString" name="labeling/shadowColorB" value="0"/>
      <Option type="QString" name="labeling/shadowColorG" value="0"/>
      <Option type="QString" name="labeling/shadowColorR" value="0"/>
      <Option type="QString" name="labeling/shadowDraw" value="false"/>
      <Option type="QString" name="labeling/shadowOffsetAngle" value="135"/>
      <Option type="QString" name="labeling/shadowOffsetDist" value="1"/>
      <Option type="QString" name="labeling/shadowOffsetGlobal" value="true"/>
      <Option type="QString" name="labeling/shadowOffsetMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shadowOffsetUnits" value="1"/>
      <Option type="QString" name="labeling/shadowRadius" value="1.5"/>
      <Option type="QString" name="labeling/shadowRadiusAlphaOnly" value="false"/>
      <Option type="QString" name="labeling/shadowRadiusMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shadowRadiusUnits" value="1"/>
      <Option type="QString" name="labeling/shadowScale" value="100"/>
      <Option type="QString" name="labeling/shadowTransparency" value="30"/>
      <Option type="QString" name="labeling/shadowUnder" value="0"/>
      <Option type="QString" name="labeling/shapeBlendMode" value="0"/>
      <Option type="QString" name="labeling/shapeBorderColorA" value="255"/>
      <Option type="QString" name="labeling/shapeBorderColorB" value="128"/>
      <Option type="QString" name="labeling/shapeBorderColorG" value="128"/>
      <Option type="QString" name="labeling/shapeBorderColorR" value="128"/>
      <Option type="QString" name="labeling/shapeBorderWidth" value="0"/>
      <Option type="QString" name="labeling/shapeBorderWidthMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeBorderWidthUnits" value="1"/>
      <Option type="QString" name="labeling/shapeDraw" value="false"/>
      <Option type="QString" name="labeling/shapeFillColorA" value="255"/>
      <Option type="QString" name="labeling/shapeFillColorB" value="255"/>
      <Option type="QString" name="labeling/shapeFillColorG" value="255"/>
      <Option type="QString" name="labeling/shapeFillColorR" value="255"/>
      <Option type="QString" name="labeling/shapeJoinStyle" value="64"/>
      <Option type="QString" name="labeling/shapeOffsetMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeOffsetUnits" value="1"/>
      <Option type="QString" name="labeling/shapeOffsetX" value="0"/>
      <Option type="QString" name="labeling/shapeOffsetY" value="0"/>
      <Option type="QString" name="labeling/shapeRadiiMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeRadiiUnits" value="1"/>
      <Option type="QString" name="labeling/shapeRadiiX" value="0"/>
      <Option type="QString" name="labeling/shapeRadiiY" value="0"/>
      <Option type="QString" name="labeling/shapeRotation" value="0"/>
      <Option type="QString" name="labeling/shapeRotationType" value="0"/>
      <Option type="QString" name="labeling/shapeSVGFile" value=""/>
      <Option type="QString" name="labeling/shapeSizeMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeSizeType" value="0"/>
      <Option type="QString" name="labeling/shapeSizeUnits" value="1"/>
      <Option type="QString" name="labeling/shapeSizeX" value="0"/>
      <Option type="QString" name="labeling/shapeSizeY" value="0"/>
      <Option type="QString" name="labeling/shapeTransparency" value="0"/>
      <Option type="QString" name="labeling/shapeType" value="0"/>
      <Option type="QString" name="labeling/substitutions" value="&lt;substitutions/>"/>
      <Option type="QString" name="labeling/textColorA" value="255"/>
      <Option type="QString" name="labeling/textColorB" value="1"/>
      <Option type="QString" name="labeling/textColorG" value="240"/>
      <Option type="QString" name="labeling/textColorR" value="252"/>
      <Option type="QString" name="labeling/textTransp" value="0"/>
      <Option type="QString" name="labeling/upsidedownLabels" value="0"/>
      <Option type="QString" name="labeling/useSubstitutions" value="false"/>
      <Option type="QString" name="labeling/wrapChar" value=""/>
      <Option type="QString" name="labeling/xOffset" value="0"/>
      <Option type="QString" name="labeling/yOffset" value="0"/>
      <Option type="QString" name="labeling/zIndex" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory minScaleDenominator="1" backgroundAlpha="255" enabled="0" maxScaleDenominator="1e+08" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" diagramOrientation="Up" penWidth="0" direction="1" spacingUnit="MM" scaleBasedVisibility="0" scaleDependency="Area" height="15" spacingUnitScale="3x:0,0,0,0,0,0" sizeType="MM" rotationOffset="270" penColor="#000000" opacity="1" labelPlacementMethod="XHeight" barWidth="5" backgroundColor="#ffffff" lineSizeScale="3x:0,0,0,0,0,0" penAlpha="255" width="15" showAxis="0" spacing="0">
      <fontProperties underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" italic="0"/>
      <attribute label="" color="#000000" colorOpacity="1" field=""/>
      <axisSymbol>
        <symbol frame_rate="10" is_animated="0" type="line" clip_to_extent="1" name="" force_rhr="0" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" id="{1aaa47c0-ed29-4e0e-8e20-91197653a62e}" enabled="1" locked="0" class="SimpleLine">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" dist="0" linePlacementFlags="18" showAll="1" priority="0" placement="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_support">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_foyer">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_depart">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_armoire">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Actif" value="Actif"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="hauteur_feu">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="marque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type_vasque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="classe_electrique">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="degre_protection">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="resistance_choc">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="num_phase">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="1" value="1"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="2" value="2"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="3" value="3"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="varistance">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ral">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cellule">
      <editWidget type="UniqueValues">
        <config>
          <Option type="Map">
            <Option type="bool" name="Editable" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_pose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="dd/MM/yyyy"/>
            <Option type="QString" name="field_format" value="yyyy-MM-dd"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_depose">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_action">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="desc_action">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="remarque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mslink">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mapid">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ecp_intervention_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acteur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="uid4">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="created_at">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="updated_at">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pkid">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intervention">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rot_z">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photo">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option type="QString" name="DefaultRoot" value="P:/SIG/documents/photos"/>
            <Option type="int" name="DocumentViewer" value="1"/>
            <Option type="int" name="DocumentViewerHeight" value="0"/>
            <Option type="int" name="DocumentViewerWidth" value="0"/>
            <Option type="bool" name="FileWidget" value="false"/>
            <Option type="bool" name="FileWidgetButton" value="false"/>
            <Option type="QString" name="FileWidgetFilter" value=""/>
            <Option type="Map" name="PropertyCollection">
              <Option type="QString" name="name" value=""/>
              <Option type="invalid" name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
            <Option type="int" name="RelativeStorage" value="1"/>
            <Option type="int" name="StorageMode" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="fid"/>
    <alias index="1" name="" field="numero"/>
    <alias index="2" name="numéro insee commune" field="insee"/>
    <alias index="3" name="Identifiant(s) support(s)" field="id_support"/>
    <alias index="4" name="Identifiant(s) foyer(s)" field="id_foyer"/>
    <alias index="5" name="Identifiant départ" field="id_depart"/>
    <alias index="6" name="Identifiant armoire" field="id_armoire"/>
    <alias index="7" name="état" field="etat"/>
    <alias index="8" name="Hauteur de feu" field="hauteur_feu"/>
    <alias index="9" name="Marque " field="marque"/>
    <alias index="10" name="Type de vasque" field="type_vasque"/>
    <alias index="11" name="" field="classe_electrique"/>
    <alias index="12" name="" field="degre_protection"/>
    <alias index="13" name="" field="resistance_choc"/>
    <alias index="14" name="numéro de phase" field="num_phase"/>
    <alias index="15" name="" field="varistance"/>
    <alias index="16" name="RAL" field="ral"/>
    <alias index="17" name="" field="cellule"/>
    <alias index="18" name="Date de pose" field="date_pose"/>
    <alias index="19" name="Date de dépose" field="date_depose"/>
    <alias index="20" name="" field="date_action"/>
    <alias index="21" name="" field="desc_action"/>
    <alias index="22" name="Remarque" field="remarque"/>
    <alias index="23" name="" field="mslink"/>
    <alias index="24" name="" field="mapid"/>
    <alias index="25" name="" field="ecp_intervention_id"/>
    <alias index="26" name="chargés d'affaires" field="acteur"/>
    <alias index="27" name="" field="uid4"/>
    <alias index="28" name="" field="created_at"/>
    <alias index="29" name="" field="updated_at"/>
    <alias index="30" name="" field="pkid"/>
    <alias index="31" name="numéro d'affaires" field="intervention"/>
    <alias index="32" name="" field="rot_z"/>
    <alias index="33" name="Vue sur façade" field="photo"/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="fid"/>
    <policy policy="Duplicate" field="numero"/>
    <policy policy="Duplicate" field="insee"/>
    <policy policy="Duplicate" field="id_support"/>
    <policy policy="Duplicate" field="id_foyer"/>
    <policy policy="Duplicate" field="id_depart"/>
    <policy policy="Duplicate" field="id_armoire"/>
    <policy policy="Duplicate" field="etat"/>
    <policy policy="Duplicate" field="hauteur_feu"/>
    <policy policy="Duplicate" field="marque"/>
    <policy policy="Duplicate" field="type_vasque"/>
    <policy policy="Duplicate" field="classe_electrique"/>
    <policy policy="Duplicate" field="degre_protection"/>
    <policy policy="Duplicate" field="resistance_choc"/>
    <policy policy="Duplicate" field="num_phase"/>
    <policy policy="Duplicate" field="varistance"/>
    <policy policy="Duplicate" field="ral"/>
    <policy policy="Duplicate" field="cellule"/>
    <policy policy="Duplicate" field="date_pose"/>
    <policy policy="Duplicate" field="date_depose"/>
    <policy policy="Duplicate" field="date_action"/>
    <policy policy="Duplicate" field="desc_action"/>
    <policy policy="Duplicate" field="remarque"/>
    <policy policy="Duplicate" field="mslink"/>
    <policy policy="Duplicate" field="mapid"/>
    <policy policy="Duplicate" field="ecp_intervention_id"/>
    <policy policy="Duplicate" field="acteur"/>
    <policy policy="Duplicate" field="uid4"/>
    <policy policy="Duplicate" field="created_at"/>
    <policy policy="Duplicate" field="updated_at"/>
    <policy policy="Duplicate" field="pkid"/>
    <policy policy="Duplicate" field="intervention"/>
    <policy policy="Duplicate" field="rot_z"/>
    <policy policy="Duplicate" field="photo"/>
  </splitPolicies>
  <defaults>
    <default expression="" applyOnUpdate="0" field="fid"/>
    <default expression="" applyOnUpdate="0" field="numero"/>
    <default expression=" @lumigis_insee " applyOnUpdate="1" field="insee"/>
    <default expression="" applyOnUpdate="0" field="id_support"/>
    <default expression="" applyOnUpdate="0" field="id_foyer"/>
    <default expression="" applyOnUpdate="0" field="id_depart"/>
    <default expression="" applyOnUpdate="0" field="id_armoire"/>
    <default expression="" applyOnUpdate="0" field="etat"/>
    <default expression="" applyOnUpdate="0" field="hauteur_feu"/>
    <default expression="" applyOnUpdate="0" field="marque"/>
    <default expression="" applyOnUpdate="0" field="type_vasque"/>
    <default expression="" applyOnUpdate="0" field="classe_electrique"/>
    <default expression="" applyOnUpdate="0" field="degre_protection"/>
    <default expression="" applyOnUpdate="0" field="resistance_choc"/>
    <default expression="" applyOnUpdate="0" field="num_phase"/>
    <default expression="" applyOnUpdate="0" field="varistance"/>
    <default expression="" applyOnUpdate="0" field="ral"/>
    <default expression="" applyOnUpdate="0" field="cellule"/>
    <default expression="" applyOnUpdate="0" field="date_pose"/>
    <default expression="" applyOnUpdate="0" field="date_depose"/>
    <default expression="" applyOnUpdate="0" field="date_action"/>
    <default expression="" applyOnUpdate="0" field="desc_action"/>
    <default expression="" applyOnUpdate="0" field="remarque"/>
    <default expression="" applyOnUpdate="0" field="mslink"/>
    <default expression="" applyOnUpdate="0" field="mapid"/>
    <default expression="" applyOnUpdate="0" field="ecp_intervention_id"/>
    <default expression=" @lumigis_technicien " applyOnUpdate="1" field="acteur"/>
    <default expression="" applyOnUpdate="0" field="uid4"/>
    <default expression="" applyOnUpdate="0" field="created_at"/>
    <default expression="" applyOnUpdate="0" field="updated_at"/>
    <default expression="" applyOnUpdate="0" field="pkid"/>
    <default expression=" @lumigis_intervention " applyOnUpdate="1" field="intervention"/>
    <default expression="" applyOnUpdate="0" field="rot_z"/>
    <default expression="" applyOnUpdate="0" field="photo"/>
  </defaults>
  <constraints>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" unique_strength="1" field="fid"/>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" unique_strength="1" field="numero"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="insee"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="id_support"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="id_foyer"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="id_depart"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="id_armoire"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="etat"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="hauteur_feu"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="marque"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="type_vasque"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="classe_electrique"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="degre_protection"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="resistance_choc"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="num_phase"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="varistance"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="ral"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="cellule"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_pose"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_depose"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_action"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="desc_action"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="remarque"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="mslink"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="mapid"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="ecp_intervention_id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="acteur"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="uid4"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="created_at"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="updated_at"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="pkid"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="intervention"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="rot_z"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="photo"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" exp="" desc=""/>
    <constraint field="numero" exp="" desc=""/>
    <constraint field="insee" exp="" desc=""/>
    <constraint field="id_support" exp="" desc=""/>
    <constraint field="id_foyer" exp="" desc=""/>
    <constraint field="id_depart" exp="" desc=""/>
    <constraint field="id_armoire" exp="" desc=""/>
    <constraint field="etat" exp="" desc=""/>
    <constraint field="hauteur_feu" exp="" desc=""/>
    <constraint field="marque" exp="" desc=""/>
    <constraint field="type_vasque" exp="" desc=""/>
    <constraint field="classe_electrique" exp="" desc=""/>
    <constraint field="degre_protection" exp="" desc=""/>
    <constraint field="resistance_choc" exp="" desc=""/>
    <constraint field="num_phase" exp="" desc=""/>
    <constraint field="varistance" exp="" desc=""/>
    <constraint field="ral" exp="" desc=""/>
    <constraint field="cellule" exp="" desc=""/>
    <constraint field="date_pose" exp="" desc=""/>
    <constraint field="date_depose" exp="" desc=""/>
    <constraint field="date_action" exp="" desc=""/>
    <constraint field="desc_action" exp="" desc=""/>
    <constraint field="remarque" exp="" desc=""/>
    <constraint field="mslink" exp="" desc=""/>
    <constraint field="mapid" exp="" desc=""/>
    <constraint field="ecp_intervention_id" exp="" desc=""/>
    <constraint field="acteur" exp="" desc=""/>
    <constraint field="uid4" exp="" desc=""/>
    <constraint field="created_at" exp="" desc=""/>
    <constraint field="updated_at" exp="" desc=""/>
    <constraint field="pkid" exp="" desc=""/>
    <constraint field="intervention" exp="" desc=""/>
    <constraint field="rot_z" exp="" desc=""/>
    <constraint field="photo" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
    <actionsetting action="[%photo%]" type="5" id="{d4275832-bedf-4526-89d8-927c85354d7e}" name="photos" notificationMessage="" shortTitle="photos " icon="P:/SIG/gestion/svg/elts_divers/icon_photo.svg" isEnabledOnlyWhenEditable="0" capture="0">
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;cellule&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" name="insee" width="-1" hidden="0"/>
      <column type="field" name="id_armoire" width="-1" hidden="0"/>
      <column type="field" name="id_depart" width="-1" hidden="0"/>
      <column type="field" name="id_foyer" width="-1" hidden="0"/>
      <column type="field" name="id_support" width="-1" hidden="0"/>
      <column type="field" name="etat" width="-1" hidden="0"/>
      <column type="field" name="marque" width="183" hidden="0"/>
      <column type="field" name="type_vasque" width="-1" hidden="0"/>
      <column type="field" name="ral" width="-1" hidden="0"/>
      <column type="field" name="hauteur_feu" width="-1" hidden="0"/>
      <column type="field" name="cellule" width="-1" hidden="0"/>
      <column type="field" name="date_pose" width="-1" hidden="0"/>
      <column type="field" name="date_depose" width="-1" hidden="0"/>
      <column type="field" name="acteur" width="-1" hidden="0"/>
      <column type="field" name="mslink" width="-1" hidden="0"/>
      <column type="field" name="mapid" width="-1" hidden="0"/>
      <column type="field" name="created_at" width="-1" hidden="0"/>
      <column type="field" name="updated_at" width="190" hidden="0"/>
      <column type="field" name="remarque" width="-1" hidden="0"/>
      <column type="field" name="rot_z" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" name="num_phase" width="-1" hidden="0"/>
      <column type="field" name="varistance" width="-1" hidden="0"/>
      <column type="field" name="date_action" width="-1" hidden="0"/>
      <column type="field" name="photo" width="278" hidden="0"/>
      <column type="field" name="uid4" width="-1" hidden="0"/>
      <column type="field" name="numero" width="-1" hidden="0"/>
      <column type="field" name="classe_electrique" width="-1" hidden="0"/>
      <column type="field" name="degre_protection" width="-1" hidden="0"/>
      <column type="field" name="resistance_choc" width="-1" hidden="0"/>
      <column type="field" name="desc_action" width="-1" hidden="0"/>
      <column type="field" name="ecp_intervention_id" width="-1" hidden="0"/>
      <column type="field" name="pkid" width="-1" hidden="0"/>
      <column type="field" name="fid" width="-1" hidden="0"/>
      <column type="field" name="intervention" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">P:/SIG/gestion</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>P:/SIG/gestion</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
      <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
    </labelStyle>
    <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Fiche descriptive" visibilityExpression="" groupBox="0">
      <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
        <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      </labelStyle>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Généralités" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="31" showLabel="1" name="intervention">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="26" showLabel="1" name="acteur">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="2" showLabel="1" name="insee">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Identification" visibilityExpression="&quot;id&quot;" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="6" showLabel="1" name="id_armoire">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="5" showLabel="1" name="id_depart">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="3" showLabel="1" name="id_support">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Etat" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="4" showLabel="1" name="id_foyer">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="7" showLabel="1" name="etat">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="14" showLabel="1" name="num_phase">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="18" showLabel="1" name="date_pose">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Caractéristiques techniques" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="9" showLabel="1" name="marque">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="10" showLabel="1" name="type_vasque">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="16" showLabel="1" name="ral">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="8" showLabel="1" name="hauteur_feu">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="10" showLabel="1" name="type_vasque">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Remarques" visibilityExpression="" groupBox="0">
      <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
        <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      </labelStyle>
      <attributeEditorField index="22" showLabel="1" name="remarque">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="acteur"/>
    <field editable="1" name="cellule"/>
    <field editable="1" name="classe_elec"/>
    <field editable="1" name="classe_electrique"/>
    <field editable="1" name="created_at"/>
    <field editable="1" name="date_action"/>
    <field editable="1" name="date_depose"/>
    <field editable="1" name="date_intervention"/>
    <field editable="1" name="date_pose"/>
    <field editable="1" name="degre_protection"/>
    <field editable="1" name="desc_action"/>
    <field editable="1" name="ecp_intervention_id"/>
    <field editable="0" name="entreprises maintenance 2019/2022adherent"/>
    <field editable="0" name="entreprises maintenance 2019/2022entreprise"/>
    <field editable="0" name="entreprises maintenance 2019/2022lot"/>
    <field editable="1" name="etat"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="hauteur_feu"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_armoire"/>
    <field editable="1" name="id_depart"/>
    <field editable="1" name="id_foyer"/>
    <field editable="1" name="id_intervention"/>
    <field editable="1" name="id_support"/>
    <field editable="1" name="insee"/>
    <field editable="1" name="intervention"/>
    <field editable="1" name="mapid"/>
    <field editable="1" name="marque"/>
    <field editable="1" name="mslink"/>
    <field editable="1" name="num_phase"/>
    <field editable="1" name="numero"/>
    <field editable="1" name="photo"/>
    <field editable="1" name="pkid"/>
    <field editable="1" name="protection_ip"/>
    <field editable="1" name="ral"/>
    <field editable="1" name="remarque"/>
    <field editable="1" name="resistance_choc"/>
    <field editable="1" name="resistance_ik"/>
    <field editable="1" name="rot_z"/>
    <field editable="0" name="sources_appareillage"/>
    <field editable="0" name="sources_culot"/>
    <field editable="0" name="sources_date_pose"/>
    <field editable="0" name="sources_etat"/>
    <field editable="0" name="sources_id_foyer"/>
    <field editable="0" name="sources_insee"/>
    <field editable="0" name="sources_localisation_appareillage"/>
    <field editable="0" name="sources_nature"/>
    <field editable="0" name="sources_nature_source"/>
    <field editable="0" name="sources_numero"/>
    <field editable="0" name="sources_photometrie"/>
    <field editable="0" name="sources_puissance"/>
    <field editable="0" name="sources_remarque"/>
    <field editable="0" name="sources_temperature_couleur"/>
    <field editable="1" name="type_vasque"/>
    <field editable="1" name="uid4"/>
    <field editable="1" name="updated_at"/>
    <field editable="1" name="varistance"/>
    <field editable="0" name="vue_sources_acteur"/>
    <field editable="0" name="vue_sources_appareillage"/>
    <field editable="0" name="vue_sources_created_at"/>
    <field editable="0" name="vue_sources_culot"/>
    <field editable="0" name="vue_sources_date_intervention"/>
    <field editable="0" name="vue_sources_date_pose"/>
    <field editable="0" name="vue_sources_etat"/>
    <field editable="0" name="vue_sources_id"/>
    <field editable="0" name="vue_sources_id_foyer"/>
    <field editable="0" name="vue_sources_id_intervention"/>
    <field editable="0" name="vue_sources_insee"/>
    <field editable="0" name="vue_sources_localisation_appareillage"/>
    <field editable="0" name="vue_sources_nature_source"/>
    <field editable="0" name="vue_sources_photometrie"/>
    <field editable="0" name="vue_sources_puissance"/>
    <field editable="0" name="vue_sources_remarque"/>
    <field editable="0" name="vue_sources_temperature_couleur"/>
    <field editable="0" name="vue_sources_updated_at"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="1" name="acteur"/>
    <field labelOnTop="0" name="cellule"/>
    <field labelOnTop="0" name="classe_elec"/>
    <field labelOnTop="0" name="classe_electrique"/>
    <field labelOnTop="0" name="created_at"/>
    <field labelOnTop="0" name="date_action"/>
    <field labelOnTop="1" name="date_depose"/>
    <field labelOnTop="0" name="date_intervention"/>
    <field labelOnTop="1" name="date_pose"/>
    <field labelOnTop="0" name="degre_protection"/>
    <field labelOnTop="0" name="desc_action"/>
    <field labelOnTop="0" name="ecp_intervention_id"/>
    <field labelOnTop="0" name="entreprises maintenance 2019/2022adherent"/>
    <field labelOnTop="0" name="entreprises maintenance 2019/2022entreprise"/>
    <field labelOnTop="0" name="entreprises maintenance 2019/2022lot"/>
    <field labelOnTop="1" name="etat"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="1" name="hauteur_feu"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="1" name="id_armoire"/>
    <field labelOnTop="1" name="id_depart"/>
    <field labelOnTop="1" name="id_foyer"/>
    <field labelOnTop="0" name="id_intervention"/>
    <field labelOnTop="1" name="id_support"/>
    <field labelOnTop="1" name="insee"/>
    <field labelOnTop="1" name="intervention"/>
    <field labelOnTop="0" name="mapid"/>
    <field labelOnTop="1" name="marque"/>
    <field labelOnTop="0" name="mslink"/>
    <field labelOnTop="1" name="num_phase"/>
    <field labelOnTop="0" name="numero"/>
    <field labelOnTop="1" name="photo"/>
    <field labelOnTop="0" name="pkid"/>
    <field labelOnTop="0" name="protection_ip"/>
    <field labelOnTop="1" name="ral"/>
    <field labelOnTop="1" name="remarque"/>
    <field labelOnTop="0" name="resistance_choc"/>
    <field labelOnTop="0" name="resistance_ik"/>
    <field labelOnTop="0" name="rot_z"/>
    <field labelOnTop="0" name="sources_appareillage"/>
    <field labelOnTop="0" name="sources_culot"/>
    <field labelOnTop="0" name="sources_date_pose"/>
    <field labelOnTop="0" name="sources_etat"/>
    <field labelOnTop="0" name="sources_id_foyer"/>
    <field labelOnTop="0" name="sources_insee"/>
    <field labelOnTop="0" name="sources_localisation_appareillage"/>
    <field labelOnTop="0" name="sources_nature"/>
    <field labelOnTop="0" name="sources_nature_source"/>
    <field labelOnTop="0" name="sources_numero"/>
    <field labelOnTop="0" name="sources_photometrie"/>
    <field labelOnTop="0" name="sources_puissance"/>
    <field labelOnTop="0" name="sources_remarque"/>
    <field labelOnTop="0" name="sources_temperature_couleur"/>
    <field labelOnTop="1" name="type_vasque"/>
    <field labelOnTop="0" name="uid4"/>
    <field labelOnTop="0" name="updated_at"/>
    <field labelOnTop="0" name="varistance"/>
    <field labelOnTop="0" name="vue_sources_acteur"/>
    <field labelOnTop="0" name="vue_sources_appareillage"/>
    <field labelOnTop="0" name="vue_sources_created_at"/>
    <field labelOnTop="0" name="vue_sources_culot"/>
    <field labelOnTop="0" name="vue_sources_date_intervention"/>
    <field labelOnTop="0" name="vue_sources_date_pose"/>
    <field labelOnTop="0" name="vue_sources_etat"/>
    <field labelOnTop="0" name="vue_sources_id"/>
    <field labelOnTop="0" name="vue_sources_id_foyer"/>
    <field labelOnTop="0" name="vue_sources_id_intervention"/>
    <field labelOnTop="0" name="vue_sources_insee"/>
    <field labelOnTop="0" name="vue_sources_localisation_appareillage"/>
    <field labelOnTop="0" name="vue_sources_nature_source"/>
    <field labelOnTop="0" name="vue_sources_photometrie"/>
    <field labelOnTop="0" name="vue_sources_puissance"/>
    <field labelOnTop="0" name="vue_sources_remarque"/>
    <field labelOnTop="0" name="vue_sources_temperature_couleur"/>
    <field labelOnTop="0" name="vue_sources_updated_at"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="acteur" reuseLastValue="0"/>
    <field name="cellule" reuseLastValue="0"/>
    <field name="classe_electrique" reuseLastValue="0"/>
    <field name="created_at" reuseLastValue="0"/>
    <field name="date_action" reuseLastValue="0"/>
    <field name="date_depose" reuseLastValue="0"/>
    <field name="date_pose" reuseLastValue="0"/>
    <field name="degre_protection" reuseLastValue="0"/>
    <field name="desc_action" reuseLastValue="0"/>
    <field name="ecp_intervention_id" reuseLastValue="0"/>
    <field name="etat" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="hauteur_feu" reuseLastValue="0"/>
    <field name="id_armoire" reuseLastValue="0"/>
    <field name="id_depart" reuseLastValue="0"/>
    <field name="id_foyer" reuseLastValue="0"/>
    <field name="id_support" reuseLastValue="0"/>
    <field name="insee" reuseLastValue="0"/>
    <field name="intervention" reuseLastValue="0"/>
    <field name="mapid" reuseLastValue="0"/>
    <field name="marque" reuseLastValue="0"/>
    <field name="mslink" reuseLastValue="0"/>
    <field name="num_phase" reuseLastValue="0"/>
    <field name="numero" reuseLastValue="0"/>
    <field name="photo" reuseLastValue="0"/>
    <field name="pkid" reuseLastValue="0"/>
    <field name="ral" reuseLastValue="0"/>
    <field name="remarque" reuseLastValue="0"/>
    <field name="resistance_choc" reuseLastValue="0"/>
    <field name="rot_z" reuseLastValue="0"/>
    <field name="type_vasque" reuseLastValue="0"/>
    <field name="uid4" reuseLastValue="0"/>
    <field name="updated_at" reuseLastValue="0"/>
    <field name="varistance" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="intervention">
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option type="Map" name="properties">
          <Option type="Map" name="dataDefinedAlias">
            <Option type="bool" name="active" value="false"/>
            <Option type="int" name="type" value="1"/>
            <Option type="QString" name="val" value=""/>
          </Option>
        </Option>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>COALESCE( "id_foyer", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
