<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" readOnly="0" minScale="1e+08" version="3.30.0-'s-Hertogenbosch" hasScaleBasedVisibilityFlag="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" fixedDuration="0" endField="" durationField="" accumulate="0" startField="" startExpression="" enabled="0" mode="0" endExpression="" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation showMarkerSymbolInSurfacePlots="0" respectLayerSymbol="1" zoffset="0" binding="Centroid" extrusion="0" clamping="Terrain" symbology="Line" type="IndividualFeatures" zscale="1" extrusionEnabled="0">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" force_rhr="0" clip_to_extent="1" is_animated="0" type="line" name="" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" id="{5b63e585-deb7-4bb7-85c1-4d63a9fd721d}" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="196,60,57,255" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.6" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" force_rhr="0" clip_to_extent="1" is_animated="0" type="fill" name="" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{79c87fde-bc5a-4aea-a065-7e50e676bc54}" enabled="1" locked="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="196,60,57,255" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="140,43,41,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="solid" type="QString" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" force_rhr="0" clip_to_extent="1" is_animated="0" type="marker" name="" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{02b842e0-eb15-4fe7-b492-2326c303e302}" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="196,60,57,255" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="diamond" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="140,43,41,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="3" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <customproperties>
    <Option type="Map">
      <Option value="remove" type="QString" name="QFieldSync/action"/>
      <Option value="{}" type="QString" name="QFieldSync/photo_naming"/>
      <Option type="List" name="dualview/previewExpressions">
        <Option value="&quot;id_armoire&quot;" type="QString"/>
      </Option>
      <Option value="0" type="QString" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="insee">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_armoire">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_depart">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="A déposer" type="QString" name="A déposer"/>
              </Option>
              <Option type="Map">
                <Option value="A poser" type="QString" name="A poser"/>
              </Option>
              <Option type="Map">
                <Option value="A remplacer" type="QString" name="A remplacer"/>
              </Option>
              <Option type="Map">
                <Option value="Actif" type="QString" name="Actif"/>
              </Option>
              <Option type="Map">
                <Option value="Déposé" type="QString" name="Déposé"/>
              </Option>
              <Option type="Map">
                <Option value="Hors contrat" type="QString" name="Hors contrat"/>
              </Option>
              <Option type="Map">
                <Option value="Inactif" type="QString" name="Inactif"/>
              </Option>
              <Option type="Map">
                <Option value="Virtuel" type="QString" name="Virtuel"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nature_depart">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Monophasé" type="QString" name="Monophasé"/>
              </Option>
              <Option type="Map">
                <Option value="Triphasé" type="QString" name="Triphasé"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="iphase1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="iphase2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="iphase3">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="designation">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Autre" type="QString" name="Autre"/>
              </Option>
              <Option type="Map">
                <Option value="Illuminations" type="QString" name="Illuminations"/>
              </Option>
              <Option type="Map">
                <Option value="éclairage public" type="QString" name="éclairage public"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nature_protection">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Disjoncteur" type="QString" name="Disjoncteur"/>
              </Option>
              <Option type="Map">
                <Option value="Disjoncteur Différentiel" type="QString" name="Disjoncteur Différentiel"/>
              </Option>
              <Option type="Map">
                <Option value="Fusible" type="QString" name="Fusible"/>
              </Option>
              <Option type="Map">
                <Option value="interdifférentiel et fusible" type="QString" name="interdifférentiel et fusible"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="calibrage_protection">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="depart_sens_diff">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="30" type="QString" name="30"/>
              </Option>
              <Option type="Map">
                <Option value="300" type="QString" name="300"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="heure_allumage">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="HH:mm:ss" type="QString" name="display_format"/>
            <Option value="HH:mm:ss" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="heure_extinction">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="HH:mm:ss" type="QString" name="display_format"/>
            <Option value="HH:mm:ss" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regime_allumage">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Autre" type="QString" name="Autre"/>
              </Option>
              <Option type="Map">
                <Option value="Gradation" type="QString" name="Gradation"/>
              </Option>
              <Option type="Map">
                <Option value="Permanent" type="QString" name="Permanent"/>
              </Option>
              <Option type="Map">
                <Option value="Semi-permanent" type="QString" name="Semi-permanent"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pilote">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="remarque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mslink">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mapid">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ecp_intervention_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acteur">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Anthony Louis-Villemin" type="QString" name="Anthony Louis-Villemin"/>
              </Option>
              <Option type="Map">
                <Option value="Bruno Idoux" type="QString" name="Bruno Idoux"/>
              </Option>
              <Option type="Map">
                <Option value="Damien Jacquot" type="QString" name="Damien Jacquot"/>
              </Option>
              <Option type="Map">
                <Option value="Julien Pinot" type="QString" name="Julien Pinot"/>
              </Option>
              <Option type="Map">
                <Option value="Morgan Fontaine" type="QString" name="Morgan Fontaine"/>
              </Option>
              <Option type="Map">
                <Option value="Sophie Massonneau" type="QString" name="Sophie Massonneau"/>
              </Option>
              <Option type="Map">
                <Option value="Xavier Schuler" type="QString" name="Xavier Schuler"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="uid4">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="created_at">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="updated_at">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pkid">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intervention">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="fid" index="0" name=""/>
    <alias field="numero" index="1" name="Numéro"/>
    <alias field="insee" index="2" name="Insee commune"/>
    <alias field="id_armoire" index="3" name="Identifiant armoire"/>
    <alias field="id_depart" index="4" name="Identifiant départ"/>
    <alias field="etat" index="5" name="Etat"/>
    <alias field="nature_depart" index="6" name="Nature départ"/>
    <alias field="iphase1" index="7" name="Intensité phase 1 - neutre"/>
    <alias field="iphase2" index="8" name="Intensité phase 2 - neutre"/>
    <alias field="iphase3" index="9" name="Intensité phase 3 - neutre"/>
    <alias field="designation" index="10" name="Désignation"/>
    <alias field="nature_protection" index="11" name="Nature de la protection (fusible, interdifférentiel etc ....) en pied de mât"/>
    <alias field="calibrage_protection" index="12" name="Calibre de la protection (fusible, disjoncteur etc...) "/>
    <alias field="depart_sens_diff" index="13" name="Sensibilité du différentiel en mA"/>
    <alias field="heure_allumage" index="14" name="Heure allumage"/>
    <alias field="heure_extinction" index="15" name="Heure extinction"/>
    <alias field="regime_allumage" index="16" name="Régime allumage"/>
    <alias field="pilote" index="17" name="pilote"/>
    <alias field="remarque" index="18" name="remarque"/>
    <alias field="mslink" index="19" name=""/>
    <alias field="mapid" index="20" name=""/>
    <alias field="ecp_intervention_id" index="21" name="Numéro d'intervention (interne)"/>
    <alias field="acteur" index="22" name="Chargé d'affaire"/>
    <alias field="uid4" index="23" name=""/>
    <alias field="created_at" index="24" name=""/>
    <alias field="updated_at" index="25" name=""/>
    <alias field="pkid" index="26" name=""/>
    <alias field="intervention" index="27" name="Intervention"/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="fid"/>
    <policy policy="Duplicate" field="numero"/>
    <policy policy="Duplicate" field="insee"/>
    <policy policy="Duplicate" field="id_armoire"/>
    <policy policy="Duplicate" field="id_depart"/>
    <policy policy="Duplicate" field="etat"/>
    <policy policy="Duplicate" field="nature_depart"/>
    <policy policy="Duplicate" field="iphase1"/>
    <policy policy="Duplicate" field="iphase2"/>
    <policy policy="Duplicate" field="iphase3"/>
    <policy policy="Duplicate" field="designation"/>
    <policy policy="Duplicate" field="nature_protection"/>
    <policy policy="Duplicate" field="calibrage_protection"/>
    <policy policy="Duplicate" field="depart_sens_diff"/>
    <policy policy="Duplicate" field="heure_allumage"/>
    <policy policy="Duplicate" field="heure_extinction"/>
    <policy policy="Duplicate" field="regime_allumage"/>
    <policy policy="Duplicate" field="pilote"/>
    <policy policy="Duplicate" field="remarque"/>
    <policy policy="Duplicate" field="mslink"/>
    <policy policy="Duplicate" field="mapid"/>
    <policy policy="Duplicate" field="ecp_intervention_id"/>
    <policy policy="Duplicate" field="acteur"/>
    <policy policy="Duplicate" field="uid4"/>
    <policy policy="Duplicate" field="created_at"/>
    <policy policy="Duplicate" field="updated_at"/>
    <policy policy="Duplicate" field="pkid"/>
    <policy policy="Duplicate" field="intervention"/>
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" field="fid" expression=""/>
    <default applyOnUpdate="0" field="numero" expression=""/>
    <default applyOnUpdate="0" field="insee" expression=""/>
    <default applyOnUpdate="0" field="id_armoire" expression=""/>
    <default applyOnUpdate="0" field="id_depart" expression=""/>
    <default applyOnUpdate="0" field="etat" expression=""/>
    <default applyOnUpdate="0" field="nature_depart" expression=""/>
    <default applyOnUpdate="0" field="iphase1" expression=""/>
    <default applyOnUpdate="0" field="iphase2" expression=""/>
    <default applyOnUpdate="0" field="iphase3" expression=""/>
    <default applyOnUpdate="0" field="designation" expression=""/>
    <default applyOnUpdate="0" field="nature_protection" expression=""/>
    <default applyOnUpdate="0" field="calibrage_protection" expression=""/>
    <default applyOnUpdate="0" field="depart_sens_diff" expression=""/>
    <default applyOnUpdate="0" field="heure_allumage" expression=""/>
    <default applyOnUpdate="0" field="heure_extinction" expression=""/>
    <default applyOnUpdate="0" field="regime_allumage" expression=""/>
    <default applyOnUpdate="0" field="pilote" expression=""/>
    <default applyOnUpdate="0" field="remarque" expression=""/>
    <default applyOnUpdate="0" field="mslink" expression=""/>
    <default applyOnUpdate="0" field="mapid" expression=""/>
    <default applyOnUpdate="0" field="ecp_intervention_id" expression=""/>
    <default applyOnUpdate="0" field="acteur" expression=""/>
    <default applyOnUpdate="0" field="uid4" expression=""/>
    <default applyOnUpdate="0" field="created_at" expression=""/>
    <default applyOnUpdate="0" field="updated_at" expression=""/>
    <default applyOnUpdate="0" field="pkid" expression=""/>
    <default applyOnUpdate="0" field="intervention" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" field="fid" constraints="3" exp_strength="0"/>
    <constraint unique_strength="1" notnull_strength="1" field="numero" constraints="3" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="insee" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="id_armoire" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="id_depart" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="etat" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="nature_depart" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="iphase1" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="iphase2" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="iphase3" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="designation" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="nature_protection" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="calibrage_protection" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="depart_sens_diff" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="heure_allumage" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="heure_extinction" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="regime_allumage" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="pilote" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="remarque" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="mslink" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="mapid" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="ecp_intervention_id" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="acteur" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="uid4" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="created_at" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="updated_at" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="pkid" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="intervention" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="fid" exp=""/>
    <constraint desc="" field="numero" exp=""/>
    <constraint desc="" field="insee" exp=""/>
    <constraint desc="" field="id_armoire" exp=""/>
    <constraint desc="" field="id_depart" exp=""/>
    <constraint desc="" field="etat" exp=""/>
    <constraint desc="" field="nature_depart" exp=""/>
    <constraint desc="" field="iphase1" exp=""/>
    <constraint desc="" field="iphase2" exp=""/>
    <constraint desc="" field="iphase3" exp=""/>
    <constraint desc="" field="designation" exp=""/>
    <constraint desc="" field="nature_protection" exp=""/>
    <constraint desc="" field="calibrage_protection" exp=""/>
    <constraint desc="" field="depart_sens_diff" exp=""/>
    <constraint desc="" field="heure_allumage" exp=""/>
    <constraint desc="" field="heure_extinction" exp=""/>
    <constraint desc="" field="regime_allumage" exp=""/>
    <constraint desc="" field="pilote" exp=""/>
    <constraint desc="" field="remarque" exp=""/>
    <constraint desc="" field="mslink" exp=""/>
    <constraint desc="" field="mapid" exp=""/>
    <constraint desc="" field="ecp_intervention_id" exp=""/>
    <constraint desc="" field="acteur" exp=""/>
    <constraint desc="" field="uid4" exp=""/>
    <constraint desc="" field="created_at" exp=""/>
    <constraint desc="" field="updated_at" exp=""/>
    <constraint desc="" field="pkid" exp=""/>
    <constraint desc="" field="intervention" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;numero&quot;" actionWidgetStyle="dropDown" sortOrder="1">
    <columns>
      <column hidden="0" width="-1" type="field" name="numero"/>
      <column hidden="0" width="-1" type="field" name="insee"/>
      <column hidden="0" width="-1" type="field" name="id_armoire"/>
      <column hidden="0" width="-1" type="field" name="id_depart"/>
      <column hidden="0" width="-1" type="field" name="etat"/>
      <column hidden="0" width="-1" type="field" name="nature_depart"/>
      <column hidden="0" width="-1" type="field" name="iphase1"/>
      <column hidden="0" width="-1" type="field" name="iphase2"/>
      <column hidden="0" width="-1" type="field" name="iphase3"/>
      <column hidden="0" width="-1" type="field" name="designation"/>
      <column hidden="0" width="-1" type="field" name="nature_protection"/>
      <column hidden="0" width="-1" type="field" name="calibrage_protection"/>
      <column hidden="0" width="-1" type="field" name="depart_sens_diff"/>
      <column hidden="0" width="-1" type="field" name="heure_allumage"/>
      <column hidden="0" width="-1" type="field" name="heure_extinction"/>
      <column hidden="0" width="-1" type="field" name="regime_allumage"/>
      <column hidden="0" width="-1" type="field" name="pilote"/>
      <column hidden="0" width="-1" type="field" name="remarque"/>
      <column hidden="0" width="-1" type="field" name="mslink"/>
      <column hidden="0" width="-1" type="field" name="mapid"/>
      <column hidden="0" width="321" type="field" name="ecp_intervention_id"/>
      <column hidden="0" width="-1" type="field" name="acteur"/>
      <column hidden="0" width="-1" type="field" name="uid4"/>
      <column hidden="0" width="-1" type="field" name="created_at"/>
      <column hidden="0" width="-1" type="field" name="updated_at"/>
      <column hidden="0" width="-1" type="field" name="pkid"/>
      <column hidden="0" width="-1" type="field" name="intervention"/>
      <column hidden="0" width="-1" type="field" name="fid"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
      <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
    </labelStyle>
    <attributeEditorField index="2" name="insee" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="27" name="intervention" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="21" name="ecp_intervention_id" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="3" name="id_armoire" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="4" name="id_depart" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="5" name="etat" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="6" name="nature_depart" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="7" name="iphase1" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="8" name="iphase2" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="9" name="iphase3" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="10" name="designation" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="11" name="nature_protection" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="12" name="calibrage_protection" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="13" name="depart_sens_diff" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="14" name="heure_allumage" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="15" name="heure_extinction" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="16" name="regime_allumage" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="17" name="pilote" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="18" name="remarque" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="22" name="acteur" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="acteur"/>
    <field editable="1" name="calibrage_protection"/>
    <field editable="1" name="created_at"/>
    <field editable="1" name="depart_sens_diff"/>
    <field editable="1" name="designation"/>
    <field editable="1" name="ecp_intervention_id"/>
    <field editable="1" name="etat"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="heure_allumage"/>
    <field editable="1" name="heure_extinction"/>
    <field editable="1" name="id_armoire"/>
    <field editable="1" name="id_depart"/>
    <field editable="1" name="insee"/>
    <field editable="1" name="intervention"/>
    <field editable="1" name="iphase1"/>
    <field editable="1" name="iphase2"/>
    <field editable="1" name="iphase3"/>
    <field editable="1" name="mapid"/>
    <field editable="1" name="mslink"/>
    <field editable="1" name="nature_depart"/>
    <field editable="1" name="nature_protection"/>
    <field editable="1" name="numero"/>
    <field editable="1" name="pilote"/>
    <field editable="1" name="pkid"/>
    <field editable="1" name="regime_allumage"/>
    <field editable="1" name="remarque"/>
    <field editable="1" name="uid4"/>
    <field editable="1" name="updated_at"/>
  </editable>
  <labelOnTop>
    <field name="acteur" labelOnTop="1"/>
    <field name="calibrage_protection" labelOnTop="1"/>
    <field name="created_at" labelOnTop="0"/>
    <field name="depart_sens_diff" labelOnTop="1"/>
    <field name="designation" labelOnTop="1"/>
    <field name="ecp_intervention_id" labelOnTop="1"/>
    <field name="etat" labelOnTop="1"/>
    <field name="fid" labelOnTop="0"/>
    <field name="heure_allumage" labelOnTop="1"/>
    <field name="heure_extinction" labelOnTop="1"/>
    <field name="id_armoire" labelOnTop="1"/>
    <field name="id_depart" labelOnTop="1"/>
    <field name="insee" labelOnTop="1"/>
    <field name="intervention" labelOnTop="1"/>
    <field name="iphase1" labelOnTop="1"/>
    <field name="iphase2" labelOnTop="1"/>
    <field name="iphase3" labelOnTop="1"/>
    <field name="mapid" labelOnTop="0"/>
    <field name="mslink" labelOnTop="0"/>
    <field name="nature_depart" labelOnTop="1"/>
    <field name="nature_protection" labelOnTop="1"/>
    <field name="numero" labelOnTop="1"/>
    <field name="pilote" labelOnTop="1"/>
    <field name="pkid" labelOnTop="0"/>
    <field name="regime_allumage" labelOnTop="1"/>
    <field name="remarque" labelOnTop="0"/>
    <field name="uid4" labelOnTop="0"/>
    <field name="updated_at" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="acteur"/>
    <field reuseLastValue="0" name="calibrage_protection"/>
    <field reuseLastValue="0" name="created_at"/>
    <field reuseLastValue="0" name="depart_sens_diff"/>
    <field reuseLastValue="0" name="designation"/>
    <field reuseLastValue="0" name="ecp_intervention_id"/>
    <field reuseLastValue="0" name="etat"/>
    <field reuseLastValue="0" name="fid"/>
    <field reuseLastValue="0" name="heure_allumage"/>
    <field reuseLastValue="0" name="heure_extinction"/>
    <field reuseLastValue="0" name="id_armoire"/>
    <field reuseLastValue="0" name="id_depart"/>
    <field reuseLastValue="0" name="insee"/>
    <field reuseLastValue="0" name="intervention"/>
    <field reuseLastValue="0" name="iphase1"/>
    <field reuseLastValue="0" name="iphase2"/>
    <field reuseLastValue="0" name="iphase3"/>
    <field reuseLastValue="0" name="mapid"/>
    <field reuseLastValue="0" name="mslink"/>
    <field reuseLastValue="0" name="nature_depart"/>
    <field reuseLastValue="0" name="nature_protection"/>
    <field reuseLastValue="0" name="numero"/>
    <field reuseLastValue="0" name="pilote"/>
    <field reuseLastValue="0" name="pkid"/>
    <field reuseLastValue="0" name="regime_allumage"/>
    <field reuseLastValue="0" name="remarque"/>
    <field reuseLastValue="0" name="uid4"/>
    <field reuseLastValue="0" name="updated_at"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"id_armoire"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
