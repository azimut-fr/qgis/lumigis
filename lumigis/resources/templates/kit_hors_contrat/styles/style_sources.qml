<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" readOnly="0" minScale="1e+08" version="3.30.0-'s-Hertogenbosch" hasScaleBasedVisibilityFlag="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" fixedDuration="0" endField="" durationField="" accumulate="0" startField="" startExpression="" enabled="0" mode="0" endExpression="" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation showMarkerSymbolInSurfacePlots="0" respectLayerSymbol="1" zoffset="0" binding="Centroid" extrusion="0" clamping="Terrain" symbology="Line" type="IndividualFeatures" zscale="1" extrusionEnabled="0">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" force_rhr="0" clip_to_extent="1" is_animated="0" type="line" name="" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" id="{a8fc3e90-f1bf-4e2c-8ba5-1b9e770240dc}" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="225,89,137,255" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.6" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" force_rhr="0" clip_to_extent="1" is_animated="0" type="fill" name="" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" id="{d942a32b-44ca-44a5-933b-3682fd68aa13}" enabled="1" locked="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="225,89,137,255" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="161,64,98,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="solid" type="QString" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" force_rhr="0" clip_to_extent="1" is_animated="0" type="marker" name="" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" id="{39416ff0-4703-4daf-a649-4db32340ed93}" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="225,89,137,255" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="diamond" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="161,64,98,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="3" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <customproperties>
    <Option type="Map">
      <Option value="remove" type="QString" name="QFieldSync/action"/>
      <Option value="{}" type="QString" name="QFieldSync/photo_naming"/>
      <Option type="List" name="dualview/previewExpressions">
        <Option value="&quot;desc_action&quot;" type="QString"/>
      </Option>
      <Option value="0" type="QString" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_foyer">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="A déposer" type="QString" name="A déposer"/>
              </Option>
              <Option type="Map">
                <Option value="A poser" type="QString" name="A poser"/>
              </Option>
              <Option type="Map">
                <Option value="A remplacer" type="QString" name="A remplacer"/>
              </Option>
              <Option type="Map">
                <Option value="Actif" type="QString" name="Actif"/>
              </Option>
              <Option type="Map">
                <Option value="Déposé" type="QString" name="Déposé"/>
              </Option>
              <Option type="Map">
                <Option value="Hors contrat" type="QString" name="Hors contrat"/>
              </Option>
              <Option type="Map">
                <Option value="Inactif" type="QString" name="Inactif"/>
              </Option>
              <Option type="Map">
                <Option value="Virtuel" type="QString" name="Virtuel"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_pose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_depose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_remplacement">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="temperature_couleur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="localisation_appareillage">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Ballast ferromagnetique" type="QString" name="Ballast ferromagnetique"/>
              </Option>
              <Option type="Map">
                <Option value="Ballast graduable" type="QString" name="Ballast graduable"/>
              </Option>
              <Option type="Map">
                <Option value="Ballast électronique" type="QString" name="Ballast électronique"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photometrie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nature">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Ballon fluo" type="QString" name="Ballon fluo"/>
              </Option>
              <Option type="Map">
                <Option value="Cosmopolis" type="QString" name="Cosmopolis"/>
              </Option>
              <Option type="Map">
                <Option value="Fluo compacte" type="QString" name="Fluo compacte"/>
              </Option>
              <Option type="Map">
                <Option value="Halogène" type="QString" name="Halogène"/>
              </Option>
              <Option type="Map">
                <Option value="Iodure métallique" type="QString" name="Iodure métallique"/>
              </Option>
              <Option type="Map">
                <Option value="Led" type="QString" name="Led"/>
              </Option>
              <Option type="Map">
                <Option value="Sodium basse pression" type="QString" name="Sodium basse pression"/>
              </Option>
              <Option type="Map">
                <Option value="Sodium haute pression" type="QString" name="Sodium haute pression"/>
              </Option>
              <Option type="Map">
                <Option value="Tube fluo" type="QString" name="Tube fluo"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="puissance">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="culot">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="2G7" type="QString" name="2G7"/>
              </Option>
              <Option type="Map">
                <Option value="Autre" type="QString" name="Autre"/>
              </Option>
              <Option type="Map">
                <Option value="B22" type="QString" name="B22"/>
              </Option>
              <Option type="Map">
                <Option value="Ba20d" type="QString" name="Ba20d"/>
              </Option>
              <Option type="Map">
                <Option value="Ba20s" type="QString" name="Ba20s"/>
              </Option>
              <Option type="Map">
                <Option value="E27" type="QString" name="E27"/>
              </Option>
              <Option type="Map">
                <Option value="E40" type="QString" name="E40"/>
              </Option>
              <Option type="Map">
                <Option value="G12" type="QString" name="G12"/>
              </Option>
              <Option type="Map">
                <Option value="G13" type="QString" name="G13"/>
              </Option>
              <Option type="Map">
                <Option value="G24d" type="QString" name="G24d"/>
              </Option>
              <Option type="Map">
                <Option value="G5" type="QString" name="G5"/>
              </Option>
              <Option type="Map">
                <Option value="GU5.3" type="QString" name="GU5.3"/>
              </Option>
              <Option type="Map">
                <Option value="PG12" type="QString" name="PG12"/>
              </Option>
              <Option type="Map">
                <Option value="PGz12" type="QString" name="PGz12"/>
              </Option>
              <Option type="Map">
                <Option value="R7s" type="QString" name="R7s"/>
              </Option>
              <Option type="Map">
                <Option value="RX7s" type="QString" name="RX7s"/>
              </Option>
              <Option type="Map">
                <Option value="S14S" type="QString" name="S14S"/>
              </Option>
              <Option type="Map">
                <Option value="S19" type="QString" name="S19"/>
              </Option>
              <Option type="Map">
                <Option value="SFC10" type="QString" name="SFC10"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="appareillage">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Ballast ferromagnetique" type="QString" name="Ballast ferromagnetique"/>
              </Option>
              <Option type="Map">
                <Option value="Ballast graduable" type="QString" name="Ballast graduable"/>
              </Option>
              <Option type="Map">
                <Option value="Ballast électronique" type="QString" name="Ballast électronique"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_action">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="desc_action">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="remarque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mslink">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mapid">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ecp_intervention_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="created_at">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="updated_at">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="display_format"/>
            <Option value="dd/MM/yyyy HH:mm:ss" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pkid">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intervention">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acteur">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" type="QString" name="&lt;NULL>"/>
              </Option>
              <Option type="Map">
                <Option value="Anthony Louis-Villemin" type="QString" name="Anthony Louis-Villemin"/>
              </Option>
              <Option type="Map">
                <Option value="Bruno Idoux" type="QString" name="Bruno Idoux"/>
              </Option>
              <Option type="Map">
                <Option value="Damien Jacquot" type="QString" name="Damien Jacquot"/>
              </Option>
              <Option type="Map">
                <Option value="Jean-Claude Anotta" type="QString" name="Jean-Claude Anotta"/>
              </Option>
              <Option type="Map">
                <Option value="Julien Pinot" type="QString" name="Julien Pinot"/>
              </Option>
              <Option type="Map">
                <Option value="Morgan Fontaine" type="QString" name="Morgan Fontaine"/>
              </Option>
              <Option type="Map">
                <Option value="Sophie Massonneau" type="QString" name="Sophie Massonneau"/>
              </Option>
              <Option type="Map">
                <Option value="Xavier Schuler" type="QString" name="Xavier Schuler"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="flux">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intensite_alim">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="abaissement_puissance">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="oui" type="QString" name="CheckedState"/>
            <Option value="1" type="int" name="TextDisplayMethod"/>
            <Option value="non" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="parafoudre">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="oui" type="QString" name="CheckedState"/>
            <Option value="1" type="int" name="TextDisplayMethod"/>
            <Option value="non" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photo">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="uid4">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="fid" index="0" name=""/>
    <alias field="numero" index="1" name=""/>
    <alias field="insee" index="2" name="Numéro insee de la commune"/>
    <alias field="id_foyer" index="3" name="Identifiant du foyer"/>
    <alias field="etat" index="4" name="Etat"/>
    <alias field="date_pose" index="5" name="Date de pose"/>
    <alias field="date_depose" index="6" name="Date de dépose"/>
    <alias field="date_remplacement" index="7" name="Date de remplacement"/>
    <alias field="temperature_couleur" index="8" name="Température de couleur"/>
    <alias field="localisation_appareillage" index="9" name="Localisation de l'appareillage"/>
    <alias field="photometrie" index="10" name="Photométrie"/>
    <alias field="nature" index="11" name="Nature de la source"/>
    <alias field="puissance" index="12" name="Puissance de la source"/>
    <alias field="culot" index="13" name="Culot"/>
    <alias field="appareillage" index="14" name="Appareillage "/>
    <alias field="date_action" index="15" name=""/>
    <alias field="desc_action" index="16" name=""/>
    <alias field="remarque" index="17" name="Remarque"/>
    <alias field="mslink" index="18" name=""/>
    <alias field="mapid" index="19" name=""/>
    <alias field="ecp_intervention_id" index="20" name="Identifiant de l'intervention (interne)"/>
    <alias field="created_at" index="21" name=""/>
    <alias field="updated_at" index="22" name=""/>
    <alias field="pkid" index="23" name=""/>
    <alias field="intervention" index="24" name="Numéro de chantier"/>
    <alias field="acteur" index="25" name="Chargé d'affaires"/>
    <alias field="flux" index="26" name="Flux"/>
    <alias field="intensite_alim" index="27" name="Intensité d'alimentation"/>
    <alias field="abaissement_puissance" index="28" name="Abaissement de puissance"/>
    <alias field="parafoudre" index="29" name="Parafoudre"/>
    <alias field="photo" index="30" name="Photo"/>
    <alias field="uid4" index="31" name=""/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="fid"/>
    <policy policy="Duplicate" field="numero"/>
    <policy policy="Duplicate" field="insee"/>
    <policy policy="Duplicate" field="id_foyer"/>
    <policy policy="Duplicate" field="etat"/>
    <policy policy="Duplicate" field="date_pose"/>
    <policy policy="Duplicate" field="date_depose"/>
    <policy policy="Duplicate" field="date_remplacement"/>
    <policy policy="Duplicate" field="temperature_couleur"/>
    <policy policy="Duplicate" field="localisation_appareillage"/>
    <policy policy="Duplicate" field="photometrie"/>
    <policy policy="Duplicate" field="nature"/>
    <policy policy="Duplicate" field="puissance"/>
    <policy policy="Duplicate" field="culot"/>
    <policy policy="Duplicate" field="appareillage"/>
    <policy policy="Duplicate" field="date_action"/>
    <policy policy="Duplicate" field="desc_action"/>
    <policy policy="Duplicate" field="remarque"/>
    <policy policy="Duplicate" field="mslink"/>
    <policy policy="Duplicate" field="mapid"/>
    <policy policy="Duplicate" field="ecp_intervention_id"/>
    <policy policy="Duplicate" field="created_at"/>
    <policy policy="Duplicate" field="updated_at"/>
    <policy policy="Duplicate" field="pkid"/>
    <policy policy="Duplicate" field="intervention"/>
    <policy policy="Duplicate" field="acteur"/>
    <policy policy="Duplicate" field="flux"/>
    <policy policy="Duplicate" field="intensite_alim"/>
    <policy policy="Duplicate" field="abaissement_puissance"/>
    <policy policy="Duplicate" field="parafoudre"/>
    <policy policy="Duplicate" field="photo"/>
    <policy policy="Duplicate" field="uid4"/>
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" field="fid" expression=""/>
    <default applyOnUpdate="0" field="numero" expression=""/>
    <default applyOnUpdate="0" field="insee" expression=""/>
    <default applyOnUpdate="0" field="id_foyer" expression=""/>
    <default applyOnUpdate="0" field="etat" expression=""/>
    <default applyOnUpdate="0" field="date_pose" expression=""/>
    <default applyOnUpdate="0" field="date_depose" expression=""/>
    <default applyOnUpdate="0" field="date_remplacement" expression=""/>
    <default applyOnUpdate="0" field="temperature_couleur" expression=""/>
    <default applyOnUpdate="0" field="localisation_appareillage" expression=""/>
    <default applyOnUpdate="0" field="photometrie" expression=""/>
    <default applyOnUpdate="0" field="nature" expression=""/>
    <default applyOnUpdate="0" field="puissance" expression=""/>
    <default applyOnUpdate="0" field="culot" expression=""/>
    <default applyOnUpdate="0" field="appareillage" expression=""/>
    <default applyOnUpdate="0" field="date_action" expression=""/>
    <default applyOnUpdate="0" field="desc_action" expression=""/>
    <default applyOnUpdate="0" field="remarque" expression=""/>
    <default applyOnUpdate="0" field="mslink" expression=""/>
    <default applyOnUpdate="0" field="mapid" expression=""/>
    <default applyOnUpdate="0" field="ecp_intervention_id" expression=""/>
    <default applyOnUpdate="0" field="created_at" expression=""/>
    <default applyOnUpdate="0" field="updated_at" expression=""/>
    <default applyOnUpdate="0" field="pkid" expression=""/>
    <default applyOnUpdate="0" field="intervention" expression=""/>
    <default applyOnUpdate="0" field="acteur" expression=""/>
    <default applyOnUpdate="0" field="flux" expression=""/>
    <default applyOnUpdate="0" field="intensite_alim" expression=""/>
    <default applyOnUpdate="0" field="abaissement_puissance" expression=""/>
    <default applyOnUpdate="0" field="parafoudre" expression=""/>
    <default applyOnUpdate="0" field="photo" expression=""/>
    <default applyOnUpdate="0" field="uid4" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" field="fid" constraints="3" exp_strength="0"/>
    <constraint unique_strength="1" notnull_strength="1" field="numero" constraints="3" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="insee" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="id_foyer" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="etat" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="date_pose" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="date_depose" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="date_remplacement" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="temperature_couleur" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="localisation_appareillage" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="photometrie" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="nature" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="puissance" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="culot" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="appareillage" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="date_action" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="desc_action" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="remarque" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="mslink" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="mapid" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="ecp_intervention_id" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="created_at" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="updated_at" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="pkid" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="intervention" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="acteur" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="flux" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="intensite_alim" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="abaissement_puissance" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="parafoudre" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="photo" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="uid4" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="fid" exp=""/>
    <constraint desc="" field="numero" exp=""/>
    <constraint desc="" field="insee" exp=""/>
    <constraint desc="" field="id_foyer" exp=""/>
    <constraint desc="" field="etat" exp=""/>
    <constraint desc="" field="date_pose" exp=""/>
    <constraint desc="" field="date_depose" exp=""/>
    <constraint desc="" field="date_remplacement" exp=""/>
    <constraint desc="" field="temperature_couleur" exp=""/>
    <constraint desc="" field="localisation_appareillage" exp=""/>
    <constraint desc="" field="photometrie" exp=""/>
    <constraint desc="" field="nature" exp=""/>
    <constraint desc="" field="puissance" exp=""/>
    <constraint desc="" field="culot" exp=""/>
    <constraint desc="" field="appareillage" exp=""/>
    <constraint desc="" field="date_action" exp=""/>
    <constraint desc="" field="desc_action" exp=""/>
    <constraint desc="" field="remarque" exp=""/>
    <constraint desc="" field="mslink" exp=""/>
    <constraint desc="" field="mapid" exp=""/>
    <constraint desc="" field="ecp_intervention_id" exp=""/>
    <constraint desc="" field="created_at" exp=""/>
    <constraint desc="" field="updated_at" exp=""/>
    <constraint desc="" field="pkid" exp=""/>
    <constraint desc="" field="intervention" exp=""/>
    <constraint desc="" field="acteur" exp=""/>
    <constraint desc="" field="flux" exp=""/>
    <constraint desc="" field="intensite_alim" exp=""/>
    <constraint desc="" field="abaissement_puissance" exp=""/>
    <constraint desc="" field="parafoudre" exp=""/>
    <constraint desc="" field="photo" exp=""/>
    <constraint desc="" field="uid4" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column hidden="0" width="-1" type="field" name="numero"/>
      <column hidden="0" width="-1" type="field" name="insee"/>
      <column hidden="0" width="-1" type="field" name="id_foyer"/>
      <column hidden="0" width="-1" type="field" name="etat"/>
      <column hidden="0" width="-1" type="field" name="date_pose"/>
      <column hidden="0" width="-1" type="field" name="date_depose"/>
      <column hidden="0" width="-1" type="field" name="date_remplacement"/>
      <column hidden="0" width="-1" type="field" name="temperature_couleur"/>
      <column hidden="0" width="-1" type="field" name="localisation_appareillage"/>
      <column hidden="0" width="-1" type="field" name="photometrie"/>
      <column hidden="0" width="-1" type="field" name="nature"/>
      <column hidden="0" width="-1" type="field" name="puissance"/>
      <column hidden="0" width="-1" type="field" name="culot"/>
      <column hidden="0" width="-1" type="field" name="appareillage"/>
      <column hidden="0" width="-1" type="field" name="date_action"/>
      <column hidden="0" width="-1" type="field" name="desc_action"/>
      <column hidden="0" width="-1" type="field" name="remarque"/>
      <column hidden="0" width="-1" type="field" name="mslink"/>
      <column hidden="0" width="-1" type="field" name="mapid"/>
      <column hidden="0" width="-1" type="field" name="ecp_intervention_id"/>
      <column hidden="0" width="-1" type="field" name="created_at"/>
      <column hidden="0" width="-1" type="field" name="updated_at"/>
      <column hidden="0" width="-1" type="field" name="pkid"/>
      <column hidden="0" width="-1" type="field" name="intervention"/>
      <column hidden="0" width="-1" type="field" name="acteur"/>
      <column hidden="0" width="-1" type="field" name="flux"/>
      <column hidden="0" width="-1" type="field" name="intensite_alim"/>
      <column hidden="0" width="193" type="field" name="abaissement_puissance"/>
      <column hidden="0" width="-1" type="field" name="parafoudre"/>
      <column hidden="0" width="-1" type="field" name="photo"/>
      <column hidden="0" width="-1" type="field" name="uid4"/>
      <column hidden="0" width="-1" type="field" name="fid"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
      <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
    </labelStyle>
    <attributeEditorField index="2" name="insee" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="24" name="intervention" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="25" name="acteur" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="20" name="ecp_intervention_id" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="3" name="id_foyer" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="4" name="etat" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="5" name="date_pose" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="6" name="date_depose" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="8" name="temperature_couleur" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="9" name="localisation_appareillage" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="10" name="photometrie" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="11" name="nature" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="12" name="puissance" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="13" name="culot" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="14" name="appareillage" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="26" name="flux" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="27" name="intensite_alim" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="28" name="abaissement_puissance" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="29" name="parafoudre" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField index="17" name="remarque" showLabel="1">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255">
        <labelFont bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" underline="0" style=""/>
      </labelStyle>
    </attributeEditorField>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="abaissement_puissance"/>
    <field editable="1" name="acteur"/>
    <field editable="1" name="appareillage"/>
    <field editable="1" name="created_at"/>
    <field editable="1" name="culot"/>
    <field editable="1" name="date_action"/>
    <field editable="1" name="date_depose"/>
    <field editable="1" name="date_pose"/>
    <field editable="1" name="date_remplacement"/>
    <field editable="1" name="desc_action"/>
    <field editable="1" name="ecp_intervention_id"/>
    <field editable="1" name="etat"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="flux"/>
    <field editable="1" name="id_foyer"/>
    <field editable="1" name="insee"/>
    <field editable="1" name="intensite_alim"/>
    <field editable="1" name="intervention"/>
    <field editable="1" name="localisation_appareillage"/>
    <field editable="1" name="mapid"/>
    <field editable="1" name="mslink"/>
    <field editable="1" name="nature"/>
    <field editable="1" name="numero"/>
    <field editable="1" name="parafoudre"/>
    <field editable="1" name="photo"/>
    <field editable="1" name="photometrie"/>
    <field editable="1" name="pkid"/>
    <field editable="1" name="puissance"/>
    <field editable="1" name="remarque"/>
    <field editable="1" name="temperature_couleur"/>
    <field editable="1" name="uid4"/>
    <field editable="1" name="updated_at"/>
  </editable>
  <labelOnTop>
    <field name="abaissement_puissance" labelOnTop="1"/>
    <field name="acteur" labelOnTop="1"/>
    <field name="appareillage" labelOnTop="1"/>
    <field name="created_at" labelOnTop="0"/>
    <field name="culot" labelOnTop="1"/>
    <field name="date_action" labelOnTop="0"/>
    <field name="date_depose" labelOnTop="1"/>
    <field name="date_pose" labelOnTop="1"/>
    <field name="date_remplacement" labelOnTop="0"/>
    <field name="desc_action" labelOnTop="0"/>
    <field name="ecp_intervention_id" labelOnTop="1"/>
    <field name="etat" labelOnTop="1"/>
    <field name="fid" labelOnTop="0"/>
    <field name="flux" labelOnTop="1"/>
    <field name="id_foyer" labelOnTop="1"/>
    <field name="insee" labelOnTop="1"/>
    <field name="intensite_alim" labelOnTop="1"/>
    <field name="intervention" labelOnTop="1"/>
    <field name="localisation_appareillage" labelOnTop="1"/>
    <field name="mapid" labelOnTop="0"/>
    <field name="mslink" labelOnTop="0"/>
    <field name="nature" labelOnTop="1"/>
    <field name="numero" labelOnTop="0"/>
    <field name="parafoudre" labelOnTop="1"/>
    <field name="photo" labelOnTop="1"/>
    <field name="photometrie" labelOnTop="1"/>
    <field name="pkid" labelOnTop="0"/>
    <field name="puissance" labelOnTop="1"/>
    <field name="remarque" labelOnTop="1"/>
    <field name="temperature_couleur" labelOnTop="1"/>
    <field name="uid4" labelOnTop="0"/>
    <field name="updated_at" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="abaissement_puissance"/>
    <field reuseLastValue="0" name="acteur"/>
    <field reuseLastValue="0" name="appareillage"/>
    <field reuseLastValue="0" name="created_at"/>
    <field reuseLastValue="0" name="culot"/>
    <field reuseLastValue="0" name="date_action"/>
    <field reuseLastValue="0" name="date_depose"/>
    <field reuseLastValue="0" name="date_pose"/>
    <field reuseLastValue="0" name="date_remplacement"/>
    <field reuseLastValue="0" name="desc_action"/>
    <field reuseLastValue="0" name="ecp_intervention_id"/>
    <field reuseLastValue="0" name="etat"/>
    <field reuseLastValue="0" name="fid"/>
    <field reuseLastValue="0" name="flux"/>
    <field reuseLastValue="0" name="id_foyer"/>
    <field reuseLastValue="0" name="insee"/>
    <field reuseLastValue="0" name="intensite_alim"/>
    <field reuseLastValue="0" name="intervention"/>
    <field reuseLastValue="0" name="localisation_appareillage"/>
    <field reuseLastValue="0" name="mapid"/>
    <field reuseLastValue="0" name="mslink"/>
    <field reuseLastValue="0" name="nature"/>
    <field reuseLastValue="0" name="numero"/>
    <field reuseLastValue="0" name="parafoudre"/>
    <field reuseLastValue="0" name="photo"/>
    <field reuseLastValue="0" name="photometrie"/>
    <field reuseLastValue="0" name="pkid"/>
    <field reuseLastValue="0" name="puissance"/>
    <field reuseLastValue="0" name="remarque"/>
    <field reuseLastValue="0" name="temperature_couleur"/>
    <field reuseLastValue="0" name="uid4"/>
    <field reuseLastValue="0" name="updated_at"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"desc_action"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
