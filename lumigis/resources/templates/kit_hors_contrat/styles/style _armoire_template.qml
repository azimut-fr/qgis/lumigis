<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyLocal="1" hasScaleBasedVisibilityFlag="0" labelsEnabled="1" simplifyDrawingHints="0" minScale="5000" symbologyReferenceScale="200" version="3.28.6-Firenze" simplifyDrawingTol="1" simplifyMaxScale="1" readOnly="0" simplifyAlgorithm="0" maxScale="1" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationField="fid" fixedDuration="0" enabled="0" endField="" mode="0" limitMode="0" accumulate="0" startField="date_pose" startExpression="" endExpression="" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation binding="Centroid" zoffset="0" symbology="Line" extrusionEnabled="0" zscale="1" clamping="Terrain" respectLayerSymbol="1" extrusion="0" showMarkerSymbolInSurfacePlots="0" type="IndividualFeatures">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol force_rhr="0" alpha="1" name="" is_animated="0" frame_rate="10" type="line" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="145,82,45,255" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.6" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol force_rhr="0" alpha="1" name="" is_animated="0" frame_rate="10" type="fill" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleFill" enabled="1" locked="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
            <Option value="145,82,45,255" name="color" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="104,59,32,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="solid" name="style" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol force_rhr="0" alpha="1" name="" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="145,82,45,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="104,59,32,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 enableorderby="0" forceraster="0" symbollevels="0" referencescale="200" type="RuleRenderer">
    <rules key="{9584e0b9-7efd-4ac4-ad63-1d6ac48b5ccb}">
      <rule label="Actif" filter=" &quot;etat&quot; = 'Actif' " key="{5a391dc9-9f01-435b-91fe-4d346f14cb9d}">
        <rule symbol="0" filter=" &quot;cellule&quot; = 'ARMOIRE'" key="{7a8c7280-bab7-423f-80da-b70057d3d9e5}"/>
      </rule>
      <rule label="A poser" filter=" &quot;etat&quot; =  'A poser' " key="{0c02fe0b-4204-4fd4-b7bf-3876705a141a}">
        <rule symbol="1" filter=" &quot;cellule&quot; =  'ARMOIRE A POSER' " key="{51d4d851-6fe8-44ea-976d-0efc00a7cfac}"/>
      </rule>
      <rule label="A remplacer" filter=" &quot;etat&quot; =  'A remplacer' " key="{9bda2467-3988-4c37-ade0-4d4c88177bf8}">
        <rule symbol="2" filter=" &quot;cellule&quot; = 'ARMOIRE'" key="{01f36089-db89-4c84-8076-60fb900de8b9}"/>
      </rule>
      <rule label="A déposer" filter=" &quot;etat&quot; =  'A déposer' " key="{47fdf281-e02e-4585-9222-1bd55f7819d6}">
        <rule symbol="3" filter=" &quot;cellule&quot; = 'ARMOIRE'" key="{942512db-e2ad-4635-9622-0cfa8a211176}"/>
      </rule>
    </rules>
    <symbols>
      <symbol force_rhr="0" alpha="1" name="0" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SvgMarker" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuMzkzOCAyNTE1Ljc0OCIKICAgaWQ9InN2ZzgzMTMiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9YXJtb2lyZS5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSmVhbiBDbGF1ZGVcRGVza3RvcFxwaG90b3MgY2VsbHVsZXNcc21kZXY9YXJtb2lyZS5zdmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczgzMTUiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJiYXNlIgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxLjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnpvb209IjAuMTg1MzU0MDciCiAgICAgaW5rc2NhcGU6Y3g9IjI4NzAuMTgyNCIKICAgICBpbmtzY2FwZTpjeT0iMTYwMi4zMzg3IgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnOTQ3IgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGJvcmRlcmxheWVyPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjQ1MjIuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjM3NTQuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgxMDYiCiAgICAgICBvcmlnaW54PSIwIgogICAgICAgb3JpZ2lueT0iMCIKICAgICAgIHNwYWNpbmd4PSIxIgogICAgICAgc3BhY2luZ3k9IjEiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhODMxOCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpbmtzY2FwZTpsYWJlbD0iQ2FscXVlIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsNDI2Mi41OTk0KSI+CiAgICA8ZwogICAgICAgaWQ9Imc5NDciCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5MjQ0OTQsMCwwLDAuOTk2NTcyNTMsLTQ2NzYuMjEzNSwtMzE3Ny4zMzYyKSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDAzIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLDAuOTk1ODYyNjcsLTEuMDA0MTU1MywwLDgzMjkuODE4NywxMTkzLjM2NzMpIj4KICAgICAgICA8cmVjdAogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwZmY7c3Ryb2tlLXdpZHRoOjEyMy45NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlkPSJyZWN0OTAwIgogICAgICAgICAgIHdpZHRoPSIyMzU2LjM3NDgiCiAgICAgICAgICAgaGVpZ2h0PSI1MTkxLjAyMDUiCiAgICAgICAgICAgeD0iLTIxOTkuNTI3OCIKICAgICAgICAgICB5PSItMTY4MC4xNTU4IiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDBmZjtmaWxsLW9wYWNpdHk6MC45ODgyMDg7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjIxODYuOTY7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiCiAgICAgICAgICAgZD0iTSAtNDMyLjI0Njg3LDM1MTAuODY0OCBIIC0xMDIxLjM0MDUgViAyMjEzLjEwOTcgOTE1LjM1NDQ5IGggNTg5LjA5MzYzIDU4OS4wOTM2OSBWIDIyMTMuMTA5NyAzNTEwLjg2NDggWiIKICAgICAgICAgICBpZD0icGF0aDQ4MzEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwZmY7ZmlsbC1vcGFjaXR5OjAuOTg4MjA4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyMTQ2LjIxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIgogICAgICAgICAgIGQ9Ik0gLTE2MDIuMDU3Miw5MTUuMzU0NSBIIC0yMTgyLjc3NCBWIC0zNzIuODcwNzcgLTE2NjEuMDk2NSBoIDU4MC43MTY4IDU4MC43MTY2IFYgLTM3Mi44NzA3NyA5MTUuMzU0NSBaIgogICAgICAgICAgIGlkPSJwYXRoNDgyOSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMTEuNjE3Njk5LDkuOTUxNzU1MSwwLC04MTkuNzc0Myw5OC42NjA0NCkiCiAgICAgICAgICAgaWQ9Imc0NzY2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiPgogICAgICAgICAgPGxpbmUKICAgICAgICAgICAgIGlkPSJsaW5lMiIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDQiCiAgICAgICAgICAgICBkPSJtIDIuOTk4LDc0LjM3NyBjIC0wLjUwNCwwIC0xLjAxNCwtMC4xMjkgLTEuNDgzLC0wLjM5NiAtMS40MzksLTAuODIgLTEuOTQsLTIuNjUyIC0xLjEyLC00LjA5MiBsIDM5LC02OC4zNzQgYyAwLjgyMSwtMS40MzkgMi42NTQsLTEuOTQxIDQuMDkyLC0xLjEyIDEuNDM5LDAuODIxIDEuOTQsMi42NTMgMS4xMTksNC4wOTIgbCAtMzksNjguMzc0IEMgNS4wNTMsNzMuODMzIDQuMDQsNzQuMzc3IDIuOTk4LDc0LjM3NyBaIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMDAwMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgaWQ9InBhdGg2IgogICAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgIGlkPSJwYXRoOCIKICAgICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIGlkPSJwb2x5Z29uMTAiCiAgICAgICAgICAgICBwb2ludHM9IjQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzICIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiIC8+CiAgICAgICAgPC9nPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMTEuNjE3Njk5LDkuOTUxNzU0NywwLC0xOTg4Ljc5MjMsMjY5NS43OTY1KSIKICAgICAgICAgICBpZD0iZzQ3NjYtOSIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIj4KICAgICAgICAgIDxsaW5lCiAgICAgICAgICAgICBpZD0ibGluZTItNCIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDQtMCIKICAgICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDYtOSIKICAgICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDgtOSIKICAgICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIGlkPSJwb2x5Z29uMTAtOCIKICAgICAgICAgICAgIHBvaW50cz0iNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMDAwMCIgLz4KICAgICAgICA8L2c+CiAgICAgIDwvZz4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo=" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="rot_z" name="field" type="QString"/>
                  <Option value="2" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" name="1" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SvgMarker" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuMzkzOCAyNTE1Ljc0OCIKICAgaWQ9InN2ZzgzMTMiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9YXJtb2lyZV9hX3Bvc2VyLnN2ZyIKICAgaW5rc2NhcGU6ZXhwb3J0LWZpbGVuYW1lPSJDOlxVc2Vyc1xKZWFuIENsYXVkZVxEZXNrdG9wXHBob3RvcyBjZWxsdWxlc1xzbWRldj1hcm1vaXJlLnN2Zy5wbmciCiAgIGlua3NjYXBlOmV4cG9ydC14ZHBpPSI5NiIKICAgaW5rc2NhcGU6ZXhwb3J0LXlkcGk9Ijk2IgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzODMxNSIgLz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9ImJhc2UiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMC4wIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4zMiIKICAgICBpbmtzY2FwZTpjeD0iMTM0Ni44NzUiCiAgICAgaW5rc2NhcGU6Y3k9IjI0NDYuODc1IgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnOTQ3IgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGJvcmRlcmxheWVyPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjQ1MjIuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjM3NTQuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgxMDYiCiAgICAgICBvcmlnaW54PSIwIgogICAgICAgb3JpZ2lueT0iMCIKICAgICAgIHNwYWNpbmd4PSIxIgogICAgICAgc3BhY2luZ3k9IjEiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhODMxOCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpbmtzY2FwZTpsYWJlbD0iQ2FscXVlIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsNDI2Mi41OTk0KSI+CiAgICA8ZwogICAgICAgaWQ9Imc5NDciCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5MjQ0OTQsMCwwLDAuOTk2NTcyNTMsLTQ2NzYuMjEzNSwtMzE3Ny4zMzYyKSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDE5IgogICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSg5NjI5LjY5NDYsMzYxOS4zMTAzKSI+CiAgICAgICAgPHJlY3QKICAgICAgICAgICB5PSItMzg1LjIxNDMyIgogICAgICAgICAgIHg9Ii00NjE3Ljg5MjEiCiAgICAgICAgICAgaGVpZ2h0PSI1MjEyLjU4NjQiCiAgICAgICAgICAgd2lkdGg9IjIzNDYuNjI1NyIKICAgICAgICAgICBpZD0icmVjdDkwMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojZmYzM2ZmO3N0cm9rZS13aWR0aDoxMjMuOTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MC45OTYwNzgiCiAgICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoOTApIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDQ4MzEiCiAgICAgICAgICAgZD0ibSAtNDc4Ny43NzAyLC0yODgyLjU4NjYgdiAtNTU5Ljk2NDYgaCAxMjg1LjU5OCAxMjg1LjU5ODIgdiA1NTkuOTY0NiA1NTkuOTY0NyBoIC0xMjg1LjU5ODIgLTEyODUuNTk4IHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MjEyMi4yO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDQ4MjkiCiAgICAgICAgICAgZD0ibSAtMjIxNi41NzQsLTQwMjEuODc5NSB2IC01NzkuMzI4NCBIIC05MjUuMjQ5MzIgMzY2LjA3NTc3IHYgNTc5LjMyODQgNTc5LjMyODMgSCAtOTI1LjI0OTMyIC0yMjE2LjU3NCBaIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjMzZmY7ZmlsbC1vcGFjaXR5OjAuOTk2MDc4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyMTQ2LjIyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgICAgIDxnCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MSIKICAgICAgICAgICBpZD0iZzQ3NjYiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMTEuNjY1OTY1LDAsMCw5LjkxMDU4MTYsLTE0MDAuOTkyLC0zMjQzLjg0NzMpIj4KICAgICAgICAgIDxsaW5lCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICAgIHgyPSIzIgogICAgICAgICAgICAgeTI9IjcxLjM3NSIKICAgICAgICAgICAgIGlkPSJsaW5lMiIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0ibSAyLjk5OCw3NC4zNzcgYyAtMC41MDQsMCAtMS4wMTQsLTAuMTI5IC0xLjQ4MywtMC4zOTYgLTEuNDM5LC0wLjgyIC0xLjk0LC0yLjY1MiAtMS4xMiwtNC4wOTIgbCAzOSwtNjguMzc0IGMgMC44MjEsLTEuNDM5IDIuNjU0LC0xLjk0MSA0LjA5MiwtMS4xMiAxLjQzOSwwLjgyMSAxLjk0LDIuNjUzIDEuMTE5LDQuMDkyIGwgLTM5LDY4LjM3NCBDIDUuMDUzLDczLjgzMyA0LjA0LDc0LjM3NyAyLjk5OCw3NC4zNzcgWiIKICAgICAgICAgICAgIGlkPSJwYXRoNCIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MSIKICAgICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgICBpZD0icGF0aDYiCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjM0ZmY7ZmlsbC1vcGFjaXR5OjEiCiAgICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgICAgaWQ9InBhdGg4IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwb2x5Z29uCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgcG9pbnRzPSI0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSAiCiAgICAgICAgICAgICBpZD0icG9seWdvbjEwIiAvPgogICAgICAgIDwvZz4KICAgICAgICA8ZwogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjM0ZmY7ZmlsbC1vcGFjaXR5OjEiCiAgICAgICAgICAgaWQ9Imc0NzY2LTkiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMTEuNjY1OTY1LDAsMCw5LjkxMDU4MTIsLTQwMDguOTE3OSwtNDQwOC4wMjg3KSI+CiAgICAgICAgICA8bGluZQogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MSIKICAgICAgICAgICAgIHgxPSI0MiIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICBpZD0ibGluZTItNCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0ibSAyLjk5OCw3NC4zNzcgYyAtMC41MDQsMCAtMS4wMTQsLTAuMTI5IC0xLjQ4MywtMC4zOTYgLTEuNDM5LC0wLjgyIC0xLjk0LC0yLjY1MiAtMS4xMiwtNC4wOTIgbCAzOSwtNjguMzc0IGMgMC44MjEsLTEuNDM5IDIuNjU0LC0xLjk0MSA0LjA5MiwtMS4xMiAxLjQzOSwwLjgyMSAxLjk0LDIuNjUzIDEuMTE5LDQuMDkyIGwgLTM5LDY4LjM3NCBDIDUuMDUzLDczLjgzMyA0LjA0LDc0LjM3NyAyLjk5OCw3NC4zNzcgWiIKICAgICAgICAgICAgIGlkPSJwYXRoNC0wIgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICAgIGlkPSJwYXRoNi05IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0ibSA4MC4zNzgsNzQuMzc3IGMgLTEuMDUsMCAtMi4wNjgsLTAuNTUzIC0yLjYxOSwtMS41MzMgTCAzOS4zODQsNC40NyBjIC0wLjgxMSwtMS40NDUgLTAuMjk3LC0zLjI3MyAxLjE0OCwtNC4wODQgMS40NDUsLTAuODEzIDMuMjczLC0wLjI5NyA0LjA4NCwxLjE0OCBsIDM4LjM3NSw2OC4zNzQgYyAwLjgxMSwxLjQ0NSAwLjI5NywzLjI3NSAtMS4xNDcsNC4wODYgLTAuNDY1LDAuMjU4IC0wLjk2OSwwLjM4MyAtMS40NjYsMC4zODMgeiIKICAgICAgICAgICAgIGlkPSJwYXRoOC05IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwb2x5Z29uCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgcG9pbnRzPSI0Ni4xNTMsNTYuNDM2IDQ0LjE3OCw1Ni40ODIgNDkuMDY4LDM4LjUyNiAzOS4yOTEsNDIuMzggNDYuMzQxLDI2Ljg0NSA0MC43MDEsMjYuODQ1IDM0LjMwOCw0OS4xNDkgNDMuNTIxLDQ1LjQ4MyA0MC43MDEsNTYuMTA1IDM4LjYzMyw1Ni4yNDcgNDEuMzU5LDYzLjUzMyAiCiAgICAgICAgICAgICBpZD0icG9seWdvbjEwLTgiIC8+CiAgICAgICAgPC9nPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="&quot;rot_z&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" name="2" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SvgMarker" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuMzkzOCAyNTE1Ljc0OCIKICAgaWQ9InN2ZzgzMTMiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9YXJtb2lyZV9hX3JlbXBsYWNlci5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSmVhbiBDbGF1ZGVcRGVza3RvcFxwaG90b3MgY2VsbHVsZXNcc21kZXY9YXJtb2lyZS5zdmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczgzMTUiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJiYXNlIgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxLjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnpvb209IjAuMTc2Nzc2NyIKICAgICBpbmtzY2FwZTpjeD0iMjU4NS4xODIzIgogICAgIGlua3NjYXBlOmN5PSIyMzMwLjYyMzkiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9Imc5NDciCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgYm9yZGVybGF5ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxOTIwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMjciCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5MTIiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgdW5pdHM9ImNtIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iNDUyMi42NywtMTAyNCIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMzc1NC42NywtMTAyNCIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1pbnRlcnNlY3Rpb24tcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vYmplY3QtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ2xvYmFsPSJ0cnVlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkZXNrY29sb3I9IiNkMWQxZDEiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODEwNiIKICAgICAgIG9yaWdpbng9IjAiCiAgICAgICBvcmlnaW55PSIwIgogICAgICAgc3BhY2luZ3g9IjEiCiAgICAgICBzcGFjaW5neT0iMSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGE4MzE4Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlua3NjYXBlOmxhYmVsPSJDYWxxdWUgMSIKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlkPSJsYXllcjEiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCw0MjYyLjU5OTQpIj4KICAgIDxnCiAgICAgICBpZD0iZzk0NyIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTkyNDQ5NCwwLDAsMC45OTY1NzI1MywtNDY3Ni4yMTM1LC0zMTc3LjMzNjIpIj4KICAgICAgPGcKICAgICAgICAgaWQ9ImczODciCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsMC45OTU4NjI2OSwtMS4wMDQxNTQ1LDAsODk5My4wNTE4LC01NzYyLjI0NDQpIj4KICAgICAgICA8cmVjdAogICAgICAgICAgIHk9Ii0xMDE3Ljg4NTkiCiAgICAgICAgICAgeD0iNDc4My4xOTU4IgogICAgICAgICAgIGhlaWdodD0iNTE5MS4wMjA1IgogICAgICAgICAgIHdpZHRoPSIyMzU2LjM3NDgiCiAgICAgICAgICAgaWQ9InJlY3Q5MDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MDtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6I2ZmYzk0ZTtzdHJva2Utd2lkdGg6MTIzLjk0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjAuOTg4MjM1IiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDQ4MzEiCiAgICAgICAgICAgZD0iTSA2NTI1LjcxMDcsNDEzMy42OTYzIEggNTk2My40MTk1IFYgMjg1My40MTczIDE1NzMuMTM4MSBoIDU2Mi4yOTEyIDU2Mi4yOTEyIHYgMTI4MC4yNzkyIDEyODAuMjc5IHoiCiAgICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyMTIyLjI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIGlkPSJwYXRoNDgyOSIKICAgICAgICAgICBkPSJNIDUzODEuNjg0NiwxNTczLjEzODEgSCA0Nzk5Ljk0OTUgViAyODcuMTU1OTggLTk5OC44MjY1NyBoIDU4MS43MzUxIDU4MS43MzQ5IFYgMjg3LjE1NTk4IDE1NzMuMTM4MSBaIgogICAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MjE0Ni4yMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIgLz4KICAgICAgICA8ZwogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgIGlkPSJnNDc2NiIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTUxLDAsNjE2Mi45NDkyLDc2MC45MzAzMykiPgogICAgICAgICAgPGxpbmUKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICAgIHgyPSIzIgogICAgICAgICAgICAgeTI9IjcxLjM3NSIKICAgICAgICAgICAgIGlkPSJsaW5lMiIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgICBpZD0icGF0aDQiCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICAgIGlkPSJwYXRoNiIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmYzk0ZTtmaWxsLW9wYWNpdHk6MC45ODgyMzUiCiAgICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgICAgaWQ9InBhdGg4IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwb2x5Z29uCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICAgIHBvaW50cz0iNDYuMTUzLDU2LjQzNiA0NC4xNzgsNTYuNDgyIDQ5LjA2OCwzOC41MjYgMzkuMjkxLDQyLjM4IDQ2LjM0MSwyNi44NDUgNDAuNzAxLDI2Ljg0NSAzNC4zMDgsNDkuMTQ5IDQzLjUyMSw0NS40ODMgNDAuNzAxLDU2LjEwNSAzOC42MzMsNTYuMjQ3IDQxLjM1OSw2My41MzMgIgogICAgICAgICAgICAgaWQ9InBvbHlnb24xMCIgLz4KICAgICAgICA8L2c+CiAgICAgICAgPGcKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICBpZD0iZzQ3NjYtOSIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTQ3LDAsNDk5My45MzEyLDMzNTguMDY2NCkiPgogICAgICAgICAgPGxpbmUKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICAgIHgyPSIzIgogICAgICAgICAgICAgeTI9IjcxLjM3NSIKICAgICAgICAgICAgIGlkPSJsaW5lMi00IiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgZD0ibSAyLjk5OCw3NC4zNzcgYyAtMC41MDQsMCAtMS4wMTQsLTAuMTI5IC0xLjQ4MywtMC4zOTYgLTEuNDM5LC0wLjgyIC0xLjk0LC0yLjY1MiAtMS4xMiwtNC4wOTIgbCAzOSwtNjguMzc0IGMgMC44MjEsLTEuNDM5IDIuNjU0LC0xLjk0MSA0LjA5MiwtMS4xMiAxLjQzOSwwLjgyMSAxLjk0LDIuNjUzIDEuMTE5LDQuMDkyIGwgLTM5LDY4LjM3NCBDIDUuMDUzLDczLjgzMyA0LjA0LDc0LjM3NyAyLjk5OCw3NC4zNzcgWiIKICAgICAgICAgICAgIGlkPSJwYXRoNC0wIgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgICBpZD0icGF0aDYtOSIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmYzk0ZTtmaWxsLW9wYWNpdHk6MC45ODgyMzUiCiAgICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgICAgaWQ9InBhdGg4LTkiCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgcG9pbnRzPSI0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCAiCiAgICAgICAgICAgICBpZD0icG9seWdvbjEwLTgiIC8+CiAgICAgICAgPC9nPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="&quot;rot_z&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" name="3" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SvgMarker" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuNDM1OCAyNTE1Ljc2NzgiCiAgIGlkPSJzdmc4MzEzIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMi4yICg3MzJhMDFkYTYzLCAyMDIyLTEyLTA5KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic2RlPWFybW9pcmVfZGVwb3Nlci5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSmVhbiBDbGF1ZGVcRGVza3RvcFxwaG90b3MgY2VsbHVsZXNcc21kZXY9YXJtb2lyZS5zdmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM4MzE1Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODgwMCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMGZlO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3OTgiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NzkyIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwZmY7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODc5MCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg3ODQiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDBmNDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4NzgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODc3OCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3NzYiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NzcwIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODc2OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg3NTgiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4NzU2IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODc1MCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3NDgiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iRG90TCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkRvdEwiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDg1MjMiCiAgICAgICAgIGQ9Im0gLTIuNSwtMSBjIDAsMi43NiAtMi4yNCw1IC01LDUgLTIuNzYsMCAtNSwtMi4yNCAtNSwtNSAwLC0yLjc2IDIuMjQsLTUgNSwtNSAyLjc2LDAgNSwyLjI0IDUsNSB6IgogICAgICAgICBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC44LDAsMCwwLjgsNS45MiwwLjgpIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDk3MTIiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0idHJhbnNsYXRlKDM4LjE2MjY1OSwxOTQ1LjE1OCkiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDBmZjtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NzE0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50OTcxMiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgyNDIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIHgxPSIxOS45OTg0MyIKICAgICAgIHkxPSI2Ny42NjkyOTYiCiAgICAgICB4Mj0iODAuNTAyNzY5IgogICAgICAgeTI9IjY3LjY2OTI5NiIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIuODg3NDM5MywtMTAuNTg3Mjc4KSIgLz4KICAgIDxtYXJrZXIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9Im1hcmtlcjExMzIxIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBpZD0icGF0aDExMzIzIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjYyNTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4LjcxODU4NzgsNC4wMzM3MzUyIC0yLjIwNzI4OTUsMC4wMTYwMTMyNiA4LjcxODU4ODQsLTQuMDAxNzA3OCBjIC0xLjc0NTQ5ODQsMi4zNzIwNjA5IC0xLjczNTQ0MDgsNS42MTc0NTE5IC02ZS03LDguMDM1NDQzIHoiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0xLjEsMCwwLC0xLjEsLTEuMSwwKSIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpZD0ibWFya2VyODQ5MyIKICAgICAgIHJlZlg9IjAiCiAgICAgICByZWZZPSIwIgogICAgICAgb3JpZW50PSJhdXRvIj4KICAgICAgPHBhdGgKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTEuMSwwLDAsLTEuMSwtMS4xLDApIgogICAgICAgICBkPSJNIDguNzE4NTg3OCw0LjAzMzczNTIgLTIuMjA3Mjg5NSwwLjAxNjAxMzI2IDguNzE4NTg4NCwtNC4wMDE3MDc4IGMgLTEuNzQ1NDk4NCwyLjM3MjA2MDkgLTEuNzM1NDQwOCw1LjYxNzQ1MTkgLTZlLTcsOC4wMzU0NDMgeiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGlkPSJwYXRoODQ5NSIgLz4KICAgIDwvbWFya2VyPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9ImJhc2UiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMC4wIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4yMjYyNzQxNyIKICAgICBpbmtzY2FwZTpjeD0iMTE4NC40MDM5IgogICAgIGlua3NjYXBlOmN5PSIxNzE0LjczNCIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ibGF5ZXIxIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGJvcmRlcmxheWVyPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjQ1MjIuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjM3NTQuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MTA2IgogICAgICAgb3JpZ2lueD0iMCIKICAgICAgIG9yaWdpbnk9IjAiCiAgICAgICBzcGFjaW5neD0iMSIKICAgICAgIHNwYWNpbmd5PSIxIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTgzMTgiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxnCiAgICAgaW5rc2NhcGU6bGFiZWw9IkNhbHF1ZSAxIgogICAgIGlua3NjYXBlOmdyb3VwbW9kZT0ibGF5ZXIiCiAgICAgaWQ9ImxheWVyMSIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDQyOTguMDMyNSkiPgogICAgPGcKICAgICAgIGlkPSJnNDI3IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwwLjk5OTk5OTk3LC0xLjAwMDAwMTIsMCwxMDcwLjA3OTEsLTQyODAuMzE2KSI+CiAgICAgIDxyZWN0CiAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiNjY2NjY2M7c3Ryb2tlLXdpZHRoOjEyMy45NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBpZD0icmVjdDkwMCIKICAgICAgICAgd2lkdGg9IjIzNTYuMzc0OCIKICAgICAgICAgaGVpZ2h0PSI1MTkxLjAyMDUiCiAgICAgICAgIHg9IjYxLjk3MDE0MiIKICAgICAgICAgeT0iLTQyMDAuNjI5NCIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjIxMjIuMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIKICAgICAgICAgZD0iTSAxODA0LjQ4NTMsOTUwLjk1MjY2IEggMTI0Mi4xOTQxIFYgLTMyOS4zMjY1IC0xNjA5LjYwNTcgaCA1NjIuMjkxMiA1NjIuMjkxMyBWIC0zMjkuMzI2NSA5NTAuOTUyNjYgWiIKICAgICAgICAgaWQ9InBhdGg0ODMxIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MjE0Ni4yMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIKICAgICAgICAgZD0iTSA2NjAuNDU5MjEsLTE2MDkuNjA1NyBIIDc4LjcyNDIgdiAtMTI4NS45ODIgLTEyODUuOTgyNSBoIDU4MS43MzUwMSA1ODEuNzM0ODkgdiAxMjg1Ljk4MjUgMTI4NS45ODIgeiIKICAgICAgICAgaWQ9InBhdGg0ODI5IgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8ZwogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTUxLDAsMTQ0MS43MjM5LC0yNDIxLjgxMzMpIgogICAgICAgICBpZD0iZzQ3NjYiCiAgICAgICAgIHN0eWxlPSJmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjEiPgogICAgICAgIDxsaW5lCiAgICAgICAgICAgaWQ9ImxpbmUyIgogICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgeDI9IjMiCiAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg0IgogICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg2IgogICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg4IgogICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cG9seWdvbgogICAgICAgICAgIGlkPSJwb2x5Z29uMTAiCiAgICAgICAgICAgcG9pbnRzPSI0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgPC9nPgogICAgICA8ZwogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTQ3LDAsMjcyLjcwNTksMTc1LjMyMjcpIgogICAgICAgICBpZD0iZzQ3NjYtOSIKICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSI+CiAgICAgICAgPGxpbmUKICAgICAgICAgICBpZD0ibGluZTItNCIKICAgICAgICAgICB5Mj0iNzEuMzc1IgogICAgICAgICAgIHgyPSIzIgogICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIGlkPSJwYXRoNC0wIgogICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg2LTkiCiAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojY2NjY2NjO2ZpbGwtb3BhY2l0eToxIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDgtOSIKICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHBvbHlnb24KICAgICAgICAgICBpZD0icG9seWdvbjEwLTgiCiAgICAgICAgICAgcG9pbnRzPSIzOS4yOTEsNDIuMzggNDYuMzQxLDI2Ljg0NSA0MC43MDEsMjYuODQ1IDM0LjMwOCw0OS4xNDkgNDMuNTIxLDQ1LjQ4MyA0MC43MDEsNTYuMTA1IDM4LjYzMyw1Ni4yNDcgNDEuMzU5LDYzLjUzMyA0Ni4xNTMsNTYuNDM2IDQ0LjE3OCw1Ni40ODIgNDkuMDY4LDM4LjUyNiAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgPC9nPgogICAgPC9nPgogIDwvZz4KPC9zdmc+Cg==" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="&quot;rot_z&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{ef776389-cc80-43b0-ae4d-084bbc5d85ce}">
      <rule filter="&quot;etat&quot; = 'A remplacer'" key="{213deb1b-782d-4d38-a1ee-2b0471272965}" description="étiquettes armoire à remplacer">
        <settings calloutType="simple">
          <text-style fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" textOpacity="1" fontSize="1.5" isExpression="1" fontUnderline="0" fontItalic="0" fontWeight="75" blendMode="0" capitalization="0" fontWordSpacing="0" multilineHeight="1" textColor="255,201,78,255" multilineHeightUnit="Percentage" allowHtml="0" legendString="Aa" textOrientation="horizontal" previewBkgrdColor="0,0,0,255" fontKerning="1" forcedBold="0" namedStyle="Bold" fontStrikeout="0" fieldName="'armoire à remplacer : '  || id_armoire" useSubstitutions="0" fontSizeUnit="MM" forcedItalic="0" fontFamily="Arial" fontLetterSpacing="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255" bufferNoFill="0" bufferOpacity="1" bufferJoinStyle="128" bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MapUnit" bufferSize="0.20000000000000001" bufferBlendMode="0"/>
            <text-mask maskType="0" maskOpacity="1" maskSize="0" maskEnabled="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeUnits="MM" maskSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <background shapeType="0" shapeOpacity="1" shapeSizeType="0" shapeFillColor="255,255,255,255" shapeOffsetX="0" shapeOffsetUnit="MM" shapeBorderWidthUnit="MM" shapeBorderColor="255,201,78,255" shapeRotationType="1" shapeSizeX="0.14999999999999999" shapeRadiiX="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0.14999999999999999" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="1" shapeRadiiUnit="MM" shapeSizeY="0.14999999999999999" shapeSVGFile="" shapeRotation="0" shapeJoinStyle="64" shapeBlendMode="0" shapeSizeUnit="MapUnit">
              <symbol force_rhr="0" alpha="1" name="markerSymbol" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
                  <Option type="Map">
                    <Option value="0" name="angle" type="QString"/>
                    <Option value="square" name="cap_style" type="QString"/>
                    <Option value="152,125,183,255" name="color" type="QString"/>
                    <Option value="1" name="horizontal_anchor_point" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="circle" name="name" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="35,35,35,255" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="diameter" name="scale_method" type="QString"/>
                    <Option value="2" name="size" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                    <Option value="MM" name="size_unit" type="QString"/>
                    <Option value="1" name="vertical_anchor_point" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol force_rhr="0" alpha="1" name="fillSymbol" is_animated="0" frame_rate="10" type="fill" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" class="SimpleFill" enabled="1" locked="0">
                  <Option type="Map">
                    <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
                    <Option value="255,255,255,255" name="color" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="255,201,78,255" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0.15" name="outline_width" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="solid" name="style" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowOffsetAngle="135" shadowDraw="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowOpacity="0.69999999999999996" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowBlendMode="6" shadowOffsetUnit="MM" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowColor="0,0,0,255" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetDist="1"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format plussign="0" reverseDirectionSymbol="0" formatNumbers="0" autoWrapLength="0" rightDirectionSymbol=">" wrapChar="" addDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" placeDirectionSymbol="0" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3"/>
          <placement centroidInside="0" allowDegraded="0" lineAnchorType="0" distMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorEnabled="0" rotationAngle="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="0" overlapHandling="PreventOverlap" offsetUnits="MapUnit" lineAnchorTextPoint="CenterOfText" centroidWhole="0" rotationUnit="AngleDegrees" geometryGenerator="" placementFlags="10" distUnits="MapUnit" lineAnchorPercent="0.5" priority="5" geometryGeneratorType="PointGeometry" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" xOffset="0" overrunDistanceUnit="MM" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" yOffset="0" maxCurvedCharAngleOut="-25" offsetType="1" lineAnchorClipping="0" overrunDistance="0" repeatDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" polygonPlacementFlags="2" maxCurvedCharAngleIn="25" dist="5" fitInPolygonOnly="0" preserveRotation="1" layerType="PointGeometry" quadOffset="4"/>
          <rendering obstacleType="0" mergeLines="0" minFeatureSize="0" fontMinPixelSize="1" unplacedVisibility="0" drawLabels="1" maxNumLabels="2000" scaleMin="1" scaleMax="5000" fontMaxPixelSize="10000" obstacle="1" obstacleFactor="2" labelPerPart="0" fontLimitPixelSize="0" zIndex="0" limitNumLabels="0" upsidedownLabels="0" scaleVisibility="1"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="LabelRotation" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="rot_z" name="field" type="QString"/>
                  <Option value="2" name="type" type="int"/>
                </Option>
                <Option name="ObstacleFactor" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
                <Option name="OffsetQuad" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
              <Option value="0" name="blendMode" type="int"/>
              <Option name="ddProperties" type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
              <Option value="false" name="drawToAllParts" type="bool"/>
              <Option value="1" name="enabled" type="QString"/>
              <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
              <Option value="&lt;symbol force_rhr=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; is_animated=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; class=&quot;ArrowLine&quot; enabled=&quot;1&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;0.03&quot; name=&quot;arrow_start_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;arrow_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;arrow_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.15&quot; name=&quot;head_length&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;head_thickness&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;head_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_curved&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_repeated&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;ring_filter&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol force_rhr=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; is_animated=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; class=&quot;SimpleFill&quot; enabled=&quot;1&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,201,78,255&quot; name=&quot;color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;bevel&quot; name=&quot;joinstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0,0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,255,255,255&quot; name=&quot;outline_color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;outline_style&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.009&quot; name=&quot;outline_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;style&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol" type="QString"/>
              <Option value="0" name="minLength" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale" type="QString"/>
              <Option value="MM" name="minLengthUnit" type="QString"/>
              <Option value="0" name="offsetFromAnchor" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
              <Option value="0" name="offsetFromLabel" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromLabelUnit" type="QString"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule filter="&quot;etat&quot; = 'A poser'" key="{ebd408da-90e8-4c1e-84b2-5e1e55d4e6ee}" description="étiquettes armmoire à poser">
        <settings calloutType="simple">
          <text-style fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" textOpacity="1" fontSize="1.5" isExpression="1" fontUnderline="0" fontItalic="0" fontWeight="75" blendMode="0" capitalization="0" fontWordSpacing="0" multilineHeight="1" textColor="255,0,252,255" multilineHeightUnit="Percentage" allowHtml="0" legendString="Aa" textOrientation="horizontal" previewBkgrdColor="0,0,0,255" fontKerning="1" forcedBold="0" namedStyle="Bold" fontStrikeout="0" fieldName="'armoire à poser : '  ||  &quot;id_armoire&quot; " useSubstitutions="0" fontSizeUnit="MM" forcedItalic="0" fontFamily="Arial" fontLetterSpacing="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255" bufferNoFill="0" bufferOpacity="1" bufferJoinStyle="128" bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MapUnit" bufferSize="0.20000000000000001" bufferBlendMode="0"/>
            <text-mask maskType="0" maskOpacity="1" maskSize="0" maskEnabled="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeUnits="MM" maskSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <background shapeType="0" shapeOpacity="1" shapeSizeType="0" shapeFillColor="255,255,255,255" shapeOffsetX="0" shapeOffsetUnit="MM" shapeBorderWidthUnit="MM" shapeBorderColor="255,0,252,255" shapeRotationType="1" shapeSizeX="0.14999999999999999" shapeRadiiX="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0.14999999999999999" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="1" shapeRadiiUnit="MM" shapeSizeY="0.14999999999999999" shapeSVGFile="" shapeRotation="0" shapeJoinStyle="64" shapeBlendMode="0" shapeSizeUnit="MapUnit">
              <symbol force_rhr="0" alpha="1" name="markerSymbol" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
                  <Option type="Map">
                    <Option value="0" name="angle" type="QString"/>
                    <Option value="square" name="cap_style" type="QString"/>
                    <Option value="152,125,183,255" name="color" type="QString"/>
                    <Option value="1" name="horizontal_anchor_point" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="circle" name="name" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="35,35,35,255" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="diameter" name="scale_method" type="QString"/>
                    <Option value="2" name="size" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                    <Option value="MM" name="size_unit" type="QString"/>
                    <Option value="1" name="vertical_anchor_point" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol force_rhr="0" alpha="1" name="fillSymbol" is_animated="0" frame_rate="10" type="fill" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" class="SimpleFill" enabled="1" locked="0">
                  <Option type="Map">
                    <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
                    <Option value="255,255,255,255" name="color" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="255,0,252,255" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0.15" name="outline_width" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="solid" name="style" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowOffsetAngle="135" shadowDraw="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowOpacity="0.69999999999999996" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowBlendMode="6" shadowOffsetUnit="MM" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowColor="0,0,0,255" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetDist="1"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format plussign="0" reverseDirectionSymbol="0" formatNumbers="0" autoWrapLength="0" rightDirectionSymbol=">" wrapChar="" addDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" placeDirectionSymbol="0" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3"/>
          <placement centroidInside="0" allowDegraded="0" lineAnchorType="0" distMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorEnabled="0" rotationAngle="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="0" overlapHandling="PreventOverlap" offsetUnits="MapUnit" lineAnchorTextPoint="CenterOfText" centroidWhole="0" rotationUnit="AngleDegrees" geometryGenerator="" placementFlags="10" distUnits="MapUnit" lineAnchorPercent="0.5" priority="5" geometryGeneratorType="PointGeometry" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" xOffset="0" overrunDistanceUnit="MM" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" yOffset="0" maxCurvedCharAngleOut="-25" offsetType="1" lineAnchorClipping="0" overrunDistance="0" repeatDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" polygonPlacementFlags="2" maxCurvedCharAngleIn="25" dist="5" fitInPolygonOnly="0" preserveRotation="1" layerType="PointGeometry" quadOffset="4"/>
          <rendering obstacleType="0" mergeLines="0" minFeatureSize="0" fontMinPixelSize="1" unplacedVisibility="0" drawLabels="1" maxNumLabels="2000" scaleMin="1" scaleMax="5000" fontMaxPixelSize="10000" obstacle="1" obstacleFactor="2" labelPerPart="0" fontLimitPixelSize="0" zIndex="0" limitNumLabels="0" upsidedownLabels="0" scaleVisibility="1"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="LabelRotation" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="rot_z" name="field" type="QString"/>
                  <Option value="2" name="type" type="int"/>
                </Option>
                <Option name="ObstacleFactor" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
                <Option name="OffsetQuad" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
              <Option value="0" name="blendMode" type="int"/>
              <Option name="ddProperties" type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
              <Option value="false" name="drawToAllParts" type="bool"/>
              <Option value="1" name="enabled" type="QString"/>
              <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
              <Option value="&lt;symbol force_rhr=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; is_animated=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; class=&quot;ArrowLine&quot; enabled=&quot;1&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;0.03&quot; name=&quot;arrow_start_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;arrow_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;arrow_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.15&quot; name=&quot;head_length&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;head_thickness&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;head_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_curved&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_repeated&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;ring_filter&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol force_rhr=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; is_animated=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; class=&quot;SimpleFill&quot; enabled=&quot;1&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,0,252,255&quot; name=&quot;color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;bevel&quot; name=&quot;joinstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0,0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,255,255,255&quot; name=&quot;outline_color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;outline_style&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.009&quot; name=&quot;outline_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;style&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol" type="QString"/>
              <Option value="0" name="minLength" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale" type="QString"/>
              <Option value="MM" name="minLengthUnit" type="QString"/>
              <Option value="0" name="offsetFromAnchor" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
              <Option value="0" name="offsetFromLabel" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromLabelUnit" type="QString"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule filter="&quot;etat&quot; = 'Actif'" key="{ce514e54-3902-44d7-a624-59d555c25e74}" description="texte armoire">
        <settings calloutType="balloon">
          <text-style fontSizeMapUnitScale="3x:0,0,0,0,0,0" textOpacity="1" fontSize="1.5" isExpression="1" fontUnderline="0" fontItalic="1" fontWeight="75" blendMode="0" capitalization="0" fontWordSpacing="0" multilineHeight="1" textColor="21,0,255,255" multilineHeightUnit="Percentage" allowHtml="0" legendString="Aa" textOrientation="horizontal" previewBkgrdColor="255,255,255,255" fontKerning="1" forcedBold="0" namedStyle="Bold Italic" fontStrikeout="0" fieldName="'armoire : ' || &quot;id_armoire&quot;" useSubstitutions="0" fontSizeUnit="MM" forcedItalic="0" fontFamily="Arial" fontLetterSpacing="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255" bufferNoFill="0" bufferOpacity="1" bufferJoinStyle="128" bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM" bufferSize="0" bufferBlendMode="0"/>
            <text-mask maskType="0" maskOpacity="1" maskSize="0" maskEnabled="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeUnits="MM" maskSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <background shapeType="0" shapeOpacity="1" shapeSizeType="0" shapeFillColor="255,255,255,255" shapeOffsetX="0" shapeOffsetUnit="Point" shapeBorderWidthUnit="Point" shapeBorderColor="128,128,128,255" shapeRotationType="0" shapeSizeX="0" shapeRadiiX="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeRadiiUnit="Point" shapeSizeY="0" shapeSVGFile="" shapeRotation="0" shapeJoinStyle="64" shapeBlendMode="0" shapeSizeUnit="Point">
              <symbol force_rhr="0" alpha="1" name="markerSymbol" is_animated="0" frame_rate="10" type="marker" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
                  <Option type="Map">
                    <Option value="0" name="angle" type="QString"/>
                    <Option value="square" name="cap_style" type="QString"/>
                    <Option value="152,125,183,255" name="color" type="QString"/>
                    <Option value="1" name="horizontal_anchor_point" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="circle" name="name" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="35,35,35,255" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="diameter" name="scale_method" type="QString"/>
                    <Option value="2" name="size" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                    <Option value="MM" name="size_unit" type="QString"/>
                    <Option value="1" name="vertical_anchor_point" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol force_rhr="0" alpha="1" name="fillSymbol" is_animated="0" frame_rate="10" type="fill" clip_to_extent="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" class="SimpleFill" enabled="1" locked="0">
                  <Option type="Map">
                    <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
                    <Option value="255,255,255,255" name="color" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="128,128,128,255" name="outline_color" type="QString"/>
                    <Option value="no" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="Point" name="outline_width_unit" type="QString"/>
                    <Option value="solid" name="style" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowOffsetAngle="135" shadowDraw="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowOpacity="1" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowBlendMode="6" shadowOffsetUnit="Point" shadowOffsetGlobal="1" shadowRadiusUnit="Point" shadowColor="0,0,0,255" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetDist="1"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format plussign="0" reverseDirectionSymbol="0" formatNumbers="0" autoWrapLength="0" rightDirectionSymbol=">" wrapChar="" addDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" placeDirectionSymbol="0" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3"/>
          <placement centroidInside="0" allowDegraded="0" lineAnchorType="0" distMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorEnabled="0" rotationAngle="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="6" overlapHandling="PreventOverlap" offsetUnits="MM" lineAnchorTextPoint="CenterOfText" centroidWhole="0" rotationUnit="AngleDegrees" geometryGenerator="" placementFlags="10" distUnits="MM" lineAnchorPercent="0.5" priority="5" geometryGeneratorType="PointGeometry" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" xOffset="0" overrunDistanceUnit="MM" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" yOffset="0" maxCurvedCharAngleOut="-25" offsetType="1" lineAnchorClipping="0" overrunDistance="0" repeatDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" polygonPlacementFlags="2" maxCurvedCharAngleIn="25" dist="12" fitInPolygonOnly="0" preserveRotation="1" layerType="PointGeometry" quadOffset="4"/>
          <rendering obstacleType="1" mergeLines="0" minFeatureSize="0" fontMinPixelSize="3" unplacedVisibility="0" drawLabels="1" maxNumLabels="2000" scaleMin="0" scaleMax="0" fontMaxPixelSize="10000" obstacle="1" obstacleFactor="1" labelPerPart="0" fontLimitPixelSize="0" zIndex="0" limitNumLabels="0" upsidedownLabels="0" scaleVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="PositionX" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option value="" name="val" type="QString"/>
                </Option>
                <Option name="PositionY" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option value="" name="val" type="QString"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </dd_properties>
          <callout type="balloon">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
              <Option value="0" name="blendMode" type="int"/>
              <Option value="0.5" name="cornerRadius" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="cornerRadiusMapUnitScale" type="QString"/>
              <Option value="MM" name="cornerRadiusUnit" type="QString"/>
              <Option name="ddProperties" type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties" type="Map">
                  <Option name="OriginX" type="Map">
                    <Option value="false" name="active" type="bool"/>
                    <Option value="1" name="type" type="int"/>
                    <Option value="" name="val" type="QString"/>
                  </Option>
                  <Option name="OriginY" type="Map">
                    <Option value="false" name="active" type="bool"/>
                    <Option value="1" name="type" type="int"/>
                    <Option value="" name="val" type="QString"/>
                  </Option>
                </Option>
                <Option value="collection" name="type" type="QString"/>
              </Option>
              <Option value="1" name="enabled" type="QString"/>
              <Option value="&lt;symbol force_rhr=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; is_animated=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; class=&quot;SimpleFill&quot; enabled=&quot;1&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;204,204,204,255&quot; name=&quot;color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;bevel&quot; name=&quot;joinstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0,0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;Point&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;21,0,255,255&quot; name=&quot;outline_color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;outline_style&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;outline_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;outline_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;style&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="fillSymbol" type="QString"/>
              <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
              <Option value="0.5,0.5,0.5,0.5" name="margins" type="QString"/>
              <Option value="MM" name="marginsUnit" type="QString"/>
              <Option value="0" name="offsetFromAnchor" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
              <Option value="1" name="wedgeWidth" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="wedgeWidthMapUnitScale" type="QString"/>
              <Option value="MM" name="wedgeWidthUnit" type="QString"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option value="remove" name="QFieldSync/action" type="QString"/>
      <Option value="offline" name="QFieldSync/cloud_action" type="QString"/>
      <Option value="{&quot;photo&quot;: &quot;'DCIM/armoires_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;, &quot;schema_elec&quot;: &quot;'DCIM/armoires_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;}" name="QFieldSync/photo_naming" type="QString"/>
      <Option name="dualview/previewExpressions" type="List">
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
      </Option>
      <Option value="0" name="embeddedWidgets/count" type="QString"/>
      <Option value="false" name="geopdf/includeFeatures" type="QString"/>
      <Option value="true" name="qgis2web/Interactive" type="QString"/>
      <Option value="true" name="qgis2web/Visible" type="QString"/>
      <Option value="no label" name="qgis2web/popup/acteur" type="QString"/>
      <Option value="no label" name="qgis2web/popup/cellule" type="QString"/>
      <Option value="no label" name="qgis2web/popup/created_at" type="QString"/>
      <Option value="no label" name="qgis2web/popup/date_action" type="QString"/>
      <Option value="no label" name="qgis2web/popup/date_depose" type="QString"/>
      <Option value="no label" name="qgis2web/popup/date_pose" type="QString"/>
      <Option value="no label" name="qgis2web/popup/desc_action" type="QString"/>
      <Option value="no label" name="qgis2web/popup/ecp_intervention_id" type="QString"/>
      <Option value="no label" name="qgis2web/popup/enveloppe_pose" type="QString"/>
      <Option value="no label" name="qgis2web/popup/enveloppe_poste_amont" type="QString"/>
      <Option value="no label" name="qgis2web/popup/etat" type="QString"/>
      <Option value="no label" name="qgis2web/popup/fermeture" type="QString"/>
      <Option value="no label" name="qgis2web/popup/id_armoire" type="QString"/>
      <Option value="no label" name="qgis2web/popup/insee" type="QString"/>
      <Option value="no label" name="qgis2web/popup/intervention" type="QString"/>
      <Option value="no label" name="qgis2web/popup/mapid" type="QString"/>
      <Option value="no label" name="qgis2web/popup/mslink" type="QString"/>
      <Option value="no label" name="qgis2web/popup/nbre_depart" type="QString"/>
      <Option value="no label" name="qgis2web/popup/nom_voie" type="QString"/>
      <Option value="no label" name="qgis2web/popup/numero" type="QString"/>
      <Option value="no label" name="qgis2web/popup/numero_voie" type="QString"/>
      <Option value="no label" name="qgis2web/popup/photo" type="QString"/>
      <Option value="no label" name="qgis2web/popup/pkid" type="QString"/>
      <Option value="no label" name="qgis2web/popup/remarque" type="QString"/>
      <Option value="no label" name="qgis2web/popup/rot_z" type="QString"/>
      <Option value="no label" name="qgis2web/popup/schema_elec" type="QString"/>
      <Option value="no label" name="qgis2web/popup/type_voie" type="QString"/>
      <Option value="no label" name="qgis2web/popup/uid4" type="QString"/>
      <Option value="no label" name="qgis2web/popup/updated_at" type="QString"/>
      <Option value="no label" name="qgis2web/popup/vetuste" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory lineSizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" minScaleDenominator="1" backgroundAlpha="255" enabled="0" penColor="#000000" spacingUnit="MM" showAxis="0" width="15" height="15" spacing="0" penWidth="0" scaleDependency="Area" rotationOffset="270" labelPlacementMethod="XHeight" backgroundColor="#ffffff" barWidth="5" diagramOrientation="Up" opacity="1" sizeScale="3x:0,0,0,0,0,0" spacingUnitScale="3x:0,0,0,0,0,0" sizeType="MM" direction="1" maxScaleDenominator="1e+08" minimumSize="0" penAlpha="255" scaleBasedVisibility="0">
      <fontProperties underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
      <attribute label="" field="" color="#000000" colorOpacity="1"/>
      <axisSymbol>
        <symbol force_rhr="0" alpha="1" name="" is_animated="0" frame_rate="10" type="line" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" class="SimpleLine" enabled="1" locked="0">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="0" zIndex="0" obstacle="0" dist="0" priority="0" showAll="1" linePlacementFlags="18">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numero" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="insee" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_armoire" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="numero_voie" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="type_voie" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_voie" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="etat" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Actif" name="Actif" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="A déposer" name="A déposer" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="vetuste" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Bon" name="Bon" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Moyen" name="Moyen" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Vétuste" name="Vétuste" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fermeture" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Clés" name="Clés" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Triangle" name="Triangle" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Plat" name="Plat" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enveloppe_poste_amont" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enveloppe_pose" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Autre" name="Autre" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Encastrée" name="Encastrée" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Saillie" name="Saillie" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Sur poteau de distribution" name="Sur poteau de distribution" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Sur façade" name="Sur façade" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nbre_depart" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cellule" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="date_pose" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="dd/MM/yyyy" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date_depose" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date_action" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="desc_action" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="remarque" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="acteur" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="mslink" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ecp_intervention_id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="photo" configurationFlags="None">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="P:/SIG/documents/photos" name="DefaultRoot" type="QString"/>
            <Option value="1" name="DocumentViewer" type="int"/>
            <Option value="0" name="DocumentViewerHeight" type="int"/>
            <Option value="0" name="DocumentViewerWidth" type="int"/>
            <Option value="false" name="FileWidget" type="bool"/>
            <Option value="false" name="FileWidgetButton" type="bool"/>
            <Option value="" name="FileWidgetFilter" type="QString"/>
            <Option name="PropertyCollection" type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="invalid"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
            <Option value="1" name="RelativeStorage" type="int"/>
            <Option value="0" name="StorageMode" type="int"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="schema_elec" configurationFlags="None">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="P:/SIG/documents/schemas" name="DefaultRoot" type="QString"/>
            <Option value="0" name="DocumentViewer" type="int"/>
            <Option value="0" name="DocumentViewerHeight" type="int"/>
            <Option value="0" name="DocumentViewerWidth" type="int"/>
            <Option value="true" name="FileWidget" type="bool"/>
            <Option value="false" name="FileWidgetButton" type="bool"/>
            <Option value="" name="FileWidgetFilter" type="QString"/>
            <Option name="PropertyCollection" type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="documentViewerContent" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option value="" name="val" type="QString"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
            <Option value="0" name="RelativeStorage" type="int"/>
            <Option value="1" name="StorageMode" type="int"/>
            <Option value="true" name="UseLink" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="created_at" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd HH:mm:ss" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="updated_at" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pkid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="intervention" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="rot_z" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="uid4" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="fid"/>
    <alias name="" index="1" field="numero"/>
    <alias name="numéro insee commune" index="2" field="insee"/>
    <alias name="identifiant" index="3" field="id_armoire"/>
    <alias name="Numéro rue" index="4" field="numero_voie"/>
    <alias name="Type (rue, route, chemin.....)" index="5" field="type_voie"/>
    <alias name="Nom de la rue" index="6" field="nom_voie"/>
    <alias name="Etat" index="7" field="etat"/>
    <alias name="Vétusté" index="8" field="vetuste"/>
    <alias name="Fermeture" index="9" field="fermeture"/>
    <alias name="Rattachement au poste de distribution publique" index="10" field="enveloppe_poste_amont"/>
    <alias name="Type de pose de l'enveloppe" index="11" field="enveloppe_pose"/>
    <alias name="Nombre de départ" index="12" field="nbre_depart"/>
    <alias name="" index="13" field="cellule"/>
    <alias name="Date de pose" index="14" field="date_pose"/>
    <alias name="Date de dépose" index="15" field="date_depose"/>
    <alias name="" index="16" field="date_action"/>
    <alias name="" index="17" field="desc_action"/>
    <alias name="Remarque" index="18" field="remarque"/>
    <alias name="chargé d'affaires" index="19" field="acteur"/>
    <alias name="" index="20" field="mslink"/>
    <alias name="" index="21" field="mapid"/>
    <alias name="identifiant interne intervention" index="22" field="ecp_intervention_id"/>
    <alias name="intérieur/extérieur" index="23" field="photo"/>
    <alias name="cliquez sur le lien" index="24" field="schema_elec"/>
    <alias name="" index="25" field="created_at"/>
    <alias name="" index="26" field="updated_at"/>
    <alias name="" index="27" field="pkid"/>
    <alias name="" index="28" field="intervention"/>
    <alias name="" index="29" field="rot_z"/>
    <alias name="" index="30" field="uid4"/>
  </aliases>
  <defaults>
    <default field="fid" applyOnUpdate="0" expression=""/>
    <default field="numero" applyOnUpdate="0" expression=""/>
    <default field="insee" applyOnUpdate="1" expression=" @lumigis_insee "/>
    <default field="id_armoire" applyOnUpdate="0" expression=""/>
    <default field="numero_voie" applyOnUpdate="0" expression=""/>
    <default field="type_voie" applyOnUpdate="0" expression=""/>
    <default field="nom_voie" applyOnUpdate="0" expression=""/>
    <default field="etat" applyOnUpdate="0" expression=""/>
    <default field="vetuste" applyOnUpdate="0" expression=""/>
    <default field="fermeture" applyOnUpdate="0" expression=""/>
    <default field="enveloppe_poste_amont" applyOnUpdate="0" expression=""/>
    <default field="enveloppe_pose" applyOnUpdate="0" expression=""/>
    <default field="nbre_depart" applyOnUpdate="0" expression=""/>
    <default field="cellule" applyOnUpdate="0" expression=""/>
    <default field="date_pose" applyOnUpdate="0" expression=""/>
    <default field="date_depose" applyOnUpdate="0" expression=""/>
    <default field="date_action" applyOnUpdate="0" expression=""/>
    <default field="desc_action" applyOnUpdate="0" expression=""/>
    <default field="remarque" applyOnUpdate="0" expression=""/>
    <default field="acteur" applyOnUpdate="1" expression=" @lumigis_technicien "/>
    <default field="mslink" applyOnUpdate="0" expression=""/>
    <default field="mapid" applyOnUpdate="0" expression=""/>
    <default field="ecp_intervention_id" applyOnUpdate="1" expression=" &quot;ecp_intervention_id&quot; "/>
    <default field="photo" applyOnUpdate="0" expression=""/>
    <default field="schema_elec" applyOnUpdate="0" expression=""/>
    <default field="created_at" applyOnUpdate="0" expression=""/>
    <default field="updated_at" applyOnUpdate="0" expression=""/>
    <default field="pkid" applyOnUpdate="0" expression=""/>
    <default field="intervention" applyOnUpdate="1" expression=" @lumigis_intervention "/>
    <default field="rot_z" applyOnUpdate="0" expression=""/>
    <default field="uid4" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" constraints="3" unique_strength="1" field="fid" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="3" unique_strength="1" field="numero" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="insee" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="id_armoire" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="numero_voie" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="type_voie" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="nom_voie" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="1" unique_strength="0" field="etat" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="1" unique_strength="0" field="vetuste" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="1" unique_strength="0" field="fermeture" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="enveloppe_poste_amont" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="1" unique_strength="0" field="enveloppe_pose" notnull_strength="1"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="nbre_depart" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="cellule" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="date_pose" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="date_depose" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="date_action" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="desc_action" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="remarque" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="acteur" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="mslink" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="mapid" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="ecp_intervention_id" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="photo" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="schema_elec" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="created_at" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="updated_at" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="pkid" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="intervention" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="rot_z" notnull_strength="0"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" field="uid4" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="fid" desc=""/>
    <constraint exp="" field="numero" desc=""/>
    <constraint exp="" field="insee" desc=""/>
    <constraint exp="" field="id_armoire" desc=""/>
    <constraint exp="" field="numero_voie" desc=""/>
    <constraint exp="" field="type_voie" desc=""/>
    <constraint exp="" field="nom_voie" desc=""/>
    <constraint exp="" field="etat" desc=""/>
    <constraint exp="" field="vetuste" desc=""/>
    <constraint exp="" field="fermeture" desc=""/>
    <constraint exp="" field="enveloppe_poste_amont" desc=""/>
    <constraint exp="" field="enveloppe_pose" desc=""/>
    <constraint exp="" field="nbre_depart" desc=""/>
    <constraint exp="" field="cellule" desc=""/>
    <constraint exp="" field="date_pose" desc=""/>
    <constraint exp="" field="date_depose" desc=""/>
    <constraint exp="" field="date_action" desc=""/>
    <constraint exp="" field="desc_action" desc=""/>
    <constraint exp="" field="remarque" desc=""/>
    <constraint exp="" field="acteur" desc=""/>
    <constraint exp="" field="mslink" desc=""/>
    <constraint exp="" field="mapid" desc=""/>
    <constraint exp="" field="ecp_intervention_id" desc=""/>
    <constraint exp="" field="photo" desc=""/>
    <constraint exp="" field="schema_elec" desc=""/>
    <constraint exp="" field="created_at" desc=""/>
    <constraint exp="" field="updated_at" desc=""/>
    <constraint exp="" field="pkid" desc=""/>
    <constraint exp="" field="intervention" desc=""/>
    <constraint exp="" field="rot_z" desc=""/>
    <constraint exp="" field="uid4" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{5c55e0f0-75c5-4988-87e5-e7f75979c44a}" key="Canvas"/>
    <actionsetting name="visualisation schéma" isEnabledOnlyWhenEditable="0" action="[%schema_elec%]" capture="0" shortTitle="" icon="" notificationMessage="" id="{82c346aa-d346-4561-968f-431458228657}" type="5">
      <actionScope id="Canvas"/>
      <actionScope id="Feature"/>
      <actionScope id="Layer"/>
    </actionsetting>
    <actionsetting name="visualisaton photos extérieur/intérieur" isEnabledOnlyWhenEditable="0" action="[%photo%]" capture="0" shortTitle="" icon="" notificationMessage="" id="{5c55e0f0-75c5-4988-87e5-e7f75979c44a}" type="5">
      <actionScope id="Canvas"/>
      <actionScope id="Feature"/>
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id_armoire&quot;" sortOrder="0" actionWidgetStyle="buttonList">
    <columns>
      <column name="intervention" width="-1" type="field" hidden="0"/>
      <column name="acteur" width="100" type="field" hidden="0"/>
      <column name="insee" width="-1" type="field" hidden="0"/>
      <column name="id_armoire" width="-1" type="field" hidden="0"/>
      <column name="numero_voie" width="-1" type="field" hidden="0"/>
      <column name="type_voie" width="-1" type="field" hidden="0"/>
      <column name="nom_voie" width="-1" type="field" hidden="0"/>
      <column name="etat" width="100" type="field" hidden="0"/>
      <column name="vetuste" width="100" type="field" hidden="0"/>
      <column name="fermeture" width="-1" type="field" hidden="0"/>
      <column name="enveloppe_poste_amont" width="252" type="field" hidden="0"/>
      <column name="enveloppe_pose" width="154" type="field" hidden="0"/>
      <column name="nbre_depart" width="-1" type="field" hidden="0"/>
      <column name="cellule" width="-1" type="field" hidden="0"/>
      <column name="date_pose" width="100" type="field" hidden="0"/>
      <column name="date_depose" width="100" type="field" hidden="0"/>
      <column name="mslink" width="-1" type="field" hidden="1"/>
      <column name="mapid" width="-1" type="field" hidden="1"/>
      <column name="ecp_intervention_id" width="143" type="field" hidden="1"/>
      <column name="photo" width="382" type="field" hidden="0"/>
      <column name="schema_elec" width="361" type="field" hidden="0"/>
      <column name="created_at" width="-1" type="field" hidden="1"/>
      <column name="updated_at" width="-1" type="field" hidden="1"/>
      <column name="remarque" width="-1" type="field" hidden="0"/>
      <column name="rot_z" width="-1" type="field" hidden="1"/>
      <column width="-1" type="actions" hidden="0"/>
      <column name="date_action" width="-1" type="field" hidden="0"/>
      <column name="desc_action" width="-1" type="field" hidden="0"/>
      <column name="uid4" width="-1" type="field" hidden="0"/>
      <column name="numero" width="-1" type="field" hidden="0"/>
      <column name="fid" width="-1" type="field" hidden="0"/>
      <column name="pkid" width="-1" type="field" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">P:/SIG/gestion</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>X:/AGENTS/Jean-Claude/transfert_shape/201607014pairetgrandrupt/QGIS</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
      <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
    </labelStyle>
    <attributeEditorContainer showLabel="1" groupBox="0" name="Fiche descriptive" collapsedExpression="" visibilityExpression="" columnCount="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" backgroundColor="#a6cee3">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
      </labelStyle>
      <attributeEditorContainer showLabel="1" groupBox="1" name="Généralités" collapsedExpression="" visibilityExpression="" columnCount="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" backgroundColor="#a6cee3">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
        </labelStyle>
        <attributeEditorField showLabel="1" name="intervention" index="28">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField showLabel="1" name="acteur" index="19">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField showLabel="1" name="insee" index="2">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" groupBox="1" name="Caractéristiques techniques" collapsedExpression="" visibilityExpression="" columnCount="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" backgroundColor="#a6cee3">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
        </labelStyle>
        <attributeEditorContainer showLabel="1" groupBox="1" name="Situation" collapsedExpression="" visibilityExpression="" columnCount="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" backgroundColor="#a6cee3">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
          </labelStyle>
          <attributeEditorField showLabel="1" name="type_voie" index="5">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" name="nom_voie" index="6">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" name="numero_voie" index="4">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
        </attributeEditorContainer>
        <attributeEditorContainer showLabel="1" groupBox="1" name="identification" collapsedExpression="" visibilityExpression="" columnCount="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" backgroundColor="#a6cee3">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
          </labelStyle>
          <attributeEditorField showLabel="1" name="id_armoire" index="3">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" name="etat" index="7">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" name="vetuste" index="8">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" name="enveloppe_pose" index="11">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" name="fermeture" index="9">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" name="date_pose" index="14">
            <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
              <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
        </attributeEditorContainer>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" groupBox="0" name="Remarques" collapsedExpression="" visibilityExpression=" &quot;remarque&quot; &lt;> ''" columnCount="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="1" collapsed="0">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
      </labelStyle>
      <attributeEditorField showLabel="1" name="remarque" index="18">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont underline="0" strikethrough="0" style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0" bold="0"/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="acteur" editable="1"/>
    <field name="cellule" editable="1"/>
    <field name="created_at" editable="1"/>
    <field name="date_action" editable="1"/>
    <field name="date_depose" editable="1"/>
    <field name="date_maintenance" editable="1"/>
    <field name="date_pose" editable="1"/>
    <field name="departs_allumage" editable="0"/>
    <field name="departs_calibrage_protection" editable="0"/>
    <field name="departs_designation" editable="0"/>
    <field name="departs_heure_allumage" editable="0"/>
    <field name="departs_heure_extinction" editable="0"/>
    <field name="departs_id" editable="0"/>
    <field name="departs_id_armoire" editable="0"/>
    <field name="departs_id_depart" editable="0"/>
    <field name="departs_insee" editable="0"/>
    <field name="departs_nature_depart" editable="0"/>
    <field name="departs_nature_protection" editable="0"/>
    <field name="departs_pilote" editable="0"/>
    <field name="departs_remarque" editable="0"/>
    <field name="departs_sensibilite_diff" editable="0"/>
    <field name="desc_action" editable="1"/>
    <field name="desc_maintenance" editable="1"/>
    <field name="ecp_intervention_id" editable="1"/>
    <field name="enveloppe_pose" editable="1"/>
    <field name="enveloppe_poste_amont" editable="1"/>
    <field name="etat" editable="1"/>
    <field name="fermeture" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_armoire" editable="1"/>
    <field name="insee" editable="1"/>
    <field name="intervention" editable="1"/>
    <field name="mapid" editable="1"/>
    <field name="mslink" editable="1"/>
    <field name="nbre_depart" editable="1"/>
    <field name="nom_voie" editable="1"/>
    <field name="numero" editable="1"/>
    <field name="numero_voie" editable="1"/>
    <field name="photo" editable="0"/>
    <field name="pkid" editable="1"/>
    <field name="remarque" editable="1"/>
    <field name="rot_z" editable="1"/>
    <field name="schema_elec" editable="0"/>
    <field name="type_voie" editable="1"/>
    <field name="uid4" editable="1"/>
    <field name="updated_at" editable="1"/>
    <field name="vetuste" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="acteur" labelOnTop="1"/>
    <field name="cellule" labelOnTop="0"/>
    <field name="created_at" labelOnTop="0"/>
    <field name="date_action" labelOnTop="0"/>
    <field name="date_depose" labelOnTop="1"/>
    <field name="date_maintenance" labelOnTop="0"/>
    <field name="date_pose" labelOnTop="1"/>
    <field name="departs_allumage" labelOnTop="0"/>
    <field name="departs_calibrage_protection" labelOnTop="0"/>
    <field name="departs_designation" labelOnTop="0"/>
    <field name="departs_heure_allumage" labelOnTop="0"/>
    <field name="departs_heure_extinction" labelOnTop="0"/>
    <field name="departs_id" labelOnTop="0"/>
    <field name="departs_id_armoire" labelOnTop="0"/>
    <field name="departs_id_depart" labelOnTop="0"/>
    <field name="departs_insee" labelOnTop="0"/>
    <field name="departs_nature_depart" labelOnTop="0"/>
    <field name="departs_nature_protection" labelOnTop="0"/>
    <field name="departs_pilote" labelOnTop="0"/>
    <field name="departs_remarque" labelOnTop="0"/>
    <field name="departs_sensibilite_diff" labelOnTop="0"/>
    <field name="desc_action" labelOnTop="0"/>
    <field name="desc_maintenance" labelOnTop="0"/>
    <field name="ecp_intervention_id" labelOnTop="0"/>
    <field name="enveloppe_pose" labelOnTop="1"/>
    <field name="enveloppe_poste_amont" labelOnTop="1"/>
    <field name="etat" labelOnTop="1"/>
    <field name="fermeture" labelOnTop="1"/>
    <field name="fid" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="id_armoire" labelOnTop="1"/>
    <field name="insee" labelOnTop="1"/>
    <field name="intervention" labelOnTop="1"/>
    <field name="mapid" labelOnTop="0"/>
    <field name="mslink" labelOnTop="0"/>
    <field name="nbre_depart" labelOnTop="1"/>
    <field name="nom_voie" labelOnTop="1"/>
    <field name="numero" labelOnTop="0"/>
    <field name="numero_voie" labelOnTop="1"/>
    <field name="photo" labelOnTop="0"/>
    <field name="pkid" labelOnTop="0"/>
    <field name="remarque" labelOnTop="1"/>
    <field name="rot_z" labelOnTop="0"/>
    <field name="schema_elec" labelOnTop="0"/>
    <field name="type_voie" labelOnTop="1"/>
    <field name="uid4" labelOnTop="0"/>
    <field name="updated_at" labelOnTop="0"/>
    <field name="vetuste" labelOnTop="1"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="acteur" reuseLastValue="0"/>
    <field name="cellule" reuseLastValue="0"/>
    <field name="created_at" reuseLastValue="0"/>
    <field name="date_action" reuseLastValue="0"/>
    <field name="date_depose" reuseLastValue="0"/>
    <field name="date_pose" reuseLastValue="0"/>
    <field name="desc_action" reuseLastValue="0"/>
    <field name="ecp_intervention_id" reuseLastValue="0"/>
    <field name="enveloppe_pose" reuseLastValue="0"/>
    <field name="enveloppe_poste_amont" reuseLastValue="0"/>
    <field name="etat" reuseLastValue="0"/>
    <field name="fermeture" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="id_armoire" reuseLastValue="0"/>
    <field name="insee" reuseLastValue="0"/>
    <field name="intervention" reuseLastValue="0"/>
    <field name="mapid" reuseLastValue="0"/>
    <field name="mslink" reuseLastValue="0"/>
    <field name="nbre_depart" reuseLastValue="0"/>
    <field name="nom_voie" reuseLastValue="0"/>
    <field name="numero" reuseLastValue="0"/>
    <field name="numero_voie" reuseLastValue="0"/>
    <field name="photo" reuseLastValue="0"/>
    <field name="pkid" reuseLastValue="0"/>
    <field name="remarque" reuseLastValue="0"/>
    <field name="rot_z" reuseLastValue="0"/>
    <field name="schema_elec" reuseLastValue="0"/>
    <field name="type_voie" reuseLastValue="0"/>
    <field name="uid4" reuseLastValue="0"/>
    <field name="updated_at" reuseLastValue="0"/>
    <field name="vetuste" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="intervention">
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="dataDefinedAlias" type="Map">
            <Option value="false" name="active" type="bool"/>
            <Option value="1" name="type" type="int"/>
            <Option value="" name="val" type="QString"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </field>
    <field name="schema_elec">
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="dataDefinedAlias" type="Map">
            <Option value="false" name="active" type="bool"/>
            <Option value="1" name="type" type="int"/>
            <Option value="" name="val" type="QString"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>COALESCE( "id_armoire", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
