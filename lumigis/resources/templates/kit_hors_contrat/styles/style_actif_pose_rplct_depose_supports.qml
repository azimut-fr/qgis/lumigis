<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" simplifyLocal="1" labelsEnabled="1" readOnly="0" hasScaleBasedVisibilityFlag="0" symbologyReferenceScale="200" version="3.30.1-'s-Hertogenbosch" maxScale="1" simplifyDrawingHints="0" simplifyAlgorithm="0" simplifyMaxScale="1" minScale="2500" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" durationField="fid" startExpression="" durationUnit="min" startField="date_pose" accumulate="0" enabled="0" mode="0" fixedDuration="0" limitMode="0" endField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation showMarkerSymbolInSurfacePlots="0" type="IndividualFeatures" extrusionEnabled="0" extrusion="0" respectLayerSymbol="1" clamping="Terrain" zoffset="0" binding="Centroid" symbology="Line" zscale="1">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol frame_rate="10" is_animated="0" type="line" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{1b3a73fe-ce72-4bfc-9274-5e6a2bb48873}" enabled="1" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="square"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="190,207,80,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.6"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{17b58185-3222-4f1b-a28e-4f9db3b82568}" enabled="1" locked="0" class="SimpleFill">
          <Option type="Map">
            <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="color" value="190,207,80,255"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="136,148,57,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="style" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{9f8e75e6-c6a1-4768-9249-5011f6e12d1e}" enabled="1" locked="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="190,207,80,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="diamond"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="136,148,57,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="3"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="200" type="RuleRenderer" symbollevels="0" forceraster="0" enableorderby="0">
    <rules key="{53b1b2cb-380d-44e5-a3fe-c9b28aaf2427}">
      <rule label=" Actif" filter="&quot;etat&quot; =  'Hors contrat' " key="{f6fe1ebe-c47a-4b0b-b83d-c6a9e8e21cc1}">
        <rule symbol="0" label="Mât boule" filter=" &quot;cellule&quot; = 'MAT BOULE'" key="{96116492-3ca6-46cb-a9ea-1fe73cc3f384}"/>
        <rule symbol="1" label="Mât simple" filter=" &quot;cellule&quot; = 'MAT SIMPLE' " key="{d5c773cc-3d0c-4c22-bcaa-64e734ca37af}"/>
        <rule symbol="2" label="Mât double" filter=" &quot;cellule&quot; = 'MAT DOUBLE' " key="{af8761bd-1137-4b52-b2b5-0d325c974d92}"/>
        <rule symbol="3" label="Mât triple" filter=" &quot;cellule&quot; = 'MAT TRIPLE' " key="{7130f5b6-6f96-4c3e-b85c-d3d010bf2cf7}"/>
        <rule symbol="4" label="Mât quadruple" filter=" &quot;cellule&quot; =   'MAT QUADRUPLE' " key="{5f42834f-4706-4e94-836e-028028ba7604}"/>
        <rule symbol="5" label="Poteau béton" filter=" &quot;cellule&quot; =  'POTEAU BETON' " key="{c973e7ab-263a-4ae5-b08f-daa7d2290969}"/>
        <rule symbol="6" label="Poteau bois" filter=" &quot;cellule&quot;  =  'POTEAU BOIS' " key="{dfd2a8ef-7fe8-4250-8eca-8bd0842b634d}"/>
        <rule symbol="7" label="Support projecteur" filter=" &quot;cellule&quot;  = 'SUPPORT PROJECTEUR'" key="{b4607fdf-b86c-4f16-a110-3532d5833c0f}"/>
        <rule symbol="8" label="carre fer" filter=" &quot;cellule&quot; =   'CARRE FER' " key="{540cd4a7-1b17-4080-9dfc-b490dc0265c8}"/>
        <rule symbol="9" label="Poteau bois double" filter=" &quot;cellule&quot;  =  'POTEAU BOIS DOUBLE'" key="{0354a5fc-669b-4a37-a148-53555e023f6b}"/>
        <rule symbol="10" label="Poteau bois contrefiché" filter=" &quot;cellule&quot;  =  'POTEAU BOIS CONTREFICHE'" key="{5f5ab463-7a23-42d7-943c-5feb062f16e8}"/>
        <rule symbol="11" label="Support façade " filter=" &quot;cellule&quot; = 'SUPPORT FACADE'" key="{ac878612-8be8-42a5-af2e-c5d1f633e4c5}"/>
      </rule>
      <rule label="Solaire" filter=" &quot;etat&quot; ='Solaire'" key="{5a29e24f-bb34-4df8-8bc2-263bd70cb0a1}">
        <rule symbol="12" label="Mât simple " filter=" &quot;cellule&quot; = 'SUPPORT SOLAIRE' " key="{557773b3-6817-45f7-a7de-e9ba53dcbde3}"/>
      </rule>
    </rules>
    <symbols>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="0" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{01afe4e8-8a8a-4e4f-8eae-de0db3a1edff}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="158,37,16,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAyNzIxLjI2IDI3MjEuMjYiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1tYXRfYm91bGUuc3ZnIgogICB3aWR0aD0iNzJjbSIKICAgaGVpZ2h0PSI3MmNtIj4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg1MTAiCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODUwMiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODQ5MCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzk3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMjEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExNyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTEzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEwMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk2IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTQyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEzNCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgxMDgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTEwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5OCIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgxMDAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDkyIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA5NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODYiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwNztzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDg4IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA4MCIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwODIiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODUxMC02IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxMDI0IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjcwNSIKICAgICBpZD0ibmFtZWR2aWV3MTIiCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4xMjUiCiAgICAgaW5rc2NhcGU6Y3g9IjE0NTYuOTU2OSIKICAgICBpbmtzY2FwZTpjeT0iMTg3OC43MTk5IgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmcyIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpsb2NrZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODA3NiIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjAsMCIKICAgICAgIG9yaWVudGF0aW9uPSIwLDI2NDUuNjY5MyIKICAgICAgIGlkPSJndWlkZTg0OCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDI2NDUuNjY5NCIKICAgICAgIG9yaWVudGF0aW9uPSIyNjQ1LjY2OTMsMCIKICAgICAgIGlkPSJndWlkZTg1NCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIyNzIxLjI2MDEsMjcyMS4yNjAxIgogICAgICAgb3JpZW50YXRpb249Ii0wLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU4NTYiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwyNzIxLjI2IgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTg1OCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzQ4MDEiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDAwMDU2LDAsMCwwLjk5OTc0MDEsOC43NzEwNzMxLDY2LjIwOTQwNikiPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtmaWxsOiMwMDAwMDA7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMy4zODU4MzM3NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJNIDI5Ni4wNjMwMiwxNTUuNTkwNTYgMTkyLjEyNTk5LDMyNS42NzA1OCAyOTYuMDYzMDIsNDk1Ljc0ODA1IEggNTAzLjkzNzAzIEwgNjA3Ljg3NDA0LDMyNS42NzA1OCA1MDMuOTM3MDMsMTU1LjU5MDU2IFogbSAxMDMuOTM3MDIsNzMuNTQ5MjMgYyA1Ny40MDI4MSwwIDEwMy45MzY5OSw0My4yMTgzMiAxMDMuOTM2OTksOTYuNTMwNzkgMCw1My4zMTI0NyAtNDYuNTM0MTgsOTYuNTMwODEgLTEwMy45MzY5OSw5Ni41MzA4MSAtNTcuNDAyODUsMCAtMTAzLjkzNzAyLC00My4yMTgzNCAtMTAzLjkzNzAyLC05Ni41MzA4MSAwLC01My4zMTI0NyA0Ni41MzQxNywtOTYuNTMwNzkgMTAzLjkzNzAyLC05Ni41MzA3OSB6IgogICAgICAgaWQ9InBhdGg2IgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDxyZWN0CiAgICAgICB5PSIyOC4yODYxNTQiCiAgICAgICB4PSI4NS43MTY3MTMiCiAgICAgICBoZWlnaHQ9IjI1MzIuOTQxOSIKICAgICAgIHdpZHRoPSIyNTMyLjI2OTMiCiAgICAgICBpZD0icmVjdDgwNzgiCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRkNGQ0ZDtzdHJva2Utd2lkdGg6MTEzLjQwMDI0NTY3O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxlbGxpcHNlCiAgICAgICByeT0iNjIuOTY3MDg3IgogICAgICAgcng9IjIxNy45MTM3IgogICAgICAgY3k9IjExNjkuMTU5MyIKICAgICAgIGN4PSI2OTguMTEwMzUiCiAgICAgICBpZD0icGF0aDgxMDQiCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEzLjM4NTgzMzc0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOmJldmVsO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMzg1ODMzNzQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTttYXJrZXI6bm9uZTtlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIgogICAgICAgZD0ibSAyMjE2LjA2MywxNTUuNTkwNTcgLTEwMy45MzY5LDE3MC4wOCAxMDMuOTM2OSwxNzAuMDc3NDcgaCAyMDcuODc0MiBsIDEwMy45MzY5LC0xNzAuMDc3NDcgLTEwMy45MzY5LC0xNzAuMDggeiBNIDIzMjAsMjI5LjEzOTc4IGMgNTcuNDAyOCwwIDEwMy45MzcyLDQzLjIxODMzIDEwMy45MzcyLDk2LjUzMDc5IDAsNTMuMzEyNDcgLTQ2LjUzNDQsOTYuNTMwODEgLTEwMy45MzcyLDk2LjUzMDgxIC01Ny40MDI3LDAgLTEwMy45MzcsLTQzLjIxODM0IC0xMDMuOTM3LC05Ni41MzA4MSAwLC01My4zMTI0NiA0Ni41MzQzLC05Ni41MzA3OSAxMDMuOTM3LC05Ni41MzA3OSB6IgogICAgICAgaWQ9InBhdGg2LTciCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtmaWxsOiMwMDAwMDA7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMy4zODU4MzM3NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJtIDI5Ni4wNjMwMSwyMDc1LjU5MDYgLTEwMy45MzcwMiwxNzAuMDggMTAzLjkzNzAyLDE3MC4wNzc1IGggMjA3Ljg3NCBsIDEwMy45MzcwMywtMTcwLjA3NzUgLTEwMy45MzcwMywtMTcwLjA4IHogTSA0MDAsMjE0OS4xMzk5IGMgNTcuNDAyODQsMCAxMDMuOTM3MDEsNDMuMjE4MiAxMDMuOTM3MDEsOTYuNTMwNyAwLDUzLjMxMjUgLTQ2LjUzNDE3LDk2LjUzMDggLTEwMy45MzcwMSw5Ni41MzA4IC01Ny40MDI4MSwwIC0xMDMuOTM2OTksLTQzLjIxODMgLTEwMy45MzY5OSwtOTYuNTMwOCAwLC01My4zMTI1IDQ2LjUzNDE4LC05Ni41MzA3IDEwMy45MzY5OSwtOTYuNTMwNyB6IgogICAgICAgaWQ9InBhdGg2LTQiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtmaWxsOiMwMDAwMDA7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMy4zODU4MzM3NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJtIDIyMTYuMDYzMSwyMDc1LjU5MDYgLTEwMy45MzcsMTcwLjA3ODggMTAzLjkzNywxNzAuMDc4NyBoIDIwNy44NzQgbCAxMDMuOTM3LC0xNzAuMDc4NyAtMTAzLjkzNywtMTcwLjA3ODggeiBtIDEwMy45MzcsNzMuNTQ3MyBjIDU3LjQwMjksMCAxMDMuOTM3LDQzLjIxODYgMTAzLjkzNyw5Ni41MzE1IDAsNTMuMzEyOCAtNDYuNTM0MSw5Ni41MzE1IC0xMDMuOTM3LDk2LjUzMTUgLTU3LjQwMywwIC0xMDMuOTM3LC00My4yMTg3IC0xMDMuOTM3LC05Ni41MzE1IDAsLTUzLjMxMjkgNDYuNTM0LC05Ni41MzE1IDEwMy45MzcsLTk2LjUzMTUgeiIKICAgICAgIGlkPSJwYXRoNi02IgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDxlbGxpcHNlCiAgICAgICByeT0iMTAwMS41NzQ4IgogICAgICAgcng9IjEwMDEuNTc0OSIKICAgICAgIGN5PSIxMjk1LjA5MzUiCiAgICAgICBjeD0iMTM1MS44NTEzIgogICAgICAgaWQ9InBhdGg0NzkzIgogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTA0NDc3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiM0ZDRkNGQ7c3Ryb2tlLXdpZHRoOjExMy4zODU4MzM3NDtzdHJva2UtbGluZWNhcDpzcXVhcmU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{830bfd70-026c-4057-8a4f-e7498389bfd1}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="120,117,172,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIHg9IjBweCIKICAgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgMjcyMS4yNTk5IDI3MjEuMjU5OSIKICAgaWQ9InN2ZzIiCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PW1hdF9zaW1wbGUuc3ZnIgogICB3aWR0aD0iNzJjbSIKICAgaGVpZ2h0PSI3MmNtIgogICBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3Ij4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NTA5NiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDUwOTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTA5MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJEb3RMIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iRG90TCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNTEyNyIKICAgICAgICAgZD0ibSAtMi41LC0xIGMgMCwyLjc2IC0yLjI0LDUgLTUsNSAtMi43NiwwIC01LC0yLjI0IC01LC01IDAsLTIuNzYgMi4yNCwtNSA1LC01IDIuNzYsMCA1LDIuMjQgNSw1IHoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjEuMDAwMDAwMDNwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjgsMCwwLDAuOCw1LjkyLDAuOCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDk1MSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZmZmZjAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ5NDkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODYzOSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MjQ5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUyNDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MjE0IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNTIxMiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDk5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkV4cGVyaW1lbnRhbEFycm93IgogICAgICAgb3JpZW50PSJhdXRvLXN0YXJ0LXJldmVyc2UiCiAgICAgICByZWZZPSIzIgogICAgICAgcmVmWD0iNSIKICAgICAgIGlkPSJtYXJrZXI1NjUzIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNTE3MCIKICAgICAgICAgZD0iTSAxMCwzIDAsNiBWIDAgWiIKICAgICAgICAgc3R5bGU9ImZpbGw6Y29udGV4dC1zdHJva2U7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iU3RvcEwiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJTdG9wTCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNTA4NiIKICAgICAgICAgZD0iTSAwLDUuNjUgViAtNS42NSIKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC43NTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MS4wMDAwMDAwM3B0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoMC44KSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iRW1wdHlUcmlhbmdsZU91dEwiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJFbXB0eVRyaWFuZ2xlT3V0TCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNTE3OSIKICAgICAgICAgZD0iTSA1Ljc3LDAgLTIuODgsNSBWIC01IFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjEuMDAwMDAwMDNwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjgsMCwwLDAuOCwtNC44LDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJUYWlsIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iVGFpbCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDk0MiIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgtMS4yKSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDkzMCIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsLTMuOTU4NTIyNyAwLjU0MzUyMDk0LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ5MzIiCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLC0zLjk1ODUyMjcgMy4wNjE3MDUzLDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ5MzQiCiAgICAgICAgICAgZD0iTSAxLjMwNTM1ODIsLTMuOTU4NTIyNyA1LjY1Mzc0NjYsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDkzNiIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsNC4xNzc1ODM4IDAuNTQzNTIwOTQsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDkzOCIKICAgICAgICAgICBkPSJNIC0xLjI4NjY4MzIsNC4xNzc1ODM4IDMuMDYxNzA1MywwLjIxOTc0MjI2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODAwMDAwMDE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTQwIgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLDQuMTc3NTgzOCA1LjY1Mzc0NjYsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9tYXJrZXI+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q2NzU3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IlNxdWFyZVMiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJTcXVhcmVTIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0OTQxIgogICAgICAgICBkPSJNIC01LC01IFYgNSBIIDUgViAtNSBaIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxLjAwMDAwMDAzcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjIpIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzJTZW5kIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cyU2VuZCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg5OCIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4zLDAsMCwtMC4zLDAuNjksMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDU3NjYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iRXhwZXJpbWVudGFsQXJyb3ciCiAgICAgICBvcmllbnQ9ImF1dG8tc3RhcnQtcmV2ZXJzZSIKICAgICAgIHJlZlk9IjMiCiAgICAgICByZWZYPSI1IgogICAgICAgaWQ9IkV4cGVyaW1lbnRhbEFycm93IgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNTExOCIKICAgICAgICAgZD0iTSAxMCwzIDAsNiBWIDAgWiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJUb3JzbyIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IlRvcnNvIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPGcKICAgICAgICAgaWQ9Imc1MDkxIgogICAgICAgICB0cmFuc2Zvcm09InNjYWxlKDAuNykiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLW9wYWNpdHk6MSI+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDUwNzciCiAgICAgICAgICAgZD0ibSAtNC43NzkyMjgxLC0zLjIzOTU0MiBjIDIuMzUwMzc0LDAuMzY1OTM5MyA1LjMwMDI2NzMyLDEuOTM3NTQ3NyA1LjAzNzE1NTMyLDMuNjI3NDg1NDYgQyAtMC4wMDUxODc3OSwyLjA3Nzg4MTkgLTIuMjEyNjc0MSwyLjYxNzY1MzkgLTQuNTYzMDQ3MSwyLjI1MTcxNjkgLTYuOTEzNDIyMSwxLjg4NTc3NjkgLTguNTIxMDM1LDAuNzUyMDE0MTQgLTguMjU3OTIyLC0wLjkzNzkyMzM2IC03Ljk5NDgwOSwtMi42Mjc4NjE1IC03LjEyOTYwNDEsLTMuNjA1NDgxMyAtNC43NzkyMjgxLC0zLjIzOTU0MiBaIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjEuMjU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg1MDc5IgogICAgICAgICAgIGQ9Ik0gNC40NTk4Nzg5LDAuMDg4NjY1NzQgQyAtMi41NTY0NTcxLC00LjM3ODMzMiA1LjIyNDg3NjksLTMuOTA2MTgwNiAtMC44NDgyOTU3OCwtOC43MTk3MzMxIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjEuMDAwMDAwMDNwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDUwODEiCiAgICAgICAgICAgZD0iTSA0LjkyOTg3MTksMC4wNTc1MjA3NCBDIC0xLjM4NzI3MzEsMS43NDk0Njg5IDEuODAyNzU3OSw1LjQ3ODIwNzkgLTQuOTQ0ODczMSw3LjU0NjI3MjUiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MS4wMDAwMDAwM3B0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cmVjdAogICAgICAgICAgIGlkPSJyZWN0NTA4MyIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjUyNzUzNiwtMC44NDk1MzMsMC44ODc2NjgsMC40NjA0ODQsMCwwKSIKICAgICAgICAgICB5PSItMS43NDA4NTc1IgogICAgICAgICAgIHg9Ii0xMC4zOTE3MDYiCiAgICAgICAgICAgaGVpZ2h0PSIyLjc2MDgxNDciCiAgICAgICAgICAgd2lkdGg9IjIuNjM2NjU4MiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxLjAwMDAwMDAzcHQ7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cmVjdAogICAgICAgICAgIGlkPSJyZWN0NTA4NSIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjY3MTIwNSwtMC43NDEyNzIsMC43OTA4MDIsMC42MTIwNzIsMCwwKSIKICAgICAgICAgICB5PSItNy45NjI5MzA3IgogICAgICAgICAgIHg9IjQuOTU4NzI2OSIKICAgICAgICAgICBoZWlnaHQ9IjIuODYxNDE2MSIKICAgICAgICAgICB3aWR0aD0iMi43MzI3MzU2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjEuMDAwMDAwMDNwdDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg1MDg3IgogICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMTA5NTE3LDEuMTA5NTE3LDAsMjUuOTY2NDgsMTkuNzE2MTkpIgogICAgICAgICAgIGQ9Im0gMTYuNzc5OTUxLC0yOC42ODUwNDUgYSAwLjYwNzMxNzI3LDAuNjA3MzE3MjcgMCAxIDAgLTEuMjE0NjM0LDAgMC42MDczMTcyNywwLjYwNzMxNzI3IDAgMSAwIDEuMjE0NjM0LDAgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxLjAwMDAwMDAzcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg1MDg5IgogICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMTA5NTE3LDEuMTA5NTE3LDAsMjYuODI0NSwxNi45OTEyNikiCiAgICAgICAgICAgZD0ibSAxNi43Nzk5NTEsLTI4LjY4NTA0NSBhIDAuNjA3MzE3MjcsMC42MDczMTcyNyAwIDEgMCAtMS4yMTQ2MzQsMCAwLjYwNzMxNzI3LDAuNjA3MzE3MjcgMCAxIDAgMS4yMTQ2MzQsMCB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjEuMDAwMDAwMDNwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgIDwvZz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cxTGVuZCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkFycm93MUxlbmQiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4NjgiCiAgICAgICAgIGQ9Ik0gMCwwIDUsLTUgLTEyLjUsMCA1LDUgWiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MS4wMDAwMDAwM3B0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0wLjgsMCwwLC0wLjgsLTEwLDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzJMc3RhcnQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzJMc3RhcnQiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4ODMiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuNjI1O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJNIDguNzE4NTg3OCw0LjAzMzczNTIgLTIuMjA3Mjg5NSwwLjAxNjAxMzI2IDguNzE4NTg4NCwtNC4wMDE3MDc4IGMgLTEuNzQ1NDk4NCwyLjM3MjA2MDkgLTEuNzM1NDQwOCw1LjYxNzQ1MTkgLTZlLTcsOC4wMzU0NDMgeiIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4xLDAsMCwxLjEsMS4xLDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzFNc3RhcnQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzFNc3RhcnQiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4NzEiCiAgICAgICAgIGQ9Ik0gMCwwIDUsLTUgLTEyLjUsMCA1LDUgWiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MS4wMDAwMDAwM3B0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNCwwLDAsMC40LDQsMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MUxzdGFydCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkFycm93MUxzdGFydCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg2NSIKICAgICAgICAgZD0iTSAwLDAgNSwtNSAtMTIuNSwwIDUsNSBaIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxLjAwMDAwMDAzcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC44LDAsMCwwLjgsMTAsMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDg2MSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4NTkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg1NyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NTEwIgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg1MDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODQ5OCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NDk0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5NyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTIxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTA4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTAwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5MiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwOTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDg2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDc7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODAiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg1MTAtNiIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDk5LTMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ1MDk2IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NTA5OCIKICAgICAgIHgxPSIyNzIxLjg3MyIKICAgICAgIHkxPSIxNTE3LjM0NzgiCiAgICAgICB4Mj0iMzk4OC4wMTQ5IgogICAgICAgeTI9IjE1MTcuMzQ3OCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0zNzcuOTUyNzcpIiAvPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjA3OTA5OTM5NSIKICAgICBpbmtzY2FwZTpjeD0iMTI1MS41NzAzIgogICAgIGlua3NjYXBlOmN5PSI2NDkuNzg0MjYiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzIiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOmxvY2tndWlkZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10by1ndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vYmplY3QtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb3RoZXJzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iMzE3Ny45NCwxNzM3Ljk2IgogICAgIGlua3NjYXBlOm1lYXN1cmUtZW5kPSIzMTg3LjgzLDEyNzAuNiIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MDc2IiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMjcyMS4yNiwyNzIxLjI2IgogICAgICAgb3JpZW50YXRpb249Ii0wLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGUxMTUxIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjAsMjcyMS4yNTk5IgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTExNTMiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxwYXRoCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTg4MjA3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiM0ZDRkNGQ7c3Ryb2tlLXdpZHRoOjkuNDQ4ODE5MTY7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIgogICAgIGlkPSJwYXRoNjY3OC02IgogICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICBzb2RpcG9kaTpjeD0iNzcyOC4wMTUxIgogICAgIHNvZGlwb2RpOmN5PSIxMTQ4Ljg0MzgiCiAgICAgc29kaXBvZGk6cng9IjEwIgogICAgIHNvZGlwb2RpOnJ5PSIxMCIKICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgIGQ9Im0gNzczOC4wMTUxLDExNDguODQzOCBhIDEwLDEwIDAgMCAxIC05Ljk5NjMsOS45OTk5IDEwLDEwIDAgMCAxIC0xMC4wMDM3LC05Ljk5MjcgMTAsMTAgMCAwIDEgOS45ODkyLC0xMC4wMDcyIDEwLDEwIDAgMCAxIDEwLjAxMDgsOS45ODU1IiAvPgogIDxnCiAgICAgaWQ9ImcxMTQ5IgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTgxMzc2MjksMCwwLDEuMDAyMzE5Niw2Ny4zMjA4NTEsNDAuNDE1MjY3KSI+CiAgICA8cGF0aAogICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEzLjE3NTc1MDczO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7bWFya2VyOm5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGQ9Im0gMjM2OS43MjcyLDI0NTYuNjY3MSAxMDMuNTUxNywtMTcwLjA4MDcgLTEwMy41NTE3LC0xNzAuMDc4MiBoIC0yMDcuMTAzNiBsIC0xMDMuNTUxOCwxNzAuMDc4MiAxMDMuNTUxOCwxNzAuMDgwNyB6IG0gLTEwMy41NTE4LC03My41NDk1IGMgLTU3LjE5MDEsMCAtMTAzLjU1MTgsLTQzLjIxODUgLTEwMy41NTE4LC05Ni41MzEyIDAsLTUzLjMxMjcgNDYuMzYxNywtOTYuNTMxMiAxMDMuNTUxOCwtOTYuNTMxMiA1Ny4xOSwwIDEwMy41NTE4LDQzLjIxODUgMTAzLjU1MTgsOTYuNTMxMiAwLDUzLjMxMjcgLTQ2LjM2MTgsOTYuNTMxMiAtMTAzLjU1MTgsOTYuNTMxMiB6IgogICAgICAgaWQ9InBhdGg2LTAiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPHJlY3QKICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoLTEpIgogICAgICAgeT0iLTI1ODMuMjA2MSIKICAgICAgIHg9Ii0yNTk0LjI2MjciCiAgICAgICBoZWlnaHQ9IjI1MjYuNDIzMSIKICAgICAgIHdpZHRoPSIyNTM3Ljc1OTMiCiAgICAgICBpZD0icmVjdDgwNzgtNyIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojNGQ0ZDRkO3N0cm9rZS13aWR0aDoxMTMuMzc2OTM3ODc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtmaWxsOiMwMDAwMDA7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMy4xNzU3NTA3MztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJNIDQ1Ni44NDMyNywyNDU2LjY2NzEgNTYwLjM5NDk0LDIyODYuNTg2NCA0NTYuODQzMjcsMjExNi41MDgyIEggMjQ5LjczOTUyIGwgLTEwMy41NTE2OSwxNzAuMDc4MiAxMDMuNTUxNjksMTcwLjA4MDcgeiBtIC0xMDMuNTUxNzgsLTczLjU0OTUgYyAtNTcuMTkwMDUsMCAtMTAzLjU1MTk3LC00My4yMTg1IC0xMDMuNTUxOTcsLTk2LjUzMTIgMCwtNTMuMzEyNyA0Ni4zNjE5MiwtOTYuNTMxMiAxMDMuNTUxOTcsLTk2LjUzMTIgNTcuMTg5OTUsMCAxMDMuNTUxNzgsNDMuMjE4NSAxMDMuNTUxNzgsOTYuNTMxMiAwLDUzLjMxMjcgLTQ2LjM2MTgzLDk2LjUzMTIgLTEwMy41NTE3OCw5Ni41MzEyIHoiCiAgICAgICBpZD0icGF0aDYtNy03IgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMTc1NzUwNzM7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTttYXJrZXI6bm9uZTtlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIgogICAgICAgZD0iTSAyMzY5LjcyNzIsNTM2LjY1OTIgMjQ3My4yNzg5LDM2Ni41Nzg1IDIzNjkuNzI3MiwxOTYuNTAwMzEgaCAtMjA3LjEwMzYgbCAtMTAzLjU1MTgsMTcwLjA3ODE5IDEwMy41NTE4LDE3MC4wODA3IHogbSAtMTAzLjU1MTgsLTczLjU0OTYgYyAtNTcuMTkwMSwwIC0xMDMuNTUxOCwtNDMuMjE4MzggLTEwMy41NTE4LC05Ni41MzExIDAsLTUzLjMxMjcxIDQ2LjM2MTcsLTk2LjUzMTE5IDEwMy41NTE4LC05Ni41MzExOSA1Ny4xOSwwIDEwMy41NTE4LDQzLjIxODQ4IDEwMy41NTE4LDk2LjUzMTE5IDAsNTMuMzEyNzIgLTQ2LjM2MTgsOTYuNTMxMSAtMTAzLjU1MTgsOTYuNTMxMSB6IgogICAgICAgaWQ9InBhdGg2LTQtMSIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEzLjE3NTc1MDczO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7bWFya2VyOm5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGQ9Ik0gNDU2Ljg0MzE3LDUzNi42NTkyIDU2MC4zOTQ5NCwzNjYuNTc5NyA0NTYuODQzMTcsMTk2LjUwMDMxIEggMjQ5LjczOTYyIEwgMTQ2LjE4NzgzLDM2Ni41Nzk3IDI0OS43Mzk2Miw1MzYuNjU5MiBaIE0gMzUzLjI5MTM5LDQ2My4xMTE2IGMgLTU3LjE5MDE0LDAgLTEwMy41NTE3NywtNDMuMjE4NzggLTEwMy41NTE3NywtOTYuNTMxOSAwLC01My4zMTMwMSA0Ni4zNjE2MywtOTYuNTMxODkgMTAzLjU1MTc3LC05Ni41MzE4OSA1Ny4xOTAyNSwwIDEwMy41NTE3OCw0My4yMTg4OCAxMDMuNTUxNzgsOTYuNTMxODkgMCw1My4zMTMxMiAtNDYuMzYxNTMsOTYuNTMxOSAtMTAzLjU1MTc4LDk2LjUzMTkgeiIKICAgICAgIGlkPSJwYXRoNi02LTciCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPGVsbGlwc2UKICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoLTEpIgogICAgICAgcnk9IjEwMDEuNTc4OSIKICAgICAgIHJ4PSI5OTcuODYyNzMiCiAgICAgICBjeT0iLTEzMTcuMTU5NCIKICAgICAgIGN4PSItMTMxNy44NTI0IgogICAgICAgaWQ9InBhdGg0NzkzLTIiCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45MDQ0Nzc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRkNGQ0ZDtzdHJva2Utd2lkdGg6MTEzLjE1OTA1NzYyO3N0cm9rZS1saW5lY2FwOnNxdWFyZTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoLTEpIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSIzOS43MDQxMjUiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii00Mi43MzgxMDEiCiAgICAgICBkPSJtIC0yNTUxLjY4MywtMTMxOS45OTQ1IGEgNDIuNTc5NjY2LDQyLjUxOTY4NCAwIDAgMSAtNDIuNTY0Myw0Mi41MTk3IDQyLjU3OTY2Niw0Mi41MTk2ODQgMCAwIDEgLTQyLjU5NTEsLTQyLjQ4ODkgNDIuNTc5NjY2LDQyLjUxOTY4NCAwIDAgMSA0Mi41MzM0LC00Mi41NTA1IDQyLjU3OTY2Niw0Mi41MTk2ODQgMCAwIDEgNDIuNjI1OSw0Mi40NTgxIgogICAgICAgc29kaXBvZGk6b3Blbj0idHJ1ZSIKICAgICAgIHNvZGlwb2RpOmVuZD0iNi4yODE3MzY3IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTpyeT0iNDIuNTE5Njg0IgogICAgICAgc29kaXBvZGk6cng9IjQyLjU3OTY2NiIKICAgICAgIHNvZGlwb2RpOmN5PSItMTMxOS45OTQ1IgogICAgICAgc29kaXBvZGk6Y3g9Ii0yNTk0LjI2MjciCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBpZD0icGF0aDUxMDAiCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MjguMzY2NDQzNjM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgc3Ryb2tlIGZpbGwiIC8+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoMTE2OSIKICAgICAgIGQ9Im0gMjUzNy45NDYyLDEzMTkuOTk5OSAtMTIyMC4wOTM4LC0yLjg0MDUiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDozOC4yODY2MTM0NjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogIDwvZz4KPC9zdmc+Cg=="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="10" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option type="Map" name="properties">
              <Option type="Map" name="alpha">
                <Option type="bool" name="active" value="true"/>
                <Option type="QString" name="expression" value="'0,2'"/>
                <Option type="int" name="type" value="3"/>
              </Option>
            </Option>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{b8703885-7bae-4165-a3cf-5c74a3b330d2}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="160,214,128,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9IjIxNWNtIgogICBoZWlnaHQ9Ijk1Y20iCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDIxNTAgOTUwIgogICBpZD0ic3ZnMjIiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXBvdGVhdV9ib2lzX2NvbnRyZWZpY2hlLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSI+CiAgPGRlZnMKICAgICBpZD0iZGVmczI2Ij4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4ODIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg3OCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODA0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4MDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODA0LTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDgwMC0yIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODA0LTIwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4MDAtMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzk2LTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDgwNC0yMC0wIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4MDAtMS0zIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtOC0xIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtMiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTEwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODk2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODk0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDgwMC0zIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtOSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTMtMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzk2LTktMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5Ni0wMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5Ni01IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxMDI0IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjcwNSIKICAgICBpZD0ibmFtZWR2aWV3MjQiCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnpvb209IjAuMDQiCiAgICAgaW5rc2NhcGU6Y3g9IjM1MDUuMTA4NCIKICAgICBpbmtzY2FwZTpjeT0iMjA1Ny4xMDM0IgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmcyMiIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10ZXh0LWJhc2VsaW5lPSJ0cnVlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkNDc2NSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGEyIj4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICAgIDxjYzpsaWNlbnNlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIiAvPgogICAgICAgIDxkYzpjcmVhdG9yPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+am1hPC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpjcmVhdG9yPgogICAgICAgIDxkYzpwdWJsaXNoZXI+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5BemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnB1Ymxpc2hlcj4KICAgICAgICA8ZGM6cmlnaHRzPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+KGMpQXppbXV0PC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpyaWdodHM+CiAgICAgIDwvY2M6V29yaz4KICAgICAgPGNjOkxpY2Vuc2UKICAgICAgICAgcmRmOmFib3V0PSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIj4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjUmVwcm9kdWN0aW9uIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEaXN0cmlidXRpb24iIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNOb3RpY2UiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNBdHRyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cHJvaGliaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNDb21tZXJjaWFsVXNlIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEZXJpdmF0aXZlV29ya3MiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNTaGFyZUFsaWtlIiAvPgogICAgICA8L2NjOkxpY2Vuc2U+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjUwLjExNDkzNjgzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA3NDAuODMzMzIsNzgwLjMzMzM3IEggMjg5OS44MzMzIgogICAgIGlkPSJwYXRoNDg4MCIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0ODgyIgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gNzQwLjgzMzMyLDc4MC4zMzMzNyBIIDI4OTkuODMzMyIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjU1MzQ4MDY1LDAsMCwwLjU5MzQ4MzE3LDY3LjQ4MDcsMTEuODg1MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZGlzcGxheTppbmxpbmU7ZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxNjYuMzQ5OTkwODQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDU0OTguNjcxOCwtMTc2Ny42NzIzIC01MzMzLjY2NzQsLTAuMDQ5IgogICAgIGlkPSJwYXRoNDc5NC03IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3OTYtMDEiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSA1NDk4LjY3MTgsLTE3NjcuNjcyMyAtNTMzMy42Njc0LC0wLjA0OSIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4xNTkzNjUwMSwwLDAsLTAuMjA0MDgxNjMsOTI4LjgxMzUsMTE0LjI0MDIyKSIKICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO2ZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6ODMuMTgzNTQwMzQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIgogICAgIGQ9Im0gMjgzMS44NTE5LC00NTc2LjY4NjIgdiAyODA1LjAwODUiCiAgICAgaWQ9InBhdGg0Nzk4LTkiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDgwMC05IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMjgzMS44NTE5LC00NTc2LjY4NjIgdiAyODA1LjAwODUiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC40MjkyMjMyNCwwLDAsMC4zMDMwMjkyMiwtNzM3Ljk3OTAyLDE0MzYuODY5NSkiCiAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjYyIgLz4KICA8Y2lyY2xlCiAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO29wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTA0NDc3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjMwLjAwMDAwMTkxO3N0cm9rZS1saW5lY2FwOnNxdWFyZTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIgogICAgIGlkPSJwYXRoNDc5MyIKICAgICBjeD0iNDc3LjUxNzYxIgogICAgIGN5PSI0NzUuMDAwMDMiCiAgICAgcj0iNDI1LjAwMDAzIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO2ZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTY2LjM0OTk5MDg0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA1NDk4LjY3MTgsLTE3NjcuNjcyMyAtNTMzMy42Njc0LC0wLjA0OSIKICAgICBpZD0icGF0aDQ3OTQtMiIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzk2LTUiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSA1NDk4LjY3MTgsLTE3NjcuNjcyMyAtNTMzMy42Njc0LC0wLjA0OSIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4xNTkzNjUwMSwwLDAsLTAuMjA0MDgxNjMsMjEyMy43NzgxLDExNC4yNDAyNSkiCiAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjYyIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJkaXNwbGF5OmlubGluZTtmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjgzLjE4MzU0MDM0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIKICAgICBkPSJtIDI4MzEuODUxOSwtNDU3Ni42ODYyIHYgMjgwNS4wMDg1IgogICAgIGlkPSJwYXRoNDc5OC03IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ4MDAtNSIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDI4MzEuODUxOSwtNDU3Ni42ODYyIHYgMjgwNS4wMDg1IgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNDI5MjIzMjQsMCwwLDAuMzAzMDI5MjIsNDU2Ljk4NTY4LDE0MzYuODY5NikiCiAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjYyIgLz4KICA8Y2lyY2xlCiAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO29wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTA0NDc3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjMwLjAwMDAwMTkxO3N0cm9rZS1saW5lY2FwOnNxdWFyZTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIgogICAgIGlkPSJwYXRoNDc5My00IgogICAgIGN4PSIxNjcyLjQ4MjMiCiAgICAgY3k9IjQ3NS4wMDAwMyIKICAgICByPSI0MjUuMDAwMDMiIC8+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <effect type="effectStack" enabled="0">
            <effect type="dropShadow">
              <Option type="Map">
                <Option type="QString" name="blend_mode" value="13"/>
                <Option type="QString" name="blur_level" value="2.645"/>
                <Option type="QString" name="blur_unit" value="MM"/>
                <Option type="QString" name="blur_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="color" value="0,0,0,255"/>
                <Option type="QString" name="draw_mode" value="2"/>
                <Option type="QString" name="enabled" value="0"/>
                <Option type="QString" name="offset_angle" value="135"/>
                <Option type="QString" name="offset_distance" value="2"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="offset_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="opacity" value="1"/>
              </Option>
            </effect>
            <effect type="outerGlow">
              <Option type="Map">
                <Option type="QString" name="blend_mode" value="0"/>
                <Option type="QString" name="blur_level" value="2.645"/>
                <Option type="QString" name="blur_unit" value="MM"/>
                <Option type="QString" name="blur_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="color1" value="0,0,255,255"/>
                <Option type="QString" name="color2" value="0,255,0,255"/>
                <Option type="QString" name="color_type" value="0"/>
                <Option type="QString" name="direction" value="ccw"/>
                <Option type="QString" name="discrete" value="0"/>
                <Option type="QString" name="draw_mode" value="2"/>
                <Option type="QString" name="enabled" value="0"/>
                <Option type="QString" name="opacity" value="0.5"/>
                <Option type="QString" name="rampType" value="gradient"/>
                <Option type="QString" name="single_color" value="255,255,255,255"/>
                <Option type="QString" name="spec" value="rgb"/>
                <Option type="QString" name="spread" value="2"/>
                <Option type="QString" name="spread_unit" value="MM"/>
                <Option type="QString" name="spread_unit_scale" value="3x:0,0,0,0,0,0"/>
              </Option>
            </effect>
            <effect type="transform">
              <Option type="Map">
                <Option type="QString" name="draw_mode" value="1"/>
                <Option type="QString" name="enabled" value="1"/>
                <Option type="QString" name="reflect_x" value="0"/>
                <Option type="QString" name="reflect_y" value="0"/>
                <Option type="QString" name="rotation" value="0"/>
                <Option type="QString" name="scale_x" value="0.5"/>
                <Option type="QString" name="scale_y" value="0.5"/>
                <Option type="QString" name="shear_x" value="0"/>
                <Option type="QString" name="shear_y" value="0"/>
                <Option type="QString" name="translate_unit" value="MM"/>
                <Option type="QString" name="translate_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="translate_x" value="0"/>
                <Option type="QString" name="translate_y" value="0"/>
              </Option>
            </effect>
            <effect type="innerShadow">
              <Option type="Map">
                <Option type="QString" name="blend_mode" value="13"/>
                <Option type="QString" name="blur_level" value="2.645"/>
                <Option type="QString" name="blur_unit" value="MM"/>
                <Option type="QString" name="blur_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="color" value="0,0,0,255"/>
                <Option type="QString" name="draw_mode" value="2"/>
                <Option type="QString" name="enabled" value="0"/>
                <Option type="QString" name="offset_angle" value="135"/>
                <Option type="QString" name="offset_distance" value="2"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="offset_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="opacity" value="1"/>
              </Option>
            </effect>
            <effect type="innerGlow">
              <Option type="Map">
                <Option type="QString" name="blend_mode" value="0"/>
                <Option type="QString" name="blur_level" value="2.645"/>
                <Option type="QString" name="blur_unit" value="MM"/>
                <Option type="QString" name="blur_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="color1" value="0,0,255,255"/>
                <Option type="QString" name="color2" value="0,255,0,255"/>
                <Option type="QString" name="color_type" value="0"/>
                <Option type="QString" name="direction" value="ccw"/>
                <Option type="QString" name="discrete" value="0"/>
                <Option type="QString" name="draw_mode" value="2"/>
                <Option type="QString" name="enabled" value="0"/>
                <Option type="QString" name="opacity" value="0.5"/>
                <Option type="QString" name="rampType" value="gradient"/>
                <Option type="QString" name="single_color" value="255,255,255,255"/>
                <Option type="QString" name="spec" value="rgb"/>
                <Option type="QString" name="spread" value="2"/>
                <Option type="QString" name="spread_unit" value="MM"/>
                <Option type="QString" name="spread_unit_scale" value="3x:0,0,0,0,0,0"/>
              </Option>
            </effect>
          </effect>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="size">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value=""/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="11" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{5e9298e9-2484-4411-b90e-3c382d9bd0e3}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="118,50,173,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHdpZHRoPSIxNDJjbSIKICAgaGVpZ2h0PSIzMTdjbSIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgMTQyMC4wMDAxIDMxNzAiCiAgIGlkPSJzdmczMCIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9c3VwcG9ydF9mYWNhZGUuc3ZnIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzMzQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMDM2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wMTAzNCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3OTEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0Nzg5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc4MSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NzkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0NzkxIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc4My03LTAiCiAgICAgICB4MT0iMCIKICAgICAgIHkxPSIzMDUuODMzMzQiCiAgICAgICB4Mj0iMTY5LjMzMzMzIgogICAgICAgeTI9IjMwNS44MzMzNCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09InRyYW5zbGF0ZSgtMTQxLjI4NzUsLTgzOS4xODMzKSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3NzctMy04IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDc5MSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3ODMtNy0wLTAiCiAgICAgICB4MT0iMCIKICAgICAgIHkxPSIzMDUuODMzMzQiCiAgICAgICB4Mj0iMTY5LjMzMzMzIgogICAgICAgeTI9IjMwNS44MzMzNCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09InRyYW5zbGF0ZSgtMTQxLjI4NzQ5LC04MzkuMTgzMykiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzc3LTMtOC04IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDc4MSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDExMjYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoMCwtNy44MTEwMjQsLTcuMTc5MTIxNywwLDUzNzUuMjgwNyw5NzA2Ljg5MjYpIgogICAgICAgeDE9IjAiCiAgICAgICB5MT0iMzA1LjgzMzM0IgogICAgICAgeDI9IjE2OS4zMzMzMyIKICAgICAgIHkyPSIzMDUuODMzMzQiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDEwMzYiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMTI2LTgiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE0MS4yODc0OSwtODM5LjE4MzMpIgogICAgICAgeDE9IjAiCiAgICAgICB5MT0iMzA1LjgzMzM0IgogICAgICAgeDI9IjE2OS4zMzMzMyIKICAgICAgIHkyPSIzMDUuODMzMzQiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzc3LTMtOC04LTktMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDEwMzYiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMTI2LTEiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE0MS4yODc0OSwtODM5LjE4MzMpIgogICAgICAgeDE9IjAiCiAgICAgICB5MT0iMzA1LjgzMzM0IgogICAgICAgeDI9IjE2OS4zMzMzMyIKICAgICAgIHkyPSIzMDUuODMzMzQiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzc3LTMtOC04LTktMDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQxMDM2IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50MTEyNi00IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNDEuMjg3NDksLTgzOS4xODMzKSIKICAgICAgIHgxPSIwIgogICAgICAgeTE9IjMwNS44MzMzNCIKICAgICAgIHgyPSIxNjkuMzMzMzMiCiAgICAgICB5Mj0iMzA1LjgzMzM0IiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc3Ny0zLTgtOC05LTQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQxMDM2IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50MTEyNi05IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNDEuMjg3NDksLTgzOS4xODMzKSIKICAgICAgIHgxPSIwIgogICAgICAgeTE9IjMwNS44MzMzNCIKICAgICAgIHgyPSIxNjkuMzMzMzMiCiAgICAgICB5Mj0iMzA1LjgzMzM0IiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc3Ny0zLTgtOC05LTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQxMDM2IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc4My03LTAtOSIKICAgICAgIHgxPSIwIgogICAgICAgeTE9IjMwNS44MzMzNCIKICAgICAgIHgyPSIxNjkuMzMzMzMiCiAgICAgICB5Mj0iMzA1LjgzMzM0IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KDAsLTcuODExMDIzMiwtNy41MDM0MDI5LDAsNTQ5Ni43ODE3LDE2NzM2LjgxMykiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDEwMzYiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMTI2LTktNyIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09InRyYW5zbGF0ZSgtMTQxLjI4NzQ5LC04MzkuMTgzMykiCiAgICAgICB4MT0iMCIKICAgICAgIHkxPSIzMDUuODMzMzQiCiAgICAgICB4Mj0iMTY5LjMzMzMzIgogICAgICAgeTI9IjMwNS44MzMzNCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3NzctMy04LTgtOS05LTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQxMDM2IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50MTEyNi05LTQiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLTguMjE4MzIxLDAsMCwtNy41NTM0Njg4LDEwMjY0LjY0NSwxNDcwNi42NCkiCiAgICAgICB4MT0iMCIKICAgICAgIHkxPSIzMDUuODMzMzQiCiAgICAgICB4Mj0iMTY5LjMzMzMzIgogICAgICAgeTI9IjMwNS44MzMzNCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3NzctMy04LTktMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDEwMzYiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMDcwIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNDEuMjg3NSwtODM5LjE4MzMpIgogICAgICAgeDE9IjAiCiAgICAgICB5MT0iMzA1LjgzMzM0IgogICAgICAgeDI9IjE2OS4zMzMzMyIKICAgICAgIHkyPSIzMDUuODMzMzQiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzMyIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6em9vbT0iMC4wODgxMjAxNTIiCiAgICAgaW5rc2NhcGU6Y3g9Ii03MTYuOTM3MzMiCiAgICAgaW5rc2NhcGU6Y3k9IjExNTE1LjA3NiIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMzAiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC10by1ndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtZWRnZS1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1pbnRlcnNlY3Rpb24tcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vYmplY3QtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjMxMjYuNTgsMzgwNC4zNSIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMzE1My4zMSwyNDk0Ljc5IgogICAgIGlua3NjYXBlOnNuYXAtb3RoZXJzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdGV4dC1iYXNlbGluZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ0NzczIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgICAgPGNjOmxpY2Vuc2UKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iIC8+CiAgICAgICAgPGRjOmNyZWF0b3I+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5qbWE8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOmNyZWF0b3I+CiAgICAgICAgPGRjOnB1Ymxpc2hlcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPkF6aW11dDwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cHVibGlzaGVyPgogICAgICAgIDxkYzpyaWdodHM+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT4oYylBemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnJpZ2h0cz4KICAgICAgPC9jYzpXb3JrPgogICAgICA8Y2M6TGljZW5zZQogICAgICAgICByZGY6YWJvdXQ9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNSZXByb2R1Y3Rpb24iIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rpc3RyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI05vdGljZSIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0F0dHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpwcm9oaWJpdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0NvbW1lcmNpYWxVc2UiIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rlcml2YXRpdmVXb3JrcyIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1NoYXJlQWxpa2UiIC8+CiAgICAgIDwvY2M6TGljZW5zZT4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxnCiAgICAgaWQ9ImcxMTAzIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDU5Ljk5OTk5LC0zNC45OTk5KSI+CiAgICA8ZwogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTY0MjkzMSwwLDAsMSwtNTcuOTgwNDEsMjcuNjcwNzQpIgogICAgICAgaWQ9ImcxMDg2Ij4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDQ3ODMtNy0wKTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MjQuOTkyOTQ0NzI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA0ODAuOTQ3NywtNDY0LjUwNTEgSCA3ODAuOTQ3NzEiCiAgICAgICAgIGlkPSJwYXRoNDc3NS0wLTYiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzc3LTMtOCIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSA0ODAuOTQ3NywtNDY0LjUwNTEgSCA3ODAuOTQ3NzEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTIuMDY2NjY2NiwtMS45ODUyNzU0LDAsLTIxMS42NTMxLDI4OTYuMjg3NykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOnVybCgjbGluZWFyR3JhZGllbnQ0NzgzLTctMC0wKTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MjQuOTkyOTQ0NzI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA3ODAuOTQ3NzEsLTQ2NC41MDUxIEggMTA4MC45NDc3IgogICAgICAgICBpZD0icGF0aDQ3NzUtMC02LTEiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzc3LTMtOC04IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDc4MC45NDc3MSwtNDY0LjUwNTEgSCAxMDgwLjk0NzciCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTIuMDY2NjY2OCwtMi4wNjI0NDg3LDAsLTI0Ny41MDA0OCwyODk2LjI4NzkpIiAvPgogICAgICA8cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0iZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50MTEyNik7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjE3OS42MTA3Nzg4MTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDozO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJNIDI2ODUuNDE5OSwxNTkuOTg0MzggViAyNTAzLjI5MSBaIgogICAgICAgICB0cmFuc2Zvcm09InNjYWxlKDAuMjY0NTgzMzQpIgogICAgICAgICBpZD0icGF0aDQ3NzUtMC02LTEtOCIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDExMjYtOCk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjIzLjk4NTE2NjU1O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjM7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gNzgwLjk0NzcxLC00NjQuNTA1MSBIIDEwODAuOTQ3NyIKICAgICAgICAgaWQ9InBhdGg0Nzc1LTAtNi0xLTgtMSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3NzctMy04LTgtOS0wIgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDc4MC45NDc3MSwtNDY0LjUwNTEgSCAxMDgwLjk0NzciCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0yLjE3NDQzMDgsMCwwLC0xLjk5ODUyMiwyNDA4LjYzNDIsLTg4NS45OTQ0OSkiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOnVybCgjbGluZWFyR3JhZGllbnQxMTI2LTEpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoyMy45ODUxNjY1NTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJNIDc4MC45NDc3MSwtNDY0LjUwNTEgSCAxMDgwLjk0NzciCiAgICAgICAgIGlkPSJwYXRoNDc3NS0wLTYtMS04LTMiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzc3LTMtOC04LTktMDUiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gNzgwLjk0NzcxLC00NjQuNTA1MSBIIDEwODAuOTQ3NyIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTIuMTc0NDMwOCwwLDAsLTEuOTk4NTIyLDI0MDguNjM0MiwtMjY1Ljk5NDQ3KSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDExMjYtNCk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjIzLjk4NTE2NjU1O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gNzgwLjk0NzcxLC00NjQuNTA1MSBIIDEwODAuOTQ3NyIKICAgICAgICAgaWQ9InBhdGg0Nzc1LTAtNi0xLTgtNyIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3NzctMy04LTgtOS00IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDc4MC45NDc3MSwtNDY0LjUwNTEgSCAxMDgwLjk0NzciCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0yLjE3NDQzMDgsMCwwLC0xLjk5ODUyMiwyNDA4LjYzNDIsMzU0LjAwNTUzKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDExMjYtOSk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjIzLjk4NTE2NjU1O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gNzgwLjk0NzcxLC00NjQuNTA1MSBIIDEwODAuOTQ3NyIKICAgICAgICAgaWQ9InBhdGg0Nzc1LTAtNi0xLTgtMiIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3NzctMy04LTgtOS05IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDc4MC45NDc3MSwtNDY0LjUwNTEgSCAxMDgwLjk0NzciCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0yLjE3NDQzMDgsMCwwLC0xLjk5ODUyMiwyNDA4LjYzNDIsOTc0LjAwNTQ5KSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgc3R5bGU9ImZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDQ3ODMtNy0wLTkpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxOTEuMzM3NjkyMjY7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MztzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSAyNjg1LjQxOTksOTUzMy4yMTI5IFYgMTE4NzYuNTIgWiIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjI2NDU4MzM0KSIKICAgICAgICAgaWQ9InBhdGg0Nzc1LTAtNi02IiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50MTEyNi05LTcpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoyMy45ODUxNjY1NTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJNIDc4MC45NDc3MSwtNDY0LjUwNTEgSCAxMDgwLjk0NzciCiAgICAgICAgIGlkPSJwYXRoNDc3NS0wLTYtMS04LTItMiIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3NzctMy04LTgtOS05LTIiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gNzgwLjk0NzcxLC00NjQuNTA1MSBIIDEwODAuOTQ3NyIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTIuMTc0NDMwOCwwLDAsLTEuOTk4NTIyLDI0MDguNjM0MiwxNTk0LjAwNTUpIiAvPgogICAgICA8cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0iZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50MTEyNi05LTQpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxODguOTc2Mzc5Mzk7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46YmV2ZWw7c3Ryb2tlLW1pdGVybGltaXQ6MztzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSAyMTkuOTIzODMsMTE4NzYuNTIgSCAyNjg1LjQxOTkgWiIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjI2NDU4MzM0KSIKICAgICAgICAgaWQ9InBhdGg0Nzc1LTAtNi0xLTgtMi0wIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50MTA3MCk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjI0Ljk5Mjk0NDcyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gNDgwLjk0NzcsLTQ2NC41MDUxIEggNzgwLjk0NzcxIgogICAgICAgICBpZD0icGF0aDQ3NzUtMC02LTYtMSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3NzctMy04LTktMCIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSA0ODAuOTQ3NywtNDY0LjUwNTEgSCA3ODAuOTQ3NzEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTIuMDY2NjY2NiwtMS45ODUyNzU0LDAsLTIxMS42NTMxLDM1MTYuMjg3NykiIC8+CiAgICA8L2c+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoMTA4OCIKICAgICAgIGQ9Ik0gNjUwLjAwMDAxLDE2MTkuOTk5OSAxMzI1LDE2MjQuMzc2OCIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjEwLjI3MTkxMzUzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgPC9nPgo8L3N2Zz4K"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="12" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{6b713e3f-1649-4c5d-9bc1-bc4e7b2894f7}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="120,117,172,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgd2lkdGg9IjE3MGNtIgogICBoZWlnaHQ9IjgwY20iCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDQ4MTguODY3NCAyMjY3LjcwMjMiCiAgIGlkPSJzdmcxNTAiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXNvbGFpcmUuc3ZnIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjIuMiAoNzMyYTAxZGE2MywgMjAyMi0xMi0wOSkiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJuYW1lZHZpZXcxNTIiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjMDAwMDAwIgogICAgIGJvcmRlcm9wYWNpdHk9IjAuMjUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0icHQiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjAuMjE1ODY4ODciCiAgICAgaW5rc2NhcGU6Y3g9IjMyMTIuNTk4NCIKICAgICBpbmtzY2FwZTpjeT0iMTUxMi40OTIzIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzE1MCIgLz4KICA8ZGVmcwogICAgIGlkPSJkZWZzNjIiPgogICAgPHN5bWJvbAogICAgICAgaWQ9InQiCiAgICAgICBvdmVyZmxvdz0idmlzaWJsZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gMTguNzY2LC0xLjEyNSBjIC0wLjk2ODc1LDAuNSAtMS45ODA1LDAuODc1IC0zLjAzMTIsMS4xMjUgLTEuMDQzLDAuMjU3ODEgLTIuMTM2NywwLjM5MDYyIC0zLjI4MTIsMC4zOTA2MiAtMy4zOTg0LDAgLTYuMDg5OCwtMC45NDUzMSAtOC4wNzgxLC0yLjg0MzggLTEuOTkyMiwtMS45MDYyIC0yLjk4NDQsLTQuNDg0NCAtMi45ODQ0LC03LjczNDQgMCwtMy4yNTc4IDAuOTkyMTksLTUuODM1OSAyLjk4NDQsLTcuNzM0NCAxLjk4ODMsLTEuOTA2MiA0LjY3OTcsLTIuODU5NCA4LjA3ODEsLTIuODU5NCAxLjE0NDUsMCAyLjIzODMsMC4xMzI4MSAzLjI4MTIsMC4zOTA2MiAxLjA1MDgsMC4yNSAyLjA2MjUsMC42MjUgMy4wMzEyLDEuMTI1IHYgNC4yMTg4IGMgLTAuOTgwNDcsLTAuNjU2MjUgLTEuOTQ1MywtMS4xNDA2IC0yLjg5MDYsLTEuNDUzMSAtMC45NDkyMiwtMC4zMTI1IC0xLjk0OTIsLTAuNDY4NzUgLTMsLTAuNDY4NzUgLTEuODc1LDAgLTMuMzUxNiwwLjYwNTQ3IC00LjQyMTksMS44MTI1IC0xLjA3NDIsMS4xOTkyIC0xLjYwOTQsMi44NTU1IC0xLjYwOTQsNC45Njg4IDAsMi4xMDU1IDAuNTM1MTYsMy43NjE3IDEuNjA5NCw0Ljk2ODggMS4wNzAzLDEuMTk5MiAyLjU0NjksMS43OTY5IDQuNDIxOSwxLjc5NjkgMS4wNTA4LDAgMi4wNTA4LC0wLjE0ODQ0IDMsLTAuNDUzMTIgMC45NDUzMSwtMC4zMTI1IDEuOTEwMiwtMC44MDA3OCAyLjg5MDYsLTEuNDY4OCB6IgogICAgICAgICBpZD0icGF0aDIiIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJkIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDEzLjczNCwtMTEuMTQxIGMgLTAuNDM3NSwtMC4xOTUzMSAtMC44NzEwOSwtMC4zNDM3NSAtMS4yOTY5LC0wLjQzNzUgLTAuNDE3OTcsLTAuMTAxNTYgLTAuODM5ODQsLTAuMTU2MjUgLTEuMjY1NiwtMC4xNTYyNSAtMS4yNjE3LDAgLTIuMjMwNSwwLjQwNjI1IC0yLjkwNjIsMS4yMTg4IC0wLjY3OTY5LDAuODA0NjkgLTEuMDE1NiwxLjk1MzEgLTEuMDE1NiwzLjQ1MzEgdiA3LjA2MjUgSCAyLjM1OTEgdiAtMTUuMzEyIGggNC44OTA2IHYgMi41MTU2IGMgMC42MjUsLTEgMS4zNDM4LC0xLjcyNjYgMi4xNTYyLC0yLjE4NzUgMC44MjAzMSwtMC40Njg3NSAxLjgwMDgsLTAuNzAzMTIgMi45Mzc1LC0wLjcwMzEyIDAuMTY0MDYsMCAwLjM0Mzc1LDAuMDExNzIgMC41MzEyNSwwLjAzMTI1IDAuMTk1MzEsMC4wMTE3MiAwLjQ3NjU2LDAuMDM5MDYgMC44NDM3NSwwLjA3ODEzIHoiCiAgICAgICAgIGlkPSJwYXRoNSIgLz4KICAgIDwvc3ltYm9sPgogICAgPHN5bWJvbAogICAgICAgaWQ9ImMiCiAgICAgICBvdmVyZmxvdz0idmlzaWJsZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gMTcuNjQxLC03LjcwMzEgdiAxLjQwNjIgSCA2LjE4OCBjIDAuMTI1LDEuMTQ4NCAwLjUzOTA2LDIuMDA3OCAxLjI1LDIuNTc4MSAwLjcwNzAzLDAuNTc0MjIgMS43MDMxLDAuODU5MzggMi45ODQ0LDAuODU5MzggMS4wMzEyLDAgMi4wODIsLTAuMTQ4NDQgMy4xNTYyLC0wLjQ1MzEyIDEuMDgyLC0wLjMxMjUgMi4xOTE0LC0wLjc3MzQ0IDMuMzI4MSwtMS4zOTA2IHYgMy43NjU2IGMgLTEuMTU2MiwwLjQzNzUgLTIuMzEyNSwwLjc2NTYyIC0zLjQ2ODgsMC45ODQzOCAtMS4xNTYyLDAuMjI2NTYgLTIuMzEyNSwwLjM0Mzc1IC0zLjQ2ODgsMC4zNDM3NSAtMi43NzM0LDAgLTQuOTI5NywtMC43MDMxMiAtNi40Njg4LC0yLjEwOTQgLTEuNTMxMiwtMS40MDYyIC0yLjI5NjksLTMuMzc4OSAtMi4yOTY5LC01LjkyMTkgMCwtMi41IDAuNzUzOTEsLTQuNDYwOSAyLjI2NTYsLTUuODkwNiAxLjUwNzgsLTEuNDM3NSAzLjU4MiwtMi4xNTYyIDYuMjE4OCwtMi4xNTYyIDIuNDA2MiwwIDQuMzMyLDAuNzMwNDcgNS43ODEyLDIuMTg3NSAxLjQ0NTMsMS40NDkyIDIuMTcxOSwzLjM4MjggMi4xNzE5LDUuNzk2OSB6IG0gLTUuMDMxMiwtMS42MjUgYyAwLC0wLjkyNTc4IC0wLjI3MzQ0LC0xLjY3MTkgLTAuODEyNSwtMi4yMzQ0IC0wLjU0Mjk3LC0wLjU3MDMxIC0xLjI1LC0wLjg1OTM4IC0yLjEyNSwtMC44NTkzOCAtMC45NDkyMiwwIC0xLjcxODgsMC4yNjU2MiAtMi4zMTI1LDAuNzk2ODggLTAuNTkzNywwLjUzMTI2IC0wLjk2NDg0LDEuMjk2OSAtMS4xMDk0LDIuMjk2OSB6IgogICAgICAgICBpZD0icGF0aDgiIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJrIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDkuMjE4OCwtNi44OTA2IGMgLTEuMDIzNCwwIC0xLjc5MywwLjE3MTg4IC0yLjMxMjUsMC41MTU2MiAtMC41MTE3MiwwLjM0Mzc1IC0wLjc2NTYyLDAuODU1NDcgLTAuNzY1NjIsMS41MzEyIDAsMC42MjUgMC4yMDcwMywxLjExNzIgMC42MjUsMS40Njg4IDAuNDE0MDYsMC4zNDM3NSAwLjk4ODI4LDAuNTE1NjIgMS43MTg4LDAuNTE1NjIgMC45MjU3OCwwIDEuNzAzMSwtMC4zMjgxMiAyLjMyODEsLTAuOTg0MzggMC42MzI4MSwtMC42NjQwNiAwLjk1MzEyLC0xLjQ5MjIgMC45NTMxMiwtMi40ODQ0IHYgLTAuNTYyNSB6IG0gNy40Njg4LC0xLjg0MzggViAwIGggLTQuOTIxOSB2IC0yLjI2NTYgYyAtMC42NTYyNSwwLjkyOTY5IC0xLjM5ODQsMS42MDU1IC0yLjIxODgsMi4wMzEyIC0wLjgyNDIyLDAuNDE0MDYgLTEuODI0MiwwLjYyNSAtMywwLjYyNSAtMS41ODU5LDAgLTIuODcxMSwtMC40NTcwMyAtMy44NTk0LC0xLjM3NSAtMC45OTIxOSwtMC45MjU3OCAtMS40ODQ0LC0yLjEyODkgLTEuNDg0NCwtMy42MDk0IDAsLTEuNzg5MSAwLjYxMzI4LC0zLjEwMTYgMS44NDM4LC0zLjkzNzUgMS4yMzgzLC0wLjg0Mzc1IDMuMTc5NywtMS4yNjU2IDUuODI4MSwtMS4yNjU2IGggMi44OTA2IHYgLTAuMzkwNjIgYyAwLC0wLjc2OTUzIC0wLjMwODU5LC0xLjMzMiAtMC45MjE4OCwtMS42ODc1IC0wLjYxNzE5LC0wLjM2MzI4IC0xLjU3MDMsLTAuNTQ2ODggLTIuODU5NCwtMC41NDY4OCAtMS4wNTQ3LDAgLTIuMDMxMiwwLjEwNTQ3IC0yLjkzNzUsMC4zMTI1IC0wLjg5ODQ0LDAuMjEwOTQgLTEuNzMwNSwwLjUyMzQ0IC0yLjUsMC45Mzc1IHYgLTMuNzM0NCBjIDEuMDM5MSwtMC4yNSAyLjA4NTksLTAuNDQxNDEgMy4xNDA2LC0wLjU3ODEyIDEuMDYyNSwtMC4xMzI4MSAyLjEyNSwtMC4yMDMxMiAzLjE4NzUsLTAuMjAzMTIgMi43NTc4LDAgNC43NSwwLjU0Njg4IDUuOTY4OCwxLjY0MDYgMS4yMjY2LDEuMDg1OSAxLjg0MzgsMi44NTU1IDEuODQzOCw1LjMxMjUgeiIKICAgICAgICAgaWQ9InBhdGgxMSIgLz4KICAgIDwvc3ltYm9sPgogICAgPHN5bWJvbAogICAgICAgaWQ9ImEiCiAgICAgICBvdmVyZmxvdz0idmlzaWJsZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gNy43MDMxLC0xOS42NTYgdiA0LjM0MzggSCAxMi43NSB2IDMuNSBIIDcuNzAzMSB2IDYuNSBjIDAsMC43MTA5NCAwLjE0MDYyLDEuMTg3NSAwLjQyMTg4LDEuNDM3NSAwLjI4MTI2LDAuMjUgMC44MzU5NCwwLjM3NSAxLjY3MTksMC4zNzUgaCAyLjUxNTYgdiAzLjUgaCAtNC4xODc1IGMgLTEuOTM3NSwwIC0zLjMxMjUsLTAuMzk4NDQgLTQuMTI1LC0xLjIwMzEgLTAuODA0NjksLTAuODEyNSAtMS4yMDMxLC0yLjE3OTcgLTEuMjAzMSwtNC4xMDk0IHYgLTYuNSBoIC0yLjQyMTkgdiAtMy41IGggMi40MjE5IHYgLTQuMzQzOCB6IgogICAgICAgICBpZD0icGF0aDE0IiAvPgogICAgPC9zeW1ib2w+CiAgICA8c3ltYm9sCiAgICAgICBpZD0iaiIKICAgICAgIG92ZXJmbG93PSJ2aXNpYmxlIj4KICAgICAgPHBhdGgKICAgICAgICAgZD0ibSAxMi43NjYsLTEzLjA3OCB2IC04LjIwMzEgaCA0LjkyMTkgViAtMWUtNCBIIDEyLjc2NiB2IC0yLjIxODggYyAtMC42Njc5NywwLjkwNjI1IC0xLjQwNjIsMS41NzAzIC0yLjIxODgsMS45ODQ0IC0wLjgxMjYsMC40MTQxIC0xLjc1NzgsMC42MjUgLTIuODI4MSwwLjYyNSAtMS44ODY3LDAgLTMuNDMzNiwtMC43NSAtNC42NDA2LC0yLjI1IC0xLjIxMDksLTEuNSAtMS44MTI1LC0zLjQyNTggLTEuODEyNSwtNS43ODEyIDAsLTIuMzYzMyAwLjYwMTU2LC00LjI5NjkgMS44MTI1LC01Ljc5NjkgMS4yMDcsLTEuNSAyLjc1MzksLTIuMjUgNC42NDA2LC0yLjI1IDEuMDYyNSwwIDIsMC4yMTQ4NCAyLjgxMjUsMC42NDA2MiAwLjgyMDMxLDAuNDI5NjkgMS41NjY0LDEuMDg1OSAyLjIzNDQsMS45Njg4IHogbSAtMy4yMTg4LDkuOTIxOSBjIDEuMDM5MSwwIDEuODM1OSwtMC4zNzg5MSAyLjM5MDYsLTEuMTQwNiAwLjU1MDc4LC0wLjc2OTUzIDAuODI4MTIsLTEuODgyOCAwLjgyODEyLC0zLjM0MzggMCwtMS40NTcgLTAuMjc3MzQsLTIuNTY2NCAtMC44MjgxMiwtMy4zMjgxIC0wLjU1NDY5LC0wLjc2OTUzIC0xLjM1MTYsLTEuMTU2MiAtMi4zOTA2LC0xLjE1NjIgLTEuMDQzLDAgLTEuODM5OCwwLjM4NjcyIC0yLjM5MDYsMS4xNTYyIC0wLjU1NDY5LDAuNzYxNzIgLTAuODI4MTIsMS44NzExIC0wLjgyODEyLDMuMzI4MSAwLDEuNDYwOSAwLjI3MzQ0LDIuNTc0MiAwLjgyODEyLDMuMzQzOCAwLjU1MDc4LDAuNzYxNzIgMS4zNDc3LDEuMTQwNiAyLjM5MDYsMS4xNDA2IHoiCiAgICAgICAgIGlkPSJwYXRoMTciIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJpIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDEwLjUsLTMuMTU2MiBjIDEuMDUwOCwwIDEuODUxNiwtMC4zNzg5MSAyLjQwNjIsLTEuMTQwNiAwLjU1MDc4LC0wLjc2OTUzIDAuODI4MTIsLTEuODgyOCAwLjgyODEyLC0zLjM0MzggMCwtMS40NTcgLTAuMjc3MzQsLTIuNTY2NCAtMC44MjgxMiwtMy4zMjgxIC0wLjU1NDY5LC0wLjc2OTUzIC0xLjM1NTUsLTEuMTU2MiAtMi40MDYyLC0xLjE1NjIgLTEuMDU0NywwIC0xLjg1OTQsMC4zODY3MiAtMi40MjE5LDEuMTU2MiAtMC41NTQ2OSwwLjc3MzQ0IC0wLjgyODEyLDEuODgyOCAtMC44MjgxMiwzLjMyODEgMCwxLjQ0OTIgMC4yNzM0NCwyLjU1ODYgMC44MjgxMiwzLjMyODEgMC41NjI1LDAuNzczNDQgMS4zNjcyLDEuMTU2MiAyLjQyMTksMS4xNTYyIHogbSAtMy4yNSwtOS45MjE5IGMgMC42NzU3OCwtMC44ODI4MSAxLjQyMTksLTEuNTM5MSAyLjIzNDQsLTEuOTY4OCAwLjgyMDMxLC0wLjQyNTc4IDEuNzY1NiwtMC42NDA2MiAyLjgyODEsLTAuNjQwNjIgMS44OTQ1LDAgMy40NDUzLDAuNzUgNC42NTYyLDIuMjUgMS4yMDcsMS41IDEuODEyNSwzLjQzMzYgMS44MTI1LDUuNzk2OSAwLDIuMzU1NSAtMC42MDU0Nyw0LjI4MTIgLTEuODEyNSw1Ljc4MTIgLTEuMjEwOSwxLjUgLTIuNzYxNywyLjI1IC00LjY1NjIsMi4yNSAtMS4wNjI1LDAgLTIuMDA3OCwtMC4yMTA5NCAtMi44MjgxLC0wLjYyNSBDIDguNjcxOSwtMC42NjAyIDcuOTI1OCwtMS4zMjAzMiA3LjI1LC0yLjIxODgyIFYgLTJlLTUgSCAyLjM1OTQgdiAtMjEuMjgxIEggNy4yNSBaIgogICAgICAgICBpZD0icGF0aDIwIiAvPgogICAgPC9zeW1ib2w+CiAgICA8c3ltYm9sCiAgICAgICBpZD0iaCIKICAgICAgIG92ZXJmbG93PSJ2aXNpYmxlIj4KICAgICAgPHBhdGgKICAgICAgICAgZD0ibSAwLjM0Mzc1LC0xNS4zMTIgaCA0Ljg5MDYgbCA0LjEyNSwxMC4zOTEgMy41LC0xMC4zOTEgaCA0Ljg5MDYgbCAtNi40Mzc1LDE2Ljc2NiBjIC0wLjY0ODQ0LDEuNjk1MyAtMS40MDIzLDIuODgyOCAtMi4yNjU2LDMuNTYyNSAtMC44NjcxOSwwLjY4NzUgLTIsMS4wMzEyIC0zLjQwNjIsMS4wMzEyIEggMi43OTY4NSBWIDIuODI4OSBoIDEuNTMxMiBjIDAuODMyMDMsMCAxLjQzNzUsLTAuMTM2NzIgMS44MTI1LC0wLjQwNjI1IEMgNi41MjMzNiwyLjE2MDkzIDYuODIwMjQsMS42OTIxOCA3LjAzMTE3LDEuMDE2NDUgTCA3LjE3MTc5LDAuNTk0NTcgWiIKICAgICAgICAgaWQ9InBhdGgyMyIgLz4KICAgIDwvc3ltYm9sPgogICAgPHN5bWJvbAogICAgICAgaWQ9ImciCiAgICAgICBvdmVyZmxvdz0idmlzaWJsZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gMTAuMDQ3LC0xMS4zNTkgYyAxLjEwMTYsMCAxLjg5NDUsLTAuMjAzMTIgMi4zNzUsLTAuNjA5MzggMC40NzY1NiwtMC40MTQwNiAwLjcxODc1LC0xLjA5MzggMC43MTg3NSwtMi4wMzEyIDAsLTAuOTI1NzggLTAuMjQyMTksLTEuNTg1OSAtMC43MTg3NSwtMS45ODQ0IC0wLjQ4MDQ3LC0wLjQwNjI1IC0xLjI3MzQsLTAuNjA5MzggLTIuMzc1LC0wLjYwOTM4IEggNy44MjgyIHYgNS4yMzQ0IHogTSA3LjgyODIsLTcuNzE4NCBWIDRlLTQgaCAtNS4yNSB2IC0yMC40MDYgaCA4LjAzMTIgYyAyLjY4NzUsMCA0LjY1NjIsMC40NTMxMiA1LjkwNjIsMS4zNTk0IDEuMjU3OCwwLjg5ODQ0IDEuODkwNiwyLjMyMDMgMS44OTA2LDQuMjY1NiAwLDEuMzU1NSAtMC4zMjgxMiwyLjQ2NDggLTAuOTg0MzgsMy4zMjgxIC0wLjY0ODQ0LDAuODY3MTkgLTEuNjI1LDEuNSAtMi45Mzc1LDEuOTA2MiAwLjcxODc1LDAuMTY3OTcgMS4zNTk0LDAuNTQyOTcgMS45MjE5LDEuMTI1IDAuNTcwMzEsMC41NzQyMiAxLjE0ODQsMS40NDkyIDEuNzM0NCwyLjYyNSBsIDIuODU5NCw1Ljc5NjkgaCAtNS42MDk0IGwgLTIuNDg0NCwtNS4wNzgxIGMgLTAuNSwtMS4wMTk1IC0xLjAxMTcsLTEuNzEwOSAtMS41MzEyLC0yLjA3ODEgLTAuNTExNzIsLTAuMzc1IC0xLjE5NTMsLTAuNTYyNSAtMi4wNDY5LC0wLjU2MjUgeiIKICAgICAgICAgaWQ9InBhdGgyNiIgLz4KICAgIDwvc3ltYm9sPgogICAgPHN5bWJvbAogICAgICAgaWQ9ImIiCiAgICAgICBvdmVyZmxvdz0idmlzaWJsZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gOS42NDA2LC0xMi4xODggYyAtMS4wODU5LDAgLTEuOTE0MSwwLjM5MDYyIC0yLjQ4NDQsMS4xNzE5IC0wLjU3NDIyLDAuNzgxMjUgLTAuODU5MzgsMS45MDYyIC0wLjg1OTM4LDMuMzc1IDAsMS40Njg4IDAuMjg1MTYsMi41OTM4IDAuODU5MzgsMy4zNzUgMC41NzAzMSwwLjc3MzQ0IDEuMzk4NCwxLjE1NjIgMi40ODQ0LDEuMTU2MiAxLjA2MjUsMCAxLjg3NSwtMC4zODI4MSAyLjQzNzUsLTEuMTU2MiAwLjU3MDMxLC0wLjc4MTI1IDAuODU5MzgsLTEuOTA2MiAwLjg1OTM4LC0zLjM3NSAwLC0xLjQ2ODggLTAuMjg5MDYsLTIuNTkzOCAtMC44NTkzOCwtMy4zNzUgLTAuNTYyNSwtMC43ODEyNSAtMS4zNzUsLTEuMTcxOSAtMi40Mzc1LC0xLjE3MTkgeiBtIDAsLTMuNSBjIDIuNjMyOCwwIDQuNjkxNCwwLjcxNDg0IDYuMTcxOSwyLjE0MDYgMS40NzY2LDEuNDE4IDIuMjE4OCwzLjM4NjcgMi4yMTg4LDUuOTA2MiAwLDIuNTExNyAtMC43NDIxOSw0LjQ4MDUgLTIuMjE4OCw1LjkwNjIgQyAxNC4zMzIsLTAuMzE3IDEyLjI3MzQsMC4zOSA5LjY0MDYsMC4zOSA2Ljk5MjIsMC4zOSA0LjkyNTgsLTAuMzE3MDMgMy40Mzc1LC0xLjczNSAxLjk0NTMsLTMuMTYwOCAxLjIwMzEsLTUuMTI5NSAxLjIwMzEsLTcuNjQxMiBjIDAsLTIuNTE5NSAwLjc0MjE5LC00LjQ4ODMgMi4yMzQ0LC01LjkwNjIgMS40ODgzLC0xLjQyNTggMy41NTQ3LC0yLjE0MDYgNi4yMDMxLC0yLjE0MDYgeiIKICAgICAgICAgaWQ9InBhdGgyOSIgLz4KICAgIDwvc3ltYm9sPgogICAgPHN5bWJvbAogICAgICAgaWQ9ImYiCiAgICAgICBvdmVyZmxvdz0idmlzaWJsZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Ik0gMTcuNzUsLTkuMzI4MSBWIDAgaCAtNC45MjE5IHYgLTcuMTQwNiBjIDAsLTEuMzIwMyAtMC4wMzEyNSwtMi4yMzQ0IC0wLjA5Mzc1LC0yLjczNDQgLTAuMDYyNSwtMC41IC0wLjE2Nzk3LC0wLjg2NzE5IC0wLjMxMjUsLTEuMTA5NCAtMC4xODc1LC0wLjMxMjUgLTAuNDQ5MjIsLTAuNTU0NjkgLTAuNzgxMjUsLTAuNzM0MzggLTAuMzI0MjIsLTAuMTc1NzggLTAuNjk1MzEsLTAuMjY1NjIgLTEuMTA5NCwtMC4yNjU2MiAtMS4wMjM0LDAgLTEuODI0MiwwLjM5ODQ0IC0yLjQwNjIsMS4xODc1IC0wLjU4NTk0LDAuNzgxMjUgLTAuODc1LDEuODcxMSAtMC44NzUsMy4yNjU2IFYgLTFlLTQgSCAyLjM1OTQgdiAtMTUuMzEyIEggNy4yNSB2IDIuMjM0NCBjIDAuNzM4MjgsLTAuODgyODEgMS41MTk1LC0xLjUzOTEgMi4zNDM4LC0xLjk2ODggMC44MzIwMywtMC40MjU3OCAxLjc1LC0wLjY0MDYyIDIuNzUsLTAuNjQwNjIgMS43Njk1LDAgMy4xMTMzLDAuNTQ2ODggNC4wMzEyLDEuNjQwNiAwLjkxNDA2LDEuMDg1OSAxLjM3NSwyLjY1NjIgMS4zNzUsNC43MTg4IHoiCiAgICAgICAgIGlkPSJwYXRoMzIiIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJzIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDE2Ljc4MSwtMTkuNzY2IHYgNC4zMTI1IGMgLTEuMTI1LC0wLjUgLTIuMjI2NiwtMC44NzUgLTMuMjk2OSwtMS4xMjUgLTEuMDYyNSwtMC4yNTc4MSAtMi4wNzAzLC0wLjM5MDYyIC0zLjAxNTYsLTAuMzkwNjIgLTEuMjUsMCAtMi4xNzk3LDAuMTc5NjkgLTIuNzgxMiwwLjUzMTI1IC0wLjYwNTQ3LDAuMzQzNzUgLTAuOTA2MjUsMC44ODI4MSAtMC45MDYyNSwxLjYwOTQgMCwwLjU0Mjk3IDAuMjAzMTIsMC45Njg3NSAwLjYwOTM4LDEuMjgxMiAwLjQwNjI1LDAuMzA0NjkgMS4xNDA2LDAuNTYyNSAyLjIwMzEsMC43ODEyNSBsIDIuMjUsMC40NTMxMiBjIDIuMjY5NSwwLjQ0OTIyIDMuODc4OSwxLjE0MDYgNC44MjgxLDIuMDc4MSAwLjk1NzAzLDAuOTI5NjkgMS40Mzc1LDIuMjQ2MSAxLjQzNzUsMy45NTMxIDAsMi4yNSAtMC42Njc5NywzLjkyOTcgLTIsNS4wMzEyIC0xLjMzNTksMS4wOTM4IC0zLjM3MTEsMS42NDA2IC02LjEwOTQsMS42NDA2IC0xLjMwNDcsMCAtMi42MDU1LC0wLjEyNSAtMy45MDYyLC0wLjM3NSAtMS4zMDQ3LC0wLjIzODI4IC0yLjYwNTUsLTAuNTk3NjYgLTMuOTA2MiwtMS4wNzgxIHYgLTQuNDUzMSBjIDEuMzAwOCwwLjY5OTIyIDIuNTYyNSwxLjIyNjYgMy43ODEyLDEuNTc4MSAxLjIxODgsMC4zNDM3NSAyLjM5MDYsMC41MTU2MiAzLjUxNTYsMC41MTU2MiAxLjE1NjIsMCAyLjAzNTIsLTAuMTg3NSAyLjY0MDYsLTAuNTYyNSAwLjYxMzI4LC0wLjM4MjgxIDAuOTIxODgsLTAuOTM3NSAwLjkyMTg4LC0xLjY1NjIgMCwtMC42MzI4MSAtMC4yMTA5NCwtMS4xMjUgLTAuNjI1LC0xLjQ2ODggLTAuNDE3OTcsLTAuMzQzNzUgLTEuMjQ2MSwtMC42NTYyNSAtMi40ODQ0LC0wLjkzNzUgbCAtMi4wMzEyLC0wLjQzNzUgYyAtMi4wNDMsLTAuNDM3NSAtMy41MzkxLC0xLjEzMjggLTQuNDg0NCwtMi4wOTM4IC0wLjkzNzUsLTAuOTU3MDMgLTEuNDA2MiwtMi4yNSAtMS40MDYyLC0zLjg3NSAwLC0yLjAzMTIgMC42NTYyNSwtMy41OTM4IDEuOTY4OCwtNC42ODc1IDEuMzEyNTUsLTEuMDkzNyAzLjE5NTMsLTEuNjQwNiA1LjY1NjIsLTEuNjQwNiAxLjEyNSwwIDIuMjczNCwwLjA4NTk0IDMuNDUzMSwwLjI1IDEuMTg3NSwwLjE2Nzk3IDIuNDE0MSwwLjQyMTg4IDMuNjg3NSwwLjc2NTYyIHoiCiAgICAgICAgIGlkPSJwYXRoMzUiIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJlIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDE0LjcxOSwtMTQuODI4IHYgMy45ODQ0IGMgLTAuNjU2MjUsLTAuNDU3MDMgLTEuMzI0MiwtMC43OTY4OCAtMiwtMS4wMTU2IC0wLjY2Nzk3LC0wLjIxODc1IC0xLjM1OTQsLTAuMzI4MTIgLTIuMDc4MSwtMC4zMjgxMiAtMS4zNjcyLDAgLTIuNDMzNiwwLjQwMjM0IC0zLjIwMzEsMS4yMDMxIC0wLjc2MTcyLDAuNzkyOTcgLTEuMTQwNiwxLjkwNjIgLTEuMTQwNiwzLjM0MzggMCwxLjQyOTcgMC4zNzg5MSwyLjU0MyAxLjE0MDYsMy4zNDM4IDAuNzY5NTMsMC43OTI5NyAxLjgzNTksMS4xODc1IDMuMjAzMSwxLjE4NzUgMC43NTc4MSwwIDEuNDg0NCwtMC4xMDkzOCAyLjE3MTksLTAuMzI4MTIgMC42ODc1LC0wLjIyNjU2IDEuMzIwMywtMC41NjY0MSAxLjkwNjIsLTEuMDE1NiB2IDQgYyAtMC43NjE3MiwwLjI4MTI1IC0xLjUzOTEsMC40ODgyOCAtMi4zMjgxLDAuNjI1IC0wLjc4MTI1LDAuMTQ0NTMgLTEuNTc0MiwwLjIxODc1IC0yLjM3NSwwLjIxODc1IC0yLjc2MTcsMCAtNC45MjE5LC0wLjcwNzAzIC02LjQ4NDQsLTIuMTI1IC0xLjU1NDcsLTEuNDE0MSAtMi4zMjgxLC0zLjM4MjggLTIuMzI4MSwtNS45MDYyIDAsLTIuNTMxMiAwLjc3MzQ0LC00LjUwMzkgMi4zMjgxLC01LjkyMTkgMS41NjI1LC0xLjQxNDEgMy43MjI3LC0yLjEyNSA2LjQ4NDQsLTIuMTI1IDAuODAwNzgsMCAxLjU5MzgsMC4wNzQyMiAyLjM3NSwwLjIxODc1IDAuNzgxMjUsMC4xMzY3MiAxLjU1NDcsMC4zNTE1NiAyLjMyODEsMC42NDA2MiB6IgogICAgICAgICBpZD0icGF0aDM4IiAvPgogICAgPC9zeW1ib2w+CiAgICA8c3ltYm9sCiAgICAgICBpZD0iciIKICAgICAgIG92ZXJmbG93PSJ2aXNpYmxlIj4KICAgICAgPHBhdGgKICAgICAgICAgZD0ibSAxMi40MjIsLTIxLjI4MSB2IDMuMjE4OCBIIDkuNzE4OSBjIC0wLjY4NzUsMCAtMS4xNzE5LDAuMTI1IC0xLjQ1MzEsMC4zNzUgLTAuMjczNDQsMC4yNSAtMC40MDYyNSwwLjY4NzUgLTAuNDA2MjUsMS4zMTI1IHYgMS4wNjI1IGggNC4xODc1IHYgMy41IEggNy44NTk1NSBWIC0yZS00IGggLTQuODkwNiB2IC0xMS44MTIgaCAtMi40Mzc1IHYgLTMuNSBoIDIuNDM3NSB2IC0xLjA2MjUgYyAwLC0xLjY2NDEgMC40NjA5NCwtMi44OTg0IDEuMzkwNiwtMy43MDMxIDAuOTI1NzgsLTAuODAwNzggMi4zNjcyLC0xLjIwMzEgNC4zMjgxLC0xLjIwMzEgeiIKICAgICAgICAgaWQ9InBhdGg0MSIgLz4KICAgIDwvc3ltYm9sPgogICAgPHN5bWJvbAogICAgICAgaWQ9InEiCiAgICAgICBvdmVyZmxvdz0idmlzaWJsZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gMTYuNTQ3LC0xMi43NjYgYyAwLjYxMzI4LC0wLjk0NTMxIDEuMzQ3NywtMS42NzE5IDIuMjAzMSwtMi4xNzE5IDAuODUxNTYsLTAuNSAxLjc4OTEsLTAuNzUgMi44MTI1LC0wLjc1IDEuNzU3OCwwIDMuMDk3NywwLjU0Njg4IDQuMDE1NiwxLjY0MDYgMC45MjU3OCwxLjA4NTkgMS4zOTA2LDIuNjU2MiAxLjM5MDYsNC43MTg4IFYgLTRlLTQgaCAtNC45MjE5IHYgLTcuOTg0NCAtMC4zNTkzOCBjIDAuMDA3OCwtMC4xMzI4MSAwLjAxNTYzLC0wLjMyMDMxIDAuMDE1NjMsLTAuNTYyNSAwLC0xLjA4MiAtMC4xNjQwNiwtMS44NjMzIC0wLjQ4NDM4LC0yLjM0MzggLTAuMzEyNSwtMC40ODgyOCAtMC44MjQyMiwtMC43MzQzOCAtMS41MzEyLC0wLjczNDM4IC0wLjkyOTY5LDAgLTEuNjQ4NCwwLjM4NjcyIC0yLjE1NjIsMS4xNTYyIC0wLjUxMTcyLDAuNzYxNzIgLTAuNzczNDQsMS44NjcyIC0wLjc4MTI1LDMuMzEyNSB2IDcuNTE1NiBoIC00LjkyMTkgdiAtNy45ODQ0IGMgMCwtMS42OTUzIC0wLjE0ODQ0LC0yLjc4NTIgLTAuNDM3NSwtMy4yNjU2IC0wLjI5Mjk3LC0wLjQ4ODI4IC0wLjgxMjUsLTAuNzM0MzggLTEuNTYyNSwtMC43MzQzOCAtMC45Mzc1LDAgLTEuNjY0MSwwLjM4NjcyIC0yLjE3MTksMS4xNTYyIC0wLjUxMTcyLDAuNzYxNzIgLTAuNzY1NjIsMS44NTk0IC0wLjc2NTYyLDMuMjk2OSB2IDcuNTMxMiBoIC00LjkyMTkgdiAtMTUuMzEyIGggNC45MjE5IHYgMi4yMzQ0IGMgMC42MDE1NiwtMC44NjMyOCAxLjI4OTEsLTEuNTE1NiAyLjA2MjUsLTEuOTUzMSAwLjc4MTI1LC0wLjQzNzUgMS42NDA2LC0wLjY1NjI1IDIuNTc4MSwtMC42NTYyNSAxLjA2MjUsMCAyLDAuMjU3ODEgMi44MTI1LDAuNzY1NjIgMC44MTI1LDAuNTExNzIgMS40MjU4LDEuMjMwNSAxLjg0MzgsMi4xNTYyIHoiCiAgICAgICAgIGlkPSJwYXRoNDQiIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJwIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJNIDE3Ljc1LC05LjMyODEgViAwIGggLTQuOTIxOSB2IC03LjEwOTQgYyAwLC0xLjM0MzggLTAuMDMxMjUsLTIuMjY1NiAtMC4wOTM3NSwtMi43NjU2IC0wLjA2MjUsLTAuNSAtMC4xNjc5NywtMC44NjcxOSAtMC4zMTI1LC0xLjEwOTQgLTAuMTg3NSwtMC4zMTI1IC0wLjQ0OTIyLC0wLjU1NDY5IC0wLjc4MTI1LC0wLjczNDM4IC0wLjMyNDIyLC0wLjE3NTc4IC0wLjY5NTMxLC0wLjI2NTYyIC0xLjEwOTQsLTAuMjY1NjIgLTEuMDIzNCwwIC0xLjgyNDIsMC4zOTg0NCAtMi40MDYyLDEuMTg3NSAtMC41ODU5NCwwLjc4MTI1IC0wLjg3NSwxLjg3MTEgLTAuODc1LDMuMjY1NiBWIC0xZS00IEggMi4zNTk0IHYgLTIxLjI4MSBIIDcuMjUgdiA4LjIwMzEgYyAwLjczODI4LC0wLjg4MjgxIDEuNTE5NSwtMS41MzkxIDIuMzQzOCwtMS45Njg4IDAuODMyMDMsLTAuNDI1NzggMS43NSwtMC42NDA2MiAyLjc1LC0wLjY0MDYyIDEuNzY5NSwwIDMuMTEzMywwLjU0Njg4IDQuMDMxMiwxLjY0MDYgMC45MTQwNiwxLjA4NTkgMS4zNzUsMi42NTYyIDEuMzc1LDQuNzE4OCB6IgogICAgICAgICBpZD0icGF0aDQ3IiAvPgogICAgPC9zeW1ib2w+CiAgICA8c3ltYm9sCiAgICAgICBpZD0ibyIKICAgICAgIG92ZXJmbG93PSJ2aXNpYmxlIj4KICAgICAgPHBhdGgKICAgICAgICAgZD0ibSAyLjU3ODEsLTIwLjQwNiBoIDUuODc1IGwgNy40MjE5LDE0IHYgLTE0IGggNC45ODQ0IFYgMCBoIC01Ljg3NSBMIDcuNTYyNSwtMTQgViAwIEggMi41NzgxIFoiCiAgICAgICAgIGlkPSJwYXRoNTAiIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJuIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDIuMTg3NSwtNS45Njg4IHYgLTkuMzQzOCBoIDQuOTIxOSB2IDEuNTMxMiBjIDAsMC44MzU5NCAtMC4wMDc4MSwxLjg3NSAtMC4wMTU2MjUsMy4xMjUgLTAuMDExNzE5LDEuMjUgLTAuMDE1NjI1LDIuMDg1OSAtMC4wMTU2MjUsMi41IDAsMS4yNDIyIDAuMDMxMjUsMi4xMzI4IDAuMDkzNzUsMi42NzE5IDAuMDcwMzEzLDAuNTQyOTcgMC4xNzk2OSwwLjkzMzU5IDAuMzI4MTIsMS4xNzE5IDAuMjA3MDMsMC4zMjQyMiAwLjQ3MjY2LDAuNTc0MjIgMC43OTY4OCwwLjc1IDAuMzIwMzEsMC4xNjc5NyAwLjY5MTQxLDAuMjUgMS4xMDk0LDAuMjUgMS4wMTk1LDAgMS44MjAzLC0wLjM5MDYyIDIuNDA2MiwtMS4xNzE5IDAuNTgyMDMsLTAuNzgxMjUgMC44NzUsLTEuODY3MiAwLjg3NSwtMy4yNjU2IHYgLTcuNTYyNSBoIDQuODkwNiBWIC02ZS00IEggMTIuNjg3NSBWIC0yLjIxOTQgQyAxMS45NDUzMSwtMS4zMjA5NiAxMS4xNjQxLC0wLjY2MDggMTAuMzQzNywtMC4yMzUgOS41MTk0OCwwLjE3OTA2IDguNjA5MywwLjM5IDcuNjA5MywwLjM5IDUuODQ3NiwwLjM5IDQuNTAzOCwtMC4xNDkwNiAzLjU3ODEsLTEuMjM1IDIuNjQ4NDEsLTIuMzE3IDIuMTg3NSwtMy44OTUyIDIuMTg3NSwtNS45Njk0IFoiCiAgICAgICAgIGlkPSJwYXRoNTMiIC8+CiAgICA8L3N5bWJvbD4KICAgIDxzeW1ib2wKICAgICAgIGlkPSJtIgogICAgICAgb3ZlcmZsb3c9InZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDIuNTc4MSwtMjAuNDA2IGggOC43MzQ0IGMgMi41OTM4LDAgNC41ODIsMC41NzgxMiA1Ljk2ODgsMS43MzQ0IDEuMzk0NSwxLjE0ODQgMi4wOTM4LDIuNzg5MSAyLjA5MzgsNC45MjE5IDAsMi4xMzY3IC0wLjY5OTIyLDMuNzgxMiAtMi4wOTM4LDQuOTM3NSAtMS4zODY3LDEuMTU2MiAtMy4zNzUsMS43MzQ0IC01Ljk2ODgsMS43MzQ0IEggNy44MjgxIFYgM2UtNCBoIC01LjI1IHogbSA1LjI1LDMuODEyNSB2IDUuNzAzMSBIIDEwLjc1IGMgMS4wMTk1LDAgMS44MDQ3LC0wLjI1IDIuMzU5NCwtMC43NSAwLjU2MjUsLTAuNSAwLjg0Mzc1LC0xLjIwMzEgMC44NDM3NSwtMi4xMDk0IDAsLTAuOTE0MDYgLTAuMjgxMjUsLTEuNjE3MiAtMC44NDM3NSwtMi4xMDk0IC0wLjU1NDY5LC0wLjQ4ODI4IC0xLjMzOTgsLTAuNzM0MzggLTIuMzU5NCwtMC43MzQzOCB6IgogICAgICAgICBpZD0icGF0aDU2IiAvPgogICAgPC9zeW1ib2w+CiAgICA8c3ltYm9sCiAgICAgICBpZD0ibCIKICAgICAgIG92ZXJmbG93PSJ2aXNpYmxlIj4KICAgICAgPHBhdGgKICAgICAgICAgZD0iTSAyLjM1OTQsLTE1LjMxMiBIIDcuMjUgdiAxNS4wMzEgYyAwLDIuMDUwOCAtMC40OTYwOSwzLjYxNzIgLTEuNDg0NCw0LjcwMzEgLTAuOTgwNDcsMS4wODIgLTIuNDA2MiwxLjYyNSAtNC4yODEyLDEuNjI1IEggLTAuOTM3NSBWIDIuODI4MyBoIDAuODU5MzggYyAwLjkyNTc4LDAgMS41NjI1LC0wLjIxMDk0IDEuOTA2MiwtMC42MjUgMC4zNTE1NiwtMC40MTc5NyAwLjUzMTI1LC0xLjI0NjEgMC41MzEyNSwtMi40ODQ0IHogbSAwLC01Ljk2ODggSCA3LjI1IHYgNCBIIDIuMzU5NCBaIgogICAgICAgICBpZD0icGF0aDU5IiAvPgogICAgPC9zeW1ib2w+CiAgPC9kZWZzPgogIDxnCiAgICAgaWQ9Imc0NzAiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjQ4Ljg5MTQzLDE2LjM3MDg1MSkiPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6ODQuMTg1MztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJtIDM3NjQuMDU4OSwxOTg4LjMzMzEgNzYuMjE3NCwtMTI3Ljg1NjQgLTc2LjIxNzQsLTEyNy44NTQ2IGggLTE1Mi40MzUgbCAtNzYuMjE3NCwxMjcuODU0NiA3Ni4yMTc0LDEyNy44NTY0IHogbSAtNzYuMjE3NSwtNTUuMjkwMSBjIC00Mi4wOTM3LDAgLTc2LjIxNzUsLTMyLjQ4OTEgLTc2LjIxNzUsLTcyLjU2NjMgMCwtNDAuMDc3MyAzNC4xMjM4LC03Mi41NjY0IDc2LjIxNzUsLTcyLjU2NjQgNDIuMDkzNywwIDc2LjIxNzUsMzIuNDg5MSA3Ni4yMTc1LDcyLjU2NjQgMCw0MC4wNzcyIC0zNC4xMjM4LDcyLjU2NjMgLTc2LjIxNzUsNzIuNTY2MyB6IgogICAgICAgaWQ9InBhdGg2LTAiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPHJlY3QKICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoLTEpIgogICAgICAgeT0iLTIwODMuNDU3NSIKICAgICAgIHg9Ii0zOTI5LjMyNDIiCiAgICAgICBoZWlnaHQ9IjE4OTkuMjEyNSIKICAgICAgIHdpZHRoPSIxODY3Ljg3MjYiCiAgICAgICBpZD0icmVjdDgwNzgtNyIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiM0ZDRkNGQ7c3Ryb2tlLXdpZHRoOjg0LjMzNDk7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6ODQuMTg1MztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJtIDIzNTYuMTE0NywxOTg4LjMzMzEgNzYuMjE3MywtMTI3Ljg1NjQgLTc2LjIxNzMsLTEyNy44NTQ2IGggLTE1Mi40MzUxIGwgLTc2LjIxNzMsMTI3Ljg1NDYgNzYuMjE3MywxMjcuODU2NCB6IG0gLTc2LjIxNzUsLTU1LjI5MDEgYyAtNDIuMDkzNywwIC03Ni4yMTc2LC0zMi40ODkxIC03Ni4yMTc2LC03Mi41NjYzIDAsLTQwLjA3NzMgMzQuMTIzOSwtNzIuNTY2NCA3Ni4yMTc2LC03Mi41NjY0IDQyLjA5MzcsMCA3Ni4yMTc1LDMyLjQ4OTEgNzYuMjE3NSw3Mi41NjY0IDAsNDAuMDc3MiAtMzQuMTIzOCw3Mi41NjYzIC03Ni4yMTc1LDcyLjU2NjMgeiIKICAgICAgIGlkPSJwYXRoNi03LTciCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6ODQuMTg1MztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJtIDM3NjQuMDU4OSw1NDQuOTg2OTEgNzYuMjE3NCwtMTI3Ljg1NjQyIC03Ni4yMTc0LC0xMjcuODU0NTMgaCAtMTUyLjQzNSBsIC03Ni4yMTc0LDEyNy44NTQ1MyA3Ni4yMTc0LDEyNy44NTY0MiB6IG0gLTc2LjIxNzUsLTU1LjI5MDE2IGMgLTQyLjA5MzcsMCAtNzYuMjE3NSwtMzIuNDg4OTcgLTc2LjIxNzUsLTcyLjU2NjI2IDAsLTQwLjA3NzI4IDM0LjEyMzgsLTcyLjU2NjMzIDc2LjIxNzUsLTcyLjU2NjMzIDQyLjA5MzcsMCA3Ni4yMTc1LDMyLjQ4OTA1IDc2LjIxNzUsNzIuNTY2MzMgMCw0MC4wNzcyOSAtMzQuMTIzOCw3Mi41NjYyNiAtNzYuMjE3NSw3Mi41NjYyNiB6IgogICAgICAgaWQ9InBhdGg2LTQtMSIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDo4NC4xODUzO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7bWFya2VyOm5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGQ9Im0gMjM1Ni4xMTQ2LDU0NC45ODY5MSA3Ni4yMTc0LC0xMjcuODU1NTIgLTc2LjIxNzQsLTEyNy44NTU0MyBoIC0xNTIuNDM0OSBsIC03Ni4yMTc0LDEyNy44NTU0MyA3Ni4yMTc0LDEyNy44NTU1MiB6IG0gLTc2LjIxNzUsLTU1LjI4ODY1IGMgLTQyLjA5MzcsMCAtNzYuMjE3NCwtMzIuNDg5MjggLTc2LjIxNzQsLTcyLjU2Njg3IDAsLTQwLjA3NzUgMzQuMTIzNywtNzIuNTY2ODUgNzYuMjE3NCwtNzIuNTY2ODUgNDIuMDkzOSwwIDc2LjIxNzUsMzIuNDg5MzUgNzYuMjE3NSw3Mi41NjY4NSAwLDQwLjA3NzU5IC0zNC4xMjM2LDcyLjU2Njg3IC03Ni4yMTc1LDcyLjU2Njg3IHoiCiAgICAgICBpZD0icGF0aDYtNi03IgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDxlbGxpcHNlCiAgICAgICB0cmFuc2Zvcm09InNjYWxlKC0xKSIKICAgICAgIHJ5PSI3NTIuOTI2NjQiCiAgICAgICByeD0iNzM0LjQ1OTExIgogICAgICAgY3k9Ii0xMTMxLjcyIgogICAgICAgY3g9Ii0yOTg5Ljg0NTIiCiAgICAgICBpZD0icGF0aDQ3OTMtMiIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTA0NDc4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojNGQ0ZDRkO3N0cm9rZS13aWR0aDo4NC4xNzI3O3N0cm9rZS1saW5lY2FwOnNxdWFyZTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoLTEpIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSIzOS43MDQxMjUiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii00Mi43MzgxMDEiCiAgICAgICBkPSJtIC0zODk3Ljk4NDIsLTExMzMuODUxMiBhIDMxLjM0MDAwNiwzMS45NjM3MzQgMCAwIDEgLTMxLjMyODcsMzEuOTYzNyAzMS4zNDAwMDYsMzEuOTYzNzM0IDAgMCAxIC0zMS4zNTEzLC0zMS45NDA1IDMxLjM0MDAwNiwzMS45NjM3MzQgMCAwIDEgMzEuMzA1OSwtMzEuOTg2OSAzMS4zNDAwMDYsMzEuOTYzNzM0IDAgMCAxIDMxLjM3NDEsMzEuOTE3NCIKICAgICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgc29kaXBvZGk6cnk9IjMxLjk2MzczNCIKICAgICAgIHNvZGlwb2RpOnJ4PSIzMS4zNDAwMDYiCiAgICAgICBzb2RpcG9kaTpjeT0iLTExMzMuODUxMiIKICAgICAgIHNvZGlwb2RpOmN4PSItMzkyOS4zMjQyIgogICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgaWQ9InBhdGg1MTAwIgogICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MjEuMTAwMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBzdHJva2UgZmlsbCIKICAgICAgIHNvZGlwb2RpOmFyYy10eXBlPSJhcmMiIC8+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoMTE2OSIKICAgICAgIGQ9Im0gMzg4Ny44NzM1LDExMzMuODU1MiAtODk4LjAyODQsLTIuMTM1MyIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjI4LjQ3OTM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxwYXRoCiAgICAgICBkPSJtIC00NTAuNDA3MTIsMTAzMS4zMzMzIGggMzE2Ljg5NDEgdiAxNzIuMjk1NyBoIC0zMTYuODk0MSBWIDIyMjEuOTg0NyBIIDE0NjAuMTcyNSBWIDEyMDMuNjI5IGggLTMwNC41OTAzIHYgLTI0LjYxNDggSCAyMzA2LjIzNTUgViAxMDU5LjAyNjUgSCAxMTU1LjU4MjIgdiAtMjcuNjk0NiBoIDMwNC41OTAzIFYgMTIuOTc1OTA1IEggLTQ1MC40MDcxMiBaIG0gMTg4OS4wNDQ1MiwxOTMuODQ2MiB2IDk3Mi4yMDM3IEggLTQyOC44NzE3MiBWIDEyMjUuMTc5NSBaIE0gMTA5NC4wMzg2LDEwMzEuMzMzMyBWIDEyMDMuNjI5IEggNTM3LjE3MTc4IHYgLTE3Mi4yOTU3IHogbSAtNjE4LjM5OTIyLDAgViAxMjAzLjYyOSBIIC03Mi4wMDE3MjMgViAxMDMxLjMzMzMgWiBNIDE0MzguNjIwMSwzNy41OTA4OTUgViAxMDA5LjgwNjIgSCAtNDI4Ljg4NzIyIFYgMzcuNTkwODk1IFoiCiAgICAgICBpZD0icGF0aDY0IgogICAgICAgc3R5bGU9InN0cm9rZS13aWR0aDo1LjQ5Mzk4IiAvPgogICAgPHBhdGgKICAgICAgIGQ9Im0gLTM3OS42NDk4MiwyMTU3LjM4NzcgdiAtODg2LjA2OTYgaCA1NTYuODY2MyB2IDg4Ni4wNjk2IHoiCiAgICAgICBpZD0icGF0aDY2IgogICAgICAgc3R5bGU9InN0cm9rZS13aWR0aDo1LjQ5Mzk4IiAvPgogICAgPHBhdGgKICAgICAgIGQ9Im0gMjI2LjQ0MzE4LDIxNTcuMzg3NyB2IC04ODYuMDY5NiBoIDU1Ni44Njg5IHYgODg2LjA2OTYgeiIKICAgICAgIGlkPSJwYXRoNjgiCiAgICAgICBzdHlsZT0ic3Ryb2tlLXdpZHRoOjUuNDkzOTgiIC8+CiAgICA8cGF0aAogICAgICAgZD0ibSA4MzUuNjE1MzgsMjE1Ny4zODc3IHYgLTg4Ni4wNjk2IGggNTU2Ljg2NjQyIHYgODg2LjA2OTYgeiIKICAgICAgIGlkPSJwYXRoNzAiCiAgICAgICBzdHlsZT0ic3Ryb2tlLXdpZHRoOjUuNDkzOTgiIC8+CiAgICA8cGF0aAogICAgICAgZD0iTSAtMzc5LjY1MDEyLDk2My42NTI2NiBWIDc3LjU3OTcxNSBoIDU1Ni44NjcgViA5NjMuNjUyNjYgWiIKICAgICAgIGlkPSJwYXRoNzIiCiAgICAgICBzdHlsZT0ic3Ryb2tlLXdpZHRoOjUuNDkzOTgiIC8+CiAgICA8cGF0aAogICAgICAgZD0iTSAyMjYuNDQzMjgsOTYzLjY1MjY2IFYgNzcuNTgwNDE1IGggNTU2Ljg2ODkgViA5NjMuNjUyNjYgWiIKICAgICAgIGlkPSJwYXRoNzQiCiAgICAgICBzdHlsZT0ic3Ryb2tlLXdpZHRoOjUuNDkzOTgiIC8+CiAgICA8cGF0aAogICAgICAgZD0iTSA4MzUuNjE1MDgsOTYzLjY1MjY2IFYgNzcuNTc5NzE1IEggMTM5Mi40ODIyIFYgOTYzLjY1MjY2IFoiCiAgICAgICBpZD0icGF0aDc2IgogICAgICAgc3R5bGU9InN0cm9rZS13aWR0aDo1LjQ5Mzk4IiAvPgogIDwvZz4KPC9zdmc+Cg=="/>
            <Option type="QString" name="offset" value="-2.39999999999999991,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="CASE WHEN  &quot;rot_z&quot;  IS NOT NULL THEN  &quot;rot_z&quot;   else  - &quot;rot_z&quot;  END"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="2" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{7a9b9fbb-9049-44e9-ae04-6b6a4b014877}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="195,115,186,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgdmVyc2lvbj0iMS4xIgogICB4PSIwcHgiCiAgIHk9IjBweCIKICAgdmlld0JveD0iMCAwIDU4OTYuMDYzMSA1ODk2LjA2MjkiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjIgKGRjMmFlZGFmMDMsIDIwMjItMDUtMTUpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1tYXRfZG91YmxlLnN2ZyIKICAgd2lkdGg9IjE1NmNtIgogICBoZWlnaHQ9IjE1NmNtIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUwODAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NTA3MiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDUwNzAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTA2OCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MDk0IgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZmZmMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNTA5MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDkwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDg2IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDgyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJUYWlsIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iVGFpbCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDk0MiIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgtMS4yKSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDkzMCIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsLTMuOTU4NTIyNyAwLjU0MzUyMDk0LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDkzMiIKICAgICAgICAgICBkPSJNIC0xLjI4NjY4MzIsLTMuOTU4NTIyNyAzLjA2MTcwNTMsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTM0IgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLC0zLjk1ODUyMjcgNS42NTM3NDY2LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDkzNiIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsNC4xNzc1ODM4IDAuNTQzNTIwOTQsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTM4IgogICAgICAgICAgIGQ9Ik0gLTEuMjg2NjgzMiw0LjE3NzU4MzggMy4wNjE3MDUzLDAuMjE5NzQyMjYiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDk0MCIKICAgICAgICAgICBkPSJNIDEuMzA1MzU4Miw0LjE3NzU4MzggNS42NTM3NDY2LDAuMjE5NzQyMjYiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9tYXJrZXI+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q2NzU3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJTcXVhcmVTIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iU3F1YXJlUyIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDk0MSIKICAgICAgICAgZD0iTSAtNSwtNSBWIDUgSCA1IFYgLTUgWiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoMC4yKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cyU2VuZCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkFycm93MlNlbmQiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4OTgiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuNjI1O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJNIDguNzE4NTg3OCw0LjAzMzczNTIgLTIuMjA3Mjg5NSwwLjAxNjAxMzI2IDguNzE4NTg4NCwtNC4wMDE3MDc4IGMgLTEuNzQ1NDk4NCwyLjM3MjA2MDkgLTEuNzM1NDQwOCw1LjYxNzQ1MTkgLTZlLTcsOC4wMzU0NDMgeiIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTAuMywwLDAsLTAuMywwLjY5LDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1NzY2IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJFeHBlcmltZW50YWxBcnJvdyIKICAgICAgIG9yaWVudD0iYXV0by1zdGFydC1yZXZlcnNlIgogICAgICAgcmVmWT0iMyIKICAgICAgIHJlZlg9IjUiCiAgICAgICBpZD0iRXhwZXJpbWVudGFsQXJyb3ciCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg1MTE4IgogICAgICAgICBkPSJNIDEwLDMgMCw2IFYgMCBaIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMDAwO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IlRvcnNvIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iVG9yc28iCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8ZwogICAgICAgICBpZD0iZzUwOTEiCiAgICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoMC43KSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA3NyIKICAgICAgICAgICBkPSJtIC00Ljc3OTIyODEsLTMuMjM5NTQyIGMgMi4zNTAzNzQsMC4zNjU5MzkzIDUuMzAwMjY3MzIsMS45Mzc1NDc3IDUuMDM3MTU1MzIsMy42Mjc0ODU0NiBDIC0wLjAwNTE4Nzc5LDIuMDc3ODgxOSAtMi4yMTI2NzQxLDIuNjE3NjUzOSAtNC41NjMwNDcxLDIuMjUxNzE2OSAtNi45MTM0MjIxLDEuODg1Nzc2OSAtOC41MjEwMzUsMC43NTIwMTQxNCAtOC4yNTc5MjIsLTAuOTM3OTIzMzYgLTcuOTk0ODA5LC0yLjYyNzg2MTUgLTcuMTI5NjA0MSwtMy42MDU0ODEzIC00Ljc3OTIyODEsLTMuMjM5NTQyIFoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MS4yNTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDUwNzkiCiAgICAgICAgICAgZD0iTSA0LjQ1OTg3ODksMC4wODg2NjU3NCBDIC0yLjU1NjQ1NzEsLTQuMzc4MzMyIDUuMjI0ODc2OSwtMy45MDYxODA2IC0wLjg0ODI5NTc4LC04LjcxOTczMzEiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA4MSIKICAgICAgICAgICBkPSJNIDQuOTI5ODcxOSwwLjA1NzUyMDc0IEMgLTEuMzg3MjczMSwxLjc0OTQ2ODkgMS44MDI3NTc5LDUuNDc4MjA3OSAtNC45NDQ4NzMxLDcuNTQ2MjcyNSIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxyZWN0CiAgICAgICAgICAgaWQ9InJlY3Q1MDgzIgogICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNTI3NTM2LC0wLjg0OTUzMywwLjg4NzY2OCwwLjQ2MDQ4NCwwLDApIgogICAgICAgICAgIHk9Ii0xLjc0MDg1NzUiCiAgICAgICAgICAgeD0iLTEwLjM5MTcwNiIKICAgICAgICAgICBoZWlnaHQ9IjIuNzYwODE0NyIKICAgICAgICAgICB3aWR0aD0iMi42MzY2NTgyIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgICAgIDxyZWN0CiAgICAgICAgICAgaWQ9InJlY3Q1MDg1IgogICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNjcxMjA1LC0wLjc0MTI3MiwwLjc5MDgwMiwwLjYxMjA3MiwwLDApIgogICAgICAgICAgIHk9Ii03Ljk2MjkzMDciCiAgICAgICAgICAgeD0iNC45NTg3MjY5IgogICAgICAgICAgIGhlaWdodD0iMi44NjE0MTYxIgogICAgICAgICAgIHdpZHRoPSIyLjczMjczNTYiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDUwODciCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4xMDk1MTcsMS4xMDk1MTcsMCwyNS45NjY0OCwxOS43MTYxOSkiCiAgICAgICAgICAgZD0ibSAxNi43Nzk5NTEsLTI4LjY4NTA0NSBhIDAuNjA3MzE3MjcsMC42MDczMTcyNyAwIDEgMCAtMS4yMTQ2MzQsMCAwLjYwNzMxNzI3LDAuNjA3MzE3MjcgMCAxIDAgMS4yMTQ2MzQsMCB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDUwODkiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4xMDk1MTcsMS4xMDk1MTcsMCwyNi44MjQ1LDE2Ljk5MTI2KSIKICAgICAgICAgICBkPSJtIDE2Ljc3OTk1MSwtMjguNjg1MDQ1IGEgMC42MDczMTcyNywwLjYwNzMxNzI3IDAgMSAwIC0xLjIxNDYzNCwwIDAuNjA3MzE3MjcsMC42MDczMTcyNyAwIDEgMCAxLjIxNDYzNCwwIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzFMZW5kIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cxTGVuZCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg2OCIKICAgICAgICAgZD0iTSAwLDAgNSwtNSAtMTIuNSwwIDUsNSBaIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTAuOCwwLDAsLTAuOCwtMTAsMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MkxzdGFydCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkFycm93MkxzdGFydCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg4MyIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgxLjEsMCwwLDEuMSwxLjEsMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MU1zdGFydCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkFycm93MU1zdGFydCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg3MSIKICAgICAgICAgZD0iTSAwLDAgNSwtNSAtMTIuNSwwIDUsNSBaIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC40LDAsMCwwLjQsNCwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cxTHN0YXJ0IgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cxTHN0YXJ0IgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODY1IgogICAgICAgICBkPSJNIDAsMCA1LC01IC0xMi41LDAgNSw1IFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjgsMCwwLDAuOCwxMCwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0ODYxIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDg1OSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODU3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NTEwIgogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NTAyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NDk0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NDkwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzk3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTIxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTE3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTEzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTAwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk2IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTQ2IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTQyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgxMDgiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTEwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5OCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgxMDAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDkyIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA5NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODYiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwNztzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDg4IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA4MCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwODIiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODUxMC02IgogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzJTZW5kIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cyU2VuZC01IgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODk4LTciCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuNjI1O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJNIDguNzE4NTg3OCw0LjAzMzczNTIgLTIuMjA3Mjg5NSwwLjAxNjAxMzI2IDguNzE4NTg4NCwtNC4wMDE3MDc4IGMgLTEuNzQ1NDk4NCwyLjM3MjA2MDkgLTEuNzM1NDQwOCw1LjYxNzQ1MTkgLTZlLTcsOC4wMzU0NDMgeiIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTAuMywwLDAsLTAuMywwLjY5LDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q2NzU3LTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg2MzkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUyNDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUwOTktMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTA5OSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODYzOS05IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MjQ1LTYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUwOTktMy01IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDk5LTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg2MzktNCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTI0NS00IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDk5LTMtNyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTA5OS01IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NjM5LTQtMiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTI0NS00LTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUwOTktMy03LTAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUwOTktNS02IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDgwLTQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxOTIwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMjciCiAgICAgaWQ9Im5hbWVkdmlldzEyIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDYyNDk5OTk3IgogICAgIGlua3NjYXBlOmN4PSIzNjQ4LjAwMDIiCiAgICAgaW5rc2NhcGU6Y3k9IjQ1MTIuMDAwMiIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6bG9ja2d1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIgogICAgIGZpdC1tYXJnaW4tdG9wPSIwIgogICAgIGZpdC1tYXJnaW4tbGVmdD0iMCIKICAgICBmaXQtbWFyZ2luLXJpZ2h0PSIwIgogICAgIGZpdC1tYXJnaW4tYm90dG9tPSIwIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MDc2IgogICAgICAgb3JpZ2lueD0iMC4xOTgxMTcwNiIKICAgICAgIG9yaWdpbnk9IjAuMDE4NjcwNTM3IiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iNTkyMC4xOTgyLDU4OTYuMDQ0MiIKICAgICAgIG9yaWVudGF0aW9uPSIwLjcwNzEwNjc4LC0wLjcwNzEwNjc4IgogICAgICAgaWQ9Imd1aWRlNDE0MjciCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMC4xOTgxMTcwNiw1ODk2LjA0NDIiCiAgICAgICBvcmllbnRhdGlvbj0iLTAuNzA3MTA2NzgsLTAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU0MTQyOSIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzQxMjE4IgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE1OTkuNTY4NiwxNTk5LjM4ODgpIj4KICAgIDxwYXRoCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg2LTAiCiAgICAgICBkPSJtIDMzNy4wNDc1MywyMjEuMTgxODggLTEwMC43NjY1LDE3MC4wNzE4MyAxMDAuNzY2NSwxNzAuMDY5MjggSCA1MzguNTgwNTEgTCA2MzkuMzQ2OTksMzkxLjI1MzcxIDUzOC41ODA1MSwyMjEuMTgxODggWiBtIDEwMC43NjY0OCw3My41NDU2OSBjIDU1LjY1MTc4LDAgMTAwLjc2NjUsNDMuMjE2MjQgMTAwLjc2NjUsOTYuNTI2MTQgMCw1My4zMDk5MSAtNDUuMTE0NzIsOTYuNTI2MTYgLTEwMC43NjY1LDk2LjUyNjE2IC01NS42NTE3NywwIC0xMDAuNzY2NDgsLTQzLjIxNjI2IC0xMDAuNzY2NDgsLTk2LjUyNjE2IDAsLTUzLjMwOTkgNDUuMTE0NzEsLTk2LjUyNjE1IDEwMC43NjY0OCwtOTYuNTI2MTQgeiIKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtmaWxsOiMwMDAwMDA7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMS42NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiM0ZDRkNGQ7c3Ryb2tlLXdpZHRoOjExMS41ODE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgaWQ9InJlY3Q4MDc4LTciCiAgICAgICB3aWR0aD0iMjQ0OS42NTk5IgogICAgICAgaGVpZ2h0PSIyNTMyLjI4MzQiCiAgICAgICB4PSIxMzMuMDUxNyIKICAgICAgIHk9Ijk0LjQ4ODI2NiIgLz4KICAgIDxwYXRoCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg2LTctNyIKICAgICAgIGQ9Im0gMjE5OC40Nzk2LDIyMS4xODE5IC0xMDAuNzY2NCwxNzAuMDcxOCAxMDAuNzY2NCwxNzAuMDY5MjkgaCAyMDEuNTMzMSBMIDI1MDAuNzc5MSwzOTEuMjUzNyAyNDAwLjAxMjcsMjIxLjE4MTg5IFogbSAxMDAuNzY2NCw3My41NDU2NSBjIDU1LjY1MTksMCAxMDAuNzY2Nyw0My4yMTYyNiAxMDAuNzY2Nyw5Ni41MjYxNSAwLDUzLjMwOTkxIC00NS4xMTQ4LDk2LjUyNjE2IC0xMDAuNzY2Nyw5Ni41MjYxNiAtNTUuNjUxNiwwIC0xMDAuNzY2NCwtNDMuMjE2MjYgLTEwMC43NjY0LC05Ni41MjYxNiAwLC01My4zMDk4OSA0NS4xMTQ4LC05Ni41MjYxNSAxMDAuNzY2NCwtOTYuNTI2MTUgeiIKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtmaWxsOiMwMDAwMDA7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMS42NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoNi00LTEiCiAgICAgICBkPSJtIDMzNy4wNDc1MywyMTQxLjA4OTQgLTEwMC43NjY1LDE3MC4wNzE4IDEwMC43NjY1LDE3MC4wNjkzIEggNTM4LjU4MDUxIEwgNjM5LjM0Njk5LDIzMTEuMTYxMiA1MzguNTgwNTEsMjE0MS4wODk0IFogbSAxMDAuNzY2NDgsNzMuNTQ1OCBjIDU1LjY1MTc4LDAgMTAwLjc2NjUsNDMuMjE2MSAxMDAuNzY2NSw5Ni41MjYgMCw1My4zMSAtNDUuMTE0NzIsOTYuNTI2MiAtMTAwLjc2NjUsOTYuNTI2MiAtNTUuNjUxNzcsMCAtMTAwLjc2NjQ4LC00My4yMTYyIC0xMDAuNzY2NDgsLTk2LjUyNjIgMCwtNTMuMzA5OSA0NS4xMTQ3MSwtOTYuNTI2IDEwMC43NjY0OCwtOTYuNTI2IHoiCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTEuNjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTttYXJrZXI6bm9uZTtlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgPHBhdGgKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDYtNi03IgogICAgICAgZD0ibSAyMTk4LjQ3OTcsMjE0MS4wODk0IC0xMDAuNzY2NSwxNzAuMDcwNiAxMDAuNzY2NSwxNzAuMDcwNSBoIDIwMS41MzI5IEwgMjUwMC43NzkxLDIzMTEuMTYgMjQwMC4wMTI2LDIxNDEuMDg5NCBaIG0gMTAwLjc2NjQsNzMuNTQzOCBjIDU1LjY1MiwwIDEwMC43NjY1LDQzLjIxNjUgMTAwLjc2NjUsOTYuNTI2OCAwLDUzLjMxMDMgLTQ1LjExNDUsOTYuNTI2OSAtMTAwLjc2NjUsOTYuNTI2OSAtNTUuNjUxOSwwIC0xMDAuNzY2NCwtNDMuMjE2NiAtMTAwLjc2NjQsLTk2LjUyNjkgMCwtNTMuMzEwMyA0NS4xMTQ1LC05Ni41MjY4IDEwMC43NjY0LC05Ni41MjY4IHoiCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTEuNjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTttYXJrZXI6bm9uZTtlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgPGVsbGlwc2UKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eTowLjkwNDQ3ODtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRkNGQ0ZDtzdHJva2Utd2lkdGg6MTExLjU4MTtzdHJva2UtbGluZWNhcDpzcXVhcmU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIGlkPSJwYXRoNDc5My0yIgogICAgICAgY3g9IjEzNjAuNjI5NSIKICAgICAgIGN5PSIxMzYwLjYzIgogICAgICAgcng9Ijk3MS4wMjI3MSIKICAgICAgIHJ5PSIxMDAxLjUyNjYiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjI3Ljg5NTM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgc3Ryb2tlIGZpbGwiCiAgICAgICBpZD0icGF0aDUwNzYiCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBzb2RpcG9kaTpjeD0iMTMzLjA1MTciCiAgICAgICBzb2RpcG9kaTpjeT0iMTM2MC42MyIKICAgICAgIHNvZGlwb2RpOnJ4PSI0MS4zMTE3MjIiCiAgICAgICBzb2RpcG9kaTpyeT0iNDIuMzgxMTE5IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgICBkPSJtIDE3NC4zNjM0MiwxMzYwLjYzIGEgNDEuMzExNzIyLDQyLjM4MTExOSAwIDAgMSAtNDEuMjk2NzYsNDIuMzgxMSA0MS4zMTE3MjIsNDIuMzgxMTE5IDAgMCAxIC00MS4zMjY2NzQsLTQyLjM1MDQgNDEuMzExNzIyLDQyLjM4MTExOSAwIDAgMSA0MS4yNjY4MjQsLTQyLjQxMTggNDEuMzExNzIyLDQyLjM4MTExOSAwIDAgMSA0MS4zNTY1Nyw0Mi4zMTk3IgogICAgICAgc29kaXBvZGk6YXJjLXR5cGU9ImFyYyIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDozNy4yMTM1O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICBkPSJNIDEzNjAuNjI5NCwxMzYwLjYyOTkgSCAyNTg4LjE0OTIiCiAgICAgICBpZD0icGF0aDUwNzgiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDUwODAiCiAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDEzNjAuNjI5NCwxMzYwLjYyOTkgSCAyNTg4LjE0OTIiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjI3Ljg5NTM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgc3Ryb2tlIGZpbGwiCiAgICAgICBpZD0icGF0aDUwODIiCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBzb2RpcG9kaTpjeD0iMjU4Mi43MTE3IgogICAgICAgc29kaXBvZGk6Y3k9IjEzNjAuNjI5OSIKICAgICAgIHNvZGlwb2RpOnJ4PSI0MS4zMTE3MjIiCiAgICAgICBzb2RpcG9kaTpyeT0iNDIuMzgxMTE5IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgICBkPSJtIDI2MjQuMDIzNCwxMzYwLjYyOTkgYSA0MS4zMTE3MjIsNDIuMzgxMTE5IDAgMCAxIC00MS4yOTY4LDQyLjM4MTEgNDEuMzExNzIyLDQyLjM4MTExOSAwIDAgMSAtNDEuMzI2NiwtNDIuMzUwNCA0MS4zMTE3MjIsNDIuMzgxMTE5IDAgMCAxIDQxLjI2NjgsLTQyLjQxMTggNDEuMzExNzIyLDQyLjM4MTExOSAwIDAgMSA0MS4zNTY1LDQyLjMxOTciCiAgICAgICBzb2RpcG9kaTphcmMtdHlwZT0iYXJjIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjM3LjIxMzU7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIGQ9Ik0gMTMzLjExMDIxLDEzNjAuNjI5OSBIIDEzNjAuNjMiCiAgICAgICBpZD0icGF0aDUwNzgtMiIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NTA4MC00IgogICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSAxMzMuMTEwMjEsMTM2MC42Mjk5IEggMTM2MC42MyIgLz4KICA8L2c+CiAgPGVsbGlwc2UKICAgICBzdHlsZT0iZmlsbDojMDAwMGZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMGZmO3N0cm9rZS13aWR0aDoxMC43OTg3O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtZGFzaGFycmF5Om5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6bmV3IgogICAgIGlkPSJwYXRoMTE5MzciCiAgICAgY3g9IjU3NjYuNzUyOSIKICAgICBjeT0iMjk0OC42NzM2IgogICAgIHJ4PSIzMi4zOTU5MzUiCiAgICAgcnk9IjMyLjM5NTk1NCIgLz4KICA8ZWxsaXBzZQogICAgIHN0eWxlPSJmaWxsOiMwMDAwZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiMwMDAwZmY7c3Ryb2tlLXdpZHRoOjEwLjc5ODc7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtlbmFibGUtYmFja2dyb3VuZDpuZXciCiAgICAgaWQ9InBhdGgxMTkzNy0zIgogICAgIGN4PSIxNTMuNjQzMjYiCiAgICAgY3k9IjI5NjUuNTA5NSIKICAgICByeD0iMzIuMzk1OTM1IgogICAgIHJ5PSIzMi4zOTU5NTQiIC8+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="3" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{fafa7858-7081-471d-9740-09990c5953ec}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="251,35,143,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgdmVyc2lvbj0iMS4xIgogICB4PSIwcHgiCiAgIHk9IjBweCIKICAgdmlld0JveD0iMCAwIDYwNDcuMjQ0MiA2MDQ3LjI0NDIiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjIgKGRjMmFlZGFmMDMsIDIwMjItMDUtMTUpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1tYXRfdHJpcGxlLnN2ZyIKICAgd2lkdGg9IjE2MGNtIgogICBoZWlnaHQ9IjE2MGNtIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDcyNDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDcyMzgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NzIzMiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDcyMzAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NzIyOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ3MjIyIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNzIyMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q3MjE4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDcxNjkiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A3MTY3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDcxNjUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IlRhaWwiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJUYWlsIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPGcKICAgICAgICAgaWQ9Imc0OTQyIgogICAgICAgICB0cmFuc2Zvcm09InNjYWxlKC0xLjIpIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMDAwO3N0cm9rZS1vcGFjaXR5OjEiPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTMwIgogICAgICAgICAgIGQ9Ik0gLTMuODA0ODY3NCwtMy45NTg1MjI3IDAuNTQzNTIwOTQsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTMyIgogICAgICAgICAgIGQ9Ik0gLTEuMjg2NjgzMiwtMy45NTg1MjI3IDMuMDYxNzA1MywwIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ5MzQiCiAgICAgICAgICAgZD0iTSAxLjMwNTM1ODIsLTMuOTU4NTIyNyA1LjY1Mzc0NjYsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTM2IgogICAgICAgICAgIGQ9Ik0gLTMuODA0ODY3NCw0LjE3NzU4MzggMC41NDM1MjA5NCwwLjIxOTc0MjI2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ5MzgiCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLDQuMTc3NTgzOCAzLjA2MTcwNTMsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTQwIgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLDQuMTc3NTgzOCA1LjY1Mzc0NjYsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8L2c+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDY3NTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IlNxdWFyZVMiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJTcXVhcmVTIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0OTQxIgogICAgICAgICBkPSJNIC01LC01IFYgNSBIIDUgViAtNSBaIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjIpIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzJTZW5kIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cyU2VuZCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg5OCIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4zLDAsMCwtMC4zLDAuNjksMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDU3NjYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkV4cGVyaW1lbnRhbEFycm93IgogICAgICAgb3JpZW50PSJhdXRvLXN0YXJ0LXJldmVyc2UiCiAgICAgICByZWZZPSIzIgogICAgICAgcmVmWD0iNSIKICAgICAgIGlkPSJFeHBlcmltZW50YWxBcnJvdyIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDUxMTgiCiAgICAgICAgIGQ9Ik0gMTAsMyAwLDYgViAwIFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iVG9yc28iCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJUb3JzbyIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNTA5MSIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjcpIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMDAwO3N0cm9rZS1vcGFjaXR5OjEiPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg1MDc3IgogICAgICAgICAgIGQ9Im0gLTQuNzc5MjI4MSwtMy4yMzk1NDIgYyAyLjM1MDM3NCwwLjM2NTkzOTMgNS4zMDAyNjczMiwxLjkzNzU0NzcgNS4wMzcxNTUzMiwzLjYyNzQ4NTQ2IEMgLTAuMDA1MTg3NzksMi4wNzc4ODE5IC0yLjIxMjY3NDEsMi42MTc2NTM5IC00LjU2MzA0NzEsMi4yNTE3MTY5IC02LjkxMzQyMjEsMS44ODU3NzY5IC04LjUyMTAzNSwwLjc1MjAxNDE0IC04LjI1NzkyMiwtMC45Mzc5MjMzNiAtNy45OTQ4MDksLTIuNjI3ODYxNSAtNy4xMjk2MDQxLC0zLjYwNTQ4MTMgLTQuNzc5MjI4MSwtMy4yMzk1NDIgWiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxLjI1O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA3OSIKICAgICAgICAgICBkPSJNIDQuNDU5ODc4OSwwLjA4ODY2NTc0IEMgLTIuNTU2NDU3MSwtNC4zNzgzMzIgNS4yMjQ4NzY5LC0zLjkwNjE4MDYgLTAuODQ4Mjk1NzgsLTguNzE5NzMzMSIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg1MDgxIgogICAgICAgICAgIGQ9Ik0gNC45Mjk4NzE5LDAuMDU3NTIwNzQgQyAtMS4zODcyNzMxLDEuNzQ5NDY4OSAxLjgwMjc1NzksNS40NzgyMDc5IC00Ljk0NDg3MzEsNy41NDYyNzI1IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHJlY3QKICAgICAgICAgICBpZD0icmVjdDUwODMiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC41Mjc1MzYsLTAuODQ5NTMzLDAuODg3NjY4LDAuNDYwNDg0LDAsMCkiCiAgICAgICAgICAgeT0iLTEuNzQwODU3NSIKICAgICAgICAgICB4PSItMTAuMzkxNzA2IgogICAgICAgICAgIGhlaWdodD0iMi43NjA4MTQ3IgogICAgICAgICAgIHdpZHRoPSIyLjYzNjY1ODIiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHJlY3QKICAgICAgICAgICBpZD0icmVjdDUwODUiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC42NzEyMDUsLTAuNzQxMjcyLDAuNzkwODAyLDAuNjEyMDcyLDAsMCkiCiAgICAgICAgICAgeT0iLTcuOTYyOTMwNyIKICAgICAgICAgICB4PSI0Ljk1ODcyNjkiCiAgICAgICAgICAgaGVpZ2h0PSIyLjg2MTQxNjEiCiAgICAgICAgICAgd2lkdGg9IjIuNzMyNzM1NiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA4NyIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjEwOTUxNywxLjEwOTUxNywwLDI1Ljk2NjQ4LDE5LjcxNjE5KSIKICAgICAgICAgICBkPSJtIDE2Ljc3OTk1MSwtMjguNjg1MDQ1IGEgMC42MDczMTcyNywwLjYwNzMxNzI3IDAgMSAwIC0xLjIxNDYzNCwwIDAuNjA3MzE3MjcsMC42MDczMTcyNyAwIDEgMCAxLjIxNDYzNCwwIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA4OSIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjEwOTUxNywxLjEwOTUxNywwLDI2LjgyNDUsMTYuOTkxMjYpIgogICAgICAgICAgIGQ9Im0gMTYuNzc5OTUxLC0yOC42ODUwNDUgYSAwLjYwNzMxNzI3LDAuNjA3MzE3MjcgMCAxIDAgLTEuMjE0NjM0LDAgMC42MDczMTcyNywwLjYwNzMxNzI3IDAgMSAwIDEuMjE0NjM0LDAgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8L2c+CiAgICA8L21hcmtlcj4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MUxlbmQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzFMZW5kIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODY4IgogICAgICAgICBkPSJNIDAsMCA1LC01IC0xMi41LDAgNSw1IFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC44LDAsMCwtMC44LC0xMCwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cyTHN0YXJ0IgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cyTHN0YXJ0IgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODgzIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjYyNTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4LjcxODU4NzgsNC4wMzM3MzUyIC0yLjIwNzI4OTUsMC4wMTYwMTMyNiA4LjcxODU4ODQsLTQuMDAxNzA3OCBjIC0xLjc0NTQ5ODQsMi4zNzIwNjA5IC0xLjczNTQ0MDgsNS42MTc0NTE5IC02ZS03LDguMDM1NDQzIHoiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDEuMSwwLDAsMS4xLDEuMSwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cxTXN0YXJ0IgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cxTXN0YXJ0IgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODcxIgogICAgICAgICBkPSJNIDAsMCA1LC01IC0xMi41LDAgNSw1IFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjQsMCwwLDAuNCw0LDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzFMc3RhcnQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzFMc3RhcnQiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4NjUiCiAgICAgICAgIGQ9Ik0gMCwwIDUsLTUgLTEyLjUsMCA1LDUgWiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOCwwLDAsMC44LDEwLDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4NjEiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODU5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4NTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg1MTAiCiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg1MDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMjEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODEwOCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgxMTAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDk4IgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODEwMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTIiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDk0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA4NiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDA3O3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwODgiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDgwIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NTEwLTYiCiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MlNlbmQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzJTZW5kLTUiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4OTgtNyIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4zLDAsMCwtMC4zLDAuNjksMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDY3NTctOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cyU2VuZCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkFycm93MlNlbmQtMCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg5OC03MSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4zLDAsMCwtMC4zLDAuNjksMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDY3NTctODgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MlNlbmQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzJTZW5kLTAtNSIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg5OC03MS00IgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjYyNTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4LjcxODU4NzgsNC4wMzM3MzUyIC0yLjIwNzI4OTUsMC4wMTYwMTMyNiA4LjcxODU4ODQsLTQuMDAxNzA3OCBjIC0xLjc0NTQ5ODQsMi4zNzIwNjA5IC0xLjczNTQ0MDgsNS42MTc0NTE5IC02ZS03LDguMDM1NDQzIHoiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0wLjMsMCwwLC0wLjMsMC42OSwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0Njc1Ny04OC02IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjA2MjQ5OTk5NSIKICAgICBpbmtzY2FwZTpjeD0iMjA4LjAwMDAyIgogICAgIGlua3NjYXBlOmN5PSI1MjgwLjAwMDQiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9Imc4NDkzIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpsb2NrZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjMwMzYuMTYsMjQ3NS43IgogICAgIGlua3NjYXBlOm1lYXN1cmUtZW5kPSI2ODQ0LjE2LDE2NzUuNyIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MDc2IgogICAgICAgb3JpZ2lueD0iMCIKICAgICAgIG9yaWdpbnk9IjAiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDYwNDcuMjQ0IgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTExMzciCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxnCiAgICAgaWQ9Imc4NDkzIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTY3MjcuNTU5MSkiPgogICAgPHBhdGgKICAgICAgIGQ9Im0gMjA2LjEzMjY4LDQzNDcuMzA1NyBhIDEwLDEwIDAgMCAxIC05Ljk5NjM4LDEwIDEwLDEwIDAgMCAxIC0xMC4wMDM2MiwtOS45OTI4IDEwLDEwIDAgMCAxIDkuOTg5MTMsLTEwLjAwNzIgMTAsMTAgMCAwIDEgMTAuMDEwODUsOS45ODU1IgogICAgICAgc29kaXBvZGk6b3Blbj0idHJ1ZSIKICAgICAgIHNvZGlwb2RpOmVuZD0iNi4yODE3MzY3IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTpyeT0iMTAiCiAgICAgICBzb2RpcG9kaTpyeD0iMTAiCiAgICAgICBzb2RpcG9kaTpjeT0iNDM0Ny4zMDU3IgogICAgICAgc29kaXBvZGk6Y3g9IjE5Ni4xMzI2OCIKICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgIGlkPSJwYXRoNjY3OCIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eTowLjk4ODIwODtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRkNGQ0ZDtzdHJva2Utd2lkdGg6OS40NDg4MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIKICAgICAgIHNvZGlwb2RpOmFyYy10eXBlPSJhcmMiIC8+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSIwLjcwNzEwNjgyIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItNzAuNzEwNjgyIgogICAgICAgZD0ibSAzOTQyLjExNzcsNjcxLjk5MjggYSAxMCwxMCAwIDAgMSAtOS45OTY0LDEwIDEwLDEwIDAgMCAxIC0xMC4wMDM2LC05Ljk5Mjc2IDEwLDEwIDAgMCAxIDkuOTg5MSwtMTAuMDA3MjQgMTAsMTAgMCAwIDEgMTAuMDEwOSw5Ljk4NTUxIgogICAgICAgc29kaXBvZGk6b3Blbj0idHJ1ZSIKICAgICAgIHNvZGlwb2RpOmVuZD0iNi4yODE3MzY3IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTpyeT0iMTAiCiAgICAgICBzb2RpcG9kaTpyeD0iMTAiCiAgICAgICBzb2RpcG9kaTpjeT0iNjcxLjk5MjgiCiAgICAgICBzb2RpcG9kaTpjeD0iMzkzMi4xMTc3IgogICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgaWQ9InBhdGg2Njc4LTctOCIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eTowLjk4ODIwODtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRkNGQ0ZDtzdHJva2Utd2lkdGg6OS40NDg4MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIKICAgICAgIHNvZGlwb2RpOmFyYy10eXBlPSJhcmMiIC8+CiAgICA8ZwogICAgICAgaWQ9ImcxMTYxIgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45NzE5NzUwNywwLDAsMC45NjkyNzg2NCwxNzM2LjE0OTksMTg5OS4zNDUxKSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlkPSJwYXRoNi0wIgogICAgICAgICBkPSJNIDE2Ny42NjgwMSw5MTgzLjIxMiAzNDAuNzQ4MDMsOTI4OC4xMTYzIDUxMy44MjU0MSw5MTgzLjIxMiBWIDg5NzMuNDAzMyBMIDM0MC43NDgwMyw4ODY4LjQ5OSAxNjcuNjY4MDEsODk3My40MDMzIFogbSA3NC44NDY1MywtMTA0LjkwNDQgYyAwLC01Ny45MzcgNDMuOTgwNjIsLTEwNC45MDQzIDk4LjIzMzQ5LC0xMDQuOTA0MyA1NC4yNTI4OCwwIDk4LjIzMzUsNDYuOTY3MyA5OC4yMzM1LDEwNC45MDQzIDAsNTcuOTM3MSAtNDMuOTgwNzIsMTA0LjkwNDQgLTk4LjIzMzUsMTA0LjkwNDQgLTU0LjI1Mjg3LDAgLTk4LjIzMzQ5LC00Ni45NjczIC05OC4yMzM0OSwtMTA0LjkwNDQgeiIKICAgICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTE0LjkxMztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICAgIDxyZWN0CiAgICAgICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojNGQ0ZDRkO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgaWQ9InJlY3Q4MDc4LTciCiAgICAgICAgIHdpZHRoPSIyNTU1Ljg1MTYiCiAgICAgICAgIGhlaWdodD0iMjU3Ni45NTA0IgogICAgICAgICB4PSItOTM5NS41MjM0IgogICAgICAgICB5PSIzOC43OTU0NjciIC8+CiAgICAgIDxlbGxpcHNlCiAgICAgICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTQuOTEzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOmJldmVsO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgaWQ9InBhdGg4MTA0LTciCiAgICAgICAgIGN4PSItODc3Ny40MjI5IgogICAgICAgICBjeT0iMTE5OS4xMTUiCiAgICAgICAgIHJ4PSIyMTkuOTQxODIiCiAgICAgICAgIHJ5PSI2NC4wNzc3NTkiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlkPSJwYXRoNi03LTciCiAgICAgICAgIGQ9Im0gMTY3LjY2ODAxLDcyNDUuMzQyNiAxNzMuMDgwMDIsMTA0LjkwNDIgMTczLjA3NzM4LC0xMDQuOTA0MiBWIDcwMzUuNTMzNyBMIDM0MC43NDgwMyw2OTMwLjYyOTUgMTY3LjY2ODAxLDcwMzUuNTMzNyBaIG0gNzQuODQ2NTMsLTEwNC45MDQ0IGMgMCwtNTcuOTM3IDQzLjk4MDYyLC0xMDQuOTA0NSA5OC4yMzM0OSwtMTA0LjkwNDUgNTQuMjUyNzgsMCA5OC4yMzM1LDQ2Ljk2NzUgOTguMjMzNSwxMDQuOTA0NSAwLDU3LjkzNyAtNDMuOTgwNzIsMTA0LjkwNDQgLTk4LjIzMzUsMTA0LjkwNDQgLTU0LjI1Mjg3LDAgLTk4LjIzMzQ5LC00Ni45Njc0IC05OC4yMzM0OSwtMTA0LjkwNDQgeiIKICAgICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTE0LjkxMztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlkPSJwYXRoNi00LTEiCiAgICAgICAgIGQ9Im0gMjEyMS41MzQ3LDkxODMuMjEyIDE3My4wOCwxMDQuOTA0MyAxNzMuMDc3NSwtMTA0LjkwNDMgdiAtMjA5LjgwODcgbCAtMTczLjA3NzUsLTEwNC45MDQzIC0xNzMuMDgsMTA0LjkwNDMgeiBtIDc0Ljg0NjYsLTEwNC45MDQ0IGMgMCwtNTcuOTM3IDQzLjk4MDUsLTEwNC45MDQzIDk4LjIzMzQsLTEwNC45MDQzIDU0LjI1MjksMCA5OC4yMzM1LDQ2Ljk2NzMgOTguMjMzNSwxMDQuOTA0MyAwLDU3LjkzNzEgLTQzLjk4MDYsMTA0LjkwNDQgLTk4LjIzMzUsMTA0LjkwNDQgLTU0LjI1MjksMCAtOTguMjMzNCwtNDYuOTY3MyAtOTguMjMzNCwtMTA0LjkwNDQgeiIKICAgICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTE0LjkxMztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlkPSJwYXRoNi02LTciCiAgICAgICAgIGQ9Im0gMjEyMS41MzQ3LDcyNDUuMzQyNSAxNzMuMDc4OCwxMDQuOTA0MyAxNzMuMDc4NywtMTA0LjkwNDMgdiAtMjA5LjgwODcgbCAtMTczLjA3ODcsLTEwNC45MDQzIC0xNzMuMDc4OCwxMDQuOTA0MyB6IG0gNzQuODQ0NiwtMTA0LjkwNDQgYyAwLC01Ny45MzcxIDQzLjk4MDksLTEwNC45MDQzIDk4LjIzNDIsLTEwNC45MDQzIDU0LjI1MzIsMCA5OC4yMzQyLDQ2Ljk2NzIgOTguMjM0MiwxMDQuOTA0MyAwLDU3LjkzNzMgLTQzLjk4MSwxMDQuOTA0NCAtOTguMjM0MiwxMDQuOTA0NCAtNTQuMjUzMywwIC05OC4yMzQyLC00Ni45NjcxIC05OC4yMzQyLC0xMDQuOTA0NCB6IgogICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTQuOTEzO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7bWFya2VyOm5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIgLz4KICAgICAgPGVsbGlwc2UKICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTA0NDc4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojNGQ0ZDRkO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOnNxdWFyZTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBpZD0icGF0aDQ3OTMtMiIKICAgICAgICAgY3g9Ii04MTE3LjU5ODEiCiAgICAgICAgIGN5PSIxMzI3LjI3MDQiCiAgICAgICAgIHJ4PSIxMDEwLjg5NjUiCiAgICAgICAgIHJ5PSIxMDE5LjI0MTUiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA5MzA3LDEuMDE3NjM4OSwwLC0zMTY0LjAyNTcsMTIwOTAuMzY0KSIKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MzcuNzk1MztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJNIDM5MzYuMTMyOCw0NDEzLjQ0NzggSCAyNjY5Ljk5MDgiCiAgICAgICAgIGlkPSJwYXRoNjc1NSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDY3NTciCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gMzkzNi4xMzI4LDQ0MTMuNDQ3OCBIIDI2NjkuOTkwOCIgLz4KICAgICAgPHBhdGgKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDkzMDcsMS4wMTc2Mzg5LDAsLTMxNjQuMDI1NywxMjA5MC4zNjQpIgogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDozNy43OTUzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gMzkzMS4zMjE5LDQ0MTMuNzIyMSBIIDUyMDIuMjc0NyIKICAgICAgICAgaWQ9InBhdGg2NzU1LTciCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q2NzU3LTgiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gMzkzMS4zMjE5LDQ0MTMuNzIyMSBIIDUyMDIuMjc0NyIgLz4KICAgICAgPHBhdGgKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDkzMDcsMS4wMTc2Mzg5LDAsLTMxNjQuMDI1NywxMjA5MC4zNjQpIgogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDozNy43OTUzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gMzkyMi45NjY3LDQ0MDQuMTUyNyBWIDU2NzkuNTkwMSIKICAgICAgICAgaWQ9InBhdGg2NzU1LTAiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q2NzU3LTg4IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDM5MjIuOTY2Nyw0NDA0LjE1MjcgViA1Njc5LjU5MDEiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiCiAgICAgICAgIGQ9Im0gLTkzNjcuMTc3LDEzMjcuMjcwNSBhIDI4LjM0NjQ1NywyOC4zNDY0NjIgMCAwIDEgLTI4LjMzNjIsMjguMzQ2NSAyOC4zNDY0NTcsMjguMzQ2NDYyIDAgMCAxIC0yOC4zNTY3LC0yOC4zMjYgMjguMzQ2NDU3LDI4LjM0NjQ2MiAwIDAgMSAyOC4zMTU3LC0yOC4zNjY5IDI4LjM0NjQ1NywyOC4zNDY0NjIgMCAwIDEgMjguMzc3MiwyOC4zMDUzIgogICAgICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICAgIHNvZGlwb2RpOnJ5PSIyOC4zNDY0NjIiCiAgICAgICAgIHNvZGlwb2RpOnJ4PSIyOC4zNDY0NTciCiAgICAgICAgIHNvZGlwb2RpOmN5PSIxMzI3LjI3MDUiCiAgICAgICAgIHNvZGlwb2RpOmN4PSItOTM5NS41MjM0IgogICAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICAgIGlkPSJwYXRoNTAxMyIKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjU2LjY5Mjk7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgc3Ryb2tlIGZpbGwiCiAgICAgICAgIHNvZGlwb2RpOmFyYy10eXBlPSJhcmMiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiCiAgICAgICAgIGQ9Im0gLTgxMDIuNTM5OCwyNjE1Ljc0NTggYSAyOC4zNDY0NTcsMjguMzQ2NDYgMCAwIDEgLTI4LjMzNjIsMjguMzQ2NSAyOC4zNDY0NTcsMjguMzQ2NDYgMCAwIDEgLTI4LjM1NjcsLTI4LjMyNTkgMjguMzQ2NDU3LDI4LjM0NjQ2IDAgMCAxIDI4LjMxNTcsLTI4LjM2NyAyOC4zNDY0NTcsMjguMzQ2NDYgMCAwIDEgMjguMzc3MiwyOC4zMDU0IgogICAgICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICAgIHNvZGlwb2RpOnJ5PSIyOC4zNDY0NiIKICAgICAgICAgc29kaXBvZGk6cng9IjI4LjM0NjQ1NyIKICAgICAgICAgc29kaXBvZGk6Y3k9IjI2MTUuNzQ1OCIKICAgICAgICAgc29kaXBvZGk6Y3g9Ii04MTMwLjg4NjIiCiAgICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgICAgaWQ9InBhdGg1MDE1IgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NTYuNjkyOTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBzdHJva2UgZmlsbCIKICAgICAgICAgc29kaXBvZGk6YXJjLXR5cGU9ImFyYyIgLz4KICAgICAgPHBhdGgKICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIKICAgICAgICAgZD0ibSAtNjgxMS4zMjU0LDEzMjcuMjcwNiBhIDI4LjM0NjQ1OCwyOC4zNDY0NjQgMCAwIDEgLTI4LjMzNjIsMjguMzQ2NSAyOC4zNDY0NTgsMjguMzQ2NDY0IDAgMCAxIC0yOC4zNTY3LC0yOC4zMjU5IDI4LjM0NjQ1OCwyOC4zNDY0NjQgMCAwIDEgMjguMzE1NiwtMjguMzY3IDI4LjM0NjQ1OCwyOC4zNDY0NjQgMCAwIDEgMjguMzc3MywyOC4zMDU0IgogICAgICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICAgIHNvZGlwb2RpOnJ5PSIyOC4zNDY0NjQiCiAgICAgICAgIHNvZGlwb2RpOnJ4PSIyOC4zNDY0NTgiCiAgICAgICAgIHNvZGlwb2RpOmN5PSIxMzI3LjI3MDYiCiAgICAgICAgIHNvZGlwb2RpOmN4PSItNjgzOS42NzE5IgogICAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICAgIGlkPSJwYXRoNTAxMy0wIgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NTYuNjkyOTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBzdHJva2UgZmlsbCIKICAgICAgICAgc29kaXBvZGk6YXJjLXR5cGU9ImFyYyIgLz4KICAgIDwvZz4KICAgIDxlbGxpcHNlCiAgICAgICBzdHlsZT0iZmlsbDojMDAwMGZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMGZmO3N0cm9rZS13aWR0aDoxMC43OTg3O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtZGFzaGFycmF5Om5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6bmV3IgogICAgICAgaWQ9InBhdGgxMTkzNyIKICAgICAgIGN4PSI1ODQ2LjU1NDciCiAgICAgICBjeT0iOTc1Ni4yMTM5IgogICAgICAgcng9IjMyLjM5NTkzNSIKICAgICAgIHJ5PSIzMi4zOTU5NTQiIC8+CiAgICA8ZWxsaXBzZQogICAgICAgc3R5bGU9ImZpbGw6IzAwMDBmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDBmZjtzdHJva2Utd2lkdGg6MTAuNzk4NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyIKICAgICAgIGlkPSJwYXRoMTE5MzctMCIKICAgICAgIGN4PSIzMDQwLjAyNDkiCiAgICAgICBjeT0iMTI1NzkuNjMiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii00MDIuODYxOTgiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9Ijk3LjE0NDE3NiIKICAgICAgIHJ4PSIzMi4zOTU5MzUiCiAgICAgICByeT0iMzIuMzk1OTU0IiAvPgogICAgPGNpcmNsZQogICAgICAgc3R5bGU9ImZpbGw6IzAwMDBmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDBmZjtzdHJva2Utd2lkdGg6MS44NDM2NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyIKICAgICAgIGlkPSJwYXRoMTE5MzctNiIKICAgICAgIGN4PSIzMDQzLjI3NjkiCiAgICAgICBjeT0iNjk1OS45NjU4IgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItMy4yNTIwOTg5IgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSItNi41MDQxMzMyIgogICAgICAgcj0iMzYuODczNDQ0IiAvPgogIDwvZz4KPC9zdmc+Cg=="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="4" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{fa2a3c09-41ae-4bbc-ba30-37009ab43b8e}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="251,35,143,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgdmVyc2lvbj0iMS4xIgogICB4PSIwcHgiCiAgIHk9IjBweCIKICAgdmlld0JveD0iMCAwIDYwNDcuMjQ0MiA2MDQ3LjI0NDIiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjIgKGRjMmFlZGFmMDMsIDIwMjItMDUtMTUpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1tYXRfcXVhZHJ1cGxlLnN2ZyIKICAgd2lkdGg9IjE2MGNtIgogICBoZWlnaHQ9IjE2MGNtIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDcyNDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDcyMzgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NzIzMiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDcyMzAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NzIyOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ3MjIyIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNzIyMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q3MjE4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDcxNjkiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A3MTY3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDcxNjUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IlRhaWwiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJUYWlsIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPGcKICAgICAgICAgaWQ9Imc0OTQyIgogICAgICAgICB0cmFuc2Zvcm09InNjYWxlKC0xLjIpIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMDAwO3N0cm9rZS1vcGFjaXR5OjEiPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTMwIgogICAgICAgICAgIGQ9Ik0gLTMuODA0ODY3NCwtMy45NTg1MjI3IDAuNTQzNTIwOTQsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTMyIgogICAgICAgICAgIGQ9Ik0gLTEuMjg2NjgzMiwtMy45NTg1MjI3IDMuMDYxNzA1MywwIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ5MzQiCiAgICAgICAgICAgZD0iTSAxLjMwNTM1ODIsLTMuOTU4NTIyNyA1LjY1Mzc0NjYsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTM2IgogICAgICAgICAgIGQ9Ik0gLTMuODA0ODY3NCw0LjE3NzU4MzggMC41NDM1MjA5NCwwLjIxOTc0MjI2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ5MzgiCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLDQuMTc3NTgzOCAzLjA2MTcwNTMsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0OTQwIgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLDQuMTc3NTgzOCA1LjY1Mzc0NjYsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8L2c+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDY3NTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IlNxdWFyZVMiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJTcXVhcmVTIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0OTQxIgogICAgICAgICBkPSJNIC01LC01IFYgNSBIIDUgViAtNSBaIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjIpIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzJTZW5kIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cyU2VuZCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg5OCIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4zLDAsMCwtMC4zLDAuNjksMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDU3NjYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkV4cGVyaW1lbnRhbEFycm93IgogICAgICAgb3JpZW50PSJhdXRvLXN0YXJ0LXJldmVyc2UiCiAgICAgICByZWZZPSIzIgogICAgICAgcmVmWD0iNSIKICAgICAgIGlkPSJFeHBlcmltZW50YWxBcnJvdyIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDUxMTgiCiAgICAgICAgIGQ9Ik0gMTAsMyAwLDYgViAwIFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iVG9yc28iCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJUb3JzbyIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNTA5MSIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjcpIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMDAwO3N0cm9rZS1vcGFjaXR5OjEiPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg1MDc3IgogICAgICAgICAgIGQ9Im0gLTQuNzc5MjI4MSwtMy4yMzk1NDIgYyAyLjM1MDM3NCwwLjM2NTkzOTMgNS4zMDAyNjczMiwxLjkzNzU0NzcgNS4wMzcxNTUzMiwzLjYyNzQ4NTQ2IEMgLTAuMDA1MTg3NzksMi4wNzc4ODE5IC0yLjIxMjY3NDEsMi42MTc2NTM5IC00LjU2MzA0NzEsMi4yNTE3MTY5IC02LjkxMzQyMjEsMS44ODU3NzY5IC04LjUyMTAzNSwwLjc1MjAxNDE0IC04LjI1NzkyMiwtMC45Mzc5MjMzNiAtNy45OTQ4MDksLTIuNjI3ODYxNSAtNy4xMjk2MDQxLC0zLjYwNTQ4MTMgLTQuNzc5MjI4MSwtMy4yMzk1NDIgWiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxLjI1O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA3OSIKICAgICAgICAgICBkPSJNIDQuNDU5ODc4OSwwLjA4ODY2NTc0IEMgLTIuNTU2NDU3MSwtNC4zNzgzMzIgNS4yMjQ4NzY5LC0zLjkwNjE4MDYgLTAuODQ4Mjk1NzgsLTguNzE5NzMzMSIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg1MDgxIgogICAgICAgICAgIGQ9Ik0gNC45Mjk4NzE5LDAuMDU3NTIwNzQgQyAtMS4zODcyNzMxLDEuNzQ5NDY4OSAxLjgwMjc1NzksNS40NzgyMDc5IC00Ljk0NDg3MzEsNy41NDYyNzI1IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHJlY3QKICAgICAgICAgICBpZD0icmVjdDUwODMiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC41Mjc1MzYsLTAuODQ5NTMzLDAuODg3NjY4LDAuNDYwNDg0LDAsMCkiCiAgICAgICAgICAgeT0iLTEuNzQwODU3NSIKICAgICAgICAgICB4PSItMTAuMzkxNzA2IgogICAgICAgICAgIGhlaWdodD0iMi43NjA4MTQ3IgogICAgICAgICAgIHdpZHRoPSIyLjYzNjY1ODIiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHJlY3QKICAgICAgICAgICBpZD0icmVjdDUwODUiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC42NzEyMDUsLTAuNzQxMjcyLDAuNzkwODAyLDAuNjEyMDcyLDAsMCkiCiAgICAgICAgICAgeT0iLTcuOTYyOTMwNyIKICAgICAgICAgICB4PSI0Ljk1ODcyNjkiCiAgICAgICAgICAgaGVpZ2h0PSIyLjg2MTQxNjEiCiAgICAgICAgICAgd2lkdGg9IjIuNzMyNzM1NiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA4NyIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjEwOTUxNywxLjEwOTUxNywwLDI1Ljk2NjQ4LDE5LjcxNjE5KSIKICAgICAgICAgICBkPSJtIDE2Ljc3OTk1MSwtMjguNjg1MDQ1IGEgMC42MDczMTcyNywwLjYwNzMxNzI3IDAgMSAwIC0xLjIxNDYzNCwwIDAuNjA3MzE3MjcsMC42MDczMTcyNyAwIDEgMCAxLjIxNDYzNCwwIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNTA4OSIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjEwOTUxNywxLjEwOTUxNywwLDI2LjgyNDUsMTYuOTkxMjYpIgogICAgICAgICAgIGQ9Im0gMTYuNzc5OTUxLC0yOC42ODUwNDUgYSAwLjYwNzMxNzI3LDAuNjA3MzE3MjcgMCAxIDAgLTEuMjE0NjM0LDAgMC42MDczMTcyNywwLjYwNzMxNzI3IDAgMSAwIDEuMjE0NjM0LDAgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8L2c+CiAgICA8L21hcmtlcj4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MUxlbmQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzFMZW5kIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODY4IgogICAgICAgICBkPSJNIDAsMCA1LC01IC0xMi41LDAgNSw1IFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC44LDAsMCwtMC44LC0xMCwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cyTHN0YXJ0IgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cyTHN0YXJ0IgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODgzIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjYyNTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4LjcxODU4NzgsNC4wMzM3MzUyIC0yLjIwNzI4OTUsMC4wMTYwMTMyNiA4LjcxODU4ODQsLTQuMDAxNzA3OCBjIC0xLjc0NTQ5ODQsMi4zNzIwNjA5IC0xLjczNTQ0MDgsNS42MTc0NTE5IC02ZS03LDguMDM1NDQzIHoiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDEuMSwwLDAsMS4xLDEuMSwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cxTXN0YXJ0IgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iQXJyb3cxTXN0YXJ0IgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpbmtzY2FwZTppc3N0b2NrPSJ0cnVlIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg0ODcxIgogICAgICAgICBkPSJNIDAsMCA1LC01IC0xMi41LDAgNSw1IFoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjQsMCwwLDAuNCw0LDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJBcnJvdzFMc3RhcnQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzFMc3RhcnQiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4NjUiCiAgICAgICAgIGQ9Ik0gMCwwIDUsLTUgLTEyLjUsMCA1LDUgWiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOCwwLDAsMC44LDEwLDApIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4NjEiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODU5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4NTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg1MTAiCiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg1MDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDg0OTAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMjEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODEwOCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgxMTAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDk4IgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODEwMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTIiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDk0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA4NiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDA3O3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwODgiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDgwIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4NTEwLTYiCiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MlNlbmQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzJTZW5kLTUiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDQ4OTgtNyIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4zLDAsMCwtMC4zLDAuNjksMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDY3NTctOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iQXJyb3cyU2VuZCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkFycm93MlNlbmQtMCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg5OC03MSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gOC43MTg1ODc4LDQuMDMzNzM1MiAtMi4yMDcyODk1LDAuMDE2MDEzMjYgOC43MTg1ODg0LC00LjAwMTcwNzggYyAtMS43NDU0OTg0LDIuMzcyMDYwOSAtMS43MzU0NDA4LDUuNjE3NDUxOSAtNmUtNyw4LjAzNTQ0MyB6IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgtMC4zLDAsMCwtMC4zLDAuNjksMCkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L21hcmtlcj4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDY3NTctODgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz4KICAgIDxtYXJrZXIKICAgICAgIGlua3NjYXBlOnN0b2NraWQ9IkFycm93MlNlbmQiCiAgICAgICBvcmllbnQ9ImF1dG8iCiAgICAgICByZWZZPSIwIgogICAgICAgcmVmWD0iMCIKICAgICAgIGlkPSJBcnJvdzJTZW5kLTAtNSIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlkPSJwYXRoNDg5OC03MS00IgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjYyNTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4LjcxODU4NzgsNC4wMzM3MzUyIC0yLjIwNzI4OTUsMC4wMTYwMTMyNiA4LjcxODU4ODQsLTQuMDAxNzA3OCBjIC0xLjc0NTQ5ODQsMi4zNzIwNjA5IC0xLjczNTQ0MDgsNS42MTc0NTE5IC02ZS03LDguMDM1NDQzIHoiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0wLjMsMCwwLC0wLjMsMC42OSwwKSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDwvbWFya2VyPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0Njc1Ny04OC02IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjA0NDE5NDE3IgogICAgIGlua3NjYXBlOmN4PSItMjM5OC41MDY0IgogICAgIGlua3NjYXBlOmN5PSI0MTg2LjA3MjUiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzIiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOmxvY2tndWlkZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1pbnRlcnNlY3Rpb24tcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iMzAzNi4xNiwyNDc1LjciCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjY4NDQuMTYsMTY3NS43IgogICAgIGlua3NjYXBlOmJib3gtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtZWRnZS1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MDc2IgogICAgICAgb3JpZ2lueD0iMCIKICAgICAgIG9yaWdpbnk9IjAiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIyNTYwLDM0ODcuMjQ0MSIKICAgICAgIG9yaWVudGF0aW9uPSIwLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGUxMTM2IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjU3NjAuMDAwMSw1NzI3LjI0NDIiCiAgICAgICBvcmllbnRhdGlvbj0iMC43MDcxMDY3OCwtMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTI0MzEzIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8ZwogICAgIGlkPSJnMTE3NSIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk3ODEwMjE0LDAsMCwwLjk3ODEwMjIzLC0xMTYuMzAzMDEsLTEyNDkuNDE2MykiCiAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItNDI2LjI4MzU3IgogICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iMTIzLjE3Njc2Ij4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7bWFya2VyOm5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGQ9Im0gMjE4NC4zNDQyLDMyNDUuOTQ0OSAtMTAzLjkzNywxNzAuMDggMTAzLjkzNywxNzAuMDc3NCBoIDIwNy44NzQgbCAxMDMuOTM3LC0xNzAuMDc3NCAtMTAzLjkzNywtMTcwLjA4IHogbSAxMDMuOTM3LDczLjU0OTIgYyA1Ny40MDI4LDAgMTAzLjkzNyw0My4yMTgzIDEwMy45MzcsOTYuNTMwOCAwLDUzLjMxMjUgLTQ2LjUzNDIsOTYuNTMwOCAtMTAzLjkzNyw5Ni41MzA4IC01Ny40MDI4LDAgLTEwMy45MzcsLTQzLjIxODQgLTEwMy45MzcsLTk2LjUzMDggMCwtNTMuMzEyNSA0Ni41MzQyLC05Ni41MzA4IDEwMy45MzcsLTk2LjUzMDggeiIKICAgICAgIGlkPSJwYXRoNi0wIgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDxyZWN0CiAgICAgICB5PSIzMTE5LjMwNjIiCiAgICAgICB4PSIxOTczLjk5MDciCiAgICAgICBoZWlnaHQ9IjI1MzIuMjgzNyIKICAgICAgIHdpZHRoPSIyNTMyLjI4MzciCiAgICAgICBpZD0icmVjdDgwNzgtNyIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojNGQ0ZDRkO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxlbGxpcHNlCiAgICAgICByeT0iNjIuOTY3MDg3IgogICAgICAgcng9IjIxNy45MTM3IgogICAgICAgY3k9IjQyNTkuNTEzNyIKICAgICAgIGN4PSIyNTg2LjM5MTYiCiAgICAgICBpZD0icGF0aDgxMDQtNyIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOmJldmVsO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7bWFya2VyOm5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGQ9Im0gNDEwNC4zNDQyLDMyNDUuOTQ0OSAtMTAzLjkzNjksMTcwLjA4IDEwMy45MzY5LDE3MC4wNzc0IGggMjA3Ljg3NDIgbCAxMDMuOTM2OSwtMTcwLjA3NzQgLTEwMy45MzY5LC0xNzAuMDggeiBtIDEwMy45MzcsNzMuNTQ5MiBjIDU3LjQwMjgsMCAxMDMuOTM3Miw0My4yMTgzIDEwMy45MzcyLDk2LjUzMDggMCw1My4zMTI0IC00Ni41MzQ0LDk2LjUzMDggLTEwMy45MzcyLDk2LjUzMDggLTU3LjQwMjcsMCAtMTAzLjkzNywtNDMuMjE4NCAtMTAzLjkzNywtOTYuNTMwOCAwLC01My4zMTI1IDQ2LjUzNDMsLTk2LjUzMDggMTAzLjkzNywtOTYuNTMwOCB6IgogICAgICAgaWQ9InBhdGg2LTctNyIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEzLjM4NjtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJtIDIxODQuMzQ0Miw1MTY1Ljk0NDkgLTEwMy45MzcsMTcwLjA4IDEwMy45MzcsMTcwLjA3NzUgaCAyMDcuODc0IGwgMTAzLjkzNywtMTcwLjA3NzUgLTEwMy45MzcsLTE3MC4wOCB6IG0gMTAzLjkzNyw3My41NDkzIGMgNTcuNDAyOCwwIDEwMy45MzcsNDMuMjE4MiAxMDMuOTM3LDk2LjUzMDcgMCw1My4zMTI1IC00Ni41MzQyLDk2LjUzMDggLTEwMy45MzcsOTYuNTMwOCAtNTcuNDAyOCwwIC0xMDMuOTM3LC00My4yMTgzIC0xMDMuOTM3LC05Ni41MzA4IDAsLTUzLjMxMjUgNDYuNTM0MiwtOTYuNTMwNyAxMDMuOTM3LC05Ni41MzA3IHoiCiAgICAgICBpZD0icGF0aDYtNC0xIgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7bWFya2VyOm5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGQ9Im0gNDEwNC4zNDQzLDUxNjUuOTQ0OSAtMTAzLjkzNywxNzAuMDc4OCAxMDMuOTM3LDE3MC4wNzg3IGggMjA3Ljg3NCBsIDEwMy45MzcsLTE3MC4wNzg3IC0xMDMuOTM3LC0xNzAuMDc4OCB6IG0gMTAzLjkzNyw3My41NDczIGMgNTcuNDAyOSwwIDEwMy45MzcsNDMuMjE4NiAxMDMuOTM3LDk2LjUzMTUgMCw1My4zMTI4IC00Ni41MzQxLDk2LjUzMTUgLTEwMy45MzcsOTYuNTMxNSAtNTcuNDAzLDAgLTEwMy45MzcsLTQzLjIxODcgLTEwMy45MzcsLTk2LjUzMTUgMCwtNTMuMzEyOSA0Ni41MzQsLTk2LjUzMTUgMTAzLjkzNywtOTYuNTMxNSB6IgogICAgICAgaWQ9InBhdGg2LTYtNyIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8ZWxsaXBzZQogICAgICAgcnk9IjEwMDEuNTc0OCIKICAgICAgIHJ4PSIxMDAxLjU3NDkiCiAgICAgICBjeT0iNDM4NS40NDc4IgogICAgICAgY3g9IjMyNDAuMTMyMSIKICAgICAgIGlkPSJwYXRoNDc5My0yIgogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTA0NDc4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojNGQ0ZDRkO3N0cm9rZS13aWR0aDoxMTMuMzg2O3N0cm9rZS1saW5lY2FwOnNxdWFyZTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gMzkzNi4xMzI4LDQ0MTMuNDQ3OCBIIDI2NjkuOTkwOCIKICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q2NzU3IgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoNjc1NSIKICAgICAgIGQ9Ik0gMzkzNi4xMzI4LDQ0MTMuNDQ3OCBIIDI2NjkuOTkwOCIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjM3Ljc5NTM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02OTYuMDAwMDUsLTI4LjAwMDAwNikiIC8+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSAzOTMxLjMyMTksNDQxMy43MjIxIEggNTIwMi4yNzQ3IgogICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDY3NTctOCIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDY3NTUtNyIKICAgICAgIGQ9Ik0gMzkzMS4zMjE5LDQ0MTMuNzIyMSBIIDUyMDIuMjc0NyIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjM3Ljc5NTM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02OTYuMDAwMDUsLTI4LjAwMDAwNikiIC8+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSAzOTIyLjk2NjcsNDQwNC4xNTI3IFYgNTY3OS41OTAxIgogICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDY3NTctODgiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg2NzU1LTAiCiAgICAgICBkPSJNIDM5MjIuOTY2Nyw0NDA0LjE1MjcgViA1Njc5LjU5MDEiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDozNy43OTUzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNjk2LjAwMDA1LC0yOC4wMDAwMDYpIiAvPgogICAgPHBhdGgKICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gMzkyMi45NjY3LDQ0MTMuNDQ3OSBWIDMxNDcuMzA2MyIKICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q2NzU3LTg4LTYiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg2NzU1LTAtNyIKICAgICAgIGQ9Ik0gMzkyMi45NjY3LDQ0MTMuNDQ3OSBWIDMxNDcuMzA2MyIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjM3Ljc5NTM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02OTYuMDAwMDUsLTI4LjAwMDAwNikiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjU2LjY5Mjk7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgc3Ryb2tlIGZpbGwiCiAgICAgICBpZD0icGF0aDUwMTEiCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBzb2RpcG9kaTpjeD0iNDUwNi4yNzQ0IgogICAgICAgc29kaXBvZGk6Y3k9IjQzODUuNDQ4MiIKICAgICAgIHNvZGlwb2RpOnJ4PSIyOC4zNDY0NiIKICAgICAgIHNvZGlwb2RpOnJ5PSIyOC4zNDY0NTUiCiAgICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICAgIHNvZGlwb2RpOmVuZD0iNi4yODE3MzY3IgogICAgICAgc29kaXBvZGk6b3Blbj0idHJ1ZSIKICAgICAgIGQ9Im0gNDUzNC42MjA5LDQzODUuNDQ4MiBhIDI4LjM0NjQ2LDI4LjM0NjQ1NSAwIDAgMSAtMjguMzM2MiwyOC4zNDY1IDI4LjM0NjQ2LDI4LjM0NjQ1NSAwIDAgMSAtMjguMzU2NywtMjguMzI1OSAyOC4zNDY0NiwyOC4zNDY0NTUgMCAwIDEgMjguMzE1NiwtMjguMzY3IDI4LjM0NjQ2LDI4LjM0NjQ1NSAwIDAgMSAyOC4zNzcyLDI4LjMwNTQiCiAgICAgICBzb2RpcG9kaTphcmMtdHlwZT0iYXJjIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDo1Ni42OTI5O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIHN0cm9rZSBmaWxsIgogICAgICAgaWQ9InBhdGg1MDEzIgogICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgc29kaXBvZGk6Y3g9IjE5NzMuOTkwNyIKICAgICAgIHNvZGlwb2RpOmN5PSI0Mzg1LjQ0NzgiCiAgICAgICBzb2RpcG9kaTpyeD0iMjguMzQ2NDYiCiAgICAgICBzb2RpcG9kaTpyeT0iMjguMzQ2NDU1IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgICBkPSJtIDIwMDIuMzM3Miw0Mzg1LjQ0NzggYSAyOC4zNDY0NiwyOC4zNDY0NTUgMCAwIDEgLTI4LjMzNjIsMjguMzQ2NCAyOC4zNDY0NiwyOC4zNDY0NTUgMCAwIDEgLTI4LjM1NjcsLTI4LjMyNTkgMjguMzQ2NDYsMjguMzQ2NDU1IDAgMCAxIDI4LjMxNTYsLTI4LjM2NyAyOC4zNDY0NiwyOC4zNDY0NTUgMCAwIDEgMjguMzc3MywyOC4zMDU0IgogICAgICAgc29kaXBvZGk6YXJjLXR5cGU9ImFyYyIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NTYuNjkyOTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBzdHJva2UgZmlsbCIKICAgICAgIGlkPSJwYXRoNTAxNSIKICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgIHNvZGlwb2RpOmN4PSIzMjI2Ljk2NjYiCiAgICAgICBzb2RpcG9kaTpjeT0iNTY1MS41ODk4IgogICAgICAgc29kaXBvZGk6cng9IjI4LjM0NjQ1OCIKICAgICAgIHNvZGlwb2RpOnJ5PSIyOC4zNDY0NTUiCiAgICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICAgIHNvZGlwb2RpOmVuZD0iNi4yODE3MzY3IgogICAgICAgc29kaXBvZGk6b3Blbj0idHJ1ZSIKICAgICAgIGQ9Im0gMzI1NS4zMTMsNTY1MS41ODk4IGEgMjguMzQ2NDU4LDI4LjM0NjQ1NSAwIDAgMSAtMjguMzM2MiwyOC4zNDY1IDI4LjM0NjQ1OCwyOC4zNDY0NTUgMCAwIDEgLTI4LjM1NjcsLTI4LjMyNTkgMjguMzQ2NDU4LDI4LjM0NjQ1NSAwIDAgMSAyOC4zMTU3LC0yOC4zNjcgMjguMzQ2NDU4LDI4LjM0NjQ1NSAwIDAgMSAyOC4zNzcyLDI4LjMwNTQiCiAgICAgICBzb2RpcG9kaTphcmMtdHlwZT0iYXJjIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDo1Ni42OTM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgc3Ryb2tlIGZpbGwiCiAgICAgICBpZD0icGF0aDUwMTciCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBzb2RpcG9kaTpjeD0iMzIyNi45NjY2IgogICAgICAgc29kaXBvZGk6Y3k9IjMxMTkuMzA2NCIKICAgICAgIHNvZGlwb2RpOnJ4PSIyOC4zNDY0NzIiCiAgICAgICBzb2RpcG9kaTpyeT0iMjguMzQ2NDUxIgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgICBkPSJtIDMyNTUuMzEzLDMxMTkuMzA2NCBhIDI4LjM0NjQ3MiwyOC4zNDY0NTEgMCAwIDEgLTI4LjMzNjIsMjguMzQ2NCAyOC4zNDY0NzIsMjguMzQ2NDUxIDAgMCAxIC0yOC4zNTY3LC0yOC4zMjU5IDI4LjM0NjQ3MiwyOC4zNDY0NTEgMCAwIDEgMjguMzE1NywtMjguMzY2OSAyOC4zNDY0NzIsMjguMzQ2NDUxIDAgMCAxIDI4LjM3NzIsMjguMzA1MyIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iNjYuMDAwMTI3IgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSItMTAyIgogICAgICAgc29kaXBvZGk6YXJjLXR5cGU9ImFyYyIgLz4KICA8L2c+CiAgPGVsbGlwc2UKICAgICBzdHlsZT0iZmlsbDojMDAwMGZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojMDAwMGZmO3N0cm9rZS13aWR0aDoxMC43OTg3O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtZGFzaGFycmF5Om5vbmU7ZW5hYmxlLWJhY2tncm91bmQ6bmV3IgogICAgIGlkPSJwYXRoMTE5MzciCiAgICAgY3g9IjU4NDYuNTU1MiIKICAgICBjeT0iMzAyOC42NTQ4IgogICAgIHJ4PSIzMi4zOTU5MzUiCiAgICAgcnk9IjMyLjM5NTk1NCIgLz4KICA8ZWxsaXBzZQogICAgIHN0eWxlPSJmaWxsOiMwMDAwZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiMwMDAwZmY7c3Ryb2tlLXdpZHRoOjEwLjc5ODc7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtlbmFibGUtYmFja2dyb3VuZDpuZXciCiAgICAgaWQ9InBhdGgxMTkzNy0wIgogICAgIGN4PSIzMDQwLjAyNDkiCiAgICAgY3k9IjU4NTIuMDcwOCIKICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii00MDIuODYxOTgiCiAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI5Ny4xNDQxNzYiCiAgICAgcng9IjMyLjM5NTkzNSIKICAgICByeT0iMzIuMzk1OTU0IiAvPgogIDxlbGxpcHNlCiAgICAgc3R5bGU9ImZpbGw6IzAwMDBmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDBmZjtzdHJva2Utd2lkdGg6MTAuNzk4NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyIKICAgICBpZD0icGF0aDExOTM3LTMiCiAgICAgY3g9IjIzMy40NDQ4MiIKICAgICBjeT0iMzA0NS40OTA3IgogICAgIHJ4PSIzMi4zOTU5MzUiCiAgICAgcnk9IjMyLjM5NTk1NCIgLz4KICA8Y2lyY2xlCiAgICAgc3R5bGU9ImZpbGw6IzAwMDBmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDBmZjtzdHJva2Utd2lkdGg6MS44NDM2NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyIKICAgICBpZD0icGF0aDExOTM3LTYiCiAgICAgY3g9IjMwNDMuMjc2OSIKICAgICBjeT0iMjMyLjQwNjQ4IgogICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTMuMjUyMDk4OSIKICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9Ii02LjUwNDEzMzIiCiAgICAgcj0iMzYuODczNDQ0IiAvPgo8L3N2Zz4K"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="5" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{8239cc94-f567-4005-99d7-403accd755ae}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="175,175,175,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9Ijk1Y20iCiAgIGhlaWdodD0iMTYwY20iCiAgIHZpZXdCb3g9IjAgMCA5NTAuMDAwMDEgMTYwMCIKICAgaWQ9InN2ZzIiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9cG90ZWF1X2JldG9uLnN2ZyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ2MzE4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNjMxNiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q2MzE0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDYzMTAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ2MjY4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNjI2NiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3OTkiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0Nzk3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0Nzg5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc4NyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODA0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4MDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0NzkyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3ODgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc4NCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0NzgwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDEzODIyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDE0ODU3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDE0ODUzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDE0ODQ5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDE0ODQ1IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50MTQ4MzEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAzMDMwMztzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3AxNDgzMyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDE0ODI1IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wMTQ4MjciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxNDgxNSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDE0ODE3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEzOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEwMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA4NCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDgwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwNzYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA3MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBpZD0iYmFzZSIKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMS4wIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp6b29tPSIwLjA1MzY5NDQ0NyIKICAgICBpbmtzY2FwZTpjeD0iMjkyNC4zNTk1IgogICAgIGlua3NjYXBlOmN5PSIxODg1LjAwMTciCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9ImxheWVyMSIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIHNob3dib3JkZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1nbG9iYWw9InRydWUiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxMDI0IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjcwNSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb3RoZXJzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJmYWxzZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1pbnRlcnNlY3Rpb24tcGF0aHM9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjciCiAgICAgaW5rc2NhcGU6c25hcC1wZXJwZW5kaWN1bGFyPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRhbmdlbnRpYWw9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtcGF0aC1jbGlwPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXBhdGgtbWFzaz0iZmFsc2UiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNjgiCiAgICAgICB1bml0cz0ibW0iCiAgICAgICBzcGFjaW5neD0iMC4wMzc3OTUyNzYiCiAgICAgICBzcGFjaW5neT0iMC4wMzc3OTUyNzYiCiAgICAgICBkb3R0ZWQ9ImZhbHNlIgogICAgICAgZW1wc3BhY2luZz0iMiIKICAgICAgIHNuYXB2aXNpYmxlZ3JpZGxpbmVzb25seT0iZmFsc2UiCiAgICAgICBlbmFibGVkPSJmYWxzZSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGE3Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlua3NjYXBlOmxhYmVsPSJDYWxxdWUgMSIKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlkPSJsYXllcjEiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCw0NzE2LjkyOTMpIgogICAgIHN0eWxlPSJkaXNwbGF5OmlubGluZSI+CiAgICA8ZwogICAgICAgaWQ9Imc4NzYiCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgxLDAsMCwxLjAzNDQ4MjYsMTQ3Mi4zNjYsMTU2Ljk2ODExKSI+CiAgICAgIDxwYXRoCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlkPSJwYXRoNjMyMiIKICAgICAgICAgZD0ibSAtNTgxLjkyOTg3LC0zMjIyLjU4NyBjIC05LjQ4NjYzLC0xNC41NDYzIC04NDAuNDM2MTMsLTE0MjYuMjEzMSAtODQwLjQzNjEzLC0xNDI3Ljc4MzIgMCwtMC43NTc4IDE5MC41NDYsLTEuMzc4IDQyMy40MzU1NiwtMS4zNzggaCA0MjMuNDM1NTggbCAtMC4xMjQ0NCw3MTkuMzIzNiAtMC4xMjQzNCw3MTkuMzIzMiB6IgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MC45ODgyMDc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6ODExLjk5NTQ4MzQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiCiAgICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iNjg1NS42MDgzIgogICAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjE0OS45ODI0NiIgLz4KICAgICAgPHJlY3QKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiMwNjAwMDA7c3Ryb2tlLXdpZHRoOjQ5LjcxMjEyMDA2O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgaWQ9InJlY3Q4OTEiCiAgICAgICAgIHdpZHRoPSI4NDYuODcxMTUiCiAgICAgICAgIGhlaWdodD0iMTQzOC42NDY3IgogICAgICAgICB4PSItMTQxOS4yMzcyIgogICAgICAgICB5PSItNDY2My4xMDE2IgogICAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9IjczMjkuNjMyNiIKICAgICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSIxNTUuNTM3NjEiIC8+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="6" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{f42533dd-23c7-4b3a-bd48-a8f48c343f67}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="160,214,128,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHdpZHRoPSI5NWNtIgogICBoZWlnaHQ9Ijk1Y20iCiAgIHZpZXdCb3g9IjAgMCA5NTAgOTUwIgogICBpZD0ic3ZnMiIKICAgdmVyc2lvbj0iMS4xIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1wb3RlYXVfYm9pcy5zdmciPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM0Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODcyIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODcwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODc0IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNlNzAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODcyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50OTAyIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wOTAwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODk2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODk0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODg4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODg2IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4MDQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDgwMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzk2IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc4OCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzg0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3ODAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0MTM4MjIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0MTQ4NTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0MTQ4NTMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0MTQ4NDkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0MTQ4NDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxNDgzMSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDMwMzAzO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDE0ODMzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50MTQ4MjUiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3AxNDgyNyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDE0ODE1IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wMTQ4MTciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEzMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTEzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEwNSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTAxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5MyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDg0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwODAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA3NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDcyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50ODg4IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODkwIgogICAgICAgeDE9IjE3MzYuNjM2NCIKICAgICAgIHkxPSItMTUxNy40MDk5IgogICAgICAgeDI9Ijc0MDEuNjI0NSIKICAgICAgIHkyPSItMTUxNy40MDk5IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5Ni05IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJiYXNlIgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxLjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnpvb209IjAuMDgiCiAgICAgaW5rc2NhcGU6Y3g9IjEyOTQuNzU4IgogICAgIGlua3NjYXBlOmN5PSIyMzk2LjgyMjEiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9ImxheWVyMSIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIHNob3dib3JkZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1nbG9iYWw9InRydWUiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxMDI0IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjcwNSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSI3IgogICAgIGlua3NjYXBlOnNuYXAtcGVycGVuZGljdWxhcj0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC10YW5nZW50aWFsPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXBhdGgtY2xpcD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1wYXRoLW1hc2s9ImZhbHNlIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIHNjYWxlLXg9IjE1IgogICAgIGlua3NjYXBlOmxvY2tndWlkZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNjgiCiAgICAgICB1bml0cz0ibW0iCiAgICAgICBzcGFjaW5neD0iMC4wMzc3OTUyNzYiCiAgICAgICBzcGFjaW5neT0iMC4wMzc3OTUyNzYiCiAgICAgICBkb3R0ZWQ9ImZhbHNlIgogICAgICAgZW1wc3BhY2luZz0iMiIKICAgICAgIHNuYXB2aXNpYmxlZ3JpZGxpbmVzb25seT0idHJ1ZSIKICAgICAgIGVuYWJsZWQ9InRydWUiCiAgICAgICBvcmlnaW54PSI2MTYuMjA0NTgiCiAgICAgICBvcmlnaW55PSI2MDMuMzY4MDYiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDk0OS45OTk5OCIKICAgICAgIG9yaWVudGF0aW9uPSIwLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU4OTEiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iOTQ5Ljk5OTk4LDk0OS45OTk5OCIKICAgICAgIG9yaWVudGF0aW9uPSItMC43MDcxMDY3OCwwLjcwNzEwNjc4IgogICAgICAgaWQ9Imd1aWRlODkzIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGE3Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlua3NjYXBlOmxhYmVsPSJDYWxxdWUgMSIKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlkPSJsYXllcjEiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjE2LjIwNDU5LDYzMzcuNzI0MikiCiAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lIj4KICAgIDxjaXJjbGUKICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNzE5O2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAzMDMwMztzdHJva2Utd2lkdGg6NjM5LjAyNjA2MjAxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MCIKICAgICAgIGlkPSJwYXRoODA5OCIKICAgICAgIGN4PSIzMDIwLjg1MTEiCiAgICAgICBjeT0iLTE4NjcuMzU0NyIKICAgICAgIHI9IjAiIC8+CiAgICA8Y2lyY2xlCiAgICAgICBzdHlsZT0ib3BhY2l0eTowLjcxOTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMzAzMDM7c3Ryb2tlLXdpZHRoOjg3LjU5MjE0NzgzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MCIKICAgICAgIGlkPSJwYXRoMTQ4MDMiCiAgICAgICBjeD0iMzAzMi4zNDg2IgogICAgICAgY3k9Ii0xODcxLjM3MjEiCiAgICAgICByPSIwIiAvPgogICAgPHBhdGgKICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNzE5O2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAzMDMwMztzdHJva2Utd2lkdGg6MTQ0Ny42NDMxODg0ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjAiCiAgICAgICBpZD0icGF0aDE0ODExIgogICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgc29kaXBvZGk6Y3g9Ii02NTIxLjgwNTIiCiAgICAgICBzb2RpcG9kaTpjeT0iLTI0NzAuNDkyOSIKICAgICAgIHNvZGlwb2RpOnJ4PSIzNDk1LjUyMzQiCiAgICAgICBzb2RpcG9kaTpyeT0iMzQ0MS4xOTE0IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTplbmQ9IjQuNzE1MTMwNiIKICAgICAgIGQ9Ik0gLTMwMjYuMjgxNywtMjQ3MC40OTI5IEEgMzQ5NS41MjM0LDM0NDEuMTkxNCAwIDAgMSAtNTE4Ni4zNDAxLDcwOS42NTUyNCAzNDk1LjUyMzQsMzQ0MS4xOTE0IDAgMCAxIC04OTk2Ljg5OTQsLTQwLjU0MTAwNCAzNDk1LjUyMzQsMzQ0MS4xOTE0IDAgMCAxIC05NzQ4LjQ5MDQsLTM3OTMuOTE0MyAzNDk1LjUyMzQsMzQ0MS4xOTE0IDAgMCAxIC02NTEyLjIyMTgsLTU5MTEuNjcxNCBsIC05LjU4MzQsMzQ0MS4xNzg1IHoiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9Im9wYWNpdHk6MC43MTk7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDMwMzAzO3N0cm9rZS13aWR0aDo0NDUuNTc4NzA0ODM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46YmV2ZWw7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eTowIgogICAgICAgaWQ9InBhdGgxNDgyMSIKICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgIHNvZGlwb2RpOmN4PSItNjUyMS43NDYxIgogICAgICAgc29kaXBvZGk6Y3k9Ii0yNTM0LjA2MjUiCiAgICAgICBzb2RpcG9kaTpyeD0iMzk5NC4xOTUzIgogICAgICAgc29kaXBvZGk6cnk9IjM5MzEuMTc0OCIKICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgc29kaXBvZGk6ZW5kPSI1LjkxMjA0MSIKICAgICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgICBkPSJtIC0yNTI3LjU1MDgsLTI1MzQuMDYyNSBhIDM5OTQuMTk1MywzOTMxLjE3NDggMCAwIDEgLTM2MjQuMTIxMSwzOTE0LjI2NDcgMzk5NC4xOTUzLDM5MzEuMTc0OCAwIDAgMSAtNDI5NS42OTIxLC0zMTg4LjkyOCAzOTk0LjE5NTMsMzkzMS4xNzQ4IDAgMCAxIDI4MjguMTAzMSwtNDUwNS4xOTIyIDM5OTQuMTk1MywzOTMxLjE3NDggMCAwIDEgNDgxOS43NTcxLDIzNTQuMDg4OSIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxNjYuMzQ5OTkwODQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIGQ9Im0gNTQ5OC42NzE4LC0xNzY3LjY3MjMgLTUzMzMuNjY3NCwtMC4wNDkiCiAgICAgICBpZD0icGF0aDQ3OTQiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3OTYiCiAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDU0OTguNjcxOCwtMTc2Ny42NzIzIC01MzMzLjY2NzQsLTAuMDQ5IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTAuMTU5MzY1MDEsMCwwLC0wLjIwNDA4MTYzLDMxMC4wOTEzMSwtNjIyMy40Nzg2KSIKICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2MiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6ODMuMTgzNTM2NjI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIgogICAgICAgZD0ibSAyODMxLjg1MTksLTQ1NzYuNjg2MiB2IDI4MDUuMDA4NSIKICAgICAgIGlkPSJwYXRoNDc5OCIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDgwMCIKICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMjgzMS44NTE5LC00NTc2LjY4NjIgdiAyODA1LjAwODUiCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjQyOTIyMzIzLDAsMCwwLjMwMzAyOTIyLC0xMzU2LjcwMTIsLTQ5MDAuODQ5MykiCiAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjIiAvPgogICAgPGNpcmNsZQogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTA0NDc3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjMwO3N0cm9rZS1saW5lY2FwOnNxdWFyZTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgaWQ9InBhdGg0NzkzIgogICAgICAgY3g9Ii0xNDEuMjA0NTciCiAgICAgICBjeT0iLTU4NjIuNzE4OCIKICAgICAgIHI9IjQyNSIgLz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="size">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="coalesce(scale_exp(&quot;id_support&quot;, 1e+08, -1e+08, 1, 10, 0.57), 0)"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="7" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{e0be357b-2154-4419-9523-1dfd5656b631}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="86,135,150,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAyODcyLjQ0MSAyODcyLjQ0MSIKICAgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMTAwIDEwMCIKICAgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIKICAgaWQ9InN2ZzgiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXN1cHBvcnRfcHJvamVjdGV1ci5zdmciCiAgIHdpZHRoPSI3NmNtIgogICBoZWlnaHQ9Ijc2Y20iCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiPjxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE0Ij48cmRmOlJERj48Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPjxkYzp0aXRsZT48L2RjOnRpdGxlPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZGVmcwogICAgIGlkPSJkZWZzMTIiPjxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODQ3IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+PHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAyMDBmNztzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4NDUiIC8+PC9saW5lYXJHcmFkaWVudD48bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg1OCIKICAgICAgIG9zYjpwYWludD0ic29saWQiPjxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODU2IiAvPjwvbGluZWFyR3JhZGllbnQ+PGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NTAiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj48c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0OCIgLz48L2xpbmVhckdyYWRpZW50PjxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDUzMiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPjxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmNjAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDUzMCIgLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcxMCIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10ZXh0LWJhc2VsaW5lPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4wNDE3MTkzIgogICAgIGlua3NjYXBlOmN4PSIyMDc0LjczMTkiCiAgICAgaW5rc2NhcGU6Y3k9Ii02NTcuMDg4ODgiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzgiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0iZmFsc2UiPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjAsMCIKICAgICAgIG9yaWVudGF0aW9uPSIwLDI3MjEuMjU5OCIKICAgICAgIGlkPSJndWlkZTgyMCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTQzNi4yMjA0LDE0MzYuMjIwNiIKICAgICAgIG9yaWVudGF0aW9uPSItMjcyMS4yNTk4LDAiCiAgICAgICBpZD0iZ3VpZGU4MjIiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjE0MzYuMjIwNCwxNDM2LjIyMDYiCiAgICAgICBvcmllbnRhdGlvbj0iMCwtMjcyMS4yNTk4IgogICAgICAgaWQ9Imd1aWRlODI0IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDI3MjEuMjYiCiAgICAgICBvcmllbnRhdGlvbj0iMjcyMS4yNTk4LDAiCiAgICAgICBpZD0iZ3VpZGU4MjYiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjI4NzIuNDQxLDI4NzIuNDQxIgogICAgICAgb3JpZW50YXRpb249Ii0wLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU4MjgiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjAsMjg3Mi40NDEiCiAgICAgICBvcmllbnRhdGlvbj0iMC43MDcxMDY3OCwwLjcwNzEwNjc4IgogICAgICAgaWQ9Imd1aWRlODMwIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MjgiIC8+PC9zb2RpcG9kaTpuYW1lZHZpZXc+PHBhdGgKICAgICBzb2RpcG9kaTp0eXBlPSJzdGFyIgogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eTowLjk2NDcwNTg4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojNGQ0ZDRkO3N0cm9rZS13aWR0aDoxMTMuMzg1ODMzNzQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpmaWxsIG1hcmtlcnMgc3Ryb2tlIgogICAgIGlkPSJwYXRoODY4IgogICAgIHNvZGlwb2RpOnNpZGVzPSI4IgogICAgIHNvZGlwb2RpOmN4PSIxNDM2LjIyMDQiCiAgICAgc29kaXBvZGk6Y3k9IjE0MzYuMjIwNCIKICAgICBzb2RpcG9kaTpyMT0iMTQyNy44Mjc4IgogICAgIHNvZGlwb2RpOnIyPSIxMzE5LjE0MTEiCiAgICAgc29kaXBvZGk6YXJnMT0iLTEuOTU2Njc4OSIKICAgICBzb2RpcG9kaTphcmcyPSItMS41NjM5Nzk5IgogICAgIGlua3NjYXBlOmZsYXRzaWRlZD0idHJ1ZSIKICAgICBpbmtzY2FwZTpyb3VuZGVkPSIwIgogICAgIGlua3NjYXBlOnJhbmRvbWl6ZWQ9IjAiCiAgICAgZD0ibSA4OTguODE4OTksMTEzLjM4NTcxIDEwOTIuNzg2NzEsNy40NDkxMSA3NjcuNDQ5NSw3NzcuOTg0MTcgLTcuNDQ5MSwxMDkyLjc4NjcxIC03NzcuOTg0Miw3NjcuNDQ5NSAtMTA5Mi43ODY2NCwtNy40NDkxIC03NjcuNDQ5NTUsLTc3Ny45ODQyIDcuNDQ5MTEsLTEwOTIuNzg2NjQgeiIgLz48L3N2Zz4="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="5"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="8" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{744495b3-3e8b-495e-acce-0d79e8aebc42}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="30,237,44,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHdpZHRoPSIyMGNtIgogICBoZWlnaHQ9IjIwY20iCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDIwMCAyMDAuMDAwMDEiCiAgIGlkPSJzdmcxMiIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9Y2FycmVfZmVyLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSI+CiAgPGRlZnMKICAgICBpZD0iZGVmczE2Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODUyIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODUwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODQ2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODQ0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODQwIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODM4IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODQ1NCIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0NTIiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NDQ4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODQ0NiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDg0NiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg0MiIKICAgICAgIHgxPSIzNjguMDg2OTgiCiAgICAgICB5MT0iLTM3Ny4xNzA3NSIKICAgICAgIHgyPSI2NTAuMTAzNTgiCiAgICAgICB5Mj0iLTM3Ny4xNzA3NSIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09Im1hdHJpeCgwLjgxMDQ1MzI4LC0wLjU4NTgwMzI3LDMuMzA2NDMzMiw0LjU3NDQxOSwtNjUuNTA4MDI2LDI5NzMuNTY2OCkiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDg1MiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg1NCIKICAgICAgIHgxPSIzNjguMDg2OTgiCiAgICAgICB5MT0iLTM3Ny4xNzA3NSIKICAgICAgIHgyPSI2NTAuMTAzNTgiCiAgICAgICB5Mj0iLTM3Ny4xNzA3NSIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09Im1hdHJpeCgwLjgxMDQ1MzI4LC0wLjU4NTgwMzI3LDMuMzA2NDMzMiw0LjU3NDQxOSwtNjUuNTA4MDI2LDI5NzMuNTY2OCkiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzE0IgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iLTgwMCwzNTIwIgogICAgIGlua3NjYXBlOm1lYXN1cmUtZW5kPSIzNzc5LjUzLDM3NzkuNTMiCiAgICAgaW5rc2NhcGU6em9vbT0iMi44Mjg0MjcyIgogICAgIGlua3NjYXBlOmN4PSIxMzQuNTc2MjUiCiAgICAgaW5rc2NhcGU6Y3k9IjEwMC43NzYzOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMTIiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10ZXh0LWJhc2VsaW5lPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10by1ndWlkZXM9InRydWUiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODQ0NCIKICAgICAgIG9yaWdpbng9IjY3LjE5MTcxMyIKICAgICAgIG9yaWdpbnk9IjY3LjE5NDYyNCIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjE5OS45OTcxLDIwMCIKICAgICAgIG9yaWVudGF0aW9uPSItNzU1LjkwNTUxLDAiCiAgICAgICBpZD0iZ3VpZGU4MjciCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iNjcuMTkxNzE1LDY3LjE5NDYyMiIKICAgICAgIG9yaWVudGF0aW9uPSIwLC03NTUuOTA1NTEiCiAgICAgICBpZD0iZ3VpZGU4MjkiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwyMDAiCiAgICAgICBvcmllbnRhdGlvbj0iNzU1LjkwNTUxLDAiCiAgICAgICBpZD0iZ3VpZGU4MzEiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwyMDAiCiAgICAgICBvcmllbnRhdGlvbj0iMC43MDcxMDY3OCwwLjcwNzEwNjc4IgogICAgICAgaWQ9Imd1aWRlODMzIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjY3LjE5MTcxNSw2Ny4xOTQ2MjIiCiAgICAgICBvcmllbnRhdGlvbj0iLTAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTgzNSIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgICA8Y2M6bGljZW5zZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnktbmMtc2EvNC4wLyIgLz4KICAgICAgICA8ZGM6Y3JlYXRvcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPmptYTwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6Y3JlYXRvcj4KICAgICAgICA8ZGM6cHVibGlzaGVyPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+QXppbXV0PC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpwdWJsaXNoZXI+CiAgICAgICAgPGRjOnJpZ2h0cz4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPihjKUF6aW11dDwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cmlnaHRzPgogICAgICA8L2NjOldvcms+CiAgICAgIDxjYzpMaWNlbnNlCiAgICAgICAgIHJkZjphYm91dD0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnktbmMtc2EvNC4wLyI+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1JlcHJvZHVjdGlvbiIgLz4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjRGlzdHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjTm90aWNlIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjQXR0cmlidXRpb24iIC8+CiAgICAgICAgPGNjOnByb2hpYml0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjQ29tbWVyY2lhbFVzZSIgLz4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjRGVyaXZhdGl2ZVdvcmtzIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjU2hhcmVBbGlrZSIgLz4KICAgICAgPC9jYzpMaWNlbnNlPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpZD0iZzg3NyIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMi4xOTMxNTA0LDIwMi4xOTMzKSI+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoODM3IgogICAgICAgZD0iTSAyNy4xOTE3MTIsLTE3Ny4xOTQ2MyBIIDE3Ny4xOTE3MiIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjU7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NTtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxwYXRoCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg4MzkiCiAgICAgICBkPSJNIDI3LjE5MTcxMiwtMTc3LjE5NDYzIFYgLTI3LjE5NDYyNyIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjU7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NTtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxwYXRoCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg4MzktOSIKICAgICAgIGQ9Im0gMTc3LjE5MTcxLC0xNzcuMTk0NjMgdiAxNTAiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDo1O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjU7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoODM5LTEiCiAgICAgICBkPSJNIDI3LjE5MTcxMiwtMjcuMTk0NjIzIEggMTc3LjE5MTcxIgogICAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo1O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIGlkPSJwYXRoODYyIgogICAgICAgdHJhbnNmb3JtPSJzY2FsZSgwLjI2NDU4MzM0KSIKICAgICAgIGQ9Ik0gMTAyLjc3MTQ4LC02NjkuNzEyODkgNjY5LjcwMTE3LC0xMDIuNzgzMiBaIgogICAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTguODk3NjM4MzI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NTtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8cGF0aAogICAgICAgaWQ9InBhdGg4NjYiCiAgICAgICB0cmFuc2Zvcm09InNjYWxlKDAuMjY0NTgzMzQpIgogICAgICAgZD0iTSA2NjkuNzAxMTcsLTY2OS43MTI4OSAxMDIuNzcxNDgsLTEwMi43ODMyIFoiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxOC44OTc2MzgzMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo1O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="9" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{4ca552d3-8d5b-43e8-8fdc-33428f4f1d2d}" enabled="1" locked="0" class="SvgMarker">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="color" value="160,214,128,255"/>
            <Option type="QString" name="fixedAspectRatio" value="0"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="name" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9IjE3NWNtIgogICBoZWlnaHQ9Ijk1Y20iCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDE3NTAgOTUwLjAwMDA1IgogICBpZD0ic3ZnMjIiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXBvdGVhdV9ib2lzX2RvdWJsZS5zdmciCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMyNiI+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODA0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4MDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODA0LTMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDgwMC02IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtNyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzk2LTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDgwMC04IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODk2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODk0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtNCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDgwMC0zIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3OTYtOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODAwLTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc5Ni0wIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxMDI0IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjcwNSIKICAgICBpZD0ibmFtZWR2aWV3MjQiCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjExMzEzNzA4IgogICAgIGlua3NjYXBlOmN4PSI1OTY3Ljk5MzUiCiAgICAgaW5rc2NhcGU6Y3k9Ii04NC43MzczMTUiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzIyIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10by1ndWlkZXM9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4NDk4IiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgICAgPGNjOmxpY2Vuc2UKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iIC8+CiAgICAgICAgPGRjOmNyZWF0b3I+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5qbWE8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOmNyZWF0b3I+CiAgICAgICAgPGRjOnB1Ymxpc2hlcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPkF6aW11dDwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cHVibGlzaGVyPgogICAgICAgIDxkYzpyaWdodHM+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT4oYylBemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnJpZ2h0cz4KICAgICAgPC9jYzpXb3JrPgogICAgICA8Y2M6TGljZW5zZQogICAgICAgICByZGY6YWJvdXQ9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNSZXByb2R1Y3Rpb24iIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rpc3RyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI05vdGljZSIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0F0dHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpwcm9oaWJpdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0NvbW1lcmNpYWxVc2UiIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rlcml2YXRpdmVXb3JrcyIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1NoYXJlQWxpa2UiIC8+CiAgICAgIDwvY2M6TGljZW5zZT4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxnCiAgICAgaWQ9Imc4OTUiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTcuNTQ5MTYzOCwtOS44MzI3MzgyKSI+CiAgICA8cGF0aAogICAgICAgZD0iTSA4MjIuOTA2OTgsNDgxLjc2NjkxIEEgMzY1LjExMDg3LDM2Ni4zMDYwMyAwIDAgMSA1OTcuMjg2NzYsODIwLjI4NTU1IDM2NS4xMTA4NywzNjYuMzA2MDMgMCAwIDEgMTk5LjI3MDA4LDc0MC40MjkwOCAzNjUuMTEwODcsMzY2LjMwNjAzIDAgMCAxIDEyMC43NjU2NiwzNDAuODkyMDggMzY1LjExMDg3LDM2Ni4zMDYwMyAwIDAgMSA0NTguNzk3MTEsMTE1LjQ2MjI1IGwgLTEuMDAxLDM2Ni4zMDQ2NiB6IgogICAgICAgc29kaXBvZGk6ZW5kPSI0LjcxNTEzMDYiCiAgICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICAgIHNvZGlwb2RpOnJ5PSIzNjYuMzA2MDMiCiAgICAgICBzb2RpcG9kaTpyeD0iMzY1LjExMDg3IgogICAgICAgc29kaXBvZGk6Y3k9IjQ4MS43NjY5MSIKICAgICAgIHNvZGlwb2RpOmN4PSI0NTcuNzk2MTEiCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBpZD0icGF0aDE0ODExIgogICAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO29wYWNpdHk6MC43MTk7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDMwMzAzO3N0cm9rZS13aWR0aDoxNTAuNzM4MTg5NztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjAiIC8+CiAgICA8cGF0aAogICAgICAgZD0iTSA4NzUsNDc1LjAwMDAzIEEgNDE3LjE5NzY2LDQxOC40NjM1IDAgMCAxIDQ5Ni40NTY5NSw4OTEuNjYzNDkgNDE3LjE5NzY2LDQxOC40NjM1IDAgMCAxIDQ3Ljc2NzYwNyw1NTIuMjEwMjcgNDE3LjE5NzY2LDQxOC40NjM1IDAgMCAxIDM0My4xNjU4Myw3Mi42NDQwODcgNDE3LjE5NzY2LDQxOC40NjM1IDAgMCAxIDg0Ni41OTQyNCwzMjMuMjMwODIiCiAgICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgICAgc29kaXBvZGk6ZW5kPSI1LjkxMjA0MSIKICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgc29kaXBvZGk6cnk9IjQxOC40NjM1IgogICAgICAgc29kaXBvZGk6cng9IjQxNy4xOTc2NiIKICAgICAgIHNvZGlwb2RpOmN5PSI0NzUuMDAwMDMiCiAgICAgICBzb2RpcG9kaTpjeD0iNDU3LjgwMjM0IgogICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgaWQ9InBhdGgxNDgyMSIKICAgICAgIHN0eWxlPSJkaXNwbGF5OmlubGluZTtvcGFjaXR5OjAuNzE5O2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAzMDMwMztzdHJva2Utd2lkdGg6NDYuMzk2NjA2NDU7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46YmV2ZWw7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eTowIiAvPgogICAgPHBhdGgKICAgICAgIGQ9Ik0gODMwLjU2NDE1LDQ4NC4wMzY4NyBBIDM2NC44MzM3NCwzNTcuNDc4MjEgMCAwIDEgNjA1LjExNTE4LDgxNC4zOTczNSAzNjQuODMzNzQsMzU3LjQ3ODIxIDAgMCAxIDIwNy40MDA2LDczNi40NjUzOSAzNjQuODMzNzQsMzU3LjQ3ODIxIDAgMCAxIDEyOC45NTU3NywzNDYuNTU3MDcgMzY0LjgzMzc0LDM1Ny40NzgyMSAwIDAgMSA0NjYuNzMwNjQsMTI2LjU2IGwgLTEuMDAwMjMsMzU3LjQ3Njg3IHoiCiAgICAgICBzb2RpcG9kaTplbmQ9IjQuNzE1MTMwNiIKICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgc29kaXBvZGk6cnk9IjM1Ny40NzgyMSIKICAgICAgIHNvZGlwb2RpOnJ4PSIzNjQuODMzNzQiCiAgICAgICBzb2RpcG9kaTpjeT0iNDg0LjAzNjg3IgogICAgICAgc29kaXBvZGk6Y3g9IjQ2NS43MzA0MSIKICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgIGlkPSJwYXRoMTQ4MTEtOCIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNzE5O2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAzMDMwMztzdHJva2Utd2lkdGg6MTUwLjczODE4OTc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eTowIiAvPgogICAgPHBhdGgKICAgICAgIGQ9Ik0gODgyLjYxNzU1LDQ3Ny40MzMyIEEgNDE2Ljg4MDk1LDQwOC4zNzg3MiAwIDAgMSA1MDQuMzYxODcsODg0LjA1NTI2IDQxNi44ODA5NSw0MDguMzc4NzIgMCAwIDEgNTYuMDEzMTQ2LDU1Mi43ODI3IDQxNi44ODA5NSw0MDguMzc4NzIgMCAwIDEgMzUxLjE4NzEyLDg0Ljc3Mzg0NyA0MTYuODgwOTUsNDA4LjM3ODcyIDAgMCAxIDg1NC4yMzMzNSwzMjkuMzIxNTYiCiAgICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgICAgc29kaXBvZGk6ZW5kPSI1LjkxMjA0MSIKICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgc29kaXBvZGk6cnk9IjQwOC4zNzg3MiIKICAgICAgIHNvZGlwb2RpOnJ4PSI0MTYuODgwOTUiCiAgICAgICBzb2RpcG9kaTpjeT0iNDc3LjQzMzIiCiAgICAgICBzb2RpcG9kaTpjeD0iNDY1LjczNjYiCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBpZD0icGF0aDE0ODIxLTIiCiAgICAgICBzdHlsZT0ib3BhY2l0eTowLjcxOTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMzAzMDM7c3Ryb2tlLXdpZHRoOjQ2LjM5NjYwNjQ1O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOmJldmVsO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MCIgLz4KICAgIDxwYXRoCiAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjIgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTAuMTU5MzY1MDEsMCwwLC0wLjIwNDA4MTY0LDE3NTguODQ1LDEyNC4wNzI5MikiCiAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDU0OTguNjcxOCwtMTc2Ny42NzIzIC01MzMzLjY2NzQsLTAuMDQ5IgogICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ3OTYtOCIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDQ3OTQiCiAgICAgICBkPSJtIDU0OTguNjcxOCwtMTc2Ny42NzIzIC01MzMzLjY2NzQsLTAuMDQ5IgogICAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO2ZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTY2LjM0OTk5MDg0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICA8cGF0aAogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjYyIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNDI5MjIzMjUsMCwwLDAuMzAzMDI5MjIsOTIuMDUyNDc1LDE0NDYuNzAyMikiCiAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDI4MzEuODUxOSwtNDU3Ni42ODYyIHYgMjgwNS4wMDg1IgogICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ4MDAtMyIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDQ3OTgiCiAgICAgICBkPSJtIDI4MzEuODUxOSwtNDU3Ni42ODYyIHYgMjgwNS4wMDg1IgogICAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO2ZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6ODMuMTgzNTQwMzQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgPGNpcmNsZQogICAgICAgcj0iNDI1LjAwMDAzIgogICAgICAgY3k9IjQ4NC44MzI3MyIKICAgICAgIGN4PSIxMzA3LjU0OTIiCiAgICAgICBpZD0icGF0aDQ3OTMiCiAgICAgICBzdHlsZT0iZGlzcGxheTppbmxpbmU7b3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45MDQ0Nzc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MzAuMDAwMDAxOTE7c3Ryb2tlLWxpbmVjYXA6c3F1YXJlO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICA8cGF0aAogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjYyIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0wLjE1OTM2NTAxLDAsMCwtMC4yMDQwODE2NCw5MDguODQ1MTQsMTI0LjA2MjkyKSIKICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gNTQ5OC42NzE4LC0xNzY3LjY3MjMgLTUzMzMuNjY3NCwtMC4wNDkiCiAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDc5Ni0wIgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoNDc5NC0xIgogICAgICAgZD0ibSA1NDk4LjY3MTgsLTE3NjcuNjcyMyAtNTMzMy42Njc0LC0wLjA0OSIKICAgICAgIHN0eWxlPSJkaXNwbGF5OmlubGluZTtmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjE2Ni4zNDk5OTA4NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2MiCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjQyOTIyMzI1LDAsMCwwLjMwMzAyOTIyLC03NTcuOTQ3NSwxNDQ2LjY5MjMpIgogICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAyODMxLjg1MTksLTQ1NzYuNjg2MiB2IDI4MDUuMDA4NSIKICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0ODAwLTciCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg0Nzk4LTQiCiAgICAgICBkPSJtIDI4MzEuODUxOSwtNDU3Ni42ODYyIHYgMjgwNS4wMDg1IgogICAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO2ZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6ODMuMTgzNTQwMzQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgPGNpcmNsZQogICAgICAgcj0iNDI1LjAwMDAzIgogICAgICAgY3k9IjQ4NC44MjI4NSIKICAgICAgIGN4PSI0NTcuNTQ5MTkiCiAgICAgICBpZD0icGF0aDQ3OTMtMiIKICAgICAgIHN0eWxlPSJkaXNwbGF5OmlubGluZTtvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eTowLjkwNDQ3NzU5O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDozMC4wMDAwMDE5MTtzdHJva2UtbGluZWNhcDpzcXVhcmU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option name="parameters"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="10"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="size">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="coalesce(scale_exp(&quot;id_support&quot;, 1e+08, -1e+08, 1, 10, 0.57), 0)"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{a9fdc403-03b1-4c24-bb8e-6164ff62a8ed}">
      <rule description="étiquettes supports à poser" filter="&quot;etat&quot; = 'A poser'" key="{44681bc8-475a-410a-b0cf-d95feddfa926}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'support à poser : '  ||  &quot;id_support&quot;||  '\n'  || if( &quot;prise_illumination&quot;  = 'oui' ,'(PI)','')" fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="255,0,252,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0.050000000000000003" bufferNoFill="0" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="1" shapeRadiiX="0" shapeRotationType="1" shapeBorderColor="255,0,252,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0.14999999999999999" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0.14999999999999999" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0.14999999999999999" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="255,0,252,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0.15"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="0" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="-2" distUnits="MM" rotationAngle="0" quadOffset="2" centroidInside="0" yOffset="6" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="1" upsidedownLabels="0" scaleMin="1" maxNumLabels="2000" fontMinPixelSize="1" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="2" scaleMax="5000" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{959b8426-4682-438e-bad8-d6130f4ecf65}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{267e02a1-e5b5-454f-88f6-96177eda6fe6}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,0,252,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="étiquettes supports à remplacer" filter="&quot;etat&quot; =  'A remplacer'" key="{4c09af45-e12e-4bc8-88eb-e5ce966d5d57}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'support à remplacer : '  ||  &quot;id_support&quot; " fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="255,201,78,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0.050000000000000003" bufferNoFill="0" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="1" shapeRadiiX="0" shapeRotationType="1" shapeBorderColor="255,201,78,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0.14999999999999999" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0.14999999999999999" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="0.90000000000000002" shapeSizeX="0.14999999999999999" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="255,201,78,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0.15"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="2" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MM" rotationAngle="0" quadOffset="4" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="1" upsidedownLabels="0" scaleMin="1" maxNumLabels="2000" fontMinPixelSize="1" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="2" scaleMax="5000" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="field" value="rot_z"/>
                  <Option type="int" name="type" value="2"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="&quot;cellule&quot;"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{00fbd3ae-df85-409b-8fea-5894faac3476}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{3396c916-0c2e-46c7-a28b-d501599c8a31}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,201,78,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="étiquettes supports à déposer" filter="&quot;etat&quot; =  'A déposer'" key="{669342a2-3571-4eab-b0ad-1a8d91dc6f11}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Normal" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="50" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'support à déposer : '  ||  &quot;id_support&quot; " fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="165,165,165,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="1" bufferNoFill="1" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="0" shapeRadiiX="0" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="190,178,151,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="5" distUnits="MM" rotationAngle="0" quadOffset="0" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="0" maxNumLabels="2000" fontMinPixelSize="3" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="1" scaleMax="0" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{eefd30d0-885e-4803-ade6-8efcb1466854}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{3fe81067-58ff-4573-b40d-6e39511a7065}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;204,204,204,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="étiquettes supports déposer" filter="&quot;etat&quot;= 'Déposé'" key="{546bcbbe-0cac-4584-a821-092d78fbfebd}">
        <settings calloutType="simple">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Normal" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="0" previewBkgrdColor="0,0,0,255" fontWordSpacing="0" fontWeight="50" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName="'support  déposé  : '  ||  &quot;id_support&quot; " fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="165,165,165,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="1" bufferNoFill="1" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="0" shapeRadiiX="0" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="190,178,151,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="15" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="5" distUnits="MM" rotationAngle="0" quadOffset="0" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="0" maxNumLabels="2000" fontMinPixelSize="3" mergeLines="0" obstacleType="0" minFeatureSize="0" obstacle="1" obstacleFactor="1" scaleMax="0" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="bool" name="drawToAllParts" value="false"/>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="lineSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{0009b5ff-915e-4853-8b9d-35fa56901540}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;ArrowLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width&quot; value=&quot;0.03&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_start_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;arrow_width_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length&quot; value=&quot;0.15&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_length_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_thickness_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;head_type&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_curved&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;is_repeated&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{65bff589-5a4b-4b8a-8601-ae3e53b51372}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;204,204,204,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;255,255,255,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.009&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MapUnit&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="double" name="minLength" value="0"/>
              <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="minLengthUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="offsetFromLabel" value="0"/>
              <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="texte supports" filter="&quot;etat&quot; = 'Actif' or ( &quot;terre&quot; is not null or  &quot;photo&quot; is not null or  &quot;prise_illumination&quot; = 'oui' and  &quot;etat&quot;  =  'Actif'  )" key="{d9c35564-2005-4082-9c1a-4264829fda89}">
        <settings calloutType="balloon">
          <text-style fontSize="1.5" useSubstitutions="0" namedStyle="Bold Italic" fontKerning="1" forcedBold="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontItalic="1" previewBkgrdColor="255,255,255,255" fontWordSpacing="0" fontWeight="75" fontSizeMapUnitScale="3x:0,0,0,0,0,0" forcedItalic="0" blendMode="0" capitalization="0" textOpacity="1" fieldName=" &quot;id_support&quot;  || if( &quot;terre&quot; is not null, ' T = ' ||  &quot;terre&quot;  || ' ohms',if( &quot;remarque&quot; is not null,&quot;remarque&quot; ,''))" fontLetterSpacing="0" isExpression="1" fontSizeUnit="MM" fontStrikeout="0" fontUnderline="0" multilineHeight="1" legendString="Aa" textColor="0,0,0,255" allowHtml="0" fontFamily="Arial">
            <families/>
            <text-buffer bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferColor="255,255,255,255" bufferSize="0" bufferNoFill="0" bufferSizeUnits="MM" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskSizeUnits="MM" maskJoinStyle="128" maskEnabled="0" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskedSymbolLayers="" maskOpacity="1"/>
            <background shapeSizeUnit="Point" shapeOffsetUnit="Point" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="Point" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeType="0" shapeSVGFile="" shapeDraw="0" shapeRadiiX="0" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeOffsetY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeBorderWidthUnit="Point" shapeRotation="0" shapeOffsetX="0" shapeRadiiY="0" shapeOpacity="1" shapeSizeX="0" shapeJoinStyle="64">
              <symbol frame_rate="10" is_animated="0" type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleMarker">
                  <Option type="Map">
                    <Option type="QString" name="angle" value="0"/>
                    <Option type="QString" name="cap_style" value="square"/>
                    <Option type="QString" name="color" value="152,125,183,255"/>
                    <Option type="QString" name="horizontal_anchor_point" value="1"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="name" value="circle"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="35,35,35,255"/>
                    <Option type="QString" name="outline_style" value="solid"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="outline_width_unit" value="MM"/>
                    <Option type="QString" name="scale_method" value="diameter"/>
                    <Option type="QString" name="size" value="2"/>
                    <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="size_unit" value="MM"/>
                    <Option type="QString" name="vertical_anchor_point" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol frame_rate="10" is_animated="0" type="fill" clip_to_extent="1" name="fillSymbol" force_rhr="0" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" name="name" value=""/>
                    <Option name="properties"/>
                    <Option type="QString" name="type" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="" enabled="1" locked="0" class="SimpleFill">
                  <Option type="Map">
                    <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="color" value="255,255,255,255"/>
                    <Option type="QString" name="joinstyle" value="bevel"/>
                    <Option type="QString" name="offset" value="0,0"/>
                    <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                    <Option type="QString" name="offset_unit" value="MM"/>
                    <Option type="QString" name="outline_color" value="128,128,128,255"/>
                    <Option type="QString" name="outline_style" value="no"/>
                    <Option type="QString" name="outline_width" value="0"/>
                    <Option type="QString" name="outline_width_unit" value="Point"/>
                    <Option type="QString" name="style" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" name="name" value=""/>
                      <Option name="properties"/>
                      <Option type="QString" name="type" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowDraw="0" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOpacity="1" shadowBlendMode="6" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowUnder="0" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="Point" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="Point"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" multilineAlign="3" decimals="3" autoWrapLength="0" rightDirectionSymbol=">" formatNumbers="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" wrapChar=""/>
          <placement lineAnchorTextPoint="CenterOfText" dist="6" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" lineAnchorPercent="0.5" fitInPolygonOnly="0" placementFlags="10" layerType="PointGeometry" priority="5" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" overrunDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="1" placement="6" repeatDistanceUnits="MM" preserveRotation="1" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorClipping="0" distMapUnitScale="3x:0,0,0,0,0,0" overlapHandling="PreventOverlap" maxCurvedCharAngleIn="25" xOffset="0" distUnits="MM" rotationAngle="0" quadOffset="4" centroidInside="0" yOffset="0" centroidWhole="0" lineAnchorType="0" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry"/>
          <rendering fontLimitPixelSize="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" labelPerPart="0" limitNumLabels="0" scaleVisibility="0" upsidedownLabels="0" scaleMin="0" maxNumLabels="2000" fontMinPixelSize="3" mergeLines="0" obstacleType="1" minFeatureSize="0" obstacle="1" obstacleFactor="1" scaleMax="0" unplacedVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="PositionX">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
                <Option type="Map" name="PositionY">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="int" name="type" value="1"/>
                  <Option type="QString" name="val" value=""/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="balloon">
            <Option type="Map">
              <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
              <Option type="int" name="blendMode" value="0"/>
              <Option type="double" name="cornerRadius" value="0.5"/>
              <Option type="QString" name="cornerRadiusMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="cornerRadiusUnit" value="MM"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" name="name" value=""/>
                <Option type="Map" name="properties">
                  <Option type="Map" name="OriginX">
                    <Option type="bool" name="active" value="false"/>
                    <Option type="int" name="type" value="1"/>
                    <Option type="QString" name="val" value=""/>
                  </Option>
                  <Option type="Map" name="OriginY">
                    <Option type="bool" name="active" value="false"/>
                    <Option type="int" name="type" value="1"/>
                    <Option type="QString" name="val" value=""/>
                  </Option>
                </Option>
                <Option type="QString" name="type" value="collection"/>
              </Option>
              <Option type="QString" name="enabled" value="1"/>
              <Option type="QString" name="fillSymbol" value="&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;fill&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{773924b6-6eef-4fb1-b685-48ea09620456}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleFill&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;border_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;204,204,204,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;0,0,0,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0.06&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;style&quot; value=&quot;solid&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;layer pass=&quot;0&quot; id=&quot;{16af9c1c-8b30-4b20-9030-e14f89d4b10a}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;MarkerLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;average_angle_length&quot; value=&quot;4&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;average_angle_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;average_angle_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;interval&quot; value=&quot;3&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;interval_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;interval_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_along_line&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_along_line_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_along_line_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;bool&quot; name=&quot;place_on_every_part&quot; value=&quot;true&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;placements&quot; value=&quot;FirstVertex&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;rotate&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;marker&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@1&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{939d0289-d5e9-4509-8d37-4faa95aae79e}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SvgMarker&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;angle&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,0,0,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;fixedAspectRatio&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;horizontal_anchor_point&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBoZWlnaHQ9IjY1IgogICB3aWR0aD0iNjUiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXRlcnJlLnN2ZyIKICAgaWQ9InN2ZzI0IgogICB5PSIwcHgiCiAgIHg9IjBweCIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgNjUuMDAwMDAxIDY1LjAwMDAwMSIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMzAiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMyOCIgLz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5MTIiCiAgICAgaW5rc2NhcGU6Y3k9IjM4LjY2Mjc3NCIKICAgICBpbmtzY2FwZTpjeD0iLTQuNjc2MDI4OCIKICAgICBpbmtzY2FwZTp6b29tPSI0LjM4NDA2MiIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10ZXh0LWJhc2VsaW5lPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtZWRnZS1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGlkPSJuYW1lZHZpZXcyNiIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgaWQ9Imd1aWRlNDMiCiAgICAgICBvcmllbnRhdGlvbj0iMC43MDcxMDY3OCwtMC43MDcxMDY3OCIKICAgICAgIHBvc2l0aW9uPSI2NS4wMDAwMDEsNjUuMDAwMDAxIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgaWQ9Imd1aWRlNDciCiAgICAgICBvcmllbnRhdGlvbj0iLTAuNzA3MTA2NzgsLTAuNzA3MTA2NzgiCiAgICAgICBwb3NpdGlvbj0iMCw2NS4wMDAwMDEiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxjaXJjbGUKICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojZmYzMzAwO3N0cm9rZS13aWR0aDo1LjQ4NjtzdHJva2UtbGluZWNhcDpzcXVhcmU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjExLjI7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MC45NjQ3MDYiCiAgICAgY3g9IjMyLjUiCiAgICAgY3k9IjMyLjUiCiAgICAgcj0iMjkuMjU3MTQzIgogICAgIGlkPSJjaXJjbGUyIiAvPgogIDxnCiAgICAgZGlzcGxheT0iaW5saW5lIgogICAgIGlkPSJnMTgiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC41LDAuNTAwMDE4NDIpIgogICAgIHN0eWxlPSJzdHJva2U6I2ZmMzMwMDtzdHJva2Utb3BhY2l0eTowLjk2NDcwNTg4Ij4KICAgIDxnCiAgICAgICBpZD0iZzE2IgogICAgICAgc3R5bGU9InN0cm9rZTojZmYzMzAwO3N0cm9rZS1vcGFjaXR5OjAuOTY0NzA1ODgiPgogICAgICA8cGF0aAogICAgICAgICBkPSJNIDMyLjAwOTA4LDEyLjk4ODY0MSBWIDI5LjMwNzI5IgogICAgICAgICBmaWxsPSJub25lIgogICAgICAgICBzdHJva2U9IiMwMDAwMDAiCiAgICAgICAgIHN0cm9rZS13aWR0aD0iNCIKICAgICAgICAgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIgogICAgICAgICBzdHJva2UtbGluZWpvaW49Im1pdGVyIgogICAgICAgICBzdHJva2UtbWl0ZXJsaW1pdD0iNCIKICAgICAgICAgc3Ryb2tlLWRhc2hhcnJheT0ibm9uZSIKICAgICAgICAgc3Ryb2tlLW9wYWNpdHk9IjEiCiAgICAgICAgIGlkPSJwYXRoNiIKICAgICAgICAgc3R5bGU9InN0cm9rZTojZmYzMzAwO3N0cm9rZS1vcGFjaXR5OjAuOTY0NzA1ODgiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Ik0gMTMuMDQxMTEzLDMxLjQ5MDI5NSBIIDUwLjk5NTc5OSIKICAgICAgICAgZmlsbD0ibm9uZSIKICAgICAgICAgc3Ryb2tlPSIjMDAwMDAwIgogICAgICAgICBzdHJva2Utd2lkdGg9IjQiCiAgICAgICAgIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIKICAgICAgICAgc3Ryb2tlLWxpbmVqb2luPSJtaXRlciIKICAgICAgICAgc3Ryb2tlLW1pdGVybGltaXQ9IjQiCiAgICAgICAgIHN0cm9rZS1kYXNoYXJyYXk9Im5vbmUiCiAgICAgICAgIHN0cm9rZS1vcGFjaXR5PSIxIgogICAgICAgICBpZD0icGF0aDgiCiAgICAgICAgIHN0eWxlPSJzdHJva2U6I2ZmMzMwMDtzdHJva2Utb3BhY2l0eTowLjk2NDcwNTg4IiAvPgogICAgICA8cGF0aAogICAgICAgICBkPSJNIDE4LjAyOTQ5NSwzNy45MDM4MjggSCA0Ni4wMTE2MzQiCiAgICAgICAgIGZpbGw9Im5vbmUiCiAgICAgICAgIHN0cm9rZT0iIzAwMDAwMCIKICAgICAgICAgc3Ryb2tlLXdpZHRoPSI0IgogICAgICAgICBzdHJva2UtbGluZWNhcD0icm91bmQiCiAgICAgICAgIHN0cm9rZS1saW5lam9pbj0ibWl0ZXIiCiAgICAgICAgIHN0cm9rZS1taXRlcmxpbWl0PSI0IgogICAgICAgICBzdHJva2UtZGFzaGFycmF5PSJub25lIgogICAgICAgICBzdHJva2Utb3BhY2l0eT0iMSIKICAgICAgICAgaWQ9InBhdGgxMCIKICAgICAgICAgc3R5bGU9InN0cm9rZTojZmYzMzAwO3N0cm9rZS1vcGFjaXR5OjAuOTY0NzA1ODgiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gMjMuMTIxMzA0LDQ0LjI5NjcwNiBoIDE3Ljc3NDUyIgogICAgICAgICBmaWxsPSJub25lIgogICAgICAgICBzdHJva2U9IiMwMDAwMDAiCiAgICAgICAgIHN0cm9rZS13aWR0aD0iNCIKICAgICAgICAgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIgogICAgICAgICBzdHJva2UtbGluZWpvaW49Im1pdGVyIgogICAgICAgICBzdHJva2UtbWl0ZXJsaW1pdD0iNCIKICAgICAgICAgc3Ryb2tlLWRhc2hhcnJheT0ibm9uZSIKICAgICAgICAgc3Ryb2tlLW9wYWNpdHk9IjEiCiAgICAgICAgIGlkPSJwYXRoMTIiCiAgICAgICAgIHN0eWxlPSJzdHJva2U6I2ZmMzMwMDtzdHJva2Utb3BhY2l0eTowLjk2NDcwNTg4IiAvPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDI4LjM4NDM4Miw1MC42OTcyMSBoIDcuMjM4NzIiCiAgICAgICAgIGZpbGw9Im5vbmUiCiAgICAgICAgIHN0cm9rZT0iIzAwMDAwMCIKICAgICAgICAgc3Ryb2tlLXdpZHRoPSI0IgogICAgICAgICBzdHJva2UtbGluZWNhcD0icm91bmQiCiAgICAgICAgIHN0cm9rZS1saW5lam9pbj0ibWl0ZXIiCiAgICAgICAgIHN0cm9rZS1taXRlcmxpbWl0PSI0IgogICAgICAgICBzdHJva2UtZGFzaGFycmF5PSJub25lIgogICAgICAgICBzdHJva2Utb3BhY2l0eT0iMSIKICAgICAgICAgaWQ9InBhdGgxNCIKICAgICAgICAgc3R5bGU9InN0cm9rZTojZmYzMzAwO3N0cm9rZS1vcGFjaXR5OjAuOTY0NzA1ODgiIC8+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;1.99999999999999889,1.19999999999999996&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;35,35,35,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;parameters&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;scale_method&quot; value=&quot;diameter&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;size&quot; value=&quot;2&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;size_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;size_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;vertical_anchor_point&quot; value=&quot;1&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option type=&quot;Map&quot; name=&quot;properties&quot;>&lt;Option type=&quot;Map&quot; name=&quot;enabled&quot;>&lt;Option type=&quot;bool&quot; name=&quot;active&quot; value=&quot;true&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;expression&quot; value=&quot;&amp;quot;prise_illumination&amp;quot; = 'oui'&quot;/>&lt;Option type=&quot;int&quot; name=&quot;type&quot; value=&quot;3&quot;/>&lt;/Option>&lt;/Option>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;layer pass=&quot;0&quot; id=&quot;{b67585f4-928a-4abf-b070-d536e0c07eb6}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;MarkerLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;average_angle_length&quot; value=&quot;4&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;average_angle_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;average_angle_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;interval&quot; value=&quot;3&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;interval_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;interval_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_along_line&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_along_line_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_along_line_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;Point&quot;/>&lt;Option type=&quot;bool&quot; name=&quot;place_on_every_part&quot; value=&quot;true&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;placements&quot; value=&quot;FirstVertex&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;rotate&quot; value=&quot;0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol frame_rate=&quot;10&quot; is_animated=&quot;0&quot; type=&quot;marker&quot; clip_to_extent=&quot;1&quot; name=&quot;@symbol@2&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; id=&quot;{80ad1576-df21-4c51-840c-cd476a8f97b7}&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SvgMarker&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;angle&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;color&quot; value=&quot;255,0,0,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;fixedAspectRatio&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;horizontal_anchor_point&quot; value=&quot;1&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgdmVyc2lvbj0iMS4xIgogICB4PSIwcHgiCiAgIHk9IjBweCIKICAgdmlld0JveD0iMCAwIDEzMi4yODM0NyAxMzIuMjgzNDciCiAgIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDExMCAxMTAiCiAgIHhtbDpzcGFjZT0icHJlc2VydmUiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjIuMiAoNzMyYTAxZGE2MywgMjAyMi0xMi0wOSkiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXByaXNlX2lsbHVtaW5hdGlvbi5zdmciCiAgIHdpZHRoPSIzLjVjbSIKICAgaGVpZ2h0PSIzLjVjbSIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+PG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMjQiPjxyZGY6UkRGPjxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxkZWZzCiAgICAgaWQ9ImRlZnMyMiIgLz48c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE5MjAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMTAyNyIKICAgICBpZD0ibmFtZWR2aWV3MjAiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjEuNTU5NDAyMiIKICAgICBpbmtzY2FwZTpjeD0iNDcuNzc0NzE4IgogICAgIGlua3NjYXBlOmN5PSIzNC4zMDgwMTkiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5MTIiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0iZzQiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwwIgogICAgICAgb3JpZW50YXRpb249IjAsMTMyLjI4MzQ2IgogICAgICAgaWQ9Imd1aWRlODI1IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIxMzIuMjgzNDcsMCIKICAgICAgIG9yaWVudGF0aW9uPSItMTMyLjI4MzQ2LDAiCiAgICAgICBpZD0iZ3VpZGU4MjciCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjEzMi4yODM0NywxMzIuMjgzNDciCiAgICAgICBvcmllbnRhdGlvbj0iMCwtMTMyLjI4MzQ2IgogICAgICAgaWQ9Imd1aWRlODI5IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDEzMi4yODM0NyIKICAgICAgIG9yaWVudGF0aW9uPSIxMzIuMjgzNDYsMCIKICAgICAgIGlkPSJndWlkZTgzMSIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwwIgogICAgICAgb3JpZW50YXRpb249IjAsMTMyLjI4MzQ2IgogICAgICAgaWQ9Imd1aWRlODMzIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIxMzIuMjgzNDcsMCIKICAgICAgIG9yaWVudGF0aW9uPSItMTMyLjI4MzQ2LDAiCiAgICAgICBpZD0iZ3VpZGU4MzUiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjEzMi4yODM0NywxMzIuMjgzNDciCiAgICAgICBvcmllbnRhdGlvbj0iMCwtMTMyLjI4MzQ2IgogICAgICAgaWQ9Imd1aWRlODM3IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDEzMi4yODM0NyIKICAgICAgIG9yaWVudGF0aW9uPSIxMzIuMjgzNDYsMCIKICAgICAgIGlkPSJndWlkZTgzOSIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwwIgogICAgICAgb3JpZW50YXRpb249IjAsMTMyLjI4MzQ2IgogICAgICAgaWQ9Imd1aWRlODQxIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIxMzIuMjgzNDcsMCIKICAgICAgIG9yaWVudGF0aW9uPSItMTMyLjI4MzQ2LDAiCiAgICAgICBpZD0iZ3VpZGU4NDMiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjEzMi4yODM0NywxMzIuMjgzNDciCiAgICAgICBvcmllbnRhdGlvbj0iMCwtMTMyLjI4MzQ2IgogICAgICAgaWQ9Imd1aWRlODQ1IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDEzMi4yODM0NyIKICAgICAgIG9yaWVudGF0aW9uPSIxMzIuMjgzNDYsMCIKICAgICAgIGlkPSJndWlkZTg0NyIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTMyLjI4MzQ3LDEzMi4yODM0NyIKICAgICAgIG9yaWVudGF0aW9uPSItMC43MDcxMDY3OCwwLjcwNzEwNjc4IgogICAgICAgaWQ9Imd1aWRlODQ5IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDEzMi4yODM0NyIKICAgICAgIG9yaWVudGF0aW9uPSIwLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU4NTEiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjwvc29kaXBvZGk6bmFtZWR2aWV3PjxnCiAgICAgaWQ9Imc0IgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTUuMjE2NTI5NikiPjxjaXJjbGUKICAgICAgIHN0eWxlPSJmaWxsOiM0ZDRkNGQ7c3Ryb2tlLXdpZHRoOjEuMjU5MyIKICAgICAgIGlkPSJjaXJjbGU4IgogICAgICAgcj0iNC4yMzEyMzY5IgogICAgICAgY3k9IjcxLjM1ODI2OSIKICAgICAgIGN4PSI0Ni4zODcxNDYiIC8+PHBhdGgKICAgICAgIHN0eWxlPSJmaWxsOiNiM2IzYjM7c3Ryb2tlLXdpZHRoOjEuMjU5MztzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgaWQ9InBhdGgxMCIKICAgICAgIGQ9Ik0gMTA2LjY5ODY1LDI1LjQyMTYzOCBIIDI1LjU4MzU2MyBjIC0yLjk3MDY4MSwwIC01LjM3OTcxNiwyLjQwOTAzNSAtNS4zNzk3MTYsNS4zNzg0NTcgdiA4MS4xMjAxMjUgYyAwLDIuOTY4MTYgMi40MDkwMzUsNS4zNzQ2OCA1LjM3OTcxNiw1LjM3NDY4IGggODEuMTE1MDg3IGMgMi45NzMyLDAgNS4zODA5NywtMi40MDUyNiA1LjM4MDk3LC01LjM3NDY4IFYgMzAuODAwMDk1IGMgMTBlLTQsLTIuOTY5NDIyIC0yLjQwNzc3LC01LjM3ODQ1NyAtNS4zODA5NywtNS4zNzg0NTcgeiBNIDY2LjE0Mjk5NCwxMDEuODM0NTEgYyAtMTYuODM0MjgsMCAtMzAuNDc0OTgyLC0xMy42NDQ0ODIgLTMwLjQ3NDk4MiwtMzAuNDc0OTg0IDAsLTE2LjgzMDUwMiAxMy42NDA3MDIsLTMwLjQ3NzUwMSAzMC40NzQ5ODIsLTMwLjQ3NzUwMSAxNi44MjkyNDMsMCAzMC40NzQ5ODMsMTMuNjQ2OTk5IDMwLjQ3NDk4MywzMC40Nzc1MDEgMCwxNi44MzA1MDIgLTEzLjY0NTc0LDMwLjQ3NDk4NCAtMzAuNDc0OTgzLDMwLjQ3NDk4NCB6IgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz48cGF0aAogICAgICAgc3R5bGU9ImZpbGw6IzRkNGQ0ZDtzdHJva2Utd2lkdGg6MS4yNTkzIgogICAgICAgaWQ9InBhdGgxMiIKICAgICAgIGQ9Im0gODEuNjYwMDQ5LDcxLjM1OTUyNiBjIDAsMi4zMzcyNTUgMS44OTM5ODMsNC4yMjk5NzggNC4yMzUwMTUsNC4yMjk5NzggMi4zMzQ3MzcsMCA0LjIyODcxOSwtMS44OTI3MjMgNC4yMjg3MTksLTQuMjI5OTc4IDAsLTIuMzM3MjU1IC0xLjg5Mzk4MiwtNC4yMzI0OTcgLTQuMjI4NzE5LC00LjIzMjQ5NyAtMi4zNDEwMzIsMCAtNC4yMzUwMTUsMS44OTUyNDIgLTQuMjM1MDE1LDQuMjMyNDk3IHoiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPjxwYXRoCiAgICAgICBzdHlsZT0iZmlsbDojYjNiM2IzO3N0cm9rZS13aWR0aDoxLjI1OTM7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLW9wYWNpdHk6MTtmaWxsLW9wYWNpdHk6MSIKICAgICAgIGlkPSJwYXRoMTQiCiAgICAgICBkPSJNIDExNC43NjY5NiwxMjguMDUzMDcgSCAxNy41MTUyNDggYyAtNC40NDc4MzYsMCAtOC4wNjU3OTU4LC0zLjYyMDQ4IC04LjA2NTc5NTgsLTguMDY4MzIgViAyMi43MzMwMzkgYyAwLC00LjQ0OTA5NSAzLjYxNzk1OTgsLTguMDY1Nzk2IDguMDY1Nzk1OCwtOC4wNjU3OTYgaCA5Ny4yNTE3MTIgYyA0LjQ0Nzg0LDAgOC4wNjgzMiwzLjYxNzk2IDguMDY4MzIsOC4wNjU3OTYgdiA5Ny4yNTA0NTEgYyAwLjAwMSw0LjQ0OTEgLTMuNjE5MjIsOC4wNjk1OCAtOC4wNjgzMiw4LjA2OTU4IHogTSAxNy41MTUyNDgsMjAuMDQ0NDQxIGMgLTEuNDg0NzExLDAgLTIuNjg4NTk4LDEuMjA1MTQ3IC0yLjY4ODU5OCwyLjY4ODU5OCB2IDk3LjI1MDQ1MSBjIDAsMS40ODQ3MSAxLjIwMzg4NywyLjY5MTEyIDIuNjg4NTk4LDIuNjkxMTIgaCA5Ny4yNTE3MTIgYyAxLjQ4NDcxLDAgMi42OTExMiwtMS4yMDY0MSAyLjY5MTEyLC0yLjY5MTEyIFYgMjIuNzMzMDM5IGMgMCwtMS40ODQ3MTEgLTEuMjA2NDEsLTIuNjg4NTk4IC0yLjY5MTEyLC0yLjY4ODU5OCB6IgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz48L2c+PC9zdmc+Cg==&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;-0.19999999999999973,1.19999999999999996&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_color&quot; value=&quot;35,35,35,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;outline_width_unit&quot; value=&quot;Point&quot;/>&lt;Option name=&quot;parameters&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;scale_method&quot; value=&quot;diameter&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;size&quot; value=&quot;2&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;size_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;size_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;vertical_anchor_point&quot; value=&quot;1&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option type=&quot;Map&quot; name=&quot;properties&quot;>&lt;Option type=&quot;Map&quot; name=&quot;enabled&quot;>&lt;Option type=&quot;bool&quot; name=&quot;active&quot; value=&quot;true&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;expression&quot; value=&quot;&amp;quot;terre&amp;quot; IS NOT NULL&quot;/>&lt;Option type=&quot;int&quot; name=&quot;type&quot; value=&quot;3&quot;/>&lt;/Option>&lt;/Option>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>"/>
              <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
              <Option type="QString" name="margins" value="0.5,0.5,0.5,0.5"/>
              <Option type="QString" name="marginsUnit" value="MM"/>
              <Option type="double" name="offsetFromAnchor" value="0"/>
              <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
              <Option type="double" name="wedgeWidth" value="1"/>
              <Option type="QString" name="wedgeWidthMapUnitScale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="wedgeWidthUnit" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="QString" name="QFieldSync/action" value="remove"/>
      <Option type="QString" name="QFieldSync/cloud_action" value="offline"/>
      <Option type="QString" name="QFieldSync/photo_naming" value="{&quot;photo&quot;: &quot;'DCIM/supports_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;}"/>
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="COALESCE( &quot;id_support&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_support&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_support&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_support&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_support&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_support&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_support&quot;, '&lt;NULL>' )"/>
      </Option>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option type="QString" name="geopdf/includeFeatures" value="false"/>
      <Option type="QString" name="labeling/addDirectionSymbol" value="false"/>
      <Option type="QString" name="labeling/angleOffset" value="0"/>
      <Option type="QString" name="labeling/blendMode" value="0"/>
      <Option type="QString" name="labeling/bufferBlendMode" value="0"/>
      <Option type="QString" name="labeling/bufferColorA" value="255"/>
      <Option type="QString" name="labeling/bufferColorB" value="255"/>
      <Option type="QString" name="labeling/bufferColorG" value="255"/>
      <Option type="QString" name="labeling/bufferColorR" value="255"/>
      <Option type="QString" name="labeling/bufferDraw" value="false"/>
      <Option type="QString" name="labeling/bufferJoinStyle" value="128"/>
      <Option type="QString" name="labeling/bufferNoFill" value="false"/>
      <Option type="QString" name="labeling/bufferSize" value="1"/>
      <Option type="QString" name="labeling/bufferSizeInMapUnits" value="false"/>
      <Option type="QString" name="labeling/bufferSizeMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/bufferTransp" value="0"/>
      <Option type="QString" name="labeling/centroidInside" value="false"/>
      <Option type="QString" name="labeling/centroidWhole" value="false"/>
      <Option type="QString" name="labeling/decimals" value="3"/>
      <Option type="QString" name="labeling/displayAll" value="false"/>
      <Option type="QString" name="labeling/dist" value="0.5"/>
      <Option type="QString" name="labeling/distInMapUnits" value="false"/>
      <Option type="QString" name="labeling/distMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/drawLabels" value="true"/>
      <Option type="QString" name="labeling/enabled" value="true"/>
      <Option type="QString" name="labeling/fieldName" value="id_support"/>
      <Option type="QString" name="labeling/fitInPolygonOnly" value="false"/>
      <Option type="QString" name="labeling/fontCapitals" value="0"/>
      <Option type="QString" name="labeling/fontFamily" value="Tahoma"/>
      <Option type="QString" name="labeling/fontItalic" value="false"/>
      <Option type="QString" name="labeling/fontLetterSpacing" value="0"/>
      <Option type="QString" name="labeling/fontLimitPixelSize" value="false"/>
      <Option type="QString" name="labeling/fontMaxPixelSize" value="10000"/>
      <Option type="QString" name="labeling/fontMinPixelSize" value="3"/>
      <Option type="QString" name="labeling/fontSize" value="1"/>
      <Option type="QString" name="labeling/fontSizeInMapUnits" value="true"/>
      <Option type="QString" name="labeling/fontSizeMapUnitScale" value="0.000001,0.005,0,0,0,0"/>
      <Option type="QString" name="labeling/fontStrikeout" value="false"/>
      <Option type="QString" name="labeling/fontUnderline" value="false"/>
      <Option type="QString" name="labeling/fontWeight" value="75"/>
      <Option type="QString" name="labeling/fontWordSpacing" value="0"/>
      <Option type="QString" name="labeling/formatNumbers" value="false"/>
      <Option type="QString" name="labeling/isExpression" value="false"/>
      <Option type="QString" name="labeling/labelOffsetInMapUnits" value="true"/>
      <Option type="QString" name="labeling/labelOffsetMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/labelPerPart" value="false"/>
      <Option type="QString" name="labeling/leftDirectionSymbol" value="&lt;"/>
      <Option type="QString" name="labeling/limitNumLabels" value="false"/>
      <Option type="QString" name="labeling/maxCurvedCharAngleIn" value="25"/>
      <Option type="QString" name="labeling/maxCurvedCharAngleOut" value="-25"/>
      <Option type="QString" name="labeling/maxNumLabels" value="2000"/>
      <Option type="QString" name="labeling/mergeLines" value="false"/>
      <Option type="QString" name="labeling/minFeatureSize" value="0"/>
      <Option type="QString" name="labeling/multilineAlign" value="3"/>
      <Option type="QString" name="labeling/multilineHeight" value="1"/>
      <Option type="QString" name="labeling/namedStyle" value="Bold"/>
      <Option type="QString" name="labeling/obstacle" value="true"/>
      <Option type="QString" name="labeling/obstacleFactor" value="1"/>
      <Option type="QString" name="labeling/obstacleType" value="0"/>
      <Option type="QString" name="labeling/offsetType" value="1"/>
      <Option type="QString" name="labeling/placeDirectionSymbol" value="0"/>
      <Option type="QString" name="labeling/placement" value="6"/>
      <Option type="QString" name="labeling/placementFlags" value="10"/>
      <Option type="QString" name="labeling/plussign" value="false"/>
      <Option type="QString" name="labeling/predefinedPositionOrder" value="TR,TL,BR,BL,R,L,TSR,BSR"/>
      <Option type="QString" name="labeling/preserveRotation" value="true"/>
      <Option type="QString" name="labeling/previewBkgrdColor" value="#ffffff"/>
      <Option type="QString" name="labeling/priority" value="5"/>
      <Option type="QString" name="labeling/quadOffset" value="4"/>
      <Option type="QString" name="labeling/repeatDistance" value="0"/>
      <Option type="QString" name="labeling/repeatDistanceMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/repeatDistanceUnit" value="1"/>
      <Option type="QString" name="labeling/reverseDirectionSymbol" value="false"/>
      <Option type="QString" name="labeling/rightDirectionSymbol" value=">"/>
      <Option type="QString" name="labeling/scaleMax" value="10000000"/>
      <Option type="QString" name="labeling/scaleMin" value="1"/>
      <Option type="QString" name="labeling/scaleVisibility" value="false"/>
      <Option type="QString" name="labeling/shadowBlendMode" value="6"/>
      <Option type="QString" name="labeling/shadowColorB" value="0"/>
      <Option type="QString" name="labeling/shadowColorG" value="0"/>
      <Option type="QString" name="labeling/shadowColorR" value="0"/>
      <Option type="QString" name="labeling/shadowDraw" value="false"/>
      <Option type="QString" name="labeling/shadowOffsetAngle" value="135"/>
      <Option type="QString" name="labeling/shadowOffsetDist" value="1"/>
      <Option type="QString" name="labeling/shadowOffsetGlobal" value="true"/>
      <Option type="QString" name="labeling/shadowOffsetMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shadowOffsetUnits" value="1"/>
      <Option type="QString" name="labeling/shadowRadius" value="1.5"/>
      <Option type="QString" name="labeling/shadowRadiusAlphaOnly" value="false"/>
      <Option type="QString" name="labeling/shadowRadiusMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shadowRadiusUnits" value="1"/>
      <Option type="QString" name="labeling/shadowScale" value="100"/>
      <Option type="QString" name="labeling/shadowTransparency" value="30"/>
      <Option type="QString" name="labeling/shadowUnder" value="0"/>
      <Option type="QString" name="labeling/shapeBlendMode" value="0"/>
      <Option type="QString" name="labeling/shapeBorderColorA" value="255"/>
      <Option type="QString" name="labeling/shapeBorderColorB" value="128"/>
      <Option type="QString" name="labeling/shapeBorderColorG" value="128"/>
      <Option type="QString" name="labeling/shapeBorderColorR" value="128"/>
      <Option type="QString" name="labeling/shapeBorderWidth" value="0"/>
      <Option type="QString" name="labeling/shapeBorderWidthMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeBorderWidthUnits" value="1"/>
      <Option type="QString" name="labeling/shapeDraw" value="false"/>
      <Option type="QString" name="labeling/shapeFillColorA" value="255"/>
      <Option type="QString" name="labeling/shapeFillColorB" value="255"/>
      <Option type="QString" name="labeling/shapeFillColorG" value="255"/>
      <Option type="QString" name="labeling/shapeFillColorR" value="255"/>
      <Option type="QString" name="labeling/shapeJoinStyle" value="64"/>
      <Option type="QString" name="labeling/shapeOffsetMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeOffsetUnits" value="1"/>
      <Option type="QString" name="labeling/shapeOffsetX" value="0"/>
      <Option type="QString" name="labeling/shapeOffsetY" value="0"/>
      <Option type="QString" name="labeling/shapeRadiiMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeRadiiUnits" value="1"/>
      <Option type="QString" name="labeling/shapeRadiiX" value="0"/>
      <Option type="QString" name="labeling/shapeRadiiY" value="0"/>
      <Option type="QString" name="labeling/shapeRotation" value="0"/>
      <Option type="QString" name="labeling/shapeRotationType" value="0"/>
      <Option type="QString" name="labeling/shapeSVGFile" value=""/>
      <Option type="QString" name="labeling/shapeSizeMapUnitScale" value="0,0,0,0,0,0"/>
      <Option type="QString" name="labeling/shapeSizeType" value="0"/>
      <Option type="QString" name="labeling/shapeSizeUnits" value="1"/>
      <Option type="QString" name="labeling/shapeSizeX" value="0"/>
      <Option type="QString" name="labeling/shapeSizeY" value="0"/>
      <Option type="QString" name="labeling/shapeTransparency" value="0"/>
      <Option type="QString" name="labeling/shapeType" value="0"/>
      <Option type="QString" name="labeling/substitutions" value="&lt;substitutions/>"/>
      <Option type="QString" name="labeling/textColorA" value="255"/>
      <Option type="QString" name="labeling/textColorB" value="0"/>
      <Option type="QString" name="labeling/textColorG" value="0"/>
      <Option type="QString" name="labeling/textColorR" value="0"/>
      <Option type="QString" name="labeling/textTransp" value="0"/>
      <Option type="QString" name="labeling/upsidedownLabels" value="0"/>
      <Option type="QString" name="labeling/useSubstitutions" value="false"/>
      <Option type="QString" name="labeling/wrapChar" value=""/>
      <Option type="QString" name="labeling/xOffset" value="0"/>
      <Option type="QString" name="labeling/yOffset" value="0"/>
      <Option type="QString" name="labeling/zIndex" value="0"/>
      <Option type="QString" name="qgis2web/Interactive" value="true"/>
      <Option type="QString" name="qgis2web/Visible" value="true"/>
      <Option type="QString" name="qgis2web/popup/acteur" value="no label"/>
      <Option type="QString" name="qgis2web/popup/boitier" value="no label"/>
      <Option type="QString" name="qgis2web/popup/calibre_diff" value="no label"/>
      <Option type="QString" name="qgis2web/popup/cellule" value="no label"/>
      <Option type="QString" name="qgis2web/popup/created_at" value="no label"/>
      <Option type="QString" name="qgis2web/popup/date_action" value="no label"/>
      <Option type="QString" name="qgis2web/popup/date_depose" value="no label"/>
      <Option type="QString" name="qgis2web/popup/date_pose" value="no label"/>
      <Option type="QString" name="qgis2web/popup/desc_action" value="no label"/>
      <Option type="QString" name="qgis2web/popup/ecp_intervention_id" value="no label"/>
      <Option type="QString" name="qgis2web/popup/entraxe_massif" value="no label"/>
      <Option type="QString" name="qgis2web/popup/etat" value="no label"/>
      <Option type="QString" name="qgis2web/popup/forme" value="no label"/>
      <Option type="QString" name="qgis2web/popup/hauteur_totale" value="no label"/>
      <Option type="QString" name="qgis2web/popup/id_support" value="no label"/>
      <Option type="QString" name="qgis2web/popup/insee" value="no label"/>
      <Option type="QString" name="qgis2web/popup/intervention" value="no label"/>
      <Option type="QString" name="qgis2web/popup/mapid" value="no label"/>
      <Option type="QString" name="qgis2web/popup/marque" value="no label"/>
      <Option type="QString" name="qgis2web/popup/matiere" value="no label"/>
      <Option type="QString" name="qgis2web/popup/mslink" value="no label"/>
      <Option type="QString" name="qgis2web/popup/nature" value="no label"/>
      <Option type="QString" name="qgis2web/popup/nom_voie" value="no label"/>
      <Option type="QString" name="qgis2web/popup/numero" value="no label"/>
      <Option type="QString" name="qgis2web/popup/numero_voie" value="no label"/>
      <Option type="QString" name="qgis2web/popup/photo" value="no label"/>
      <Option type="QString" name="qgis2web/popup/pkid" value="no label"/>
      <Option type="QString" name="qgis2web/popup/prise_illumination" value="no label"/>
      <Option type="QString" name="qgis2web/popup/ral_crosse" value="no label"/>
      <Option type="QString" name="qgis2web/popup/ral_illum" value="no label"/>
      <Option type="QString" name="qgis2web/popup/ral_mat" value="no label"/>
      <Option type="QString" name="qgis2web/popup/remarque" value="no label"/>
      <Option type="QString" name="qgis2web/popup/rot_z" value="no label"/>
      <Option type="QString" name="qgis2web/popup/sensibilite_diff" value="no label"/>
      <Option type="QString" name="qgis2web/popup/terre" value="no label"/>
      <Option type="QString" name="qgis2web/popup/type_crosse" value="no label"/>
      <Option type="QString" name="qgis2web/popup/type_massif" value="no label"/>
      <Option type="QString" name="qgis2web/popup/type_protection" value="no label"/>
      <Option type="QString" name="qgis2web/popup/type_voie" value="no label"/>
      <Option type="QString" name="qgis2web/popup/uid4" value="no label"/>
      <Option type="QString" name="qgis2web/popup/updated_at" value="no label"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory minScaleDenominator="1" backgroundAlpha="255" enabled="0" maxScaleDenominator="1e+08" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" diagramOrientation="Up" penWidth="0" direction="1" spacingUnit="MM" scaleBasedVisibility="0" scaleDependency="Area" height="15" spacingUnitScale="3x:0,0,0,0,0,0" sizeType="MM" rotationOffset="270" penColor="#000000" opacity="1" labelPlacementMethod="XHeight" barWidth="5" backgroundColor="#ffffff" lineSizeScale="3x:0,0,0,0,0,0" penAlpha="255" width="15" showAxis="0" spacing="0">
      <fontProperties underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" italic="0"/>
      <attribute label="" color="#000000" colorOpacity="1" field=""/>
      <axisSymbol>
        <symbol frame_rate="10" is_animated="0" type="line" clip_to_extent="1" name="" force_rhr="0" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" id="{9aff26b0-f6f0-4315-bd77-be5128e953d5}" enabled="1" locked="0" class="SimpleLine">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" dist="0" linePlacementFlags="18" showAll="1" priority="0" placement="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_support">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nom_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="Actif" value="Actif"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Solaire" value="Solaire"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nature">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="balisage" value="balisage"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="béton" value="béton"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="bois" value="bois"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="borne" value="borne"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="carré fer" value="carré fer"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="façade" value="façade"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="mât" value="mât"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="projecteur" value="projecteur"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="top" value="top"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="solaire" value="solaire"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="&lt;NULL>" value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="terre">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="boitier">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value="oui"/>
            <Option type="int" name="TextDisplayMethod" value="1"/>
            <Option type="QString" name="UncheckedState" value="non"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type_protection">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="&lt;NULL>" value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Disjoncteur" value="Disjoncteur"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Disjoncteur Différentiel" value="Disjoncteur Différentiel"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Fusible" value="Fusible"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="interdifférentiel et fusible" value="interdifférentiel et fusible"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sensibilite_diff">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="calibre_diff">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="forme">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="&lt;NULL>" value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Autre" value="Autre"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Cylindrique" value="Cylindrique"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Cylindro-conique" value="Cylindro-conique"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Octogonal" value="Octogonal"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="marque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="matiere">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="hauteur_totale">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="entraxe_massif">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="&lt;NULL>" value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="200 x 200" value="200 x 200"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="300 x 300" value="300 x 300"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="320 x 320" value="320 x 320"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type_massif">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="&lt;NULL>" value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Coulé" value="Coulé"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Préfabriqué" value="Préfabriqué"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ral_mat">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type_crosse">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" name="&lt;NULL>" value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Double" value="Double"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Quadruple" value="Quadruple"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Simple" value="Simple"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Top" value="Top"/>
              </Option>
              <Option type="Map">
                <Option type="QString" name="Triple" value="Triple"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ral_crosse">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="prise_illumination">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value="oui"/>
            <Option type="int" name="TextDisplayMethod" value="1"/>
            <Option type="QString" name="UncheckedState" value="non"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ral_illum">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cellule">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_pose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="dd/MM/yyyy"/>
            <Option type="QString" name="field_format" value="yyyy-MM-dd"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_depose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="dd-MM-yyyy"/>
            <Option type="QString" name="field_format" value="yyyy-MM-dd"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_action">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="desc_action">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="remarque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="true"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mslink">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mapid">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ecp_intervention_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acteur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rot_z">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="uid4">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="created_at">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="updated_at">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" name="IsMultiline" value="0"/>
            <Option type="QString" name="UseHtml" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pkid">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intervention">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photo">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option type="QString" name="DefaultRoot" value="P:/SIG/documents/rq_particulieres"/>
            <Option type="int" name="DocumentViewer" value="1"/>
            <Option type="int" name="DocumentViewerHeight" value="0"/>
            <Option type="int" name="DocumentViewerWidth" value="0"/>
            <Option type="bool" name="FileWidget" value="false"/>
            <Option type="bool" name="FileWidgetButton" value="false"/>
            <Option type="QString" name="FileWidgetFilter" value=""/>
            <Option type="Map" name="PropertyCollection">
              <Option type="QString" name="name" value=""/>
              <Option type="invalid" name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
            <Option type="int" name="RelativeStorage" value="1"/>
            <Option type="int" name="StorageMode" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="fid"/>
    <alias index="1" name="" field="numero"/>
    <alias index="2" name="numéro insee commune" field="insee"/>
    <alias index="3" name="identifiant" field="id_support"/>
    <alias index="4" name="numéro rue" field="numero_voie"/>
    <alias index="5" name="type (rue, route, chemin.......)" field="type_voie"/>
    <alias index="6" name="nom de la voie" field="nom_voie"/>
    <alias index="7" name="état" field="etat"/>
    <alias index="8" name="nature du support ou du candélabre (bois, béton, borne, projecteur....)" field="nature"/>
    <alias index="9" name="" field="terre"/>
    <alias index="10" name="présence boitier de raccordement" field="boitier"/>
    <alias index="11" name="type de protection du luminaire (fusible, disjoncteur, disjoncteur différentiel.....)" field="type_protection"/>
    <alias index="12" name="sensibilité du différentiel" field="sensibilite_diff"/>
    <alias index="13" name="calibre du disjoncteur (différentiel ou pas ), fusible de la protection du foyer " field="calibre_diff"/>
    <alias index="14" name="" field="forme"/>
    <alias index="15" name="" field="marque"/>
    <alias index="16" name="" field="matiere"/>
    <alias index="17" name="hauteur totale du mât" field="hauteur_totale"/>
    <alias index="18" name="entraxe du massif" field="entraxe_massif"/>
    <alias index="19" name="type de massif" field="type_massif"/>
    <alias index="20" name="teinte du mât du candélabre (RAL)" field="ral_mat"/>
    <alias index="21" name="type de crosse" field="type_crosse"/>
    <alias index="22" name="teinte de la crosse (RAL)" field="ral_crosse"/>
    <alias index="23" name="présence d'une prise illumination" field="prise_illumination"/>
    <alias index="24" name="teinte de la prise illumination (RAL)" field="ral_illum"/>
    <alias index="25" name="" field="cellule"/>
    <alias index="26" name="date de pose" field="date_pose"/>
    <alias index="27" name="date de dépose" field="date_depose"/>
    <alias index="28" name="" field="date_action"/>
    <alias index="29" name="" field="desc_action"/>
    <alias index="30" name="" field="remarque"/>
    <alias index="31" name="" field="mslink"/>
    <alias index="32" name="" field="mapid"/>
    <alias index="33" name="identifiant interne intervention" field="ecp_intervention_id"/>
    <alias index="34" name="chargés d'affaires" field="acteur"/>
    <alias index="35" name="" field="rot_z"/>
    <alias index="36" name="" field="uid4"/>
    <alias index="37" name="" field="created_at"/>
    <alias index="38" name="" field="updated_at"/>
    <alias index="39" name="" field="pkid"/>
    <alias index="40" name="numéro d'affaires" field="intervention"/>
    <alias index="41" name="point particulier" field="photo"/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="fid"/>
    <policy policy="Duplicate" field="numero"/>
    <policy policy="Duplicate" field="insee"/>
    <policy policy="Duplicate" field="id_support"/>
    <policy policy="Duplicate" field="numero_voie"/>
    <policy policy="Duplicate" field="type_voie"/>
    <policy policy="Duplicate" field="nom_voie"/>
    <policy policy="Duplicate" field="etat"/>
    <policy policy="Duplicate" field="nature"/>
    <policy policy="Duplicate" field="terre"/>
    <policy policy="Duplicate" field="boitier"/>
    <policy policy="Duplicate" field="type_protection"/>
    <policy policy="Duplicate" field="sensibilite_diff"/>
    <policy policy="Duplicate" field="calibre_diff"/>
    <policy policy="Duplicate" field="forme"/>
    <policy policy="Duplicate" field="marque"/>
    <policy policy="Duplicate" field="matiere"/>
    <policy policy="Duplicate" field="hauteur_totale"/>
    <policy policy="Duplicate" field="entraxe_massif"/>
    <policy policy="Duplicate" field="type_massif"/>
    <policy policy="Duplicate" field="ral_mat"/>
    <policy policy="Duplicate" field="type_crosse"/>
    <policy policy="Duplicate" field="ral_crosse"/>
    <policy policy="Duplicate" field="prise_illumination"/>
    <policy policy="Duplicate" field="ral_illum"/>
    <policy policy="Duplicate" field="cellule"/>
    <policy policy="Duplicate" field="date_pose"/>
    <policy policy="Duplicate" field="date_depose"/>
    <policy policy="Duplicate" field="date_action"/>
    <policy policy="Duplicate" field="desc_action"/>
    <policy policy="Duplicate" field="remarque"/>
    <policy policy="Duplicate" field="mslink"/>
    <policy policy="Duplicate" field="mapid"/>
    <policy policy="Duplicate" field="ecp_intervention_id"/>
    <policy policy="Duplicate" field="acteur"/>
    <policy policy="Duplicate" field="rot_z"/>
    <policy policy="Duplicate" field="uid4"/>
    <policy policy="Duplicate" field="created_at"/>
    <policy policy="Duplicate" field="updated_at"/>
    <policy policy="Duplicate" field="pkid"/>
    <policy policy="Duplicate" field="intervention"/>
    <policy policy="Duplicate" field="photo"/>
  </splitPolicies>
  <defaults>
    <default expression="" applyOnUpdate="0" field="fid"/>
    <default expression="" applyOnUpdate="0" field="numero"/>
    <default expression=" @lumigis_insee " applyOnUpdate="1" field="insee"/>
    <default expression="" applyOnUpdate="0" field="id_support"/>
    <default expression="" applyOnUpdate="0" field="numero_voie"/>
    <default expression="" applyOnUpdate="0" field="type_voie"/>
    <default expression="" applyOnUpdate="0" field="nom_voie"/>
    <default expression="" applyOnUpdate="0" field="etat"/>
    <default expression="" applyOnUpdate="0" field="nature"/>
    <default expression="" applyOnUpdate="0" field="terre"/>
    <default expression="" applyOnUpdate="0" field="boitier"/>
    <default expression="" applyOnUpdate="0" field="type_protection"/>
    <default expression="" applyOnUpdate="0" field="sensibilite_diff"/>
    <default expression="" applyOnUpdate="0" field="calibre_diff"/>
    <default expression="" applyOnUpdate="0" field="forme"/>
    <default expression="" applyOnUpdate="0" field="marque"/>
    <default expression="" applyOnUpdate="0" field="matiere"/>
    <default expression="" applyOnUpdate="0" field="hauteur_totale"/>
    <default expression="" applyOnUpdate="0" field="entraxe_massif"/>
    <default expression="" applyOnUpdate="0" field="type_massif"/>
    <default expression="" applyOnUpdate="0" field="ral_mat"/>
    <default expression="" applyOnUpdate="0" field="type_crosse"/>
    <default expression="" applyOnUpdate="0" field="ral_crosse"/>
    <default expression="" applyOnUpdate="0" field="prise_illumination"/>
    <default expression="" applyOnUpdate="0" field="ral_illum"/>
    <default expression="" applyOnUpdate="0" field="cellule"/>
    <default expression="" applyOnUpdate="0" field="date_pose"/>
    <default expression="" applyOnUpdate="0" field="date_depose"/>
    <default expression="" applyOnUpdate="0" field="date_action"/>
    <default expression="" applyOnUpdate="0" field="desc_action"/>
    <default expression="" applyOnUpdate="0" field="remarque"/>
    <default expression="" applyOnUpdate="0" field="mslink"/>
    <default expression="" applyOnUpdate="0" field="mapid"/>
    <default expression="" applyOnUpdate="0" field="ecp_intervention_id"/>
    <default expression=" @lumigis_technicien " applyOnUpdate="1" field="acteur"/>
    <default expression="" applyOnUpdate="0" field="rot_z"/>
    <default expression="" applyOnUpdate="0" field="uid4"/>
    <default expression="" applyOnUpdate="0" field="created_at"/>
    <default expression="" applyOnUpdate="0" field="updated_at"/>
    <default expression="" applyOnUpdate="0" field="pkid"/>
    <default expression=" @lumigis_intervention " applyOnUpdate="1" field="intervention"/>
    <default expression="" applyOnUpdate="0" field="photo"/>
  </defaults>
  <constraints>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" unique_strength="1" field="fid"/>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" unique_strength="1" field="numero"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="insee"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="id_support"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="numero_voie"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="type_voie"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="nom_voie"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="etat"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="nature"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="terre"/>
    <constraint constraints="4" notnull_strength="0" exp_strength="2" unique_strength="0" field="boitier"/>
    <constraint constraints="1" notnull_strength="2" exp_strength="0" unique_strength="0" field="type_protection"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="sensibilite_diff"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="calibre_diff"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="forme"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="marque"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="matiere"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="hauteur_totale"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="entraxe_massif"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="type_massif"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="ral_mat"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="type_crosse"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="ral_crosse"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="prise_illumination"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="ral_illum"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="cellule"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_pose"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_depose"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="date_action"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="desc_action"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="remarque"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="mslink"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="mapid"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="ecp_intervention_id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="acteur"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="rot_z"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="uid4"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="created_at"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="updated_at"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="pkid"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="intervention"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="photo"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" exp="" desc=""/>
    <constraint field="numero" exp="" desc=""/>
    <constraint field="insee" exp="" desc=""/>
    <constraint field="id_support" exp="" desc=""/>
    <constraint field="numero_voie" exp="" desc=""/>
    <constraint field="type_voie" exp="" desc=""/>
    <constraint field="nom_voie" exp="" desc=""/>
    <constraint field="etat" exp="" desc=""/>
    <constraint field="nature" exp="" desc=""/>
    <constraint field="terre" exp="" desc=""/>
    <constraint field="boitier" exp="&quot;id&quot;" desc=""/>
    <constraint field="type_protection" exp="" desc=""/>
    <constraint field="sensibilite_diff" exp="" desc=""/>
    <constraint field="calibre_diff" exp="" desc=""/>
    <constraint field="forme" exp="" desc=""/>
    <constraint field="marque" exp="" desc=""/>
    <constraint field="matiere" exp="" desc=""/>
    <constraint field="hauteur_totale" exp="" desc=""/>
    <constraint field="entraxe_massif" exp="" desc=""/>
    <constraint field="type_massif" exp="" desc=""/>
    <constraint field="ral_mat" exp="" desc=""/>
    <constraint field="type_crosse" exp="" desc=""/>
    <constraint field="ral_crosse" exp="" desc=""/>
    <constraint field="prise_illumination" exp="" desc=""/>
    <constraint field="ral_illum" exp="" desc=""/>
    <constraint field="cellule" exp="" desc=""/>
    <constraint field="date_pose" exp="" desc=""/>
    <constraint field="date_depose" exp="" desc=""/>
    <constraint field="date_action" exp="" desc=""/>
    <constraint field="desc_action" exp="" desc=""/>
    <constraint field="remarque" exp="" desc=""/>
    <constraint field="mslink" exp="" desc=""/>
    <constraint field="mapid" exp="" desc=""/>
    <constraint field="ecp_intervention_id" exp="" desc=""/>
    <constraint field="acteur" exp="" desc=""/>
    <constraint field="rot_z" exp="" desc=""/>
    <constraint field="uid4" exp="" desc=""/>
    <constraint field="created_at" exp="" desc=""/>
    <constraint field="updated_at" exp="" desc=""/>
    <constraint field="pkid" exp="" desc=""/>
    <constraint field="intervention" exp="" desc=""/>
    <constraint field="photo" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
    <actionsetting action="[%photo%]" type="5" id="{682e9d02-8a3d-4610-a5ab-bd4105737c5a}" name="photos" notificationMessage="" shortTitle="photos" icon="icon_photo.svg" isEnabledOnlyWhenEditable="0" capture="0">
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id_support&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" name="insee" width="-1" hidden="0"/>
      <column type="field" name="id_support" width="-1" hidden="0"/>
      <column type="field" name="numero_voie" width="-1" hidden="0"/>
      <column type="field" name="type_voie" width="191" hidden="0"/>
      <column type="field" name="nom_voie" width="-1" hidden="0"/>
      <column type="field" name="etat" width="-1" hidden="0"/>
      <column type="field" name="nature" width="308" hidden="0"/>
      <column type="field" name="terre" width="-1" hidden="0"/>
      <column type="field" name="boitier" width="203" hidden="0"/>
      <column type="field" name="type_protection" width="-1" hidden="0"/>
      <column type="field" name="calibre_diff" width="-1" hidden="0"/>
      <column type="field" name="sensibilite_diff" width="-1" hidden="0"/>
      <column type="field" name="forme" width="-1" hidden="0"/>
      <column type="field" name="marque" width="-1" hidden="0"/>
      <column type="field" name="matiere" width="-1" hidden="0"/>
      <column type="field" name="hauteur_totale" width="-1" hidden="0"/>
      <column type="field" name="type_massif" width="-1" hidden="0"/>
      <column type="field" name="entraxe_massif" width="-1" hidden="0"/>
      <column type="field" name="ral_mat" width="-1" hidden="0"/>
      <column type="field" name="type_crosse" width="-1" hidden="0"/>
      <column type="field" name="ral_crosse" width="-1" hidden="0"/>
      <column type="field" name="prise_illumination" width="182" hidden="0"/>
      <column type="field" name="ral_illum" width="216" hidden="0"/>
      <column type="field" name="cellule" width="238" hidden="0"/>
      <column type="field" name="date_pose" width="-1" hidden="0"/>
      <column type="field" name="date_depose" width="-1" hidden="0"/>
      <column type="field" name="mslink" width="-1" hidden="0"/>
      <column type="field" name="mapid" width="-1" hidden="0"/>
      <column type="field" name="acteur" width="-1" hidden="0"/>
      <column type="field" name="created_at" width="-1" hidden="0"/>
      <column type="field" name="updated_at" width="-1" hidden="0"/>
      <column type="field" name="remarque" width="-1" hidden="0"/>
      <column type="field" name="rot_z" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" name="date_action" width="-1" hidden="0"/>
      <column type="field" name="photo" width="-1" hidden="0"/>
      <column type="field" name="uid4" width="-1" hidden="0"/>
      <column type="field" name="numero" width="-1" hidden="0"/>
      <column type="field" name="desc_action" width="-1" hidden="0"/>
      <column type="field" name="ecp_intervention_id" width="-1" hidden="0"/>
      <column type="field" name="pkid" width="-1" hidden="0"/>
      <column type="field" name="fid" width="-1" hidden="0"/>
      <column type="field" name="intervention" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">P:/SIG/gestion</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>P:/SIG/gestion</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
      <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
    </labelStyle>
    <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#cccccc" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Fiche descriptive" visibilityExpression="" groupBox="0">
      <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
        <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      </labelStyle>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Généralités" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="40" showLabel="1" name="intervention">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="34" showLabel="1" name="acteur">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="2" showLabel="1" name="insee">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Situation" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="4" showLabel="1" name="numero_voie">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="5" showLabel="1" name="type_voie">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="6" showLabel="1" name="nom_voie">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="0" collapsed="0" columnCount="1" name="Identification" visibilityExpression="" groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="3" showLabel="1" name="id_support">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="7" showLabel="1" name="etat">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="8" showLabel="1" name="nature">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="26" showLabel="1" name="date_pose">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="1" collapsed="0" columnCount="1" name="Caractéristiques techniques candélabre" visibilityExpression=" &quot;cellule&quot; = 'MAT BOULE' or  &quot;cellule&quot; = 'MAT SIMPLE' or  &quot;cellule&quot; = 'MAT TRIPLE' or &quot;cellule&quot; =  'MAT DOUBLE'  or &quot;cellule&quot; =  'MAT QUADRUPLE' " groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="14" showLabel="1" name="forme">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="10" showLabel="1" name="boitier">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="16" showLabel="1" name="matiere">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="15" showLabel="1" name="marque">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="20" showLabel="1" name="ral_mat">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="17" showLabel="1" name="hauteur_totale">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="9" showLabel="1" name="terre">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="11" showLabel="1" name="type_protection">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="13" showLabel="1" name="calibre_diff">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="12" showLabel="1" name="sensibilite_diff">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="23" showLabel="1" name="prise_illumination">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="24" showLabel="1" name="ral_illum">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="18" showLabel="1" name="entraxe_massif">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="19" showLabel="1" name="type_massif">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="21" showLabel="1" name="type_crosse">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField index="22" showLabel="1" name="ral_crosse">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="1" collapsed="0" columnCount="1" name="Caractéristiques techniques supports (béton, bois......)" visibilityExpression=" &quot;cellule&quot; = 'CARRE FER' or  &quot;cellule&quot; = 'POTEAU BETON' or  &quot;cellule&quot; = 'POTEAU BOIS' or  &quot;cellule&quot; = 'POTEAU BOIS CONTREFICHE' or  &quot;cellule&quot; = 'POTEAU BOIS DOUBLE' or  &quot;cellule&quot; = 'SUPPORT FACADE' or  &quot;cellule&quot; = 'SUPPORT PROJECTEUR' " groupBox="1">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
        <attributeEditorField index="10" showLabel="1" name="boitier">
          <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
            <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" collapsedExpression="" collapsedExpressionEnabled="0" backgroundColor="#a6cee3" visibilityExpressionEnabled="1" collapsed="0" columnCount="1" name="Remarques" visibilityExpression=" &quot;remarque&quot; &lt;> ''" groupBox="0">
      <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
        <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
      </labelStyle>
      <attributeEditorField index="30" showLabel="1" name="remarque">
        <labelStyle overrideLabelColor="0" labelColor="0,0,0,255" overrideLabelFont="0">
          <labelFont underline="0" style="" bold="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" italic="0"/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="acteur"/>
    <field editable="1" name="boitier"/>
    <field editable="0" name="boitier_support"/>
    <field editable="1" name="calibre_diff"/>
    <field editable="1" name="cellule"/>
    <field editable="1" name="created_at"/>
    <field editable="1" name="date_action"/>
    <field editable="1" name="date_depose"/>
    <field editable="1" name="date_intervention"/>
    <field editable="1" name="date_pose"/>
    <field editable="1" name="desc_action"/>
    <field editable="1" name="ecp_intervention_id"/>
    <field editable="1" name="entraxe_massif"/>
    <field editable="1" name="etat"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="forme"/>
    <field editable="1" name="hauteur_totale"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_intervention"/>
    <field editable="1" name="id_support"/>
    <field editable="1" name="insee"/>
    <field editable="1" name="intervention"/>
    <field editable="1" name="mapid"/>
    <field editable="1" name="marque"/>
    <field editable="1" name="matiere"/>
    <field editable="1" name="mslink"/>
    <field editable="1" name="nature"/>
    <field editable="1" name="nom_voie"/>
    <field editable="1" name="numero"/>
    <field editable="1" name="numero_voie"/>
    <field editable="1" name="photo"/>
    <field editable="1" name="pkid"/>
    <field editable="1" name="prise_illumination"/>
    <field editable="1" name="ral_crosse"/>
    <field editable="1" name="ral_illum"/>
    <field editable="1" name="ral_mat"/>
    <field editable="1" name="remarque"/>
    <field editable="1" name="rot_z"/>
    <field editable="1" name="sensibilite_diff"/>
    <field editable="1" name="terre"/>
    <field editable="1" name="type_crosse"/>
    <field editable="1" name="type_massif"/>
    <field editable="1" name="type_protection"/>
    <field editable="1" name="type_voie"/>
    <field editable="1" name="uid4"/>
    <field editable="1" name="updated_at"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="1" name="acteur"/>
    <field labelOnTop="0" name="boitier"/>
    <field labelOnTop="0" name="boitier_support"/>
    <field labelOnTop="0" name="calibre_diff"/>
    <field labelOnTop="0" name="cellule"/>
    <field labelOnTop="0" name="created_at"/>
    <field labelOnTop="0" name="date_action"/>
    <field labelOnTop="0" name="date_depose"/>
    <field labelOnTop="0" name="date_intervention"/>
    <field labelOnTop="0" name="date_pose"/>
    <field labelOnTop="0" name="desc_action"/>
    <field labelOnTop="0" name="ecp_intervention_id"/>
    <field labelOnTop="0" name="entraxe_massif"/>
    <field labelOnTop="0" name="etat"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="forme"/>
    <field labelOnTop="0" name="hauteur_totale"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_intervention"/>
    <field labelOnTop="0" name="id_support"/>
    <field labelOnTop="1" name="insee"/>
    <field labelOnTop="1" name="intervention"/>
    <field labelOnTop="0" name="mapid"/>
    <field labelOnTop="0" name="marque"/>
    <field labelOnTop="0" name="matiere"/>
    <field labelOnTop="0" name="mslink"/>
    <field labelOnTop="0" name="nature"/>
    <field labelOnTop="0" name="nom_voie"/>
    <field labelOnTop="0" name="numero"/>
    <field labelOnTop="0" name="numero_voie"/>
    <field labelOnTop="1" name="photo"/>
    <field labelOnTop="0" name="pkid"/>
    <field labelOnTop="0" name="prise_illumination"/>
    <field labelOnTop="0" name="ral_crosse"/>
    <field labelOnTop="0" name="ral_illum"/>
    <field labelOnTop="0" name="ral_mat"/>
    <field labelOnTop="1" name="remarque"/>
    <field labelOnTop="0" name="rot_z"/>
    <field labelOnTop="0" name="sensibilite_diff"/>
    <field labelOnTop="0" name="terre"/>
    <field labelOnTop="0" name="type_crosse"/>
    <field labelOnTop="0" name="type_massif"/>
    <field labelOnTop="0" name="type_protection"/>
    <field labelOnTop="0" name="type_voie"/>
    <field labelOnTop="0" name="uid4"/>
    <field labelOnTop="0" name="updated_at"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="acteur" reuseLastValue="0"/>
    <field name="boitier" reuseLastValue="0"/>
    <field name="calibre_diff" reuseLastValue="0"/>
    <field name="cellule" reuseLastValue="0"/>
    <field name="created_at" reuseLastValue="0"/>
    <field name="date_action" reuseLastValue="0"/>
    <field name="date_depose" reuseLastValue="0"/>
    <field name="date_pose" reuseLastValue="0"/>
    <field name="desc_action" reuseLastValue="0"/>
    <field name="ecp_intervention_id" reuseLastValue="0"/>
    <field name="entraxe_massif" reuseLastValue="0"/>
    <field name="etat" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="forme" reuseLastValue="0"/>
    <field name="hauteur_totale" reuseLastValue="0"/>
    <field name="id_support" reuseLastValue="0"/>
    <field name="insee" reuseLastValue="0"/>
    <field name="intervention" reuseLastValue="0"/>
    <field name="mapid" reuseLastValue="0"/>
    <field name="marque" reuseLastValue="0"/>
    <field name="matiere" reuseLastValue="0"/>
    <field name="mslink" reuseLastValue="0"/>
    <field name="nature" reuseLastValue="0"/>
    <field name="nom_voie" reuseLastValue="0"/>
    <field name="numero" reuseLastValue="0"/>
    <field name="numero_voie" reuseLastValue="0"/>
    <field name="photo" reuseLastValue="0"/>
    <field name="pkid" reuseLastValue="0"/>
    <field name="prise_illumination" reuseLastValue="0"/>
    <field name="ral_crosse" reuseLastValue="0"/>
    <field name="ral_illum" reuseLastValue="0"/>
    <field name="ral_mat" reuseLastValue="0"/>
    <field name="remarque" reuseLastValue="0"/>
    <field name="rot_z" reuseLastValue="0"/>
    <field name="sensibilite_diff" reuseLastValue="0"/>
    <field name="terre" reuseLastValue="0"/>
    <field name="type_crosse" reuseLastValue="0"/>
    <field name="type_massif" reuseLastValue="0"/>
    <field name="type_protection" reuseLastValue="0"/>
    <field name="type_voie" reuseLastValue="0"/>
    <field name="uid4" reuseLastValue="0"/>
    <field name="updated_at" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="intervention">
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option type="Map" name="properties">
          <Option type="Map" name="dataDefinedAlias">
            <Option type="bool" name="active" value="false"/>
            <Option type="int" name="type" value="1"/>
            <Option type="QString" name="val" value=""/>
          </Option>
        </Option>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>COALESCE( "id_support", '&lt;NULL>' )</previewExpression>
  <mapTip>[%'&lt;div style="width:200">' || "id_support"  ||  '&lt;br/>'  ||  "nom_voie" || '&lt;/div>' %]</mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
