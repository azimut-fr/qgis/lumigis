<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="0" styleCategories="AllStyleCategories" maxScale="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" minScale="100000000" simplifyAlgorithm="0" simplifyMaxScale="1" symbologyReferenceScale="-1" readOnly="0" version="3.32.0-Lima" labelsEnabled="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" startField="" accumulate="0" endField="" durationUnit="min" fixedDuration="0" endExpression="" enabled="0" mode="0" startExpression="" durationField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zscale="1" extrusion="0" type="IndividualFeatures" symbology="Line" zoffset="0" clamping="Terrain" extrusionEnabled="0" respectLayerSymbol="1" showMarkerSymbolInSurfacePlots="0" binding="Centroid">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" type="line" alpha="1" force_rhr="0" name="" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{5aef585e-60e0-4510-a353-d1aaee32455e}" class="SimpleLine" pass="0" locked="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="243,166,178,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" type="fill" alpha="1" force_rhr="0" name="" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{1618b2ca-cfdc-43eb-a50f-db14a6543e85}" class="SimpleFill" pass="0" locked="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="243,166,178,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="174,119,127,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" type="marker" alpha="1" force_rhr="0" name="" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{83b41263-ee3a-4b66-b183-678caa876b99}" class="SimpleMarker" pass="0" locked="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="243,166,178,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="174,119,127,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="0" type="RuleRenderer" referencescale="-1" enableorderby="0" forceraster="0">
    <rules key="{9ac1a0d0-2e77-43de-819e-7f85a55f26bc}">
      <rule scalemindenom="1500" scalemaxdenom="1000000" symbol="0" key="{d2a7d2c8-9b0c-46d0-b704-424422dc9480}"/>
      <rule scalemaxdenom="1500" symbol="1" key="{258fb294-7aef-45ad-8427-cf6e5acebf9e}"/>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" type="marker" alpha="1" force_rhr="0" name="0" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{3e7fc04c-98f4-40f2-9345-4418a0048b1f}" class="SvgMarker" pass="0" locked="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="255,255,255,255" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgd2lkdGg9IjMyLjg4NDkzbW0iCiAgIGhlaWdodD0iMzYuMTI0NDQzbW0iCiAgIHZpZXdCb3g9IjAgMCAxMTYuNTIxNCAxMjcuOTk5OTkiCiAgIGlkPSJzdmc0MjM3IgogICB2ZXJzaW9uPSIxLjEiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM0MjM5IiAvPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTQyNDIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTAiCiAgICAgc3R5bGU9ImZpbGw6I2JkYmNiYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDQxLjEgNjAuOCwzMCBjIDEuNiwwLjggMi41LDIuMiAyLjgsNCAwLjMsMS43IC0wLjIsMy4zIC0xLjQsNC42IGwgLTQxLjIsNDQuOSBjIC0xLjksMiAtNC44LDIuMSAtNywwLjQgTCA0LjYsODEuNiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDM2LjYgNjAuOCwzMC4xIGMgMS42LDAuNyAyLjUsMi4xIDIuOCwzLjkgMC4zLDEuNyAtMC4yLDMuNCAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC41IEwgNC42LDc3LjEgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU0IgogICAgIHN0eWxlPSJmaWxsOiNlOWU5ZTk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gNDkuMSwzMi4xIDYwLjgsMzAuMSBjIDEuNiwwLjggMi41LDIuMiAyLjgsMy45IDAuMywxLjggLTAuMiwzLjQgLTEuNCw0LjcgbCAtNDEuMiw0NC44IGMgLTEuOSwyLjEgLTQuOCwyLjIgLTcsMC41IEwgNC42LDcyLjYgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU2IgogICAgIGQ9Im0gNDkuMSwyNy43IDYwLjgsMzAgYyAxLjYsMC44IDIuNSwyLjIgMi44LDQgMC4zLDEuNyAtMC4yLDMuMyAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC40IEwgNC42LDY4LjIgWiIgZmlsbD0icGFyYW0oZmlsbCkiIGZpbGwtb3BhY2l0eT0icGFyYW0oZmlsbC1vcGFjaXR5KSIgc3Ryb2tlPSJwYXJhbShvdXRsaW5lKSIgc3Ryb2tlLW9wYWNpdHk9InBhcmFtKG91dGxpbmUtb3BhY2l0eSkiIHN0cm9rZS13aWR0aD0icGFyYW0ob3V0bGluZS13aWR0aCkiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ1OCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDM5LDM1LjYgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjEsLTIuNCA0LjMsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMSw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDI5LjUsNDQuNSBjIC0wLjgsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMSwtMS4xIDIuOCwxLjkgNC4yLDUuMyAzLjEsNy43IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjIiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxOS45LDUzLjUgYyAtMC43LC0xLjYgLTAuOCwtMy4yIC0wLjIsLTQuNSAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwLjQsNjIuNCBjIC0wLjcsLTEuNiAtMC44LC0zLjIgLTAuMiwtNC41IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMSAyLjgsMS44IDQuMSw1LjIgMyw3LjYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDQuNyw3NS42IEMgNC42LDc1LjUgNC41LDc1LjUgNC40LDc1LjQgMS41LDczLjYgMC4yLDcwLjEgMS4zLDY3LjcgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjgiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0My40LDMwLjcgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDMzLjksMzkuNiBjIC0wLjcsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMS4xIDIuOCwxLjkgNC4xLDUuMyAzLDcuNyIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDcyIgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMjQuNCw0OC42IGMgLTAuOCwtMS42IC0wLjgsLTMuMiAtMC4yLC00LjUgMS4xLC0yLjQgNC4zLC0yLjkgNy4xLC0xLjEgMi44LDEuOSA0LjIsNS4zIDMuMSw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDE0LjgsNTcuNSBDIDE0LjEsNTUuOSAxNCw1NC4zIDE0LjYsNTMgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NzYiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA2LDY3LjMgQyA1LjIsNjUuNyA1LjIsNjQuMSA1LjgsNjIuOCBjIDEuMSwtMi40IDQuMywtMi45IDcuMSwtMSAyLjgsMS44IDQuMiw1LjIgMy4xLDcuNiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDc4IgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gMjUuNiw3Ni40IDQ1LjIsNTYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDM0LjEsNzkuMyA0NC4yLDY4LjgiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDUwLjYsNjEuMyA2MS43LDQ5LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDY5LjIsNTYgNzksNTIuNSA5NS4yLDU5LjYgNjMuNyw5Ni45IFoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NiIKICAgICBzdHlsZT0iZmlsbDojZDNkMmQyO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTpub25lIgogICAgIGQ9Ik0gOTQuNCw1OS4yIDc0LjMsODMgYyAwLDAgLTMuMSwxLjMgLTIuNCwtMC4xIEwgODQuMSw1OS40IGMgNiwtMy4yIDguMiwtMy41IDEwLjMsLTAuMiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDg4IgogICAgIHN0eWxlPSJmaWxsOiM5MThmOTA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTEzLjUsMTUuMjEgLTExLjksMC41OSBjIDAsMCAtMTcuNjYsMzguNDggLTE4LjUsNDUuNTEgLTAuNjEsNS4wOCA4LjY4LC02LjcyIDExLjYsLTEuMTEgMC4zLDMuOCAxOC44LC00NC45OSAxOC44LC00NC45OSB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTAiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSAxMDEuNiwxNS44IDg4LjIsOS44IDY5LjIsNTYgYyAtMS40LDMuNCAxMi41LC02LjQ4IDEzLjksNS42MiAtMS4yLDIuOSAxMi44LC0zMS43MiAxOC41LC00NS44MiB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA5MC4zLDguMiA4Ny4xLDYuMDExIGMgMCwwIC03Ljk2LDE4Ljc0OSAtMTYuMDgsMzguNTE5IDAsMCAtMC45NSw1LjM2IC0xLjc4LDExLjE2IEMgNjguMzQsNjEuOTMgOTAuMyw4LjIgOTAuMyw4LjIgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDk0IgogICAgIHN0eWxlPSJmaWxsOiMyMzFmMjA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gNzUuNiw4Mi44IDYzLjcsOTYuOSBjIDAuNywtNS44IDEuNSwtMTEuNiAyLjMsLTE3LjQgMi44LDIuOSA2LjEsNCA5LjYsMy4zIHoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ5OCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwNC43LDQuMiBjIDcsMy4yIDEwLjksOC4zIDguNiwxMS42IC0yLjIsMy4yIC05LjcsMy4zIC0xNi43LDAuMSBDIDg5LjYsMTIuOCA4NS44LDcuNiA4OCw0LjMgOTAuMywxLjEgOTcuOCwxIDEwNC43LDQuMiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg1MDAiCiAgICAgc3R5bGU9ImZpbGw6IzIzMWYyMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMDEuOSw4LjMgYyAyLjEsMSAzLjIsMi41IDIuNiwzLjUgLTAuNywwLjkgLTIuOSwxIC01LDAgLTIuMSwtMC45IC0zLjMsLTIuNSAtMi42LC0zLjUgMC43LC0wLjkgMi45LC0wLjkgNSwwIHoiIC8+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255" name="outline_color"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="8" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" type="marker" alpha="1" force_rhr="0" name="1" is_animated="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{80beb79f-a4c8-45fd-aa09-c19c69aec3c4}" class="SvgMarker" pass="0" locked="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="255,255,255,255" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgd2lkdGg9IjMyLjg4NDkzbW0iCiAgIGhlaWdodD0iMzYuMTI0NDQzbW0iCiAgIHZpZXdCb3g9IjAgMCAxMTYuNTIxNCAxMjcuOTk5OTkiCiAgIGlkPSJzdmc0MjM3IgogICB2ZXJzaW9uPSIxLjEiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM0MjM5IiAvPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTQyNDIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTAiCiAgICAgc3R5bGU9ImZpbGw6I2JkYmNiYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDQxLjEgNjAuOCwzMCBjIDEuNiwwLjggMi41LDIuMiAyLjgsNCAwLjMsMS43IC0wLjIsMy4zIC0xLjQsNC42IGwgLTQxLjIsNDQuOSBjIC0xLjksMiAtNC44LDIuMSAtNywwLjQgTCA0LjYsODEuNiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDM2LjYgNjAuOCwzMC4xIGMgMS42LDAuNyAyLjUsMi4xIDIuOCwzLjkgMC4zLDEuNyAtMC4yLDMuNCAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC41IEwgNC42LDc3LjEgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU0IgogICAgIHN0eWxlPSJmaWxsOiNlOWU5ZTk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gNDkuMSwzMi4xIDYwLjgsMzAuMSBjIDEuNiwwLjggMi41LDIuMiAyLjgsMy45IDAuMywxLjggLTAuMiwzLjQgLTEuNCw0LjcgbCAtNDEuMiw0NC44IGMgLTEuOSwyLjEgLTQuOCwyLjIgLTcsMC41IEwgNC42LDcyLjYgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU2IgogICAgIGQ9Im0gNDkuMSwyNy43IDYwLjgsMzAgYyAxLjYsMC44IDIuNSwyLjIgMi44LDQgMC4zLDEuNyAtMC4yLDMuMyAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC40IEwgNC42LDY4LjIgWiIgZmlsbD0icGFyYW0oZmlsbCkiIGZpbGwtb3BhY2l0eT0icGFyYW0oZmlsbC1vcGFjaXR5KSIgc3Ryb2tlPSJwYXJhbShvdXRsaW5lKSIgc3Ryb2tlLW9wYWNpdHk9InBhcmFtKG91dGxpbmUtb3BhY2l0eSkiIHN0cm9rZS13aWR0aD0icGFyYW0ob3V0bGluZS13aWR0aCkiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ1OCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDM5LDM1LjYgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjEsLTIuNCA0LjMsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMSw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDI5LjUsNDQuNSBjIC0wLjgsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMSwtMS4xIDIuOCwxLjkgNC4yLDUuMyAzLjEsNy43IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjIiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxOS45LDUzLjUgYyAtMC43LC0xLjYgLTAuOCwtMy4yIC0wLjIsLTQuNSAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwLjQsNjIuNCBjIC0wLjcsLTEuNiAtMC44LC0zLjIgLTAuMiwtNC41IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMSAyLjgsMS44IDQuMSw1LjIgMyw3LjYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDQuNyw3NS42IEMgNC42LDc1LjUgNC41LDc1LjUgNC40LDc1LjQgMS41LDczLjYgMC4yLDcwLjEgMS4zLDY3LjcgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjgiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0My40LDMwLjcgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDMzLjksMzkuNiBjIC0wLjcsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMS4xIDIuOCwxLjkgNC4xLDUuMyAzLDcuNyIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDcyIgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMjQuNCw0OC42IGMgLTAuOCwtMS42IC0wLjgsLTMuMiAtMC4yLC00LjUgMS4xLC0yLjQgNC4zLC0yLjkgNy4xLC0xLjEgMi44LDEuOSA0LjIsNS4zIDMuMSw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDE0LjgsNTcuNSBDIDE0LjEsNTUuOSAxNCw1NC4zIDE0LjYsNTMgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NzYiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA2LDY3LjMgQyA1LjIsNjUuNyA1LjIsNjQuMSA1LjgsNjIuOCBjIDEuMSwtMi40IDQuMywtMi45IDcuMSwtMSAyLjgsMS44IDQuMiw1LjIgMy4xLDcuNiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDc4IgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gMjUuNiw3Ni40IDQ1LjIsNTYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDM0LjEsNzkuMyA0NC4yLDY4LjgiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDUwLjYsNjEuMyA2MS43LDQ5LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDY5LjIsNTYgNzksNTIuNSA5NS4yLDU5LjYgNjMuNyw5Ni45IFoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NiIKICAgICBzdHlsZT0iZmlsbDojZDNkMmQyO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTpub25lIgogICAgIGQ9Ik0gOTQuNCw1OS4yIDc0LjMsODMgYyAwLDAgLTMuMSwxLjMgLTIuNCwtMC4xIEwgODQuMSw1OS40IGMgNiwtMy4yIDguMiwtMy41IDEwLjMsLTAuMiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDg4IgogICAgIHN0eWxlPSJmaWxsOiM5MThmOTA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTEzLjUsMTUuMjEgLTExLjksMC41OSBjIDAsMCAtMTcuNjYsMzguNDggLTE4LjUsNDUuNTEgLTAuNjEsNS4wOCA4LjY4LC02LjcyIDExLjYsLTEuMTEgMC4zLDMuOCAxOC44LC00NC45OSAxOC44LC00NC45OSB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTAiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSAxMDEuNiwxNS44IDg4LjIsOS44IDY5LjIsNTYgYyAtMS40LDMuNCAxMi41LC02LjQ4IDEzLjksNS42MiAtMS4yLDIuOSAxMi44LC0zMS43MiAxOC41LC00NS44MiB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA5MC4zLDguMiA4Ny4xLDYuMDExIGMgMCwwIC03Ljk2LDE4Ljc0OSAtMTYuMDgsMzguNTE5IDAsMCAtMC45NSw1LjM2IC0xLjc4LDExLjE2IEMgNjguMzQsNjEuOTMgOTAuMyw4LjIgOTAuMyw4LjIgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDk0IgogICAgIHN0eWxlPSJmaWxsOiMyMzFmMjA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gNzUuNiw4Mi44IDYzLjcsOTYuOSBjIDAuNywtNS44IDEuNSwtMTEuNiAyLjMsLTE3LjQgMi44LDIuOSA2LjEsNCA5LjYsMy4zIHoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ5OCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwNC43LDQuMiBjIDcsMy4yIDEwLjksOC4zIDguNiwxMS42IC0yLjIsMy4yIC05LjcsMy4zIC0xNi43LDAuMSBDIDg5LjYsMTIuOCA4NS44LDcuNiA4OCw0LjMgOTAuMywxLjEgOTcuOCwxIDEwNC43LDQuMiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg1MDAiCiAgICAgc3R5bGU9ImZpbGw6IzIzMWYyMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMDEuOSw4LjMgYyAyLjEsMSAzLjIsMi41IDIuNiwzLjUgLTAuNywwLjkgLTIuOSwxIC01LDAgLTIuMSwtMC45IC0zLjMsLTIuNSAtMi42LC0zLjUgMC43LC0wLjkgMi45LC0wLjkgNSwwIHoiIC8+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontSize="10" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontKerning="1" multilineHeightUnit="Percentage" blendMode="0" useSubstitutions="0" fieldName="texte" fontWordSpacing="0.1875" capitalization="0" legendString="Aa" fontWeight="75" fontSizeUnit="Point" namedStyle="Bold Italic" multilineHeight="1" textColor="0,0,0,255" textOpacity="1" forcedItalic="0" fontItalic="1" fontLetterSpacing="0.1875" fontStrikeout="0" previewBkgrdColor="255,255,255,255" forcedBold="0" fontFamily="Arial" fontUnderline="0" isExpression="0" textOrientation="horizontal" allowHtml="0">
        <families/>
        <text-buffer bufferOpacity="1" bufferSizeUnits="MM" bufferBlendMode="0" bufferNoFill="1" bufferJoinStyle="128" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferDraw="0" bufferSize="1" bufferColor="255,255,255,255"/>
        <text-mask maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSizeUnits="MM" maskType="0" maskOpacity="1" maskSize="0" maskEnabled="0" maskJoinStyle="128" maskedSymbolLayers=""/>
        <background shapeOpacity="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="1" shapeSizeType="0" shapeRotationType="0" shapeType="0" shapeSizeUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="MM" shapeSizeX="1" shapeOffsetX="0" shapeRadiiUnit="MM" shapeJoinStyle="64" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRotation="0" shapeDraw="1" shapeRadiiX="1" shapeOffsetUnit="MM" shapeSVGFile="" shapeBlendMode="0" shapeOffsetY="0" shapeBorderColor="227,26,28,255" shapeSizeY="1" shapeBorderWidth="0.29999999999999999" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0">
          <symbol clip_to_extent="1" type="marker" alpha="1" force_rhr="0" name="markerSymbol" is_animated="0" frame_rate="10">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer id="" class="SimpleMarker" pass="0" locked="0" enabled="1">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="square" name="cap_style"/>
                <Option type="QString" value="231,113,72,255" name="color"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="circle" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="2" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol clip_to_extent="1" type="fill" alpha="1" force_rhr="0" name="fillSymbol" is_animated="0" frame_rate="10">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer id="" class="SimpleFill" pass="0" locked="0" enabled="1">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,255,255,255" name="color"/>
                <Option type="QString" value="round" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="227,26,28,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0.3" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetDist="1" shadowOffsetGlobal="1" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowUnder="0" shadowScale="100" shadowDraw="1" shadowRadius="1.5" shadowRadiusUnit="MM" shadowColor="0,0,0,255" shadowOpacity="0.69999999999999996" shadowBlendMode="6" shadowOffsetUnit="MM" shadowRadiusAlphaOnly="0" shadowOffsetAngle="135" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format placeDirectionSymbol="0" reverseDirectionSymbol="0" wrapChar="" useMaxLineLengthForAutoWrap="1" decimals="3" autoWrapLength="0" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" plussign="0" addDirectionSymbol="0" formatNumbers="0" multilineAlign="3"/>
      <placement overrunDistance="0" placementFlags="10" quadOffset="4" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" maxCurvedCharAngleOut="-25" centroidInside="0" geometryGeneratorType="PointGeometry" allowDegraded="0" polygonPlacementFlags="2" centroidWhole="0" yOffset="0" distMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorEnabled="0" preserveRotation="1" overlapHandling="PreventOverlap" lineAnchorClipping="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorPercent="0.5" distUnits="MM" repeatDistance="0" offsetType="0" xOffset="0" repeatDistanceUnits="MM" lineAnchorType="0" maxCurvedCharAngleIn="25" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="0" overrunDistanceUnit="MM" rotationAngle="0" layerType="PointGeometry" geometryGenerator="" rotationUnit="AngleDegrees" lineAnchorTextPoint="CenterOfText" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" priority="5" dist="5"/>
      <rendering upsidedownLabels="0" minFeatureSize="0" fontLimitPixelSize="0" obstacleType="0" unplacedVisibility="0" mergeLines="0" obstacleFactor="1" scaleMin="0" maxNumLabels="2000" scaleVisibility="1" fontMinPixelSize="3" drawLabels="1" zIndex="5" scaleMax="1501" fontMaxPixelSize="10000" labelPerPart="0" limitNumLabels="0" obstacle="1"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" value="" name="name"/>
          <Option type="Map" name="properties">
            <Option type="Map" name="LabelRotation">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="rot_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="PositionX">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="x_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="PositionY">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="y_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="ShapeBorderColor">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FF0000', '#000000')" name="expression"/>
              <Option type="int" value="3" name="type"/>
            </Option>
            <Option type="Map" name="ShapeFillColor">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
              <Option type="int" value="3" name="type"/>
            </Option>
          </Option>
          <Option type="QString" value="collection" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
          <Option type="int" value="0" name="blendMode"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
          <Option type="bool" value="false" name="drawToAllParts"/>
          <Option type="QString" value="1" name="enabled"/>
          <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
          <Option type="QString" value="&lt;symbol clip_to_extent=&quot;1&quot; type=&quot;line&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot; name=&quot;symbol&quot; is_animated=&quot;0&quot; frame_rate=&quot;10&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer id=&quot;{b77f6238-49f1-40e2-8b80-b5778915dfe5}&quot; class=&quot;SimpleLine&quot; pass=&quot;0&quot; locked=&quot;0&quot; enabled=&quot;1&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;align_dash_pattern&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;square&quot; name=&quot;capstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;5;2&quot; name=&quot;customdash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;customdash_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;customdash_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;dash_pattern_offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;dash_pattern_offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;draw_inside_polygon&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;82,82,82,255&quot; name=&quot;line_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;line_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.3&quot; name=&quot;line_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;line_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_end&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_end_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_end_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_start&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_start_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_start_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;use_custom_dash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;width_map_unit_scale&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
          <Option type="double" value="0.2" name="minLength"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
          <Option type="QString" value="MM" name="minLengthUnit"/>
          <Option type="double" value="1" name="offsetFromAnchor"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
          <Option type="double" value="1.9999999999999998" name="offsetFromLabel"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;texte&quot;"/>
      </Option>
      <Option type="QString" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory direction="1" opacity="1" minimumSize="0" penColor="#000000" maxScaleDenominator="1e+08" showAxis="0" scaleDependency="Area" width="15" sizeScale="3x:0,0,0,0,0,0" lineSizeScale="3x:0,0,0,0,0,0" sizeType="MM" enabled="0" penWidth="0" spacing="0" backgroundAlpha="255" labelPlacementMethod="XHeight" barWidth="5" spacingUnitScale="3x:0,0,0,0,0,0" diagramOrientation="Up" spacingUnit="MM" scaleBasedVisibility="0" height="15" backgroundColor="#ffffff" lineSizeType="MM" rotationOffset="270" minScaleDenominator="0" penAlpha="255">
      <fontProperties description="Roboto,11,-1,5,25,0,0,0,0,0" underline="0" strikethrough="0" style="" bold="0" italic="0"/>
      <attribute label="" color="#000000" colorOpacity="1" field=""/>
      <axisSymbol>
        <symbol clip_to_extent="1" type="line" alpha="1" force_rhr="0" name="" is_animated="0" frame_rate="10">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer id="{a9b1d55a-1957-4cbc-8bf9-cd93267e4342}" class="SimpleLine" pass="0" locked="0" enabled="1">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" showAll="1" obstacle="0" linePlacementFlags="18" priority="0" placement="0" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id_note" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="texte" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="important" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date_creation" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="date_modification" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="user_creation" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="user_modification" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="x_label" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="y_label" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="rot_label" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id_note"/>
    <alias index="1" name="" field="texte"/>
    <alias index="2" name="" field="important"/>
    <alias index="3" name="" field="date_creation"/>
    <alias index="4" name="" field="date_modification"/>
    <alias index="5" name="" field="user_creation"/>
    <alias index="6" name="" field="user_modification"/>
    <alias index="7" name="" field="x_label"/>
    <alias index="8" name="" field="y_label"/>
    <alias index="9" name="" field="rot_label"/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="id_note"/>
    <policy policy="Duplicate" field="texte"/>
    <policy policy="Duplicate" field="important"/>
    <policy policy="Duplicate" field="date_creation"/>
    <policy policy="Duplicate" field="date_modification"/>
    <policy policy="Duplicate" field="user_creation"/>
    <policy policy="Duplicate" field="user_modification"/>
    <policy policy="Duplicate" field="x_label"/>
    <policy policy="Duplicate" field="y_label"/>
    <policy policy="Duplicate" field="rot_label"/>
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" expression="" field="id_note"/>
    <default applyOnUpdate="0" expression="" field="texte"/>
    <default applyOnUpdate="0" expression="" field="important"/>
    <default applyOnUpdate="0" expression="now()" field="date_creation"/>
    <default applyOnUpdate="1" expression="now()" field="date_modification"/>
    <default applyOnUpdate="0" expression=" @user_full_name " field="user_creation"/>
    <default applyOnUpdate="1" expression=" @user_full_name " field="user_modification"/>
    <default applyOnUpdate="0" expression="" field="x_label"/>
    <default applyOnUpdate="0" expression="" field="y_label"/>
    <default applyOnUpdate="0" expression="" field="rot_label"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" exp_strength="0" notnull_strength="1" constraints="3" field="id_note"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="texte"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="important"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="date_creation"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="date_modification"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="user_creation"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="user_modification"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="x_label"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="y_label"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" constraints="0" field="rot_label"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id_note" exp=""/>
    <constraint desc="" field="texte" exp=""/>
    <constraint desc="" field="important" exp=""/>
    <constraint desc="" field="date_creation" exp=""/>
    <constraint desc="" field="date_modification" exp=""/>
    <constraint desc="" field="user_creation" exp=""/>
    <constraint desc="" field="user_modification" exp=""/>
    <constraint desc="" field="x_label" exp=""/>
    <constraint desc="" field="y_label" exp=""/>
    <constraint desc="" field="rot_label" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id_note&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" hidden="0" name="id_note" width="-1"/>
      <column type="field" hidden="0" name="texte" width="674"/>
      <column type="field" hidden="0" name="important" width="82"/>
      <column type="field" hidden="0" name="date_creation" width="167"/>
      <column type="field" hidden="0" name="date_modification" width="-1"/>
      <column type="field" hidden="0" name="user_creation" width="-1"/>
      <column type="field" hidden="0" name="user_modification" width="-1"/>
      <column type="field" hidden="0" name="x_label" width="-1"/>
      <column type="field" hidden="0" name="y_label" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="rot_label" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="0" name="date_creation"/>
    <field editable="0" name="date_modification"/>
    <field editable="0" name="id_note"/>
    <field editable="1" name="important"/>
    <field editable="0" name="rot_label"/>
    <field editable="1" name="texte"/>
    <field editable="0" name="user_creation"/>
    <field editable="0" name="user_modification"/>
    <field editable="0" name="x_label"/>
    <field editable="0" name="y_label"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="date_creation"/>
    <field labelOnTop="0" name="date_modification"/>
    <field labelOnTop="0" name="id_note"/>
    <field labelOnTop="0" name="important"/>
    <field labelOnTop="0" name="rot_label"/>
    <field labelOnTop="0" name="texte"/>
    <field labelOnTop="0" name="user_creation"/>
    <field labelOnTop="0" name="user_modification"/>
    <field labelOnTop="0" name="x_label"/>
    <field labelOnTop="0" name="y_label"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="date_creation" reuseLastValue="0"/>
    <field name="date_modification" reuseLastValue="0"/>
    <field name="id_note" reuseLastValue="0"/>
    <field name="important" reuseLastValue="0"/>
    <field name="rot_label" reuseLastValue="0"/>
    <field name="texte" reuseLastValue="0"/>
    <field name="user_creation" reuseLastValue="0"/>
    <field name="user_modification" reuseLastValue="0"/>
    <field name="x_label" reuseLastValue="0"/>
    <field name="y_label" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"texte"</previewExpression>
  <mapTip enabled="1">[% "texte" %]
&lt;/br>
modif par [% "user_modification" %] le [% "date_modification" %]</mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
