<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyLocal="1" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" readOnly="0" simplifyDrawingHints="0" styleCategories="AllStyleCategories" simplifyDrawingTol="1" maxScale="1" version="3.36.2-Maidenhead" labelsEnabled="1" minScale="2500" simplifyMaxScale="1" symbologyReferenceScale="200">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal fixedDuration="0" enabled="0" durationUnit="min" accumulate="0" limitMode="0" durationField="fid" startField="date_pose" startExpression="" mode="0" endField="" endExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation type="IndividualFeatures" respectLayerSymbol="1" showMarkerSymbolInSurfacePlots="0" binding="Centroid" clamping="Terrain" extrusion="0" zscale="1" zoffset="0" symbology="Line" extrusionEnabled="0">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol type="line" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleLine" id="{59ecb17d-0369-4d74-8477-0e3ea8bf825a}">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="125,139,143,255,rgb:0.49019607843137253,0.54509803921568623,0.5607843137254902,1" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol type="fill" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleFill" id="{1f8eb620-ce9d-47a4-8d02-0b517439d087}">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="125,139,143,255,rgb:0.49019607843137253,0.54509803921568623,0.5607843137254902,1" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="89,99,102,255,rgb:0.34901960784313724,0.38823529411764707,0.40000000000000002,1" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker" id="{c590bf5f-0b16-438a-9dd1-b4351801573d}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="125,139,143,255,rgb:0.49019607843137253,0.54509803921568623,0.5607843137254902,1" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="89,99,102,255,rgb:0.34901960784313724,0.38823529411764707,0.40000000000000002,1" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 type="RuleRenderer" symbollevels="0" referencescale="200" enableorderby="0" forceraster="0">
    <rules key="{32f5279c-1000-4b8e-af20-e49e291fdfd7}">
      <rule filter=" &quot;etat&quot; = 'Actif' " label="Actif" key="{16dd0af1-73d5-4cf1-9ec0-a2837f04135c}">
        <rule filter="&quot;cellule&quot; ='FOYER MAT'" label="candélabre" symbol="0" key="{829e0bbb-fcaa-4fbb-9c1b-f726264fe091}"/>
        <rule filter=" &quot;cellule&quot; = 'FOYER SIMPLE'" label="lanterne poteau, façade" symbol="1" key="{02aba51b-f029-490a-8e61-f6559c301a96}" description="Foyer imple"/>
        <rule filter=" &quot;cellule&quot; = 'PROJECTEUR'" label="projecteur" symbol="2" key="{8392ffdc-cc56-40cb-ab2f-4a0b1efb3642}"/>
      </rule>
      <rule filter=" &quot;etat&quot; = 'A poser' " label="A poser" key="{b2b3fe69-7342-4bdb-ac8b-b3f3018b7217}">
        <rule filter=" &quot;cellule&quot; =  'FOYER A POSER'" label="Foyer mat" symbol="3" key="{6e022597-9947-47a3-b9fb-25f48bb4094f}"/>
      </rule>
      <rule filter=" &quot;etat&quot; = 'A remplacer' " label="A remplacer" key="{718e6c05-e079-466d-9763-23eb11f8b846}">
        <rule filter=" &quot;cellule&quot; =  'FOYER MAT' " label="Foyer mat" symbol="4" key="{5ae660dc-d2b6-4bca-88a5-6dcca3eb57ce}"/>
        <rule filter=" &quot;cellule&quot; =   'FOYER SIMPLE'" label="Foyer simple" symbol="5" key="{8e7a252e-44be-4dec-84c4-bf647712ccc7}"/>
        <rule filter=" &quot;cellule&quot; =  'PROJECTEUR'" label="Projecteur" symbol="6" key="{7d7698df-cb4c-411c-9f39-15bab93dfd26}"/>
      </rule>
      <rule filter=" &quot;etat&quot; = 'A déposer' " label="A déposer" key="{f260b6a6-ee19-4164-a1db-7a45fa384cba}">
        <rule filter=" &quot;cellule&quot; =  'FOYER MAT'" label="Foyer mat" symbol="7" key="{939bf04c-03fa-4074-9b5a-3b8cbe1c20c2}"/>
        <rule filter=" &quot;cellule&quot; =   'FOYER SIMPLE'" label="Foyer simple" symbol="8" key="{e9672935-b3d2-4192-8ad3-8e50ad504383}"/>
        <rule filter=" &quot;cellule&quot; =  'PROJECTEUR'" label="Projecteur" symbol="9" key="{cc43d88d-436a-4a17-893c-3ffd96a6823b}"/>
      </rule>
      <rule filter=" &quot;etat&quot; = 'Déposer' " label="Déposé" key="{adaeb54d-d925-45de-aeba-30a8c3b6dea9}">
        <rule filter=" &quot;cellule&quot; =  'FOYER MAT'" label="Foyer mat" symbol="10" key="{7c95832e-ab34-440e-b335-1c32e76269eb}"/>
        <rule filter=" &quot;cellule&quot; =   'FOYER SIMPLE' " label="Foyer simple" symbol="11" key="{ab463738-3be0-4c50-b065-7639d340a048}"/>
        <rule filter=" &quot;cellule&quot; =  'PROJECTEUR' " label="Projecteur" symbol="12" key="{a29e2380-c632-48cb-8334-33d0bde30de5}"/>
      </rule>
    </rules>
    <symbols>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="0" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{9aa87f35-5004-4118-b4a2-a8210718ee3a}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="251,154,153,255,rgb:0.98431372549019602,0.60392156862745094,0.59999999999999998,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAxMzAwIDEzMDAiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1mb3llcl9tYXQuc3ZnIgogICB3aWR0aD0iMTMwY20iCiAgIGhlaWdodD0iMTMwY20iCiAgIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXciPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE2Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MTY5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNTE2NyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJUYWlsIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iVGFpbCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDg2NyIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgtMS4yKSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg1NSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsLTMuOTU4NTIyNyAwLjU0MzUyMDk0LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTciCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLC0zLjk1ODUyMjcgMy4wNjE3MDUzLDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTkiCiAgICAgICAgICAgZD0iTSAxLjMwNTM1ODIsLTMuOTU4NTIyNyA1LjY1Mzc0NjYsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsNC4xNzc1ODM4IDAuNTQzNTIwOTQsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MyIKICAgICAgICAgICBkPSJNIC0xLjI4NjY4MzIsNC4xNzc1ODM4IDMuMDYxNzA1MywwLjIxOTc0MjI2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODAwMDAwMDE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0ODY1IgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLDQuMTc3NTgzOCA1LjY1Mzc0NjYsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4MTUiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODEzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDgwOSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4MDciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTEzIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTIxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTA4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjAuNzY0MTc5MTE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTAwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5MiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwOTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDg2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDc7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODAiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OC0yLTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg0NzIiPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICAgIG1vZGU9InNjcmVlbiIKICAgICAgICAgaW4yPSJCYWNrZ3JvdW5kSW1hZ2UiCiAgICAgICAgIGlkPSJmZUJsZW5kODQ3NCIgLz4KICAgIDwvZmlsdGVyPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJJbiBhbmQgb3V0IGdsb3cgd2l0aCBhIHBvc3NpYmxlIG9mZnNldCBhbmQgY29sb3JpemFibGUgZmxvb2QiCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJDdXRvdXQgR2xvdyIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlkPSJmaWx0ZXI4NDc5Ij4KICAgICAgPGZlT2Zmc2V0CiAgICAgICAgIGR5PSIzIgogICAgICAgICBkeD0iMyIKICAgICAgICAgaWQ9ImZlT2Zmc2V0ODQ4MSIgLz4KICAgICAgPGZlR2F1c3NpYW5CbHVyCiAgICAgICAgIHN0ZERldmlhdGlvbj0iMyIKICAgICAgICAgcmVzdWx0PSJibHVyIgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NDgzIiAvPgogICAgICA8ZmVGbG9vZAogICAgICAgICBmbG9vZC1jb2xvcj0icmdiKDAsMCwwKSIKICAgICAgICAgZmxvb2Qtb3BhY2l0eT0iMSIKICAgICAgICAgcmVzdWx0PSJmbG9vZCIKICAgICAgICAgaWQ9ImZlRmxvb2Q4NDg1IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49ImZsb29kIgogICAgICAgICBpbjI9IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIG9wZXJhdG9yPSJpbiIKICAgICAgICAgcmVzdWx0PSJjb21wb3NpdGUiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg0ODciIC8+CiAgICAgIDxmZUJsZW5kCiAgICAgICAgIGluPSJibHVyIgogICAgICAgICBpbjI9ImNvbXBvc2l0ZSIKICAgICAgICAgbW9kZT0ibm9ybWFsIgogICAgICAgICBpZD0iZmVCbGVuZDg0ODkiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIGlua3NjYXBlOm1lbnUtdG9vbHRpcD0iSW4gYW5kIG91dCBnbG93IHdpdGggYSBwb3NzaWJsZSBvZmZzZXQgYW5kIGNvbG9yaXphYmxlIGZsb29kIgogICAgICAgaW5rc2NhcGU6bWVudT0iU2hhZG93cyBhbmQgR2xvd3MiCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iQ3V0b3V0IEdsb3ciCiAgICAgICBzdHlsZT0iY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOnNSR0IiCiAgICAgICBpZD0iZmlsdGVyODQ5MSI+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeT0iMyIKICAgICAgICAgZHg9IjMiCiAgICAgICAgIGlkPSJmZU9mZnNldDg0OTMiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBzdGREZXZpYXRpb249IjMiCiAgICAgICAgIHJlc3VsdD0iYmx1ciIKICAgICAgICAgaWQ9ImZlR2F1c3NpYW5CbHVyODQ5NSIgLz4KICAgICAgPGZlRmxvb2QKICAgICAgICAgZmxvb2QtY29sb3I9InJnYigwLDAsMCkiCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjEiCiAgICAgICAgIHJlc3VsdD0iZmxvb2QiCiAgICAgICAgIGlkPSJmZUZsb29kODQ5NyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGluPSJmbG9vZCIKICAgICAgICAgaW4yPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iaW4iCiAgICAgICAgIHJlc3VsdD0iY29tcG9zaXRlIgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NDk5IiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0iYmx1ciIKICAgICAgICAgaW4yPSJjb21wb3NpdGUiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaWQ9ImZlQmxlbmQ4NTAxIiAvPgogICAgPC9maWx0ZXI+CiAgICA8ZmlsdGVyCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg1MDMiPgogICAgICA8ZmVHYXVzc2lhbkJsdXIKICAgICAgICAgc3RkRGV2aWF0aW9uPSI1IgogICAgICAgICByZXN1bHQ9InJlc3VsdDYiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1MDUiIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICByZXN1bHQ9InJlc3VsdDgiCiAgICAgICAgIGluPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NTA3IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ5IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgaW4yPSJTb3VyY2VBbHBoYSIKICAgICAgICAgaW49InJlc3VsdDgiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1MDkiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIKICAgICAgICAgaWQ9ImZlQ29sb3JNYXRyaXg4NTExIiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0icmVzdWx0MTAiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVCbGVuZDg1MTMiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgaWQ9ImZpbHRlcjg1NzUiCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyI+CiAgICAgIDxmZUZsb29kCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjAuMzI1NDkiCiAgICAgICAgIGZsb29kLWNvbG9yPSJyZ2IoMCwwLDApIgogICAgICAgICByZXN1bHQ9ImZsb29kIgogICAgICAgICBpZD0iZmVGbG9vZDg1NzciIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICBpbj0iZmxvb2QiCiAgICAgICAgIGluMj0iU291cmNlR3JhcGhpYyIKICAgICAgICAgb3BlcmF0b3I9ImluIgogICAgICAgICByZXN1bHQ9ImNvbXBvc2l0ZTEiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1NzkiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpbj0iY29tcG9zaXRlMSIKICAgICAgICAgc3RkRGV2aWF0aW9uPSIzIgogICAgICAgICByZXN1bHQ9ImJsdXIiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1ODEiIC8+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeD0iNiIKICAgICAgICAgZHk9IjYiCiAgICAgICAgIHJlc3VsdD0ib2Zmc2V0IgogICAgICAgICBpZD0iZmVPZmZzZXQ4NTgzIiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGluMj0ib2Zmc2V0IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgcmVzdWx0PSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1ODUiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHJlc3VsdD0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIGluPSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIHZhbHVlcz0iMCAwIDAgLTEgMCAwIDAgMCAtMSAwIDAgMCAwIC0xIDAgMCAwIDAgMSAwIgogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MjciIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NjI5IgogICAgICAgICBzdGREZXZpYXRpb249IjUiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0NiIKICAgICAgICAgaW49ImZiU291cmNlR3JhcGhpYyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzEiCiAgICAgICAgIGluMj0icmVzdWx0NiIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ4IgogICAgICAgICBpbj0iZmJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzMiCiAgICAgICAgIGluMj0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0OSIKICAgICAgICAgb3BlcmF0b3I9Im92ZXIiCiAgICAgICAgIGluPSJyZXN1bHQ4IiAvPgogICAgICA8ZmVDb2xvck1hdHJpeAogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MzUiCiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIgLz4KICAgICAgPGZlQmxlbmQKICAgICAgICAgaWQ9ImZlQmxlbmQ4NjM3IgogICAgICAgICBpbjI9InJlc3VsdDYiCiAgICAgICAgIGluPSJyZXN1bHQxMCIKICAgICAgICAgbW9kZT0ibm9ybWFsIiAvPgogICAgPC9maWx0ZXI+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzEyIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDYyNSIKICAgICBpbmtzY2FwZTpjeD0iNDIzNy4xODIyIgogICAgIGlua3NjYXBlOmN5PSIxNzk0LjM0NSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSI1NjE4LjY0LDIzODEuMzYiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjUxMzguOSwyNTYwIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODA3NiIKICAgICAgIGVuYWJsZWQ9InRydWUiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzg0NzYiCiAgICAgc3R5bGU9ImZpbHRlcjp1cmwoI2ZpbHRlcjg0OTEpIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM4NS40MTMxNiwyODU2LjQ4OTIpIiAvPgogIDxnCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIGlua3NjYXBlOmxhYmVsPSJMYXllciAxIgogICAgIHN0eWxlPSJkaXNwbGF5Om5vbmU7ZmlsdGVyOnVybCgjZmlsdGVyODQ3MikiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwzMDE3LjI4MzUpIiAvPgogIDxnCiAgICAgaWQ9Imc5NDciCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDA4NjcsMCwwLDAuOTkzMTA3ODgsMC4xMzY0ODYwNCwxMy4xOTM0OTMpIj4KICAgIDxwYXRoCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjY3NS44MjE5MiIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTMzNy45MDU3NCIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5OTk3MTcsLTAuMDAyMzc5NTcsMS45MzAxMjU2ZS00LDAuOTk5OTk5OTgsMCwwKSIKICAgICAgIGQ9Im0gMTE2MC40Njg0LDEyNTkuMDU0NiBhIDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgLTIxMS41OTAzNSw0MS44MDcyIDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgLTIxMS43NDM2NiwtNDEuNzc3IDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgMjExLjQzNzAzLC00MS44Mzc1IDIxMS42NjcwNSw0MS44MDcyNzQgMCAwIDEgMjExLjg5Njc4LDQxLjc0NjciCiAgICAgICBzb2RpcG9kaTpvcGVuPSJ0cnVlIgogICAgICAgc29kaXBvZGk6ZW5kPSI2LjI4MTczNjciCiAgICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICAgIHNvZGlwb2RpOnJ5PSI0MS44MDcyNzQiCiAgICAgICBzb2RpcG9kaTpyeD0iMjExLjY2NzA1IgogICAgICAgc29kaXBvZGk6Y3k9IjEyNTkuMDU0NiIKICAgICAgIHNvZGlwb2RpOmN4PSI5NDguODAxMzkiCiAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICBpZD0icGF0aDUxNjEiCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45ODgyMDc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6NTkuNjI2MDk0ODI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5OjE3OC44NzgyODc0OCwgNTkuNjI2MDk1ODM7c3Ryb2tlLWRhc2hvZmZzZXQ6MTEzLjM4NTgyNjExO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIgLz4KICAgIDxlbGxpcHNlCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjYwLjI1MSIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTM4LjE2NDU3OCIKICAgICAgIHJ5PSI2MzEuNTAwMDYiCiAgICAgICByeD0iNjM5LjQ0NzM5IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTk5OTcxNywtMC4wMDIzNzk1NywxLjkzMDEyNTZlLTQsMC45OTk5OTk5OCwwLDApIgogICAgICAgY3k9IjY0Mi43NzA2OSIKICAgICAgIGN4PSI2NDkuMTc4MzQiCiAgICAgICBpZD0icGF0aDQ2NDciCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO2ZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTkuNTg0NzYwNjc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5OjU4Ljc1NDI3NjA0LCAxOS41ODQ3NTg2ODtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgPGVsbGlwc2UKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iNjAuMjUxIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItMzguMTY0NTc4IgogICAgICAgcnk9IjQ4NC4wOTgzIgogICAgICAgcng9IjQ5MC4xOTA2MSIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5OTk3MTcsLTAuMDAyMzc5NTcsMS45MzAxMjU2ZS00LDAuOTk5OTk5OTgsMCwwKSIKICAgICAgIGN5PSI2NDIuNzcwNjkiCiAgICAgICBjeD0iNjQ5LjE3ODM0IgogICAgICAgaWQ9InBhdGg5NDAiCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO2ZpbGw6I2ZmZmYwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTkuNDk4NDAxNjQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIgLz4KICA8L2c+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="253,200,7,255,rgb:0.99215686274509807,0.78431372549019607,0.02745098039215686,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="name">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="@project_folder || '/svg/smdev=' || replace( lower(&quot;cellule&quot;), ' ','_')   || '.svg'" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{247bdfd4-90fd-4673-b30f-fa3d50a4202d}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="103,99,139,255,rgb:0.40392156862745099,0.38823529411764707,0.54509803921568623,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHdpZHRoPSI4NGNtIgogICBoZWlnaHQ9IjY0MGNtIgogICB2ZXJzaW9uPSIxLjEiCiAgIHZpZXdCb3g9IjAgMCA4NDAuMDAwMDMgNjQwMC4wMDAyIgogICBpZD0ic3ZnMTgiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PWZveWVyX3NpbXBsZS5zdmciCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMyMiI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NzQiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0NzcyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc2MSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NTkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzY2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc2NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0NzYyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDc2NiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NjgiCiAgICAgICB4MT0iLTI2NC40MjMwNyIKICAgICAgIHkxPSI4MzkuODY5MzgiCiAgICAgICB4Mj0iMTUyNC4xNjIyIgogICAgICAgeTI9IjgzOS44NjkzOCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09InRyYW5zbGF0ZSgtMTA1NjEuMDMsLTQyODMuMzA5NikiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwMDAwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzIwIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb3RoZXJzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1taWRwb2ludHM9InRydWUiCiAgICAgc2hvd2d1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMTEzMTM3MDkiCiAgICAgaW5rc2NhcGU6Y3g9IjY4NS45Nzg4MiIKICAgICBpbmtzY2FwZTpjeT0iMjIzNDAuNjMxIgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnNDkxMiIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSIxMzkuNTY1LDEyNDEuMzkiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjE2NjI4LjIsMTI4My40OCIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wZXJwZW5kaWN1bGFyPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRhbmdlbnRpYWw9ImZhbHNlIgogICAgIHNjYWxlLXg9IjEwIgogICAgIGZpdC1tYXJnaW4tdG9wPSIwIgogICAgIGZpdC1tYXJnaW4tbGVmdD0iMCIKICAgICBmaXQtbWFyZ2luLXJpZ2h0PSIwIgogICAgIGZpdC1tYXJnaW4tYm90dG9tPSIwIgogICAgIGlua3NjYXBlOm9iamVjdC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgc2hvd2JvcmRlcj0idHJ1ZSIKICAgICB2aWV3Ym94LXdpZHRoPSI3OC4yNDIiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkNDc3MSIKICAgICAgIG9yaWdpbng9IjkuODMxMDkzMWUtMDUiCiAgICAgICBvcmlnaW55PSIxLjg4Nzg4NDllLTA3IiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTYzOC4yNjU0LC05NjMuNjg1NjQiCiAgICAgICBvcmllbnRhdGlvbj0iMCwxIgogICAgICAgaWQ9Imd1aWRlODc2IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGEyIj4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICAgIDxjYzpsaWNlbnNlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIiAvPgogICAgICAgIDxkYzpjcmVhdG9yPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+am1hPC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpjcmVhdG9yPgogICAgICAgIDxkYzpwdWJsaXNoZXI+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5BemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnB1Ymxpc2hlcj4KICAgICAgICA8ZGM6cmlnaHRzPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+KGMpQXppbXV0PC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpyaWdodHM+CiAgICAgIDwvY2M6V29yaz4KICAgICAgPGNjOkxpY2Vuc2UKICAgICAgICAgcmRmOmFib3V0PSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIj4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjUmVwcm9kdWN0aW9uIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEaXN0cmlidXRpb24iIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNOb3RpY2UiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNBdHRyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cHJvaGliaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNDb21tZXJjaWFsVXNlIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEZXJpdmF0aXZlV29ya3MiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNTaGFyZUFsaWtlIiAvPgogICAgICA8L2NjOkxpY2Vuc2U+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlkPSJnNDkxMCIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1NTE3Ljk1NjIsOTM3MC44NTY2KSIKICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii03ODkuMDYwODkiCiAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI0MzMuMjUxMTYiPgogICAgPGcKICAgICAgIGlkPSJnNDkxMiIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iNTcuMTA3MTgzIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSItMTguNzEyOTMyIgogICAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lIj4KICAgICAgPGcKICAgICAgICAgaWQ9Imc4NTEiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk2OTA2OSwwLDAsMC45OTk2ODU2NSwxMi45MzI1ODIsLTIwLjkzMzM5NSkiPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDAwMDAxLDAsMCwxLjAwMDA1MzYsLTIuNjM5MjUwNCwtMS44MjQ2MzkpIgogICAgICAgICAgIGlkPSJnODk3Ij4KICAgICAgICAgIDxnCiAgICAgICAgICAgICBpZD0iZzg4OCIKICAgICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDEuMDAyMTExNCwwLDAsMC45OTg2Mjk0OSwxMS42NDQ4NywtMTAuMzkxMDA2KSI+CiAgICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2NjY2NjY2NjIgogICAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgICBpZD0icGF0aDgiCiAgICAgICAgICAgICAgIGQ9Im0gLTU0ODUuMjM0NCwtODAyMi42MDk0IGggLTMwLjA4MiBsIDE3Ny42MTEzLDQ0MC40MzU2IDQyMy42MjUsMC4zNDE4IDE3OS41MzUyLC00NDIuODQ5OSAtMzAuMDgyMSwwLjA1MSAtMTY5LjY2MDEsNDEyLjY5OTMgLTM4My4wODIsLTAuMzA4NiB6IgogICAgICAgICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjY2NjY2NjY2NjIgogICAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgICBpZD0icGF0aDEwIgogICAgICAgICAgICAgICBkPSJtIC01NTAwLjI3NTQsLTkzMzIuOTMxNiBjIC04LjMwNjQsMCAtMTUuMDM5OSw2LjczNDYgLTE1LjA0MSwxNS4wNDEgdiAxMjk1LjI4MTIgaCAzMC4wODIgdiAtMTI4MC4yNDAyIGggNzE4LjQ3NjYgbCAyLjEzMDgsMTI3OC4yMTg3IDMwLjA4MjEsLTAuMDUxIC0yLjE1NDMsLTEyOTMuMjM0NCBjIC0wLjAxNSwtOC4yOTczIC02Ljc0NTcsLTE1LjAxNTYgLTE1LjA0MywtMTUuMDE1NiB6IgogICAgICAgICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNzY2NjY2NzY2NjY2NzYyIKICAgICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgICAgaWQ9InBhdGgxMiIKICAgICAgICAgICAgICAgZD0ibSAtNTEyNy4wNDMsLTkyMzIuOTQ1MyBjIC04NC44NzIzLDEuMjEwMyAtMTU2LjMwMTEsNzQuODA0IC0yMDcuOTUxMSwxNDQuOTkyMiAtNTEuNjUwMSw3MC4xODgyIC04My43NjU3LDEzOS44NTM1IC04My43NjU3LDEzOS44NTM1IGwgLTkuODU3NCwyMS4zNDc2IGggMjMuNTEzNyA1NzkuNDY2OCBsIC05LjExNzIsLTIxLjAyNTMgYyAwLDAgLTMwLjY2MTcsLTcwLjg1MDYgLTgxLjY0MjYsLTE0MS42OTkzIC01MC45ODA4LC03MC44NDg2IC0xMjIuOTU0NiwtMTQ0LjcxODkgLTIxMC42NDY1LC0xNDMuNDY4NyB6IG0gMC40Mjc4LDMwLjA4MDEgYyA2OS44NzIzLC0wLjk5NjIgMTM3LjAzNTUsNjMuMTg5NCAxODUuODAwNywxMzAuOTU5IDMyLjkwODUsNDUuNzMzMiA1Ni4xMTI4LDkwLjAwMDUgNjguMzg2OCwxMTUuMDcyMiBoIC01MDguMjExIGMgMTIuODQzLC0yNC44NTc4IDM2LjY4NzcsLTY4LjE5MDIgNjkuODc1LC0xMTMuMjg5IDQ5LjU0NDksLTY3LjMyNzQgMTE3LjI1MTgsLTEzMS43ODgzIDE4NC4xNDg1LC0xMzIuNzQyMiB6IgogICAgICAgICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjY2NjY2NjY2NjY2Njc3NzYyIKICAgICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgICAgaWQ9InBhdGg0Nzc5IgogICAgICAgICAgICAgICBkPSJtIC01MTI3LjAxMjQsLTg5MTguNzcxIGggMzE0LjEwOTkgbCAtMTIuMzAzLC0yNy41MjY0IGMgLTM5LjUyNTQsLTg4LjQzMDQgLTk0LjEwNjMsLTE2OS45MzE2IC0xNDguNjY4OSwtMjIxLjk5MzggLTM3LjY0MTgsLTM1LjkxNzQgLTc0LjQ2NDYsLTU3Ljc1NTEgLTExNC4xNzY4LC02Ny43MTMzIC0yMC4wODU2LC01LjAzNiAtNTUuNjc0NiwtNC42NjkgLTc1Ljg1MDgsMC43ODM2IC03MS41MTUsMTkuMzI3OCAtMTQxLjc2MjcsODIuODQ4NiAtMjA5LjQzODMsMTg5LjM4NDQgLTI0Ljk5NjcsMzkuMzUwMyAtNTMuMjE3Nyw5MS44NTIxIC02Ni4wNTE4LDEyMi44ODE1IGwgLTEuNzMwNSw0LjE4NCB6IG0gLTIzMC43NSwtNjMuMDcyNSBjIDc4Ljc1MjgsLTEzOS4wNTM3IDE2OS4wMzM4LC0yMTkuMjQwMiAyNDAuMDg0NiwtMjEzLjI0MSAzNi43NjYxLDMuMTAzNiA3Mi40MTA5LDIxLjkxNTYgMTEwLjQ3NDksNTguMzA0MyAzMC45NDg2LDI5LjU4NTUgNjMuMzkzNCw3MS40NDM5IDkxLjcwNjgsMTE4LjMxNDQgMTIuOTcyOSwyMS40NzU4IDI4LjUwMDUsNTAuNDQyOCAyOC41MDA1LDUzLjE2ODIgMCwxLjI3ODkgLTc4LjEzOTgsMS44OTY1IC0yMzkuOTAyNiwxLjg5NjUgLTEzMS45NDY0LDAgLTIzOS45MDI3LC0wLjU1ODYgLTIzOS45MDI3LC0xLjI0MTIgMCwtMC42ODM1IDQuMDY3MiwtOC40MjM1IDkuMDM4NSwtMTcuMjAxMiB6IgogICAgICAgICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45ODgyMDc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDgzMDgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0ic3NjY2NjY3MiCiAgICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICAgIGlkPSJwYXRoNDc4MSIKICAgICAgICAgICAgICAgZD0ibSAtNTEyNi44OTgyLC04OTU4LjI4NTUgYyAxMzcuNTIzMiwwIDI1MC4wNDIsLTAuNTkwMyAyNTAuMDQyLC0xLjMxMzEgMCwtMy4xODMxIC0zMC4xMDM1LC01Ni4xNDc4IC00NS4wMzQ3LC03OS4yMzQ0IC01OC4yMTY3LC05MC4wMTM4IC0xMjMuNjYwMywtMTQ4LjMwODcgLTE3OS4yNTk3LC0xNTkuNjc2NiAtNDYuMzA4OSwtOS40NjgxIC05My4zMTM2LDkuOTgwNCAtMTQ2LjAwMDUsNjAuNDA2OSAtNDAuMjcxMSwzOC41NDI4IC04Mi41Nzk3LDk1LjgxMiAtMTIwLjA3NTQsMTYyLjUzMzggbCAtOS43MTMzLDE3LjI4MzQgeiIKICAgICAgICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNmZmZmMDA7ZmlsbC1vcGFjaXR5OjAuOTg4MjA3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjMwLjA4MzA4MjI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgICAgICAgPC9nPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOnVybCgjbGluZWFyR3JhZGllbnQ0NzY4KTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MzguMjE2MzUwNTY7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICAgIGQ9Ik0gLTMxNDcuMjc5OCwtMzI5OS4zMDk1IEggLTU0ODEuMDI5NiIKICAgICAgICAgICAgIGlkPSJwYXRoNDc2MCIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDc2MiIKICAgICAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gLTMxNDcuMjc5OCwtMzI5OS4zMDk1IEggLTU0ODEuMDI5NiIKICAgICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTAuNjEzNzMzMzYsLTEuMDAzMTcwNCwwLC04NDM0Ljg0MDIsLTk1MTMuNTkzMykiIC8+CiAgICAgICAgPC9nPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDg3NCIKICAgICAgICAgICBkPSJtIC01MTI3LjcwOTgsLTYxNTEuODU2OSA0ZS00LDMxODEiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC4wNDk5OTc5NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eTowIiAvPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K" name="name"/>
            <Option type="QString" value="0.00000000000000006,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="rot_z" name="field"/>
                  <Option type="int" value="2" name="type"/>
                </Option>
                <Option type="Map" name="name">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="@project_folder || '/svg/smdev=' || replace( lower(&quot;cellule&quot;), ' ','_')   || '.svg'" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="10" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{8022e56b-68eb-4d18-baa3-8dbbcfbffa3a}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIHg9IjBweCIKICAgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgMTMwMCAxMzAwIgogICBpZD0ic3ZnMiIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9Zm95ZXJfbWF0X2RlcG9zZXIuc3ZnIgogICB3aWR0aD0iMTMwY20iCiAgIGhlaWdodD0iMTMwY20iCiAgIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXciPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE2Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ5NTgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2E1YTVhNTtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NTYiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ5NTIiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2NjY2NjYztzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NTAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ5NDYiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NDQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MTY5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNTE2NyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJUYWlsIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iVGFpbCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDg2NyIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgtMS4yKSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg1NSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsLTMuOTU4NTIyNyAwLjU0MzUyMDk0LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTciCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLC0zLjk1ODUyMjcgMy4wNjE3MDUzLDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTkiCiAgICAgICAgICAgZD0iTSAxLjMwNTM1ODIsLTMuOTU4NTIyNyA1LjY1Mzc0NjYsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsNC4xNzc1ODM4IDAuNTQzNTIwOTQsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MyIKICAgICAgICAgICBkPSJNIC0xLjI4NjY4MzIsNC4xNzc1ODM4IDMuMDYxNzA1MywwLjIxOTc0MjI2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODAwMDAwMDE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0ODY1IgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLDQuMTc3NTgzOCA1LjY1Mzc0NjYsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4MTUiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODEzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDgwOSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4MDciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTEzIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTIxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTA4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjAuNzY0MTc5MTE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTAwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5MiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwOTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDg2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDc7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODAiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OC0yLTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg0NzIiPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICAgIG1vZGU9InNjcmVlbiIKICAgICAgICAgaW4yPSJCYWNrZ3JvdW5kSW1hZ2UiCiAgICAgICAgIGlkPSJmZUJsZW5kODQ3NCIgLz4KICAgIDwvZmlsdGVyPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJJbiBhbmQgb3V0IGdsb3cgd2l0aCBhIHBvc3NpYmxlIG9mZnNldCBhbmQgY29sb3JpemFibGUgZmxvb2QiCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJDdXRvdXQgR2xvdyIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlkPSJmaWx0ZXI4NDc5Ij4KICAgICAgPGZlT2Zmc2V0CiAgICAgICAgIGR5PSIzIgogICAgICAgICBkeD0iMyIKICAgICAgICAgaWQ9ImZlT2Zmc2V0ODQ4MSIgLz4KICAgICAgPGZlR2F1c3NpYW5CbHVyCiAgICAgICAgIHN0ZERldmlhdGlvbj0iMyIKICAgICAgICAgcmVzdWx0PSJibHVyIgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NDgzIiAvPgogICAgICA8ZmVGbG9vZAogICAgICAgICBmbG9vZC1jb2xvcj0icmdiKDAsMCwwKSIKICAgICAgICAgZmxvb2Qtb3BhY2l0eT0iMSIKICAgICAgICAgcmVzdWx0PSJmbG9vZCIKICAgICAgICAgaWQ9ImZlRmxvb2Q4NDg1IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49ImZsb29kIgogICAgICAgICBpbjI9IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIG9wZXJhdG9yPSJpbiIKICAgICAgICAgcmVzdWx0PSJjb21wb3NpdGUiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg0ODciIC8+CiAgICAgIDxmZUJsZW5kCiAgICAgICAgIGluPSJibHVyIgogICAgICAgICBpbjI9ImNvbXBvc2l0ZSIKICAgICAgICAgbW9kZT0ibm9ybWFsIgogICAgICAgICBpZD0iZmVCbGVuZDg0ODkiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIGlua3NjYXBlOm1lbnUtdG9vbHRpcD0iSW4gYW5kIG91dCBnbG93IHdpdGggYSBwb3NzaWJsZSBvZmZzZXQgYW5kIGNvbG9yaXphYmxlIGZsb29kIgogICAgICAgaW5rc2NhcGU6bWVudT0iU2hhZG93cyBhbmQgR2xvd3MiCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iQ3V0b3V0IEdsb3ciCiAgICAgICBzdHlsZT0iY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOnNSR0IiCiAgICAgICBpZD0iZmlsdGVyODQ5MSI+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeT0iMyIKICAgICAgICAgZHg9IjMiCiAgICAgICAgIGlkPSJmZU9mZnNldDg0OTMiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBzdGREZXZpYXRpb249IjMiCiAgICAgICAgIHJlc3VsdD0iYmx1ciIKICAgICAgICAgaWQ9ImZlR2F1c3NpYW5CbHVyODQ5NSIgLz4KICAgICAgPGZlRmxvb2QKICAgICAgICAgZmxvb2QtY29sb3I9InJnYigwLDAsMCkiCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjEiCiAgICAgICAgIHJlc3VsdD0iZmxvb2QiCiAgICAgICAgIGlkPSJmZUZsb29kODQ5NyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGluPSJmbG9vZCIKICAgICAgICAgaW4yPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iaW4iCiAgICAgICAgIHJlc3VsdD0iY29tcG9zaXRlIgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NDk5IiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0iYmx1ciIKICAgICAgICAgaW4yPSJjb21wb3NpdGUiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaWQ9ImZlQmxlbmQ4NTAxIiAvPgogICAgPC9maWx0ZXI+CiAgICA8ZmlsdGVyCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg1MDMiPgogICAgICA8ZmVHYXVzc2lhbkJsdXIKICAgICAgICAgc3RkRGV2aWF0aW9uPSI1IgogICAgICAgICByZXN1bHQ9InJlc3VsdDYiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1MDUiIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICByZXN1bHQ9InJlc3VsdDgiCiAgICAgICAgIGluPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NTA3IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ5IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgaW4yPSJTb3VyY2VBbHBoYSIKICAgICAgICAgaW49InJlc3VsdDgiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1MDkiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIKICAgICAgICAgaWQ9ImZlQ29sb3JNYXRyaXg4NTExIiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0icmVzdWx0MTAiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVCbGVuZDg1MTMiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgaWQ9ImZpbHRlcjg1NzUiCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyI+CiAgICAgIDxmZUZsb29kCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjAuMzI1NDkiCiAgICAgICAgIGZsb29kLWNvbG9yPSJyZ2IoMCwwLDApIgogICAgICAgICByZXN1bHQ9ImZsb29kIgogICAgICAgICBpZD0iZmVGbG9vZDg1NzciIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICBpbj0iZmxvb2QiCiAgICAgICAgIGluMj0iU291cmNlR3JhcGhpYyIKICAgICAgICAgb3BlcmF0b3I9ImluIgogICAgICAgICByZXN1bHQ9ImNvbXBvc2l0ZTEiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1NzkiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpbj0iY29tcG9zaXRlMSIKICAgICAgICAgc3RkRGV2aWF0aW9uPSIzIgogICAgICAgICByZXN1bHQ9ImJsdXIiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1ODEiIC8+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeD0iNiIKICAgICAgICAgZHk9IjYiCiAgICAgICAgIHJlc3VsdD0ib2Zmc2V0IgogICAgICAgICBpZD0iZmVPZmZzZXQ4NTgzIiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGluMj0ib2Zmc2V0IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgcmVzdWx0PSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1ODUiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHJlc3VsdD0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIGluPSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIHZhbHVlcz0iMCAwIDAgLTEgMCAwIDAgMCAtMSAwIDAgMCAwIC0xIDAgMCAwIDAgMSAwIgogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MjciIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NjI5IgogICAgICAgICBzdGREZXZpYXRpb249IjUiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0NiIKICAgICAgICAgaW49ImZiU291cmNlR3JhcGhpYyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzEiCiAgICAgICAgIGluMj0icmVzdWx0NiIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ4IgogICAgICAgICBpbj0iZmJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzMiCiAgICAgICAgIGluMj0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0OSIKICAgICAgICAgb3BlcmF0b3I9Im92ZXIiCiAgICAgICAgIGluPSJyZXN1bHQ4IiAvPgogICAgICA8ZmVDb2xvck1hdHJpeAogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MzUiCiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIgLz4KICAgICAgPGZlQmxlbmQKICAgICAgICAgaWQ9ImZlQmxlbmQ4NjM3IgogICAgICAgICBpbjI9InJlc3VsdDYiCiAgICAgICAgIGluPSJyZXN1bHQxMCIKICAgICAgICAgbW9kZT0ibm9ybWFsIiAvPgogICAgPC9maWx0ZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDk1MiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDk1NCIKICAgICAgIHgxPSItMC4wNjE0MjMzMDIiCiAgICAgICB5MT0iNjQyLjc3MDY5IgogICAgICAgeDI9IjEyOTguNDE4MSIKICAgICAgIHkyPSI2NDIuNzcwNjkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIuNzAyMTcwNWUtNCwxLjM5OTk4NzIpIiAvPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjAzMjkzMDI5NSIKICAgICBpbmtzY2FwZTpjeD0iLTU4NC44MDQ1NiIKICAgICBpbmtzY2FwZTpjeT0iMzQzOC42NzI5IgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmcyIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6c25hcC1vYmplY3QtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjU2MTguNjQsMjM4MS4zNiIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iNTEzOC45LDI1NjAiCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNzYiCiAgICAgICBlbmFibGVkPSJ0cnVlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTMwMCwxMzAwIgogICAgICAgb3JpZW50YXRpb249Ii0wLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU5NDciCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwxMzAwIgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTk0OSIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzg0NzYiCiAgICAgc3R5bGU9ImZpbHRlcjp1cmwoI2ZpbHRlcjg0OTEpIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM4NS40MTMxNiwyODU2LjQ4OTIpIiAvPgogIDxnCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIGlua3NjYXBlOmxhYmVsPSJMYXllciAxIgogICAgIHN0eWxlPSJkaXNwbGF5Om5vbmU7ZmlsdGVyOnVybCgjZmlsdGVyODQ3MikiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwzMDE3LjI4MzUpIiAvPgogIDxnCiAgICAgaWQ9Imc5NDUiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDA4NjcsMCwwLDEuMDEzNDU3LDAuMTM2NDU5OTQsLTEuMjczNjkzMSkiPgogICAgPGVsbGlwc2UKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iNjAuMjUxIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItMzguMTY0NTc4IgogICAgICAgcnk9IjYzMS41MDAwNiIKICAgICAgIHJ4PSI2MzkuNDQ3MzkiCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5OTk5NzE3LC0wLjAwMjM3OTU3LDEuOTMwMTI1NmUtNCwwLjk5OTk5OTk4LDAsMCkiCiAgICAgICBjeT0iNjQ0LjE3MDY1IgogICAgICAgY3g9IjY0OS4xNzgxIgogICAgICAgaWQ9InBhdGg0NjQ3IgogICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7Y2xpcC1ydWxlOm5vbnplcm87ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2lzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbDtjb2xvci1pbnRlcnBvbGF0aW9uOnNSR0I7Y29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOmxpbmVhclJHQjtzb2xpZC1jb2xvcjojMDAwMDAwO3NvbGlkLW9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjA7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ5NTQpO3N0cm9rZS13aWR0aDoxOS41ODQ3NjA2NztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6NTguNzU0Mjc2MDQsIDE5LjU4NDc1ODY4O3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICA8ZWxsaXBzZQogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI2MC4yNTEiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii0zOC4xNjQ1NzgiCiAgICAgICByeT0iNDg0LjA5ODMiCiAgICAgICByeD0iNDkwLjE5MDYxIgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTk5OTcxNywtMC4wMDIzNzk1NywxLjkzMDEyNTZlLTQsMC45OTk5OTk5OCwwLDApIgogICAgICAgY3k9IjY0NC4xNzA2NSIKICAgICAgIGN4PSI2NDkuMTc4MSIKICAgICAgIGlkPSJwYXRoOTQwIgogICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7Y2xpcC1ydWxlOm5vbnplcm87ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2lzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbDtjb2xvci1pbnRlcnBvbGF0aW9uOnNSR0I7Y29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOmxpbmVhclJHQjtzb2xpZC1jb2xvcjojMDAwMDAwO3NvbGlkLW9wYWNpdHk6MTtmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiNiN2IyYjU7c3Ryb2tlLXdpZHRoOjE5LjQ5ODQwMTY0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgPC9nPgo8L3N2Zz4K" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="11" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{51a19d48-12dd-4d9a-955f-80a63a6a948a}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9IjgwY20iCiAgIGhlaWdodD0iNjQwY20iCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDgwMC4wMDAwMyA2NDAwLjAwMDIiCiAgIGlkPSJzdmcxOCIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9Zm95ZXJfc2ltcGxlX2RlcG9zZXIuc3ZnIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzMjIiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NDgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzE5ZjYwMjtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4NDYiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MzYiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2NjY2NjYztzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MzQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0Nzc0IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmMjAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc3MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NjEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0NzU5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc2NiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojY2NjOWNjO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NjQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc2MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwMDAwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzIwIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1nbG9iYWw9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6Z3VpZGUtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDEiCiAgICAgaW5rc2NhcGU6Y3g9Ijg0MzEuMjMxOCIKICAgICBpbmtzY2FwZTpjeT0iOTI4Ljg3MzcyIgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnNDkxMiIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSIxMzkuNTY1LDEyNDEuMzkiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjE2NjI4LjIsMTI4My40OCIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wZXJwZW5kaWN1bGFyPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRhbmdlbnRpYWw9ImZhbHNlIgogICAgIHNjYWxlLXg9IjEwIgogICAgIGZpdC1tYXJnaW4tdG9wPSIwIgogICAgIGZpdC1tYXJnaW4tbGVmdD0iMCIKICAgICBmaXQtbWFyZ2luLXJpZ2h0PSIwIgogICAgIGZpdC1tYXJnaW4tYm90dG9tPSIwIgogICAgIGlua3NjYXBlOm9iamVjdC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgc2hvd2JvcmRlcj0idHJ1ZSIKICAgICB2aWV3Ym94LXdpZHRoPSI3OC4yNDIiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkNDc3MSIKICAgICAgIG9yaWdpbng9IjkuODMxMDkzMWUtMDUiCiAgICAgICBvcmlnaW55PSIxLjg4Nzg4NDllLTA3IiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTYzOC4yNjU0LC05NjMuNjg1NjQiCiAgICAgICBvcmllbnRhdGlvbj0iMCwxIgogICAgICAgaWQ9Imd1aWRlODc2IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249Ii0zNy40MTc3MzMsODY2Mi4yMDUxIgogICAgICAgb3JpZW50YXRpb249IjAsMSIKICAgICAgIGlkPSJndWlkZTg2MSIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgICA8Y2M6bGljZW5zZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnktbmMtc2EvNC4wLyIgLz4KICAgICAgICA8ZGM6Y3JlYXRvcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPmptYTwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6Y3JlYXRvcj4KICAgICAgICA8ZGM6cHVibGlzaGVyPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+QXppbXV0PC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpwdWJsaXNoZXI+CiAgICAgICAgPGRjOnJpZ2h0cz4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPihjKUF6aW11dDwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cmlnaHRzPgogICAgICA8L2NjOldvcms+CiAgICAgIDxjYzpMaWNlbnNlCiAgICAgICAgIHJkZjphYm91dD0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnktbmMtc2EvNC4wLyI+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1JlcHJvZHVjdGlvbiIgLz4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjRGlzdHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjTm90aWNlIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjQXR0cmlidXRpb24iIC8+CiAgICAgICAgPGNjOnByb2hpYml0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjQ29tbWVyY2lhbFVzZSIgLz4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjRGVyaXZhdGl2ZVdvcmtzIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjU2hhcmVBbGlrZSIgLz4KICAgICAgPC9jYzpMaWNlbnNlPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpZD0iZzQ5MTAiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTUxNy45NTYyLDkzNzAuODU2NikiCiAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItNzg5LjA2MDg5IgogICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iNDMzLjI1MTE2Ij4KICAgIDxnCiAgICAgICBpZD0iZzQ5MTIiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9IjU3LjEwNzE4MyIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iLTE4LjcxMjkzMiIKICAgICAgIHN0eWxlPSJkaXNwbGF5OmlubGluZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnODgzIgogICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSg3OTAuOTYxNDcsLTIuMzg3OTk3NmUtNSkiPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjY2NjY2NjY2MiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDgiCiAgICAgICAgICAgZD0ibSAtNjI2Ny45MDM5LC04MDQyLjY3MDkgaCAtMzAuMDUyMyBsIDE3Ny40MzU4LDQzOS43MTczIDQyMy4yMDY0LDAuMzQxMyAxNzkuMzU3OCwtNDQyLjEyNzcgLTMwLjA1MjMsMC4wNTEgLTE2OS40OTI1LDQxMi4wMjYyIC0zODIuNzAzNSwtMC4zMDgxIHoiCiAgICAgICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7Zm9udC1zdHlsZTpub3JtYWw7Zm9udC12YXJpYW50Om5vcm1hbDtmb250LXdlaWdodDpub3JtYWw7Zm9udC1zdHJldGNoOm5vcm1hbDtmb250LXNpemU6bWVkaXVtO2xpbmUtaGVpZ2h0Om5vcm1hbDtmb250LWZhbWlseTpzYW5zLXNlcmlmO2ZvbnQtdmFyaWFudC1saWdhdHVyZXM6bm9ybWFsO2ZvbnQtdmFyaWFudC1wb3NpdGlvbjpub3JtYWw7Zm9udC12YXJpYW50LWNhcHM6bm9ybWFsO2ZvbnQtdmFyaWFudC1udW1lcmljOm5vcm1hbDtmb250LXZhcmlhbnQtYWx0ZXJuYXRlczpub3JtYWw7Zm9udC1mZWF0dXJlLXNldHRpbmdzOm5vcm1hbDt0ZXh0LWluZGVudDowO3RleHQtYWxpZ246c3RhcnQ7dGV4dC1kZWNvcmF0aW9uOm5vbmU7dGV4dC1kZWNvcmF0aW9uLWxpbmU6bm9uZTt0ZXh0LWRlY29yYXRpb24tc3R5bGU6c29saWQ7dGV4dC1kZWNvcmF0aW9uLWNvbG9yOiMwMDAwMDA7bGV0dGVyLXNwYWNpbmc6bm9ybWFsO3dvcmQtc3BhY2luZzpub3JtYWw7dGV4dC10cmFuc2Zvcm06bm9uZTt3cml0aW5nLW1vZGU6bHItdGI7ZGlyZWN0aW9uOmx0cjt0ZXh0LW9yaWVudGF0aW9uOm1peGVkO2RvbWluYW50LWJhc2VsaW5lOmF1dG87YmFzZWxpbmUtc2hpZnQ6YmFzZWxpbmU7dGV4dC1hbmNob3I6c3RhcnQ7d2hpdGUtc3BhY2U6bm9ybWFsO3NoYXBlLXBhZGRpbmc6MDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO3ZlY3Rvci1lZmZlY3Q6bm9uZTtmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiNjY2NjY2M7c3Ryb2tlLXdpZHRoOjAuOTk4Mjk1MzE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2NjY2NjY2NjY2MiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDEwIgogICAgICAgICAgIGQ9Im0gLTYyODIuOTMsLTkzNTAuODU2IGMgLTguMjk4MywwIC0xNS4wMjUxLDYuNzIzNSAtMTUuMDI2MiwxNS4wMTY0IHYgMTI5My4xNjg3IGggMzAuMDUyMyB2IC0xMjc4LjE1MjIgaCA3MTcuNzY2NyBsIDIuMTI4NywxMjc2LjEzNCAzMC4wNTIzLC0wLjA1MSAtMi4xNTIxLC0xMjkxLjEyNTMgYyAtMC4wMTUsLTguMjgzNiAtNi43MzkxLC0xNC45OTEgLTE1LjAyODIsLTE0Ljk5MSB6IgogICAgICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2ZvbnQtc3R5bGU6bm9ybWFsO2ZvbnQtdmFyaWFudDpub3JtYWw7Zm9udC13ZWlnaHQ6bm9ybWFsO2ZvbnQtc3RyZXRjaDpub3JtYWw7Zm9udC1zaXplOm1lZGl1bTtsaW5lLWhlaWdodDpub3JtYWw7Zm9udC1mYW1pbHk6c2Fucy1zZXJpZjtmb250LXZhcmlhbnQtbGlnYXR1cmVzOm5vcm1hbDtmb250LXZhcmlhbnQtcG9zaXRpb246bm9ybWFsO2ZvbnQtdmFyaWFudC1jYXBzOm5vcm1hbDtmb250LXZhcmlhbnQtbnVtZXJpYzpub3JtYWw7Zm9udC12YXJpYW50LWFsdGVybmF0ZXM6bm9ybWFsO2ZvbnQtZmVhdHVyZS1zZXR0aW5nczpub3JtYWw7dGV4dC1pbmRlbnQ6MDt0ZXh0LWFsaWduOnN0YXJ0O3RleHQtZGVjb3JhdGlvbjpub25lO3RleHQtZGVjb3JhdGlvbi1saW5lOm5vbmU7dGV4dC1kZWNvcmF0aW9uLXN0eWxlOnNvbGlkO3RleHQtZGVjb3JhdGlvbi1jb2xvcjojMDAwMDAwO2xldHRlci1zcGFjaW5nOm5vcm1hbDt3b3JkLXNwYWNpbmc6bm9ybWFsO3RleHQtdHJhbnNmb3JtOm5vbmU7d3JpdGluZy1tb2RlOmxyLXRiO2RpcmVjdGlvbjpsdHI7dGV4dC1vcmllbnRhdGlvbjptaXhlZDtkb21pbmFudC1iYXNlbGluZTphdXRvO2Jhc2VsaW5lLXNoaWZ0OmJhc2VsaW5lO3RleHQtYW5jaG9yOnN0YXJ0O3doaXRlLXNwYWNlOm5vcm1hbDtzaGFwZS1wYWRkaW5nOjA7Y2xpcC1ydWxlOm5vbnplcm87ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2lzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbDtjb2xvci1pbnRlcnBvbGF0aW9uOnNSR0I7Y29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOmxpbmVhclJHQjtzb2xpZC1jb2xvcjojMDAwMDAwO3NvbGlkLW9wYWNpdHk6MTt2ZWN0b3ItZWZmZWN0Om5vbmU7ZmlsbDojY2NjY2NjO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojY2NjY2NjO3N0cm9rZS13aWR0aDowLjk5ODI5NTMxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MC43MjM1Mjk0MTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNzY2NjY2NzY2NjY2NzYyIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIGlkPSJwYXRoMTIiCiAgICAgICAgICAgZD0ibSAtNTkxMC42Njk2LC05MjUyLjc4MjQgYyAtODQuNzg4NCwxLjIwODMgLTE1Ni4xNDY2LDc0LjY4MiAtMjA3Ljc0NTUsMTQ0Ljc1NTggLTUxLjU5OTIsNzAuMDczNyAtODMuNjgzLDEzOS42MjU0IC04My42ODMsMTM5LjYyNTQgbCAtOS44NDc3LDIxLjMxMjggaCAyMy40OTA1IDU3OC44OTQyIGwgLTkuMTA4MiwtMjAuOTkxIGMgMCwwIC0zMC42MzE0LC03MC43MzUxIC04MS41NjE5LC0xNDEuNDY4MiAtNTAuOTMwNSwtNzAuNzMzMSAtMTIyLjgzMzEsLTE0NC40ODI5IC0yMTAuNDM4NCwtMTQzLjIzNDggeiBtIDAuNDI3NSwzMC4wMzExIGMgNjkuODAzMiwtMC45OTQ2IDEzNi45LDYzLjA4NjMgMTg1LjYxNywxMzAuNzQ1NCAzMi44NzYsNDUuNjU4NiA1Ni4wNTc0LDg5Ljg1MzcgNjguMzE5MywxMTQuODg0NSBoIC01MDcuNzA4OSBjIDEyLjgzMDMsLTI0LjgxNzMgMzYuNjUxNCwtNjguMDc5IDY5LjgwNTksLTExMy4xMDQyIDQ5LjQ5NiwtNjcuMjE3NiAxMTcuMTM2LC0xMzEuNTczMyAxODMuOTY2NywtMTMyLjUyNTcgeiIKICAgICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6I2I3YjViNTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC45OTgyOTUzMTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjY2NjY2NjY2NjY2NjY3Nzc2MiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDQ3NzkiCiAgICAgICAgICAgZD0ibSAtNTkxMC4wMzU5LC04OTM3LjM3MDkgaCAzMTMuNzk5NiBsIC0xMi4yOTA5LC0yNy40ODE1IGMgLTM5LjQ4NjQsLTg4LjI4NjIgLTk0LjAxMzMsLTE2OS42NTQ1IC0xNDguNTIyLC0yMjEuNjMxNyAtMzcuNjA0NiwtMzUuODU4OSAtNzQuMzkxLC01Ny42NjEgLTExNC4wNjQsLTY3LjYwMjkgLTIwLjA2NTcsLTUuMDI3OCAtNTUuNjE5NiwtNC42NjE0IC03NS43NzU4LDAuNzgyMyAtNzEuNDQ0NCwxOS4yOTYzIC0xNDEuNjIyNiw4Mi43MTM1IC0yMDkuMjMxNCwxODkuMDc1NSAtMjQuOTcyLDM5LjI4NjIgLTUzLjE2NTEsOTEuNzAyMyAtNjUuOTg2NSwxMjIuNjgxMSBsIC0xLjcyODgsNC4xNzcyIHogbSAtMjMwLjUyMTksLTYyLjk2OTcgYyA3OC42NzQ5LC0xMzguODI2OCAxNjguODY2NywtMjE4Ljg4MjUgMjM5Ljg0NzMsLTIxMi44OTMxIDM2LjcyOTgsMy4wOTg1IDcyLjMzOTMsMjEuODc5OCAxMTAuMzY1Nyw1OC4yMDkyIDMwLjkxOCwyOS41MzcyIDYzLjMzMDgsNzEuMzI3MyA5MS42MTYyLDExOC4xMjE0IDEyLjk2MDEsMjEuNDQwOCAyOC40NzIzLDUwLjM2MDUgMjguNDcyMyw1My4wODE0IDAsMS4yNzY5IC03OC4wNjI1LDEuODkzNCAtMjM5LjY2NTQsMS44OTM0IC0xMzEuODE2MiwwIC0yMzkuNjY1NywtMC41NTc2IC0yMzkuNjY1NywtMS4yMzkxIDAsLTAuNjgyNCA0LjA2MzIsLTguNDA5NyA5LjAyOTYsLTE3LjE3MzIgeiIKICAgICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45ODgyMDc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC45OTgyOTUzMTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9InNzY2NjY2NzIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg0NzgxIgogICAgICAgICAgIGQ9Im0gLTU5MTAuMDMyNSwtODk3Ni40Mjk1IGMgMTQwLjEyODksMCAyNTQuNzc5NiwtMC42MTA5IDI1NC43Nzk2LC0xLjM1OTYgMCwtMy4yOTU3IC0zMC42NzM3LC01OC4xMzg4IC00NS44ODgzLC04Mi4wNDQyIC01OS4zMTk3LC05My4yMDU2IC0xMjYuMDAyNywtMTUzLjU2ODIgLTE4Mi42NTU4LC0xNjUuMzM4OSAtNDcuMTg2NCwtOS44MDQgLTk1LjA4MTYsMTAuMzM0MiAtMTQ4Ljc2NzIsNjIuNTQ4OSAtNDEuMDM0LDM5LjkwOTUgLTg0LjE0NDIsOTkuMjA5NSAtMTIyLjM1MDYsMTY4LjI5NzIgbCAtOS44OTcxLDE3Ljg5NjYgeiIKICAgICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MS4wMjY3NjUxMTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjYyIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0wLjYxMzU3MzMxLC0xLjAwMDA2NzYsMCwtOTIwOC40NTAyLC05NTMzLjg2OTkpIgogICAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gLTMxNDcuMjc5OCwtMzI5OS4zMDk1IGggLTYxOS4xOTk4IC0xNzE0LjU1IgogICAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0NzYyIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg0NzYwIgogICAgICAgICAgIGQ9Im0gLTMxNDcuMjc5OCwtMzI5OS4zMDk1IGggLTYxOS4xOTk4IC0xNzE0LjU1IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiNjY2NjY2M7c3Ryb2tlLXdpZHRoOjM4LjIxNjM1MDU2O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIGlkPSJwYXRoODc0IgogICAgICAgICAgIGQ9Im0gLTU5MDguOTE2OCwtNjE3MC44NTY1IDRlLTQsMzE4MCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjA0OTkxMjcxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjAiIC8+CiAgICAgIDwvZz4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="12" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{e1525e60-effd-4bfa-bc7f-f96c37886433}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIHg9IjBweCIKICAgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgNTI5MS4zMzg3IDEyMjgzLjQ2MyIKICAgaWQ9InN2ZzIiCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXByb2plY3RldXJfZGVwb3Nlci5zdmciCiAgIHdpZHRoPSIxNDBjbSIKICAgaGVpZ2h0PSIzMjVjbSI+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMTYiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMxNCI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDk3MSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojNGQ0ZDRkO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDk2OSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MjciCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0OTI1IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MjMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkxOSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTE1IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkwOSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ5MDciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkwNSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODgxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc4MSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NzkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc3NyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ3ODEiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzgzIgogICAgICAgeDE9Ijk1OS40OTk5NCIKICAgICAgIHkxPSIyNTk3Ljc5NTIiCiAgICAgICB4Mj0iOTYwLjQ5OTk0IgogICAgICAgeTI9IjI1OTcuNzk1MiIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0OTA5IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkxMSIKICAgICAgIHgxPSIyNzk5Ljg4ODkiCiAgICAgICB5MT0iMzE1Ny43OTUyIgogICAgICAgeDI9IjYzMjAuMTEwNCIKICAgICAgIHkyPSIzMTU3Ljc5NTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkyNyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MjkiCiAgICAgICB4MT0iMjU0MS4wNzMiCiAgICAgICB5MT0iNDE1Ny43OTQ5IgogICAgICAgeDI9IjY0MTguOTI2MyIKICAgICAgIHkyPSI0MTU3Ljc5NDkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkyNyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MjktOCIKICAgICAgIHgxPSIyNTQxLjA3MyIKICAgICAgIHkxPSI0MTU3Ljc5NDkiCiAgICAgICB4Mj0iNjQxOC45MjYzIgogICAgICAgeTI9IjQxNTcuNzk0OSIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkyMy0wIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTktMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTE1LTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0OTI3IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkxMS0yIgogICAgICAgeDE9IjI3OTkuODg4OSIKICAgICAgIHkxPSIzMTU3Ljc5NTIiCiAgICAgICB4Mj0iNjMyMC4xMTA0IgogICAgICAgeTI9IjMxNTcuNzk1MiIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkwNS01IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4ODEtOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzEyIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDgiCiAgICAgaW5rc2NhcGU6Y3g9IjI4NjguODAwNCIKICAgICBpbmtzY2FwZTpjeT0iODQwNS41ODM3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmcyIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0iZmFsc2UiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb3RoZXJzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSIxMjQ4MCwtNjQwIgogICAgIGlua3NjYXBlOm1lYXN1cmUtZW5kPSIxMjQ4MCw0NDgwIgogICAgIGlua3NjYXBlOmJib3gtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtZWRnZS1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODA3NiIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0NzgzKTtzdHJva2Utd2lkdGg6MC45ODI2NzcxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA4NDI5LjU4MTEsNDM3My4wMzI0IFYgMTgxMy4wMzI5IgogICAgIGlkPSJwYXRoNDc3NSIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzc3IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gODQyOS41ODExLDQzNzMuMDMyNCBWIDE4MTMuMDMyOSIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgaWQ9InBhdGg0ODEzIgogICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICBzb2RpcG9kaTpjeD0iLTI3NTAuNDQ4NSIKICAgICBzb2RpcG9kaTpjeT0iMTM3OC40MDk4IgogICAgIHNvZGlwb2RpOnJ4PSI5MjQuMDU3OTIiCiAgICAgc29kaXBvZGk6cnk9IjE1OS4zNjMzOSIKICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICBzb2RpcG9kaTplbmQ9IjAuNzQ0ODgwMzUiCiAgICAgZD0ibSAtMTgyNi4zOTA2LDEzNzguNDA5OCBhIDkyNC4wNTc5MiwxNTkuMzYzMzkgMCAwIDEgLTI0NC43MTkxLDEwOC4wMjk5IGwgLTY3OS4zMzg4LC0xMDguMDI5OSB6IgogICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiIC8+CiAgPGVsbGlwc2UKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEyLjkwNDgwMDQyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBpZD0icGF0aDQ4MzMiCiAgICAgY3g9Ii0yNTQ1LjE5MDkiCiAgICAgY3k9IjE1MjguNTY2IgogICAgIHJ4PSIyNTkuNjMzMTgiCiAgICAgcnk9IjMyLjI5MDQ5MyIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTApIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgaWQ9InBhdGg0ODM1IgogICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICBzb2RpcG9kaTpjeD0iLTMxNzMuMzY0NyIKICAgICBzb2RpcG9kaTpjeT0iMTUzMS42MzQ0IgogICAgIHNvZGlwb2RpOnJ4PSI1MDEuMTQxMTQiCiAgICAgc29kaXBvZGk6cnk9IjYuMTM4MzQxOSIKICAgICBzb2RpcG9kaTpzdGFydD0iNC43MTMxNzAxIgogICAgIHNvZGlwb2RpOmVuZD0iNS4wMDA3MDU2IgogICAgIGQ9Im0gLTMxNzIuOTczMywxNTI1LjQ5NjEgYSA1MDEuMTQxMTQsNi4xMzgzNDE5IDAgMCAxIDE0Mi4xMDI0LDAuMjUzMyBsIC0xNDIuNDkzOCw1Ljg4NSB6IgogICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6I2I3YjViNTtzdHJva2Utd2lkdGg6MTg4LjE3NDY4MjYyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBpZD0icGF0aDQ4NjEiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMSwtMSwwLDAsMCkiCiAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgIHNvZGlwb2RpOmN4PSItNjE0MS43MzE5IgogICAgIHNvZGlwb2RpOmN5PSItMjYzMC44NDMzIgogICAgIHNvZGlwb2RpOnJ4PSIyMjY5LjQ2MzYiCiAgICAgc29kaXBvZGk6cnk9IjE3OTkuNzU3NiIKICAgICBzb2RpcG9kaTpzdGFydD0iNC43MTY4NDc5IgogICAgIHNvZGlwb2RpOmVuZD0iMS41NzAxMjA5IgogICAgIGQ9Im0gLTYxMzEuNjEyNiwtNDQzMC41ODI5IGEgMjI2OS40NjM2LDE3OTkuNzU3NiAwIDAgMSAyMjU5LjM0MDIsMTgwMy4xNDQzIDIyNjkuNDYzNiwxNzk5Ljc1NzYgMCAwIDEgLTIyNjcuOTI2NywxNzk2LjM1MjUgbCAtMS41MzI4LC0xNzk5Ljc1NzIgeiIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTI1MTguOTE1LDUxMTYuMjQxNCB2IDAiCiAgICAgaWQ9InBhdGg0ODc5IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ4ODEiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAxMjUxOC45MTUsNTExNi4yNDE0IGMgMzEuODQ3LC05NS4wNzEgMzEuODQ3LC05NS4wNzEgMCwwIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NDkxMSk7c3Ryb2tlLXdpZHRoOjExMy4zODU4MTg0ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gODkxOC43NDczLDM1MTYuMjQxNCAzNTYyLjcwNzcsODAwIgogICAgIGlkPSJwYXRoNDkwMyIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTA1IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gODkxOC43NDczLDM1MTYuMjQxNCAzNTYyLjcwNzcsODAwIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEyNTE4LjkxNSw1NTk2LjI0MTQgLTg4MCwtNDAwIgogICAgIGlkPSJwYXRoNDkxMyIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE1IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCBjIC0yMDAuMzY3LC0yNjkuNzgyIC01NzAuMDIyLC0zMDQuNTg2IC04ODAsLTQwMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMjUxOC45MTUsNTU5Ni4yNDE0IC04MCwtODAiCiAgICAgaWQ9InBhdGg0OTE3IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MTkiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAxMjUxOC45MTUsNTU5Ni4yNDE0IGMgLTE3LjEzNiwtMzMuNTk1IC01My4zMzQsLTUzLjMzMyAtODAsLTgwIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NDkyOSk7c3Ryb2tlLXdpZHRoOjExMy4zODU4MTg0ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gODY3OC45MTQ4LDQyMzYuMjQxNCAzODQwLjAwMDIsMTM2MCIKICAgICBpZD0icGF0aDQ5MjEiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkyMyIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDg2NzguOTE0OCw0MjM2LjI0MTQgMzg0MC4wMDAyLDEzNjAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTI1MTEuNzk2LDk1OC43NzMyOSB2IDAiCiAgICAgaWQ9InBhdGg0ODc5LTQiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDg4MS04IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDk1OC43NzMyOSBjIDMxLjg0Nyw5NS4wNzE2MSAzMS44NDcsOTUuMDcxNjEgMCwwIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NDkxMS0yKTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA4OTExLjYyODgsMjU1OC43NzMyIDEyNDc0LjMzNiwxNzU4Ljc3MzMiCiAgICAgaWQ9InBhdGg0OTAzLTkiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkwNS01IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gODkxMS42Mjg4LDI1NTguNzczMiAxMjQ3NC4zMzYsMTc1OC43NzMzIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEyNTExLjc5Niw0NzguNzczNDkgLTg4MCwzOTkuOTk5OCIKICAgICBpZD0icGF0aDQ5MTMtOSIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE1LTkiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IGMgLTIwMC4zNjcsMjY5Ljc4MTggLTU3MC4wMjIsMzA0LjU4NjIgLTg4MCwzOTkuOTk5OCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IC04MCw4MCIKICAgICBpZD0icGF0aDQ5MTctNCIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE5LTEiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IGMgLTE3LjEzNSwzMy41OTQ3IC01My4zMzMsNTMuMzMzNCAtODAsODAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTI5LTgpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDg2NzEuNzk2MywxODM4Ljc3MzMgMTI1MTEuNzk2LDQ3OC43NzM0OSIKICAgICBpZD0icGF0aDQ5MjEtMiIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTIzLTAiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSA4NjcxLjc5NjMsMTgzOC43NzMzIDEyNTExLjc5Niw0NzguNzczNDkiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTIuOTA0ODAwNDI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGQ9Im0gMzYwNS44MTgzLDYwNTcuMTIwNSBjIC0zOTEuMDY4LDguNzg5MyAtMTE3Mi4yNzIsMTUuOTgwNSAtMTczNi4wMTA3LDE1Ljk4MDUgSCA4NDQuODI5IGwgMTUuMzcyMywtMTg3LjI5MTkgYyA1My4xODU5LC02NDguMDAxMyAzNjcuNDQ4MSwtMTI4MS40MTI0IDgwMS44NTk4LC0xNjE2LjE4ODUgMzA1LjEwNjYsLTIzNS4xMjggNTc2LjI2NzQsLTMzMC43NTAxIDkzMy45NTQyLC0zMjkuMzUgODIzLjcwNCwzLjIyNDcgMTUzNC4yNTQsNzU2Ljk3OTIgMTY4NS45MDgsMTc4OC40MTgxIDE5LjA1OCwxMjkuNjE0MiAzNC43MTMsMjUyLjkzOTcgMzQuNzg4LDI3NC4wNTY5IDAuMTE5LDMzLjg4NDUgLTgzLjM4NCw0MC4yNzE3IC03MTAuODkzLDU0LjM3NDkgeiIKICAgICBpZD0icGF0aDUxMzciCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICA8cGF0aAogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiNiN2I1YjU7c3Ryb2tlLXdpZHRoOjc1LjI2OTg3NDU3O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBzdHJva2UgZmlsbCIKICAgICBkPSJtIDk0LjQ4ODIsMTM2LjM3ODEzIGMgLTAuNTgxNyw0Ny4xMjIwMyAxMjkwLjEwNjgsMzgxNC40NzY2NyAxMzE2LjA4ODIsMzg0MS4xMTIzNyA2LjEyODksNi4yODMyIDI3LjQ2NjUsNS4wMDEgNDcuNDE3LC0yLjg0NzQgMzAuODM0NywtMTIuMTMwMyAyMi43MjEyLC01My4zNDAyIC01NC4xMTg4LC0yNzQuODU5MSBDIDEzNTQuMTU4NCwzNTU2LjQ1OTEgMTA1NS4zNjEyLDI2OTEuNTM1OCA3MzkuODgwOSwxNzc3LjczMzYgMzg4LjU4MDQsNzYwLjE3Njg2IDE1NC42ODUxLDExNi4yNzUzIDEzNi4zNjE3LDExNi4yNzUzIGMgLTE2LjQ1NTEsMCAtMzUuMjY4LDguODcyMyAtNDEuODA2NCwxOS43MTg3IC0wLjA0MiwwLjA3MDUgLTAuMDY0LDAuMTk5NjIgLTAuMDY3LDAuMzg0MTMgeiBtIDEyMzcuODI4OCwzNS4yNTYzMiBjIC0zLjU1MDEsMy42Mzg2NiA3NjMuMzQ3OCwzNTMwLjIxODI1IDc3Ny4xMDA1LDM1NzMuNDk1NTUgMi4zMDIzLDcuMjQ1MyAxNi42NTEzLDYuMzgyOSA1My45MjY2LC0zLjI0MzIgMzkuNzM5MiwtMTAuMjYyNCA0OS45MTYzLC0xNi4xMjc0IDQ2Ljc4MzEsLTI2Ljk1OTkgLTMuOTc0NiwtMTMuNzQxMiAtNzY4LjcyOTcsLTM1MDUuMTg1ODkgLTc3Ny4wNzc0LC0zNTQ3LjcwMTM4IGwgLTQuMzU2NSwtMjIuMTg2ODEgLTQ1Ljg4MjMsMTAuOTMzMSBjIC0yNS4yMzU1LDYuMDE0MDEgLTQ3Ljk1NzYsMTMuMDYxNDUgLTUwLjQ5NCwxNS42NjI2NCB6IgogICAgIGlkPSJwYXRoNDg4MyIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojY2NjY2NjO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojYjdiNWI1O3N0cm9rZS13aWR0aDo3NS4yNjk4NzQ1NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGQ9Im0gMzU0Ni40NzczLDE5NjYuNjA3OSBjIC0yMTQuNzk2LDk3Ny40NTcgLTM5MC42NDIsMTc3Ny44MDk0IC0zOTAuNzcsMTc3OC41NjA0IC0wLjQ1NywyLjcwMTIgLTkyLjc2NCwtMjAuNzU0MiAtOTYuNDE4LC0yNC41MDAyIC00LjM4MywtNC40OTQyIDc3NS4wMTksLTM1NjcuODUzMDEgNzgxLjkzNywtMzU3NC45NDQ4MSAyLjU3MywtMi42Mzg1IDI1LjYwMywtMC4zNDM4IDUxLjE3OSw1LjA5OTU2IDM0LjkwMiw3LjQyNzUzIDQ2LjI2NCwxMy40NzM4MSA0NS41NTYsMjQuMjQyNTYgLTAuNTE5LDcuODkwMzEgLTE3Ni42ODcsODE0LjA4NDQ1IC0zOTEuNDg0LDE3OTEuNTQyNDkgeiIKICAgICBpZD0icGF0aDQ4ODgiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojY2NjY2NjO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojYjdiNWI1O3N0cm9rZS13aWR0aDo3NS4yNjk4NzQ1NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGQ9Im0gNDU1OS41OTIzLDE5NzkuNDI0MyBjIC0zNTAuMTkxLDEwMTIuNzAxOCAtNjUwLjI2OSwxODgwLjMzNTYgLTY2Ni44NCwxOTI4LjA3NDMgbCAtMzAuMTI4LDg2Ljc5NzggLTQ5LjAwNCwtMTYuOTY1NyAtNDkuMDA0LC0xNi45NjU3IDM3LjIxNiwtMTA4LjI4MSBjIDIwLjQ2OSwtNTkuNTU0NyAzMjAuODQ5LC05MjkuMzc3MiA2NjcuNTExLC0xOTMyLjkzOSBsIDYzMC4yOTQsLTE4MjQuNjU3MjYgNDguODc3LDE3LjU1MzA1IGMgMjYuODgyLDkuNjU0NjkgNDguNjMyLDE5LjQ3ODc1IDQ4LjMzMywyMS44Mjk5MSAtMC4zLDIuMzUyMTYgLTI4Ny4wNjQsODMyLjg1MDM2IC02MzcuMjU1LDE4NDUuNTUzNiB6IgogICAgIGlkPSJwYXRoNDg5MCIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEyLjg1MzU2OTAzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAyNjMwLjY4NjQsNjE0MS43MzE4IC0xNi41OTQxLDYwNDcuMjQ0MiIKICAgICBpZD0icGF0aDEwMTMiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KPC9zdmc+Cg==" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="2" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{e8bfee70-2c2b-4678-9816-07112e059c57}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="103,99,139,255,rgb:0.40392156862745099,0.38823529411764707,0.54509803921568623,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIHg9IjBweCIKICAgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgNTI5MS4zMzg3IDEyMjgzLjQ2MyIKICAgaWQ9InN2ZzIiCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXByb2plY3RldXIuc3ZnIgogICB3aWR0aD0iMTQwY20iCiAgIGhlaWdodD0iMzI1Y20iPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE2Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ5NzEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzRkNGQ0ZDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NjkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTI3IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDkyNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTIzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkxNSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MDkiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0OTA3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg4MSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3ODEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0Nzc5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3NzciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0NzgxIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc4MyIKICAgICAgIHgxPSI5NTkuNDk5OTQiCiAgICAgICB5MT0iMjU5Ny43OTUyIgogICAgICAgeDI9Ijk2MC40OTk5NCIKICAgICAgIHkyPSIyNTk3Ljc5NTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkwOSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MTEiCiAgICAgICB4MT0iMjc5OS44ODg5IgogICAgICAgeTE9IjMxNTcuNzk1MiIKICAgICAgIHgyPSI2MzIwLjExMDQiCiAgICAgICB5Mj0iMzE1Ny43OTUyIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ5MjciCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTI5IgogICAgICAgeDE9IjI1NDEuMDczIgogICAgICAgeTE9IjQxNTcuNzk0OSIKICAgICAgIHgyPSI2NDE4LjkyNjMiCiAgICAgICB5Mj0iNDE1Ny43OTQ5IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ5MjciCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTI5LTgiCiAgICAgICB4MT0iMjU0MS4wNzMiCiAgICAgICB5MT0iNDE1Ny43OTQ5IgogICAgICAgeDI9IjY0MTguOTI2MyIKICAgICAgIHkyPSI0MTU3Ljc5NDkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MjMtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTE5LTEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkxNS05IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkyNyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MTEtMiIKICAgICAgIHgxPSIyNzk5Ljg4ODkiCiAgICAgICB5MT0iMzE1Ny43OTUyIgogICAgICAgeDI9IjYzMjAuMTEwNCIKICAgICAgIHkyPSIzMTU3Ljc5NTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MDUtNSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODgxLTgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjAyIgogICAgIGlua3NjYXBlOmN4PSI2NTcuNDA5OTIiCiAgICAgaW5rc2NhcGU6Y3k9IjI3NDQuODgzMiIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9ImZhbHNlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iMTI0ODAsLTY0MCIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMTI0ODAsNDQ4MCIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNzYiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzEwMTkiPgogICAgPGcKICAgICAgIGlkPSJnOTk2Ij4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ3ODMpO3N0cm9rZS13aWR0aDowLjk4MjY3NzE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4NDI5LjU4MTEsNDM3My4wMzI0IFYgMTgxMy4wMzI5IgogICAgICAgICBpZD0icGF0aDQ3NzUiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzc3IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDg0MjkuNTgxMSw0MzczLjAzMjQgViAxODEzLjAzMjkiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTIuOTA0ODAwNDI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgICAgICBpZD0icGF0aDQ4MTMiCiAgICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgICAgc29kaXBvZGk6Y3g9Ii0yNzUwLjQ0ODUiCiAgICAgICAgIHNvZGlwb2RpOmN5PSIxMzc4LjQwOTgiCiAgICAgICAgIHNvZGlwb2RpOnJ4PSI5MjQuMDU3OTIiCiAgICAgICAgIHNvZGlwb2RpOnJ5PSIxNTkuMzYzMzkiCiAgICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgICBzb2RpcG9kaTplbmQ9IjAuNzQ0ODgwMzUiCiAgICAgICAgIGQ9Im0gLTE4MjYuMzkwNiwxMzc4LjQwOTggYSA5MjQuMDU3OTIsMTU5LjM2MzM5IDAgMCAxIC0yNDQuNzE5MSwxMDguMDI5OSBsIC02NzkuMzM4OCwtMTA4LjAyOTkgeiIKICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICAgICAgPGVsbGlwc2UKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGlkPSJwYXRoNDgzMyIKICAgICAgICAgY3g9Ii0yNTQ1LjE5MDkiCiAgICAgICAgIGN5PSIxNTI4LjU2NiIKICAgICAgICAgcng9IjI1OS42MzMxOCIKICAgICAgICAgcnk9IjMyLjI5MDQ5MyIKICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGlkPSJwYXRoNDgzNSIKICAgICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgICBzb2RpcG9kaTpjeD0iLTMxNzMuMzY0NyIKICAgICAgICAgc29kaXBvZGk6Y3k9IjE1MzEuNjM0NCIKICAgICAgICAgc29kaXBvZGk6cng9IjUwMS4xNDExNCIKICAgICAgICAgc29kaXBvZGk6cnk9IjYuMTM4MzQxOSIKICAgICAgICAgc29kaXBvZGk6c3RhcnQ9IjQuNzEzMTcwMSIKICAgICAgICAgc29kaXBvZGk6ZW5kPSI1LjAwMDcwNTYiCiAgICAgICAgIGQ9Im0gLTMxNzIuOTczMywxNTI1LjQ5NjEgYSA1MDEuMTQxMTQsNi4xMzgzNDE5IDAgMCAxIDE0Mi4xMDI0LDAuMjUzMyBsIC0xNDIuNDkzOCw1Ljg4NSB6IgogICAgICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTApIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmZmYwMDtmaWxsLW9wYWNpdHk6MC45MzcwNzg2ODtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTg4LjE3NDY4MjYyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICAgICAgaWQ9InBhdGg0ODYxIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLC0xLDAsMCwwKSIKICAgICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgICBzb2RpcG9kaTpjeD0iLTYxNDEuNzMxOSIKICAgICAgICAgc29kaXBvZGk6Y3k9Ii0yNjMwLjg0MzMiCiAgICAgICAgIHNvZGlwb2RpOnJ4PSIyMjY5LjQ2MzYiCiAgICAgICAgIHNvZGlwb2RpOnJ5PSIxNzk5Ljc1NzYiCiAgICAgICAgIHNvZGlwb2RpOnN0YXJ0PSI0LjcxNjg0NzkiCiAgICAgICAgIHNvZGlwb2RpOmVuZD0iMS41NzAxMjA5IgogICAgICAgICBkPSJtIC02MTMxLjYxMjYsLTQ0MzAuNTgyOSBhIDIyNjkuNDYzNiwxNzk5Ljc1NzYgMCAwIDEgMjI1OS4zNDAyLDE4MDMuMTQ0MyAyMjY5LjQ2MzYsMTc5OS43NTc2IDAgMCAxIC0yMjY3LjkyNjcsMTc5Ni4zNTI1IGwgLTEuNTMyOCwtMTc5OS43NTcyIHoiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJtIDEyNTE4LjkxNSw1MTE2LjI0MTQgdiAwIgogICAgICAgICBpZD0icGF0aDQ4NzkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0ODgxIgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTE4LjkxNSw1MTE2LjI0MTQgYyAzMS44NDcsLTk1LjA3MSAzMS44NDcsLTk1LjA3MSAwLDAiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTExKTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gODkxOC43NDczLDM1MTYuMjQxNCAzNTYyLjcwNzcsODAwIgogICAgICAgICBpZD0icGF0aDQ5MDMiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTA1IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDg5MTguNzQ3MywzNTE2LjI0MTQgMzU2Mi43MDc3LDgwMCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCAtODgwLC00MDAiCiAgICAgICAgIGlkPSJwYXRoNDkxMyIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MTUiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCBjIC0yMDAuMzY3LC0yNjkuNzgyIC01NzAuMDIyLC0zMDQuNTg2IC04ODAsLTQwMCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCAtODAsLTgwIgogICAgICAgICBpZD0icGF0aDQ5MTciCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE5IgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTE4LjkxNSw1NTk2LjI0MTQgYyAtMTcuMTM2LC0zMy41OTUgLTUzLjMzNCwtNTMuMzMzIC04MCwtODAiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTI5KTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Im0gODY3OC45MTQ4LDQyMzYuMjQxNCAzODQwLjAwMDIsMTM2MCIKICAgICAgICAgaWQ9InBhdGg0OTIxIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkyMyIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSA4Njc4LjkxNDgsNDIzNi4yNDE0IDM4NDAuMDAwMiwxMzYwIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0ibSAxMjUxMS43OTYsOTU4Ljc3MzI5IHYgMCIKICAgICAgICAgaWQ9InBhdGg0ODc5LTQiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0ODgxLTgiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDk1OC43NzMyOSBjIDMxLjg0Nyw5NS4wNzE2MSAzMS44NDcsOTUuMDcxNjEgMCwwIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NDkxMS0yKTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gODkxMS42Mjg4LDI1NTguNzczMiAxMjQ3NC4zMzYsMTc1OC43NzMzIgogICAgICAgICBpZD0icGF0aDQ5MDMtOSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MDUtNSIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSA4OTExLjYyODgsMjU1OC43NzMyIDEyNDc0LjMzNiwxNzU4Ljc3MzMiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJtIDEyNTExLjc5Niw0NzguNzczNDkgLTg4MCwzOTkuOTk5OCIKICAgICAgICAgaWQ9InBhdGg0OTEzLTkiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE1LTkiCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDQ3OC43NzM0OSBjIC0yMDAuMzY3LDI2OS43ODE4IC01NzAuMDIyLDMwNC41ODYyIC04ODAsMzk5Ljk5OTgiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBkPSJtIDEyNTExLjc5Niw0NzguNzczNDkgLTgwLDgwIgogICAgICAgICBpZD0icGF0aDQ5MTctNCIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MTktMSIKICAgICAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IGMgLTE3LjEzNSwzMy41OTQ3IC01My4zMzMsNTMuMzMzNCAtODAsODAiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTI5LTgpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4NjcxLjc5NjMsMTgzOC43NzMzIDEyNTExLjc5Niw0NzguNzczNDkiCiAgICAgICAgIGlkPSJwYXRoNDkyMS0yIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkyMy0wIgogICAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDg2NzEuNzk2MywxODM4Ljc3MzMgMTI1MTEuNzk2LDQ3OC43NzM0OSIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGQ9Im0gMzYwNS44MTgzLDYwNTcuMTIwNSBjIC0zOTEuMDY4LDguNzg5MyAtMTE3Mi4yNzIsMTUuOTgwNSAtMTczNi4wMTA3LDE1Ljk4MDUgSCA4NDQuODI5IGwgMTUuMzcyMywtMTg3LjI5MTkgYyA1My4xODU5LC02NDguMDAxMyAzNjcuNDQ4MSwtMTI4MS40MTI0IDgwMS44NTk4LC0xNjE2LjE4ODUgMzA1LjEwNjYsLTIzNS4xMjggNTc2LjI2NzQsLTMzMC43NTAxIDkzMy45NTQyLC0zMjkuMzUgODIzLjcwNCwzLjIyNDcgMTUzNC4yNTQsNzU2Ljk3OTIgMTY4NS45MDgsMTc4OC40MTgxIDE5LjA1OCwxMjkuNjE0MiAzNC43MTMsMjUyLjkzOTcgMzQuNzg4LDI3NC4wNTY5IDAuMTE5LDMzLjg4NDUgLTgzLjM4NCw0MC4yNzE3IC03MTAuODkzLDU0LjM3NDkgeiIKICAgICAgICAgaWQ9InBhdGg1MTM3IgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmZmYwMDtmaWxsLW9wYWNpdHk6MC45MzcwNzg2ODtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NzUuMjY5ODc0NTc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIHN0cm9rZSBmaWxsIgogICAgICAgICBkPSJtIDk0LjQ4ODIsMTM2LjM3ODEzIGMgLTAuNTgxNyw0Ny4xMjIwMyAxMjkwLjEwNjgsMzgxNC40NzY2NyAxMzE2LjA4ODIsMzg0MS4xMTIzNyA2LjEyODksNi4yODMyIDI3LjQ2NjUsNS4wMDEgNDcuNDE3LC0yLjg0NzQgMzAuODM0NywtMTIuMTMwMyAyMi43MjEyLC01My4zNDAyIC01NC4xMTg4LC0yNzQuODU5MSBDIDEzNTQuMTU4NCwzNTU2LjQ1OTEgMTA1NS4zNjEyLDI2OTEuNTM1OCA3MzkuODgwOSwxNzc3LjczMzYgMzg4LjU4MDQsNzYwLjE3Njg2IDE1NC42ODUxLDExNi4yNzUzIDEzNi4zNjE3LDExNi4yNzUzIGMgLTE2LjQ1NTEsMCAtMzUuMjY4LDguODcyMyAtNDEuODA2NCwxOS43MTg3IC0wLjA0MiwwLjA3MDUgLTAuMDY0LDAuMTk5NjIgLTAuMDY3LDAuMzg0MTMgeiBtIDEyMzcuODI4OCwzNS4yNTYzMiBjIC0zLjU1MDEsMy42Mzg2NiA3NjMuMzQ3OCwzNTMwLjIxODI1IDc3Ny4xMDA1LDM1NzMuNDk1NTUgMi4zMDIzLDcuMjQ1MyAxNi42NTEzLDYuMzgyOSA1My45MjY2LC0zLjI0MzIgMzkuNzM5MiwtMTAuMjYyNCA0OS45MTYzLC0xNi4xMjc0IDQ2Ljc4MzEsLTI2Ljk1OTkgLTMuOTc0NiwtMTMuNzQxMiAtNzY4LjcyOTcsLTM1MDUuMTg1ODkgLTc3Ny4wNzc0LC0zNTQ3LjcwMTM4IGwgLTQuMzU2NSwtMjIuMTg2ODEgLTQ1Ljg4MjMsMTAuOTMzMSBjIC0yNS4yMzU1LDYuMDE0MDEgLTQ3Ljk1NzYsMTMuMDYxNDUgLTUwLjQ5NCwxNS42NjI2NCB6IgogICAgICAgICBpZD0icGF0aDQ4ODMiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojZmZmZjAwO2ZpbGwtb3BhY2l0eTowLjkzNzA3ODY4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDo3NS4yNjk4NzQ1NztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgICAgICBkPSJtIDM1NDYuNDc3MywxOTY2LjYwNzkgYyAtMjE0Ljc5Niw5NzcuNDU3IC0zOTAuNjQyLDE3NzcuODA5NCAtMzkwLjc3LDE3NzguNTYwNCAtMC40NTcsMi43MDEyIC05Mi43NjQsLTIwLjc1NDIgLTk2LjQxOCwtMjQuNTAwMiAtNC4zODMsLTQuNDk0MiA3NzUuMDE5LC0zNTY3Ljg1MzAxIDc4MS45MzcsLTM1NzQuOTQ0ODEgMi41NzMsLTIuNjM4NSAyNS42MDMsLTAuMzQzOCA1MS4xNzksNS4wOTk1NiAzNC45MDIsNy40Mjc1MyA0Ni4yNjQsMTMuNDczODEgNDUuNTU2LDI0LjI0MjU2IC0wLjUxOSw3Ljg5MDMxIC0xNzYuNjg3LDgxNC4wODQ0NSAtMzkxLjQ4NCwxNzkxLjU0MjQ5IHoiCiAgICAgICAgIGlkPSJwYXRoNDg4OCIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNmZmZmMDA7ZmlsbC1vcGFjaXR5OjAuOTM3MDc4Njg7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjc1LjI2OTg3NDU3O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGQ9Im0gNDU1OS41OTIzLDE5NzkuNDI0MyBjIC0zNTAuMTkxLDEwMTIuNzAxOCAtNjUwLjI2OSwxODgwLjMzNTYgLTY2Ni44NCwxOTI4LjA3NDMgbCAtMzAuMTI4LDg2Ljc5NzggLTQ5LjAwNCwtMTYuOTY1NyAtNDkuMDA0LC0xNi45NjU3IDM3LjIxNiwtMTA4LjI4MSBjIDIwLjQ2OSwtNTkuNTU0NyAzMjAuODQ5LC05MjkuMzc3MiA2NjcuNTExLC0xOTMyLjkzOSBsIDYzMC4yOTQsLTE4MjQuNjU3MjYgNDguODc3LDE3LjU1MzA1IGMgMjYuODgyLDkuNjU0NjkgNDguNjMyLDE5LjQ3ODc1IDQ4LjMzMywyMS44Mjk5MSAtMC4zLDIuMzUyMTYgLTI4Ny4wNjQsODMyLjg1MDM2IC02MzcuMjU1LDE4NDUuNTUzNiB6IgogICAgICAgICBpZD0icGF0aDQ4OTAiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICA8L2c+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoMTAxMyIKICAgICAgIGQ9Im0gMjYzMC42ODY0LDYxNDEuNzMxOCAtMTYuNTk0MSw2MDQ3LjI0NDIiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTIuODUzNTY5MDM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICA8L2c+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="rot_z" name="field"/>
                  <Option type="int" value="2" name="type"/>
                </Option>
                <Option type="Map" name="name">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="@project_folder || '/svg/smdev=' || replace( lower(&quot;cellule&quot;), ' ','_')   || '.svg'" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="3" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{2db9d601-d83b-4cb9-a56b-e0722d007f7d}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgdmVyc2lvbj0iMS4xIgogICB4PSIwcHgiCiAgIHk9IjBweCIKICAgdmlld0JveD0iMCAwIDEzMDAgMTMwMCIKICAgaWQ9InN2ZzIiCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PWZveWVyX2FfcG9zZXIuc3ZnIgogICB3aWR0aD0iMTMwY20iCiAgIGhlaWdodD0iMTMwY20iCiAgIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXciPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE2Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTQiPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg0NzIiPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICAgIG1vZGU9InNjcmVlbiIKICAgICAgICAgaW4yPSJCYWNrZ3JvdW5kSW1hZ2UiCiAgICAgICAgIGlkPSJmZUJsZW5kODQ3NCIgLz4KICAgIDwvZmlsdGVyPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjEyNSIKICAgICBpbmtzY2FwZTpjeD0iMjI1My4wNTQiCiAgICAgaW5rc2NhcGU6Y3k9IjI1MTIuNDQ5NiIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0iZzk0NyIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSI1NjE4LjY0LDIzODEuMzYiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjUxMzguOSwyNTYwIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNzYiCiAgICAgICBlbmFibGVkPSJ0cnVlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwwIgogICAgICAgb3JpZW50YXRpb249IjAsNDkxMy4zODU4IgogICAgICAgaWQ9Imd1aWRlOTQwIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICAgIDxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjEzMDAsMCIKICAgICAgIG9yaWVudGF0aW9uPSItNDkxMy4zODU4LDAiCiAgICAgICBpZD0iZ3VpZGU5NDIiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTMwMCwxMzAwIgogICAgICAgb3JpZW50YXRpb249IjAsLTQ5MTMuMzg1OCIKICAgICAgIGlkPSJndWlkZTk0NCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDEzMDAiCiAgICAgICBvcmllbnRhdGlvbj0iNDkxMy4zODU4LDAiCiAgICAgICBpZD0iZ3VpZGU5NDYiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwxMzAwIgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTk0OCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIxMzAwLDEzMDAiCiAgICAgICBvcmllbnRhdGlvbj0iLTAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTk1MCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlkPSJsYXllcjEiCiAgICAgaW5rc2NhcGU6bGFiZWw9IkxheWVyIDEiCiAgICAgc3R5bGU9ImRpc3BsYXk6bm9uZTtmaWx0ZXI6dXJsKCNmaWx0ZXI4NDcyKSIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDMwMTcuMjgzNSkiIC8+CiAgPGcKICAgICBpZD0iZzk0NyIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEuMzk5OTg3OCkiPgogICAgPGcKICAgICAgIGlkPSJnOTM4IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDA4NjcsMCwwLDEuMDEzNDU2OSwwLjEzNjQ4NjA0LC0xLjI1NDgxMzUpIj4KICAgICAgPGVsbGlwc2UKICAgICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7Y2xpcC1ydWxlOm5vbnplcm87ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2lzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbDtjb2xvci1pbnRlcnBvbGF0aW9uOnNSR0I7Y29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOmxpbmVhclJHQjtzb2xpZC1jb2xvcjojMDAwMDAwO3NvbGlkLW9wYWNpdHk6MTtmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiNmZjAwZmY7c3Ryb2tlLXdpZHRoOjE5LjU4NDc2MDY3O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTo1OC43NTQyNzYwNCwgMTkuNTg0NzU4Njg7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgICAgaWQ9InBhdGg0NjQ3IgogICAgICAgICBjeD0iNjQ5LjE3ODM0IgogICAgICAgICBjeT0iNjQyLjc3MDY5IgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5OTk5NzE3LC0wLjAwMjM3OTU3LDEuOTMwMTI1NmUtNCwwLjk5OTk5OTk4LDAsMCkiCiAgICAgICAgIHJ4PSI2MzkuNDQ3MzkiCiAgICAgICAgIHJ5PSI2MzEuNTAwMDYiCiAgICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTM4LjE2NDU3OCIKICAgICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI2MC4yNTEiIC8+CiAgICAgIDxlbGxpcHNlCiAgICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7ZmlsbDojZmZjOGZmO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojZmYwMGZmO3N0cm9rZS13aWR0aDoxOS40OTg0MDE2NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIgogICAgICAgICBpZD0icGF0aDk0MCIKICAgICAgICAgY3g9IjY0OS4xNzgzNCIKICAgICAgICAgY3k9IjY0Mi43NzA2OSIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTk5OTcxNywtMC4wMDIzNzk1NywxLjkzMDEyNTZlLTQsMC45OTk5OTk5OCwwLDApIgogICAgICAgICByeD0iNDkwLjE5MDYxIgogICAgICAgICByeT0iNDg0LjA5ODMiCiAgICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTM4LjE2NDU3OCIKICAgICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI2MC4yNTEiIC8+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="4" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{5aac91f6-d5fc-47a1-b399-f7041755ab9a}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAxMzAwIDEzMDAiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1mb3llcl9tYXRfYV9yZW1wbGFjZXIuc3ZnIgogICB3aWR0aD0iMTMwY20iCiAgIGhlaWdodD0iMTMwY20iCiAgIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXciPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE2Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MTY5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNTE2NyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bWFya2VyCiAgICAgICBpbmtzY2FwZTpzdG9ja2lkPSJUYWlsIgogICAgICAgb3JpZW50PSJhdXRvIgogICAgICAgcmVmWT0iMCIKICAgICAgIHJlZlg9IjAiCiAgICAgICBpZD0iVGFpbCIKICAgICAgIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgaW5rc2NhcGU6aXNzdG9jaz0idHJ1ZSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDg2NyIKICAgICAgICAgdHJhbnNmb3JtPSJzY2FsZSgtMS4yKSIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzAwMDAwMDtzdHJva2Utb3BhY2l0eToxIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg1NSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsLTMuOTU4NTIyNyAwLjU0MzUyMDk0LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTciCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLC0zLjk1ODUyMjcgMy4wNjE3MDUzLDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTkiCiAgICAgICAgICAgZD0iTSAxLjMwNTM1ODIsLTMuOTU4NTIyNyA1LjY1Mzc0NjYsMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MSIKICAgICAgICAgICBkPSJNIC0zLjgwNDg2NzQsNC4xNzc1ODM4IDAuNTQzNTIwOTQsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2MyIKICAgICAgICAgICBkPSJNIC0xLjI4NjY4MzIsNC4xNzc1ODM4IDMuMDYxNzA1MywwLjIxOTc0MjI2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODAwMDAwMDE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0ODY1IgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLDQuMTc3NTgzOCA1LjY1Mzc0NjYsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4MTUiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODEzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDgwOSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4MDciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTEzIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTIxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTA4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjAuNzY0MTc5MTE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTAwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5MiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwOTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDg2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDc7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODAiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OC0yLTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg0NzIiPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICAgIG1vZGU9InNjcmVlbiIKICAgICAgICAgaW4yPSJCYWNrZ3JvdW5kSW1hZ2UiCiAgICAgICAgIGlkPSJmZUJsZW5kODQ3NCIgLz4KICAgIDwvZmlsdGVyPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJJbiBhbmQgb3V0IGdsb3cgd2l0aCBhIHBvc3NpYmxlIG9mZnNldCBhbmQgY29sb3JpemFibGUgZmxvb2QiCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJDdXRvdXQgR2xvdyIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlkPSJmaWx0ZXI4NDc5Ij4KICAgICAgPGZlT2Zmc2V0CiAgICAgICAgIGR5PSIzIgogICAgICAgICBkeD0iMyIKICAgICAgICAgaWQ9ImZlT2Zmc2V0ODQ4MSIgLz4KICAgICAgPGZlR2F1c3NpYW5CbHVyCiAgICAgICAgIHN0ZERldmlhdGlvbj0iMyIKICAgICAgICAgcmVzdWx0PSJibHVyIgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NDgzIiAvPgogICAgICA8ZmVGbG9vZAogICAgICAgICBmbG9vZC1jb2xvcj0icmdiKDAsMCwwKSIKICAgICAgICAgZmxvb2Qtb3BhY2l0eT0iMSIKICAgICAgICAgcmVzdWx0PSJmbG9vZCIKICAgICAgICAgaWQ9ImZlRmxvb2Q4NDg1IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49ImZsb29kIgogICAgICAgICBpbjI9IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIG9wZXJhdG9yPSJpbiIKICAgICAgICAgcmVzdWx0PSJjb21wb3NpdGUiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg0ODciIC8+CiAgICAgIDxmZUJsZW5kCiAgICAgICAgIGluPSJibHVyIgogICAgICAgICBpbjI9ImNvbXBvc2l0ZSIKICAgICAgICAgbW9kZT0ibm9ybWFsIgogICAgICAgICBpZD0iZmVCbGVuZDg0ODkiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIGlua3NjYXBlOm1lbnUtdG9vbHRpcD0iSW4gYW5kIG91dCBnbG93IHdpdGggYSBwb3NzaWJsZSBvZmZzZXQgYW5kIGNvbG9yaXphYmxlIGZsb29kIgogICAgICAgaW5rc2NhcGU6bWVudT0iU2hhZG93cyBhbmQgR2xvd3MiCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iQ3V0b3V0IEdsb3ciCiAgICAgICBzdHlsZT0iY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOnNSR0IiCiAgICAgICBpZD0iZmlsdGVyODQ5MSI+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeT0iMyIKICAgICAgICAgZHg9IjMiCiAgICAgICAgIGlkPSJmZU9mZnNldDg0OTMiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBzdGREZXZpYXRpb249IjMiCiAgICAgICAgIHJlc3VsdD0iYmx1ciIKICAgICAgICAgaWQ9ImZlR2F1c3NpYW5CbHVyODQ5NSIgLz4KICAgICAgPGZlRmxvb2QKICAgICAgICAgZmxvb2QtY29sb3I9InJnYigwLDAsMCkiCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjEiCiAgICAgICAgIHJlc3VsdD0iZmxvb2QiCiAgICAgICAgIGlkPSJmZUZsb29kODQ5NyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGluPSJmbG9vZCIKICAgICAgICAgaW4yPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iaW4iCiAgICAgICAgIHJlc3VsdD0iY29tcG9zaXRlIgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NDk5IiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0iYmx1ciIKICAgICAgICAgaW4yPSJjb21wb3NpdGUiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaWQ9ImZlQmxlbmQ4NTAxIiAvPgogICAgPC9maWx0ZXI+CiAgICA8ZmlsdGVyCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg1MDMiPgogICAgICA8ZmVHYXVzc2lhbkJsdXIKICAgICAgICAgc3RkRGV2aWF0aW9uPSI1IgogICAgICAgICByZXN1bHQ9InJlc3VsdDYiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1MDUiIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICByZXN1bHQ9InJlc3VsdDgiCiAgICAgICAgIGluPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NTA3IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ5IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgaW4yPSJTb3VyY2VBbHBoYSIKICAgICAgICAgaW49InJlc3VsdDgiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1MDkiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIKICAgICAgICAgaWQ9ImZlQ29sb3JNYXRyaXg4NTExIiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0icmVzdWx0MTAiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVCbGVuZDg1MTMiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlua3NjYXBlOmxhYmVsPSJEYXJrIEFuZCBHbG93IgogICAgICAgaWQ9ImZpbHRlcjg1NzUiCiAgICAgICB5PSItMC4yNSIKICAgICAgIGhlaWdodD0iMS41IgogICAgICAgaW5rc2NhcGU6bWVudS10b29sdGlwPSJEYXJrZW5zIHRoZSBlZGdlIHdpdGggYW4gaW5uZXIgYmx1ciBhbmQgYWRkcyBhIGZsZXhpYmxlIGdsb3ciCiAgICAgICBpbmtzY2FwZTptZW51PSJTaGFkb3dzIGFuZCBHbG93cyI+CiAgICAgIDxmZUZsb29kCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjAuMzI1NDkiCiAgICAgICAgIGZsb29kLWNvbG9yPSJyZ2IoMCwwLDApIgogICAgICAgICByZXN1bHQ9ImZsb29kIgogICAgICAgICBpZD0iZmVGbG9vZDg1NzciIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICBpbj0iZmxvb2QiCiAgICAgICAgIGluMj0iU291cmNlR3JhcGhpYyIKICAgICAgICAgb3BlcmF0b3I9ImluIgogICAgICAgICByZXN1bHQ9ImNvbXBvc2l0ZTEiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1NzkiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpbj0iY29tcG9zaXRlMSIKICAgICAgICAgc3RkRGV2aWF0aW9uPSIzIgogICAgICAgICByZXN1bHQ9ImJsdXIiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg1ODEiIC8+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeD0iNiIKICAgICAgICAgZHk9IjYiCiAgICAgICAgIHJlc3VsdD0ib2Zmc2V0IgogICAgICAgICBpZD0iZmVPZmZzZXQ4NTgzIiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGluMj0ib2Zmc2V0IgogICAgICAgICBvcGVyYXRvcj0ib3ZlciIKICAgICAgICAgcmVzdWx0PSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg1ODUiIC8+CiAgICAgIDxmZUNvbG9yTWF0cml4CiAgICAgICAgIHJlc3VsdD0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIGluPSJmYlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIHZhbHVlcz0iMCAwIDAgLTEgMCAwIDAgMCAtMSAwIDAgMCAwIC0xIDAgMCAwIDAgMSAwIgogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MjciIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NjI5IgogICAgICAgICBzdGREZXZpYXRpb249IjUiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0NiIKICAgICAgICAgaW49ImZiU291cmNlR3JhcGhpYyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzEiCiAgICAgICAgIGluMj0icmVzdWx0NiIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ4IgogICAgICAgICBpbj0iZmJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iYXRvcCIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGlkPSJmZUNvbXBvc2l0ZTg2MzMiCiAgICAgICAgIGluMj0iZmJTb3VyY2VHcmFwaGljQWxwaGEiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0OSIKICAgICAgICAgb3BlcmF0b3I9Im92ZXIiCiAgICAgICAgIGluPSJyZXN1bHQ4IiAvPgogICAgICA8ZmVDb2xvck1hdHJpeAogICAgICAgICBpZD0iZmVDb2xvck1hdHJpeDg2MzUiCiAgICAgICAgIHZhbHVlcz0iMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwIDAgMCAwIDAgMSAwICIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQxMCIgLz4KICAgICAgPGZlQmxlbmQKICAgICAgICAgaWQ9ImZlQmxlbmQ4NjM3IgogICAgICAgICBpbjI9InJlc3VsdDYiCiAgICAgICAgIGluPSJyZXN1bHQxMCIKICAgICAgICAgbW9kZT0ibm9ybWFsIiAvPgogICAgPC9maWx0ZXI+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzEyIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMTQ2Mzc0NjIiCiAgICAgaW5rc2NhcGU6Y3g9IjI2NDcuNTg5IgogICAgIGlua3NjYXBlOmN5PSIyNjE5LjQ4MDIiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzIiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJ0cnVlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iNTYxOC42NCwyMzgxLjM2IgogICAgIGlua3NjYXBlOm1lYXN1cmUtZW5kPSI1MTM4LjksMjU2MCIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1pbnRlcnNlY3Rpb24tcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODA3NiIKICAgICAgIGVuYWJsZWQ9InRydWUiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIxMzAwLDEzMDAiCiAgICAgICBvcmllbnRhdGlvbj0iLTAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTk0MiIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDEzMDAiCiAgICAgICBvcmllbnRhdGlvbj0iMC43MDcxMDY3OCwwLjcwNzEwNjc4IgogICAgICAgaWQ9Imd1aWRlOTQ0IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8ZwogICAgIGlkPSJnODQ3NiIKICAgICBzdHlsZT0iZmlsdGVyOnVybCgjZmlsdGVyODQ5MSkiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzg1LjQxMzE2LDI4NTYuNDg5MikiIC8+CiAgPGcKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlkPSJsYXllcjEiCiAgICAgaW5rc2NhcGU6bGFiZWw9IkxheWVyIDEiCiAgICAgc3R5bGU9ImRpc3BsYXk6bm9uZTtmaWx0ZXI6dXJsKCNmaWx0ZXI4NDcyKSIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDMwMTcuMjgzNSkiIC8+CiAgPGcKICAgICBpZD0iZzk0MCIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjE0NDk3NzIsLTEwLjAwNzEzKSI+CiAgICA8cGF0aAogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI2NzEuMTY0MSIKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTMzOC4xOTg3MSIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5OTk3MjEsLTAuMDAyMzYxMTIsMS45NDUyMDU3ZS00LDAuOTk5OTk5OTgsMCwwKSIKICAgICAgIGQ9Im0gMTE2MS40NjQsMTI2MC43MjcyIGEgMjExLjg1MDU2LDQxLjUxOTEzNSAwIDAgMSAtMjExLjc3MzgxLDQxLjUxOTEgMjExLjg1MDU2LDQxLjUxOTEzNSAwIDAgMSAtMjExLjkyNzIzLC00MS40ODkxIDIxMS44NTA1Niw0MS41MTkxMzUgMCAwIDEgMjExLjYyMDM0LC00MS41NDkxIDIxMS44NTA1Niw0MS41MTkxMzUgMCAwIDEgMjEyLjA4MDUsNDEuNDU4OSIKICAgICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgICBzb2RpcG9kaTplbmQ9IjYuMjgxNzM2NyIKICAgICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgICAgc29kaXBvZGk6cnk9IjQxLjUxOTEzNSIKICAgICAgIHNvZGlwb2RpOnJ4PSIyMTEuODUwNTYiCiAgICAgICBzb2RpcG9kaTpjeT0iMTI2MC43MjcyIgogICAgICAgc29kaXBvZGk6Y3g9Ijk0OS42MTM0NiIKICAgICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICAgIGlkPSJwYXRoNTE2MSIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eTowLjk4ODIwNzU5O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDo1OS40NDYwMTgyMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6MTc4LjMzODA1NjA3LCA1OS40NDYwMTg2OTtzdHJva2UtZGFzaG9mZnNldDoxMTMuMzg1ODI2MTE7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgPGVsbGlwc2UKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iNTkuODM1NzU3IgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItMzguMTk3NjczIgogICAgICAgcnk9IjYyNy4xNDc3MSIKICAgICAgIHJ4PSI2NDAuMDAxODMiCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5OTk5NzIxLC0wLjAwMjM2MTEyLDEuOTQ1MjA1N2UtNCwwLjk5OTk5OTk4LDAsMCkiCiAgICAgICBjeT0iNjQ4LjY5MDczIgogICAgICAgY3g9IjY0OS43MzA2NSIKICAgICAgIGlkPSJwYXRoNDY0NyIKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7ZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojZmZjOTUxO3N0cm9rZS13aWR0aDoxOS41MjU2MTE4ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6NTguNTc2ODMxODksIDE5LjUyNTYxMDYzO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MC45ODgyMzUyOTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICA8ZWxsaXBzZQogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI1OS44MzU3NTciCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii0zOC4xOTc2NzMiCiAgICAgICByeT0iNDgwLjc2MTg0IgogICAgICAgcng9IjQ5MC42MTU2IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTk5OTcyMSwtMC4wMDIzNjExMiwxLjk0NTIwNTdlLTQsMC45OTk5OTk5OCwwLDApIgogICAgICAgY3k9IjY0OC42OTA3MyIKICAgICAgIGN4PSI2NDkuNzMwNjUiCiAgICAgICBpZD0icGF0aDk0MCIKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7ZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNTI5O2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojYjU3ZTAwO3N0cm9rZS13aWR0aDoxOS40Mzk1MTQxNjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjAuOTg4MjM1Mjk7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogIDwvZz4KPC9zdmc+Cg==" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="5" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{48f95143-f89e-43b0-97d4-4055d2c95d4b}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9IjgwY20iCiAgIGhlaWdodD0iNjQwY20iCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDgwMC4wMDAwMyA2NDAwLjAwMDIiCiAgIGlkPSJzdmcxOCIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9Zm95ZXJfc2ltcGxlX2FfcmVtcGxhY2VyLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSI+CiAgPGRlZnMKICAgICBpZD0iZGVmczIyIj4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc3NCIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NzIiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzYxIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc1OSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NjYiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2ZmYzc0YjtzdG9wLW9wYWNpdHk6MC45ODgyMzUyOTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0NzY0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3NjIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMDAwMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcyMCIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1nbG9iYWw9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBzaG93Z3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtZWRnZS1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4xNiIKICAgICBpbmtzY2FwZTpjeD0iMTU3OC4wNjkxIgogICAgIGlua3NjYXBlOmN5PSIyMzU5OS42MSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0iZzQ5MTAiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iMTM5LjU2NSwxMjQxLjM5IgogICAgIGlua3NjYXBlOm1lYXN1cmUtZW5kPSIxNjYyOC4yLDEyODMuNDgiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC10by1ndWlkZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10ZXh0LWJhc2VsaW5lPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXBlcnBlbmRpY3VsYXI9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtdGFuZ2VudGlhbD0iZmFsc2UiCiAgICAgc2NhbGUteD0iMTAiCiAgICAgZml0LW1hcmdpbi10b3A9IjAiCiAgICAgZml0LW1hcmdpbi1sZWZ0PSIwIgogICAgIGZpdC1tYXJnaW4tcmlnaHQ9IjAiCiAgICAgZml0LW1hcmdpbi1ib3R0b209IjAiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBzaG93Ym9yZGVyPSJ0cnVlIgogICAgIHZpZXdib3gtd2lkdGg9Ijc4LjI0MiI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ0NzcxIgogICAgICAgb3JpZ2lueD0iOS44MzEwOTMxZS0wNSIKICAgICAgIG9yaWdpbnk9IjEuODg3ODg0OWUtMDciIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIxNjM4LjI2NTQsLTk2My42ODU2NCIKICAgICAgIG9yaWVudGF0aW9uPSIwLDEiCiAgICAgICBpZD0iZ3VpZGU4NzYiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgICAgPGNjOmxpY2Vuc2UKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iIC8+CiAgICAgICAgPGRjOmNyZWF0b3I+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5qbWE8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOmNyZWF0b3I+CiAgICAgICAgPGRjOnB1Ymxpc2hlcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPkF6aW11dDwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cHVibGlzaGVyPgogICAgICAgIDxkYzpyaWdodHM+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT4oYylBemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnJpZ2h0cz4KICAgICAgPC9jYzpXb3JrPgogICAgICA8Y2M6TGljZW5zZQogICAgICAgICByZGY6YWJvdXQ9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNSZXByb2R1Y3Rpb24iIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rpc3RyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI05vdGljZSIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0F0dHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpwcm9oaWJpdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0NvbW1lcmNpYWxVc2UiIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rlcml2YXRpdmVXb3JrcyIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1NoYXJlQWxpa2UiIC8+CiAgICAgIDwvY2M6TGljZW5zZT4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxnCiAgICAgaWQ9Imc0OTEwIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDU1MTcuOTU2Miw5MzcwLjg1NjYpIgogICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTc4OS4wNjA4OSIKICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjQzMy4yNTExNiI+CiAgICA8cGF0aAogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjY2NjY2NjY2MiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg4IgogICAgICAgZD0ibSAtNTQ3Ni45NDI5LC04MDQyLjY3MTEgaCAtMzAuMDUyMyBsIDE3Ny40MzU5LDQzOS43MTc0IDQyMy4yMDYzLDAuMzQxMiAxNzkuMzU3OCwtNDQyLjEyNzcgLTMwLjA1MjMsMC4wNTEgLTE2OS40OTI0LDQxMi4wMjYyIC0zODIuNzAzNSwtMC4zMDgxIHoiCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6I2ZmYzk1MTtmaWxsLW9wYWNpdHk6MC45ODgyMzUyOTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDQzNjg1OTE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIgLz4KICAgIDxwYXRoCiAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjY2NjY2NjY2NjIgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgIGlkPSJwYXRoMTAiCiAgICAgICBkPSJtIC01NDkxLjk2OSwtOTM1MC44NTYyIGMgLTguMjk4MSwwIC0xNS4wMjUxLDYuNzIzNSAtMTUuMDI2MiwxNS4wMTY0IHYgMTI5My4xNjg3IGggMzAuMDUyMyB2IC0xMjc4LjE1MjIgaCA3MTcuNzY2NyBsIDIuMTI4NywxMjc2LjEzNDEgMzAuMDUyMywtMC4wNTEgLTIuMTUyMSwtMTI5MS4xMjUyIGMgLTAuMDE1LC04LjI4MzggLTYuNzM5MSwtMTQuOTkxMSAtMTUuMDI4MiwtMTQuOTkxMSB6IgogICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7Zm9udC1zdHlsZTpub3JtYWw7Zm9udC12YXJpYW50Om5vcm1hbDtmb250LXdlaWdodDpub3JtYWw7Zm9udC1zdHJldGNoOm5vcm1hbDtmb250LXNpemU6bWVkaXVtO2xpbmUtaGVpZ2h0Om5vcm1hbDtmb250LWZhbWlseTpzYW5zLXNlcmlmO2ZvbnQtdmFyaWFudC1saWdhdHVyZXM6bm9ybWFsO2ZvbnQtdmFyaWFudC1wb3NpdGlvbjpub3JtYWw7Zm9udC12YXJpYW50LWNhcHM6bm9ybWFsO2ZvbnQtdmFyaWFudC1udW1lcmljOm5vcm1hbDtmb250LXZhcmlhbnQtYWx0ZXJuYXRlczpub3JtYWw7Zm9udC1mZWF0dXJlLXNldHRpbmdzOm5vcm1hbDt0ZXh0LWluZGVudDowO3RleHQtYWxpZ246c3RhcnQ7dGV4dC1kZWNvcmF0aW9uOm5vbmU7dGV4dC1kZWNvcmF0aW9uLWxpbmU6bm9uZTt0ZXh0LWRlY29yYXRpb24tc3R5bGU6c29saWQ7dGV4dC1kZWNvcmF0aW9uLWNvbG9yOiMwMDAwMDA7bGV0dGVyLXNwYWNpbmc6bm9ybWFsO3dvcmQtc3BhY2luZzpub3JtYWw7dGV4dC10cmFuc2Zvcm06bm9uZTt3cml0aW5nLW1vZGU6bHItdGI7ZGlyZWN0aW9uOmx0cjt0ZXh0LW9yaWVudGF0aW9uOm1peGVkO2RvbWluYW50LWJhc2VsaW5lOmF1dG87YmFzZWxpbmUtc2hpZnQ6YmFzZWxpbmU7dGV4dC1hbmNob3I6c3RhcnQ7d2hpdGUtc3BhY2U6bm9ybWFsO3NoYXBlLXBhZGRpbmc6MDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO3ZlY3Rvci1lZmZlY3Q6bm9uZTtmaWxsOiNmZmM5NTE7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1Mjk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjMwLjA0MzY4NTkxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiIC8+CiAgICA8cGF0aAogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjc2NjY2Njc2NjY2Njc2MiCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGgxMiIKICAgICAgIGQ9Im0gLTUxMTkuMTA1NCwtOTI1MS4wMzMgYyAtODQuNzg4NCwxLjIwODMgLTE1Ni4xNDY3LDc0LjY4MiAtMjA3Ljc0NTYsMTQ0Ljc1NTggLTUxLjU5OTEsNzAuMDczNiAtODMuNjgzLDEzOS42MjU0IC04My42ODMsMTM5LjYyNTQgbCAtOS44NDc2LDIxLjMxMjggaCAyMy40OTA0IDU3OC44OTQzIGwgLTkuMTA4MiwtMjAuOTkxMSBjIDAsMCAtMzAuNjMxNCwtNzAuNzM1IC04MS41NjIsLTE0MS40NjgyIC01MC45MzA0LC03MC43MzMgLTEyMi44MzMxLC0xNDQuNDgyOSAtMjEwLjQzODMsLTE0My4yMzQ3IHogbSAwLjQyNzQsMzAuMDMxIGMgNjkuODAzMiwtMC45OTQ2IDEzNi45MDAxLDYzLjA4NjQgMTg1LjYxNzEsMTMwLjc0NTQgMzIuODc2LDQ1LjY1ODcgNTYuMDU3NCw4OS44NTM4IDY4LjMxOTIsMTE0Ljg4NDYgaCAtNTA3LjcwODggYyAxMi44MzAzLC0yNC44MTczIDM2LjY1MTUsLTY4LjA3OSA2OS44MDU5LC0xMTMuMTA0MiA0OS40OTYsLTY3LjIxNzcgMTE3LjEzNiwtMTMxLjU3MzUgMTgzLjk2NjYsLTEzMi41MjU4IHoiCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7dmVjdG9yLWVmZmVjdDpub25lO2ZpbGw6I2I1N2UwMDtmaWxsLW9wYWNpdHk6MC45ODgyMzUyOTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuMDQzNjg1OTE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIgLz4KICAgIDxwYXRoCiAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjY2NjY2NjY2NjY2Njc3NzYyIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDQ3NzkiCiAgICAgICBkPSJtIC01MTE5LjA3NDksLTg5MzcuMzcxMSBoIDMxMy43OTk2IGwgLTEyLjI5MDgsLTI3LjQ4MTUgYyAtMzkuNDg2NCwtODguMjg2MSAtOTQuMDEzNCwtMTY5LjY1NDUgLTE0OC41MjIsLTIyMS42MzE3IC0zNy42MDQ3LC0zNS44NTg4IC03NC4zOTEsLTU3LjY2MSAtMTE0LjA2NCwtNjcuNjAzIC0yMC4wNjU4LC01LjAyNzcgLTU1LjYxOTcsLTQuNjYxMyAtNzUuNzc1OSwwLjc4MjQgLTcxLjQ0NDQsMTkuMjk2MiAtMTQxLjYyMjYsODIuNzEzNSAtMjA5LjIzMTMsMTg5LjA3NTUgLTI0Ljk3MjEsMzkuMjg2MiAtNTMuMTY1Miw5MS43MDIzIC02NS45ODY2LDEyMi42ODEyIGwgLTEuNzI4Nyw0LjE3NzEgeiBtIC0yMzAuNTIxOSwtNjIuOTY5NiBjIDc4LjY3NSwtMTM4LjgyNyAxNjguODY2NywtMjE4Ljg4MjcgMjM5Ljg0NzMsLTIxMi44OTMzIDM2LjcyOTgsMy4wOTg2IDcyLjMzOTQsMjEuODc5OSAxMTAuMzY1OCw1OC4yMDkzIDMwLjkxOCwyOS41MzcyIDYzLjMzMDgsNzEuMzI3NCA5MS42MTYyLDExOC4xMjE0IDEyLjk2LDIxLjQ0MDggMjguNDcyMyw1MC4zNjA1IDI4LjQ3MjMsNTMuMDgxNSAwLDEuMjc2OCAtNzguMDYyNiwxLjg5MzQgLTIzOS42NjU1LDEuODkzNCAtMTMxLjgxNiwwIC0yMzkuNjY1NywtMC41NTc3IC0yMzkuNjY1NywtMS4yMzkxIDAsLTAuNjgyNCA0LjA2MzIsLTguNDA5OSA5LjAyOTYsLTE3LjE3MzIgeiIKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eTowLjk4ODIwNzU5O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDozMC4wNDM2ODU5MTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICA8cGF0aAogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJzc2NjY2NjcyIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDQ3ODEiCiAgICAgICBkPSJtIC01MTIwLjQ2MTMsLTg5NzQuODE2NiBjIDE0MC40MTI0LDAgMjU1LjI5NTEsLTAuNjA4NiAyNTUuMjk1MSwtMS4zNTQyIDAsLTMuMjgyNyAtMzAuNzM1OSwtNTcuOTA3MSAtNDUuOTgwNCwtODEuNzE3MiAtNTkuNDQwNiwtOTIuODM0MSAtMTI2LjI1ODUsLTE1Mi45NTU5IC0xODMuMDI2MSwtMTY0LjY3OTggLTQ3LjI4MTgsLTkuNzY1MSAtOTUuMjc0MSwxMC4yOTMgLTE0OS4wNjgzLDYyLjI5OTUgLTQxLjExNywzOS43NTA0IC04NC4zMTQzLDk4LjgxNDEgLTEyMi41OTgxLDE2Ny42MjY0IGwgLTkuOTE3MSwxNy44MjUzIHoiCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmYzk1MTtmaWxsLW9wYWNpdHk6MC45ODgyMzUyOTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MzAuODcwMDE0MTk7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgPHBhdGgKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTAuNjEzNTczMzQsLTEuMDAwMDY3NiwwLC04NDE3LjQ4ODgsLTk1MzMuODcwMSkiCiAgICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIC0zMTQ3LjI3OTgsLTMyOTkuMzA5NSBIIC01NDgxLjAyOTYiCiAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDc2MiIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDQ3NjAiCiAgICAgICBkPSJNIC0zMTQ3LjI3OTgsLTMyOTkuMzA5NSBIIC01NDgxLjAyOTYiCiAgICAgICBzdHlsZT0iZmlsbDojZmZjOTUxO2ZpbGwtb3BhY2l0eTowLjk4ODIzNTI5O2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojZmZjOTUxO3N0cm9rZS13aWR0aDozOC4yMTYzNTA1NjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eTowLjk4ODIzNTI5IiAvPgogICAgPHBhdGgKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDg3NCIKICAgICAgIGQ9Im0gLTUxMTcuOTU1NiwtNjE3MC44NTY1IDRlLTQsMzE4MC4wMDAyIgogICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC4wNDk5MTI3MTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eTowIiAvPgogIDwvZz4KPC9zdmc+Cg==" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="6" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{f8f5c9c2-4843-43a2-a40a-5cba5ef66210}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIHg9IjBweCIKICAgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgNTg0NS41MzA2IDEyMjIyLjUiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1wcm9qZWN0ZXVyLnN2ZyIKICAgd2lkdGg9IjE1NC42NjI5OWNtIgogICBoZWlnaHQ9IjMyMy4zODY5OWNtIj4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NTEwMiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDUxMDAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTA5OCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDUwOTIiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A1MDkwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDUwODgiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTI3IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDkyNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTIzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkxNSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MDkiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0OTA3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0ODk5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDg5NyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODk1IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDg4OSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4ODciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg4NSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODgxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDg3NSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4NzMiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg3MSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4NjUiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODYzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDg1NyIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4NTUiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0ODQ5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDg0NyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4NDEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODM5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDgyOSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ4MjciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0ODIzIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDgyMSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4MDkiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODA3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3ODciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzgxIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc3OSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzc3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc3NyIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NzUiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTEzIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExNSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTIxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTciCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODExMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTA5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5NiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODE0MiIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MTA4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODExMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTgiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MTAwIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA5MiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwOTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDg2IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDc7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwODAiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgtMCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OC0yLTIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0NzgxIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc4MyIKICAgICAgIHgxPSI5NTkuNDk5OTQiCiAgICAgICB5MT0iMjU5Ny43OTUyIgogICAgICAgeDI9Ijk2MC40OTk5NCIKICAgICAgIHkyPSIyNTk3Ljc5NTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDg5OSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MDEiCiAgICAgICB4MT0iNDc5LjgyMzAzIgogICAgICAgeTE9IjExNTcuNzk1MiIKICAgICAgIHgyPSI2NDAwLjE3NjgiCiAgICAgICB5Mj0iMTE1Ny43OTUyIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjUxNDI4MzdlLTUpIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0OTA5IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkxMSIKICAgICAgIHgxPSIyNzk5Ljg4ODkiCiAgICAgICB5MT0iMzE1Ny43OTUyIgogICAgICAgeDI9IjYzMjAuMTEwNCIKICAgICAgIHkyPSIzMTU3Ljc5NTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkyNyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MjkiCiAgICAgICB4MT0iMjU0MS4wNzMiCiAgICAgICB5MT0iNDE1Ny43OTQ5IgogICAgICAgeDI9IjY0MTguOTI2MyIKICAgICAgIHkyPSI0MTU3Ljc5NDkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDkyNyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ5MjktOCIKICAgICAgIHgxPSIyNTQxLjA3MyIKICAgICAgIHkxPSI0MTU3Ljc5NDkiCiAgICAgICB4Mj0iNjQxOC45MjYzIgogICAgICAgeTI9IjQxNTcuNzk0OSIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkyMy0wIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTktMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTE1LTkiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0OTI3IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkxMS0yIgogICAgICAgeDE9IjI3OTkuODg4OSIKICAgICAgIHkxPSIzMTU3Ljc5NTIiCiAgICAgICB4Mj0iNjMyMC4xMTA0IgogICAgICAgeTI9IjMxNTcuNzk1MiIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkwNS01IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4ODEtOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q1MDk4LTciCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NTA4OC02IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MjMtNyIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTE5LTE4IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTUtMCIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTA1LTQiCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg5NS00IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4ODUtNiIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0ODgxLTIiCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg3MS0wIgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3ODctMyIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzc3LTciCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEyMS0zIgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTctMiIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTEzLTgiCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEwOS01IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMDAtOCIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk2LTMiCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OC0xIgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDYtMyIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTQyLTIiCiAgICAgICBlZmZlY3Q9InNwaXJvIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEzOC0wIgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMzQtNyIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTAtOCIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTItNyIKICAgICAgIGVmZmVjdD0ic3Bpcm8iIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTItMi02IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MjMtMC0xIgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTktMS0yIgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTUtOS02IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MDUtNS00IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4ODEtOC00IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxMDI0IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjcwNSIKICAgICBpZD0ibmFtZWR2aWV3MTIiCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4wMjQ5MDU2MzgiCiAgICAgaW5rc2NhcGU6Y3g9Ii0zNjA3LjczODEiCiAgICAgaW5rc2NhcGU6Y3k9IjY0NTUuMDI5MyIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iMTI0ODAsLTY0MCIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMTI0ODAsNDQ4MCIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODA3NiIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8ZWxsaXBzZQogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDo1LjMxNDk2MDQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOmJldmVsO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBpZD0icGF0aDgxMDQiCiAgICAgY3g9Ii01MDYyLjM2MDQiCiAgICAgY3k9IjU0MjEuNTM4MSIKICAgICByeD0iNy41IgogICAgIHJ5PSIyLjUiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0NzgzKTtzdHJva2Utd2lkdGg6MC45ODI2NzcxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA4NDI5LjU4MTEsNDM3My4wMzI0IFYgMTgxMy4wMzI5IgogICAgIGlkPSJwYXRoNDc3NSIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0Nzc3IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gODQyOS41ODExLDQzNzMuMDMyNCBWIDE4MTMuMDMyOSIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTAsNjM3Mi41ODY2LDYyMDguODI4NykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEzLjM4NTgyNjExO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBpZD0icGF0aDQ4MTMiCiAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgIHNvZGlwb2RpOmN4PSItMjY4Mi40MjYiCiAgICAgc29kaXBvZGk6Y3k9IjE5MjQuOTc5NCIKICAgICBzb2RpcG9kaTpyeD0iOTE2LjUyNzE2IgogICAgIHNvZGlwb2RpOnJ5PSIxNjIuMDQ0OCIKICAgICBzb2RpcG9kaTpzdGFydD0iMCIKICAgICBzb2RpcG9kaTplbmQ9IjAuNzQ0ODgwMzUiCiAgICAgZD0ibSAtMTc2NS44OTg5LDE5MjQuOTc5NCBhIDkxNi41MjcxNiwxNjIuMDQ0OCAwIDAgMSAtMjQyLjcyNDgsMTA5Ljg0NzUgbCAtNjczLjgwMjMsLTEwOS44NDc1IHoiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICA8ZWxsaXBzZQogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMzg1ODI2MTE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGlkPSJwYXRoNDgzMyIKICAgICBjeD0iLTI0NzguODQxMSIKICAgICBjeT0iMjA3Ny42NjE5IgogICAgIHJ4PSIyNTcuNTE3MjciCiAgICAgcnk9IjMyLjgzMzgwNSIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTApIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMy4zODU4MjYxMTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgaWQ9InBhdGg0ODM1IgogICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICBzb2RpcG9kaTpjeD0iLTMxMDEuODk1OCIKICAgICBzb2RpcG9kaTpjeT0iMjA4MC43ODIiCiAgICAgc29kaXBvZGk6cng9IjQ5Ny4wNTcwNCIKICAgICBzb2RpcG9kaTpyeT0iNi4yNDE2MjQ0IgogICAgIHNvZGlwb2RpOnN0YXJ0PSI0LjcxMzE3MDEiCiAgICAgc29kaXBvZGk6ZW5kPSI1LjAwMDcwNTYiCiAgICAgZD0ibSAtMzEwMS41MDc1LDIwNzQuNTQwNCBhIDQ5Ny4wNTcwNCw2LjI0MTYyNDQgMCAwIDEgMTQwLjk0NDMsMC4yNTc2IGwgLTE0MS4zMzI2LDUuOTg0IHoiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojZmZjOTUxO2ZpbGwtb3BhY2l0eTowLjk4ODIzNTI5O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxODguOTc2Mzc5Mzk7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGlkPSJwYXRoNDg2MSIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLC0xLDAsMCwwKSIKICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgc29kaXBvZGk6Y3g9Ii02MDQ2LjA3MTMiCiAgICAgc29kaXBvZGk6Y3k9Ii0zMTk4LjQ4NTEiCiAgICAgc29kaXBvZGk6cng9IjIyNTAuOTY4NSIKICAgICBzb2RpcG9kaTpyeT0iMTgzMC4wMzk5IgogICAgIHNvZGlwb2RpOnN0YXJ0PSI0LjcxNjg0NzkiCiAgICAgc29kaXBvZGk6ZW5kPSIxLjU3MDEyMDkiCiAgICAgZD0ibSAtNjAzNi4wMzQ0LC01MDI4LjUwNjggYSAyMjUwLjk2ODUsMTgzMC4wMzk5IDAgMCAxIDIyNDAuOTI3NiwxODMzLjQ4MzcgMjI1MC45Njg1LDE4MzAuMDM5OSAwIDAgMSAtMjI0OS40NDQxLDE4MjYuNTc3NSBsIC0xLjUyMDQsLTE4MzAuMDM5NSB6IiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMjUxOC45MTUsNTExNi4yNDE0IHYgMCIKICAgICBpZD0icGF0aDQ4NzkiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDg4MSIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTE4LjkxNSw1MTE2LjI0MTQgYyAzMS44NDcsLTk1LjA3MSAzMS44NDcsLTk1LjA3MSAwLDAiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwLDYzNzIuNTg2Niw2MjA4LjgyODcpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ5MTEpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDg5MTguNzQ3MywzNTE2LjI0MTQgMzU2Mi43MDc3LDgwMCIKICAgICBpZD0icGF0aDQ5MDMiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkwNSIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDg5MTguNzQ3MywzNTE2LjI0MTQgMzU2Mi43MDc3LDgwMCIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTAsNjM3Mi41ODY2LDYyMDguODI4NykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEyNTE4LjkxNSw1NTk2LjI0MTQgLTg4MCwtNDAwIgogICAgIGlkPSJwYXRoNDkxMyIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE1IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCBjIC0yMDAuMzY3LC0yNjkuNzgyIC01NzAuMDIyLC0zMDQuNTg2IC04ODAsLTQwMCIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTAsNjM3Mi41ODY2LDYyMDguODI4NykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEyNTE4LjkxNSw1NTk2LjI0MTQgLTgwLC04MCIKICAgICBpZD0icGF0aDQ5MTciCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkxOSIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTE4LjkxNSw1NTk2LjI0MTQgYyAtMTcuMTM2LC0zMy41OTUgLTUzLjMzNCwtNTMuMzMzIC04MCwtODAiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwLDYzNzIuNTg2Niw2MjA4LjgyODcpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ5MjkpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDg2NzguOTE0OCw0MjM2LjI0MTQgMzg0MC4wMDAyLDEzNjAiCiAgICAgaWQ9InBhdGg0OTIxIgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MjMiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSA4Njc4LjkxNDgsNDIzNi4yNDE0IDM4NDAuMDAwMiwxMzYwIgogICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCw2MzcyLjU4NjYsNjIwOC44Mjg3KSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTI1MTEuNzk2LDk1OC43NzMyOSB2IDAiCiAgICAgaWQ9InBhdGg0ODc5LTQiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDg4MS04IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDk1OC43NzMyOSBjIDMxLjg0Nyw5NS4wNzE2MSAzMS44NDcsOTUuMDcxNjEgMCwwIgogICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCw2MzcyLjU4NjYsNjIwOC44Mjg3KSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ0OTExLTIpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDg5MTEuNjI4OCwyNTU4Ljc3MzIgMTI0NzQuMzM2LDE3NTguNzczMyIKICAgICBpZD0icGF0aDQ5MDMtOSIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTA1LTUiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0iTSA4OTExLjYyODgsMjU1OC43NzMyIDEyNDc0LjMzNiwxNzU4Ljc3MzMiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwLDYzNzIuNTg2Niw2MjA4LjgyODcpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IC04ODAsMzk5Ljk5OTgiCiAgICAgaWQ9InBhdGg0OTEzLTkiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkxNS05IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDQ3OC43NzM0OSBjIC0yMDAuMzY3LDI2OS43ODE4IC01NzAuMDIyLDMwNC41ODYyIC04ODAsMzk5Ljk5OTgiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwLDYzNzIuNTg2Niw2MjA4LjgyODcpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IC04MCw4MCIKICAgICBpZD0icGF0aDQ5MTctNCIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE5LTEiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IGMgLTE3LjEzNSwzMy41OTQ3IC01My4zMzMsNTMuMzMzNCAtODAsODAiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwLDYzNzIuNTg2Niw2MjA4LjgyODcpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ5MjktOCk7c3Ryb2tlLXdpZHRoOjExMy4zODU4MTg0ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gODY3MS43OTYzLDE4MzguNzczMyAxMjUxMS43OTYsNDc4Ljc3MzQ5IgogICAgIGlkPSJwYXRoNDkyMS0yIgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MjMtMCIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDg2NzEuNzk2MywxODM4Ljc3MzMgMTI1MTEuNzk2LDQ3OC43NzM0OSIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTAsNjM3Mi41ODY2LDYyMDguODI4NykiIC8+CiAgPGVsbGlwc2UKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEzLjM4NTgyNjExO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBpZD0icGF0aDUxMDYiCiAgICAgY3g9Ii0xMjE0Mi41IgogICAgIGN5PSIxNTk5Ljk5OTkiCiAgICAgcng9Ijc5Ljk5OTk5MiIKICAgICByeT0iMTU5OS45OTk5IgogICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiIC8+CiAgPGVsbGlwc2UKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEzLjM4NTgyNjExO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBpZD0icGF0aDUxMDgiCiAgICAgY3g9Ii0xMjEwMi41IgogICAgIGN5PSIzMTE5Ljk5OTgiCiAgICAgcng9IjM5Ljk5OTk5NiIKICAgICByeT0iNzkuOTk5OTkyIgogICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCkiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45ODgyMDc1OTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRkNGQ0ZDtzdHJva2Utd2lkdGg6OS40NDg4MTkxNjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiCiAgICAgaWQ9InBhdGg2Njc4IgogICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICBzb2RpcG9kaTpjeD0iLTEyMDYyLjUiCiAgICAgc29kaXBvZGk6Y3k9IjMxOTkuOTk5OCIKICAgICBzb2RpcG9kaTpyeD0iOS45OTk5OTkiCiAgICAgc29kaXBvZGk6cnk9IjkuOTk5OTk5IgogICAgIHNvZGlwb2RpOnN0YXJ0PSIwIgogICAgIHNvZGlwb2RpOmVuZD0iNi4yODE3MzY3IgogICAgIHNvZGlwb2RpOm9wZW49InRydWUiCiAgICAgZD0ibSAtMTIwNTIuNSwzMTk5Ljk5OTggYSA5Ljk5OTk5OSw5Ljk5OTk5OSAwIDAgMSAtOS45OTYsMTAgOS45OTk5OTksOS45OTk5OTkgMCAwIDEgLTEwLjAwNCwtOS45OTI4IDkuOTk5OTk5LDkuOTk5OTk5IDAgMCAxIDkuOTg5LC0xMC4wMDcyIDkuOTk5OTk5LDkuOTk5OTk5IDAgMCAxIDEwLjAxMSw5Ljk4NTUiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTMuMzg1ODI2MTE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGQ9Ik0gNDE4OS44NjYsNTk2Mi4xNDk4IEMgMzc5Mi4yMTc5LDU5NzAuODY3NCAyOTk3Ljg2ODYsNTk3OCAyNDI0LjY0NTEsNTk3OCBIIDEzODIuNDIwNSBsIDE1LjYzMDksLTE4NS43NjU0IGMgNTQuMDgwOCwtNjQyLjcyMDQgMzczLjYzMDcsLTEyNzAuOTY5NCA4MTUuMzUxOCwtMTYwMy4wMTcyIDMxMC4yNDAyLC0yMzMuMjExOCA1ODUuOTYzNSwtMzI4LjA1NDcgOTQ5LjY2OTIsLTMyNi42NjU5IDgzNy41NjI5LDMuMTk4MyAxNTYwLjA2ODYsNzUwLjgxMDEgMTcxNC4yNzQyLDE3NzMuODQzMSAxOS4zNzgxLDEyOC41NTc5IDM1LjI5NjQsMjUwLjg3ODQgMzUuMzc0LDI3MS44MjM0IDAuMTE5OSwzMy42MDg0IC04NC43ODczLDM5Ljk0MzYgLTcyMi44NTQ2LDUzLjkzMTggeiIKICAgICBpZD0icGF0aDUxMzciCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICA8cGF0aAogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNmZmM5NTE7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1Mjk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjc1LjU5MDU1MzI4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBzdHJva2UgZmlsbCIKICAgICBkPSJtIDYxOS40NTQ1Myw4OS42NTkzIGMgLTAuNTkxNCw0Ni43MzggMTMxMS44MTM5NywzNzgzLjM5MDEgMTMzOC4yMzI0NywzODA5LjgwODcgNi4yMzIsNi4yMzIgMjcuOTI4Nyw0Ljk2MDMgNDguMjE0OCwtMi44MjQyIDMxLjM1MzYsLTEyLjAzMTUgMjMuMTAzNSwtNTIuOTA1NSAtNTUuMDI5MywtMjcyLjYxOTEgQyAxOTAwLjMxOTgsMzQ4MS44Njc5IDE1OTYuNDk1LDI2MjMuOTkzMyAxMjc1LjcwNjUsMTcxNy42MzgzIDkxOC40OTUxNSw3MDguMzc0MyA2ODAuNjY0MzksNjkuNzIwMyA2NjIuMDMyNjYsNjkuNzIwMyBjIC0xNi43MzE5NSwwIC0zNS44NjEzMyw4LjggLTQyLjUwOTc3LDE5LjU1OCAtMC4wNDI5LDAuMDcgLTAuMDY2LDAuMTk4IC0wLjA2ODQsMC4zODEgeiBtIDEyNTguNjU2MjcsMzQuOTY5IGMgLTMuNjA5OCwzLjYwOSA3NzYuMTkxNywzNTAxLjQ0ODMgNzkwLjE3NTgsMzU0NC4zNzI5IDIuMzQxMSw3LjE4NjIgMTYuOTMxNSw2LjMzMDkgNTQuODM0LC0zLjIxNjggNDAuNDA3OSwtMTAuMTc4OCA1MC43NTYyLC0xNS45OTYgNDcuNTcwMywtMjYuNzQwMiAtNC4wNDE1LC0xMy42MjkyIC03ODEuNjY0MiwtMzQ3Ni42MTk5IC03OTAuMTUyNCwtMzUxOC43ODg5IGwgLTQuNDI5NywtMjIuMDA2IC00Ni42NTQzLDEwLjg0NCBjIC0yNS42NjAxLDUuOTY1IC00OC43NjQ1LDEyLjk1NSAtNTEuMzQzNywxNS41MzUgeiIKICAgICBpZD0icGF0aDQ4ODMiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmYzk1MTtmaWxsLW9wYWNpdHk6MC45ODgyMzUyOTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NzUuNTkwNTUzMjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBkPSJtIDQxMjkuNTI2NywxOTA0Ljk3MzMgYyAtMjE4LjQxMDYsOTY5LjQ5MTIgLTM5Ny4yMTYxLDE3NjMuMzIxIC0zOTcuMzQ1NCwxNzY0LjA2NTggLTAuNDY1MywyLjY3OTIgLTk0LjMyNDcsLTIwLjU4NSAtOTguMDQwMSwtMjQuMzAwNCAtNC40NTc2LC00LjQ1NzYgNzg4LjA1ODUsLTM1MzguNzc2NCA3OTUuMDkzLC0zNTQ1LjgxMDQgMi42MTY3LC0yLjYxNyAyNi4wMzQ2LC0wLjM0MSA1Mi4wMzk4LDUuMDU4IDM1LjQ5LDcuMzY3IDQ3LjA0MjksMTMuMzY0IDQ2LjMyMjcsMjQuMDQ1IC0wLjUyNzgsNy44MjYgLTE3OS42NTkzLDgwNy40NSAtMzk4LjA3LDE3NzYuOTQyIHoiCiAgICAgaWQ9InBhdGg0ODg4IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmYzk1MTtmaWxsLW9wYWNpdHk6MC45ODgyMzUyOTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NzUuNTkwNTUzMjg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjI7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBkPSJNIDUxNTkuNjg3MywxOTE3LjY4NTMgQyA0ODAzLjYwMzksMjkyMi4xMzQgNDQ5OC40NzcyLDM3ODIuNjk2OCA0NDgxLjYyNzksMzgzMC4wNDY1IGwgLTMwLjYzNTIsODYuMDkwNSAtNDkuODI4NywtMTYuODI3NCAtNDkuODI4OCwtMTYuODI3NSAzNy44NDI0LC0xMDcuMzk4NiBjIDIwLjgxMzIsLTU5LjA2OTMgMzI2LjI0NzIsLTkyMS44MDMgNjc4Ljc0MjEsLTE5MTcuMTg2MiBsIDY0MC44OTk4LC0xODA5Ljc4NyA0OS42OTg5LDE3LjQxIGMgMjcuMzM0NCw5LjU3NiA0OS40NTAyLDE5LjMyIDQ5LjE0NjEsMjEuNjUyIC0wLjMwNDEsMi4zMzMgLTI5MS44OTM4LDgyNi4wNjMgLTY0Ny45NzcyLDE4MzAuNTEzIHoiCiAgICAgaWQ9InBhdGg0ODkwIgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="7" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{d921fa23-91b0-46d7-8cb7-b5991c3010fa}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCAxMzAwIDEzMDAiCiAgIGlkPSJzdmcyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzbWRldj1mb3llcl9tYXRfYV9kZXBvc2VyLnN2ZyIKICAgd2lkdGg9IjEzMGNtIgogICBoZWlnaHQ9IjEzMGNtIgogICBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3Ij4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NTE2OSIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDUxNjciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iVGFpbCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IlRhaWwiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8ZwogICAgICAgICBpZD0iZzQ4NjciCiAgICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoLTEuMikiCiAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLW9wYWNpdHk6MSI+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NTUiCiAgICAgICAgICAgZD0iTSAtMy44MDQ4Njc0LC0zLjk1ODUyMjcgMC41NDM1MjA5NCwwIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODAwMDAwMDE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0ODU3IgogICAgICAgICAgIGQ9Ik0gLTEuMjg2NjgzMiwtMy45NTg1MjI3IDMuMDYxNzA1MywwIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjAuODAwMDAwMDE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg0ODU5IgogICAgICAgICAgIGQ9Ik0gMS4zMDUzNTgyLC0zLjk1ODUyMjcgNS42NTM3NDY2LDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NjEiCiAgICAgICAgICAgZD0iTSAtMy44MDQ4Njc0LDQuMTc3NTgzOCAwLjU0MzUyMDk0LDAuMjE5NzQyMjYiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDQ4NjMiCiAgICAgICAgICAgZD0iTSAtMS4yODY2ODMyLDQuMTc3NTgzOCAzLjA2MTcwNTMsMC4yMTk3NDIyNiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjgwMDAwMDAxO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoNDg2NSIKICAgICAgICAgICBkPSJNIDEuMzA1MzU4Miw0LjE3NzU4MzggNS42NTM3NDY2LDAuMjE5NzQyMjYiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC44MDAwMDAwMTtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgIDwvZz4KICAgIDwvbWFya2VyPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0ODE1IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDgxMyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4MDkiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0ODA3IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODExMyIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgxMTUiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEyMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTE3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxMTMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEwOSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTAwIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTYiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTQ2IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgxNDIiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODEzOCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MTM0IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODEwOCIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eTowLjc2NDE3OTExOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgxMTAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDk4IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODEwMCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgwOTIiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MDk0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODA4NiIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDA3O3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDgwODgiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MDgwIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODA4MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q4MDk4LTAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0ODA5OC0yIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDgwOTgtMi0yIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxmaWx0ZXIKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlkPSJmaWx0ZXI4NDcyIj4KICAgICAgPGZlQmxlbmQKICAgICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgICBtb2RlPSJzY3JlZW4iCiAgICAgICAgIGluMj0iQmFja2dyb3VuZEltYWdlIgogICAgICAgICBpZD0iZmVCbGVuZDg0NzQiIC8+CiAgICA8L2ZpbHRlcj4KICAgIDxmaWx0ZXIKICAgICAgIGlua3NjYXBlOm1lbnUtdG9vbHRpcD0iSW4gYW5kIG91dCBnbG93IHdpdGggYSBwb3NzaWJsZSBvZmZzZXQgYW5kIGNvbG9yaXphYmxlIGZsb29kIgogICAgICAgaW5rc2NhcGU6bWVudT0iU2hhZG93cyBhbmQgR2xvd3MiCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iQ3V0b3V0IEdsb3ciCiAgICAgICBzdHlsZT0iY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOnNSR0IiCiAgICAgICBpZD0iZmlsdGVyODQ3OSI+CiAgICAgIDxmZU9mZnNldAogICAgICAgICBkeT0iMyIKICAgICAgICAgZHg9IjMiCiAgICAgICAgIGlkPSJmZU9mZnNldDg0ODEiIC8+CiAgICAgIDxmZUdhdXNzaWFuQmx1cgogICAgICAgICBzdGREZXZpYXRpb249IjMiCiAgICAgICAgIHJlc3VsdD0iYmx1ciIKICAgICAgICAgaWQ9ImZlR2F1c3NpYW5CbHVyODQ4MyIgLz4KICAgICAgPGZlRmxvb2QKICAgICAgICAgZmxvb2QtY29sb3I9InJnYigwLDAsMCkiCiAgICAgICAgIGZsb29kLW9wYWNpdHk9IjEiCiAgICAgICAgIHJlc3VsdD0iZmxvb2QiCiAgICAgICAgIGlkPSJmZUZsb29kODQ4NSIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGluPSJmbG9vZCIKICAgICAgICAgaW4yPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBvcGVyYXRvcj0iaW4iCiAgICAgICAgIHJlc3VsdD0iY29tcG9zaXRlIgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NDg3IiAvPgogICAgICA8ZmVCbGVuZAogICAgICAgICBpbj0iYmx1ciIKICAgICAgICAgaW4yPSJjb21wb3NpdGUiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIKICAgICAgICAgaWQ9ImZlQmxlbmQ4NDg5IiAvPgogICAgPC9maWx0ZXI+CiAgICA8ZmlsdGVyCiAgICAgICBpbmtzY2FwZTptZW51LXRvb2x0aXA9IkluIGFuZCBvdXQgZ2xvdyB3aXRoIGEgcG9zc2libGUgb2Zmc2V0IGFuZCBjb2xvcml6YWJsZSBmbG9vZCIKICAgICAgIGlua3NjYXBlOm1lbnU9IlNoYWRvd3MgYW5kIEdsb3dzIgogICAgICAgaW5rc2NhcGU6bGFiZWw9IkN1dG91dCBHbG93IgogICAgICAgc3R5bGU9ImNvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpzUkdCIgogICAgICAgaWQ9ImZpbHRlcjg0OTEiPgogICAgICA8ZmVPZmZzZXQKICAgICAgICAgZHk9IjMiCiAgICAgICAgIGR4PSIzIgogICAgICAgICBpZD0iZmVPZmZzZXQ4NDkzIiAvPgogICAgICA8ZmVHYXVzc2lhbkJsdXIKICAgICAgICAgc3RkRGV2aWF0aW9uPSIzIgogICAgICAgICByZXN1bHQ9ImJsdXIiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjg0OTUiIC8+CiAgICAgIDxmZUZsb29kCiAgICAgICAgIGZsb29kLWNvbG9yPSJyZ2IoMCwwLDApIgogICAgICAgICBmbG9vZC1vcGFjaXR5PSIxIgogICAgICAgICByZXN1bHQ9ImZsb29kIgogICAgICAgICBpZD0iZmVGbG9vZDg0OTciIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICBpbj0iZmxvb2QiCiAgICAgICAgIGluMj0iU291cmNlR3JhcGhpYyIKICAgICAgICAgb3BlcmF0b3I9ImluIgogICAgICAgICByZXN1bHQ9ImNvbXBvc2l0ZSIKICAgICAgICAgaWQ9ImZlQ29tcG9zaXRlODQ5OSIgLz4KICAgICAgPGZlQmxlbmQKICAgICAgICAgaW49ImJsdXIiCiAgICAgICAgIGluMj0iY29tcG9zaXRlIgogICAgICAgICBtb2RlPSJub3JtYWwiCiAgICAgICAgIGlkPSJmZUJsZW5kODUwMSIgLz4KICAgIDwvZmlsdGVyPgogICAgPGZpbHRlcgogICAgICAgeT0iLTAuMjUiCiAgICAgICBoZWlnaHQ9IjEuNSIKICAgICAgIGlua3NjYXBlOm1lbnUtdG9vbHRpcD0iRGFya2VucyB0aGUgZWRnZSB3aXRoIGFuIGlubmVyIGJsdXIgYW5kIGFkZHMgYSBmbGV4aWJsZSBnbG93IgogICAgICAgaW5rc2NhcGU6bWVudT0iU2hhZG93cyBhbmQgR2xvd3MiCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iRGFyayBBbmQgR2xvdyIKICAgICAgIHN0eWxlPSJjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6c1JHQiIKICAgICAgIGlkPSJmaWx0ZXI4NTAzIj4KICAgICAgPGZlR2F1c3NpYW5CbHVyCiAgICAgICAgIHN0ZERldmlhdGlvbj0iNSIKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ2IgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NTA1IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgcmVzdWx0PSJyZXN1bHQ4IgogICAgICAgICBpbj0iU291cmNlR3JhcGhpYyIKICAgICAgICAgb3BlcmF0b3I9ImF0b3AiCiAgICAgICAgIGluMj0icmVzdWx0NiIKICAgICAgICAgaWQ9ImZlQ29tcG9zaXRlODUwNyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIHJlc3VsdD0icmVzdWx0OSIKICAgICAgICAgb3BlcmF0b3I9Im92ZXIiCiAgICAgICAgIGluMj0iU291cmNlQWxwaGEiCiAgICAgICAgIGluPSJyZXN1bHQ4IgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NTA5IiAvPgogICAgICA8ZmVDb2xvck1hdHJpeAogICAgICAgICB2YWx1ZXM9IjEgMCAwIDAgMCAwIDEgMCAwIDAgMCAwIDEgMCAwIDAgMCAwIDEgMCAiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0MTAiCiAgICAgICAgIGlkPSJmZUNvbG9yTWF0cml4ODUxMSIgLz4KICAgICAgPGZlQmxlbmQKICAgICAgICAgaW49InJlc3VsdDEwIgogICAgICAgICBtb2RlPSJub3JtYWwiCiAgICAgICAgIGluMj0icmVzdWx0NiIKICAgICAgICAgaWQ9ImZlQmxlbmQ4NTEzIiAvPgogICAgPC9maWx0ZXI+CiAgICA8ZmlsdGVyCiAgICAgICBzdHlsZT0iY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOnNSR0IiCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iRGFyayBBbmQgR2xvdyIKICAgICAgIGlkPSJmaWx0ZXI4NTc1IgogICAgICAgeT0iLTAuMjUiCiAgICAgICBoZWlnaHQ9IjEuNSIKICAgICAgIGlua3NjYXBlOm1lbnUtdG9vbHRpcD0iRGFya2VucyB0aGUgZWRnZSB3aXRoIGFuIGlubmVyIGJsdXIgYW5kIGFkZHMgYSBmbGV4aWJsZSBnbG93IgogICAgICAgaW5rc2NhcGU6bWVudT0iU2hhZG93cyBhbmQgR2xvd3MiPgogICAgICA8ZmVGbG9vZAogICAgICAgICBmbG9vZC1vcGFjaXR5PSIwLjMyNTQ5IgogICAgICAgICBmbG9vZC1jb2xvcj0icmdiKDAsMCwwKSIKICAgICAgICAgcmVzdWx0PSJmbG9vZCIKICAgICAgICAgaWQ9ImZlRmxvb2Q4NTc3IiAvPgogICAgICA8ZmVDb21wb3NpdGUKICAgICAgICAgaW49ImZsb29kIgogICAgICAgICBpbjI9IlNvdXJjZUdyYXBoaWMiCiAgICAgICAgIG9wZXJhdG9yPSJpbiIKICAgICAgICAgcmVzdWx0PSJjb21wb3NpdGUxIgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NTc5IiAvPgogICAgICA8ZmVHYXVzc2lhbkJsdXIKICAgICAgICAgaW49ImNvbXBvc2l0ZTEiCiAgICAgICAgIHN0ZERldmlhdGlvbj0iMyIKICAgICAgICAgcmVzdWx0PSJibHVyIgogICAgICAgICBpZD0iZmVHYXVzc2lhbkJsdXI4NTgxIiAvPgogICAgICA8ZmVPZmZzZXQKICAgICAgICAgZHg9IjYiCiAgICAgICAgIGR5PSI2IgogICAgICAgICByZXN1bHQ9Im9mZnNldCIKICAgICAgICAgaWQ9ImZlT2Zmc2V0ODU4MyIgLz4KICAgICAgPGZlQ29tcG9zaXRlCiAgICAgICAgIGluPSJTb3VyY2VHcmFwaGljIgogICAgICAgICBpbjI9Im9mZnNldCIKICAgICAgICAgb3BlcmF0b3I9Im92ZXIiCiAgICAgICAgIHJlc3VsdD0iZmJTb3VyY2VHcmFwaGljIgogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NTg1IiAvPgogICAgICA8ZmVDb2xvck1hdHJpeAogICAgICAgICByZXN1bHQ9ImZiU291cmNlR3JhcGhpY0FscGhhIgogICAgICAgICBpbj0iZmJTb3VyY2VHcmFwaGljIgogICAgICAgICB2YWx1ZXM9IjAgMCAwIC0xIDAgMCAwIDAgLTEgMCAwIDAgMCAtMSAwIDAgMCAwIDEgMCIKICAgICAgICAgaWQ9ImZlQ29sb3JNYXRyaXg4NjI3IiAvPgogICAgICA8ZmVHYXVzc2lhbkJsdXIKICAgICAgICAgaWQ9ImZlR2F1c3NpYW5CbHVyODYyOSIKICAgICAgICAgc3RkRGV2aWF0aW9uPSI1IgogICAgICAgICByZXN1bHQ9InJlc3VsdDYiCiAgICAgICAgIGluPSJmYlNvdXJjZUdyYXBoaWMiIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NjMxIgogICAgICAgICBpbjI9InJlc3VsdDYiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0OCIKICAgICAgICAgaW49ImZiU291cmNlR3JhcGhpYyIKICAgICAgICAgb3BlcmF0b3I9ImF0b3AiIC8+CiAgICAgIDxmZUNvbXBvc2l0ZQogICAgICAgICBpZD0iZmVDb21wb3NpdGU4NjMzIgogICAgICAgICBpbjI9ImZiU291cmNlR3JhcGhpY0FscGhhIgogICAgICAgICByZXN1bHQ9InJlc3VsdDkiCiAgICAgICAgIG9wZXJhdG9yPSJvdmVyIgogICAgICAgICBpbj0icmVzdWx0OCIgLz4KICAgICAgPGZlQ29sb3JNYXRyaXgKICAgICAgICAgaWQ9ImZlQ29sb3JNYXRyaXg4NjM1IgogICAgICAgICB2YWx1ZXM9IjEgMCAwIDAgMCAwIDEgMCAwIDAgMCAwIDEgMCAwIDAgMCAwIDEgMCAiCiAgICAgICAgIHJlc3VsdD0icmVzdWx0MTAiIC8+CiAgICAgIDxmZUJsZW5kCiAgICAgICAgIGlkPSJmZUJsZW5kODYzNyIKICAgICAgICAgaW4yPSJyZXN1bHQ2IgogICAgICAgICBpbj0icmVzdWx0MTAiCiAgICAgICAgIG1vZGU9Im5vcm1hbCIgLz4KICAgIDwvZmlsdGVyPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEwMjQiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlkPSJuYW1lZHZpZXcxMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjAzMTI1IgogICAgIGlua3NjYXBlOmN4PSIyNTM2LjU0MzIiCiAgICAgaW5rc2NhcGU6Y3k9IjIxMDUuODkzNCIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTptZWFzdXJlLXN0YXJ0PSI1NjE4LjY0LDIzODEuMzYiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjUxMzguOSwyNTYwIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNzYiCiAgICAgICBlbmFibGVkPSJ0cnVlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMTMwMCwxMzAwIgogICAgICAgb3JpZW50YXRpb249Ii0wLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU5NDIiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCwxMzAwIgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTk0NCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPGcKICAgICBpZD0iZzg0NzYiCiAgICAgc3R5bGU9ImZpbHRlcjp1cmwoI2ZpbHRlcjg0OTEpIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM4NS40MTMxNiwyODU2LjQ4OTIpIiAvPgogIDxnCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIGlua3NjYXBlOmxhYmVsPSJMYXllciAxIgogICAgIHN0eWxlPSJkaXNwbGF5Om5vbmU7ZmlsdGVyOnVybCgjZmlsdGVyODQ3MikiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwzMDE3LjI4MzUpIiAvPgogIDxnCiAgICAgaWQ9Imc5NDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMDA4NjcsMCwwLDAuOTkzMTA3OSwwLjEzNjQ1OTk0LDExLjgwMzE2NSkiPgogICAgPHBhdGgKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iNjc1LjgyMTkyIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItMzM3LjkwNTc0IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTk5OTcxNywtMC4wMDIzNzk1NywxLjkzMDEyNTZlLTQsMC45OTk5OTk5OCwwLDApIgogICAgICAgZD0ibSAxMTYwLjQ2ODIsMTI2MC40NTQ2IGEgMjExLjY2NzA1LDQxLjgwNzI3NCAwIDAgMSAtMjExLjU5MDQsNDEuODA3MyAyMTEuNjY3MDUsNDEuODA3Mjc0IDAgMCAxIC0yMTEuNzQzNjUsLTQxLjc3NyAyMTEuNjY3MDUsNDEuODA3Mjc0IDAgMCAxIDIxMS40MzcwMywtNDEuODM3NiAyMTEuNjY3MDUsNDEuODA3Mjc0IDAgMCAxIDIxMS44OTY4Miw0MS43NDY3IgogICAgICAgc29kaXBvZGk6b3Blbj0idHJ1ZSIKICAgICAgIHNvZGlwb2RpOmVuZD0iNi4yODE3MzY3IgogICAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgICBzb2RpcG9kaTpyeT0iNDEuODA3Mjc0IgogICAgICAgc29kaXBvZGk6cng9IjIxMS42NjcwNSIKICAgICAgIHNvZGlwb2RpOmN5PSIxMjYwLjQ1NDYiCiAgICAgICBzb2RpcG9kaTpjeD0iOTQ4LjgwMTE1IgogICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIgogICAgICAgaWQ9InBhdGg1MTYxIgogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuOTg4MjA3NTk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjU5LjYyNjA5NDgyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheToxNzguODc4Mjg3NDgsIDU5LjYyNjA5NTgzO3N0cm9rZS1kYXNob2Zmc2V0OjExMy4zODU4MjYxMTtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICA8ZWxsaXBzZQogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSI2MC4yNTEiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXg9Ii0zOC4xNjQ1NzgiCiAgICAgICByeT0iNjMxLjUwMDA2IgogICAgICAgcng9IjYzOS40NDczOSIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5OTk3MTcsLTAuMDAyMzc5NTcsMS45MzAxMjU2ZS00LDAuOTk5OTk5OTgsMCwwKSIKICAgICAgIGN5PSI2NDQuMTcwNjUiCiAgICAgICBjeD0iNjQ5LjE3ODEiCiAgICAgICBpZD0icGF0aDQ2NDciCiAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO2ZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MTkuNTg0NzYwNjc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5OjU4Ljc1NDI3NjA0LCAxOS41ODQ3NTg2ODtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogICAgPGVsbGlwc2UKICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteT0iNjAuMjUxIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSItMzguMTY0NTc4IgogICAgICAgcnk9IjQ4NC4wOTgzIgogICAgICAgcng9IjQ5MC4xOTA2MSIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5OTk3MTcsLTAuMDAyMzc5NTcsMS45MzAxMjU2ZS00LDAuOTk5OTk5OTgsMCwwKSIKICAgICAgIGN5PSI2NDQuMTcwNjUiCiAgICAgICBjeD0iNjQ5LjE3ODEiCiAgICAgICBpZD0icGF0aDk0MCIKICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7Y29sb3ItaW50ZXJwb2xhdGlvbjpzUkdCO2NvbG9yLWludGVycG9sYXRpb24tZmlsdGVyczpsaW5lYXJSR0I7c29saWQtY29sb3I6IzAwMDAwMDtzb2xpZC1vcGFjaXR5OjE7ZmlsbDojYTVhNWE1O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxOS40OTg0MDE2NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7Y29sb3ItcmVuZGVyaW5nOmF1dG87aW1hZ2UtcmVuZGVyaW5nOmF1dG87c2hhcGUtcmVuZGVyaW5nOmF1dG87dGV4dC1yZW5kZXJpbmc6YXV0bztlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIiAvPgogIDwvZz4KPC9zdmc+Cg==" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="8" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{2bdb84c2-2c32-4e1b-94fd-3fad770e69a7}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iODBjbSIKICAgaGVpZ2h0PSI2NDBjbSIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgODAwLjAwMDAzIDY0MDAuMDAwMiIKICAgaWQ9InN2ZzE4IgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9Zm95ZXJfc2ltcGxlX2FfZGVwb3Nlci5zdmciCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMi4yICg3MzJhMDFkYTYzLCAyMDIyLTEyLTA5KSIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMyMiI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NzQiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0NzcyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDc2MSIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ3NTkiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzY2IgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNhNWE1YTU7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc2NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0NzYyIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDc2NiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3NjgiCiAgICAgICB4MT0iLTI2NC40MjMwNyIKICAgICAgIHkxPSI4MzkuODY5MzgiCiAgICAgICB4Mj0iMTUyNC4xNjIyIgogICAgICAgeTI9IjgzOS44NjkzOCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09Im1hdHJpeCgwLC0wLjYxMzU3MzMyLC0xLjAwMDA2NzYsMCwtNDEzNC44NTA1LC0zMDUzLjkwNDEpIiAvPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMDAwMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE5MjAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMTAyNyIKICAgICBpZD0ibmFtZWR2aWV3MjAiCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vYmplY3QtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ2xvYmFsPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBzaG93Z3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtZWRnZS1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4wMjgyODQyNzEiCiAgICAgaW5rc2NhcGU6Y3g9Ii02MDk4Ljc5NiIKICAgICBpbmtzY2FwZTpjeT0iNDUwNy44MDU3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9Imc0OTEyIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjEzOS41NjUsMTI0MS4zOSIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMTY2MjguMiwxMjgzLjQ4IgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdGV4dC1iYXNlbGluZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXBlcnBlbmRpY3VsYXI9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtdGFuZ2VudGlhbD0iZmFsc2UiCiAgICAgc2NhbGUteD0iMTAiCiAgICAgZml0LW1hcmdpbi10b3A9IjAiCiAgICAgZml0LW1hcmdpbi1sZWZ0PSIwIgogICAgIGZpdC1tYXJnaW4tcmlnaHQ9IjAiCiAgICAgZml0LW1hcmdpbi1ib3R0b209IjAiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBzaG93Ym9yZGVyPSJ0cnVlIgogICAgIHZpZXdib3gtd2lkdGg9Ijc4LjI0MiIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ0NzcxIgogICAgICAgb3JpZ2lueD0iOS44MzEwOTMxZS0wNSIKICAgICAgIG9yaWdpbnk9IjEuODg3ODg0OWUtMDciIC8+CiAgICA8c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIxNjM4LjI2NTQsLTk2My42ODU2NCIKICAgICAgIG9yaWVudGF0aW9uPSIwLDEiCiAgICAgICBpZD0iZ3VpZGU4NzYiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGNjOmxpY2Vuc2UKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iIC8+CiAgICAgICAgPGRjOmNyZWF0b3I+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5qbWE8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOmNyZWF0b3I+CiAgICAgICAgPGRjOnB1Ymxpc2hlcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPkF6aW11dDwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cHVibGlzaGVyPgogICAgICAgIDxkYzpyaWdodHM+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT4oYylBemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnJpZ2h0cz4KICAgICAgPC9jYzpXb3JrPgogICAgICA8Y2M6TGljZW5zZQogICAgICAgICByZGY6YWJvdXQ9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LW5jLXNhLzQuMC8iPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNSZXByb2R1Y3Rpb24iIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rpc3RyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI05vdGljZSIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0F0dHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpwcm9oaWJpdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0NvbW1lcmNpYWxVc2UiIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rlcml2YXRpdmVXb3JrcyIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1NoYXJlQWxpa2UiIC8+CiAgICAgIDwvY2M6TGljZW5zZT4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxnCiAgICAgaWQ9Imc0OTEwIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDU1MTcuOTU2Miw5MzcwLjg1NjYpIgogICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iLTc4OS4wNjA4OSIKICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjQzMy4yNTExNiI+CiAgICA8ZwogICAgICAgaWQ9Imc0OTEyIgogICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSI1Ny4xMDcxODMiCiAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9Ii0xOC43MTI5MzIiCiAgICAgICBzdHlsZT0iZGlzcGxheTppbmxpbmUiPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2lzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbDtjb2xvci1pbnRlcnBvbGF0aW9uOnNSR0I7Y29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOmxpbmVhclJHQjtzb2xpZC1jb2xvcjojMDAwMDAwO3NvbGlkLW9wYWNpdHk6MTt2ZWN0b3ItZWZmZWN0Om5vbmU7ZmlsbDojYTVhNWE1O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDozMC4wNDM3O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICAgIGQ9Im0gLTU0NzcuOTAzOSwtODA0Mi42NzA5IGggLTMwLjA1MjMgbCAxNzcuNDM1OCw0MzkuNzE3MyA0MjMuMjA2NCwwLjM0MTIgMTc5LjM1NzgsLTQ0Mi4xMjc2IC0zMC4wNTI0LDAuMDUxIC0xNjkuNDkyNCw0MTIuMDI2MiAtMzgyLjcwMzUsLTAuMzA4MSB6IgogICAgICAgICBpZD0icGF0aDgiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2NjY2NjY2NjIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iY29sb3I6IzAwMDAwMDtmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6bm9ybWFsO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LXBvc2l0aW9uOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtdmFyaWFudC1hbHRlcm5hdGVzOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDAwMDtsZXR0ZXItc3BhY2luZzpub3JtYWw7d29yZC1zcGFjaW5nOm5vcm1hbDt0ZXh0LXRyYW5zZm9ybTpub25lO3dyaXRpbmctbW9kZTpsci10YjtkaXJlY3Rpb246bHRyO3RleHQtb3JpZW50YXRpb246bWl4ZWQ7ZG9taW5hbnQtYmFzZWxpbmU6YXV0bztiYXNlbGluZS1zaGlmdDpiYXNlbGluZTt0ZXh0LWFuY2hvcjpzdGFydDt3aGl0ZS1zcGFjZTpub3JtYWw7c2hhcGUtcGFkZGluZzowO2NsaXAtcnVsZTpub256ZXJvO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2lzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbDtjb2xvci1pbnRlcnBvbGF0aW9uOnNSR0I7Y29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOmxpbmVhclJHQjtzb2xpZC1jb2xvcjojMDAwMDAwO3NvbGlkLW9wYWNpdHk6MTt2ZWN0b3ItZWZmZWN0Om5vbmU7ZmlsbDojYTVhNWE1O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDozMC4wNDM3O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICAgIGQ9Im0gLTU0OTIuOTMwMSwtOTM1MC44NTYgYyAtOC4yOTgyLDAgLTE1LjAyNSw2LjcyMzYgLTE1LjAyNjEsMTUuMDE2NCB2IDEyOTMuMTY4NyBoIDMwLjA1MjMgdiAtMTI3OC4xNTIyIGggNzE3Ljc2NjYgbCAyLjEyODcsMTI3Ni4xMzQgMzAuMDUyNCwtMC4wNTEgLTIuMTUyMiwtMTI5MS4xMjUyIGMgLTAuMDE1LC04LjI4MzggLTYuNzM5LC0xNC45OTExIC0xNS4wMjgxLC0xNC45OTExIHoiCiAgICAgICAgIGlkPSJwYXRoMTAiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2NjY2NjY2NjY2MiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2ZvbnQtc3R5bGU6bm9ybWFsO2ZvbnQtdmFyaWFudDpub3JtYWw7Zm9udC13ZWlnaHQ6bm9ybWFsO2ZvbnQtc3RyZXRjaDpub3JtYWw7Zm9udC1zaXplOm1lZGl1bTtsaW5lLWhlaWdodDpub3JtYWw7Zm9udC1mYW1pbHk6c2Fucy1zZXJpZjtmb250LXZhcmlhbnQtbGlnYXR1cmVzOm5vcm1hbDtmb250LXZhcmlhbnQtcG9zaXRpb246bm9ybWFsO2ZvbnQtdmFyaWFudC1jYXBzOm5vcm1hbDtmb250LXZhcmlhbnQtbnVtZXJpYzpub3JtYWw7Zm9udC12YXJpYW50LWFsdGVybmF0ZXM6bm9ybWFsO2ZvbnQtZmVhdHVyZS1zZXR0aW5nczpub3JtYWw7dGV4dC1pbmRlbnQ6MDt0ZXh0LWFsaWduOnN0YXJ0O3RleHQtZGVjb3JhdGlvbjpub25lO3RleHQtZGVjb3JhdGlvbi1saW5lOm5vbmU7dGV4dC1kZWNvcmF0aW9uLXN0eWxlOnNvbGlkO3RleHQtZGVjb3JhdGlvbi1jb2xvcjojMDAwMDAwO2xldHRlci1zcGFjaW5nOm5vcm1hbDt3b3JkLXNwYWNpbmc6bm9ybWFsO3RleHQtdHJhbnNmb3JtOm5vbmU7d3JpdGluZy1tb2RlOmxyLXRiO2RpcmVjdGlvbjpsdHI7dGV4dC1vcmllbnRhdGlvbjptaXhlZDtkb21pbmFudC1iYXNlbGluZTphdXRvO2Jhc2VsaW5lLXNoaWZ0OmJhc2VsaW5lO3RleHQtYW5jaG9yOnN0YXJ0O3doaXRlLXNwYWNlOm5vcm1hbDtzaGFwZS1wYWRkaW5nOjA7Y2xpcC1ydWxlOm5vbnplcm87ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO3ZlY3Rvci1lZmZlY3Q6bm9uZTtmaWxsOiNhNWE1YTU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjMwLjA0Mzc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgICAgZD0ibSAtNTEyMC4wNjY1LC05MjUxLjAzMjggYyAtODQuNzg4NCwxLjIwODMgLTE1Ni4xNDY2LDc0LjY4MiAtMjA3Ljc0NTYsMTQ0Ljc1NTcgLTUxLjU5OSw3MC4wNzM3IC04My42ODI5LDEzOS42MjU0IC04My42ODI5LDEzOS42MjU0IGwgLTkuODQ3NywyMS4zMTI4IGggMjMuNDkwNSA1NzguODk0MiBsIC05LjEwODIsLTIwLjk5MSBjIDAsMCAtMzAuNjMxNCwtNzAuNzM1MSAtODEuNTYxOSwtMTQxLjQ2ODIgLTUwLjkzMDQsLTcwLjczMzEgLTEyMi44MzMxLC0xNDQuNDgyOSAtMjEwLjQzODQsLTE0My4yMzQ3IHogbSAwLjQyNzQsMzAuMDMxIGMgNjkuODAzMywtMC45OTQ2IDEzNi45MDAxLDYzLjA4NjQgMTg1LjYxNzEsMTMwLjc0NTQgMzIuODc2LDQ1LjY1ODcgNTYuMDU3NCw4OS44NTM4IDY4LjMxOTIsMTE0Ljg4NDYgaCAtNTA3LjcwODggYyAxMi44MzAzLC0yNC44MTczIDM2LjY1MTUsLTY4LjA3OSA2OS44MDYsLTExMy4xMDQzIDQ5LjQ5NTksLTY3LjIxNzYgMTE3LjEzNTksLTEzMS41NzMzIDE4My45NjY1LC0xMzIuNTI1NyB6IgogICAgICAgICBpZD0icGF0aDEyIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNzY2NjY2NzY2NjY2NzYyIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MC45ODgyMDg7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjMwLjA0Mzc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiCiAgICAgICAgIGQ9Im0gLTUxMjAuMDM1OSwtODkzNy4zNzA5IGggMzEzLjc5OTUgbCAtMTIuMjkwOCwtMjcuNDgxNSBjIC0zOS40ODY0LC04OC4yODYyIC05NC4wMTMzLC0xNjkuNjU0NSAtMTQ4LjUyMiwtMjIxLjYzMTggLTM3LjYwNDYsLTM1Ljg1ODggLTc0LjM5MSwtNTcuNjYwOSAtMTE0LjA2NCwtNjcuNjAyOCAtMjAuMDY1NywtNS4wMjc4IC01NS42MTk2LC00LjY2MTQgLTc1Ljc3NTgsMC43ODIzIC03MS40NDQ0LDE5LjI5NjMgLTE0MS42MjI3LDgyLjcxMzUgLTIwOS4yMzE0LDE4OS4wNzU1IC0yNC45NzIsMzkuMjg2MSAtNTMuMTY1MSw5MS43MDIzIC02NS45ODY1LDEyMi42ODExIGwgLTEuNzI4OCw0LjE3NzIgeiBtIC0yMzAuNTIyLC02Mi45Njk2IGMgNzguNjc1LC0xMzguODI3IDE2OC44NjY4LC0yMTguODgyNyAyMzkuODQ3NCwtMjEyLjg5MzMgMzYuNzI5NywzLjA5ODYgNzIuMzM5MywyMS44Nzk5IDExMC4zNjU3LDU4LjIwOTIgMzAuOTE4LDI5LjUzNzMgNjMuMzMwOCw3MS4zMjc0IDkxLjYxNjIsMTE4LjEyMTUgMTIuOTYwMSwyMS40NDA4IDI4LjQ3MjMsNTAuMzYwNSAyOC40NzIzLDUzLjA4MTUgMCwxLjI3NjggLTc4LjA2MjYsMS44OTM0IC0yMzkuNjY1NSwxLjg5MzQgLTEzMS44MTYsMCAtMjM5LjY2NTcsLTAuNTU3NyAtMjM5LjY2NTcsLTEuMjM5MiAwLC0wLjY4MjQgNC4wNjMyLC04LjQwOTggOS4wMjk2LC0xNy4xNzMxIHoiCiAgICAgICAgIGlkPSJwYXRoNDc3OSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjY2NjY2NjY2NjY2NjY3Nzc2MiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOiNhNWE1YTU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjMwLjA0Mzc7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiCiAgICAgICAgIGQ9Im0gLTUxMTkuOTIxOCwtODk3Ni44MjEgYyAxMzcuMzg3MywwIDI0OS43OTQ5LC0wLjU4OTMgMjQ5Ljc5NDksLTEuMzEwOSAwLC0zLjE3NzkgLTMwLjA3MzcsLTU2LjA1NjMgLTQ0Ljk5MDIsLTc5LjEwNTIgLTU4LjE1OTIsLTg5Ljg2NyAtMTIzLjUzODEsLTE0OC4wNjY4IC0xNzkuMDgyNiwtMTU5LjQxNjIgLTQ2LjI2MzEsLTkuNDUyNiAtOTMuMjIxMyw5Ljk2NDEgLTE0NS44NTYyLDYwLjMwODQgLTQwLjIzMTMsMzguNDc5OSAtODIuNDk4MSw5NS42NTU3IC0xMTkuOTU2NywxNjIuMjY4NyBsIC05LjcwMzcsMTcuMjU1MiB6IgogICAgICAgICBpZD0icGF0aDQ3ODEiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0ic3NjY2NjY3MiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gLTUxMTguOTE3MiwtNzYwMi43ODM0IHYgMTQzMS45MjY2IgogICAgICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDc2MiIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgaWQ9InBhdGg0NzYwIgogICAgICAgICBkPSJtIC01MTE4LjkxNzIsLTc2MDIuNzgzNCB2IDE0MzEuOTI2NiIKICAgICAgICAgc3R5bGU9ImZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDQ3NjgpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojYTVhNWE1O3N0cm9rZS13aWR0aDoyOS45MzYzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlkPSJwYXRoODc0IgogICAgICAgICBkPSJtIC01MTE4LjkxNjcsLTYxNzAuODU2NSA0ZS00LDMxODAuMDAwMSIKICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC4wNDk5MTI3O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjAiIC8+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="9" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SvgMarker" id="{a619e40f-bfaf-45e9-85ff-3e9433c5fd7b}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="118,50,173,255,rgb:0.46274509803921571,0.19607843137254902,0.67843137254901964,1" name="color"/>
            <Option type="QString" value="0" name="fixedAspectRatio"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIHg9IjBweCIKICAgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgNTI5MS4zMzg3IDEyMjgzLjQ2MyIKICAgaWQ9InN2ZzIiCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuNCAoNWRhNjg5YzMxMywgMjAxOS0wMS0xNCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9InNtZGV2PXByb2plY3RldXJfYV9kZXBvc2VyLnN2ZyIKICAgd2lkdGg9IjE0MGNtIgogICBoZWlnaHQ9IjMyNWNtIj4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGExNiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczE0Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50OTcxIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiM0ZDRkNGQ7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wOTY5IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkyNyIKICAgICAgIG9zYjpwYWludD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDQ5MjUiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkyMyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTE5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTA5IgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDkwNyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTA1IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ4ODEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzgxIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc3OSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0Nzc3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDc4MSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3ODMiCiAgICAgICB4MT0iOTU5LjQ5OTk0IgogICAgICAgeTE9IjI1OTcuNzk1MiIKICAgICAgIHgyPSI5NjAuNDk5OTQiCiAgICAgICB5Mj0iMjU5Ny43OTUyIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ5MDkiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTExIgogICAgICAgeDE9IjI3OTkuODg4OSIKICAgICAgIHkxPSIzMTU3Ljc5NTIiCiAgICAgICB4Mj0iNjMyMC4xMTA0IgogICAgICAgeTI9IjMxNTcuNzk1MiIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0OTI3IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkyOSIKICAgICAgIHgxPSIyNTQxLjA3MyIKICAgICAgIHkxPSI0MTU3Ljc5NDkiCiAgICAgICB4Mj0iNjQxOC45MjYzIgogICAgICAgeTI9IjQxNTcuNzk0OSIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ0OTI3IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDkyOS04IgogICAgICAgeDE9IjI1NDEuMDczIgogICAgICAgeTE9IjQxNTcuNzk0OSIKICAgICAgIHgyPSI2NDE4LjkyNjMiCiAgICAgICB5Mj0iNDE1Ny43OTQ5IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTIzLTAiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDkxOS0xIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ5MTUtOSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ5MjciCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0OTExLTIiCiAgICAgICB4MT0iMjc5OS44ODg5IgogICAgICAgeTE9IjMxNTcuNzk1MiIKICAgICAgIHgyPSI2MzIwLjExMDQiCiAgICAgICB5Mj0iMzE1Ny43OTUyIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0OTA1LTUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIiAvPgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDg4MS04IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxMDI0IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjcwNSIKICAgICBpZD0ibmFtZWR2aWV3MTIiCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4wMiIKICAgICBpbmtzY2FwZTpjeD0iMjI4MC40MjAzIgogICAgIGlua3NjYXBlOmN5PSI4MDM3LjM3NiIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMiIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9ImZhbHNlIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iMTI0ODAsLTY0MCIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMTI0ODAsNDQ4MCIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9ImZhbHNlIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNzYiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NDc4Myk7c3Ryb2tlLXdpZHRoOjAuOTgyNjc3MTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gODQyOS41ODExLDQzNzMuMDMyNCBWIDE4MTMuMDMyOSIKICAgICBpZD0icGF0aDQ3NzUiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDc3NyIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDg0MjkuNTgxMSw0MzczLjAzMjQgViAxODEzLjAzMjkiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTIuOTA0ODAwNDI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGlkPSJwYXRoNDgxMyIKICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgc29kaXBvZGk6Y3g9Ii0yNzUwLjQ0ODUiCiAgICAgc29kaXBvZGk6Y3k9IjEzNzguNDA5OCIKICAgICBzb2RpcG9kaTpyeD0iOTI0LjA1NzkyIgogICAgIHNvZGlwb2RpOnJ5PSIxNTkuMzYzMzkiCiAgICAgc29kaXBvZGk6c3RhcnQ9IjAiCiAgICAgc29kaXBvZGk6ZW5kPSIwLjc0NDg4MDM1IgogICAgIGQ9Im0gLTE4MjYuMzkwNiwxMzc4LjQwOTggYSA5MjQuMDU3OTIsMTU5LjM2MzM5IDAgMCAxIC0yNDQuNzE5MSwxMDguMDI5OSBsIC02NzkuMzM4OCwtMTA4LjAyOTkgeiIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTApIiAvPgogIDxlbGxpcHNlCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi45MDQ4MDA0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgaWQ9InBhdGg0ODMzIgogICAgIGN4PSItMjU0NS4xOTA5IgogICAgIGN5PSIxNTI4LjU2NiIKICAgICByeD0iMjU5LjYzMzE4IgogICAgIHJ5PSIzMi4yOTA0OTMiCiAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxMTIuOTA0ODAwNDI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6OTAuNjU3NTMxNzQ7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIgogICAgIGlkPSJwYXRoNDgzNSIKICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgc29kaXBvZGk6Y3g9Ii0zMTczLjM2NDciCiAgICAgc29kaXBvZGk6Y3k9IjE1MzEuNjM0NCIKICAgICBzb2RpcG9kaTpyeD0iNTAxLjE0MTE0IgogICAgIHNvZGlwb2RpOnJ5PSI2LjEzODM0MTkiCiAgICAgc29kaXBvZGk6c3RhcnQ9IjQuNzEzMTcwMSIKICAgICBzb2RpcG9kaTplbmQ9IjUuMDAwNzA1NiIKICAgICBkPSJtIC0zMTcyLjk3MzMsMTUyNS40OTYxIGEgNTAxLjE0MTE0LDYuMTM4MzQxOSAwIDAgMSAxNDIuMTAyNCwwLjI1MzMgbCAtMTQyLjQ5MzgsNS44ODUgeiIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTApIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNhNWE1YTU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjE4OC4xNzQ2ODI2MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgaWQ9InBhdGg0ODYxIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEsLTEsMCwwLDApIgogICAgIHNvZGlwb2RpOnR5cGU9ImFyYyIKICAgICBzb2RpcG9kaTpjeD0iLTYxNDEuNzMxOSIKICAgICBzb2RpcG9kaTpjeT0iLTI2MzAuODQzMyIKICAgICBzb2RpcG9kaTpyeD0iMjI2OS40NjM2IgogICAgIHNvZGlwb2RpOnJ5PSIxNzk5Ljc1NzYiCiAgICAgc29kaXBvZGk6c3RhcnQ9IjQuNzE2ODQ3OSIKICAgICBzb2RpcG9kaTplbmQ9IjEuNTcwMTIwOSIKICAgICBkPSJtIC02MTMxLjYxMjYsLTQ0MzAuNTgyOSBhIDIyNjkuNDYzNiwxNzk5Ljc1NzYgMCAwIDEgMjI1OS4zNDAyLDE4MDMuMTQ0MyAyMjY5LjQ2MzYsMTc5OS43NTc2IDAgMCAxIC0yMjY3LjkyNjcsMTc5Ni4zNTI1IGwgLTEuNTMyOCwtMTc5OS43NTcyIHoiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEyNTE4LjkxNSw1MTE2LjI0MTQgdiAwIgogICAgIGlkPSJwYXRoNDg3OSIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0ODgxIgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTguOTE1LDUxMTYuMjQxNCBjIDMxLjg0NywtOTUuMDcxIDMxLjg0NywtOTUuMDcxIDAsMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ5MTEpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDg5MTguNzQ3MywzNTE2LjI0MTQgMzU2Mi43MDc3LDgwMCIKICAgICBpZD0icGF0aDQ5MDMiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkwNSIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDg5MTguNzQ3MywzNTE2LjI0MTQgMzU2Mi43MDc3LDgwMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMjUxOC45MTUsNTU5Ni4yNDE0IC04ODAsLTQwMCIKICAgICBpZD0icGF0aDQ5MTMiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkxNSIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTE4LjkxNSw1NTk2LjI0MTQgYyAtMjAwLjM2NywtMjY5Ljc4MiAtNTcwLjAyMiwtMzA0LjU4NiAtODgwLC00MDAiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCAtODAsLTgwIgogICAgIGlkPSJwYXRoNDkxNyIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlua3NjYXBlOnBhdGgtZWZmZWN0PSIjcGF0aC1lZmZlY3Q0OTE5IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTguOTE1LDU1OTYuMjQxNCBjIC0xNy4xMzYsLTMzLjU5NSAtNTMuMzM0LC01My4zMzMgLTgwLC04MCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ5MjkpO3N0cm9rZS13aWR0aDoxMTMuMzg1ODE4NDg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDg2NzguOTE0OCw0MjM2LjI0MTQgMzg0MC4wMDAyLDEzNjAiCiAgICAgaWQ9InBhdGg0OTIxIgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MjMiCiAgICAgaW5rc2NhcGU6b3JpZ2luYWwtZD0ibSA4Njc4LjkxNDgsNDIzNi4yNDE0IDM4NDAuMDAwMiwxMzYwIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEyNTExLjc5Niw5NTguNzczMjkgdiAwIgogICAgIGlkPSJwYXRoNDg3OS00IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ4ODEtOCIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJtIDEyNTExLjc5Niw5NTguNzczMjkgYyAzMS44NDcsOTUuMDcxNjEgMzEuODQ3LDk1LjA3MTYxIDAsMCIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDQ5MTEtMik7c3Ryb2tlLXdpZHRoOjExMy4zODU4MTg0ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gODkxMS42Mjg4LDI1NTguNzczMiAxMjQ3NC4zMzYsMTc1OC43NzMzIgogICAgIGlkPSJwYXRoNDkwMy05IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgaW5rc2NhcGU6cGF0aC1lZmZlY3Q9IiNwYXRoLWVmZmVjdDQ5MDUtNSIKICAgICBpbmtzY2FwZTpvcmlnaW5hbC1kPSJNIDg5MTEuNjI4OCwyNTU4Ljc3MzIgMTI0NzQuMzM2LDE3NTguNzczMyIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xLjAwODIxNjYsMC45ODM0NTI2NCwwLC0zNTMuNjY3OSwxMjczMC43NzMpIiAvPgogIDxwYXRoCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMjUxMS43OTYsNDc4Ljc3MzQ5IC04ODAsMzk5Ljk5OTgiCiAgICAgaWQ9InBhdGg0OTEzLTkiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkxNS05IgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDQ3OC43NzM0OSBjIC0yMDAuMzY3LDI2OS43ODE4IC01NzAuMDIyLDMwNC41ODYyIC04ODAsMzk5Ljk5OTgiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMS4wMDgyMTY2LDAuOTgzNDUyNjQsMCwtMzUzLjY2NzksMTI3MzAuNzczKSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTI1MTEuNzk2LDQ3OC43NzM0OSAtODAsODAiCiAgICAgaWQ9InBhdGg0OTE3LTQiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkxOS0xIgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Im0gMTI1MTEuNzk2LDQ3OC43NzM0OSBjIC0xNy4xMzUsMzMuNTk0NyAtNTMuMzMzLDUzLjMzMzQgLTgwLDgwIgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NDkyOS04KTtzdHJva2Utd2lkdGg6MTEzLjM4NTgxODQ4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA4NjcxLjc5NjMsMTgzOC43NzMzIDEyNTExLjc5Niw0NzguNzczNDkiCiAgICAgaWQ9InBhdGg0OTIxLTIiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICBpbmtzY2FwZTpwYXRoLWVmZmVjdD0iI3BhdGgtZWZmZWN0NDkyMy0wIgogICAgIGlua3NjYXBlOm9yaWdpbmFsLWQ9Ik0gODY3MS43OTYzLDE4MzguNzczMyAxMjUxMS43OTYsNDc4Ljc3MzQ5IgogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsLTEuMDA4MjE2NiwwLjk4MzQ1MjY0LDAsLTM1My42Njc5LDEyNzMwLjc3MykiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MTEyLjkwNDgwMDQyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBkPSJtIDM2MDUuODE4Myw2MDU3LjEyMDUgYyAtMzkxLjA2OCw4Ljc4OTMgLTExNzIuMjcyLDE1Ljk4MDUgLTE3MzYuMDEwNywxNS45ODA1IEggODQ0LjgyOSBsIDE1LjM3MjMsLTE4Ny4yOTE5IGMgNTMuMTg1OSwtNjQ4LjAwMTMgMzY3LjQ0ODEsLTEyODEuNDEyNCA4MDEuODU5OCwtMTYxNi4xODg1IDMwNS4xMDY2LC0yMzUuMTI4IDU3Ni4yNjc0LC0zMzAuNzUwMSA5MzMuOTU0MiwtMzI5LjM1IDgyMy43MDQsMy4yMjQ3IDE1MzQuMjU0LDc1Ni45NzkyIDE2ODUuOTA4LDE3ODguNDE4MSAxOS4wNTgsMTI5LjYxNDIgMzQuNzEzLDI1Mi45Mzk3IDM0Ljc4OCwyNzQuMDU2OSAwLjExOSwzMy44ODQ1IC04My4zODQsNDAuMjcxNyAtNzEwLjg5Myw1NC4zNzQ5IHoiCiAgICAgaWQ9InBhdGg1MTM3IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgPHBhdGgKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojYTVhNWE1O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDo3NS4yNjk4NzQ1NztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDo5MC42NTc1MzE3NDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgc3Ryb2tlIGZpbGwiCiAgICAgZD0ibSA5NC40ODgyLDEzNi4zNzgxMyBjIC0wLjU4MTcsNDcuMTIyMDMgMTI5MC4xMDY4LDM4MTQuNDc2NjcgMTMxNi4wODgyLDM4NDEuMTEyMzcgNi4xMjg5LDYuMjgzMiAyNy40NjY1LDUuMDAxIDQ3LjQxNywtMi44NDc0IDMwLjgzNDcsLTEyLjEzMDMgMjIuNzIxMiwtNTMuMzQwMiAtNTQuMTE4OCwtMjc0Ljg1OTEgQyAxMzU0LjE1ODQsMzU1Ni40NTkxIDEwNTUuMzYxMiwyNjkxLjUzNTggNzM5Ljg4MDksMTc3Ny43MzM2IDM4OC41ODA0LDc2MC4xNzY4NiAxNTQuNjg1MSwxMTYuMjc1MyAxMzYuMzYxNywxMTYuMjc1MyBjIC0xNi40NTUxLDAgLTM1LjI2OCw4Ljg3MjMgLTQxLjgwNjQsMTkuNzE4NyAtMC4wNDIsMC4wNzA1IC0wLjA2NCwwLjE5OTYyIC0wLjA2NywwLjM4NDEzIHogbSAxMjM3LjgyODgsMzUuMjU2MzIgYyAtMy41NTAxLDMuNjM4NjYgNzYzLjM0NzgsMzUzMC4yMTgyNSA3NzcuMTAwNSwzNTczLjQ5NTU1IDIuMzAyMyw3LjI0NTMgMTYuNjUxMyw2LjM4MjkgNTMuOTI2NiwtMy4yNDMyIDM5LjczOTIsLTEwLjI2MjQgNDkuOTE2MywtMTYuMTI3NCA0Ni43ODMxLC0yNi45NTk5IC0zLjk3NDYsLTEzLjc0MTIgLTc2OC43Mjk3LC0zNTA1LjE4NTg5IC03NzcuMDc3NCwtMzU0Ny43MDEzOCBsIC00LjM1NjUsLTIyLjE4NjgxIC00NS44ODIzLDEwLjkzMzEgYyAtMjUuMjM1NSw2LjAxNDAxIC00Ny45NTc2LDEzLjA2MTQ1IC01MC40OTQsMTUuNjYyNjQgeiIKICAgICBpZD0icGF0aDQ4ODMiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2E1YTVhNTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NzUuMjY5ODc0NTc7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBkPSJtIDM1NDYuNDc3MywxOTY2LjYwNzkgYyAtMjE0Ljc5Niw5NzcuNDU3IC0zOTAuNjQyLDE3NzcuODA5NCAtMzkwLjc3LDE3NzguNTYwNCAtMC40NTcsMi43MDEyIC05Mi43NjQsLTIwLjc1NDIgLTk2LjQxOCwtMjQuNTAwMiAtNC4zODMsLTQuNDk0MiA3NzUuMDE5LC0zNTY3Ljg1MzAxIDc4MS45MzcsLTM1NzQuOTQ0ODEgMi41NzMsLTIuNjM4NSAyNS42MDMsLTAuMzQzOCA1MS4xNzksNS4wOTk1NiAzNC45MDIsNy40Mjc1MyA0Ni4yNjQsMTMuNDczODEgNDUuNTU2LDI0LjI0MjU2IC0wLjUxOSw3Ljg5MDMxIC0xNzYuNjg3LDgxNC4wODQ0NSAtMzkxLjQ4NCwxNzkxLjU0MjQ5IHoiCiAgICAgaWQ9InBhdGg0ODg4IgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgPHBhdGgKICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2E1YTVhNTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6NzUuMjY5ODc0NTc7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjI7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjkwLjY1NzUzMTc0O3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICBkPSJtIDQ1NTkuNTkyMywxOTc5LjQyNDMgYyAtMzUwLjE5MSwxMDEyLjcwMTggLTY1MC4yNjksMTg4MC4zMzU2IC02NjYuODQsMTkyOC4wNzQzIGwgLTMwLjEyOCw4Ni43OTc4IC00OS4wMDQsLTE2Ljk2NTcgLTQ5LjAwNCwtMTYuOTY1NyAzNy4yMTYsLTEwOC4yODEgYyAyMC40NjksLTU5LjU1NDcgMzIwLjg0OSwtOTI5LjM3NzIgNjY3LjUxMSwtMTkzMi45MzkgbCA2MzAuMjk0LC0xODI0LjY1NzI2IDQ4Ljg3NywxNy41NTMwNSBjIDI2Ljg4Miw5LjY1NDY5IDQ4LjYzMiwxOS40Nzg3NSA0OC4zMzMsMjEuODI5OTEgLTAuMywyLjM1MjE2IC0yODcuMDY0LDgzMi44NTAzNiAtNjM3LjI1NSwxODQ1LjU1MzYgeiIKICAgICBpZD0icGF0aDQ4OTAiCiAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjExMi44NTM1NjkwMztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMjYzMC42ODY0LDYxNDEuNzMxOCAtMTYuNTk0MSw2MDQ3LjI0NDIiCiAgICAgaWQ9InBhdGgxMDEzIgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+Cjwvc3ZnPgo=" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255,rgb:0,0,0,1" name="outline_color"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option name="parameters"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="5" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="angle">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;rot_z&quot; is null &#xd;&#xa;then 100&#xd;&#xa;else &quot;rot_z&quot;&#xd;&#xa;end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleMarker" id="{38c70836-eb90-494c-a9f5-f43785027e5c}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="255,0,0,255,rgb:1,0,0,1" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="circle" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="2" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <labeling type="rule-based">
    <rules key="{9ec9792b-8eaa-480b-bdf2-942581ca999a}">
      <rule filter="&quot;etat&quot; = 'A poser'" key="{692bcca3-487c-458f-9568-1448667cd79d}" description="étiquettes foyers à poser">
        <settings calloutType="simple">
          <text-style fontItalic="0" isExpression="1" fontSize="1.5" fontUnderline="0" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" fontSizeUnit="MM" forcedItalic="0" textOrientation="horizontal" allowHtml="0" multilineHeight="1" namedStyle="Bold" forcedBold="0" useSubstitutions="0" legendString="Aa" fontFamily="Arial" fontLetterSpacing="0" fontStrikeout="0" multilineHeightUnit="Percentage" previewBkgrdColor="0,0,0,255,rgb:0,0,0,1" fontWordSpacing="0" textColor="255,0,252,255,rgb:1,0,0.9882352941176471,1" fontWeight="75" fontKerning="1" textOpacity="1" capitalization="0" fieldName="'foyer à poser : '  ||  id_foyer ||  ' - (départ : ' ||  &quot;id_depart&quot;  || ')'||  '\n'  || 'Ph : ' ||  &quot;num_phase&quot; " blendMode="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255,rgb:1,1,1,1" bufferNoFill="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferDraw="1" bufferSize="0.20000000000000001" bufferSizeUnits="MM" bufferOpacity="1" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskedSymbolLayers="" maskSizeUnits="MM" maskSize="0" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskEnabled="0" maskOpacity="1"/>
            <background shapeBorderWidth="0.14999999999999999" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiX="0" shapeBorderColor="255,0,252,255,rgb:1,0,0.9882352941176471,1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeSizeUnit="MM" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeOffsetUnit="MM" shapeSizeY="0.14999999999999999" shapeRadiiY="0" shapeRotation="0" shapeRadiiUnit="MM" shapeOpacity="1" shapeOffsetX="0" shapeJoinStyle="64" shapeOffsetY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRotationType="1" shapeDraw="1" shapeBorderWidthUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeX="0.14999999999999999" shapeSizeType="0">
              <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleMarker" id="">
                  <Option type="Map">
                    <Option type="QString" value="0" name="angle"/>
                    <Option type="QString" value="square" name="cap_style"/>
                    <Option type="QString" value="152,125,183,255,rgb:0.59607843137254901,0.49019607843137253,0.71764705882352942,1" name="color"/>
                    <Option type="QString" value="1" name="horizontal_anchor_point"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="circle" name="name"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="diameter" name="scale_method"/>
                    <Option type="QString" value="2" name="size"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                    <Option type="QString" value="MM" name="size_unit"/>
                    <Option type="QString" value="1" name="vertical_anchor_point"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol type="fill" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleFill" id="">
                  <Option type="Map">
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                    <Option type="QString" value="255,255,255,255,rgb:1,1,1,1" name="color"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="255,0,252,255,rgb:1,0,0.9882352941176471,1" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0.15" name="outline_width"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="solid" name="style"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowUnder="0" shadowScale="100" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM" shadowOffsetAngle="135" shadowRadius="1.5" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowOpacity="0.69999999999999996" shadowOffsetUnit="MM" shadowDraw="0" shadowBlendMode="6"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format decimals="3" plussign="0" addDirectionSymbol="0" wrapChar="" autoWrapLength="0" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" reverseDirectionSymbol="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" formatNumbers="0" multilineAlign="2"/>
          <placement rotationAngle="0" maxCurvedCharAngleIn="25" geometryGenerator="" distUnits="MM" placement="6" overrunDistance="0" overrunDistanceUnit="MM" priority="5" repeatDistanceUnits="MM" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" fitInPolygonOnly="0" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorType="0" xOffset="0" lineAnchorPercent="0.5" preserveRotation="1" lineAnchorClipping="0" overlapHandling="PreventOverlap" layerType="PointGeometry" lineAnchorTextPoint="CenterOfText" centroidInside="0" quadOffset="4" allowDegraded="0" rotationUnit="AngleDegrees" centroidWhole="0" dist="20" maxCurvedCharAngleOut="-25" geometryGeneratorType="PointGeometry" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" repeatDistance="0" geometryGeneratorEnabled="0" polygonPlacementFlags="2" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" yOffset="0"/>
          <rendering minFeatureSize="0" scaleMin="1" maxNumLabels="2000" scaleVisibility="0" scaleMax="5000" limitNumLabels="0" upsidedownLabels="0" obstacle="1" obstacleFactor="2" fontMinPixelSize="1" fontLimitPixelSize="0" fontMaxPixelSize="10000" obstacleType="0" labelPerPart="0" unplacedVisibility="0" zIndex="0" drawLabels="1" mergeLines="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="rot_z" name="field"/>
                  <Option type="int" value="2" name="type"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="&quot;cellule&quot;" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="&quot;cellule&quot;" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
              <Option type="int" value="0" name="blendMode"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
              <Option type="bool" value="false" name="drawToAllParts"/>
              <Option type="QString" value="1" name="enabled"/>
              <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
              <Option type="QString" value="&lt;symbol type=&quot;line&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;ArrowLine&quot; id=&quot;{ea71aa11-ba04-4152-a42c-60c77314cb5b}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0.03&quot; name=&quot;arrow_start_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;arrow_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;arrow_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.15&quot; name=&quot;head_length&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;head_thickness&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;head_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_curved&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_repeated&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol type=&quot;fill&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;SimpleFill&quot; id=&quot;{0832786c-9b3e-4e3d-985f-39c471ebdc69}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;255,0,252,255,rgb:1,0,0.9882352941176471,1&quot; name=&quot;color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0,0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;255,255,255,255,rgb:1,1,1,1&quot; name=&quot;outline_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;outline_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.009&quot; name=&quot;outline_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;style&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
              <Option type="double" value="0" name="minLength"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
              <Option type="QString" value="MM" name="minLengthUnit"/>
              <Option type="double" value="0" name="offsetFromAnchor"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
              <Option type="double" value="0" name="offsetFromLabel"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule filter="&quot;etat&quot; =  'A remplacer'" key="{e5c2a224-76ea-403d-80e8-07764c5dad58}" description="étiquettes foyers à remplacer">
        <settings calloutType="simple">
          <text-style fontItalic="0" isExpression="1" fontSize="1.5" fontUnderline="0" fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" fontSizeUnit="MM" forcedItalic="0" textOrientation="horizontal" allowHtml="0" multilineHeight="1" namedStyle="Bold" forcedBold="0" useSubstitutions="0" legendString="Aa" fontFamily="Arial" fontLetterSpacing="0" fontStrikeout="0" multilineHeightUnit="Percentage" previewBkgrdColor="0,0,0,255,rgb:0,0,0,1" fontWordSpacing="0" textColor="255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1" fontWeight="75" fontKerning="1" textOpacity="1" capitalization="0" fieldName="'foyer à remplacer : '  ||  id_foyer || '- (départ : ' ||  &quot;id_depart&quot;  || ')'" blendMode="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255,rgb:1,1,1,1" bufferNoFill="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferDraw="1" bufferSize="0.20000000000000001" bufferSizeUnits="MM" bufferOpacity="1" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskedSymbolLayers="" maskSizeUnits="MM" maskSize="0" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskEnabled="0" maskOpacity="1"/>
            <background shapeBorderWidth="0.14999999999999999" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiX="0" shapeBorderColor="255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeSizeUnit="MM" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeOffsetUnit="MM" shapeSizeY="0.14999999999999999" shapeRadiiY="0" shapeRotation="0" shapeRadiiUnit="MM" shapeOpacity="0.90000000000000002" shapeOffsetX="0" shapeJoinStyle="64" shapeOffsetY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRotationType="1" shapeDraw="1" shapeBorderWidthUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeX="0.14999999999999999" shapeSizeType="0">
              <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleMarker" id="">
                  <Option type="Map">
                    <Option type="QString" value="0" name="angle"/>
                    <Option type="QString" value="square" name="cap_style"/>
                    <Option type="QString" value="152,125,183,255,rgb:0.59607843137254901,0.49019607843137253,0.71764705882352942,1" name="color"/>
                    <Option type="QString" value="1" name="horizontal_anchor_point"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="circle" name="name"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="diameter" name="scale_method"/>
                    <Option type="QString" value="2" name="size"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                    <Option type="QString" value="MM" name="size_unit"/>
                    <Option type="QString" value="1" name="vertical_anchor_point"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol type="fill" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleFill" id="">
                  <Option type="Map">
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                    <Option type="QString" value="255,255,255,255,rgb:1,1,1,1" name="color"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0.15" name="outline_width"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="solid" name="style"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowUnder="0" shadowScale="100" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM" shadowOffsetAngle="135" shadowRadius="1.5" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowOpacity="0.69999999999999996" shadowOffsetUnit="MM" shadowDraw="0" shadowBlendMode="6"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format decimals="3" plussign="0" addDirectionSymbol="0" wrapChar="" autoWrapLength="0" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" reverseDirectionSymbol="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" formatNumbers="0" multilineAlign="2"/>
          <placement rotationAngle="0" maxCurvedCharAngleIn="25" geometryGenerator="" distUnits="MM" placement="6" overrunDistance="0" overrunDistanceUnit="MM" priority="10" repeatDistanceUnits="MM" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" fitInPolygonOnly="0" offsetType="1" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorType="0" xOffset="0" lineAnchorPercent="0.5" preserveRotation="1" lineAnchorClipping="0" overlapHandling="PreventOverlap" layerType="PointGeometry" lineAnchorTextPoint="CenterOfText" centroidInside="0" quadOffset="0" allowDegraded="0" rotationUnit="AngleDegrees" centroidWhole="0" dist="15" maxCurvedCharAngleOut="-25" geometryGeneratorType="LineGeometry" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" repeatDistance="0" geometryGeneratorEnabled="0" polygonPlacementFlags="2" offsetUnits="MapUnit" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" yOffset="0"/>
          <rendering minFeatureSize="0" scaleMin="1" maxNumLabels="2000" scaleVisibility="0" scaleMax="5000" limitNumLabels="0" upsidedownLabels="0" obstacle="1" obstacleFactor="2" fontMinPixelSize="1" fontLimitPixelSize="0" fontMaxPixelSize="10000" obstacleType="0" labelPerPart="0" unplacedVisibility="0" zIndex="0" drawLabels="1" mergeLines="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="LabelRotation">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="rot_z" name="field"/>
                  <Option type="int" value="2" name="type"/>
                </Option>
                <Option type="Map" name="ObstacleFactor">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="&quot;cellule&quot;" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="OffsetQuad">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="&quot;cellule&quot;" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
              <Option type="int" value="0" name="blendMode"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
              <Option type="bool" value="false" name="drawToAllParts"/>
              <Option type="QString" value="1" name="enabled"/>
              <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
              <Option type="QString" value="&lt;symbol type=&quot;line&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;ArrowLine&quot; id=&quot;{ccd4eccd-460b-4d11-ba0a-c5c1dd7f773f}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0.03&quot; name=&quot;arrow_start_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;arrow_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;arrow_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.15&quot; name=&quot;head_length&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;head_thickness&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;head_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_curved&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_repeated&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol type=&quot;fill&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;SimpleFill&quot; id=&quot;{74e331cc-794e-4fd5-b594-5ecacb14a001}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1&quot; name=&quot;color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0,0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;255,255,255,255,rgb:1,1,1,1&quot; name=&quot;outline_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;outline_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.009&quot; name=&quot;outline_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;style&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
              <Option type="double" value="0" name="minLength"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
              <Option type="QString" value="MM" name="minLengthUnit"/>
              <Option type="double" value="0" name="offsetFromAnchor"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
              <Option type="double" value="0" name="offsetFromLabel"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule filter="&quot;etat&quot; = 'A déposer'" key="{3a6fdcfb-ba67-4cb9-9258-4988ff4bec73}" description="étiquettes foyers à déposer">
        <settings calloutType="simple">
          <text-style fontItalic="0" isExpression="1" fontSize="1.5" fontUnderline="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontSizeUnit="MM" forcedItalic="0" textOrientation="horizontal" allowHtml="0" multilineHeight="1" namedStyle="Normal" forcedBold="0" useSubstitutions="0" legendString="Aa" fontFamily="Arial" fontLetterSpacing="0" fontStrikeout="0" multilineHeightUnit="Percentage" previewBkgrdColor="0,0,0,255,rgb:0,0,0,1" fontWordSpacing="0" textColor="165,165,165,255,rgb:0.6470588235294118,0.6470588235294118,0.6470588235294118,1" fontWeight="50" fontKerning="1" textOpacity="1" capitalization="0" fieldName="'foyer à déposer : '  ||  &quot;id_foyer&quot; " blendMode="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255,rgb:1,1,1,1" bufferNoFill="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferDraw="0" bufferSize="1" bufferSizeUnits="MM" bufferOpacity="1" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskedSymbolLayers="" maskSizeUnits="MM" maskSize="0" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskEnabled="0" maskOpacity="1"/>
            <background shapeBorderWidth="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiX="0" shapeBorderColor="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeSizeUnit="MM" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeOffsetUnit="MM" shapeSizeY="0" shapeRadiiY="0" shapeRotation="0" shapeRadiiUnit="MM" shapeOpacity="1" shapeOffsetX="0" shapeJoinStyle="64" shapeOffsetY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRotationType="0" shapeDraw="0" shapeBorderWidthUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeX="0" shapeSizeType="0">
              <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleMarker" id="">
                  <Option type="Map">
                    <Option type="QString" value="0" name="angle"/>
                    <Option type="QString" value="square" name="cap_style"/>
                    <Option type="QString" value="190,178,151,255,rgb:0.74509803921568629,0.69803921568627447,0.59215686274509804,1" name="color"/>
                    <Option type="QString" value="1" name="horizontal_anchor_point"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="circle" name="name"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="diameter" name="scale_method"/>
                    <Option type="QString" value="2" name="size"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                    <Option type="QString" value="MM" name="size_unit"/>
                    <Option type="QString" value="1" name="vertical_anchor_point"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol type="fill" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleFill" id="">
                  <Option type="Map">
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                    <Option type="QString" value="255,255,255,255,rgb:1,1,1,1" name="color"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" name="outline_color"/>
                    <Option type="QString" value="no" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="solid" name="style"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowUnder="0" shadowScale="100" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM" shadowOffsetAngle="135" shadowRadius="1.5" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowOpacity="0.69999999999999996" shadowOffsetUnit="MM" shadowDraw="0" shadowBlendMode="6"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format decimals="3" plussign="0" addDirectionSymbol="0" wrapChar="" autoWrapLength="0" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" reverseDirectionSymbol="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" formatNumbers="0" multilineAlign="3"/>
          <placement rotationAngle="0" maxCurvedCharAngleIn="25" geometryGenerator="" distUnits="MM" placement="0" overrunDistance="0" overrunDistanceUnit="MM" priority="5" repeatDistanceUnits="MM" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" fitInPolygonOnly="0" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorType="0" xOffset="5" lineAnchorPercent="0.5" preserveRotation="1" lineAnchorClipping="0" overlapHandling="PreventOverlap" layerType="PointGeometry" lineAnchorTextPoint="CenterOfText" centroidInside="0" quadOffset="0" allowDegraded="0" rotationUnit="AngleDegrees" centroidWhole="0" dist="15" maxCurvedCharAngleOut="-25" geometryGeneratorType="PointGeometry" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" repeatDistance="0" geometryGeneratorEnabled="0" polygonPlacementFlags="2" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" yOffset="0"/>
          <rendering minFeatureSize="0" scaleMin="0" maxNumLabels="2000" scaleVisibility="0" scaleMax="0" limitNumLabels="0" upsidedownLabels="0" obstacle="1" obstacleFactor="1" fontMinPixelSize="3" fontLimitPixelSize="0" fontMaxPixelSize="10000" obstacleType="0" labelPerPart="0" unplacedVisibility="0" zIndex="0" drawLabels="1" mergeLines="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
              <Option type="int" value="0" name="blendMode"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
              <Option type="bool" value="false" name="drawToAllParts"/>
              <Option type="QString" value="1" name="enabled"/>
              <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
              <Option type="QString" value="&lt;symbol type=&quot;line&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;ArrowLine&quot; id=&quot;{64522d20-f789-440d-b630-05fa32332da1}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0.03&quot; name=&quot;arrow_start_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;arrow_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;arrow_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.15&quot; name=&quot;head_length&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;head_thickness&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;head_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_curved&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_repeated&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol type=&quot;fill&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;SimpleFill&quot; id=&quot;{38c0bf30-b85d-4063-bb14-36b49e14ea94}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;204,204,204,255,rgb:0.80000000000000004,0.80000000000000004,0.80000000000000004,1&quot; name=&quot;color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0,0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;255,255,255,255,rgb:1,1,1,1&quot; name=&quot;outline_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;outline_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.009&quot; name=&quot;outline_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;style&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
              <Option type="double" value="0" name="minLength"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
              <Option type="QString" value="MM" name="minLengthUnit"/>
              <Option type="double" value="0" name="offsetFromAnchor"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
              <Option type="double" value="0" name="offsetFromLabel"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule filter="&quot;etat&quot; = 'Déposé'" key="{9ada5fec-b456-4e51-abc7-bef9937f19a3}" description="étiquettes foyers déposé">
        <settings calloutType="simple">
          <text-style fontItalic="0" isExpression="1" fontSize="1.5" fontUnderline="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontSizeUnit="MM" forcedItalic="0" textOrientation="horizontal" allowHtml="0" multilineHeight="1" namedStyle="Normal" forcedBold="0" useSubstitutions="0" legendString="Aa" fontFamily="Arial" fontLetterSpacing="0" fontStrikeout="0" multilineHeightUnit="Percentage" previewBkgrdColor="0,0,0,255,rgb:0,0,0,1" fontWordSpacing="0" textColor="165,165,165,255,rgb:0.6470588235294118,0.6470588235294118,0.6470588235294118,1" fontWeight="50" fontKerning="1" textOpacity="1" capitalization="0" fieldName="'foyer déposé : '  ||  &quot;id_foyer&quot; " blendMode="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255,rgb:1,1,1,1" bufferNoFill="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferDraw="0" bufferSize="1" bufferSizeUnits="MM" bufferOpacity="1" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskedSymbolLayers="" maskSizeUnits="MM" maskSize="0" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskEnabled="0" maskOpacity="1"/>
            <background shapeBorderWidth="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiX="0" shapeBorderColor="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeSizeUnit="MM" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeOffsetUnit="MM" shapeSizeY="0" shapeRadiiY="0" shapeRotation="0" shapeRadiiUnit="MM" shapeOpacity="1" shapeOffsetX="0" shapeJoinStyle="64" shapeOffsetY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRotationType="0" shapeDraw="0" shapeBorderWidthUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeX="0" shapeSizeType="0">
              <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleMarker" id="">
                  <Option type="Map">
                    <Option type="QString" value="0" name="angle"/>
                    <Option type="QString" value="square" name="cap_style"/>
                    <Option type="QString" value="190,178,151,255,rgb:0.74509803921568629,0.69803921568627447,0.59215686274509804,1" name="color"/>
                    <Option type="QString" value="1" name="horizontal_anchor_point"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="circle" name="name"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="diameter" name="scale_method"/>
                    <Option type="QString" value="2" name="size"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                    <Option type="QString" value="MM" name="size_unit"/>
                    <Option type="QString" value="1" name="vertical_anchor_point"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol type="fill" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleFill" id="">
                  <Option type="Map">
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                    <Option type="QString" value="255,255,255,255,rgb:1,1,1,1" name="color"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" name="outline_color"/>
                    <Option type="QString" value="no" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="solid" name="style"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowUnder="0" shadowScale="100" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="MM" shadowOffsetAngle="135" shadowRadius="1.5" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowOpacity="0.69999999999999996" shadowOffsetUnit="MM" shadowDraw="0" shadowBlendMode="6"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format decimals="3" plussign="0" addDirectionSymbol="0" wrapChar="" autoWrapLength="0" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" reverseDirectionSymbol="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" formatNumbers="0" multilineAlign="3"/>
          <placement rotationAngle="0" maxCurvedCharAngleIn="25" geometryGenerator="" distUnits="MM" placement="0" overrunDistance="0" overrunDistanceUnit="MM" priority="5" repeatDistanceUnits="MM" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" fitInPolygonOnly="0" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorType="0" xOffset="5" lineAnchorPercent="0.5" preserveRotation="1" lineAnchorClipping="0" overlapHandling="PreventOverlap" layerType="PointGeometry" lineAnchorTextPoint="CenterOfText" centroidInside="0" quadOffset="0" allowDegraded="0" rotationUnit="AngleDegrees" centroidWhole="0" dist="15" maxCurvedCharAngleOut="-25" geometryGeneratorType="PointGeometry" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" repeatDistance="0" geometryGeneratorEnabled="0" polygonPlacementFlags="2" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" yOffset="0"/>
          <rendering minFeatureSize="0" scaleMin="0" maxNumLabels="2000" scaleVisibility="0" scaleMax="0" limitNumLabels="0" upsidedownLabels="0" obstacle="1" obstacleFactor="1" fontMinPixelSize="3" fontLimitPixelSize="0" fontMaxPixelSize="10000" obstacleType="0" labelPerPart="0" unplacedVisibility="0" zIndex="0" drawLabels="1" mergeLines="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
              <Option type="int" value="0" name="blendMode"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
              <Option type="bool" value="false" name="drawToAllParts"/>
              <Option type="QString" value="1" name="enabled"/>
              <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
              <Option type="QString" value="&lt;symbol type=&quot;line&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;ArrowLine&quot; id=&quot;{56b7bf4e-1b6b-4d27-ace8-1d9a87f1af05}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0.03&quot; name=&quot;arrow_start_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;arrow_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;arrow_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.15&quot; name=&quot;head_length&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;head_thickness&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;head_type&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_curved&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;1&quot; name=&quot;is_repeated&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol type=&quot;fill&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;SimpleFill&quot; id=&quot;{bb84b748-1720-4b54-acd4-9e9e9c157151}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;204,204,204,255,rgb:0.80000000000000004,0.80000000000000004,0.80000000000000004,1&quot; name=&quot;color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0,0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;255,255,255,255,rgb:1,1,1,1&quot; name=&quot;outline_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;outline_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.009&quot; name=&quot;outline_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;style&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
              <Option type="double" value="0" name="minLength"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
              <Option type="QString" value="MM" name="minLengthUnit"/>
              <Option type="double" value="0" name="offsetFromAnchor"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
              <Option type="double" value="0" name="offsetFromLabel"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule filter="&quot;etat&quot; = 'Actif'" key="{6fc56b08-0af6-43fd-9fbf-152fa421af3c}" description="texte foyer">
        <settings calloutType="balloon">
          <text-style fontItalic="1" isExpression="1" fontSize="1.5" fontUnderline="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontSizeUnit="MM" forcedItalic="0" textOrientation="horizontal" allowHtml="0" multilineHeight="1" namedStyle="Bold Italic" forcedBold="0" useSubstitutions="0" legendString="Aa" fontFamily="Arial" fontLetterSpacing="0" fontStrikeout="0" multilineHeightUnit="Percentage" previewBkgrdColor="255,255,255,255,rgb:1,1,1,1" fontWordSpacing="0" textColor="255,127,0,255,rgb:1,0.49803921568627452,0,1" fontWeight="75" fontKerning="1" textOpacity="1" capitalization="0" fieldName="&quot;id_foyer&quot;  ||  '\n'  || '(départ : ' ||  &quot;id_depart&quot;  || ')'" blendMode="0">
            <families/>
            <text-buffer bufferColor="255,255,255,255,rgb:1,1,1,1" bufferNoFill="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferDraw="0" bufferSize="0" bufferSizeUnits="MM" bufferOpacity="1" bufferBlendMode="0" bufferJoinStyle="128"/>
            <text-mask maskType="0" maskedSymbolLayers="" maskSizeUnits="MM" maskSize="0" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskEnabled="0" maskOpacity="1"/>
            <background shapeBorderWidth="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiX="0" shapeBorderColor="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeSizeUnit="Point" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeOffsetUnit="Point" shapeSizeY="0" shapeRadiiY="0" shapeRotation="0" shapeRadiiUnit="Point" shapeOpacity="1" shapeOffsetX="0" shapeJoinStyle="64" shapeOffsetY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRotationType="0" shapeDraw="0" shapeBorderWidthUnit="Point" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeX="0" shapeSizeType="0">
              <symbol type="marker" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleMarker" id="">
                  <Option type="Map">
                    <Option type="QString" value="0" name="angle"/>
                    <Option type="QString" value="square" name="cap_style"/>
                    <Option type="QString" value="152,125,183,255,rgb:0.59607843137254901,0.49019607843137253,0.71764705882352942,1" name="color"/>
                    <Option type="QString" value="1" name="horizontal_anchor_point"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="circle" name="name"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="diameter" name="scale_method"/>
                    <Option type="QString" value="2" name="size"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                    <Option type="QString" value="MM" name="size_unit"/>
                    <Option type="QString" value="1" name="vertical_anchor_point"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol type="fill" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" pass="0" class="SimpleFill" id="">
                  <Option type="Map">
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                    <Option type="QString" value="255,255,255,255,rgb:1,1,1,1" name="color"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" name="outline_color"/>
                    <Option type="QString" value="no" name="outline_style"/>
                    <Option type="QString" value="0" name="outline_width"/>
                    <Option type="QString" value="Point" name="outline_width_unit"/>
                    <Option type="QString" value="solid" name="style"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowUnder="0" shadowScale="100" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusUnit="Point" shadowOffsetAngle="135" shadowRadius="1.5" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowOpacity="1" shadowOffsetUnit="Point" shadowDraw="0" shadowBlendMode="6"/>
            <dd_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format decimals="3" plussign="0" addDirectionSymbol="0" wrapChar="" autoWrapLength="0" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" reverseDirectionSymbol="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" formatNumbers="0" multilineAlign="3"/>
          <placement rotationAngle="0" maxCurvedCharAngleIn="25" geometryGenerator="" distUnits="MM" placement="6" overrunDistance="0" overrunDistanceUnit="MM" priority="5" repeatDistanceUnits="MM" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" fitInPolygonOnly="0" offsetType="1" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorType="0" xOffset="0" lineAnchorPercent="0.5" preserveRotation="1" lineAnchorClipping="0" overlapHandling="PreventOverlap" layerType="PointGeometry" lineAnchorTextPoint="CenterOfText" centroidInside="0" quadOffset="4" allowDegraded="0" rotationUnit="AngleDegrees" centroidWhole="0" dist="15" maxCurvedCharAngleOut="-25" geometryGeneratorType="PointGeometry" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" repeatDistance="0" geometryGeneratorEnabled="0" polygonPlacementFlags="2" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" yOffset="0"/>
          <rendering minFeatureSize="0" scaleMin="0" maxNumLabels="2000" scaleVisibility="0" scaleMax="0" limitNumLabels="0" upsidedownLabels="0" obstacle="1" obstacleFactor="1" fontMinPixelSize="3" fontLimitPixelSize="0" fontMaxPixelSize="10000" obstacleType="1" labelPerPart="0" unplacedVisibility="0" zIndex="0" drawLabels="1" mergeLines="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="PositionX">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="int" value="1" name="type"/>
                  <Option type="QString" value="" name="val"/>
                </Option>
                <Option type="Map" name="PositionY">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="int" value="1" name="type"/>
                  <Option type="QString" value="" name="val"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </dd_properties>
          <callout type="balloon">
            <Option type="Map">
              <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
              <Option type="int" value="0" name="blendMode"/>
              <Option type="double" value="0.5" name="cornerRadius"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="cornerRadiusMapUnitScale"/>
              <Option type="QString" value="MM" name="cornerRadiusUnit"/>
              <Option type="Map" name="ddProperties">
                <Option type="QString" value="" name="name"/>
                <Option type="Map" name="properties">
                  <Option type="Map" name="OriginX">
                    <Option type="bool" value="false" name="active"/>
                    <Option type="int" value="1" name="type"/>
                    <Option type="QString" value="" name="val"/>
                  </Option>
                  <Option type="Map" name="OriginY">
                    <Option type="bool" value="false" name="active"/>
                    <Option type="int" value="1" name="type"/>
                    <Option type="QString" value="" name="val"/>
                  </Option>
                </Option>
                <Option type="QString" value="collection" name="type"/>
              </Option>
              <Option type="QString" value="1" name="enabled"/>
              <Option type="QString" value="&lt;symbol type=&quot;fill&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; class=&quot;SimpleFill&quot; id=&quot;{ef8447f6-f292-4a0b-8335-cd2134686a2a}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;204,204,204,255,rgb:0.80000000000000004,0.80000000000000004,0.80000000000000004,1&quot; name=&quot;color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0,0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;255,127,0,255,rgb:1,0.49803921568627452,0,1&quot; name=&quot;outline_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;outline_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.06&quot; name=&quot;outline_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;outline_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;style&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="fillSymbol"/>
              <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
              <Option type="QString" value="0.5,0.5,0.5,0.5" name="margins"/>
              <Option type="QString" value="MM" name="marginsUnit"/>
              <Option type="double" value="0" name="offsetFromAnchor"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
              <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
              <Option type="double" value="1" name="wedgeWidth"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="wedgeWidthMapUnitScale"/>
              <Option type="QString" value="MM" name="wedgeWidthUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="QString" value="remove" name="QFieldSync/action"/>
      <Option type="QString" value="offline" name="QFieldSync/cloud_action"/>
      <Option type="QString" value="{&quot;photo&quot;: &quot;'DCIM/foyers_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;}" name="QFieldSync/photo_naming"/>
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
        <Option type="QString" value="COALESCE( &quot;id_foyer&quot;, '&lt;NULL>' )"/>
      </Option>
      <Option type="QString" value="0" name="embeddedWidgets/count"/>
      <Option type="QString" value="false" name="labeling/addDirectionSymbol"/>
      <Option type="QString" value="0" name="labeling/angleOffset"/>
      <Option type="QString" value="0" name="labeling/blendMode"/>
      <Option type="QString" value="0" name="labeling/bufferBlendMode"/>
      <Option type="QString" value="255" name="labeling/bufferColorA"/>
      <Option type="QString" value="255" name="labeling/bufferColorB"/>
      <Option type="QString" value="255" name="labeling/bufferColorG"/>
      <Option type="QString" value="255" name="labeling/bufferColorR"/>
      <Option type="QString" value="false" name="labeling/bufferDraw"/>
      <Option type="QString" value="128" name="labeling/bufferJoinStyle"/>
      <Option type="QString" value="false" name="labeling/bufferNoFill"/>
      <Option type="QString" value="1" name="labeling/bufferSize"/>
      <Option type="QString" value="false" name="labeling/bufferSizeInMapUnits"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/bufferSizeMapUnitScale"/>
      <Option type="QString" value="0" name="labeling/bufferTransp"/>
      <Option type="QString" value="false" name="labeling/centroidInside"/>
      <Option type="QString" value="false" name="labeling/centroidWhole"/>
      <Option type="QString" value="3" name="labeling/decimals"/>
      <Option type="QString" value="false" name="labeling/displayAll"/>
      <Option type="QString" value="0" name="labeling/dist"/>
      <Option type="QString" value="false" name="labeling/distInMapUnits"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/distMapUnitScale"/>
      <Option type="QString" value="true" name="labeling/drawLabels"/>
      <Option type="QString" value="true" name="labeling/enabled"/>
      <Option type="QString" value="id_foyer" name="labeling/fieldName"/>
      <Option type="QString" value="false" name="labeling/fitInPolygonOnly"/>
      <Option type="QString" value="0" name="labeling/fontCapitals"/>
      <Option type="QString" value="MS Shell Dlg 2" name="labeling/fontFamily"/>
      <Option type="QString" value="false" name="labeling/fontItalic"/>
      <Option type="QString" value="0" name="labeling/fontLetterSpacing"/>
      <Option type="QString" value="false" name="labeling/fontLimitPixelSize"/>
      <Option type="QString" value="10000" name="labeling/fontMaxPixelSize"/>
      <Option type="QString" value="3" name="labeling/fontMinPixelSize"/>
      <Option type="QString" value="1.25" name="labeling/fontSize"/>
      <Option type="QString" value="false" name="labeling/fontSizeInMapUnits"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/fontSizeMapUnitScale"/>
      <Option type="QString" value="false" name="labeling/fontStrikeout"/>
      <Option type="QString" value="false" name="labeling/fontUnderline"/>
      <Option type="QString" value="50" name="labeling/fontWeight"/>
      <Option type="QString" value="0" name="labeling/fontWordSpacing"/>
      <Option type="QString" value="false" name="labeling/formatNumbers"/>
      <Option type="QString" value="false" name="labeling/isExpression"/>
      <Option type="QString" value="true" name="labeling/labelOffsetInMapUnits"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/labelOffsetMapUnitScale"/>
      <Option type="QString" value="false" name="labeling/labelPerPart"/>
      <Option type="QString" value="&lt;" name="labeling/leftDirectionSymbol"/>
      <Option type="QString" value="false" name="labeling/limitNumLabels"/>
      <Option type="QString" value="25" name="labeling/maxCurvedCharAngleIn"/>
      <Option type="QString" value="-25" name="labeling/maxCurvedCharAngleOut"/>
      <Option type="QString" value="2000" name="labeling/maxNumLabels"/>
      <Option type="QString" value="false" name="labeling/mergeLines"/>
      <Option type="QString" value="0" name="labeling/minFeatureSize"/>
      <Option type="QString" value="3" name="labeling/multilineAlign"/>
      <Option type="QString" value="1" name="labeling/multilineHeight"/>
      <Option type="QString" value="Normal" name="labeling/namedStyle"/>
      <Option type="QString" value="true" name="labeling/obstacle"/>
      <Option type="QString" value="1" name="labeling/obstacleFactor"/>
      <Option type="QString" value="0" name="labeling/obstacleType"/>
      <Option type="QString" value="0" name="labeling/offsetType"/>
      <Option type="QString" value="0" name="labeling/placeDirectionSymbol"/>
      <Option type="QString" value="6" name="labeling/placement"/>
      <Option type="QString" value="10" name="labeling/placementFlags"/>
      <Option type="QString" value="false" name="labeling/plussign"/>
      <Option type="QString" value="TR,TL,BR,BL,R,L,TSR,BSR" name="labeling/predefinedPositionOrder"/>
      <Option type="QString" value="true" name="labeling/preserveRotation"/>
      <Option type="QString" value="#ffffff" name="labeling/previewBkgrdColor"/>
      <Option type="QString" value="5" name="labeling/priority"/>
      <Option type="QString" value="4" name="labeling/quadOffset"/>
      <Option type="QString" value="0" name="labeling/repeatDistance"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/repeatDistanceMapUnitScale"/>
      <Option type="QString" value="1" name="labeling/repeatDistanceUnit"/>
      <Option type="QString" value="false" name="labeling/reverseDirectionSymbol"/>
      <Option type="QString" value=">" name="labeling/rightDirectionSymbol"/>
      <Option type="QString" value="10000000" name="labeling/scaleMax"/>
      <Option type="QString" value="1" name="labeling/scaleMin"/>
      <Option type="QString" value="false" name="labeling/scaleVisibility"/>
      <Option type="QString" value="6" name="labeling/shadowBlendMode"/>
      <Option type="QString" value="0" name="labeling/shadowColorB"/>
      <Option type="QString" value="0" name="labeling/shadowColorG"/>
      <Option type="QString" value="0" name="labeling/shadowColorR"/>
      <Option type="QString" value="false" name="labeling/shadowDraw"/>
      <Option type="QString" value="135" name="labeling/shadowOffsetAngle"/>
      <Option type="QString" value="1" name="labeling/shadowOffsetDist"/>
      <Option type="QString" value="true" name="labeling/shadowOffsetGlobal"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/shadowOffsetMapUnitScale"/>
      <Option type="QString" value="1" name="labeling/shadowOffsetUnits"/>
      <Option type="QString" value="1.5" name="labeling/shadowRadius"/>
      <Option type="QString" value="false" name="labeling/shadowRadiusAlphaOnly"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/shadowRadiusMapUnitScale"/>
      <Option type="QString" value="1" name="labeling/shadowRadiusUnits"/>
      <Option type="QString" value="100" name="labeling/shadowScale"/>
      <Option type="QString" value="30" name="labeling/shadowTransparency"/>
      <Option type="QString" value="0" name="labeling/shadowUnder"/>
      <Option type="QString" value="0" name="labeling/shapeBlendMode"/>
      <Option type="QString" value="255" name="labeling/shapeBorderColorA"/>
      <Option type="QString" value="128" name="labeling/shapeBorderColorB"/>
      <Option type="QString" value="128" name="labeling/shapeBorderColorG"/>
      <Option type="QString" value="128" name="labeling/shapeBorderColorR"/>
      <Option type="QString" value="0" name="labeling/shapeBorderWidth"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/shapeBorderWidthMapUnitScale"/>
      <Option type="QString" value="1" name="labeling/shapeBorderWidthUnits"/>
      <Option type="QString" value="false" name="labeling/shapeDraw"/>
      <Option type="QString" value="255" name="labeling/shapeFillColorA"/>
      <Option type="QString" value="255" name="labeling/shapeFillColorB"/>
      <Option type="QString" value="255" name="labeling/shapeFillColorG"/>
      <Option type="QString" value="255" name="labeling/shapeFillColorR"/>
      <Option type="QString" value="64" name="labeling/shapeJoinStyle"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/shapeOffsetMapUnitScale"/>
      <Option type="QString" value="1" name="labeling/shapeOffsetUnits"/>
      <Option type="QString" value="0" name="labeling/shapeOffsetX"/>
      <Option type="QString" value="0" name="labeling/shapeOffsetY"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/shapeRadiiMapUnitScale"/>
      <Option type="QString" value="1" name="labeling/shapeRadiiUnits"/>
      <Option type="QString" value="0" name="labeling/shapeRadiiX"/>
      <Option type="QString" value="0" name="labeling/shapeRadiiY"/>
      <Option type="QString" value="0" name="labeling/shapeRotation"/>
      <Option type="QString" value="0" name="labeling/shapeRotationType"/>
      <Option type="invalid" name="labeling/shapeSVGFile"/>
      <Option type="QString" value="0,0,0,0,0,0" name="labeling/shapeSizeMapUnitScale"/>
      <Option type="QString" value="0" name="labeling/shapeSizeType"/>
      <Option type="QString" value="1" name="labeling/shapeSizeUnits"/>
      <Option type="QString" value="0" name="labeling/shapeSizeX"/>
      <Option type="QString" value="0" name="labeling/shapeSizeY"/>
      <Option type="QString" value="0" name="labeling/shapeTransparency"/>
      <Option type="QString" value="0" name="labeling/shapeType"/>
      <Option type="QString" value="&lt;substitutions/>" name="labeling/substitutions"/>
      <Option type="QString" value="255" name="labeling/textColorA"/>
      <Option type="QString" value="1" name="labeling/textColorB"/>
      <Option type="QString" value="240" name="labeling/textColorG"/>
      <Option type="QString" value="252" name="labeling/textColorR"/>
      <Option type="QString" value="0" name="labeling/textTransp"/>
      <Option type="QString" value="0" name="labeling/upsidedownLabels"/>
      <Option type="QString" value="false" name="labeling/useSubstitutions"/>
      <Option type="invalid" name="labeling/wrapChar"/>
      <Option type="QString" value="0" name="labeling/xOffset"/>
      <Option type="QString" value="0" name="labeling/yOffset"/>
      <Option type="QString" value="0" name="labeling/zIndex"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory width="15" backgroundColor="#ffffff" spacing="0" lineSizeType="MM" height="15" showAxis="0" scaleDependency="Area" opacity="1" penColor="#000000" scaleBasedVisibility="0" penAlpha="255" direction="1" enabled="0" diagramOrientation="Up" minScaleDenominator="1" lineSizeScale="3x:0,0,0,0,0,0" sizeType="MM" maxScaleDenominator="1e+08" labelPlacementMethod="XHeight" barWidth="5" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" backgroundAlpha="255" rotationOffset="270" spacingUnit="MM" spacingUnitScale="3x:0,0,0,0,0,0" penWidth="0">
      <fontProperties style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0"/>
      <attribute label="" field="" color="#000000" colorOpacity="1"/>
      <axisSymbol>
        <symbol type="line" frame_rate="10" clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" locked="0" pass="0" class="SimpleLine" id="{584eea5c-9e14-46f0-9737-72e9ee123e3a}">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings dist="0" placement="0" obstacle="0" linePlacementFlags="18" priority="0" showAll="1" zIndex="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="NoFlag" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="numero">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="id_support">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="id_foyer">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="id_depart">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="id_armoire">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="etat">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="Actif" name="Actif"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="hauteur_feu">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="marque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="type_vasque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="classe_electrique">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="degre_protection">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="resistance_choc">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="num_phase">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="1" name="1"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="2" name="2"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="3" name="3"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="varistance">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="ral">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="cellule">
      <editWidget type="UniqueValues">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="Editable"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="date_pose">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="allow_null"/>
            <Option type="bool" value="true" name="calendar_popup"/>
            <Option type="QString" value="dd/MM/yyyy" name="display_format"/>
            <Option type="QString" value="yyyy-MM-dd" name="field_format"/>
            <Option type="bool" value="false" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="date_depose">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="date_action">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="desc_action">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="remarque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="mslink">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" value="0" name="IsMultiline"/>
            <Option type="QString" value="0" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="mapid">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" value="0" name="IsMultiline"/>
            <Option type="QString" value="0" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="ecp_intervention_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="acteur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="uid4">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="created_at">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" value="0" name="IsMultiline"/>
            <Option type="QString" value="0" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="updated_at">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="QString" value="0" name="IsMultiline"/>
            <Option type="QString" value="0" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="pkid">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="intervention">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="rot_z">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="photo">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option type="QString" value="P:/SIG/documents/photos" name="DefaultRoot"/>
            <Option type="int" value="1" name="DocumentViewer"/>
            <Option type="int" value="0" name="DocumentViewerHeight"/>
            <Option type="int" value="0" name="DocumentViewerWidth"/>
            <Option type="bool" value="false" name="FileWidget"/>
            <Option type="bool" value="false" name="FileWidgetButton"/>
            <Option type="invalid" name="FileWidgetFilter"/>
            <Option type="Map" name="PropertyCollection">
              <Option type="invalid" name="name"/>
              <Option type="invalid" name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
            <Option type="int" value="1" name="RelativeStorage"/>
            <Option type="int" value="0" name="StorageMode"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="tri_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="fid" index="0" name=""/>
    <alias field="numero" index="1" name=""/>
    <alias field="insee" index="2" name="numéro insee commune"/>
    <alias field="id_support" index="3" name="Identifiant(s) support(s)"/>
    <alias field="id_foyer" index="4" name="Identifiant(s) foyer(s)"/>
    <alias field="id_depart" index="5" name="Identifiant départ"/>
    <alias field="id_armoire" index="6" name="Identifiant armoire"/>
    <alias field="etat" index="7" name="état"/>
    <alias field="hauteur_feu" index="8" name="Hauteur de feu"/>
    <alias field="marque" index="9" name="Marque "/>
    <alias field="type_vasque" index="10" name="Type de vasque"/>
    <alias field="classe_electrique" index="11" name=""/>
    <alias field="degre_protection" index="12" name=""/>
    <alias field="resistance_choc" index="13" name=""/>
    <alias field="num_phase" index="14" name="numéro de phase"/>
    <alias field="varistance" index="15" name=""/>
    <alias field="ral" index="16" name="RAL"/>
    <alias field="cellule" index="17" name=""/>
    <alias field="date_pose" index="18" name="Date de pose"/>
    <alias field="date_depose" index="19" name="Date de dépose"/>
    <alias field="date_action" index="20" name=""/>
    <alias field="desc_action" index="21" name=""/>
    <alias field="remarque" index="22" name="Remarque"/>
    <alias field="mslink" index="23" name=""/>
    <alias field="mapid" index="24" name=""/>
    <alias field="ecp_intervention_id" index="25" name=""/>
    <alias field="acteur" index="26" name="chargés d'affaires"/>
    <alias field="uid4" index="27" name=""/>
    <alias field="created_at" index="28" name=""/>
    <alias field="updated_at" index="29" name=""/>
    <alias field="pkid" index="30" name=""/>
    <alias field="intervention" index="31" name="numéro d'affaires"/>
    <alias field="rot_z" index="32" name=""/>
    <alias field="photo" index="33" name="Vue sur façade"/>
    <alias field="tri_id" index="34" name=""/>
  </aliases>
  <splitPolicies>
    <policy field="fid" policy="Duplicate"/>
    <policy field="numero" policy="Duplicate"/>
    <policy field="insee" policy="Duplicate"/>
    <policy field="id_support" policy="Duplicate"/>
    <policy field="id_foyer" policy="Duplicate"/>
    <policy field="id_depart" policy="Duplicate"/>
    <policy field="id_armoire" policy="Duplicate"/>
    <policy field="etat" policy="Duplicate"/>
    <policy field="hauteur_feu" policy="Duplicate"/>
    <policy field="marque" policy="Duplicate"/>
    <policy field="type_vasque" policy="Duplicate"/>
    <policy field="classe_electrique" policy="Duplicate"/>
    <policy field="degre_protection" policy="Duplicate"/>
    <policy field="resistance_choc" policy="Duplicate"/>
    <policy field="num_phase" policy="Duplicate"/>
    <policy field="varistance" policy="Duplicate"/>
    <policy field="ral" policy="Duplicate"/>
    <policy field="cellule" policy="Duplicate"/>
    <policy field="date_pose" policy="Duplicate"/>
    <policy field="date_depose" policy="Duplicate"/>
    <policy field="date_action" policy="Duplicate"/>
    <policy field="desc_action" policy="Duplicate"/>
    <policy field="remarque" policy="Duplicate"/>
    <policy field="mslink" policy="Duplicate"/>
    <policy field="mapid" policy="Duplicate"/>
    <policy field="ecp_intervention_id" policy="Duplicate"/>
    <policy field="acteur" policy="Duplicate"/>
    <policy field="uid4" policy="Duplicate"/>
    <policy field="created_at" policy="Duplicate"/>
    <policy field="updated_at" policy="Duplicate"/>
    <policy field="pkid" policy="Duplicate"/>
    <policy field="intervention" policy="Duplicate"/>
    <policy field="rot_z" policy="Duplicate"/>
    <policy field="photo" policy="Duplicate"/>
    <policy field="tri_id" policy="Duplicate"/>
  </splitPolicies>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="numero" expression="" applyOnUpdate="0"/>
    <default field="insee" expression=" @lumigis_insee " applyOnUpdate="1"/>
    <default field="id_support" expression="" applyOnUpdate="0"/>
    <default field="id_foyer" expression="" applyOnUpdate="0"/>
    <default field="id_depart" expression="" applyOnUpdate="0"/>
    <default field="id_armoire" expression="" applyOnUpdate="0"/>
    <default field="etat" expression="" applyOnUpdate="0"/>
    <default field="hauteur_feu" expression="" applyOnUpdate="0"/>
    <default field="marque" expression="" applyOnUpdate="0"/>
    <default field="type_vasque" expression="" applyOnUpdate="0"/>
    <default field="classe_electrique" expression="" applyOnUpdate="0"/>
    <default field="degre_protection" expression="" applyOnUpdate="0"/>
    <default field="resistance_choc" expression="" applyOnUpdate="0"/>
    <default field="num_phase" expression="" applyOnUpdate="0"/>
    <default field="varistance" expression="" applyOnUpdate="0"/>
    <default field="ral" expression="" applyOnUpdate="0"/>
    <default field="cellule" expression="" applyOnUpdate="0"/>
    <default field="date_pose" expression="" applyOnUpdate="0"/>
    <default field="date_depose" expression="" applyOnUpdate="0"/>
    <default field="date_action" expression="" applyOnUpdate="0"/>
    <default field="desc_action" expression="" applyOnUpdate="0"/>
    <default field="remarque" expression="" applyOnUpdate="0"/>
    <default field="mslink" expression="" applyOnUpdate="0"/>
    <default field="mapid" expression="" applyOnUpdate="0"/>
    <default field="ecp_intervention_id" expression="" applyOnUpdate="0"/>
    <default field="acteur" expression=" @lumigis_technicien " applyOnUpdate="1"/>
    <default field="uid4" expression="" applyOnUpdate="0"/>
    <default field="created_at" expression="" applyOnUpdate="0"/>
    <default field="updated_at" expression="" applyOnUpdate="0"/>
    <default field="pkid" expression="" applyOnUpdate="0"/>
    <default field="intervention" expression=" @lumigis_intervention " applyOnUpdate="1"/>
    <default field="rot_z" expression="" applyOnUpdate="0"/>
    <default field="photo" expression="" applyOnUpdate="0"/>
    <default field="tri_id" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" field="fid" notnull_strength="1" unique_strength="1" exp_strength="0"/>
    <constraint constraints="3" field="numero" notnull_strength="1" unique_strength="1" exp_strength="0"/>
    <constraint constraints="0" field="insee" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="id_support" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="id_foyer" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="id_depart" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="id_armoire" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="1" field="etat" notnull_strength="1" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="hauteur_feu" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="marque" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="type_vasque" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="classe_electrique" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="degre_protection" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="resistance_choc" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="num_phase" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="varistance" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="ral" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="cellule" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="date_pose" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="date_depose" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="date_action" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="desc_action" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="remarque" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="mslink" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="mapid" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="ecp_intervention_id" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="acteur" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="uid4" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="created_at" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="updated_at" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="pkid" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="intervention" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="rot_z" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="photo" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="tri_id" notnull_strength="0" unique_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="numero" desc="" exp=""/>
    <constraint field="insee" desc="" exp=""/>
    <constraint field="id_support" desc="" exp=""/>
    <constraint field="id_foyer" desc="" exp=""/>
    <constraint field="id_depart" desc="" exp=""/>
    <constraint field="id_armoire" desc="" exp=""/>
    <constraint field="etat" desc="" exp=""/>
    <constraint field="hauteur_feu" desc="" exp=""/>
    <constraint field="marque" desc="" exp=""/>
    <constraint field="type_vasque" desc="" exp=""/>
    <constraint field="classe_electrique" desc="" exp=""/>
    <constraint field="degre_protection" desc="" exp=""/>
    <constraint field="resistance_choc" desc="" exp=""/>
    <constraint field="num_phase" desc="" exp=""/>
    <constraint field="varistance" desc="" exp=""/>
    <constraint field="ral" desc="" exp=""/>
    <constraint field="cellule" desc="" exp=""/>
    <constraint field="date_pose" desc="" exp=""/>
    <constraint field="date_depose" desc="" exp=""/>
    <constraint field="date_action" desc="" exp=""/>
    <constraint field="desc_action" desc="" exp=""/>
    <constraint field="remarque" desc="" exp=""/>
    <constraint field="mslink" desc="" exp=""/>
    <constraint field="mapid" desc="" exp=""/>
    <constraint field="ecp_intervention_id" desc="" exp=""/>
    <constraint field="acteur" desc="" exp=""/>
    <constraint field="uid4" desc="" exp=""/>
    <constraint field="created_at" desc="" exp=""/>
    <constraint field="updated_at" desc="" exp=""/>
    <constraint field="pkid" desc="" exp=""/>
    <constraint field="intervention" desc="" exp=""/>
    <constraint field="rot_z" desc="" exp=""/>
    <constraint field="photo" desc="" exp=""/>
    <constraint field="tri_id" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields>
    <field type="10" expression="case when substr(&quot;id_foyer&quot;,1,1) = '*' then&#xd;&#xa;substr(&quot;id_foyer&quot;,3) + 100000&#xd;&#xa;else&#xd;&#xa;substr(&quot;id_armoire&quot;,3)*10000 + 10000 || substr(&quot;id_foyer&quot;,2)+1000&#xd;&#xa;end" precision="0" length="0" name="tri_id" typeName="string" subType="0" comment=""/>
  </expressionfields>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
    <actionsetting type="5" notificationMessage="" shortTitle="photos " isEnabledOnlyWhenEditable="0" action="[%photo%]" name="photos" icon="P:/SIG/gestion/svg/elts_divers/icon_photo.svg" capture="0" id="{d4275832-bedf-4526-89d8-927c85354d7e}">
      <actionScope id="Canvas"/>
      <actionScope id="Field"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="&quot;tri_id&quot;">
    <columns>
      <column type="field" hidden="0" name="insee" width="-1"/>
      <column type="field" hidden="0" name="id_armoire" width="-1"/>
      <column type="field" hidden="0" name="id_depart" width="-1"/>
      <column type="field" hidden="0" name="id_foyer" width="-1"/>
      <column type="field" hidden="0" name="id_support" width="-1"/>
      <column type="field" hidden="0" name="etat" width="-1"/>
      <column type="field" hidden="0" name="marque" width="183"/>
      <column type="field" hidden="0" name="type_vasque" width="-1"/>
      <column type="field" hidden="0" name="ral" width="-1"/>
      <column type="field" hidden="0" name="hauteur_feu" width="-1"/>
      <column type="field" hidden="0" name="cellule" width="-1"/>
      <column type="field" hidden="0" name="date_pose" width="-1"/>
      <column type="field" hidden="0" name="date_depose" width="-1"/>
      <column type="field" hidden="0" name="acteur" width="-1"/>
      <column type="field" hidden="0" name="mslink" width="-1"/>
      <column type="field" hidden="0" name="mapid" width="-1"/>
      <column type="field" hidden="0" name="created_at" width="-1"/>
      <column type="field" hidden="0" name="updated_at" width="190"/>
      <column type="field" hidden="0" name="remarque" width="-1"/>
      <column type="field" hidden="0" name="rot_z" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="num_phase" width="-1"/>
      <column type="field" hidden="0" name="varistance" width="-1"/>
      <column type="field" hidden="0" name="date_action" width="-1"/>
      <column type="field" hidden="0" name="photo" width="278"/>
      <column type="field" hidden="0" name="uid4" width="-1"/>
      <column type="field" hidden="0" name="numero" width="-1"/>
      <column type="field" hidden="0" name="classe_electrique" width="-1"/>
      <column type="field" hidden="0" name="degre_protection" width="-1"/>
      <column type="field" hidden="0" name="resistance_choc" width="-1"/>
      <column type="field" hidden="0" name="desc_action" width="-1"/>
      <column type="field" hidden="0" name="ecp_intervention_id" width="-1"/>
      <column type="field" hidden="0" name="pkid" width="-1"/>
      <column type="field" hidden="0" name="fid" width="-1"/>
      <column type="field" hidden="0" name="tri_id" width="-1"/>
      <column type="field" hidden="0" name="intervention" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/technicien_08/Desktop/2023-A-002-88148-ep/SIG/gestion</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Users/technicien_08/Desktop/2023-A-002-88148-ep/SIG/gestion</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelFont="0" labelColor="" overrideLabelColor="0">
      <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.3,-1,5,50,0,0,0,0,0"/>
    </labelStyle>
    <attributeEditorContainer verticalStretch="0" type="Tab" horizontalStretch="0" collapsedExpression="" columnCount="1" showLabel="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" visibilityExpression="" name="Fiche descriptive" groupBox="0" backgroundColor="#a6cee3">
      <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
        <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </labelStyle>
      <attributeEditorContainer verticalStretch="0" type="GroupBox" horizontalStretch="0" collapsedExpression="" columnCount="1" showLabel="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" visibilityExpression="" name="Généralités" groupBox="1" backgroundColor="#a6cee3">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
          <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
        </labelStyle>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="31" showLabel="1" name="intervention">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="26" showLabel="1" name="acteur">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="2" showLabel="1" name="insee">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer verticalStretch="0" type="GroupBox" horizontalStretch="0" collapsedExpression="" columnCount="1" showLabel="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" visibilityExpression="&quot;id&quot;" name="Identification" groupBox="1" backgroundColor="#a6cee3">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
          <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
        </labelStyle>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="6" showLabel="1" name="id_armoire">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="5" showLabel="1" name="id_depart">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="3" showLabel="1" name="id_support">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer verticalStretch="0" type="GroupBox" horizontalStretch="0" collapsedExpression="" columnCount="1" showLabel="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" visibilityExpression="" name="Etat" groupBox="1" backgroundColor="#a6cee3">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
          <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
        </labelStyle>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="4" showLabel="1" name="id_foyer">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="7" showLabel="1" name="etat">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="14" showLabel="1" name="num_phase">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="18" showLabel="1" name="date_pose">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer verticalStretch="0" type="GroupBox" horizontalStretch="0" collapsedExpression="" columnCount="1" showLabel="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" visibilityExpression="" name="Caractéristiques techniques" groupBox="1" backgroundColor="#a6cee3">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
          <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
        </labelStyle>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="9" showLabel="1" name="marque">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="10" showLabel="1" name="type_vasque">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="16" showLabel="1" name="ral">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="8" showLabel="1" name="hauteur_feu">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField verticalStretch="0" horizontalStretch="0" index="10" showLabel="1" name="type_vasque">
          <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
            <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer verticalStretch="0" type="Tab" horizontalStretch="0" collapsedExpression="" columnCount="1" showLabel="1" collapsedExpressionEnabled="0" visibilityExpressionEnabled="0" collapsed="0" visibilityExpression="" name="Remarques" groupBox="0" backgroundColor="#a6cee3">
      <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
        <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </labelStyle>
      <attributeEditorField verticalStretch="0" horizontalStretch="0" index="22" showLabel="1" name="remarque">
        <labelStyle overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1" overrideLabelColor="0">
          <labelFont style="" bold="0" italic="0" underline="0" strikethrough="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="acteur" editable="1"/>
    <field name="cellule" editable="1"/>
    <field name="classe_elec" editable="1"/>
    <field name="classe_electrique" editable="1"/>
    <field name="created_at" editable="1"/>
    <field name="date_action" editable="1"/>
    <field name="date_depose" editable="1"/>
    <field name="date_intervention" editable="1"/>
    <field name="date_pose" editable="1"/>
    <field name="degre_protection" editable="1"/>
    <field name="desc_action" editable="1"/>
    <field name="ecp_intervention_id" editable="1"/>
    <field name="entreprises maintenance 2019/2022adherent" editable="0"/>
    <field name="entreprises maintenance 2019/2022entreprise" editable="0"/>
    <field name="entreprises maintenance 2019/2022lot" editable="0"/>
    <field name="etat" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="hauteur_feu" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_armoire" editable="1"/>
    <field name="id_depart" editable="1"/>
    <field name="id_foyer" editable="1"/>
    <field name="id_intervention" editable="1"/>
    <field name="id_support" editable="1"/>
    <field name="insee" editable="1"/>
    <field name="intervention" editable="1"/>
    <field name="mapid" editable="1"/>
    <field name="marque" editable="1"/>
    <field name="mslink" editable="1"/>
    <field name="num_phase" editable="1"/>
    <field name="numero" editable="1"/>
    <field name="photo" editable="1"/>
    <field name="pkid" editable="1"/>
    <field name="protection_ip" editable="1"/>
    <field name="ral" editable="1"/>
    <field name="remarque" editable="1"/>
    <field name="resistance_choc" editable="1"/>
    <field name="resistance_ik" editable="1"/>
    <field name="rot_z" editable="1"/>
    <field name="sources_appareillage" editable="0"/>
    <field name="sources_culot" editable="0"/>
    <field name="sources_date_pose" editable="0"/>
    <field name="sources_etat" editable="0"/>
    <field name="sources_id_foyer" editable="0"/>
    <field name="sources_insee" editable="0"/>
    <field name="sources_localisation_appareillage" editable="0"/>
    <field name="sources_nature" editable="0"/>
    <field name="sources_nature_source" editable="0"/>
    <field name="sources_numero" editable="0"/>
    <field name="sources_photometrie" editable="0"/>
    <field name="sources_puissance" editable="0"/>
    <field name="sources_remarque" editable="0"/>
    <field name="sources_temperature_couleur" editable="0"/>
    <field name="tri_id" editable="0"/>
    <field name="type_vasque" editable="1"/>
    <field name="uid4" editable="1"/>
    <field name="updated_at" editable="1"/>
    <field name="varistance" editable="1"/>
    <field name="vue_sources_acteur" editable="0"/>
    <field name="vue_sources_appareillage" editable="0"/>
    <field name="vue_sources_created_at" editable="0"/>
    <field name="vue_sources_culot" editable="0"/>
    <field name="vue_sources_date_intervention" editable="0"/>
    <field name="vue_sources_date_pose" editable="0"/>
    <field name="vue_sources_etat" editable="0"/>
    <field name="vue_sources_id" editable="0"/>
    <field name="vue_sources_id_foyer" editable="0"/>
    <field name="vue_sources_id_intervention" editable="0"/>
    <field name="vue_sources_insee" editable="0"/>
    <field name="vue_sources_localisation_appareillage" editable="0"/>
    <field name="vue_sources_nature_source" editable="0"/>
    <field name="vue_sources_photometrie" editable="0"/>
    <field name="vue_sources_puissance" editable="0"/>
    <field name="vue_sources_remarque" editable="0"/>
    <field name="vue_sources_temperature_couleur" editable="0"/>
    <field name="vue_sources_updated_at" editable="0"/>
  </editable>
  <labelOnTop>
    <field name="acteur" labelOnTop="1"/>
    <field name="cellule" labelOnTop="0"/>
    <field name="classe_elec" labelOnTop="0"/>
    <field name="classe_electrique" labelOnTop="0"/>
    <field name="created_at" labelOnTop="0"/>
    <field name="date_action" labelOnTop="0"/>
    <field name="date_depose" labelOnTop="1"/>
    <field name="date_intervention" labelOnTop="0"/>
    <field name="date_pose" labelOnTop="1"/>
    <field name="degre_protection" labelOnTop="0"/>
    <field name="desc_action" labelOnTop="0"/>
    <field name="ecp_intervention_id" labelOnTop="0"/>
    <field name="entreprises maintenance 2019/2022adherent" labelOnTop="0"/>
    <field name="entreprises maintenance 2019/2022entreprise" labelOnTop="0"/>
    <field name="entreprises maintenance 2019/2022lot" labelOnTop="0"/>
    <field name="etat" labelOnTop="1"/>
    <field name="fid" labelOnTop="0"/>
    <field name="hauteur_feu" labelOnTop="1"/>
    <field name="id" labelOnTop="0"/>
    <field name="id_armoire" labelOnTop="1"/>
    <field name="id_depart" labelOnTop="1"/>
    <field name="id_foyer" labelOnTop="1"/>
    <field name="id_intervention" labelOnTop="0"/>
    <field name="id_support" labelOnTop="1"/>
    <field name="insee" labelOnTop="1"/>
    <field name="intervention" labelOnTop="1"/>
    <field name="mapid" labelOnTop="0"/>
    <field name="marque" labelOnTop="1"/>
    <field name="mslink" labelOnTop="0"/>
    <field name="num_phase" labelOnTop="1"/>
    <field name="numero" labelOnTop="0"/>
    <field name="photo" labelOnTop="1"/>
    <field name="pkid" labelOnTop="0"/>
    <field name="protection_ip" labelOnTop="0"/>
    <field name="ral" labelOnTop="1"/>
    <field name="remarque" labelOnTop="1"/>
    <field name="resistance_choc" labelOnTop="0"/>
    <field name="resistance_ik" labelOnTop="0"/>
    <field name="rot_z" labelOnTop="0"/>
    <field name="sources_appareillage" labelOnTop="0"/>
    <field name="sources_culot" labelOnTop="0"/>
    <field name="sources_date_pose" labelOnTop="0"/>
    <field name="sources_etat" labelOnTop="0"/>
    <field name="sources_id_foyer" labelOnTop="0"/>
    <field name="sources_insee" labelOnTop="0"/>
    <field name="sources_localisation_appareillage" labelOnTop="0"/>
    <field name="sources_nature" labelOnTop="0"/>
    <field name="sources_nature_source" labelOnTop="0"/>
    <field name="sources_numero" labelOnTop="0"/>
    <field name="sources_photometrie" labelOnTop="0"/>
    <field name="sources_puissance" labelOnTop="0"/>
    <field name="sources_remarque" labelOnTop="0"/>
    <field name="sources_temperature_couleur" labelOnTop="0"/>
    <field name="tri_id" labelOnTop="0"/>
    <field name="type_vasque" labelOnTop="1"/>
    <field name="uid4" labelOnTop="0"/>
    <field name="updated_at" labelOnTop="0"/>
    <field name="varistance" labelOnTop="0"/>
    <field name="vue_sources_acteur" labelOnTop="0"/>
    <field name="vue_sources_appareillage" labelOnTop="0"/>
    <field name="vue_sources_created_at" labelOnTop="0"/>
    <field name="vue_sources_culot" labelOnTop="0"/>
    <field name="vue_sources_date_intervention" labelOnTop="0"/>
    <field name="vue_sources_date_pose" labelOnTop="0"/>
    <field name="vue_sources_etat" labelOnTop="0"/>
    <field name="vue_sources_id" labelOnTop="0"/>
    <field name="vue_sources_id_foyer" labelOnTop="0"/>
    <field name="vue_sources_id_intervention" labelOnTop="0"/>
    <field name="vue_sources_insee" labelOnTop="0"/>
    <field name="vue_sources_localisation_appareillage" labelOnTop="0"/>
    <field name="vue_sources_nature_source" labelOnTop="0"/>
    <field name="vue_sources_photometrie" labelOnTop="0"/>
    <field name="vue_sources_puissance" labelOnTop="0"/>
    <field name="vue_sources_remarque" labelOnTop="0"/>
    <field name="vue_sources_temperature_couleur" labelOnTop="0"/>
    <field name="vue_sources_updated_at" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="acteur"/>
    <field reuseLastValue="0" name="cellule"/>
    <field reuseLastValue="0" name="classe_electrique"/>
    <field reuseLastValue="0" name="created_at"/>
    <field reuseLastValue="0" name="date_action"/>
    <field reuseLastValue="0" name="date_depose"/>
    <field reuseLastValue="0" name="date_pose"/>
    <field reuseLastValue="0" name="degre_protection"/>
    <field reuseLastValue="0" name="desc_action"/>
    <field reuseLastValue="0" name="ecp_intervention_id"/>
    <field reuseLastValue="0" name="etat"/>
    <field reuseLastValue="0" name="fid"/>
    <field reuseLastValue="0" name="hauteur_feu"/>
    <field reuseLastValue="0" name="id_armoire"/>
    <field reuseLastValue="0" name="id_depart"/>
    <field reuseLastValue="0" name="id_foyer"/>
    <field reuseLastValue="0" name="id_support"/>
    <field reuseLastValue="0" name="insee"/>
    <field reuseLastValue="0" name="intervention"/>
    <field reuseLastValue="0" name="mapid"/>
    <field reuseLastValue="0" name="marque"/>
    <field reuseLastValue="0" name="mslink"/>
    <field reuseLastValue="0" name="num_phase"/>
    <field reuseLastValue="0" name="numero"/>
    <field reuseLastValue="0" name="photo"/>
    <field reuseLastValue="0" name="pkid"/>
    <field reuseLastValue="0" name="ral"/>
    <field reuseLastValue="0" name="remarque"/>
    <field reuseLastValue="0" name="resistance_choc"/>
    <field reuseLastValue="0" name="rot_z"/>
    <field reuseLastValue="0" name="tri_id"/>
    <field reuseLastValue="0" name="type_vasque"/>
    <field reuseLastValue="0" name="uid4"/>
    <field reuseLastValue="0" name="updated_at"/>
    <field reuseLastValue="0" name="varistance"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="intervention">
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option type="Map" name="properties">
          <Option type="Map" name="dataDefinedAlias">
            <Option type="bool" value="false" name="active"/>
            <Option type="int" value="1" name="type"/>
            <Option type="QString" value="" name="val"/>
          </Option>
        </Option>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>COALESCE( "id_foyer", '&lt;NULL>' )</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
