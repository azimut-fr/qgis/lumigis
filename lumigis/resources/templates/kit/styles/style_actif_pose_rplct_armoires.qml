<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis symbologyReferenceScale="200" simplifyLocal="1" simplifyMaxScale="1" readOnly="0" maxScale="1" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" labelsEnabled="1" simplifyDrawingHints="0" minScale="5000" version="3.36.2-Maidenhead" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal fixedDuration="0" mode="0" endExpression="" startField="date_pose" durationUnit="min" enabled="0" limitMode="0" startExpression="" accumulate="0" endField="" durationField="fid">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation showMarkerSymbolInSurfacePlots="0" clamping="Terrain" zoffset="0" extrusion="0" symbology="Line" zscale="1" type="IndividualFeatures" binding="Centroid" respectLayerSymbol="1" extrusionEnabled="0">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0" frame_rate="10" type="line">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" enabled="1" id="{b3f36740-4225-4233-84e1-6a6fd6760f34}" locked="0">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="145,82,45,255,rgb:0.56862745098039214,0.32156862745098042,0.17647058823529413,1" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.6" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0" frame_rate="10" type="fill">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" enabled="1" id="{5bf613ca-7dff-47e5-825d-6d19cfd5f129}" locked="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
            <Option value="145,82,45,255,rgb:0.56862745098039214,0.32156862745098042,0.17647058823529413,1" name="color" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="104,59,32,255,rgb:0.40784313725490196,0.23137254901960785,0.12549019607843137,1" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="solid" name="style" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" id="{0bf61048-bfd9-4ee6-b4bc-50ee531a9303}" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="145,82,45,255,rgb:0.56862745098039214,0.32156862745098042,0.17647058823529413,1" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="104,59,32,255,rgb:0.40784313725490196,0.23137254901960785,0.12549019607843137,1" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 enableorderby="0" forceraster="0" referencescale="200" symbollevels="0" type="RuleRenderer">
    <rules key="{9584e0b9-7efd-4ac4-ad63-1d6ac48b5ccb}">
      <rule key="{5a391dc9-9f01-435b-91fe-4d346f14cb9d}" label="Actif" filter=" &quot;etat&quot; = 'Actif' ">
        <rule key="{7a8c7280-bab7-423f-80da-b70057d3d9e5}" symbol="0" filter=" &quot;cellule&quot; = 'ARMOIRE'"/>
      </rule>
      <rule key="{0c02fe0b-4204-4fd4-b7bf-3876705a141a}" label="A poser" filter=" &quot;etat&quot; =  'A poser' ">
        <rule key="{51d4d851-6fe8-44ea-976d-0efc00a7cfac}" symbol="1" filter=" &quot;cellule&quot; =  'ARMOIRE A POSER' "/>
      </rule>
      <rule key="{9bda2467-3988-4c37-ade0-4d4c88177bf8}" label="A remplacer" filter=" &quot;etat&quot; =  'A remplacer' ">
        <rule key="{01f36089-db89-4c84-8076-60fb900de8b9}" symbol="2" filter=" &quot;cellule&quot; = 'ARMOIRE'"/>
      </rule>
      <rule key="{47fdf281-e02e-4585-9222-1bd55f7819d6}" label="A déposer" filter=" &quot;etat&quot; =  'A déposer' ">
        <rule key="{942512db-e2ad-4635-9622-0cfa8a211176}" symbol="3" filter=" &quot;cellule&quot; = 'ARMOIRE'"/>
      </rule>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="0" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" id="{b1c29d63-7e98-4c50-84c4-9447110df56c}" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255,rgb:0.6705882352941176,0.73333333333333328,0.99607843137254903,1" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuMzkzOCAyNTE1Ljc0OCIKICAgaWQ9InN2ZzgzMTMiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9YXJtb2lyZS5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSmVhbiBDbGF1ZGVcRGVza3RvcFxwaG90b3MgY2VsbHVsZXNcc21kZXY9YXJtb2lyZS5zdmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczgzMTUiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJiYXNlIgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxLjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnpvb209IjAuMTg1MzU0MDciCiAgICAgaW5rc2NhcGU6Y3g9IjI4NzAuMTgyNCIKICAgICBpbmtzY2FwZTpjeT0iMTYwMi4zMzg3IgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnOTQ3IgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGJvcmRlcmxheWVyPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjQ1MjIuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjM3NTQuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgxMDYiCiAgICAgICBvcmlnaW54PSIwIgogICAgICAgb3JpZ2lueT0iMCIKICAgICAgIHNwYWNpbmd4PSIxIgogICAgICAgc3BhY2luZ3k9IjEiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhODMxOCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpbmtzY2FwZTpsYWJlbD0iQ2FscXVlIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsNDI2Mi41OTk0KSI+CiAgICA8ZwogICAgICAgaWQ9Imc5NDciCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5MjQ0OTQsMCwwLDAuOTk2NTcyNTMsLTQ2NzYuMjEzNSwtMzE3Ny4zMzYyKSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDAzIgogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLDAuOTk1ODYyNjcsLTEuMDA0MTU1MywwLDgzMjkuODE4NywxMTkzLjM2NzMpIj4KICAgICAgICA8cmVjdAogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwZmY7c3Ryb2tlLXdpZHRoOjEyMy45NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlkPSJyZWN0OTAwIgogICAgICAgICAgIHdpZHRoPSIyMzU2LjM3NDgiCiAgICAgICAgICAgaGVpZ2h0PSI1MTkxLjAyMDUiCiAgICAgICAgICAgeD0iLTIxOTkuNTI3OCIKICAgICAgICAgICB5PSItMTY4MC4xNTU4IiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDBmZjtmaWxsLW9wYWNpdHk6MC45ODgyMDg7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjIxODYuOTY7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiCiAgICAgICAgICAgZD0iTSAtNDMyLjI0Njg3LDM1MTAuODY0OCBIIC0xMDIxLjM0MDUgViAyMjEzLjEwOTcgOTE1LjM1NDQ5IGggNTg5LjA5MzYzIDU4OS4wOTM2OSBWIDIyMTMuMTA5NyAzNTEwLjg2NDggWiIKICAgICAgICAgICBpZD0icGF0aDQ4MzEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMDAwZmY7ZmlsbC1vcGFjaXR5OjAuOTg4MjA4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyMTQ2LjIxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIgogICAgICAgICAgIGQ9Ik0gLTE2MDIuMDU3Miw5MTUuMzU0NSBIIC0yMTgyLjc3NCBWIC0zNzIuODcwNzcgLTE2NjEuMDk2NSBoIDU4MC43MTY4IDU4MC43MTY2IFYgLTM3Mi44NzA3NyA5MTUuMzU0NSBaIgogICAgICAgICAgIGlkPSJwYXRoNDgyOSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMTEuNjE3Njk5LDkuOTUxNzU1MSwwLC04MTkuNzc0Myw5OC42NjA0NCkiCiAgICAgICAgICAgaWQ9Imc0NzY2IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiPgogICAgICAgICAgPGxpbmUKICAgICAgICAgICAgIGlkPSJsaW5lMiIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDQiCiAgICAgICAgICAgICBkPSJtIDIuOTk4LDc0LjM3NyBjIC0wLjUwNCwwIC0xLjAxNCwtMC4xMjkgLTEuNDgzLC0wLjM5NiAtMS40MzksLTAuODIgLTEuOTQsLTIuNjUyIC0xLjEyLC00LjA5MiBsIDM5LC02OC4zNzQgYyAwLjgyMSwtMS40MzkgMi42NTQsLTEuOTQxIDQuMDkyLC0xLjEyIDEuNDM5LDAuODIxIDEuOTQsMi42NTMgMS4xMTksNC4wOTIgbCAtMzksNjguMzc0IEMgNS4wNTMsNzMuODMzIDQuMDQsNzQuMzc3IDIuOTk4LDc0LjM3NyBaIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMDAwMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgaWQ9InBhdGg2IgogICAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgIGlkPSJwYXRoOCIKICAgICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIGlkPSJwb2x5Z29uMTAiCiAgICAgICAgICAgICBwb2ludHM9IjQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzICIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjAwMDAiIC8+CiAgICAgICAgPC9nPgogICAgICAgIDxnCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwtMTEuNjE3Njk5LDkuOTUxNzU0NywwLC0xOTg4Ljc5MjMsMjY5NS43OTY1KSIKICAgICAgICAgICBpZD0iZzQ3NjYtOSIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIj4KICAgICAgICAgIDxsaW5lCiAgICAgICAgICAgICBpZD0ibGluZTItNCIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDQtMCIKICAgICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDYtOSIKICAgICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDgtOSIKICAgICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYwMDAwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIGlkPSJwb2x5Z29uMTAtOCIKICAgICAgICAgICAgIHBvaW50cz0iNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMDAwMCIgLz4KICAgICAgICA8L2c+CiAgICAgIDwvZz4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo=" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="rot_z" name="field" type="QString"/>
                  <Option value="2" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="1" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" id="{c8c857c5-e6bf-4caa-8e34-21813abb8a1b}" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255,rgb:0.6705882352941176,0.73333333333333328,0.99607843137254903,1" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuMzkzOCAyNTE1Ljc0OCIKICAgaWQ9InN2ZzgzMTMiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9YXJtb2lyZV9hX3Bvc2VyLnN2ZyIKICAgaW5rc2NhcGU6ZXhwb3J0LWZpbGVuYW1lPSJDOlxVc2Vyc1xKZWFuIENsYXVkZVxEZXNrdG9wXHBob3RvcyBjZWxsdWxlc1xzbWRldj1hcm1vaXJlLnN2Zy5wbmciCiAgIGlua3NjYXBlOmV4cG9ydC14ZHBpPSI5NiIKICAgaW5rc2NhcGU6ZXhwb3J0LXlkcGk9Ijk2IgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzODMxNSIgLz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9ImJhc2UiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMC4wIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4zMiIKICAgICBpbmtzY2FwZTpjeD0iMTM0Ni44NzUiCiAgICAgaW5rc2NhcGU6Y3k9IjI0NDYuODc1IgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnOTQ3IgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGJvcmRlcmxheWVyPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjQ1MjIuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjM3NTQuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXRvLWd1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgxMDYiCiAgICAgICBvcmlnaW54PSIwIgogICAgICAgb3JpZ2lueT0iMCIKICAgICAgIHNwYWNpbmd4PSIxIgogICAgICAgc3BhY2luZ3k9IjEiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhODMxOCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpbmtzY2FwZTpsYWJlbD0iQ2FscXVlIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsNDI2Mi41OTk0KSI+CiAgICA8ZwogICAgICAgaWQ9Imc5NDciCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5MjQ0OTQsMCwwLDAuOTk2NTcyNTMsLTQ2NzYuMjEzNSwtMzE3Ny4zMzYyKSI+CiAgICAgIDxnCiAgICAgICAgIGlkPSJnNDE5IgogICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSg5NjI5LjY5NDYsMzYxOS4zMTAzKSI+CiAgICAgICAgPHJlY3QKICAgICAgICAgICB5PSItMzg1LjIxNDMyIgogICAgICAgICAgIHg9Ii00NjE3Ljg5MjEiCiAgICAgICAgICAgaGVpZ2h0PSI1MjEyLjU4NjQiCiAgICAgICAgICAgd2lkdGg9IjIzNDYuNjI1NyIKICAgICAgICAgICBpZD0icmVjdDkwMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojZmYzM2ZmO3N0cm9rZS13aWR0aDoxMjMuOTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MC45OTYwNzgiCiAgICAgICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoOTApIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDQ4MzEiCiAgICAgICAgICAgZD0ibSAtNDc4Ny43NzAyLC0yODgyLjU4NjYgdiAtNTU5Ljk2NDYgaCAxMjg1LjU5OCAxMjg1LjU5ODIgdiA1NTkuOTY0NiA1NTkuOTY0NyBoIC0xMjg1LjU5ODIgLTEyODUuNTk4IHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MjEyMi4yO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDQ4MjkiCiAgICAgICAgICAgZD0ibSAtMjIxNi41NzQsLTQwMjEuODc5NSB2IC01NzkuMzI4NCBIIC05MjUuMjQ5MzIgMzY2LjA3NTc3IHYgNTc5LjMyODQgNTc5LjMyODMgSCAtOTI1LjI0OTMyIC0yMjE2LjU3NCBaIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjMzZmY7ZmlsbC1vcGFjaXR5OjAuOTk2MDc4O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyMTQ2LjIyO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlIiAvPgogICAgICAgIDxnCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MSIKICAgICAgICAgICBpZD0iZzQ3NjYiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMTEuNjY1OTY1LDAsMCw5LjkxMDU4MTYsLTE0MDAuOTkyLC0zMjQzLjg0NzMpIj4KICAgICAgICAgIDxsaW5lCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICAgIHgyPSIzIgogICAgICAgICAgICAgeTI9IjcxLjM3NSIKICAgICAgICAgICAgIGlkPSJsaW5lMiIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0ibSAyLjk5OCw3NC4zNzcgYyAtMC41MDQsMCAtMS4wMTQsLTAuMTI5IC0xLjQ4MywtMC4zOTYgLTEuNDM5LC0wLjgyIC0xLjk0LC0yLjY1MiAtMS4xMiwtNC4wOTIgbCAzOSwtNjguMzc0IGMgMC44MjEsLTEuNDM5IDIuNjU0LC0xLjk0MSA0LjA5MiwtMS4xMiAxLjQzOSwwLjgyMSAxLjk0LDIuNjUzIDEuMTE5LDQuMDkyIGwgLTM5LDY4LjM3NCBDIDUuMDUzLDczLjgzMyA0LjA0LDc0LjM3NyAyLjk5OCw3NC4zNzcgWiIKICAgICAgICAgICAgIGlkPSJwYXRoNCIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MSIKICAgICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgICBpZD0icGF0aDYiCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjM0ZmY7ZmlsbC1vcGFjaXR5OjEiCiAgICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgICAgaWQ9InBhdGg4IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwb2x5Z29uCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgcG9pbnRzPSI0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSAiCiAgICAgICAgICAgICBpZD0icG9seWdvbjEwIiAvPgogICAgICAgIDwvZz4KICAgICAgICA8ZwogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjM0ZmY7ZmlsbC1vcGFjaXR5OjEiCiAgICAgICAgICAgaWQ9Imc0NzY2LTkiCiAgICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMTEuNjY1OTY1LDAsMCw5LjkxMDU4MTIsLTQwMDguOTE3OSwtNDQwOC4wMjg3KSI+CiAgICAgICAgICA8bGluZQogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmMzRmZjtmaWxsLW9wYWNpdHk6MSIKICAgICAgICAgICAgIHgxPSI0MiIKICAgICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgICB4Mj0iMyIKICAgICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgICBpZD0ibGluZTItNCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0ibSAyLjk5OCw3NC4zNzcgYyAtMC41MDQsMCAtMS4wMTQsLTAuMTI5IC0xLjQ4MywtMC4zOTYgLTEuNDM5LC0wLjgyIC0xLjk0LC0yLjY1MiAtMS4xMiwtNC4wOTIgbCAzOSwtNjguMzc0IGMgMC44MjEsLTEuNDM5IDIuNjU0LC0xLjk0MSA0LjA5MiwtMS4xMiAxLjQzOSwwLjgyMSAxLjk0LDIuNjUzIDEuMTE5LDQuMDkyIGwgLTM5LDY4LjM3NCBDIDUuMDUzLDczLjgzMyA0LjA0LDc0LjM3NyAyLjk5OCw3NC4zNzcgWiIKICAgICAgICAgICAgIGlkPSJwYXRoNC0wIgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICAgIGlkPSJwYXRoNi05IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgZD0ibSA4MC4zNzgsNzQuMzc3IGMgLTEuMDUsMCAtMi4wNjgsLTAuNTUzIC0yLjYxOSwtMS41MzMgTCAzOS4zODQsNC40NyBjIC0wLjgxMSwtMS40NDUgLTAuMjk3LC0zLjI3MyAxLjE0OCwtNC4wODQgMS40NDUsLTAuODEzIDMuMjczLC0wLjI5NyA0LjA4NCwxLjE0OCBsIDM4LjM3NSw2OC4zNzQgYyAwLjgxMSwxLjQ0NSAwLjI5NywzLjI3NSAtMS4xNDcsNC4wODYgLTAuNDY1LDAuMjU4IC0wLjk2OSwwLjM4MyAtMS40NjYsMC4zODMgeiIKICAgICAgICAgICAgIGlkPSJwYXRoOC05IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwb2x5Z29uCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmYzNGZmO2ZpbGwtb3BhY2l0eToxIgogICAgICAgICAgICAgcG9pbnRzPSI0Ni4xNTMsNTYuNDM2IDQ0LjE3OCw1Ni40ODIgNDkuMDY4LDM4LjUyNiAzOS4yOTEsNDIuMzggNDYuMzQxLDI2Ljg0NSA0MC43MDEsMjYuODQ1IDM0LjMwOCw0OS4xNDkgNDMuNTIxLDQ1LjQ4MyA0MC43MDEsNTYuMTA1IDM4LjYzMyw1Ni4yNDcgNDEuMzU5LDYzLjUzMyAiCiAgICAgICAgICAgICBpZD0icG9seWdvbjEwLTgiIC8+CiAgICAgICAgPC9nPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="&quot;rot_z&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="2" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" id="{b5fe09a7-4aaf-49f1-86c3-bbe1619529b8}" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255,rgb:0.6705882352941176,0.73333333333333328,0.99607843137254903,1" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuMzkzOCAyNTE1Ljc0OCIKICAgaWQ9InN2ZzgzMTMiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJzZGU9YXJtb2lyZV9hX3JlbXBsYWNlci5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSmVhbiBDbGF1ZGVcRGVza3RvcFxwaG90b3MgY2VsbHVsZXNcc21kZXY9YXJtb2lyZS5zdmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczgzMTUiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJiYXNlIgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxLjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnpvb209IjAuMTc2Nzc2NyIKICAgICBpbmtzY2FwZTpjeD0iMjU4NS4xODIzIgogICAgIGlua3NjYXBlOmN5PSIyMzMwLjYyMzkiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9Imc5NDciCiAgICAgc2hvd2dyaWQ9InRydWUiCiAgICAgYm9yZGVybGF5ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxOTIwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMjciCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5MTIiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgdW5pdHM9ImNtIgogICAgIHNob3dndWlkZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1zdGFydD0iNDUyMi42NywtMTAyNCIKICAgICBpbmtzY2FwZTptZWFzdXJlLWVuZD0iMzc1NC42NywtMTAyNCIKICAgICBpbmtzY2FwZTpzbmFwLW90aGVycz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1pbnRlcnNlY3Rpb24tcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1wYWdlPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vYmplY3QtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ2xvYmFsPSJ0cnVlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkZXNrY29sb3I9IiNkMWQxZDEiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkODEwNiIKICAgICAgIG9yaWdpbng9IjAiCiAgICAgICBvcmlnaW55PSIwIgogICAgICAgc3BhY2luZ3g9IjEiCiAgICAgICBzcGFjaW5neT0iMSIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGE4MzE4Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlua3NjYXBlOmxhYmVsPSJDYWxxdWUgMSIKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlkPSJsYXllcjEiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCw0MjYyLjU5OTQpIj4KICAgIDxnCiAgICAgICBpZD0iZzk0NyIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuOTkyNDQ5NCwwLDAsMC45OTY1NzI1MywtNDY3Ni4yMTM1LC0zMTc3LjMzNjIpIj4KICAgICAgPGcKICAgICAgICAgaWQ9ImczODciCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAsMC45OTU4NjI2OSwtMS4wMDQxNTQ1LDAsODk5My4wNTE4LC01NzYyLjI0NDQpIj4KICAgICAgICA8cmVjdAogICAgICAgICAgIHk9Ii0xMDE3Ljg4NTkiCiAgICAgICAgICAgeD0iNDc4My4xOTU4IgogICAgICAgICAgIGhlaWdodD0iNTE5MS4wMjA1IgogICAgICAgICAgIHdpZHRoPSIyMzU2LjM3NDgiCiAgICAgICAgICAgaWQ9InJlY3Q5MDAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MDtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6I2ZmYzk0ZTtzdHJva2Utd2lkdGg6MTIzLjk0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjAuOTg4MjM1IiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDQ4MzEiCiAgICAgICAgICAgZD0iTSA2NTI1LjcxMDcsNDEzMy42OTYzIEggNTk2My40MTk1IFYgMjg1My40MTczIDE1NzMuMTM4MSBoIDU2Mi4yOTEyIDU2Mi4yOTEyIHYgMTI4MC4yNzkyIDEyODAuMjc5IHoiCiAgICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1O2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyMTIyLjI7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOm1hcmtlcnMgZmlsbCBzdHJva2UiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIGlkPSJwYXRoNDgyOSIKICAgICAgICAgICBkPSJNIDUzODEuNjg0NiwxNTczLjEzODEgSCA0Nzk5Ljk0OTUgViAyODcuMTU1OTggLTk5OC44MjY1NyBoIDU4MS43MzUxIDU4MS43MzQ5IFYgMjg3LjE1NTk4IDE1NzMuMTM4MSBaIgogICAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MjE0Ni4yMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIgLz4KICAgICAgICA8ZwogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgIGlkPSJnNDc2NiIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTUxLDAsNjE2Mi45NDkyLDc2MC45MzAzMykiPgogICAgICAgICAgPGxpbmUKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICAgIHgyPSIzIgogICAgICAgICAgICAgeTI9IjcxLjM3NSIKICAgICAgICAgICAgIGlkPSJsaW5lMiIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgICBpZD0icGF0aDQiCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICAgIGlkPSJwYXRoNiIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmYzk0ZTtmaWxsLW9wYWNpdHk6MC45ODgyMzUiCiAgICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgICAgaWQ9InBhdGg4IgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwb2x5Z29uCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICAgIHBvaW50cz0iNDYuMTUzLDU2LjQzNiA0NC4xNzgsNTYuNDgyIDQ5LjA2OCwzOC41MjYgMzkuMjkxLDQyLjM4IDQ2LjM0MSwyNi44NDUgNDAuNzAxLDI2Ljg0NSAzNC4zMDgsNDkuMTQ5IDQzLjUyMSw0NS40ODMgNDAuNzAxLDU2LjEwNSAzOC42MzMsNTYuMjQ3IDQxLjM1OSw2My41MzMgIgogICAgICAgICAgICAgaWQ9InBvbHlnb24xMCIgLz4KICAgICAgICA8L2c+CiAgICAgICAgPGcKICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICBpZD0iZzQ3NjYtOSIKICAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTQ3LDAsNDk5My45MzEyLDMzNTguMDY2NCkiPgogICAgICAgICAgPGxpbmUKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICAgIHgyPSIzIgogICAgICAgICAgICAgeTI9IjcxLjM3NSIKICAgICAgICAgICAgIGlkPSJsaW5lMi00IiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgZD0ibSAyLjk5OCw3NC4zNzcgYyAtMC41MDQsMCAtMS4wMTQsLTAuMTI5IC0xLjQ4MywtMC4zOTYgLTEuNDM5LC0wLjgyIC0xLjk0LC0yLjY1MiAtMS4xMiwtNC4wOTIgbCAzOSwtNjguMzc0IGMgMC44MjEsLTEuNDM5IDIuNjU0LC0xLjk0MSA0LjA5MiwtMS4xMiAxLjQzOSwwLjgyMSAxLjk0LDIuNjUzIDEuMTE5LDQuMDkyIGwgLTM5LDY4LjM3NCBDIDUuMDUzLDczLjgzMyA0LjA0LDc0LjM3NyAyLjk5OCw3NC4zNzcgWiIKICAgICAgICAgICAgIGlkPSJwYXRoNC0wIgogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZmZjOTRlO2ZpbGwtb3BhY2l0eTowLjk4ODIzNSIKICAgICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgICBpZD0icGF0aDYtOSIKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmYzk0ZTtmaWxsLW9wYWNpdHk6MC45ODgyMzUiCiAgICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgICAgaWQ9InBhdGg4LTkiCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgICAgPHBvbHlnb24KICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmM5NGU7ZmlsbC1vcGFjaXR5OjAuOTg4MjM1IgogICAgICAgICAgICAgcG9pbnRzPSI0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyA0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCAiCiAgICAgICAgICAgICBpZD0icG9seWdvbjEwLTgiIC8+CiAgICAgICAgPC9nPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="&quot;rot_z&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="3" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" id="{e5034a6e-cf55-42f8-baa4-3f038d6591e2}" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="171,187,254,255,rgb:0.6705882352941176,0.73333333333333328,0.99607843137254903,1" name="color" type="QString"/>
            <Option value="0" name="fixedAspectRatio" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTUxY20iCiAgIGhlaWdodD0iNzFjbSIKICAgdmlld0JveD0iMCAwIDUzNTAuNDM1OCAyNTE1Ljc2NzgiCiAgIGlkPSJzdmc4MzEzIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMi4yICg3MzJhMDFkYTYzLCAyMDIyLTEyLTA5KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic2RlPWFybW9pcmVfZGVwb3Nlci5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSmVhbiBDbGF1ZGVcRGVza3RvcFxwaG90b3MgY2VsbHVsZXNcc21kZXY9YXJtb2lyZS5zdmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM4MzE1Ij4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODgwMCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMGZlO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3OTgiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NzkyIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwZmY7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODc5MCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg3ODQiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDBmNDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4NzgyIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODc3OCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3NzYiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NzcwIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODc2OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg3NTgiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4NzU2IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODc1MCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3NDgiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPG1hcmtlcgogICAgICAgaW5rc2NhcGU6c3RvY2tpZD0iRG90TCIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9IkRvdEwiCiAgICAgICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgIGlua3NjYXBlOmlzc3RvY2s9InRydWUiPgogICAgICA8cGF0aAogICAgICAgICBpZD0icGF0aDg1MjMiCiAgICAgICAgIGQ9Im0gLTIuNSwtMSBjIDAsMi43NiAtMi4yNCw1IC01LDUgLTIuNzYsMCAtNSwtMi4yNCAtNSwtNSAwLC0yLjc2IDIuMjQsLTUgNSwtNSAyLjc2LDAgNSwyLjI0IDUsNSB6IgogICAgICAgICBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjFwdCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC44LDAsMCwwLjgsNS45MiwwLjgpIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgPC9tYXJrZXI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDk3MTIiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0idHJhbnNsYXRlKDM4LjE2MjY1OSwxOTQ1LjE1OCkiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDBmZjtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NzE0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50OTcxMiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgyNDIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIHgxPSIxOS45OTg0MyIKICAgICAgIHkxPSI2Ny42NjkyOTYiCiAgICAgICB4Mj0iODAuNTAyNzY5IgogICAgICAgeTI9IjY3LjY2OTI5NiIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIuODg3NDM5MywtMTAuNTg3Mjc4KSIgLz4KICAgIDxtYXJrZXIKICAgICAgIG9yaWVudD0iYXV0byIKICAgICAgIHJlZlk9IjAiCiAgICAgICByZWZYPSIwIgogICAgICAgaWQ9Im1hcmtlcjExMzIxIgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiPgogICAgICA8cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBpZD0icGF0aDExMzIzIgogICAgICAgICBzdHlsZT0iZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjYyNTtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgZD0iTSA4LjcxODU4NzgsNC4wMzM3MzUyIC0yLjIwNzI4OTUsMC4wMTYwMTMyNiA4LjcxODU4ODQsLTQuMDAxNzA3OCBjIC0xLjc0NTQ5ODQsMi4zNzIwNjA5IC0xLjczNTQ0MDgsNS42MTc0NTE5IC02ZS03LDguMDM1NDQzIHoiCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KC0xLjEsMCwwLC0xLjEsLTEuMSwwKSIgLz4KICAgIDwvbWFya2VyPgogICAgPG1hcmtlcgogICAgICAgc3R5bGU9Im92ZXJmbG93OnZpc2libGUiCiAgICAgICBpZD0ibWFya2VyODQ5MyIKICAgICAgIHJlZlg9IjAiCiAgICAgICByZWZZPSIwIgogICAgICAgb3JpZW50PSJhdXRvIj4KICAgICAgPHBhdGgKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoLTEuMSwwLDAsLTEuMSwtMS4xLDApIgogICAgICAgICBkPSJNIDguNzE4NTg3OCw0LjAzMzczNTIgLTIuMjA3Mjg5NSwwLjAxNjAxMzI2IDguNzE4NTg4NCwtNC4wMDE3MDc4IGMgLTEuNzQ1NDk4NCwyLjM3MjA2MDkgLTEuNzM1NDQwOCw1LjYxNzQ1MTkgLTZlLTcsOC4wMzU0NDMgeiIKICAgICAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzAwMDAwMDtzdHJva2Utd2lkdGg6MC42MjU7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGlkPSJwYXRoODQ5NSIgLz4KICAgIDwvbWFya2VyPgogIDwvZGVmcz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9ImJhc2UiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMC4wIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4yMjYyNzQxNyIKICAgICBpbmtzY2FwZTpjeD0iMTE4NC40MDM5IgogICAgIGlua3NjYXBlOmN5PSIxNzE0LjczNCIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ibGF5ZXIxIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGJvcmRlcmxheWVyPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm1lYXN1cmUtc3RhcnQ9IjQ1MjIuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6bWVhc3VyZS1lbmQ9IjM3NTQuNjcsLTEwMjQiCiAgICAgaW5rc2NhcGU6c25hcC1vdGhlcnM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdyaWRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtdG8tZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MTA2IgogICAgICAgb3JpZ2lueD0iMCIKICAgICAgIG9yaWdpbnk9IjAiCiAgICAgICBzcGFjaW5neD0iMSIKICAgICAgIHNwYWNpbmd5PSIxIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTgzMTgiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxnCiAgICAgaW5rc2NhcGU6bGFiZWw9IkNhbHF1ZSAxIgogICAgIGlua3NjYXBlOmdyb3VwbW9kZT0ibGF5ZXIiCiAgICAgaWQ9ImxheWVyMSIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDQyOTguMDMyNSkiPgogICAgPGcKICAgICAgIGlkPSJnNDI3IgogICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMCwwLjk5OTk5OTk3LC0xLjAwMDAwMTIsMCwxMDcwLjA3OTEsLTQyODAuMzE2KSI+CiAgICAgIDxyZWN0CiAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiNjY2NjY2M7c3Ryb2tlLXdpZHRoOjEyMy45NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBpZD0icmVjdDkwMCIKICAgICAgICAgd2lkdGg9IjIzNTYuMzc0OCIKICAgICAgICAgaGVpZ2h0PSI1MTkxLjAyMDUiCiAgICAgICAgIHg9IjYxLjk3MDE0MiIKICAgICAgICAgeT0iLTQyMDAuNjI5NCIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjIxMjIuMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIKICAgICAgICAgZD0iTSAxODA0LjQ4NTMsOTUwLjk1MjY2IEggMTI0Mi4xOTQxIFYgLTMyOS4zMjY1IC0xNjA5LjYwNTcgaCA1NjIuMjkxMiA1NjIuMjkxMyBWIC0zMjkuMzI2NSA5NTAuOTUyNjYgWiIKICAgICAgICAgaWQ9InBhdGg0ODMxIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MjE0Ni4yMjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZSIKICAgICAgICAgZD0iTSA2NjAuNDU5MjEsLTE2MDkuNjA1NyBIIDc4LjcyNDIgdiAtMTI4NS45ODIgLTEyODUuOTgyNSBoIDU4MS43MzUwMSA1ODEuNzM0ODkgdiAxMjg1Ljk4MjUgMTI4NS45ODIgeiIKICAgICAgICAgaWQ9InBhdGg0ODI5IgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICA8ZwogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTUxLDAsMTQ0MS43MjM5LC0yNDIxLjgxMzMpIgogICAgICAgICBpZD0iZzQ3NjYiCiAgICAgICAgIHN0eWxlPSJmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjEiPgogICAgICAgIDxsaW5lCiAgICAgICAgICAgaWQ9ImxpbmUyIgogICAgICAgICAgIHkyPSI3MS4zNzUiCiAgICAgICAgICAgeDI9IjMiCiAgICAgICAgICAgeTE9IjMuMDAwOTk5OSIKICAgICAgICAgICB4MT0iNDIiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg0IgogICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg2IgogICAgICAgICAgIGQ9Ik0gODAuMzc1LDc0LjM3NSBIIDMgYyAtMS42NTcsMCAtMywtMS4zNDIgLTMsLTMgMCwtMS42NTYgMS4zNDMsLTMgMywtMyBoIDc3LjM3NSBjIDEuNjU3LDAgMywxLjM0NCAzLDMgMCwxLjY1OSAtMS4zNDIsMyAtMywzIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg4IgogICAgICAgICAgIGQ9Im0gODAuMzc4LDc0LjM3NyBjIC0xLjA1LDAgLTIuMDY4LC0wLjU1MyAtMi42MTksLTEuNTMzIEwgMzkuMzg0LDQuNDcgYyAtMC44MTEsLTEuNDQ1IC0wLjI5NywtMy4yNzMgMS4xNDgsLTQuMDg0IDEuNDQ1LC0wLjgxMyAzLjI3MywtMC4yOTcgNC4wODQsMS4xNDggbCAzOC4zNzUsNjguMzc0IGMgMC44MTEsMS40NDUgMC4yOTcsMy4yNzUgLTEuMTQ3LDQuMDg2IC0wLjQ2NSwwLjI1OCAtMC45NjksMC4zODMgLTEuNDY2LDAuMzgzIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cG9seWdvbgogICAgICAgICAgIGlkPSJwb2x5Z29uMTAiCiAgICAgICAgICAgcG9pbnRzPSI0MS4zNTksNjMuNTMzIDQ2LjE1Myw1Ni40MzYgNDQuMTc4LDU2LjQ4MiA0OS4wNjgsMzguNTI2IDM5LjI5MSw0Mi4zOCA0Ni4zNDEsMjYuODQ1IDQwLjcwMSwyNi44NDUgMzQuMzA4LDQ5LjE0OSA0My41MjEsNDUuNDgzIDQwLjcwMSw1Ni4xMDUgMzguNjMzLDU2LjI0NyAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgPC9nPgogICAgICA8ZwogICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLC0xMS42MTc2OTksOS45NTE3NTQ3LDAsMjcyLjcwNTksMTc1LjMyMjcpIgogICAgICAgICBpZD0iZzQ3NjYtOSIKICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSI+CiAgICAgICAgPGxpbmUKICAgICAgICAgICBpZD0ibGluZTItNCIKICAgICAgICAgICB5Mj0iNzEuMzc1IgogICAgICAgICAgIHgyPSIzIgogICAgICAgICAgIHkxPSIzLjAwMDk5OTkiCiAgICAgICAgICAgeDE9IjQyIgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIGlkPSJwYXRoNC0wIgogICAgICAgICAgIGQ9Im0gMi45OTgsNzQuMzc3IGMgLTAuNTA0LDAgLTEuMDE0LC0wLjEyOSAtMS40ODMsLTAuMzk2IC0xLjQzOSwtMC44MiAtMS45NCwtMi42NTIgLTEuMTIsLTQuMDkyIGwgMzksLTY4LjM3NCBjIDAuODIxLC0xLjQzOSAyLjY1NCwtMS45NDEgNC4wOTIsLTEuMTIgMS40MzksMC44MjEgMS45NCwyLjY1MyAxLjExOSw0LjA5MiBsIC0zOSw2OC4zNzQgQyA1LjA1Myw3My44MzMgNC4wNCw3NC4zNzcgMi45OTgsNzQuMzc3IFoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg2LTkiCiAgICAgICAgICAgZD0iTSA4MC4zNzUsNzQuMzc1IEggMyBjIC0xLjY1NywwIC0zLC0xLjM0MiAtMywtMyAwLC0xLjY1NiAxLjM0MywtMyAzLC0zIGggNzcuMzc1IGMgMS42NTcsMCAzLDEuMzQ0IDMsMyAwLDEuNjU5IC0xLjM0MiwzIC0zLDMgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojY2NjY2NjO2ZpbGwtb3BhY2l0eToxIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDgtOSIKICAgICAgICAgICBkPSJtIDgwLjM3OCw3NC4zNzcgYyAtMS4wNSwwIC0yLjA2OCwtMC41NTMgLTIuNjE5LC0xLjUzMyBMIDM5LjM4NCw0LjQ3IGMgLTAuODExLC0xLjQ0NSAtMC4yOTcsLTMuMjczIDEuMTQ4LC00LjA4NCAxLjQ0NSwtMC44MTMgMy4yNzMsLTAuMjk3IDQuMDg0LDEuMTQ4IGwgMzguMzc1LDY4LjM3NCBjIDAuODExLDEuNDQ1IDAuMjk3LDMuMjc1IC0xLjE0Nyw0LjA4NiAtMC40NjUsMC4yNTggLTAuOTY5LDAuMzgzIC0xLjQ2NiwwLjM4MyB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNjY2NjY2M7ZmlsbC1vcGFjaXR5OjEiIC8+CiAgICAgICAgPHBvbHlnb24KICAgICAgICAgICBpZD0icG9seWdvbjEwLTgiCiAgICAgICAgICAgcG9pbnRzPSIzOS4yOTEsNDIuMzggNDYuMzQxLDI2Ljg0NSA0MC43MDEsMjYuODQ1IDM0LjMwOCw0OS4xNDkgNDMuNTIxLDQ1LjQ4MyA0MC43MDEsNTYuMTA1IDM4LjYzMyw1Ni4yNDcgNDEuMzU5LDYzLjUzMyA0Ni4xNTMsNTYuNDM2IDQ0LjE3OCw1Ni40ODIgNDkuMDY4LDM4LjUyNiAiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6I2NjY2NjYztmaWxsLW9wYWNpdHk6MSIgLz4KICAgICAgPC9nPgogICAgPC9nPgogIDwvZz4KPC9zdmc+Cg==" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option name="parameters"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="5" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="&quot;rot_z&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0" frame_rate="10" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" id="{50322660-f60d-4909-9677-ec61bbb161ba}" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="255,0,0,255,rgb:1,0,0,1" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <labeling type="rule-based">
    <rules key="{66558f3b-90c9-481b-83dd-e2a876845a84}">
      <rule key="{60602fb3-31e0-4ec3-8e24-c03b79ccfd14}" description="étiquettes armoire à remplacer" filter="&quot;etat&quot; = 'A remplacer'">
        <settings calloutType="simple">
          <text-style fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" fontSizeUnit="MM" namedStyle="Bold" multilineHeightUnit="Percentage" fontItalic="0" isExpression="1" fontWeight="75" blendMode="0" useSubstitutions="0" forcedItalic="0" legendString="Aa" fontLetterSpacing="0" multilineHeight="1" forcedBold="0" textOpacity="1" fontStrikeout="0" textColor="255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1" capitalization="0" previewBkgrdColor="0,0,0,255,rgb:0,0,0,1" fontSize="1.5" fontFamily="Arial" textOrientation="horizontal" allowHtml="0" fontKerning="1" fieldName="'armoire à remplacer : '  || id_armoire" fontUnderline="0" fontWordSpacing="0">
            <families/>
            <text-buffer bufferSize="0.20000000000000001" bufferSizeUnits="MapUnit" bufferOpacity="1" bufferDraw="1" bufferJoinStyle="128" bufferColor="255,255,255,255,rgb:1,1,1,1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferNoFill="0" bufferBlendMode="0"/>
            <text-mask maskType="0" maskEnabled="0" maskJoinStyle="128" maskSizeUnits="MM" maskedSymbolLayers="" maskSize="0" maskOpacity="1" maskSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <background shapeJoinStyle="64" shapeRadiiY="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOpacity="1" shapeSizeX="0.14999999999999999" shapeRadiiUnit="MM" shapeType="0" shapeSizeUnit="MapUnit" shapeRotationType="1" shapeOffsetX="0" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="MM" shapeOffsetY="0" shapeBorderColor="255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1" shapeDraw="1" shapeBorderWidth="0.14999999999999999" shapeRadiiX="0" shapeSizeY="0.14999999999999999" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeSVGFile="">
              <symbol clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0" frame_rate="10" type="marker">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleMarker" pass="0" enabled="1" id="" locked="0">
                  <Option type="Map">
                    <Option value="0" name="angle" type="QString"/>
                    <Option value="square" name="cap_style" type="QString"/>
                    <Option value="152,125,183,255,rgb:0.59607843137254901,0.49019607843137253,0.71764705882352942,1" name="color" type="QString"/>
                    <Option value="1" name="horizontal_anchor_point" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="circle" name="name" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="diameter" name="scale_method" type="QString"/>
                    <Option value="2" name="size" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                    <Option value="MM" name="size_unit" type="QString"/>
                    <Option value="1" name="vertical_anchor_point" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0" frame_rate="10" type="fill">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleFill" pass="0" enabled="1" id="" locked="0">
                  <Option type="Map">
                    <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
                    <Option value="255,255,255,255,rgb:1,1,1,1" name="color" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0.15" name="outline_width" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="solid" name="style" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowOpacity="0.69999999999999996" shadowOffsetGlobal="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowRadiusUnit="MM" shadowRadiusAlphaOnly="0" shadowDraw="0" shadowUnder="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetUnit="MM" shadowScale="100" shadowBlendMode="6" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetAngle="135" shadowOffsetDist="1"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format placeDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" reverseDirectionSymbol="0" plussign="0" decimals="3" leftDirectionSymbol="&lt;" autoWrapLength="0" rightDirectionSymbol=">" wrapChar="" addDirectionSymbol="0" multilineAlign="2" formatNumbers="0"/>
          <placement overrunDistance="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" centroidWhole="0" placement="0" layerType="PointGeometry" rotationUnit="AngleDegrees" repeatDistance="0" maxCurvedCharAngleOut="-25" offsetUnits="MapUnit" offsetType="1" allowDegraded="0" dist="5" preserveRotation="1" lineAnchorTextPoint="CenterOfText" quadOffset="4" polygonPlacementFlags="2" lineAnchorClipping="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" distUnits="MapUnit" fitInPolygonOnly="0" rotationAngle="0" yOffset="0" placementFlags="10" lineAnchorPercent="0.5" centroidInside="0" geometryGeneratorEnabled="0" maxCurvedCharAngleIn="25" overrunDistanceUnit="MM" repeatDistanceUnits="MM" priority="5" lineAnchorType="0" overlapHandling="PreventOverlap" geometryGenerator="" xOffset="0"/>
          <rendering fontMaxPixelSize="10000" maxNumLabels="2000" drawLabels="1" unplacedVisibility="0" mergeLines="0" scaleMin="1" fontMinPixelSize="1" scaleVisibility="1" labelPerPart="0" obstacle="1" fontLimitPixelSize="0" obstacleType="0" minFeatureSize="0" obstacleFactor="2" upsidedownLabels="0" scaleMax="5000" limitNumLabels="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="LabelRotation" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="rot_z" name="field" type="QString"/>
                  <Option value="2" name="type" type="int"/>
                </Option>
                <Option name="ObstacleFactor" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
                <Option name="OffsetQuad" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
              <Option value="0" name="blendMode" type="int"/>
              <Option name="ddProperties" type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
              <Option value="false" name="drawToAllParts" type="bool"/>
              <Option value="1" name="enabled" type="QString"/>
              <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
              <Option value="&lt;symbol clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;line&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;ArrowLine&quot; pass=&quot;0&quot; enabled=&quot;1&quot; id=&quot;{4be2328f-9a42-491c-b91f-b6fd03496189}&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;0.03&quot; name=&quot;arrow_start_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;arrow_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;arrow_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.15&quot; name=&quot;head_length&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;head_thickness&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;head_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_curved&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_repeated&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;ring_filter&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;fill&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;SimpleFill&quot; pass=&quot;0&quot; enabled=&quot;1&quot; id=&quot;{32219fb5-d543-4c32-8e92-cfe1e1370e17}&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,201,78,255,rgb:1,0.78823529411764703,0.30588235294117649,1&quot; name=&quot;color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;bevel&quot; name=&quot;joinstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0,0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,255,255,255,rgb:1,1,1,1&quot; name=&quot;outline_color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;outline_style&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.009&quot; name=&quot;outline_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;style&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol" type="QString"/>
              <Option value="0" name="minLength" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale" type="QString"/>
              <Option value="MM" name="minLengthUnit" type="QString"/>
              <Option value="0" name="offsetFromAnchor" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
              <Option value="0" name="offsetFromLabel" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromLabelUnit" type="QString"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule key="{fd316f36-f361-439d-85c1-c43881ab5eea}" description="étiquettes armmoire à poser" filter="&quot;etat&quot; = 'A poser'">
        <settings calloutType="simple">
          <text-style fontSizeMapUnitScale="3x:1000000,200,0,0,0,0" fontSizeUnit="MM" namedStyle="Bold" multilineHeightUnit="Percentage" fontItalic="0" isExpression="1" fontWeight="75" blendMode="0" useSubstitutions="0" forcedItalic="0" legendString="Aa" fontLetterSpacing="0" multilineHeight="1" forcedBold="0" textOpacity="1" fontStrikeout="0" textColor="255,0,252,255,rgb:1,0,0.9882352941176471,1" capitalization="0" previewBkgrdColor="0,0,0,255,rgb:0,0,0,1" fontSize="1.5" fontFamily="Arial" textOrientation="horizontal" allowHtml="0" fontKerning="1" fieldName="'armoire à poser : '  ||  &quot;id_armoire&quot; " fontUnderline="0" fontWordSpacing="0">
            <families/>
            <text-buffer bufferSize="0.20000000000000001" bufferSizeUnits="MapUnit" bufferOpacity="1" bufferDraw="1" bufferJoinStyle="128" bufferColor="255,255,255,255,rgb:1,1,1,1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferNoFill="0" bufferBlendMode="0"/>
            <text-mask maskType="0" maskEnabled="0" maskJoinStyle="128" maskSizeUnits="MM" maskedSymbolLayers="" maskSize="0" maskOpacity="1" maskSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <background shapeJoinStyle="64" shapeRadiiY="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidthUnit="MM" shapeRotation="0" shapeOpacity="1" shapeSizeX="0.14999999999999999" shapeRadiiUnit="MM" shapeType="0" shapeSizeUnit="MapUnit" shapeRotationType="1" shapeOffsetX="0" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="MM" shapeOffsetY="0" shapeBorderColor="255,0,252,255,rgb:1,0,0.9882352941176471,1" shapeDraw="1" shapeBorderWidth="0.14999999999999999" shapeRadiiX="0" shapeSizeY="0.14999999999999999" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeSVGFile="">
              <symbol clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0" frame_rate="10" type="marker">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleMarker" pass="0" enabled="1" id="" locked="0">
                  <Option type="Map">
                    <Option value="0" name="angle" type="QString"/>
                    <Option value="square" name="cap_style" type="QString"/>
                    <Option value="152,125,183,255,rgb:0.59607843137254901,0.49019607843137253,0.71764705882352942,1" name="color" type="QString"/>
                    <Option value="1" name="horizontal_anchor_point" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="circle" name="name" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="diameter" name="scale_method" type="QString"/>
                    <Option value="2" name="size" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                    <Option value="MM" name="size_unit" type="QString"/>
                    <Option value="1" name="vertical_anchor_point" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0" frame_rate="10" type="fill">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleFill" pass="0" enabled="1" id="" locked="0">
                  <Option type="Map">
                    <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
                    <Option value="255,255,255,255,rgb:1,1,1,1" name="color" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="255,0,252,255,rgb:1,0,0.9882352941176471,1" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0.15" name="outline_width" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="solid" name="style" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowOpacity="0.69999999999999996" shadowOffsetGlobal="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowRadiusUnit="MM" shadowRadiusAlphaOnly="0" shadowDraw="0" shadowUnder="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetUnit="MM" shadowScale="100" shadowBlendMode="6" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetAngle="135" shadowOffsetDist="1"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format placeDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" reverseDirectionSymbol="0" plussign="0" decimals="3" leftDirectionSymbol="&lt;" autoWrapLength="0" rightDirectionSymbol=">" wrapChar="" addDirectionSymbol="0" multilineAlign="2" formatNumbers="0"/>
          <placement overrunDistance="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" centroidWhole="0" placement="0" layerType="PointGeometry" rotationUnit="AngleDegrees" repeatDistance="0" maxCurvedCharAngleOut="-25" offsetUnits="MapUnit" offsetType="1" allowDegraded="0" dist="5" preserveRotation="1" lineAnchorTextPoint="CenterOfText" quadOffset="4" polygonPlacementFlags="2" lineAnchorClipping="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" distUnits="MapUnit" fitInPolygonOnly="0" rotationAngle="0" yOffset="0" placementFlags="10" lineAnchorPercent="0.5" centroidInside="0" geometryGeneratorEnabled="0" maxCurvedCharAngleIn="25" overrunDistanceUnit="MM" repeatDistanceUnits="MM" priority="5" lineAnchorType="0" overlapHandling="PreventOverlap" geometryGenerator="" xOffset="0"/>
          <rendering fontMaxPixelSize="10000" maxNumLabels="2000" drawLabels="1" unplacedVisibility="0" mergeLines="0" scaleMin="1" fontMinPixelSize="1" scaleVisibility="1" labelPerPart="0" obstacle="1" fontLimitPixelSize="0" obstacleType="0" minFeatureSize="0" obstacleFactor="2" upsidedownLabels="0" scaleMax="5000" limitNumLabels="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="LabelRotation" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="rot_z" name="field" type="QString"/>
                  <Option value="2" name="type" type="int"/>
                </Option>
                <Option name="ObstacleFactor" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
                <Option name="OffsetQuad" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="&quot;cellule&quot;" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
              <Option value="0" name="blendMode" type="int"/>
              <Option name="ddProperties" type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
              <Option value="false" name="drawToAllParts" type="bool"/>
              <Option value="1" name="enabled" type="QString"/>
              <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
              <Option value="&lt;symbol clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;line&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;ArrowLine&quot; pass=&quot;0&quot; enabled=&quot;1&quot; id=&quot;{13eebbcb-a50c-438c-8138-b3d8f71f1ca3}&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;0.03&quot; name=&quot;arrow_start_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_start_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_start_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;arrow_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;arrow_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;arrow_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;arrow_width_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.15&quot; name=&quot;head_length&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_length_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_length_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;head_thickness&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;head_thickness_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;head_thickness_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;head_type&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_curved&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;1&quot; name=&quot;is_repeated&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;ring_filter&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;symbol clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;@symbol@0&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;fill&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;SimpleFill&quot; pass=&quot;0&quot; enabled=&quot;1&quot; id=&quot;{cb07b7c7-0ca1-46eb-ad3c-d74194c99726}&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,0,252,255,rgb:1,0,0.9882352941176471,1&quot; name=&quot;color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;bevel&quot; name=&quot;joinstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0,0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;255,255,255,255,rgb:1,1,1,1&quot; name=&quot;outline_color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;outline_style&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.009&quot; name=&quot;outline_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MapUnit&quot; name=&quot;outline_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;style&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>&lt;/layer>&lt;/symbol>" name="lineSymbol" type="QString"/>
              <Option value="0" name="minLength" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale" type="QString"/>
              <Option value="MM" name="minLengthUnit" type="QString"/>
              <Option value="0" name="offsetFromAnchor" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
              <Option value="0" name="offsetFromLabel" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromLabelUnit" type="QString"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule key="{b7db8e7f-4ed5-4486-93cc-3fbef595f6cd}" description="texte armoire" filter="&quot;etat&quot; = 'Actif'">
        <settings calloutType="balloon">
          <text-style fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontSizeUnit="MM" namedStyle="Bold Italic" multilineHeightUnit="Percentage" fontItalic="1" isExpression="1" fontWeight="75" blendMode="0" useSubstitutions="0" forcedItalic="0" legendString="Aa" fontLetterSpacing="0" multilineHeight="1" forcedBold="0" textOpacity="1" fontStrikeout="0" textColor="21,0,255,255,rgb:0.08235294117647059,0,1,1" capitalization="0" previewBkgrdColor="255,255,255,255,rgb:1,1,1,1" fontSize="1.5" fontFamily="Arial" textOrientation="horizontal" allowHtml="0" fontKerning="1" fieldName="'armoire : ' || &quot;id_armoire&quot;" fontUnderline="0" fontWordSpacing="0">
            <families/>
            <text-buffer bufferSize="0" bufferSizeUnits="MM" bufferOpacity="1" bufferDraw="0" bufferJoinStyle="128" bufferColor="255,255,255,255,rgb:1,1,1,1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferNoFill="0" bufferBlendMode="0"/>
            <text-mask maskType="0" maskEnabled="0" maskJoinStyle="128" maskSizeUnits="MM" maskedSymbolLayers="" maskSize="0" maskOpacity="1" maskSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <background shapeJoinStyle="64" shapeRadiiY="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidthUnit="Point" shapeRotation="0" shapeOpacity="1" shapeSizeX="0" shapeRadiiUnit="Point" shapeType="0" shapeSizeUnit="Point" shapeRotationType="0" shapeOffsetX="0" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="Point" shapeOffsetY="0" shapeBorderColor="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" shapeDraw="0" shapeBorderWidth="0" shapeRadiiX="0" shapeSizeY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeSVGFile="">
              <symbol clip_to_extent="1" is_animated="0" alpha="1" name="markerSymbol" force_rhr="0" frame_rate="10" type="marker">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleMarker" pass="0" enabled="1" id="" locked="0">
                  <Option type="Map">
                    <Option value="0" name="angle" type="QString"/>
                    <Option value="square" name="cap_style" type="QString"/>
                    <Option value="152,125,183,255,rgb:0.59607843137254901,0.49019607843137253,0.71764705882352942,1" name="color" type="QString"/>
                    <Option value="1" name="horizontal_anchor_point" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="circle" name="name" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color" type="QString"/>
                    <Option value="solid" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                    <Option value="MM" name="outline_width_unit" type="QString"/>
                    <Option value="diameter" name="scale_method" type="QString"/>
                    <Option value="2" name="size" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                    <Option value="MM" name="size_unit" type="QString"/>
                    <Option value="1" name="vertical_anchor_point" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol clip_to_extent="1" is_animated="0" alpha="1" name="fillSymbol" force_rhr="0" frame_rate="10" type="fill">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" name="name" type="QString"/>
                    <Option name="properties"/>
                    <Option value="collection" name="type" type="QString"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleFill" pass="0" enabled="1" id="" locked="0">
                  <Option type="Map">
                    <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
                    <Option value="255,255,255,255,rgb:1,1,1,1" name="color" type="QString"/>
                    <Option value="bevel" name="joinstyle" type="QString"/>
                    <Option value="0,0" name="offset" type="QString"/>
                    <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                    <Option value="MM" name="offset_unit" type="QString"/>
                    <Option value="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" name="outline_color" type="QString"/>
                    <Option value="no" name="outline_style" type="QString"/>
                    <Option value="0" name="outline_width" type="QString"/>
                    <Option value="Point" name="outline_width_unit" type="QString"/>
                    <Option value="solid" name="style" type="QString"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" name="name" type="QString"/>
                      <Option name="properties"/>
                      <Option value="collection" name="type" type="QString"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowOpacity="1" shadowOffsetGlobal="1" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowRadiusUnit="Point" shadowRadiusAlphaOnly="0" shadowDraw="0" shadowUnder="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetUnit="Point" shadowScale="100" shadowBlendMode="6" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetAngle="135" shadowOffsetDist="1"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format placeDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" reverseDirectionSymbol="0" plussign="0" decimals="3" leftDirectionSymbol="&lt;" autoWrapLength="0" rightDirectionSymbol=">" wrapChar="" addDirectionSymbol="0" multilineAlign="3" formatNumbers="0"/>
          <placement overrunDistance="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" centroidWhole="0" placement="6" layerType="PointGeometry" rotationUnit="AngleDegrees" repeatDistance="0" maxCurvedCharAngleOut="-25" offsetUnits="MM" offsetType="1" allowDegraded="0" dist="12" preserveRotation="1" lineAnchorTextPoint="CenterOfText" quadOffset="4" polygonPlacementFlags="2" lineAnchorClipping="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" distUnits="MM" fitInPolygonOnly="0" rotationAngle="0" yOffset="0" placementFlags="10" lineAnchorPercent="0.5" centroidInside="0" geometryGeneratorEnabled="0" maxCurvedCharAngleIn="25" overrunDistanceUnit="MM" repeatDistanceUnits="MM" priority="5" lineAnchorType="0" overlapHandling="PreventOverlap" geometryGenerator="" xOffset="0"/>
          <rendering fontMaxPixelSize="10000" maxNumLabels="2000" drawLabels="1" unplacedVisibility="0" mergeLines="0" scaleMin="0" fontMinPixelSize="3" scaleVisibility="0" labelPerPart="0" obstacle="1" fontLimitPixelSize="0" obstacleType="1" minFeatureSize="0" obstacleFactor="1" upsidedownLabels="0" scaleMax="0" limitNumLabels="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="PositionX" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option value="" name="val" type="QString"/>
                </Option>
                <Option name="PositionY" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option value="" name="val" type="QString"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </dd_properties>
          <callout type="balloon">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
              <Option value="0" name="blendMode" type="int"/>
              <Option value="0.5" name="cornerRadius" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="cornerRadiusMapUnitScale" type="QString"/>
              <Option value="MM" name="cornerRadiusUnit" type="QString"/>
              <Option name="ddProperties" type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties" type="Map">
                  <Option name="OriginX" type="Map">
                    <Option value="false" name="active" type="bool"/>
                    <Option value="1" name="type" type="int"/>
                    <Option value="" name="val" type="QString"/>
                  </Option>
                  <Option name="OriginY" type="Map">
                    <Option value="false" name="active" type="bool"/>
                    <Option value="1" name="type" type="int"/>
                    <Option value="" name="val" type="QString"/>
                  </Option>
                </Option>
                <Option value="collection" name="type" type="QString"/>
              </Option>
              <Option value="1" name="enabled" type="QString"/>
              <Option value="&lt;symbol clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; type=&quot;fill&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;SimpleFill&quot; pass=&quot;0&quot; enabled=&quot;1&quot; id=&quot;{f9bfc4b8-2b4e-4736-a7d1-9bb59938cfe7}&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;border_width_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;204,204,204,255,rgb:0.80000000000000004,0.80000000000000004,0.80000000000000004,1&quot; name=&quot;color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;bevel&quot; name=&quot;joinstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0,0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;Point&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;21,0,255,255,rgb:0.08235294117647059,0,1,1&quot; name=&quot;outline_color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;outline_style&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.06&quot; name=&quot;outline_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;outline_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;style&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="fillSymbol" type="QString"/>
              <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
              <Option value="0.5,0.5,0.5,0.5" name="margins" type="QString"/>
              <Option value="MM" name="marginsUnit" type="QString"/>
              <Option value="0" name="offsetFromAnchor" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
              <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
              <Option value="1" name="wedgeWidth" type="double"/>
              <Option value="3x:0,0,0,0,0,0" name="wedgeWidthMapUnitScale" type="QString"/>
              <Option value="MM" name="wedgeWidthUnit" type="QString"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option value="remove" name="QFieldSync/action" type="QString"/>
      <Option value="offline" name="QFieldSync/cloud_action" type="QString"/>
      <Option value="{&quot;photo&quot;: &quot;'DCIM/armoires_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;, &quot;schema_elec&quot;: &quot;'DCIM/armoires_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;}" name="QFieldSync/photo_naming" type="QString"/>
      <Option name="dualview/previewExpressions" type="List">
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id_armoire&quot;, '&lt;NULL>' )" type="QString"/>
      </Option>
      <Option value="0" name="embeddedWidgets/count" type="QString"/>
      <Option value="false" name="geopdf/includeFeatures" type="QString"/>
      <Option value="true" name="qgis2web/Interactive" type="QString"/>
      <Option value="true" name="qgis2web/Visible" type="QString"/>
      <Option value="no label" name="qgis2web/popup/acteur" type="QString"/>
      <Option value="no label" name="qgis2web/popup/cellule" type="QString"/>
      <Option value="no label" name="qgis2web/popup/created_at" type="QString"/>
      <Option value="no label" name="qgis2web/popup/date_action" type="QString"/>
      <Option value="no label" name="qgis2web/popup/date_depose" type="QString"/>
      <Option value="no label" name="qgis2web/popup/date_pose" type="QString"/>
      <Option value="no label" name="qgis2web/popup/desc_action" type="QString"/>
      <Option value="no label" name="qgis2web/popup/ecp_intervention_id" type="QString"/>
      <Option value="no label" name="qgis2web/popup/enveloppe_pose" type="QString"/>
      <Option value="no label" name="qgis2web/popup/enveloppe_poste_amont" type="QString"/>
      <Option value="no label" name="qgis2web/popup/etat" type="QString"/>
      <Option value="no label" name="qgis2web/popup/fermeture" type="QString"/>
      <Option value="no label" name="qgis2web/popup/id_armoire" type="QString"/>
      <Option value="no label" name="qgis2web/popup/insee" type="QString"/>
      <Option value="no label" name="qgis2web/popup/intervention" type="QString"/>
      <Option value="no label" name="qgis2web/popup/mapid" type="QString"/>
      <Option value="no label" name="qgis2web/popup/mslink" type="QString"/>
      <Option value="no label" name="qgis2web/popup/nbre_depart" type="QString"/>
      <Option value="no label" name="qgis2web/popup/nom_voie" type="QString"/>
      <Option value="no label" name="qgis2web/popup/numero" type="QString"/>
      <Option value="no label" name="qgis2web/popup/numero_voie" type="QString"/>
      <Option value="no label" name="qgis2web/popup/photo" type="QString"/>
      <Option value="no label" name="qgis2web/popup/pkid" type="QString"/>
      <Option value="no label" name="qgis2web/popup/remarque" type="QString"/>
      <Option value="no label" name="qgis2web/popup/rot_z" type="QString"/>
      <Option value="no label" name="qgis2web/popup/schema_elec" type="QString"/>
      <Option value="no label" name="qgis2web/popup/type_voie" type="QString"/>
      <Option value="no label" name="qgis2web/popup/uid4" type="QString"/>
      <Option value="no label" name="qgis2web/popup/updated_at" type="QString"/>
      <Option value="no label" name="qgis2web/popup/vetuste" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory opacity="1" enabled="0" diagramOrientation="Up" penWidth="0" width="15" backgroundColor="#ffffff" sizeType="MM" minScaleDenominator="1" penColor="#000000" barWidth="5" showAxis="0" maxScaleDenominator="1e+08" backgroundAlpha="255" scaleDependency="Area" height="15" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" penAlpha="255" spacingUnitScale="3x:0,0,0,0,0,0" lineSizeType="MM" rotationOffset="270" spacingUnit="MM" direction="1" lineSizeScale="3x:0,0,0,0,0,0" spacing="0" minimumSize="0" labelPlacementMethod="XHeight">
      <fontProperties italic="0" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
      <attribute color="#000000" field="" label="" colorOpacity="1"/>
      <axisSymbol>
        <symbol clip_to_extent="1" is_animated="0" alpha="1" name="" force_rhr="0" frame_rate="10" type="line">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" enabled="1" id="{d3a55289-c6fd-42c4-891a-8ee59739774b}" locked="0">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" priority="0" linePlacementFlags="18" placement="0" dist="0" obstacle="0" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numero" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="insee" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_armoire" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="numero_voie" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="type_voie" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nom_voie" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="etat" configurationFlags="NoFlag">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Actif" name="Actif" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="A déposer" name="A déposer" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="vetuste" configurationFlags="NoFlag">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Bon" name="Bon" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Moyen" name="Moyen" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Vétuste" name="Vétuste" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fermeture" configurationFlags="NoFlag">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Clés" name="Clés" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Triangle" name="Triangle" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Plat" name="Plat" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enveloppe_poste_amont" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="enveloppe_pose" configurationFlags="NoFlag">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="Autre" name="Autre" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Encastrée" name="Encastrée" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Saillie" name="Saillie" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Sur poteau de distribution" name="Sur poteau de distribution" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="Sur façade" name="Sur façade" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nbre_depart" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cellule" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="date_pose" configurationFlags="NoFlag">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="dd/MM/yyyy" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date_depose" configurationFlags="NoFlag">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date_action" configurationFlags="NoFlag">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="desc_action" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="remarque" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="acteur" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="mslink" configurationFlags="NoFlag">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapid" configurationFlags="NoFlag">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ecp_intervention_id" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="photo" configurationFlags="NoFlag">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="P:/SIG/documents/photos" name="DefaultRoot" type="QString"/>
            <Option value="1" name="DocumentViewer" type="int"/>
            <Option value="0" name="DocumentViewerHeight" type="int"/>
            <Option value="0" name="DocumentViewerWidth" type="int"/>
            <Option value="false" name="FileWidget" type="bool"/>
            <Option value="false" name="FileWidgetButton" type="bool"/>
            <Option name="FileWidgetFilter" type="invalid"/>
            <Option name="PropertyCollection" type="Map">
              <Option name="name" type="invalid"/>
              <Option name="properties" type="invalid"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
            <Option value="1" name="RelativeStorage" type="int"/>
            <Option value="0" name="StorageMode" type="int"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="schema_elec" configurationFlags="NoFlag">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option value="P:/SIG/documents/schemas" name="DefaultRoot" type="QString"/>
            <Option value="0" name="DocumentViewer" type="int"/>
            <Option value="0" name="DocumentViewerHeight" type="int"/>
            <Option value="0" name="DocumentViewerWidth" type="int"/>
            <Option value="true" name="FileWidget" type="bool"/>
            <Option value="false" name="FileWidgetButton" type="bool"/>
            <Option name="FileWidgetFilter" type="invalid"/>
            <Option name="PropertyCollection" type="Map">
              <Option name="name" type="invalid"/>
              <Option name="properties" type="Map">
                <Option name="documentViewerContent" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option name="val" type="invalid"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
            <Option value="0" name="RelativeStorage" type="int"/>
            <Option value="1" name="StorageMode" type="int"/>
            <Option value="true" name="UseLink" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="created_at" configurationFlags="NoFlag">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd HH:mm:ss" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="updated_at" configurationFlags="NoFlag">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pkid" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="intervention" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="rot_z" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="uid4" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tri_id" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="fid" name="" index="0"/>
    <alias field="numero" name="" index="1"/>
    <alias field="insee" name="numéro insee commune" index="2"/>
    <alias field="id_armoire" name="identifiant" index="3"/>
    <alias field="numero_voie" name="Numéro rue" index="4"/>
    <alias field="type_voie" name="Type (rue, route, chemin.....)" index="5"/>
    <alias field="nom_voie" name="Nom de la rue" index="6"/>
    <alias field="etat" name="Etat" index="7"/>
    <alias field="vetuste" name="Vétusté" index="8"/>
    <alias field="fermeture" name="Fermeture" index="9"/>
    <alias field="enveloppe_poste_amont" name="Rattachement au poste de distribution publique" index="10"/>
    <alias field="enveloppe_pose" name="Type de pose de l'enveloppe" index="11"/>
    <alias field="nbre_depart" name="Nombre de départ" index="12"/>
    <alias field="cellule" name="" index="13"/>
    <alias field="date_pose" name="Date de pose" index="14"/>
    <alias field="date_depose" name="Date de dépose" index="15"/>
    <alias field="date_action" name="" index="16"/>
    <alias field="desc_action" name="" index="17"/>
    <alias field="remarque" name="Remarque" index="18"/>
    <alias field="acteur" name="chargé d'affaires" index="19"/>
    <alias field="mslink" name="" index="20"/>
    <alias field="mapid" name="" index="21"/>
    <alias field="ecp_intervention_id" name="identifiant interne intervention" index="22"/>
    <alias field="photo" name="intérieur/extérieur" index="23"/>
    <alias field="schema_elec" name="cliquez sur le lien" index="24"/>
    <alias field="created_at" name="" index="25"/>
    <alias field="updated_at" name="" index="26"/>
    <alias field="pkid" name="" index="27"/>
    <alias field="intervention" name="" index="28"/>
    <alias field="rot_z" name="" index="29"/>
    <alias field="uid4" name="" index="30"/>
    <alias field="tri_id" name="" index="31"/>
  </aliases>
  <splitPolicies>
    <policy field="fid" policy="Duplicate"/>
    <policy field="numero" policy="Duplicate"/>
    <policy field="insee" policy="Duplicate"/>
    <policy field="id_armoire" policy="Duplicate"/>
    <policy field="numero_voie" policy="Duplicate"/>
    <policy field="type_voie" policy="Duplicate"/>
    <policy field="nom_voie" policy="Duplicate"/>
    <policy field="etat" policy="Duplicate"/>
    <policy field="vetuste" policy="Duplicate"/>
    <policy field="fermeture" policy="Duplicate"/>
    <policy field="enveloppe_poste_amont" policy="Duplicate"/>
    <policy field="enveloppe_pose" policy="Duplicate"/>
    <policy field="nbre_depart" policy="Duplicate"/>
    <policy field="cellule" policy="Duplicate"/>
    <policy field="date_pose" policy="Duplicate"/>
    <policy field="date_depose" policy="Duplicate"/>
    <policy field="date_action" policy="Duplicate"/>
    <policy field="desc_action" policy="Duplicate"/>
    <policy field="remarque" policy="Duplicate"/>
    <policy field="acteur" policy="Duplicate"/>
    <policy field="mslink" policy="Duplicate"/>
    <policy field="mapid" policy="Duplicate"/>
    <policy field="ecp_intervention_id" policy="Duplicate"/>
    <policy field="photo" policy="Duplicate"/>
    <policy field="schema_elec" policy="Duplicate"/>
    <policy field="created_at" policy="Duplicate"/>
    <policy field="updated_at" policy="Duplicate"/>
    <policy field="pkid" policy="Duplicate"/>
    <policy field="intervention" policy="Duplicate"/>
    <policy field="rot_z" policy="Duplicate"/>
    <policy field="uid4" policy="Duplicate"/>
    <policy field="tri_id" policy="Duplicate"/>
  </splitPolicies>
  <defaults>
    <default field="fid" applyOnUpdate="0" expression=""/>
    <default field="numero" applyOnUpdate="0" expression=""/>
    <default field="insee" applyOnUpdate="1" expression=" @lumigis_insee "/>
    <default field="id_armoire" applyOnUpdate="0" expression=""/>
    <default field="numero_voie" applyOnUpdate="0" expression=""/>
    <default field="type_voie" applyOnUpdate="0" expression=""/>
    <default field="nom_voie" applyOnUpdate="0" expression=""/>
    <default field="etat" applyOnUpdate="0" expression=""/>
    <default field="vetuste" applyOnUpdate="0" expression=""/>
    <default field="fermeture" applyOnUpdate="0" expression=""/>
    <default field="enveloppe_poste_amont" applyOnUpdate="0" expression=""/>
    <default field="enveloppe_pose" applyOnUpdate="0" expression=""/>
    <default field="nbre_depart" applyOnUpdate="0" expression=""/>
    <default field="cellule" applyOnUpdate="0" expression=""/>
    <default field="date_pose" applyOnUpdate="0" expression=""/>
    <default field="date_depose" applyOnUpdate="0" expression=""/>
    <default field="date_action" applyOnUpdate="0" expression=""/>
    <default field="desc_action" applyOnUpdate="0" expression=""/>
    <default field="remarque" applyOnUpdate="0" expression=""/>
    <default field="acteur" applyOnUpdate="1" expression=" @lumigis_technicien "/>
    <default field="mslink" applyOnUpdate="0" expression=""/>
    <default field="mapid" applyOnUpdate="0" expression=""/>
    <default field="ecp_intervention_id" applyOnUpdate="1" expression=" &quot;ecp_intervention_id&quot; "/>
    <default field="photo" applyOnUpdate="0" expression=""/>
    <default field="schema_elec" applyOnUpdate="0" expression=""/>
    <default field="created_at" applyOnUpdate="0" expression=""/>
    <default field="updated_at" applyOnUpdate="0" expression=""/>
    <default field="pkid" applyOnUpdate="0" expression=""/>
    <default field="intervention" applyOnUpdate="1" expression=" @lumigis_intervention "/>
    <default field="rot_z" applyOnUpdate="0" expression=""/>
    <default field="uid4" applyOnUpdate="0" expression=""/>
    <default field="tri_id" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" field="fid" constraints="3" exp_strength="0" unique_strength="1"/>
    <constraint notnull_strength="1" field="numero" constraints="3" exp_strength="0" unique_strength="1"/>
    <constraint notnull_strength="0" field="insee" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="id_armoire" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="numero_voie" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="type_voie" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="nom_voie" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="1" field="etat" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="1" field="vetuste" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="1" field="fermeture" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="enveloppe_poste_amont" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="1" field="enveloppe_pose" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="nbre_depart" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="cellule" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="date_pose" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="date_depose" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="date_action" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="desc_action" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="remarque" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="acteur" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="mslink" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="mapid" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="ecp_intervention_id" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="photo" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="schema_elec" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="created_at" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="updated_at" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="pkid" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="intervention" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="rot_z" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="uid4" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint notnull_strength="0" field="tri_id" constraints="0" exp_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="numero" desc="" exp=""/>
    <constraint field="insee" desc="" exp=""/>
    <constraint field="id_armoire" desc="" exp=""/>
    <constraint field="numero_voie" desc="" exp=""/>
    <constraint field="type_voie" desc="" exp=""/>
    <constraint field="nom_voie" desc="" exp=""/>
    <constraint field="etat" desc="" exp=""/>
    <constraint field="vetuste" desc="" exp=""/>
    <constraint field="fermeture" desc="" exp=""/>
    <constraint field="enveloppe_poste_amont" desc="" exp=""/>
    <constraint field="enveloppe_pose" desc="" exp=""/>
    <constraint field="nbre_depart" desc="" exp=""/>
    <constraint field="cellule" desc="" exp=""/>
    <constraint field="date_pose" desc="" exp=""/>
    <constraint field="date_depose" desc="" exp=""/>
    <constraint field="date_action" desc="" exp=""/>
    <constraint field="desc_action" desc="" exp=""/>
    <constraint field="remarque" desc="" exp=""/>
    <constraint field="acteur" desc="" exp=""/>
    <constraint field="mslink" desc="" exp=""/>
    <constraint field="mapid" desc="" exp=""/>
    <constraint field="ecp_intervention_id" desc="" exp=""/>
    <constraint field="photo" desc="" exp=""/>
    <constraint field="schema_elec" desc="" exp=""/>
    <constraint field="created_at" desc="" exp=""/>
    <constraint field="updated_at" desc="" exp=""/>
    <constraint field="pkid" desc="" exp=""/>
    <constraint field="intervention" desc="" exp=""/>
    <constraint field="rot_z" desc="" exp=""/>
    <constraint field="uid4" desc="" exp=""/>
    <constraint field="tri_id" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields>
    <field typeName="string" subType="0" name="tri_id" comment="" precision="0" type="10" expression="case when substr(&quot;id_armoire&quot;,1,1)   = '*' then substr(&quot;id_armoire&quot;,3)+100000&#xd;&#xa;else substr(&quot;id_armoire&quot;,2)+10000&#xd;&#xa;end" length="0"/>
  </expressionfields>
  <attributeactions>
    <defaultAction value="{5c55e0f0-75c5-4988-87e5-e7f75979c44a}" key="Canvas"/>
    <actionsetting action="[%schema_elec%]" name="visualisation schéma" id="{82c346aa-d346-4561-968f-431458228657}" isEnabledOnlyWhenEditable="0" type="5" capture="0" notificationMessage="" shortTitle="" icon="">
      <actionScope id="Layer"/>
      <actionScope id="Canvas"/>
      <actionScope id="Feature"/>
    </actionsetting>
    <actionsetting action="[%photo%]" name="visualisaton photos extérieur/intérieur" id="{5c55e0f0-75c5-4988-87e5-e7f75979c44a}" isEnabledOnlyWhenEditable="0" type="5" capture="0" notificationMessage="" shortTitle="" icon="">
      <actionScope id="Layer"/>
      <actionScope id="Canvas"/>
      <actionScope id="Feature"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="buttonList" sortExpression="&quot;tri_id&quot;" sortOrder="0">
    <columns>
      <column hidden="0" name="intervention" type="field" width="-1"/>
      <column hidden="0" name="acteur" type="field" width="100"/>
      <column hidden="0" name="insee" type="field" width="-1"/>
      <column hidden="0" name="id_armoire" type="field" width="-1"/>
      <column hidden="0" name="numero_voie" type="field" width="-1"/>
      <column hidden="0" name="type_voie" type="field" width="-1"/>
      <column hidden="0" name="nom_voie" type="field" width="-1"/>
      <column hidden="0" name="etat" type="field" width="100"/>
      <column hidden="0" name="vetuste" type="field" width="100"/>
      <column hidden="0" name="fermeture" type="field" width="-1"/>
      <column hidden="0" name="enveloppe_poste_amont" type="field" width="252"/>
      <column hidden="0" name="enveloppe_pose" type="field" width="154"/>
      <column hidden="0" name="nbre_depart" type="field" width="-1"/>
      <column hidden="0" name="cellule" type="field" width="-1"/>
      <column hidden="0" name="date_pose" type="field" width="100"/>
      <column hidden="0" name="date_depose" type="field" width="100"/>
      <column hidden="1" name="mslink" type="field" width="-1"/>
      <column hidden="1" name="mapid" type="field" width="-1"/>
      <column hidden="1" name="ecp_intervention_id" type="field" width="143"/>
      <column hidden="0" name="photo" type="field" width="382"/>
      <column hidden="0" name="schema_elec" type="field" width="361"/>
      <column hidden="1" name="created_at" type="field" width="-1"/>
      <column hidden="1" name="updated_at" type="field" width="-1"/>
      <column hidden="0" name="remarque" type="field" width="-1"/>
      <column hidden="1" name="rot_z" type="field" width="-1"/>
      <column hidden="0" type="actions" width="-1"/>
      <column hidden="0" name="date_action" type="field" width="-1"/>
      <column hidden="0" name="desc_action" type="field" width="-1"/>
      <column hidden="0" name="uid4" type="field" width="-1"/>
      <column hidden="0" name="numero" type="field" width="-1"/>
      <column hidden="0" name="fid" type="field" width="-1"/>
      <column hidden="0" name="tri_id" type="field" width="-1"/>
      <column hidden="0" name="pkid" type="field" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/technicien_08/Desktop/2023-A-002-88148-ep/SIG/gestion</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>AGENTS/Jean-Claude/transfert_shape/201607014pairetgrandrupt/QGIS</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="">
      <labelFont italic="0" description="MS Shell Dlg 2,8.3,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
    </labelStyle>
    <attributeEditorContainer showLabel="1" collapsed="0" horizontalStretch="0" groupBox="0" columnCount="1" visibilityExpressionEnabled="0" visibilityExpression="" collapsedExpressionEnabled="0" verticalStretch="0" name="Fiche descriptive" type="Tab" collapsedExpression="" backgroundColor="#a6cee3">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
        <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
      </labelStyle>
      <attributeEditorContainer showLabel="1" collapsed="0" horizontalStretch="0" groupBox="1" columnCount="1" visibilityExpressionEnabled="0" visibilityExpression="" collapsedExpressionEnabled="0" verticalStretch="0" name="Généralités" type="GroupBox" collapsedExpression="" backgroundColor="#a6cee3">
        <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
          <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
        </labelStyle>
        <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="intervention" index="28">
          <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
            <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="acteur" index="19">
          <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
            <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="insee" index="2">
          <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
            <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer showLabel="1" collapsed="0" horizontalStretch="0" groupBox="1" columnCount="1" visibilityExpressionEnabled="0" visibilityExpression="" collapsedExpressionEnabled="0" verticalStretch="0" name="Caractéristiques techniques" type="GroupBox" collapsedExpression="" backgroundColor="#a6cee3">
        <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
          <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
        </labelStyle>
        <attributeEditorContainer showLabel="1" collapsed="0" horizontalStretch="0" groupBox="1" columnCount="1" visibilityExpressionEnabled="0" visibilityExpression="" collapsedExpressionEnabled="0" verticalStretch="0" name="Situation" type="GroupBox" collapsedExpression="" backgroundColor="#a6cee3">
          <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
            <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
          </labelStyle>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="type_voie" index="5">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="nom_voie" index="6">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="numero_voie" index="4">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
        </attributeEditorContainer>
        <attributeEditorContainer showLabel="1" collapsed="0" horizontalStretch="0" groupBox="1" columnCount="1" visibilityExpressionEnabled="0" visibilityExpression="" collapsedExpressionEnabled="0" verticalStretch="0" name="identification" type="GroupBox" collapsedExpression="" backgroundColor="#a6cee3">
          <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
            <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
          </labelStyle>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="id_armoire" index="3">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="etat" index="7">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="vetuste" index="8">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="enveloppe_pose" index="11">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="fermeture" index="9">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
          <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="date_pose" index="14">
            <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
              <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
            </labelStyle>
          </attributeEditorField>
        </attributeEditorContainer>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" collapsed="0" horizontalStretch="0" groupBox="0" columnCount="1" visibilityExpressionEnabled="1" visibilityExpression=" &quot;remarque&quot; &lt;> ''" collapsedExpressionEnabled="0" verticalStretch="0" name="Remarques" type="Tab" collapsedExpression="">
      <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
        <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
      </labelStyle>
      <attributeEditorField showLabel="1" horizontalStretch="0" verticalStretch="0" name="remarque" index="18">
        <labelStyle overrideLabelColor="0" overrideLabelFont="0" labelColor="0,0,0,255,rgb:0,0,0,1">
          <labelFont italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" style="" strikethrough="0" bold="0"/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="acteur"/>
    <field editable="1" name="cellule"/>
    <field editable="1" name="created_at"/>
    <field editable="1" name="date_action"/>
    <field editable="1" name="date_depose"/>
    <field editable="1" name="date_maintenance"/>
    <field editable="1" name="date_pose"/>
    <field editable="0" name="departs_allumage"/>
    <field editable="0" name="departs_calibrage_protection"/>
    <field editable="0" name="departs_designation"/>
    <field editable="0" name="departs_heure_allumage"/>
    <field editable="0" name="departs_heure_extinction"/>
    <field editable="0" name="departs_id"/>
    <field editable="0" name="departs_id_armoire"/>
    <field editable="0" name="departs_id_depart"/>
    <field editable="0" name="departs_insee"/>
    <field editable="0" name="departs_nature_depart"/>
    <field editable="0" name="departs_nature_protection"/>
    <field editable="0" name="departs_pilote"/>
    <field editable="0" name="departs_remarque"/>
    <field editable="0" name="departs_sensibilite_diff"/>
    <field editable="1" name="desc_action"/>
    <field editable="1" name="desc_maintenance"/>
    <field editable="1" name="ecp_intervention_id"/>
    <field editable="1" name="enveloppe_pose"/>
    <field editable="1" name="enveloppe_poste_amont"/>
    <field editable="1" name="etat"/>
    <field editable="1" name="fermeture"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_armoire"/>
    <field editable="1" name="insee"/>
    <field editable="1" name="intervention"/>
    <field editable="1" name="mapid"/>
    <field editable="1" name="mslink"/>
    <field editable="1" name="nbre_depart"/>
    <field editable="1" name="nom_voie"/>
    <field editable="1" name="numero"/>
    <field editable="1" name="numero_voie"/>
    <field editable="0" name="photo"/>
    <field editable="1" name="pkid"/>
    <field editable="1" name="remarque"/>
    <field editable="1" name="rot_z"/>
    <field editable="0" name="schema_elec"/>
    <field editable="0" name="tri_id"/>
    <field editable="1" name="type_voie"/>
    <field editable="1" name="uid4"/>
    <field editable="1" name="updated_at"/>
    <field editable="1" name="vetuste"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="1" name="acteur"/>
    <field labelOnTop="0" name="cellule"/>
    <field labelOnTop="0" name="created_at"/>
    <field labelOnTop="0" name="date_action"/>
    <field labelOnTop="1" name="date_depose"/>
    <field labelOnTop="0" name="date_maintenance"/>
    <field labelOnTop="1" name="date_pose"/>
    <field labelOnTop="0" name="departs_allumage"/>
    <field labelOnTop="0" name="departs_calibrage_protection"/>
    <field labelOnTop="0" name="departs_designation"/>
    <field labelOnTop="0" name="departs_heure_allumage"/>
    <field labelOnTop="0" name="departs_heure_extinction"/>
    <field labelOnTop="0" name="departs_id"/>
    <field labelOnTop="0" name="departs_id_armoire"/>
    <field labelOnTop="0" name="departs_id_depart"/>
    <field labelOnTop="0" name="departs_insee"/>
    <field labelOnTop="0" name="departs_nature_depart"/>
    <field labelOnTop="0" name="departs_nature_protection"/>
    <field labelOnTop="0" name="departs_pilote"/>
    <field labelOnTop="0" name="departs_remarque"/>
    <field labelOnTop="0" name="departs_sensibilite_diff"/>
    <field labelOnTop="0" name="desc_action"/>
    <field labelOnTop="0" name="desc_maintenance"/>
    <field labelOnTop="0" name="ecp_intervention_id"/>
    <field labelOnTop="1" name="enveloppe_pose"/>
    <field labelOnTop="1" name="enveloppe_poste_amont"/>
    <field labelOnTop="1" name="etat"/>
    <field labelOnTop="1" name="fermeture"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="1" name="id_armoire"/>
    <field labelOnTop="1" name="insee"/>
    <field labelOnTop="1" name="intervention"/>
    <field labelOnTop="0" name="mapid"/>
    <field labelOnTop="0" name="mslink"/>
    <field labelOnTop="1" name="nbre_depart"/>
    <field labelOnTop="1" name="nom_voie"/>
    <field labelOnTop="0" name="numero"/>
    <field labelOnTop="1" name="numero_voie"/>
    <field labelOnTop="0" name="photo"/>
    <field labelOnTop="0" name="pkid"/>
    <field labelOnTop="1" name="remarque"/>
    <field labelOnTop="0" name="rot_z"/>
    <field labelOnTop="0" name="schema_elec"/>
    <field labelOnTop="0" name="tri_id"/>
    <field labelOnTop="1" name="type_voie"/>
    <field labelOnTop="0" name="uid4"/>
    <field labelOnTop="0" name="updated_at"/>
    <field labelOnTop="1" name="vetuste"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="acteur"/>
    <field reuseLastValue="0" name="cellule"/>
    <field reuseLastValue="0" name="created_at"/>
    <field reuseLastValue="0" name="date_action"/>
    <field reuseLastValue="0" name="date_depose"/>
    <field reuseLastValue="0" name="date_pose"/>
    <field reuseLastValue="0" name="desc_action"/>
    <field reuseLastValue="0" name="ecp_intervention_id"/>
    <field reuseLastValue="0" name="enveloppe_pose"/>
    <field reuseLastValue="0" name="enveloppe_poste_amont"/>
    <field reuseLastValue="0" name="etat"/>
    <field reuseLastValue="0" name="fermeture"/>
    <field reuseLastValue="0" name="fid"/>
    <field reuseLastValue="0" name="id_armoire"/>
    <field reuseLastValue="0" name="insee"/>
    <field reuseLastValue="0" name="intervention"/>
    <field reuseLastValue="0" name="mapid"/>
    <field reuseLastValue="0" name="mslink"/>
    <field reuseLastValue="0" name="nbre_depart"/>
    <field reuseLastValue="0" name="nom_voie"/>
    <field reuseLastValue="0" name="numero"/>
    <field reuseLastValue="0" name="numero_voie"/>
    <field reuseLastValue="0" name="photo"/>
    <field reuseLastValue="0" name="pkid"/>
    <field reuseLastValue="0" name="remarque"/>
    <field reuseLastValue="0" name="rot_z"/>
    <field reuseLastValue="0" name="schema_elec"/>
    <field reuseLastValue="0" name="tri_id"/>
    <field reuseLastValue="0" name="type_voie"/>
    <field reuseLastValue="0" name="uid4"/>
    <field reuseLastValue="0" name="updated_at"/>
    <field reuseLastValue="0" name="vetuste"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="intervention">
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="dataDefinedAlias" type="Map">
            <Option value="false" name="active" type="bool"/>
            <Option value="1" name="type" type="int"/>
            <Option value="" name="val" type="QString"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </field>
    <field name="schema_elec">
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="dataDefinedAlias" type="Map">
            <Option value="false" name="active" type="bool"/>
            <Option value="1" name="type" type="int"/>
            <Option value="" name="val" type="QString"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>COALESCE( "id_armoire", '&lt;NULL>' )</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
