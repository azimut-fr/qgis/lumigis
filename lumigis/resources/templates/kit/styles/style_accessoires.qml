<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="1" simplifyDrawingHints="0" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" minScale="100000000" simplifyAlgorithm="0" simplifyDrawingTol="1" version="3.28.5-Firenze" maxScale="0" simplifyLocal="1" readOnly="0" symbologyReferenceScale="200" simplifyMaxScale="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationField="fid" endField="" startExpression="" enabled="0" endExpression="" mode="0" startField="created_at" fixedDuration="0" limitMode="0" durationUnit="min" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation extrusionEnabled="0" showMarkerSymbolInSurfacePlots="0" binding="Centroid" respectLayerSymbol="1" type="IndividualFeatures" zoffset="0" extrusion="0" symbology="Line" zscale="1" clamping="Terrain">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="" type="line" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="152,125,183,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.6"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="" type="fill" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="152,125,183,255"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="109,89,131,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="152,125,183,255"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="diamond"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="109,89,131,255"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="200" enableorderby="0" forceraster="0" type="RuleRenderer" symbollevels="0">
    <rules key="{ce1daedc-a400-47c4-bc45-ba441e3dbe51}">
      <rule key="{66b9a554-5c0a-4710-af4d-929079be3631}" label="regard" symbol="0" filter=" &quot;cellule&quot; = 'REGARD EP'"/>
      <rule key="{7cf75154-4d8d-4394-b2c5-4975ae8bce5c}" label="boite dérivation" symbol="1" filter=" &quot;cellule&quot; = 'BOITE DE DERIVATION'"/>
      <rule key="{d36ddefb-f357-49b8-8032-bcf35e6b7912}" label="boite jonction" symbol="2" filter=" &quot;cellule&quot; = 'BOITE DE JONCTION'"/>
      <rule key="{9fd4da15-2cf6-451e-a8f8-87a502e2a3cf}" label="boite incertaine" symbol="3" filter=" &quot;cellule&quot; =  'BOITE INCERTAINE'"/>
      <rule key="{5bcbfe43-a5b4-40d9-8b2a-74b55fe0c0f1}" label="boitier différentiel" symbol="4" filter=" &quot;cellule&quot; = 'BOITIER DIFFERENTIEL'"/>
      <rule key="{3e6d5c49-3bc9-434f-8d63-03c557327994}" label="séparation de réseau" symbol="5" filter=" &quot;cellule&quot; = 'SEPARATION DE RESEAU'"/>
      <rule key="{ab35ed71-d586-413e-95ca-90823a987c5e}" label="regard sur fourreau" symbol="6" filter=" &quot;cellule&quot; = 'REGARD SUR FOURREAU'"/>
    </rules>
    <symbols>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="0" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="color" type="QString" value="225,89,137,255"/>
            <Option name="fixedAspectRatio" type="QString" value="0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="name" type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6b3NiPSJodHRwOi8vd3d3Lm9wZW5zd2F0Y2hib29rLm9yZy91cmkvMjAwOS9vc2IiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHZlcnNpb249IjEuMSIKICAgeD0iMHB4IgogICB5PSIwcHgiCiAgIHZpZXdCb3g9IjAgMCA3OTM3LjAwODEgNzkzNy4wMDc5IgogICBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMTAgMTEwIgogICB4bWw6c3BhY2U9InByZXNlcnZlIgogICBpZD0ic3ZnMiIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9cmVnYXJkX2VwLnN2ZyIKICAgd2lkdGg9IjIxMGNtIgogICBoZWlnaHQ9IjIxMGNtIj48bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGEyNCI+PHJkZjpSREY+PGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz48ZGM6dGl0bGU+PC9kYzp0aXRsZT48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGRlZnMKICAgICBpZD0iZGVmczIyIj48bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg0NTkiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj48c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0NTciIC8+PC9saW5lYXJHcmFkaWVudD48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTc3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTczIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTYxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTU3IgogICAgICAgZWZmZWN0PSJzcGlybyIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTQ5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTQ1IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTQxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTM3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTMzIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTI5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz48bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDk1MjEiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj48c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDk1MjMiIC8+PC9saW5lYXJHcmFkaWVudD48bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDk1MTMiCiAgICAgICBvc2I6cGFpbnQ9ImdyYWRpZW50Ij48c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDk1MTUiIC8+PHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MDsiCiAgICAgICAgIG9mZnNldD0iMSIKICAgICAgICAgaWQ9InN0b3A5NTE3IiAvPjwvbGluZWFyR3JhZGllbnQ+PGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ5NjAwIgogICAgICAgb3NiOnBhaW50PSJzb2xpZCI+PHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NjAyIiAvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzIwIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDQiCiAgICAgaW5rc2NhcGU6Y3g9IjQ5MTkuODg2NiIKICAgICBpbmtzY2FwZTpjeT0iMTg4Mi44MTkxIgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJnNCIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIHVuaXRzPSJjbSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpsb2NrZ3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIj48aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ4MDcwIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjAsMCIKICAgICAgIG9yaWVudGF0aW9uPSIwLDc5MzcuMDA3OSIKICAgICAgIGlkPSJndWlkZTg0MCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iNzkzNy4wMDgxLDAiCiAgICAgICBvcmllbnRhdGlvbj0iLTc5MzcuMDA3OSwwIgogICAgICAgaWQ9Imd1aWRlODQyIgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSI3OTM3LjAwODEsNzkzNy4wMDgxIgogICAgICAgb3JpZW50YXRpb249IjAsLTc5MzcuMDA3OSIKICAgICAgIGlkPSJndWlkZTg0NCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCw3OTM3LjAwODEiCiAgICAgICBvcmllbnRhdGlvbj0iNzkzNy4wMDc5LDAiCiAgICAgICBpZD0iZ3VpZGU4NDYiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249Ijc5MzcuMDA4Miw3OTM3LjAwOCIKICAgICAgIG9yaWVudGF0aW9uPSItMC43MDcxMDY3OCwwLjcwNzEwNjc4IgogICAgICAgaWQ9Imd1aWRlODQ4IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLDc5MzcuMDA4IgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTg1MCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PC9zb2RpcG9kaTpuYW1lZHZpZXc+PGcKICAgICBpZD0iZzQiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCw3Nzk5LjUwOCkiPjxnCiAgICAgICBpZD0iZzgzOCIKICAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNy42MTkzNzQsNDEuNjA1NTM3KSI+PHBhdGgKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlLXdpZHRoOjAuOTk5NTMxMTU7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICAgICAgaWQ9InBhdGgxMCIKICAgICAgICAgZD0iTSA4Ni45NjU2MTIsMTguMzM0NDcxIEggMjIuNzc3MTExIGMgLTIuMzUwNzc4LDAgLTQuMjU3MTExLDEuOTE3ODkxIC00LjI1NzExMSw0LjI4MTkyIHYgNjQuNTgxNjk3IGMgMCwyLjM2MzAyNiAxLjkwNjMzMyw0LjI3ODkxMiA0LjI1NzExMSw0LjI3ODkxMiBoIDY0LjE4ODUwMSBjIDIuMzUyNzcxLDAgNC4yNTgxMDcsLTEuOTE0ODgzIDQuMjU4MTA3LC00LjI3ODkxMiBWIDIyLjYxNjM5MSBjIDkuOTdlLTQsLTIuMzY0MDI5IC0xLjkwNTMzNiwtNC4yODE5MiAtNC4yNTgxMDcsLTQuMjgxOTIgeiBtIC0zMi4wOTI3NTYsNjAuODM0MTQgYyAtMTMuMzIxNDA4LDAgLTI0LjExNTY1NiwtMTAuODYyNzAyIC0yNC4xMTU2NTYsLTI0LjI2MTg3MyAwLC0xMy4zOTkxNyAxMC43OTQyNDgsLTI0LjI2Mzg3OCAyNC4xMTU2NTYsLTI0LjI2Mzg3OCAxMy4zMTc0MjMsMCAyNC4xMTU2NTYsMTAuODY0NzA4IDI0LjExNTY1NiwyNC4yNjM4NzggMCwxMy4zOTkxNzEgLTEwLjc5ODIzMywyNC4yNjE4NzMgLTI0LjExNTY1NiwyNC4yNjE4NzMgeiIgLz48cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmOGMwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6I2ZmOGMwMDtzdHJva2Utd2lkdGg6MTEzLjMzMjY2NDQ5O3N0cm9rZS1taXRlcmxpbWl0OjA7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICAgICAgaWQ9InBhdGgxNCIKICAgICAgICAgZD0iTSA3MTYyLjk4ODQsLTExNi4yNTgwNyBIIDc5OS4wNTAwNCBjIC0yOTEuMDU2NjYsMCAtNTI3LjgwNzk4LC0yMzguMzUyNyAtNTI3LjgwNzk4LC01MzEuMTc0MDEgViAtNzA0OS45NTU5IGMgMCwtMjkyLjkwNDMgMjM2Ljc1MTMyLC01MzEuMDA4MiA1MjcuODA3OTgsLTUzMS4wMDgyIEggNzE2Mi45ODg0IGMgMjkxLjA1NjUsMCA1MjcuOTcyNywyMzguMTg2NyA1MjcuOTcyNyw1MzEuMDA4MiB2IDY0MDIuNDQwOTcgYyAwLjA4MywyOTIuOTA0MTYgLTIzNi44MzM4LDUzMS4yNTY4NiAtNTI3Ljk3MjcsNTMxLjI1Njg2IHogTSA3OTkuMDUwMDQsLTcyMjYuOTU4NyBjIC05Ny4xNTYxLDAgLTE3NS45MzYwMiw3OS4zNDA0IC0xNzUuOTM2MDIsMTc3LjAwMjggdiA2NDAyLjQ0MDk3IGMgMCw5Ny43NDUyNiA3OC43Nzk5MiwxNzcuMTY4NDkgMTc1LjkzNjAyLDE3Ny4xNjg0OSBIIDcxNjIuOTg4NCBjIDk3LjE1NjIsMCAxNzYuMTAwOCwtNzkuNDIzMjMgMTc2LjEwMDgsLTE3Ny4xNjg0OSBWIC03MDQ5Ljk1NTkgYyAwLC05Ny43NDU0IC03OC45NDQ2LC0xNzcuMDAyOCAtMTc2LjEwMDgsLTE3Ny4wMDI4IHoiIC8+PHJlY3QKICAgICAgICAgeT0iLTY5MjAuMzgxOCIKICAgICAgICAgeD0iOTU2LjcxODYzIgogICAgICAgICBoZWlnaHQ9IjYwOTUuNTQ0OSIKICAgICAgICAgd2lkdGg9IjYwNTguODA5NiIKICAgICAgICAgaWQ9InJlY3Q4MzUiCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiNmZjhjMDA7c3Ryb2tlLXdpZHRoOjExMy4zMzI2NjQ0OTtzdHJva2UtbWl0ZXJsaW1pdDowO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPjwvZz48L2c+PC9zdmc+"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="parameters"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="4"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option name="active" type="bool" value="true"/>
                  <Option name="field" type="QString" value="rot_z"/>
                  <Option name="type" type="int" value="2"/>
                </Option>
              </Option>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="1" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="color" type="QString" value="190,178,151,255"/>
            <Option name="fixedAspectRatio" type="QString" value="0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="name" type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTcwY20iCiAgIGhlaWdodD0iOTFjbSIKICAgdmlld0JveD0iMCAwIDE3MDAgOTEwIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmc4OTciCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMi4yICg3MzJhMDFkYTYzLCAyMDIyLTEyLTA5KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic2RlPWJvaXRlX2RlX2Rlcml2YXRpb24uc3ZnIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczg5MSI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg5MiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg5MCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg4NCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg4MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg3NiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg2NCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZmYxMzAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg2MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg1NiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg1NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg1MCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg0NCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3ODUiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2ZmMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0NzgzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50ODY0IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODk0IgogICAgICAgeDE9IjI0NzQuODgyOCIKICAgICAgIHkxPSItMjAzIgogICAgICAgeDI9IjI4OTQuMjgzNyIKICAgICAgIHkyPSItMjAzIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBpZD0iYmFzZSIKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMS4wIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp6b29tPSIwLjA2MzgwNTM0IgogICAgIGlua3NjYXBlOmN4PSIyNDEzLjU5MTEiCiAgICAgaW5rc2NhcGU6Y3k9IjI2MTcuMzM1OCIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ibGF5ZXIxIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIHVuaXRzPSJjbSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJmYWxzZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxOTIwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMjciCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5MTIiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ2xvYmFsPSJ0cnVlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDg2OCIKICAgICAgIG9yaWdpbng9IjAiCiAgICAgICBvcmlnaW55PSIwIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTg5NCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpbmtzY2FwZTpsYWJlbD0iQ2FscXVlIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTQwMykiPgogICAgPGcKICAgICAgIGlkPSJnMjY4IgogICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTkwLDY1My4wMzA0NiwtNzQ5Ljk2OTUyKSI+CiAgICAgIDx0ZXh0CiAgICAgICAgIHhtbDpzcGFjZT0icHJlc2VydmUiCiAgICAgICAgIHN0eWxlPSJmb250LXN0eWxlOml0YWxpYztmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zdHJldGNoOm5vcm1hbDtmb250LXNpemU6NzkuNzgwN3B4O2xpbmUtaGVpZ2h0OjEuMjU7Zm9udC1mYW1pbHk6QXJpYWw7LWlua3NjYXBlLWZvbnQtc3BlY2lmaWNhdGlvbjonQXJpYWwsIEJvbGQgSXRhbGljJztmb250LXZhcmlhbnQtbGlnYXR1cmVzOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bm9ybWFsO2ZvbnQtZmVhdHVyZS1zZXR0aW5nczpub3JtYWw7dGV4dC1hbGlnbjpzdGFydDtsZXR0ZXItc3BhY2luZzowcHg7d29yZC1zcGFjaW5nOjBweDt3cml0aW5nLW1vZGU6bHItdGI7dGV4dC1hbmNob3I6c3RhcnQ7ZmlsbDojMDIwMGY3O2ZpbGwtb3BhY2l0eTowLjk2NDcwNjtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MS45OTQ0OSIKICAgICAgICAgeD0iLTIwNi4wNjU0NCIKICAgICAgICAgeT0iLTY0My42NTI4MyIKICAgICAgICAgaWQ9InRleHQxNDQ3LTciCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNDQxNDEyODQsMC44NTI4ODA0MywtMC45MTc0Njg4MywwLjQ5Mjc1NTk0LDAsMCkiCiAgICAgICAgIGlua3NjYXBlOnRyYW5zZm9ybS1jZW50ZXIteD0iOTkuOTk5OTgxIgogICAgICAgICBpbmtzY2FwZTp0cmFuc2Zvcm0tY2VudGVyLXk9IjE1MC4wMDAwMSI+PHRzcGFuCiAgICAgICAgICAgc29kaXBvZGk6cm9sZT0ibGluZSIKICAgICAgICAgICBpZD0idHNwYW44MzYiCiAgICAgICAgICAgeD0iLTIwNi4wNjU0NCIKICAgICAgICAgICB5PSItNjQzLjY1MjgzIj5ib2l0ZSBkw6lyaXZhdGlvbjwvdHNwYW4+PC90ZXh0PgogICAgICA8ZwogICAgICAgICBpZD0iZzg3NSIKICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTY3MS4xMTM4MSw3MzQuMDc1OTMpIj4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg4NTgiCiAgICAgICAgICAgZD0iTSAxNTIxLjExMzgsLTIwODcuMDc1OSBWIC00ODcuMDc1OTQiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I2ZmMDAwMDtzdHJva2Utd2lkdGg6MzA7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgaWQ9InBhdGg4NjAiCiAgICAgICAgICAgZD0ibSAxMDk2LjExMzgsLTEyODcuMDc1OSBoIDg1MCIKICAgICAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojZmYwMDAwO3N0cm9rZS13aWR0aDozMDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDozO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDg2NCIKICAgICAgICAgICBkPSJtIDE5NDYuMTEzOCwtMTI4Ny4wNzU5IC00MjUsLTgwMCAtNDI1LDgwMCIKICAgICAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojZmYwMDAwO3N0cm9rZS13aWR0aDozMDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDozO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICBpZD0icGF0aDg2NiIKICAgICAgICAgICBkPSJtIDEwOTYuMTEzOCwtMTI4Ny4wNzU5IDQyNC45OTk5LDc5OS45OTk2OSA0MjQuOTk5OSwtNzk5Ljk5OTY5IHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I2ZmMDAwMDtzdHJva2Utd2lkdGg6MzA7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MztzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgPC9nPgogICAgPC9nPgogIDwvZz4KPC9zdmc+Cg=="/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="parameters"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3.5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option name="active" type="bool" value="true"/>
                  <Option name="field" type="QString" value="rot_z"/>
                  <Option name="type" type="int" value="2"/>
                </Option>
              </Option>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="2" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="color" type="QString" value="145,82,45,255"/>
            <Option name="fixedAspectRatio" type="QString" value="0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="name" type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTcwY20iCiAgIGhlaWdodD0iOTFjbSIKICAgdmlld0JveD0iMCAwIDE3MDAgOTEwIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmc4OTciCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMi4yICg3MzJhMDFkYTYzLCAyMDIyLTEyLTA5KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic2RlPWJvaXRlX2RlX2pvbmN0aW9uLnN2ZyIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczg5MSIgLz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9ImJhc2UiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMC4wIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6em9vbT0iMC4xMTMxMzcwOCIKICAgICBpbmtzY2FwZTpjeD0iMjQ0OC4zNTczIgogICAgIGlua3NjYXBlOmN5PSIyMzA2LjkzNiIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ibGF5ZXIxIgogICAgIHNob3dncmlkPSJmYWxzZSIKICAgICB1bml0cz0iY20iCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6b2JqZWN0LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtaW50ZXJzZWN0aW9uLXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWNlbnRlcj0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXRleHQtYmFzZWxpbmU9InRydWUiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxOTIwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMjciCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5MTIiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkZXNrY29sb3I9IiNkMWQxZDEiPgogICAgPHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iMCw5MDYuMDExMzgiCiAgICAgICBvcmllbnRhdGlvbj0iMCwtNjA0Ny4yNDQxIgogICAgICAgaWQ9Imd1aWRlMTQ5MyIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhODk0Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlua3NjYXBlOmxhYmVsPSJDYWxxdWUgMSIKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlkPSJsYXllcjEiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxNDAzKSI+CiAgICA8ZwogICAgICAgaWQ9ImcyOTMiCiAgICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtOTAsNjQ5LjIyMzk4LC03NDkuNzg3MzkpIj4KICAgICAgPHBhdGgKICAgICAgICAgaWQ9InBhdGg4NjciCiAgICAgICAgIGQ9Im0gMTI2OC43NDkxLC0xNzYuODQ1MzEgYyAwLDE1Mi4xNDQ4NyAtODAuNjExNywyOTIuNzExNjcgLTIxMS40NDY3LDM2OC43ODQxMSAtMTMwLjgzNTEsNzYuMDcyNDQgLTI5Mi4wMTQ4LDc2LjA3MjQ0IC00MjIuODQ5OSwwIEMgNTAzLjYxNzUsMTE1Ljg2NjM2IDQyMy4wMDU3LC0yNC43MDA0NCA0MjMuMDA1NywtMTc2Ljg0NTMxIgogICAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojZmYwMDAwO3N0cm9rZS13aWR0aDoyOS45NzY1cHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPGcKICAgICAgICAgaWQ9ImcyNzMiPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg4NjUiCiAgICAgICAgICAgZD0ibSA0MjcuMjg0MiwtOTI1LjE4NzI4IC00LjI3ODUsNzQ4LjM0MTk3IgogICAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNmZjAwMDA7c3Ryb2tlLXdpZHRoOjI5Ljk3NjVweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDg2OSIKICAgICAgICAgICBkPSJtIDQyNy4yODQyLC05MjUuMTg3MjggYyAwLC0xNTIuMTIzNjIgODAuNTg5OSwtMjkyLjY5MDQyIDIxMS40MjQ5LC0zNjguNzYyODIgMTMwLjgzNTEsLTc2LjA3MjUgMjkyLjAzNjcsLTc2LjA3MjUgNDIyLjg3MTgsMCAxMzAuODM1LDc2LjA3MjQgMjExLjQyNDgsMjE2LjYzOTIgMjExLjQyNDgsMzY4Ljc2MjgyIgogICAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNmZjAwMDA7c3Ryb2tlLXdpZHRoOjI5Ljk3NjVweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDg3MSIKICAgICAgICAgICBkPSJtIDEyNzMuMDA1NywtOTI1LjE4NzI4IC00LjI1NjYsNzQ4LjM0MTk3IgogICAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNmZjAwMDA7c3Ryb2tlLXdpZHRoOjI5Ljk3NjVweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDg3MyIKICAgICAgICAgICBkPSJtIDQ3MC40MjA1LC04MzMuOTE3MjggNTkuMjg1LDkuODAwMSAwLjEzMTcsMTAuNzk0OSAtMTkuODEyOSwtMy4zMDIgYyAxLjY0NTYsMS45MjYyIDIuODA4NSwzLjcyNTMgMy41MTA2LDUuNDE4NyAwLjcyNCwxLjY5MzMgMS4wOTcsMy41NTU5IDEuMTE5LDUuNTg3OSAwLjA0NCw0LjE5MSAtMS40MjYyLDcuNTk4OCAtNC40MzIxLDEwLjIwMjMgLTIuOTg0LDIuNjI0NiAtNy4zNzIzLDQuMDAwNCAtMTMuMTQyOCw0LjE2OTggLTMuOTA1NSwwLjEwNTggLTcuNzAxMywtMC4zODEgLTExLjQwOTQsLTEuNDgxNyAtMy42NjQxLC0xLjA3OTUgLTYuNjcwMSwtMi41ODIzIC05LjAxNzgsLTQuNTA4NCAtMi4zMDM4LC0xLjkwNSAtNC4wMzcyLC0zLjkzNyAtNS4yLC02LjExNzIgLTEuMTQxLC0yLjEzNzggLTEuNzExNCwtNC40MDI2IC0xLjc1NTMsLTYuODE1NiAtMC4wNjYsLTUuNjkzOCAyLjU2NzEsLTkuOTQ4MiA3LjkyMDcsLTEyLjc4NDUgbCAtNy4wODcsLTEuMTg1NCB6IG0gMTcuNjQwNywxNC4wMTIyIGMgLTIuODc0MywwLjA4NSAtNS4yLDAuODg5IC02Ljk3NzMsMi40NTUzIC0xLjc5OTIsMS41NjY0IC0yLjY3NjgsMy4zODY3IC0yLjYzMjksNS40Mzk4IDAuMDIyLDEuNzk5MiAwLjcyNCwzLjQ3MTMgMi4xNTAyLDUuMDU4OCAxLjQyNjIsMS42MDg3IDMuNjY0MiwyLjkyMSA2Ljc1NzksMy44OTQ3IDMuMDcxOCwxLjAxNiA2LjE2NTQsMS40ODE2IDkuMzAzLDEuMzk3IDMuMDI3OSwtMC4wODUgNS4zOTc2LC0wLjg4OSA3LjEwOSwtMi40MTMgMS43MzMzLC0xLjUwMjggMi41ODksLTMuMjgwOCAyLjU0NTEsLTUuMzU1MiAtMC4wMjIsLTIuNzA5MyAtMS40MDQyLC00Ljk5NTIgLTQuMTI0OSwtNi44NTc5IC0zLjcwOCwtMi41ODIzIC04LjQyNTQsLTMuNzY3NiAtMTQuMTMwMSwtMy42MTk1IHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAyMDBmNztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC4yNjkzODEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoODc1IgogICAgICAgICAgIGQ9Im0gNDg4LjgyOTEsLTc4NS42MTUyOCBjIDcuOTQyNywtMC4yMzI4IDE0LjI4MzcsMS42NzIxIDE5LjA0NSw1LjY5MzggNC43ODMxLDQuMDIxNiA3LjIxODYsOS40NDAyIDcuMzA2NCwxNi4yMTM1IDAuMDY2LDUuODYzMSAtMS42MjM3LDEwLjQ5ODYgLTUuMDY4NCwxMy44ODUyIC0zLjQyMjksMy40MDc5IC04LjA5NjMsNS4xODU4IC0xMy45NzY2LDUuMzU1MiAtNi45NTUzLDAuMTkwNSAtMTIuOTY3MiwtMS42OTM0IC0xOC4wNzk1LC01LjY3MjYgLTUuMTEyMiwtMy45Nzk0IC03LjcwMTMsLTkuNDQwMyAtNy43ODkxLC0xNi4zNjE4IC0wLjA2NiwtMy43NDY0IDAuNzAyMSwtNy4wOTA4IDIuMjgxOSwtMTAuMDMyOSAxLjU3OTgsLTIuOTQyMSAzLjgzOTcsLTUuMTY0NiA2Ljc3OTgsLTYuNjY3NCAyLjk0MDEsLTEuNTI0IDYuMDk5NywtMi4zMjg0IDkuNTAwNSwtMi40MTMgeiBtIDguNjY2OCwzMC40MTYyIGMgMi43NDI2LC0wLjA2MyA0LjkxNDgsLTAuODg5IDYuNTM4NSwtMi40MzQxIDEuNjQ1NSwtMS41MjQgMi40NTc0LC0zLjQ1MDEgMi40MzU0LC01Ljc5OTYgLTAuMDQ0LC0yLjMyODMgLTAuODMzNywtNC4zNjAzIC0yLjQzNTQsLTYuMTE3MSAtMS41MzU5LC0xLjc1NjkgLTMuODE3OCwtMy4wOTA0IC02Ljc3OTksLTQuMDAwNSAtMi45NDAxLC0wLjkzMTQgLTUuNjYwOCwtMS4zNTQ3IC04LjE0MDEsLTEuMjcgLTMuMDQ5OCwwLjA4NSAtNS40ODUzLDAuOTMxMyAtNy4yNjI1LDIuNTE4OCAtMS43NzczLDEuNjA4NyAtMi42NTQ5LDMuNTc3MiAtMi42MzMsNS45MjY2IDAuMDQ0LDIuOTQyMiAxLjQyNjIsNS4zNzYzIDQuMTY4OCw3LjMwMjUgMy44ODM2LDIuNzUxNiA4LjU3OSw0LjA0MjggMTQuMTA4MiwzLjg3MzQgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDIwMGY3O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDowLjI2OTM4MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg4NzciCiAgICAgICAgICAgZD0ibSA1MzAuOTEyMywtNzI5Ljk2ODU4IDAuMTMxNiwxMC43NTI2IC0xMC41MzE3LC0xLjcxNDUgLTAuMTMxNywtMTAuNzUyNiB6IG0gLTE2LjM0NjIsLTIuNjg4MSAwLjEzMTcsMTAuNzUyNiAtNDIuOTYwOCwtNy4xMTIgLTAuMTMxNywtMTAuNzUyNiB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMjAwZjc7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjAuMjY5MzgxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDg3OSIKICAgICAgICAgICBkPSJtIDUwNi4xNjI2LC03MTYuNzM5NDggOC42MjI5LDEuMzk3IDAuMDY2LDUuMjQ5MyA2Ljg2NzUsMS4xMjE4IDguMTQwMiwxMi4xMDczIC0xNC44NzYxLC0yLjQ3NjUgMC4wODgsNi41NjE2IC04LjYyMjksLTEuMzk3IC0wLjA4OCwtNi42MDM5IC0xNy45OTE3LC0yLjk4NDUgYyAtMy4yNjkyLC0wLjUyOTIgLTUuMTEyMywtMC43ODMyIC01LjU3MzEsLTAuNzYyIC0wLjg5OTYsMC4wMjEgLTEuNTc5NywwLjI5NjMgLTIuMDg0NCwwLjgyNTUgLTAuNDgyNywwLjU1MDMgLTAuNzI0LDEuNTAyOCAtMC43MDIxLDIuODc4NiAwLDAuNDY1NyAwLjEwOTcsMS42NTEgMC4zMjkxLDMuNTU2IGwgLTguNjIyOCwtMS40MzkzIGMgLTAuNDE2OSwtMS44NDE1IC0wLjYzNjMsLTMuNzQ2NSAtMC42ODAyLC01LjY5MzggLTAuMDQ0LC0zLjc4ODggMC43MDIxLC02LjU4MjggMi4yMzgsLTguMzM5NiAxLjU3OTcsLTEuNzc4IDMuNzUxOSwtMi42ODgyIDYuNTYwNCwtMi43NzI4IDEuMzE2NSwtMC4wNDIgNC4zNDQzLDAuMzM4NiA5LjEyNzUsMS4xMjE4IGwgMTcuMjY3NywyLjg1NzUgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDIwMGY3O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDowLjI2OTM4MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg4ODEiCiAgICAgICAgICAgZD0ibSA0OTAuNTE4NiwtNjU0LjU5NDU4IC0wLjMyOTEsLTI2Ljk4NzMgYyAtMC40NjA4LDAgLTAuODExOCwwIC0xLjA1MzIsMCAtMi44NTIzLDAuMDg1IC01LjEzNDIsMC44ODkgLTYuODY3NiwyLjQzNDEgLTEuNzMzMywxLjU4NzUgLTIuNTg5LDMuNDUwMiAtMi41NjcxLDUuNjMwMyAwLjA0NCwzLjU3NzEgMi4wODQ0LDYuMzA3NiA2LjA3NzcsOC4xOTE1IGwgLTEuNjAxNyw5LjY3MyBjIC00LjE5MDcsLTEuNzU2OCAtNy4zNzIyLC00LjE2OTggLTkuNTAwNSwtNy4yNjAxIC0yLjEwNjMsLTMuMDY5MSAtMy4xODE1LC02LjU0MDQgLTMuMjQ3MywtMTAuNDU2MiAtMC4wNjYsLTUuMzM0IDEuNzExNCwtOS43MzY2IDUuMjg3OCwtMTMuMTg2OCAzLjYyMDMsLTMuNDUwMSA4LjQyNTQsLTUuMjcwNCAxNC40MTU0LC01LjQzOTggNS44ODAyLC0wLjE0ODEgMTEuMTAyMiwxLjIwNjUgMTUuNzMxOCw0LjEwNjMgNi4yMzEzLDMuOTM3IDkuMzkwOCw5LjczNjYgOS41MDA1LDE3LjM3NzcgMC4wNDQsNC44ODk1IC0xLjUxMzksOC44MDUzIC00LjczOTMsMTEuNzY4NiAtMy4yMDM0LDIuOTYzMyAtNy43MDEzLDQuNTI5NyAtMTMuNTM3Nyw0LjY5OSAtMi44MDg1LDAuMDYzIC01LjMzMTcsLTAuMTA1OCAtNy41Njk3LC0wLjU1MDMgeiBtIDYuOTU1NCwtOS4yNzEgYyAwLjUwNDYsMC4wMjEgMC44OTk1LDAuMDIxIDEuMTYyOCwwLjAyMSAzLjE4MTUsLTAuMTA1OCA1LjU1MTIsLTAuODI1NCA3LjEzMDksLTIuMjAxMyAxLjU1NzgsLTEuMzc1OCAyLjMyNTgsLTMuMTUzOCAyLjMwMzgsLTUuMzc2MyAtMC4wMjIsLTIuMjQzNiAtMC45NjU0LC00LjIxMjEgLTIuNzg2NSwtNS45MjY2IC0xLjgyMTEsLTEuNjkzMyAtNC40OTc5LC0yLjgxNTEgLTguMDMwNSwtMy4zNDQzIHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAyMDBmNztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC4yNjkzODEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoODgzIgogICAgICAgICAgIGQ9Im0gNTMyLjMxNjUsLTYxOC45NTAxOCAwLjEzMTcsMTAuNzk0OSAtMTAuNTA5OSwtMS43MzU3IC0wLjE1MzYsLTEwLjc5NDkgeiBtIC0xNi4zNDYxLC0yLjY4ODIgMC4xMzE2LDEwLjc5NSAtMzYuMzU2NSwtNi4wNTM3IGMgLTEwLjM3ODEsLTEuNzE0NSAtMTYuNjk3MiwtMy4yMzg1IC0xOC45MTMzLC00LjYxNDMgLTMuNDQ0NywtMi4wNTMxIC01LjIsLTUuNDM5OCAtNS4yNjU4LC0xMC4yMDIyIC0wLjAyMiwtMi45NDIyIDAuMzczLC01LjY1MTUgMS4yMDY3LC04LjEyOCBsIDkuNTY2NCwxLjQxODIgYyAtMC40MTY5LDEuNzM1NiAtMC42MzYzLDMuMTk2MSAtMC42MTQ0LDQuMzYwMyAwLjAyMiwxLjQ2MDUgMC42MzYzLDIuNjAzNSAxLjg0MzEsMy40NTAxIDEuMjI4NywwLjgyNTUgNS41MDcyLDEuODYyNyAxMi44MTM2LDMuMDkwMyB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMjAwZjc7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjAuMjY5MzgxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDg4NSIKICAgICAgICAgICBkPSJtIDQ5MS4wODkxLC02MDYuNDYxOTggYyA3Ljk0MjcsLTAuMjMyOCAxNC4yODM3LDEuNjcyMiAxOS4wNDQ5LDUuNjkzOCA0Ljc4MzIsNC4wMjE3IDcuMjE4Niw5LjQ0MDMgNy4zMDY0LDE2LjIxMzUgMC4wODgsNS44NjMyIC0xLjYwMTcsMTAuNDk4NiAtNS4wNjg0LDEzLjg4NTI4IC0zLjQyMjgsMy40MDc4IC04LjA3NDMsNS4xODU4IC0xMy45NzY1LDUuMzU1MTQgLTYuOTMzNCwwLjE5MDUgLTEyLjk2NzIsLTEuNjkzMzQgLTE4LjA3OTUsLTUuNjcyNjIgLTUuMTEyMywtMy45NzkzIC03LjcwMTQsLTkuNDQwMyAtNy43ODkxLC0xNi4zNjE3IC0wLjA0NCwtMy43NDY1IDAuNzI0LC03LjA5MDggMi4zMDM4LC0xMC4wMzI5IDEuNTU3OCwtMi45NDIyIDMuODE3OCwtNS4xNjQ3IDYuNzU3OSwtNi42Njc1IDIuOTQwMSwtMS41MjQgNi4xMjE1LC0yLjMyODMgOS41MDA1LC0yLjQxMyB6IG0gOC42NjY3LDMwLjQxNjMgYyAyLjc0MjcsLTAuMDYzIDQuOTE0OSwtMC44ODkgNi41Mzg1LC0yLjQzNDEgMS42Njc1LC0xLjUyNCAyLjQ1NzQsLTMuNDUwMiAyLjQzNTUsLTUuNzk5NyAtMC4wMjIsLTIuMzI4MyAtMC44MzM4LC00LjM2MDMgLTIuNDEzNiwtNi4xMTcxIC0xLjU1NzgsLTEuNzU2OCAtMy44MTc3LC0zLjA5MDMgLTYuODAxNywtNC4wMDA1IC0yLjk0MDEsLTAuOTMxMyAtNS42Mzg5LC0xLjM1NDYgLTguMTE4MiwtMS4yNyAtMy4wNzE4LDAuMDg1IC01LjQ4NTMsMC45MzE0IC03LjI4NDUsMi41MTg5IC0xLjc3NzIsMS42MDg2IC0yLjY1NDksMy41NzcxIC0yLjYzMjksNS45MjY2IDAuMDQ0LDIuOTQyMSAxLjQyNjEsNS4zNzYzIDQuMTY4OCw3LjMwMjQgMy44ODM2LDIuNzUxNyA4LjYwMDksNC4wNDI4IDE0LjEwODEsMy44NzM1IHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAyMDBmNztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC4yNjkzODEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoODg3IgogICAgICAgICAgIGQ9Im0gNTE2LjgyNjEsLTU1My4zNTUyIDAuMTMxNiwxMC4yMDIyOCAtNS41NzMxLC0wLjg4OSBjIDIuMzkxNiwyLjQ3NjQ3IDQuMTAzLDQuODA0ODEgNS4xMzQzLDYuOTYzNzggMS4wNTMxLDIuMjAxMzEgMS41Nzk3LDQuNTI5NjQgMS42MjM2LDYuOTYzNzggMC4wNDQsMy4yODA4MSAtMC45NDM0LDUuODg0MjggLTIuOTQwMSw3LjgxMDQ1IC0xLjk5NjYsMS45Njg0NyAtNC42NTE1LDIuOTg0NDcgLTcuOTg2NiwzLjA5MDMgLTEuNTEzOSwwLjA0MjMgLTQuMzY2MywtMC4yOTYzMyAtOC41NzksLTAuOTk0ODMgbCAtMjQuMzEwOCwtNC4wMDA0NyAtMC4xMzE2LC0xMC43NzM3NiAyNC4zOTg2LDQuMDQyODEgYyAzLjY0MjIsMC41OTI2NyA1LjgxNDQsMC44ODkgNi40OTQ1LDAuODY3ODMgMS40OTIsLTAuMDQyMyAyLjYzMywtMC40ODY4MyAzLjQ2NjcsLTEuMzMzNSAwLjg1NTcsLTAuODI1NDcgMS4yNzI2LC0xLjk2ODQ3IDEuMjUwNywtMy40MDc4IC0wLjAyMiwtMS41ODc0OCAtMC43NjgsLTMuMzAxOTggLTIuMjE2MSwtNS4xODU3OCAtMS40NDgxLC0xLjg4Mzg0IC0zLjMzNSwtMy4zNDQzMSAtNS43MDQ3LC00LjM4MTQ4IC0xLjY2NzUsLTAuNzYyIC01LjA5MDMsLTEuNTQ1MTYgLTEwLjI0NjUsLTIuNDEyOTcgbCAtMTcuNjE4NywtMi45MjEgLTAuMTUzNiwtMTAuNzUyNTkgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDIwMGY3O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDowLjI2OTM4MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg4ODkiCiAgICAgICAgICAgZD0ibSA0OTAuOTU3NCwtNDg3LjE0NjM0IC0xLjY4OTQsMTAuNDk4NTggYyAtNS4wMjQ2LC0xLjU4NzUgLTguODY0MywtNC4wMjE2NCAtMTEuNTE5MSwtNy4zMjM2MSAtMi42NTQ5LC0zLjMwMTk3IC00LjAxNTMsLTcuMTc1NDUgLTQuMDU5MSwtMTEuNjIwNDIgLTAuMDY2LC01LjA3OTk1IDEuNTEzOSwtOS4xNDM5MiA0LjcxNzMsLTEyLjIxMzA2IDMuMjI1NCwtMy4wNjkxNCA3LjY3OTQsLTQuNjc3ODEgMTMuMzg0MSwtNC44MjU5OCA0LjY1MTUsLTAuMTQ4MTYgOS4wNjE3LDAuNjM1IDEzLjIzMDUsMi4zMjgzMSA0LjE2ODgsMS42NzIxNyA3LjQ2LDQuMjk2ODEgOS44NzM1LDcuODUyNzggMi40MzU1LDMuNTU1OTggMy42ODYyLDcuNDkyOTUgMy43MywxMS44MTA5MiAwLjA2Niw0LjY5ODk4IC0xLjIwNjcsOC40ODc3OCAtMy43OTU4LDExLjM2NjQyIC0yLjYxMSwyLjg1NzQ4IC02LjA1NTgsNC41Mjk2MiAtMTAuMzU2Miw1LjAxNjQ1IGwgLTEuMjk0NSwtMTAuMjAyMjUgYyAyLjM5MTUsLTAuMzE3NSA0LjE0NjgsLTEuMDU4MzQgNS4yNDM5LC0yLjIyMjQ4IDEuMTE5LC0xLjEyMTgzIDEuNjY3NSwtMi41ODIzIDEuNjQ1NiwtNC4zODE0NyAtMC4wMjIsLTIuMDc0MzEgLTAuODc3NywtNC4wMDA0NyAtMi41NjcxLC01Ljc1NzI4IC0xLjY4OTUsLTEuNzM1NjcgLTQuMjEyNywtMy4wNDc5NyAtNy41Njk3LC0zLjkzNjk3IC0zLjMxMzEsLTAuODY3ODQgLTYuNDI4OCwtMS4yNDg4NCAtOS4zMjUsLTEuMTY0MTcgLTIuNTIzMywwLjA2MzUgLTQuNDc2LDAuNjk4NSAtNS44MzY0LDEuODgzODMgLTEuMzYwMywxLjE4NTMxIC0yLjAxODUsMi42NDU4MSAtMS45OTY2LDQuMzgxNDUgMC4wMjIsMS43MTQ1IDAuNzAyMSwzLjM2NTQ3IDIuMDg0NCw0LjkzMTgxIDEuMzYwNCwxLjU0NTE2IDMuNDAwOSwyLjc1MTY0IDYuMDk5NiwzLjU3NzE0IHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAyMDBmNztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC4yNjkzODEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlkPSJwYXRoODkxIgogICAgICAgICAgIGQ9Im0gNTA5LjMwMDIsLTQ2OS4yMTgzMSA4LjYyMjksMS4zOTcgMC4wNjYsNS4yNDkyOCA2Ljg2NzYsMS4xMjE4MyA4LjE0MDIsMTIuMTA3MjMgLTE0Ljg3NjEsLTIuNDc2NDcgMC4wODgsNi41NjE2MSAtOC42MjI5LC0xLjM5Njk3IC0wLjA4OCwtNi42MDM5NyAtMTcuOTkxNywtMi45ODQ0OCBjIC0zLjI2OTMsLTAuNTI5MTcgLTUuMTM0MywtMC43ODMxNCAtNS41NzMxLC0wLjc4MzE0IC0wLjg5OTYsMC4wMjEyIC0xLjYwMTcsMC4zMTc0NyAtMi4xMDYzLDAuODQ2NjQgLTAuNDYwOCwwLjU1MDMzIC0wLjcwMjIsMS41MDI4MyAtMC42ODAyLDIuODc4NjQgMCwwLjQ2NTY3IDAuMTA5NywxLjY1MSAwLjMyOTEsMy41NTU5OCBsIC04LjYyMjksLTEuNDYwNDggYyAtMC40MTY4LC0xLjgyMDMzIC0wLjY1ODIsLTMuNzI1MzEgLTAuNjgwMSwtNS42NzI2NCAtMC4wNDQsLTMuNzg4NzggMC43MDIxLC02LjU4Mjc4IDIuMjM4LC04LjMzOTU5IDEuNTU3OCwtMS43NzggMy43NTE5LC0yLjcwOTMgNi41Mzg0LC0yLjc3MjggMS4zMTY1LC0wLjA0MjMgNC4zNjYzLDAuMzM4NjQgOS4xNDk1LDEuMTIxOCBsIDE3LjI2NzcsMi44NTc0OCB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMjAwZjc7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjAuMjY5MzgxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDg5MyIKICAgICAgICAgICBkPSJtIDUzNC42MjAzLC00MzUuNTIxMjQgMC4xNTM2LDEwLjc1MjU5IC0xMC41MzE3LC0xLjczNTY0IC0wLjEzMTcsLTEwLjc1MjU5IHogbSAtMTYuMzI0MiwtMi42ODgxNCAwLjEzMTcsMTAuNzUyNTkgLTQyLjk2MDgsLTcuMTExOTUgLTAuMTMxNywtMTAuNzUyNTkgeiIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDIwMGY3O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDowLjI2OTM4MSIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgaWQ9InBhdGg4OTUiCiAgICAgICAgICAgZD0ibSA0OTMuNDE0OCwtNDIyLjk2OTQ5IGMgNy45NDI3LC0wLjIxMTY2IDE0LjI4MzcsMS42NzIxNyAxOS4wNDUsNS42OTM3OCA0Ljc4MzEsNC4wNDI4MSA3LjIxODYsOS40NDAyOCA3LjMwNjQsMTYuMjEzNTYgMC4wODgsNS44NjMxMiAtMS42MjM3LDEwLjQ5ODU5IC01LjA2ODQsMTMuOTA2NCAtMy40MjI5LDMuMzg2NjQgLTguMDk2Myw1LjE2NDY0IC0xMy45NzY2LDUuMzMzOTUgLTYuOTMzNCwwLjIxMTY2IC0xMi45NjcyLC0xLjY5MzMxIC0xOC4wNzk1LC01LjY3MjYyIC01LjExMjIsLTMuOTc5MyAtNy43MDEzLC05LjQxOTA5IC03Ljc4OTEsLTE2LjM0MDUzIC0wLjA0NCwtMy43NDY0OCAwLjcyNDEsLTcuMTExOTUgMi4yODE5LC0xMC4wNTQxMiAxLjU3OTgsLTIuOTQyMTQgMy44Mzk3LC01LjE2NDYxIDYuNzc5OCwtNi42Njc0NCAyLjk0MDEsLTEuNTAyODEgNi4xMjE2LC0yLjMwNzE0IDkuNTAwNSwtMi40MTI5OCB6IG0gOC42NjY4LDMwLjQzNzQzIGMgMi43NDI2LC0wLjA4NDcgNC45MTQ4LC0wLjg4ODk3IDYuNTM4NSwtMi40MzQxNCAxLjY0NTUsLTEuNTIzOTcgMi40NTc0LC0zLjQ3MTMxIDIuNDM1NCwtNS44MjA3OCAtMC4wMjIsLTIuMzA3MTcgLTAuODMzNywtNC4zNjAzMSAtMi40MTM1LC02LjExNzE0IC0xLjU1NzgsLTEuNzU2ODEgLTMuODE3OCwtMy4wOTAzMSAtNi44MDE4LC00LjAwMDQ1IC0yLjk0MDEsLTAuOTEwMTYgLTUuNjYwOCwtMS4zMzM1IC04LjExODIsLTEuMjcgLTMuMDcxNywwLjA4NDcgLTUuNTA3MiwwLjkzMTM0IC03LjI4NDQsMi41Mzk5OCAtMS43NzczLDEuNTg3NSAtMi42NTQ5LDMuNTU1OTcgLTIuNjMzLDUuOTA1NDcgMC4wNDQsMi45NjMzMSAxLjQyNjIsNS4zOTc0NSA0LjE2ODgsNy4zMjM2MSAzLjg4MzYsMi43MzA0OCA4LjU3OSw0LjAyMTYyIDE0LjEwODIsMy44NzM0NSB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiMwMjAwZjc7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjAuMjY5MzgxIgogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpZD0icGF0aDg5NyIKICAgICAgICAgICBkPSJtIDUxOS4xNTE4LC0zNjkuODQxNTUgMC4xMzE3LDEwLjE4MTA5IC01LjU3MzEsLTAuODg4OTggYyAyLjM5MTYsMi40NzY0OCA0LjEwMyw0LjgwNDc4IDUuMTM0Miw2Ljk4NDk1IDEuMDUzMiwyLjIwMTMxIDEuNTc5OCw0LjUwODQ1IDEuNjIzNyw2Ljk2Mzc4IDAuMDIyLDMuMjgwODEgLTAuOTQzNSw1Ljg2MzEyIC0yLjk0MDEsNy44MTA0MiAtMS45OTY3LDEuOTQ3MzQgLTQuNjUxNiwyLjk4NDUgLTcuOTg2NiwzLjA2OTE3IC0xLjUxMzksMC4wNDIzIC00LjM2NjMsLTAuMjk2MzMgLTguNTc5LC0wLjk5NDgzIGwgLTI0LjMxMDgsLTQuMDAwNDggLTAuMTMxNiwtMTAuNzUyNTkgMjQuMzk4NSw0LjAyMTY0IGMgMy42NDIyLDAuNjEzODQgNS44MTQ0LDAuODg5IDYuNDk0NiwwLjg2Nzg0IDEuNDkyLC0wLjA0MjMgMi42MzI5LC0wLjQ4Njg0IDMuNDY2NywtMS4zMzM1IDAuODU1NywtMC44MjU1IDEuMjcyNiwtMS45NDczMSAxLjI1MDYsLTMuNDA3ODEgLTAuMDIyLC0xLjU2NjMzIC0wLjc2NzksLTMuMzAxOTcgLTIuMjE2LC01LjE4NTgxIC0xLjQ0ODEsLTEuODgzOCAtMy4zNTcsLTMuMzQ0MyAtNS43MDQ3LC00LjM4MTQ0IC0xLjY2NzUsLTAuNzQwODQgLTUuMDkwNCwtMS41NDUxNyAtMTAuMjQ2NSwtMi4zOTE4NCBsIC0xNy42MTg4LC0yLjkyMDk3IC0wLjE1MzYsLTEwLjc1MjU5IHoiCiAgICAgICAgICAgc3R5bGU9ImZpbGw6IzAyMDBmNztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC4yNjkzODEiCiAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgICAgPC9nPgogICAgPC9nPgogIDwvZz4KPC9zdmc+Cg=="/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="parameters"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3.5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option name="active" type="bool" value="true"/>
                  <Option name="field" type="QString" value="rot_z"/>
                  <Option name="type" type="int" value="2"/>
                </Option>
              </Option>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="3" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="color" type="QString" value="255,255,255,255"/>
            <Option name="fixedAspectRatio" type="QString" value="0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="name" type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTcwY20iCiAgIGhlaWdodD0iOTFjbSIKICAgdmlld0JveD0iMCAwIDE3MDAgOTEwIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmc4OTciCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMi4yICg3MzJhMDFkYTYzLCAyMDIyLTEyLTA5KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic2RlPWJvaXRlX2luY2VydGFpbmUuc3ZnIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczg5MSI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg5MiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg5MCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg4NCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg4MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg3NiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg3NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg2NCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZmYxMzAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg2MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg1NiIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg1NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg1MCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDg0NCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0MiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ3ODUiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2ZmMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A0NzgzIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50ODY0IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50ODk0IgogICAgICAgeDE9IjI0NzQuODgyOCIKICAgICAgIHkxPSItMjAzIgogICAgICAgeDI9IjI4OTQuMjgzNyIKICAgICAgIHkyPSItMjAzIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBpZD0iYmFzZSIKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMS4wIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp6b29tPSIwLjA5MDIzNDM3NyIKICAgICBpbmtzY2FwZTpjeD0iMTA1Mi44MTM4IgogICAgIGlua3NjYXBlOmN5PSIxODYxLjgxODEiCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9ImxheWVyMSIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICB1bml0cz0iY20iCiAgICAgc2hvd2d1aWRlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpndWlkZS1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6YmJveC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpiYm94LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtYmJveC1lZGdlLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLXNtb290aC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10ZXh0LWJhc2VsaW5lPSJ0cnVlIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIxOTEyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1nbG9iYWw9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC10by1ndWlkZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIj4KICAgIDxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDg2OCIKICAgICAgIG9yaWdpbng9IjAiCiAgICAgICBvcmlnaW55PSIwIiAvPgogIDwvc29kaXBvZGk6bmFtZWR2aWV3PgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTg5NCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpbmtzY2FwZTpsYWJlbD0iQ2FscXVlIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTQwMykiPgogICAgPGcKICAgICAgIGlkPSJnMzM2Ij4KICAgICAgPGcKICAgICAgICAgaWQ9ImczMjQiCiAgICAgICAgIHRyYW5zZm9ybT0icm90YXRlKC05MCw2NTYuNDc2MTksLTc1Mi4zODgxNSkiPgogICAgICAgIDx0ZXh0CiAgICAgICAgICAgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIKICAgICAgICAgICBzdHlsZT0iZm9udC1zdHlsZTppdGFsaWM7Zm9udC12YXJpYW50Om5vcm1hbDtmb250LXdlaWdodDpib2xkO2ZvbnQtc3RyZXRjaDpub3JtYWw7Zm9udC1zaXplOjc5Ljc4MDdweDtsaW5lLWhlaWdodDoxLjI1O2ZvbnQtZmFtaWx5OkFyaWFsOy1pbmtzY2FwZS1mb250LXNwZWNpZmljYXRpb246J0FyaWFsLCBCb2xkIEl0YWxpYyc7Zm9udC12YXJpYW50LWxpZ2F0dXJlczpub3JtYWw7Zm9udC12YXJpYW50LWNhcHM6bm9ybWFsO2ZvbnQtdmFyaWFudC1udW1lcmljOm5vcm1hbDtmb250LWZlYXR1cmUtc2V0dGluZ3M6bm9ybWFsO3RleHQtYWxpZ246c3RhcnQ7bGV0dGVyLXNwYWNpbmc6MHB4O3dvcmQtc3BhY2luZzowcHg7d3JpdGluZy1tb2RlOmxyLXRiO3RleHQtYW5jaG9yOnN0YXJ0O2ZpbGw6IzAyMDBmNztmaWxsLW9wYWNpdHk6MC45NjQ3MDY7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjEuOTk0NDkiCiAgICAgICAgICAgeD0iLTIwNi4wNjU0NCIKICAgICAgICAgICB5PSItNjQzLjY1MjgzIgogICAgICAgICAgIGlkPSJ0ZXh0MTQ0Ny03IgogICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNDQxNDEyODQsMC44NTI4ODA0MywtMC45MTc0Njg4MywwLjQ5Mjc1NTk0LDAsMCkiCiAgICAgICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci14PSI5OS45OTk5ODEiCiAgICAgICAgICAgaW5rc2NhcGU6dHJhbnNmb3JtLWNlbnRlci15PSIxNTAuMDAwMDEiPjx0c3BhbgogICAgICAgICAgICAgc29kaXBvZGk6cm9sZT0ibGluZSIKICAgICAgICAgICAgIGlkPSJ0c3BhbjgzOSIKICAgICAgICAgICAgIHg9Ii0yMDYuMDY1NDQiCiAgICAgICAgICAgICB5PSItNjQzLjY1MjgzIj5ib2l0ZSBpbmNlcnRhaW5lPC90c3Bhbj48L3RleHQ+CiAgICAgICAgPGcKICAgICAgICAgICBpZD0iZzg3NSIKICAgICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNjcxLjExMzgxLDczNC4wNzU5MykiPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDg1OCIKICAgICAgICAgICAgIGQ9Ik0gMTUyMS4xMTM4LC0yMDg3LjA3NTkgViAtNDg3LjA3NTk0IgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I2ZmMDAwMDtzdHJva2Utd2lkdGg6MzA7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgaWQ9InBhdGg4NjAiCiAgICAgICAgICAgICBkPSJtIDEwOTYuMTEzOCwtMTI4Ny4wNzU5IGggODUwIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I2ZmMDAwMDtzdHJva2Utd2lkdGg6MzA7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MztzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgICAgaWQ9InBhdGg4NjQiCiAgICAgICAgICAgICBkPSJtIDE5NDYuMTEzOCwtMTI4Ny4wNzU5IC00MjUsLTgwMCAtNDI1LDgwMCIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNmZjAwMDA7c3Ryb2tlLXdpZHRoOjMwO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjM7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgIGlkPSJwYXRoODY2IgogICAgICAgICAgICAgZD0ibSAxMDk2LjExMzgsLTEyODcuMDc1OSA0MjQuOTk5OSw3OTkuOTk5NjkgNDI0Ljk5OTksLTc5OS45OTk2OSB6IgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I2ZmMDAwMDtzdHJva2Utd2lkdGg6MzA7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MztzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgICAgICA8L2c+CiAgICAgIDwvZz4KICAgICAgPHRleHQKICAgICAgICAgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIKICAgICAgICAgc3R5bGU9ImZvbnQtc3R5bGU6bm9ybWFsO2ZvbnQtdmFyaWFudDpub3JtYWw7Zm9udC13ZWlnaHQ6bm9ybWFsO2ZvbnQtc3RyZXRjaDpub3JtYWw7Zm9udC1zaXplOjcwOS4zMzNweDtsaW5lLWhlaWdodDoxLjI1O2ZvbnQtZmFtaWx5OidUaW1lcyBOZXcgUm9tYW4nOy1pbmtzY2FwZS1mb250LXNwZWNpZmljYXRpb246J1RpbWVzIE5ldyBSb21hbiwgTm9ybWFsJztmb250LXZhcmlhbnQtbGlnYXR1cmVzOm5vcm1hbDtmb250LXZhcmlhbnQtY2Fwczpub3JtYWw7Zm9udC12YXJpYW50LW51bWVyaWM6bGluaW5nLW51bXMgcHJvcG9ydGlvbmFsLW51bXM7Zm9udC1mZWF0dXJlLXNldHRpbmdzOm5vcm1hbDt0ZXh0LWFsaWduOnN0YXJ0O2xldHRlci1zcGFjaW5nOjBweDt3b3JkLXNwYWNpbmc6MHB4O3dyaXRpbmctbW9kZTpsci10Yjt0ZXh0LWFuY2hvcjpzdGFydDtmaWxsOiMwMjAwZjc7ZmlsbC1vcGFjaXR5OjAuOTY0NzA2O3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyLjUyNTc1IgogICAgICAgICB4PSI2ODEuNzE0NzIiCiAgICAgICAgIHk9Ii03NDIuMjE3NDciCiAgICAgICAgIGlkPSJ0ZXh0ODQzIj48dHNwYW4KICAgICAgICAgICBzb2RpcG9kaTpyb2xlPSJsaW5lIgogICAgICAgICAgIGlkPSJ0c3Bhbjg0NSIKICAgICAgICAgICB4PSI2ODEuNzE0NzIiCiAgICAgICAgICAgeT0iLTc0Mi4yMTc0NyIKICAgICAgICAgICBzdHlsZT0iZmlsbDojMDIwMGY3O2ZpbGwtb3BhY2l0eTowLjk2NDcwNjtzdHJva2Utd2lkdGg6Mi41MjU3NSI+Pwk8L3RzcGFuPjwvdGV4dD4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo="/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="parameters"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="4"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option name="active" type="bool" value="true"/>
                  <Option name="field" type="QString" value="rot_z"/>
                  <Option name="type" type="int" value="2"/>
                </Option>
              </Option>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="4" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="color" type="QString" value="141,90,153,255"/>
            <Option name="fixedAspectRatio" type="QString" value="0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="name" type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMTcwY20iCiAgIGhlaWdodD0iNDVjbSIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgMTcwMCA0NTAiCiAgIGlkPSJzdmcyMCIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic2RlPWJvaXRpZXJfZGlmZmVyZW50aWVsLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4yLjIgKDczMmEwMWRhNjMsIDIwMjItMTItMDkpIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzMjQiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ0NzgwIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNDc3OCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDI3IgogICAgIGlkPSJuYW1lZHZpZXcyMiIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnpvb209IjAuMTc2NjExNyIKICAgICBpbmtzY2FwZTpjeD0iMjMzMi44MDEzIgogICAgIGlua3NjYXBlOmN5PSIyMTUxLjYxMjgiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5MTIiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii04IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMjAiCiAgICAgaW5rc2NhcGU6c25hcC1ncmlkcz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94PSJ0cnVlIgogICAgIGlua3NjYXBlOmJib3gtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3gtZWRnZS1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3QtcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1pbnRlcnNlY3Rpb24tcGF0aHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1vYmplY3QtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtY2VudGVyPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtcGFnZT0idHJ1ZSIKICAgICBzaG93Z3VpZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIGlua3NjYXBlOmRlc2tjb2xvcj0iI2QxZDFkMSI+CiAgICA8aW5rc2NhcGU6Z3JpZAogICAgICAgdHlwZT0ieHlncmlkIgogICAgICAgaWQ9ImdyaWQ0NzY2IgogICAgICAgb3JpZ2lueD0iMCIKICAgICAgIG9yaWdpbnk9IjAiIC8+CiAgPC9zb2RpcG9kaTpuYW1lZHZpZXc+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMiI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8Y2M6bGljZW5zZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnktbmMtc2EvNC4wLyIgLz4KICAgICAgICA8ZGM6Y3JlYXRvcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPmptYTwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6Y3JlYXRvcj4KICAgICAgICA8ZGM6cHVibGlzaGVyPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+QXppbXV0PC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpwdWJsaXNoZXI+CiAgICAgICAgPGRjOnJpZ2h0cz4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPihjKUF6aW11dDwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cmlnaHRzPgogICAgICA8L2NjOldvcms+CiAgICAgIDxjYzpMaWNlbnNlCiAgICAgICAgIHJkZjphYm91dD0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnktbmMtc2EvNC4wLyI+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1JlcHJvZHVjdGlvbiIgLz4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjRGlzdHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjTm90aWNlIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjQXR0cmlidXRpb24iIC8+CiAgICAgICAgPGNjOnByb2hpYml0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjQ29tbWVyY2lhbFVzZSIgLz4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjRGVyaXZhdGl2ZVdvcmtzIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjU2hhcmVBbGlrZSIgLz4KICAgICAgPC9jYzpMaWNlbnNlPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpZD0iZzI0OCIKICAgICB0cmFuc2Zvcm09InJvdGF0ZSg5MCw4NDguMDA5OTEsODQ2Ljg4MzYxKSI+CiAgICA8ZwogICAgICAgaWQ9Imc4MzAiCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5NjE0Mzg2LDAsMCwwLjk5NTI5MDc2LC0xMzkuMjYzODksLTQ1LjUwNjA1MSkiPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZjQ5ZDAwO3N0cm9rZS13aWR0aDoyMDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBpZD0icGF0aDYiCiAgICAgICAgIGQ9Ik0gNDA2LjY2MjQxLDE3NDMuNzE3NiBWIDkyOC42NzkzNyBMIDMwMC44OTEyNSw1MDQuMTgwMjkiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiNmNDlkMDA7c3Ryb2tlLXdpZHRoOjIwO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgIGlkPSJwYXRoOCIKICAgICAgICAgZD0iTSA0MDYuNjYyNDEsNTA0LjE4MDI5IFYgNDUuNzIxMjkiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Im0gNDA4LjE3MzQzLDEyMDAuMzU4OCBhIDE1OC42NTY3Myw4NC44OTk4MTUgMCAwIDEgLTEzOC4yNTgwMiwtNDIuMTEwMyAxNTguNjU2NzMsODQuODk5ODE1IDAgMCAxIC0wLjE3NjAzLC04NS4yMzk0IDE1OC42NTY3Myw4NC44OTk4MTUgMCAwIDEgMTM4LjI1ODAxLC00Mi4yODAyIgogICAgICAgICBpZD0icGF0aDEyIgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZjQ5ZDAwO3N0cm9rZS13aWR0aDoyMDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgICA8cGF0aAogICAgICAgICBkPSJtIDQwNi42NjI0MSwxMjAwLjM1ODggYSAxNTguNjU2NzMsODQuODk5ODE1IDAgMCAwIDEzOC4yNTgwMSwtNDIuMTEwMyAxNTguNjU2NzMsODQuODk5ODE1IDAgMCAwIDAuMTc2MDQsLTg1LjIzOTQgMTU4LjY1NjczLDg0Ljg5OTgxNSAwIDAgMCAtMTM4LjI1ODAyLC00Mi4yODAyIgogICAgICAgICBpZD0icGF0aDE0IgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZjQ5ZDAwO3N0cm9rZS13aWR0aDoyMDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgICA8cGF0aAogICAgICAgICBkPSJNIDI0OC4wMDU2OCwxMTE1LjQ1OSBIIDE2NC44OTk3NyBWIDY1Ni45OTk5NiBoIDE3My43NjY5IgogICAgICAgICBpZD0icGF0aDE2IgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZjQ5ZDAwO3N0cm9rZS13aWR0aDoyMDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPC9nPgogIDwvZz4KPC9zdmc+Cg=="/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="parameters"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option name="active" type="bool" value="true"/>
                  <Option name="expression" type="QString" value="&quot;rot_z&quot;"/>
                  <Option name="type" type="int" value="3"/>
                </Option>
              </Option>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="5" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="color" type="QString" value="141,90,153,255"/>
            <Option name="fixedAspectRatio" type="QString" value="0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="name" type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpvc2I9Imh0dHA6Ly93d3cub3BlbnN3YXRjaGJvb2sub3JnL3VyaS8yMDA5L29zYiIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9IjEwNGNtIgogICBoZWlnaHQ9IjU1Y20iCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDEwNDAgNTUwIgogICBpZD0ic3ZnOCIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic21kZXY9c2VwYXJhdGlvbl9kZV9yZXNlYXUuc3ZnIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjQgKDVkYTY4OWMzMTMsIDIwMTktMDEtMTQpIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzMTIiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4MzIiCiAgICAgICBvc2I6cGFpbnQ9InNvbGlkIj4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2ZmMDAwMDtzdG9wLW9wYWNpdHk6MTsiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A4MzAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0NDc3NCIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgICA8aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q0NzU4IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIgLz4KICAgIDxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDQ3NzQtMSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiIC8+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTAyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI3MDUiCiAgICAgaWQ9Im5hbWVkdmlldzEwIgogICAgIHNob3dncmlkPSJ0cnVlIgogICAgIGlua3NjYXBlOmRvY3VtZW50LXVuaXRzPSJjbSIKICAgICB1bml0cz0iY20iCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtbWlkcG9pbnRzPSJ0cnVlIgogICAgIGlua3NjYXBlOnNuYXAtZ3JpZHM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtc21vb3RoLW5vZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOm9iamVjdC1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWludGVyc2VjdGlvbi1wYXRocz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6YmJveC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLWdsb2JhbD0idHJ1ZSIKICAgICBpbmtzY2FwZTpzbmFwLW9iamVjdC1taWRwb2ludHM9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1jZW50ZXI9InRydWUiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LW1pZHBvaW50cz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1iYm94LWVkZ2UtbWlkcG9pbnRzPSJmYWxzZSIKICAgICBpbmtzY2FwZTpiYm94LXBhdGhzPSJ0cnVlIgogICAgIGlua3NjYXBlOnpvb209IjAuMjU4NDc3NTYiCiAgICAgaW5rc2NhcGU6Y3g9IjMyOTUuMzkxNCIKICAgICBpbmtzY2FwZTpjeT0iMzMuNTc2MDYxIgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmc4IgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9InRydWUiPgogICAgPGlua3NjYXBlOmdyaWQKICAgICAgIHR5cGU9Inh5Z3JpZCIKICAgICAgIGlkPSJncmlkNDc1NCIgLz4KICA8L3NvZGlwb2RpOm5hbWVkdmlldz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGEyIj4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICAgIDxjYzpsaWNlbnNlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIiAvPgogICAgICAgIDxkYzpjcmVhdG9yPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+am1hPC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpjcmVhdG9yPgogICAgICAgIDxkYzpwdWJsaXNoZXI+CiAgICAgICAgICA8Y2M6QWdlbnQ+CiAgICAgICAgICAgIDxkYzp0aXRsZT5BemltdXQ8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOnB1Ymxpc2hlcj4KICAgICAgICA8ZGM6cmlnaHRzPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+KGMpQXppbXV0PC9kYzp0aXRsZT4KICAgICAgICAgIDwvY2M6QWdlbnQ+CiAgICAgICAgPC9kYzpyaWdodHM+CiAgICAgIDwvY2M6V29yaz4KICAgICAgPGNjOkxpY2Vuc2UKICAgICAgICAgcmRmOmFib3V0PSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1uYy1zYS80LjAvIj4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjUmVwcm9kdWN0aW9uIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEaXN0cmlidXRpb24iIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNOb3RpY2UiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNBdHRyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cHJvaGliaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNDb21tZXJjaWFsVXNlIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEZXJpdmF0aXZlV29ya3MiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNTaGFyZUFsaWtlIiAvPgogICAgICA8L2NjOkxpY2Vuc2U+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjcuNzc3MjM1NTE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDUwMCwyMzQuMjQwNDcgLTAuNDkyMjUsMjgzLjI1OTU1IgogICAgIGlkPSJwYXRoODQwIgogICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIC8+CiAgPGcKICAgICBpZD0iZzg3NCIKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTIwLC01MjQuOTk5ODMpIj4KICAgIDxwYXRoCiAgICAgICBpZD0icGF0aDg0NCIKICAgICAgIHRyYW5zZm9ybT0ic2NhbGUoMC4yNjQ1ODMzMykiCiAgICAgICBkPSJNIDU2Ni45Mjk2OSwzMDIzLjYyMTEgSCA0MjcwLjg2NTIgWiIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNmZjAwMDA7c3Ryb2tlLXdpZHRoOjExMy4zODU4MjYxMTtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgLz4KICAgIDxwYXRoCiAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgaWQ9InBhdGg4NDQtNSIKICAgICAgIGQ9Ik0gMTEzMCw3OTkuOTk5OTggViA1MzkuOTk5OTciCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojZmYwMDAwO3N0cm9rZS13aWR0aDozMDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgPHBhdGgKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDg0NC0yIgogICAgICAgZD0ibSAxNTAsNzk5Ljk5OTk5IHYgLTI2MCIKICAgICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNmZjAwMDA7c3Ryb2tlLXdpZHRoOjMwO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgICA8cGF0aAogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjYyIKICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICBpZD0icGF0aDg2OCIKICAgICAgIGQ9Im0gNjM5Ljk5OTk0LDc5OS45OTk3NCAwLjAwNywyNTkuOTk5OTYiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDo1LjM1NTE0MTY0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+CiAgPC9nPgo8L3N2Zz4K"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="parameters"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="5"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option name="active" type="bool" value="true"/>
                  <Option name="expression" type="QString" value="&quot;rot_z&quot;"/>
                  <Option name="type" type="int" value="3"/>
                </Option>
              </Option>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="6" type="marker" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SvgMarker" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="color" type="QString" value="225,89,137,255"/>
            <Option name="fixedAspectRatio" type="QString" value="0"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="name" type="QString" value="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgdmVyc2lvbj0iMS4xIgogICB4PSIwcHgiCiAgIHk9IjBweCIKICAgdmlld0JveD0iMCAwIDY0LjI1MTk3IDY0LjI1MTk2OSIKICAgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMTEwIDExMCIKICAgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIKICAgaWQ9InN2ZzIiCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMi4yICg3MzJhMDFkYTYzLCAyMDIyLTEyLTA5KSIKICAgc29kaXBvZGk6ZG9jbmFtZT0icmVnYXJkX2VwLnN2ZyIKICAgd2lkdGg9IjE3bW0iCiAgIGhlaWdodD0iMTdtbSIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+PG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMjQiPjxyZGY6UkRGPjxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxkZWZzCiAgICAgaWQ9ImRlZnMyMiI+PGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ4NDU5IgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+PHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDg0NTciIC8+PC9saW5lYXJHcmFkaWVudD48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTc3IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+PGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0OTU3MyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPjxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDk1NjEiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTU3IgogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+PGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0OTU0OSIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPjxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDk1NDUiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTQxIgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+PGlua3NjYXBlOnBhdGgtZWZmZWN0CiAgICAgICBlZmZlY3Q9InNwaXJvIgogICAgICAgaWQ9InBhdGgtZWZmZWN0OTUzNyIKICAgICAgIGlzX3Zpc2libGU9InRydWUiCiAgICAgICBscGV2ZXJzaW9uPSIwIiAvPjxpbmtzY2FwZTpwYXRoLWVmZmVjdAogICAgICAgZWZmZWN0PSJzcGlybyIKICAgICAgIGlkPSJwYXRoLWVmZmVjdDk1MzMiCiAgICAgICBpc192aXNpYmxlPSJ0cnVlIgogICAgICAgbHBldmVyc2lvbj0iMCIgLz48aW5rc2NhcGU6cGF0aC1lZmZlY3QKICAgICAgIGVmZmVjdD0ic3Bpcm8iCiAgICAgICBpZD0icGF0aC1lZmZlY3Q5NTI5IgogICAgICAgaXNfdmlzaWJsZT0idHJ1ZSIKICAgICAgIGxwZXZlcnNpb249IjAiIC8+PGxpbmVhckdyYWRpZW50CiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ5NTIxIgogICAgICAgaW5rc2NhcGU6c3dhdGNoPSJzb2xpZCI+PHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDk1MjMiIC8+PC9saW5lYXJHcmFkaWVudD48bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDk1MTMiCiAgICAgICBpbmtzY2FwZTpzd2F0Y2g9ImdyYWRpZW50Ij48c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eToxIgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wOTUxNSIgLz48c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMDAwMDAwO3N0b3Atb3BhY2l0eTowIgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIGlkPSJzdG9wOTUxNyIgLz48L2xpbmVhckdyYWRpZW50PjxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50OTYwMCIKICAgICAgIGlua3NjYXBlOnN3YXRjaD0ic29saWQiPjxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjEiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5NjAyIiAvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMSIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBpbmtzY2FwZTpwYWdlb3BhY2l0eT0iMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMjA0OCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMDcyIgogICAgIGlkPSJuYW1lZHZpZXcyMCIKICAgICBzaG93Z3JpZD0idHJ1ZSIKICAgICBpbmtzY2FwZTp6b29tPSI1LjEyIgogICAgIGlua3NjYXBlOmN4PSIzOC43Njk1MzEiCiAgICAgaW5rc2NhcGU6Y3k9IjMxLjI1IgogICAgIGlua3NjYXBlOndpbmRvdy14PSItMTEiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii0xMSIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9Imc0IgogICAgIGlua3NjYXBlOnNuYXAtYmJveD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6c25hcC1ub2Rlcz0idHJ1ZSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0iY20iCiAgICAgdW5pdHM9ImNtIgogICAgIGlua3NjYXBlOnNuYXAtb2JqZWN0LW1pZHBvaW50cz0idHJ1ZSIKICAgICBpbmtzY2FwZTpvYmplY3Qtbm9kZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSJmYWxzZSIKICAgICBzaG93Z3VpZGVzPSJ0cnVlIgogICAgIGlua3NjYXBlOmxvY2tndWlkZXM9ImZhbHNlIgogICAgIGlua3NjYXBlOmd1aWRlLWJib3g9InRydWUiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkZXNrY29sb3I9IiNkMWQxZDEiPjxpbmtzY2FwZTpncmlkCiAgICAgICB0eXBlPSJ4eWdyaWQiCiAgICAgICBpZD0iZ3JpZDgwNzAiCiAgICAgICBvcmlnaW54PSIwIgogICAgICAgb3JpZ2lueT0iMCIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSIwLC03ODcyLjc1NjEiCiAgICAgICBvcmllbnRhdGlvbj0iMCw3OTM3LjAwNzkiCiAgICAgICBpZD0iZ3VpZGU4NDAiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249Ijc5MzcuMDA4MSwtNzg3Mi43NTYxIgogICAgICAgb3JpZW50YXRpb249Ii03OTM3LjAwNzksMCIKICAgICAgIGlkPSJndWlkZTg0MiIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PHNvZGlwb2RpOmd1aWRlCiAgICAgICBwb3NpdGlvbj0iNzkzNy4wMDgxLDY0LjI1MjA0OSIKICAgICAgIG9yaWVudGF0aW9uPSIwLC03OTM3LjAwNzkiCiAgICAgICBpZD0iZ3VpZGU4NDQiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjAsNjQuMjUyMDQ5IgogICAgICAgb3JpZW50YXRpb249Ijc5MzcuMDA3OSwwIgogICAgICAgaWQ9Imd1aWRlODQ2IgogICAgICAgaW5rc2NhcGU6bG9ja2VkPSJmYWxzZSIgLz48c29kaXBvZGk6Z3VpZGUKICAgICAgIHBvc2l0aW9uPSI3OTM3LjAwODIsNjQuMjUxOTQ5IgogICAgICAgb3JpZW50YXRpb249Ii0wLjcwNzEwNjc4LDAuNzA3MTA2NzgiCiAgICAgICBpZD0iZ3VpZGU4NDgiCiAgICAgICBpbmtzY2FwZTpsb2NrZWQ9ImZhbHNlIiAvPjxzb2RpcG9kaTpndWlkZQogICAgICAgcG9zaXRpb249IjAsNjQuMjUxOTQ5IgogICAgICAgb3JpZW50YXRpb249IjAuNzA3MTA2NzgsMC43MDcxMDY3OCIKICAgICAgIGlkPSJndWlkZTg1MCIKICAgICAgIGlua3NjYXBlOmxvY2tlZD0iZmFsc2UiIC8+PC9zb2RpcG9kaTpuYW1lZHZpZXc+PGcKICAgICBpZD0iZzQiCiAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCw3Nzk5LjUwOCkiPjxnCiAgICAgICBpZD0iZzgzOCIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuMDA3MDQsMCwwLDAuMDA3MDQsMy44MTYyMzE5LC03NzM5LjQ2NDIpIj48cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtzdHJva2Utd2lkdGg6MC45OTk1MzE7cGFpbnQtb3JkZXI6c3Ryb2tlIGZpbGwgbWFya2VycyIKICAgICAgICAgaWQ9InBhdGgxMCIKICAgICAgICAgZD0iTSA4Ni45NjU2MTIsMTguMzM0NDcxIEggMjIuNzc3MTExIGMgLTIuMzUwNzc4LDAgLTQuMjU3MTExLDEuOTE3ODkxIC00LjI1NzExMSw0LjI4MTkyIHYgNjQuNTgxNjk3IGMgMCwyLjM2MzAyNiAxLjkwNjMzMyw0LjI3ODkxMiA0LjI1NzExMSw0LjI3ODkxMiBoIDY0LjE4ODUwMSBjIDIuMzUyNzcxLDAgNC4yNTgxMDcsLTEuOTE0ODgzIDQuMjU4MTA3LC00LjI3ODkxMiBWIDIyLjYxNjM5MSBjIDkuOTdlLTQsLTIuMzY0MDI5IC0xLjkwNTMzNiwtNC4yODE5MiAtNC4yNTgxMDcsLTQuMjgxOTIgeiBtIC0zMi4wOTI3NTYsNjAuODM0MTQgYyAtMTMuMzIxNDA4LDAgLTI0LjExNTY1NiwtMTAuODYyNzAyIC0yNC4xMTU2NTYsLTI0LjI2MTg3MyAwLC0xMy4zOTkxNyAxMC43OTQyNDgsLTI0LjI2Mzg3OCAyNC4xMTU2NTYsLTI0LjI2Mzg3OCAxMy4zMTc0MjMsMCAyNC4xMTU2NTYsMTAuODY0NzA4IDI0LjExNTY1NiwyNC4yNjM4NzggMCwxMy4zOTkxNzEgLTEwLjc5ODIzMywyNC4yNjE4NzMgLTI0LjExNTY1NiwyNC4yNjE4NzMgeiIgLz48cGF0aAogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICBzdHlsZT0ib3BhY2l0eToxO2ZpbGw6I2ZmOGMwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6I2ZmOGMwMDtzdHJva2Utd2lkdGg6MTEzLjMzMztzdHJva2UtbWl0ZXJsaW1pdDowO3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxO3BhaW50LW9yZGVyOnN0cm9rZSBmaWxsIG1hcmtlcnMiCiAgICAgICAgIGlkPSJwYXRoMTQiCiAgICAgICAgIGQ9Ik0gNzE2Mi45ODg0LC0xMTYuMjU4MDcgSCA3OTkuMDUwMDQgYyAtMjkxLjA1NjY2LDAgLTUyNy44MDc5OCwtMjM4LjM1MjcgLTUyNy44MDc5OCwtNTMxLjE3NDAxIFYgLTcwNDkuOTU1OSBjIDAsLTI5Mi45MDQzIDIzNi43NTEzMiwtNTMxLjAwODIgNTI3LjgwNzk4LC01MzEuMDA4MiBIIDcxNjIuOTg4NCBjIDI5MS4wNTY1LDAgNTI3Ljk3MjcsMjM4LjE4NjcgNTI3Ljk3MjcsNTMxLjAwODIgdiA2NDAyLjQ0MDk3IGMgMC4wODMsMjkyLjkwNDE2IC0yMzYuODMzOCw1MzEuMjU2ODYgLTUyNy45NzI3LDUzMS4yNTY4NiB6IE0gNzk5LjA1MDA0LC03MjI2Ljk1ODcgYyAtOTcuMTU2MSwwIC0xNzUuOTM2MDIsNzkuMzQwNCAtMTc1LjkzNjAyLDE3Ny4wMDI4IHYgNjQwMi40NDA5NyBjIDAsOTcuNzQ1MjYgNzguNzc5OTIsMTc3LjE2ODQ5IDE3NS45MzYwMiwxNzcuMTY4NDkgSCA3MTYyLjk4ODQgYyA5Ny4xNTYyLDAgMTc2LjEwMDgsLTc5LjQyMzIzIDE3Ni4xMDA4LC0xNzcuMTY4NDkgViAtNzA0OS45NTU5IGMgMCwtOTcuNzQ1NCAtNzguOTQ0NiwtMTc3LjAwMjggLTE3Ni4xMDA4LC0xNzcuMDAyOCB6IiAvPjxyZWN0CiAgICAgICAgIHk9Ii02OTIwLjM4MTgiCiAgICAgICAgIHg9Ijk1Ni43MTg2MyIKICAgICAgICAgaGVpZ2h0PSI2MDk1LjU0NDkiCiAgICAgICAgIHdpZHRoPSI2MDU4LjgwOTYiCiAgICAgICAgIGlkPSJyZWN0ODM1IgogICAgICAgICBzdHlsZT0iZmlsbDpub25lO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZmY4YzAwO3N0cm9rZS13aWR0aDoxMTMuMzMzO3N0cm9rZS1taXRlcmxpbWl0OjA7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiIC8+PC9nPjxlbGxpcHNlCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojZmY4MDAwO3N0cm9rZS13aWR0aDoxLjU0NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIGlkPSJwYXRoNDk4NSIKICAgICAgIGN4PSIzMS45MzM1ODQiCiAgICAgICBjeT0iLTc3NjYuODkwNiIKICAgICAgIHJ4PSIxOS4wNDI5NzEiCiAgICAgICByeT0iMTkuMTQwNjI1IiAvPjwvZz48L3N2Zz4K"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="parameters"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="4"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="Map">
                <Option name="angle" type="Map">
                  <Option name="active" type="bool" value="true"/>
                  <Option name="field" type="QString" value="rot_z"/>
                  <Option name="type" type="int" value="2"/>
                </Option>
              </Option>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{9dfb949f-f9e4-4483-9ffe-951f57b81d46}">
      <rule key="{33f82b7b-c115-4fa7-931a-e00e8f7eb120}" filter="&quot;cellule&quot; = 'SEPARATION DE RESEAU'">
        <settings calloutType="simple">
          <text-style fontKerning="1" fontSize="1.5" fontSizeUnit="MM" textColor="31,120,180,255" namedStyle="Bold Italic" multilineHeight="1" fontItalic="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontWordSpacing="0" fontWeight="75" forcedBold="0" fieldName="'point ouverture'" fontLetterSpacing="0" multilineHeightUnit="Percentage" fontUnderline="0" capitalization="0" textOrientation="horizontal" useSubstitutions="0" textOpacity="1" fontStrikeout="0" previewBkgrdColor="0,0,0,255" isExpression="1" legendString="Aa" blendMode="0" fontFamily="Arial" allowHtml="0" forcedItalic="0">
            <families/>
            <text-buffer bufferSize="1" bufferColor="255,255,255,255" bufferOpacity="1" bufferNoFill="1" bufferJoinStyle="128" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferBlendMode="0" bufferDraw="0" bufferSizeUnits="MM"/>
            <text-mask maskEnabled="0" maskType="0" maskJoinStyle="128" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskOpacity="1" maskedSymbolLayers="" maskSizeUnits="MM"/>
            <background shapeRadiiUnit="MM" shapeRotationType="0" shapeRadiiY="0" shapeOffsetY="0" shapeDraw="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeUnit="MM" shapeSizeX="0" shapeRadiiX="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeBorderWidthUnit="MM" shapeSVGFile="" shapeOffsetUnit="MM" shapeOpacity="1" shapeBorderColor="128,128,128,255" shapeSizeType="0" shapeBorderWidth="0" shapeOffsetX="0" shapeRotation="0" shapeJoinStyle="64">
              <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="markerSymbol" type="marker" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option name="name" type="QString" value=""/>
                    <Option name="properties"/>
                    <Option name="type" type="QString" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
                  <Option type="Map">
                    <Option name="angle" type="QString" value="0"/>
                    <Option name="cap_style" type="QString" value="square"/>
                    <Option name="color" type="QString" value="213,180,60,255"/>
                    <Option name="horizontal_anchor_point" type="QString" value="1"/>
                    <Option name="joinstyle" type="QString" value="bevel"/>
                    <Option name="name" type="QString" value="circle"/>
                    <Option name="offset" type="QString" value="0,0"/>
                    <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                    <Option name="offset_unit" type="QString" value="MM"/>
                    <Option name="outline_color" type="QString" value="35,35,35,255"/>
                    <Option name="outline_style" type="QString" value="solid"/>
                    <Option name="outline_width" type="QString" value="0"/>
                    <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                    <Option name="outline_width_unit" type="QString" value="MM"/>
                    <Option name="scale_method" type="QString" value="diameter"/>
                    <Option name="size" type="QString" value="2"/>
                    <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                    <Option name="size_unit" type="QString" value="MM"/>
                    <Option name="vertical_anchor_point" type="QString" value="1"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option name="name" type="QString" value=""/>
                      <Option name="properties"/>
                      <Option name="type" type="QString" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
              <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="fillSymbol" type="fill" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option name="name" type="QString" value=""/>
                    <Option name="properties"/>
                    <Option name="type" type="QString" value="collection"/>
                  </Option>
                </data_defined_properties>
                <layer class="SimpleFill" pass="0" enabled="1" locked="0">
                  <Option type="Map">
                    <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                    <Option name="color" type="QString" value="255,255,255,255"/>
                    <Option name="joinstyle" type="QString" value="bevel"/>
                    <Option name="offset" type="QString" value="0,0"/>
                    <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                    <Option name="offset_unit" type="QString" value="MM"/>
                    <Option name="outline_color" type="QString" value="128,128,128,255"/>
                    <Option name="outline_style" type="QString" value="no"/>
                    <Option name="outline_width" type="QString" value="0"/>
                    <Option name="outline_width_unit" type="QString" value="MM"/>
                    <Option name="style" type="QString" value="solid"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option name="name" type="QString" value=""/>
                      <Option name="properties"/>
                      <Option name="type" type="QString" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </background>
            <shadow shadowScale="100" shadowDraw="0" shadowOffsetDist="1" shadowOffsetUnit="MM" shadowUnder="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetAngle="135" shadowBlendMode="6" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowOpacity="0.69999999999999996" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusAlphaOnly="0" shadowColor="0,0,0,255"/>
            <dd_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" multilineAlign="0" wrapChar="" autoWrapLength="0" formatNumbers="0" placeDirectionSymbol="0" plussign="0" addDirectionSymbol="0" reverseDirectionSymbol="0" decimals="3"/>
          <placement quadOffset="4" rotationAngle="0" dist="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" lineAnchorPercent="0.5" polygonPlacementFlags="2" layerType="PointGeometry" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistanceUnits="MM" yOffset="0" maxCurvedCharAngleIn="25" repeatDistance="0" distMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" xOffset="0" geometryGenerator="" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" distUnits="MM" overrunDistanceUnit="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleOut="-25" offsetType="1" preserveRotation="1" lineAnchorClipping="0" offsetUnits="MM" placement="6" priority="5" centroidWhole="0" allowDegraded="0" overlapHandling="PreventOverlap" lineAnchorTextPoint="CenterOfText" centroidInside="0" geometryGeneratorType="PointGeometry" rotationUnit="AngleDegrees" geometryGeneratorEnabled="0" lineAnchorType="0"/>
          <rendering obstacleType="0" scaleMin="0" drawLabels="1" obstacle="1" fontLimitPixelSize="0" labelPerPart="0" obstacleFactor="1" zIndex="0" unplacedVisibility="0" minFeatureSize="0" scaleVisibility="0" limitNumLabels="0" scaleMax="0" upsidedownLabels="0" fontMaxPixelSize="10000" mergeLines="0" fontMinPixelSize="3" maxNumLabels="2000"/>
          <dd_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option name="anchorPoint" type="QString" value="pole_of_inaccessibility"/>
              <Option name="blendMode" type="int" value="0"/>
              <Option name="ddProperties" type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
              <Option name="drawToAllParts" type="bool" value="false"/>
              <Option name="enabled" type="QString" value="0"/>
              <Option name="labelAnchorPoint" type="QString" value="point_on_exterior"/>
              <Option name="lineSymbol" type="QString" value="&lt;symbol force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; is_animated=&quot;0&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; type=&quot;line&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;SimpleLine&quot; pass=&quot;0&quot; enabled=&quot;1&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;align_dash_pattern&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;capstyle&quot; type=&quot;QString&quot; value=&quot;square&quot;/>&lt;Option name=&quot;customdash&quot; type=&quot;QString&quot; value=&quot;5;2&quot;/>&lt;Option name=&quot;customdash_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;customdash_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;dash_pattern_offset&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;dash_pattern_offset_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;dash_pattern_offset_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;draw_inside_polygon&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;joinstyle&quot; type=&quot;QString&quot; value=&quot;bevel&quot;/>&lt;Option name=&quot;line_color&quot; type=&quot;QString&quot; value=&quot;60,60,60,255&quot;/>&lt;Option name=&quot;line_style&quot; type=&quot;QString&quot; value=&quot;solid&quot;/>&lt;Option name=&quot;line_width&quot; type=&quot;QString&quot; value=&quot;0.3&quot;/>&lt;Option name=&quot;line_width_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;offset&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;offset_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;ring_filter&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;trim_distance_end&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;trim_distance_end_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;trim_distance_end_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;trim_distance_start&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;trim_distance_start_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;trim_distance_start_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;tweak_dash_pattern_on_corners&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;use_custom_dash&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;width_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
              <Option name="minLength" type="double" value="0"/>
              <Option name="minLengthMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="minLengthUnit" type="QString" value="MM"/>
              <Option name="offsetFromAnchor" type="double" value="0"/>
              <Option name="offsetFromAnchorMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offsetFromAnchorUnit" type="QString" value="MM"/>
              <Option name="offsetFromLabel" type="double" value="0"/>
              <Option name="offsetFromLabelMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offsetFromLabelUnit" type="QString" value="MM"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option name="QFieldSync/action" type="QString" value="remove"/>
      <Option name="QFieldSync/cloud_action" type="QString" value="offline"/>
      <Option name="QFieldSync/photo_naming" type="QString" value="{&quot;photo&quot;: &quot;'DCIM/regards-boites-pi-boitier-de-raccordement_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.jpg'&quot;}"/>
      <Option name="dualview/previewExpressions" type="List">
        <Option type="QString" value="&quot;cellule&quot;"/>
      </Option>
      <Option name="embeddedWidgets/count" type="QString" value="0"/>
      <Option name="geopdf/includeFeatures" type="bool" value="false"/>
      <Option name="qgis2web/Interactive" type="QString" value="true"/>
      <Option name="qgis2web/Visible" type="QString" value="true"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory showAxis="1" opacity="1" backgroundColor="#ffffff" backgroundAlpha="255" penColor="#000000" diagramOrientation="Up" sizeType="MM" scaleBasedVisibility="0" minScaleDenominator="0" enabled="0" penAlpha="255" spacingUnitScale="3x:0,0,0,0,0,0" direction="0" spacingUnit="MM" minimumSize="0" rotationOffset="270" spacing="5" sizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" barWidth="5" width="15" lineSizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" maxScaleDenominator="1e+08" height="15" penWidth="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" strikethrough="0" bold="0" style="" italic="0"/>
      <attribute colorOpacity="1" field="" label="" color="#000000"/>
      <axisSymbol>
        <symbol force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" name="" type="line" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" enabled="1" locked="0">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" dist="0" placement="0" linePlacementFlags="18" priority="0" zIndex="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numero">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cellule">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="niveau">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="epaisseur">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="couleur">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rot_z">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ech_x">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ech_y">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ech_z">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="gg">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="intervention">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="actor">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option name="&lt;NULL>" type="QString" value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}"/>
              </Option>
              <Option type="Map">
                <Option name="Actif" type="QString" value="Actif"/>
              </Option>
              <Option type="Map">
                <Option name="Déposé" type="QString" value="Déposé"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="created_at">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="updated_at">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option name="allow_null" type="bool" value="true"/>
            <Option name="calendar_popup" type="bool" value="true"/>
            <Option name="display_format" type="QString" value="yyyy-MM-dd HH:mm:ss"/>
            <Option name="field_format" type="QString" value="yyyy-MM-dd HH:mm:ss"/>
            <Option name="field_iso_format" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="remarque">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photo">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option name="DefaultRoot" type="QString" value="P:/SIG/documents/rq_particulieres"/>
            <Option name="DocumentViewer" type="int" value="1"/>
            <Option name="DocumentViewerHeight" type="int" value="0"/>
            <Option name="DocumentViewerWidth" type="int" value="0"/>
            <Option name="FileWidget" type="bool" value="false"/>
            <Option name="FileWidgetButton" type="bool" value="false"/>
            <Option name="FileWidgetFilter" type="QString" value=""/>
            <Option name="PropertyCollection" type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties" type="invalid"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
            <Option name="RelativeStorage" type="int" value="1"/>
            <Option name="StorageAuthConfigId" type="QString" value=""/>
            <Option name="StorageMode" type="int" value="1"/>
            <Option name="StorageType" type="QString" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="fid" index="0" name=""/>
    <alias field="numero" index="1" name=""/>
    <alias field="insee" index="2" name="Numéro insee de la commune"/>
    <alias field="cellule" index="3" name="Cellule"/>
    <alias field="niveau" index="4" name=""/>
    <alias field="epaisseur" index="5" name=""/>
    <alias field="couleur" index="6" name=""/>
    <alias field="rot_z" index="7" name=""/>
    <alias field="ech_x" index="8" name=""/>
    <alias field="ech_y" index="9" name=""/>
    <alias field="ech_z" index="10" name=""/>
    <alias field="gg" index="11" name=""/>
    <alias field="intervention" index="12" name="Numéro de chantier"/>
    <alias field="actor" index="13" name="Chargé d'affaires"/>
    <alias field="etat" index="14" name=""/>
    <alias field="created_at" index="15" name=""/>
    <alias field="updated_at" index="16" name=""/>
    <alias field="remarque" index="17" name="Remarque"/>
    <alias field="photo" index="18" name="Photo"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="numero" expression="" applyOnUpdate="0"/>
    <default field="insee" expression=" @lumigis_insee " applyOnUpdate="1"/>
    <default field="cellule" expression="" applyOnUpdate="0"/>
    <default field="niveau" expression="" applyOnUpdate="0"/>
    <default field="epaisseur" expression="" applyOnUpdate="0"/>
    <default field="couleur" expression="" applyOnUpdate="0"/>
    <default field="rot_z" expression="" applyOnUpdate="0"/>
    <default field="ech_x" expression="" applyOnUpdate="0"/>
    <default field="ech_y" expression="" applyOnUpdate="0"/>
    <default field="ech_z" expression="" applyOnUpdate="0"/>
    <default field="gg" expression="" applyOnUpdate="0"/>
    <default field="intervention" expression=" @lumigis_intervention " applyOnUpdate="1"/>
    <default field="actor" expression=" @lumigis_technicien " applyOnUpdate="1"/>
    <default field="etat" expression="" applyOnUpdate="0"/>
    <default field="created_at" expression="" applyOnUpdate="0"/>
    <default field="updated_at" expression="" applyOnUpdate="0"/>
    <default field="remarque" expression="" applyOnUpdate="0"/>
    <default field="photo" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" field="fid" exp_strength="0" constraints="3"/>
    <constraint unique_strength="1" notnull_strength="1" field="numero" exp_strength="0" constraints="3"/>
    <constraint unique_strength="0" notnull_strength="0" field="insee" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="cellule" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="niveau" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="epaisseur" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="couleur" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="rot_z" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="ech_x" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="ech_y" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="ech_z" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="gg" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="intervention" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="actor" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="1" field="etat" exp_strength="0" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="1" field="created_at" exp_strength="0" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="0" field="updated_at" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="remarque" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="photo" exp_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="numero" desc="" exp=""/>
    <constraint field="insee" desc="" exp=""/>
    <constraint field="cellule" desc="" exp=""/>
    <constraint field="niveau" desc="" exp=""/>
    <constraint field="epaisseur" desc="" exp=""/>
    <constraint field="couleur" desc="" exp=""/>
    <constraint field="rot_z" desc="" exp=""/>
    <constraint field="ech_x" desc="" exp=""/>
    <constraint field="ech_y" desc="" exp=""/>
    <constraint field="ech_z" desc="" exp=""/>
    <constraint field="gg" desc="" exp=""/>
    <constraint field="intervention" desc="" exp=""/>
    <constraint field="actor" desc="" exp=""/>
    <constraint field="etat" desc="" exp=""/>
    <constraint field="created_at" desc="" exp=""/>
    <constraint field="updated_at" desc="" exp=""/>
    <constraint field="remarque" desc="" exp=""/>
    <constraint field="photo" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{45e90342-2723-48c0-a20d-31733bbef1c9}"/>
    <actionsetting action="[%photo%]" name="photos" type="5" shortTitle="photos" icon="" capture="0" isEnabledOnlyWhenEditable="0" id="{45e90342-2723-48c0-a20d-31733bbef1c9}" notificationMessage="">
      <actionScope id="Layer"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;cellule&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column name="cellule" type="field" width="330" hidden="0"/>
      <column name="niveau" type="field" width="290" hidden="0"/>
      <column name="epaisseur" type="field" width="-1" hidden="0"/>
      <column name="couleur" type="field" width="-1" hidden="0"/>
      <column name="rot_z" type="field" width="-1" hidden="0"/>
      <column name="ech_x" type="field" width="-1" hidden="0"/>
      <column name="ech_y" type="field" width="-1" hidden="0"/>
      <column name="ech_z" type="field" width="-1" hidden="0"/>
      <column name="created_at" type="field" width="-1" hidden="0"/>
      <column name="updated_at" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column name="gg" type="field" width="-1" hidden="0"/>
      <column name="remarque" type="field" width="-1" hidden="0"/>
      <column name="numero" type="field" width="-1" hidden="0"/>
      <column name="insee" type="field" width="-1" hidden="0"/>
      <column name="intervention" type="field" width="-1" hidden="0"/>
      <column name="actor" type="field" width="-1" hidden="0"/>
      <column name="fid" type="field" width="-1" hidden="0"/>
      <column name="etat" type="field" width="-1" hidden="0"/>
      <column name="photo" type="field" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">P:/SIG/gestion</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle labelColor="0,0,0,255" overrideLabelColor="0" overrideLabelFont="0">
      <labelFont description="MS Shell Dlg 2,8,-1,5,50,0,0,0,0,0" underline="0" strikethrough="0" bold="0" style="" italic="0"/>
    </labelStyle>
    <attributeEditorField showLabel="1" index="2" name="insee">
      <labelStyle labelColor="0,0,0,255" overrideLabelColor="0" overrideLabelFont="0">
        <labelFont description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" strikethrough="0" bold="0" style="" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="13" name="actor">
      <labelStyle labelColor="0,0,0,255" overrideLabelColor="0" overrideLabelFont="0">
        <labelFont description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" strikethrough="0" bold="0" style="" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="12" name="intervention">
      <labelStyle labelColor="0,0,0,255" overrideLabelColor="0" overrideLabelFont="0">
        <labelFont description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" strikethrough="0" bold="0" style="" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="14" name="etat">
      <labelStyle labelColor="0,0,0,255" overrideLabelColor="0" overrideLabelFont="0">
        <labelFont description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" strikethrough="0" bold="0" style="" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="17" name="remarque">
      <labelStyle labelColor="0,0,0,255" overrideLabelColor="0" overrideLabelFont="0">
        <labelFont description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" underline="0" strikethrough="0" bold="0" style="" italic="0"/>
      </labelStyle>
    </attributeEditorField>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="actor"/>
    <field editable="1" name="cellule"/>
    <field editable="1" name="couleur"/>
    <field editable="1" name="created_at"/>
    <field editable="1" name="dao_id"/>
    <field editable="1" name="ech_x"/>
    <field editable="1" name="ech_y"/>
    <field editable="1" name="ech_z"/>
    <field editable="1" name="ecp_intervention_id"/>
    <field editable="1" name="epaisseur"/>
    <field editable="1" name="etat"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="gg"/>
    <field editable="1" name="id"/>
    <field editable="1" name="insee"/>
    <field editable="1" name="intervention"/>
    <field editable="1" name="niveau"/>
    <field editable="1" name="numero"/>
    <field editable="1" name="photo"/>
    <field editable="1" name="remarque"/>
    <field editable="1" name="rot_z"/>
    <field editable="1" name="t_r_actor_id"/>
    <field editable="1" name="t_r_etat_id"/>
    <field editable="1" name="t_r_insee_commune_id"/>
    <field editable="1" name="updated_at"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="1" name="actor"/>
    <field labelOnTop="1" name="cellule"/>
    <field labelOnTop="0" name="couleur"/>
    <field labelOnTop="0" name="created_at"/>
    <field labelOnTop="0" name="dao_id"/>
    <field labelOnTop="0" name="ech_x"/>
    <field labelOnTop="0" name="ech_y"/>
    <field labelOnTop="0" name="ech_z"/>
    <field labelOnTop="1" name="ecp_intervention_id"/>
    <field labelOnTop="0" name="epaisseur"/>
    <field labelOnTop="1" name="etat"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="gg"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="1" name="insee"/>
    <field labelOnTop="1" name="intervention"/>
    <field labelOnTop="0" name="niveau"/>
    <field labelOnTop="0" name="numero"/>
    <field labelOnTop="1" name="photo"/>
    <field labelOnTop="1" name="remarque"/>
    <field labelOnTop="0" name="rot_z"/>
    <field labelOnTop="1" name="t_r_actor_id"/>
    <field labelOnTop="0" name="t_r_etat_id"/>
    <field labelOnTop="1" name="t_r_insee_commune_id"/>
    <field labelOnTop="0" name="updated_at"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="actor" reuseLastValue="0"/>
    <field name="cellule" reuseLastValue="0"/>
    <field name="couleur" reuseLastValue="0"/>
    <field name="created_at" reuseLastValue="0"/>
    <field name="dao_id" reuseLastValue="0"/>
    <field name="ech_x" reuseLastValue="0"/>
    <field name="ech_y" reuseLastValue="0"/>
    <field name="ech_z" reuseLastValue="0"/>
    <field name="ecp_intervention_id" reuseLastValue="0"/>
    <field name="epaisseur" reuseLastValue="0"/>
    <field name="etat" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="gg" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="insee" reuseLastValue="0"/>
    <field name="intervention" reuseLastValue="0"/>
    <field name="niveau" reuseLastValue="0"/>
    <field name="numero" reuseLastValue="0"/>
    <field name="photo" reuseLastValue="0"/>
    <field name="remarque" reuseLastValue="0"/>
    <field name="rot_z" reuseLastValue="0"/>
    <field name="t_r_actor_id" reuseLastValue="0"/>
    <field name="t_r_etat_id" reuseLastValue="0"/>
    <field name="t_r_insee_commune_id" reuseLastValue="0"/>
    <field name="updated_at" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"cellule"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
