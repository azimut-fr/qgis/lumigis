<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <location filename="../../dlg_util.py" line="166"/>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dlg_util.py" line="166"/>
        <source>All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../dlg_util.py" line="173"/>
        <source>Select a folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="117"/>
        <source>Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="68"/>
        <source>Lot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="162"/>
        <source>Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="14"/>
        <source>Extract kit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="124"/>
        <source>Market</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="54"/>
        <source>Technician</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="110"/>
        <source>City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="78"/>
        <source>Create enterprise kit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_xdep88.ui" line="148"/>
        <source>Description of intervention</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="636"/>
        <source>Import surveyed points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="38"/>
        <source>CSV file :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="98"/>
        <source>Metadata</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="171"/>
        <source>Sinfoni</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="248"/>
        <source>Georeferencing informations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="285"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Operator&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="304"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Surveying method&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="387"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;GPS engine&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="422"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Total station engine&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="436"/>
        <source>Detection information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="473"/>
        <source>Detection engine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="492"/>
        <source>Measuring technology</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog_csv.ui" line="575"/>
        <source>Operator</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="50"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="84"/>
        <source>Features</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="108"/>
        <source>Configuration file :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="168"/>
        <source>Usage :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="207"/>
        <source>Miscellaneous</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="268"/>
        <source>Report an issue</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="296"/>
        <source>Version used to save settings:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="324"/>
        <source>Help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="352"/>
        <source>Reset setttings to factory defaults</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="377"/>
        <source>Enable debug mode.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="386"/>
        <source>Debug mode (degraded performances)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Lumigis</name>
    <message>
        <location filename="../../lumigis.py" line="480"/>
        <source>Import surveyed points csv file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="408"/>
        <source>Extract enterprise kit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="980"/>
        <source>Market 2023-2026</source>
        <translation>Marché 2023-2026</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2664"/>
        <source>ALERT</source>
        <translation>ALERTE</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2664"/>
        <source>Directory already exists; its content will be erased.
Do you actually want to do it ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2843"/>
        <source>Unable to create enterprise kit </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2843"/>
        <source> in {}</source>
        <translation> dans {}</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2821"/>
        <source>New kit created </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2680"/>
        <source>Creating kit {} ...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2840"/>
        <source>Unable to create kit {}</source>
        <translation>Impossible de créer le kit {}</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2811"/>
        <source>Kit {} created</source>
        <translation>Kit {} créé</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2867"/>
        <source>Fatal error when creating kit </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2814"/>
        <source> in &lt;a href=&quot;{}&quot;&gt;{}&lt;/a&gt;</source>
        <translation> dans &lt;a href=&quot;{}&quot;&gt;{}&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2832"/>
        <source>Enterprise kit will not be created</source>
        <translation>Le kit entreprise ne sera pas créé</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="460"/>
        <source>Report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="626"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="521"/>
        <source>Place junction box on cable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="532"/>
        <source>Place manhole on cable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="541"/>
        <source>Place differential box on cable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="556"/>
        <source>Place bypass box on cable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="567"/>
        <source>Place uncertain box on cable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="578"/>
        <source>Place network separation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="593"/>
        <source>Place manhole on sheath</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="399"/>
        <source>Browse public lighting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="490"/>
        <source>New project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="505"/>
        <source>Design project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="607"/>
        <source>Show metadata</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="1554"/>
        <source>Unable to create new project </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="1560"/>
        <source>New project created </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="417"/>
        <source>Standardize kit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="428"/>
        <source>Import enterprise kit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="638"/>
        <source>Unable to initialize GUI for {} </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="930"/>
        <source>Filename {} is invalid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="939"/>
        <source>Error on querying metadata</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="956"/>
        <source>Lumigis - Import enterprise kit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2929"/>
        <source>Uncorrect enterprise kit filename</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3104"/>
        <source>Error on creating fdw server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3134"/>
        <source>Error on creating PostgreSQL schema</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3159"/>
        <source>Error on importing GPKG tables into PostGRESQL schema</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3190"/>
        <source>Error on getting INTERVENTION informations from metadata</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3522"/>
        <source>Error on updating vue_kit_sources from GPKG tables</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3578"/>
        <source>Error on updating vue_kit_points from GPKG table &apos;accessoires&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3621"/>
        <source>Error on updating vue_kit_etiquettes from GPKG table</source>
        <translation>Erreur lors de la mise à jour de vue_kit_etiquettes depuis la table GPKG 'etiquettes'</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3664"/>
        <source>Error on updating vue_kit_lignes from GPKG table &apos;fourreaux_et_reseaux_divers&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3763"/>
        <source>Kit {} imported in db</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3741"/>
        <source>Import failed. Database has not be updated</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="2153"/>
        <source>#methode_leve uncorrect or missing in filename {}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="469"/>
        <source>Report detection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="659"/>
        <source>The project {} is unusuable for works report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="676"/>
        <source>The project {} is unusuable for detection report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3251"/>
        <source>Error on updating armoires from GPKG table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3331"/>
        <source>Error on updating departs from GPKG table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3394"/>
        <source>Error on updating supports from GPKG table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3466"/>
        <source>Error on updating foyers from GPKG table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="451"/>
        <source>Generate definitive names</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3730"/>
        <source>Error on updating cables from GPKG table &apos;cables&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3784"/>
        <source>Error when generating &apos;armoires&apos; names</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3857"/>
        <source>Database has not be updated with definitive names</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3806"/>
        <source>Error when generating &apos;departs&apos; names</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3826"/>
        <source>Error when generating &apos;foyers&apos; names</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3846"/>
        <source>Error when generating &apos;supports&apos; names</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="3865"/>
        <source>Definitive names generated in database</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="440"/>
        <source>&apos;sheat_cable&apos; tool</source>
        <translation>Outil 'fourreau' en 'câble'</translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="1040"/>
        <source>Unable to start sheath-cable tool with metadata</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../lumigis.py" line="376"/>
        <source>Export grouped layers to geopackage</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LumigisDialog</name>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="14"/>
        <source>Gestion Eclairage Public</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="26"/>
        <source>Commun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="58"/>
        <source>Description intervention</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="76"/>
        <source>Supports</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="96"/>
        <source>Appareils</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1389"/>
        <source>Support</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="150"/>
        <source>Id support</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="173"/>
        <source>Hauteur totale (m)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="186"/>
        <source>RAL m&#xc3;&#xa2;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1402"/>
        <source>Marque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="212"/>
        <source>Forme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1687"/>
        <source>Nature</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1099"/>
        <source>Protection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="251"/>
        <source>Mati&#xc3;&#xa8;re</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="264"/>
        <source>Terre (Ohms)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="297"/>
        <source>Boitier de raccordement</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="370"/>
        <source>RAL illumination</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="383"/>
        <source>Prise illumination</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1030"/>
        <source>Diff&#xc3;&#xa9;rentiel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1052"/>
        <source>Sensibilit&#xc3;&#xa9; (mA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1065"/>
        <source>Calibre (A)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="465"/>
        <source>Massif</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="546"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="510"/>
        <source>Entraxe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="524"/>
        <source>Crosse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1467"/>
        <source>RAL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1834"/>
        <source>Etat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="841"/>
        <source>Adresse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="853"/>
        <source>Type de voie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="886"/>
        <source>Num&#xc3;&#xa9;ro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="899"/>
        <source>Nom de voie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1454"/>
        <source>Armoire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="703"/>
        <source>Id armoire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="759"/>
        <source>Poste</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="772"/>
        <source>Enveloppe pose</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="795"/>
        <source>V&#xc3;&#xa9;tust&#xc3;&#xa9;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="828"/>
        <source>Fermeture</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1428"/>
        <source>D&#xc3;&#xa9;part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="938"/>
        <source>Id d&#xc3;&#xa9;part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="984"/>
        <source>D&#xc3;&#xa9;signation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1112"/>
        <source>Allumage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1134"/>
        <source>Pilote</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1147"/>
        <source>R&#xc3;&#xa9;gime d&apos;allumage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1160"/>
        <source>Heure d&apos;allumage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1193"/>
        <source>Heure d&apos;extinction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1217"/>
        <source>Intensit&#xc3;&#xa9; phases</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1229"/>
        <source>Phase 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1262"/>
        <source>Phase 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1285"/>
        <source>Phase 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1700"/>
        <source>Foyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1324"/>
        <source>Id foyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1337"/>
        <source>R&#xc3;&#xa9;sistance choc (IK)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1350"/>
        <source>Hauteur feu (m)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1363"/>
        <source>Protection (IP)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1376"/>
        <source>Varistance (Ohm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1415"/>
        <source>Classe &#xc3;&#xa9;lectrique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1441"/>
        <source>Type vasque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1520"/>
        <source>Phase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1639"/>
        <source>Source</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1651"/>
        <source>Id source</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1674"/>
        <source>Temp&#xc3;&#xa9;rature de couleur (&#xc2;&#xb0;K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1713"/>
        <source>Photom&#xc3;&#xa9;trie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1726"/>
        <source>Culot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1739"/>
        <source>Flux (Lm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1752"/>
        <source>Intensit&#xc3;&#xa9; d&apos;alim. (mA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1765"/>
        <source>Puissance (W)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1778"/>
        <source>Localisation appareillage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1791"/>
        <source>Appareillage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1917"/>
        <source>Parafoudre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1930"/>
        <source>Abaissement_puissance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1969"/>
        <source>Remise &#xc3;&#xa0; Z&#xc3;&#xa9;ro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dialog.ui" line="1976"/>
        <source>Valider</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LumigisDockWidgetBase</name>
    <message>
        <location filename="../../gui/sdev88/lumigis_dockwidget.ui" line="14"/>
        <source>Eclairage public</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="48"/>
        <source>Choisir la COMMUNE ou l&apos;EPCI adh&#xc3;&#xa9;rent du SDEV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="71"/>
        <source>COG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="96"/>
        <source>&lt;p&gt;Code Officiel G&#xc3;&#xa9;ographique&lt;br/&gt;cf. https://www.insee.fr/fr/information/2560452&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dockwidget.ui" line="140"/>
        <source>Choisir l&apos;intervention (dossier SINFONI)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dockwidget.ui" line="159"/>
        <source>Description intervention</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="187"/>
        <source>&lt;p&gt;Cadre la carte sur tous les foyers command&#xc3;&#xa9;s par l&apos;armoire ou le d&#xc3;&#xa9;part courant, sinon centre la carte sur le mat&#xc3;&#xa9;riel courant&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="216"/>
        <source>Zoome au 1/200 sur le mat&#xc3;&#xa9;riel courant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="285"/>
        <source>Arborescence des mat&#xc3;&#xa9;riels reposant sur un support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="288"/>
        <source>Supports</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="323"/>
        <source>Arborescence des mat&#xc3;&#xa9;riels command&#xc3;&#xa9;s par une armoire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="326"/>
        <source>Appareils</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1608"/>
        <source>Support</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2705"/>
        <source>Diff&#xc3;&#xa9;rentiel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2720"/>
        <source>Sensibilit&#xc3;&#xa9; (mA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2750"/>
        <source>Calibre (A)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1052"/>
        <source>Massif</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="407"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1096"/>
        <source>Entraxe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="649"/>
        <source>Crosse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3045"/>
        <source>RAL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="943"/>
        <source>Adresse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1228"/>
        <source>Nom de voie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1141"/>
        <source>Type de voie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1174"/>
        <source>Num&#xc3;&#xa9;ro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1321"/>
        <source>Protection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3276"/>
        <source>Marque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="757"/>
        <source>Terre (Ohms)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="787"/>
        <source>Boitier de raccordement</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1299"/>
        <source>Forme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1344"/>
        <source>Mati&#xc3;&#xa8;re</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="825"/>
        <source>Hauteur totale (m)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="870"/>
        <source>RAL m&#xc3;&#xa2;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="927"/>
        <source>Prise illumination</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="940"/>
        <source>RAL illumination</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dockwidget.ui" line="1094"/>
        <source>Id support</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4044"/>
        <source>Nature</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4410"/>
        <source>Etat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1656"/>
        <source>Armoire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dockwidget.ui" line="1350"/>
        <source>Enveloppe pose</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2060"/>
        <source>V&#xc3;&#xa9;tust&#xc3;&#xa9;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dockwidget.ui" line="1414"/>
        <source>Fermeture</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="905"/>
        <source>Poste</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sdev88/lumigis_dockwidget.ui" line="1491"/>
        <source>Id armoire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="860"/>
        <source>Nbre de d&#xc3;&#xa9;parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1692"/>
        <source>D&#xc3;&#xa9;part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2780"/>
        <source>Allumage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2795"/>
        <source>Heure d&apos;allumage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2830"/>
        <source>Heure d&apos;extinction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2865"/>
        <source>R&#xc3;&#xa9;gime d&apos;allumage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2907"/>
        <source>Pilote</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2499"/>
        <source>Intensit&#xc3;&#xa9; phases</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2514"/>
        <source>Phase 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2553"/>
        <source>Phase 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2592"/>
        <source>Phase 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1536"/>
        <source>N&#xc2;&#xb0; d&#xc3;&#xa9;part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2631"/>
        <source>D&#xc3;&#xa9;signation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2284"/>
        <source>Foyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1592"/>
        <source>Phase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3102"/>
        <source>Classe &#xc3;&#xa9;lectrique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3219"/>
        <source>Hauteur feu (m)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3333"/>
        <source>Type vasque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3723"/>
        <source>Id foyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3501"/>
        <source>Protection (IP)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3546"/>
        <source>R&#xc3;&#xa9;sistance choc (IK)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3615"/>
        <source>Varistance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3644"/>
        <source>N&#xc2;&#xb0; phase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3664"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3669"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3674"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2254"/>
        <source>Source</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3888"/>
        <source>Id source</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3942"/>
        <source>Temp&#xc3;&#xa9;rature de couleur (&#xc2;&#xb0;K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3993"/>
        <source>Photom&#xc3;&#xa9;trie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2360"/>
        <source>Culot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4146"/>
        <source>Appareillage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4197"/>
        <source>Flux (Lm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4257"/>
        <source>Parafoudre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4273"/>
        <source>Abaissement_puissance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4305"/>
        <source>Localisation appareillage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4461"/>
        <source>Puissance (W)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4506"/>
        <source>Intensit&#xc3;&#xa9; d&apos;alim. (mA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2407"/>
        <source>Remise &#xc3;&#xa0; z&#xc3;&#xa9;ro des champs du formulaire courant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2410"/>
        <source>Remise &#xc3;&#xa0; Z&#xc3;&#xa9;ro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2463"/>
        <source>Valide la cr&#xc3;&#xa9;ation ou la modification du mat&#xc3;&#xa9;riel affich&#xc3;&#xa9; dans le formulaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2438"/>
        <source>Valider</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2466"/>
        <source>Install</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="14"/>
        <source>Report EP</source>
        <translation>Report EP</translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1949"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pos&#xc3;&#xa9; le&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="4563"/>
        <source>Remarque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="3825"/>
        <source>Symbole</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1158"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pos&#xc3;&#xa9;e le&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1621"/>
        <source>Voie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1633"/>
        <source>Type voie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1646"/>
        <source>Nom voie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1669"/>
        <source>VNOU</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1674"/>
        <source>AV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1679"/>
        <source>CAN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1684"/>
        <source>CAR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1689"/>
        <source>CASR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1694"/>
        <source>CRS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1699"/>
        <source>CTRE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1704"/>
        <source>EMBR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1709"/>
        <source>FRM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1714"/>
        <source>GR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1719"/>
        <source>HAM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1724"/>
        <source>MAIS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1729"/>
        <source>PLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1734"/>
        <source>PONT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1739"/>
        <source>PTR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1744"/>
        <source>QUA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1749"/>
        <source>QUAI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1754"/>
        <source>RPE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1759"/>
        <source>RTE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1764"/>
        <source>RUE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1769"/>
        <source>RUIS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1774"/>
        <source>SQ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1779"/>
        <source>VCHE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1784"/>
        <source>ZI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1789"/>
        <source>D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1794"/>
        <source>ZA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1799"/>
        <source>ESP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1804"/>
        <source>PASS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1809"/>
        <source>N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1814"/>
        <source>COL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1819"/>
        <source>MTE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1824"/>
        <source>VOIE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1829"/>
        <source>ART</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1834"/>
        <source>CF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1839"/>
        <source>ACH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1844"/>
        <source>VALL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1849"/>
        <source>BRE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1854"/>
        <source>BD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1859"/>
        <source>HLM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1864"/>
        <source>VTE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1869"/>
        <source>CLOS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1874"/>
        <source>CD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1879"/>
        <source>COUR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1884"/>
        <source>CHE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1889"/>
        <source>GAL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1894"/>
        <source>ALL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="1918"/>
        <source>N&#xc2;&#xb0; voie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2009"/>
        <source>Actif</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2014"/>
        <source>D&#xc3;&#xa9;pos&#xc3;&#xa9;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2019"/>
        <source>Virtuel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2024"/>
        <source>Inactif</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2029"/>
        <source>A poser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2034"/>
        <source>A remplacer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2039"/>
        <source>A d&#xc3;&#xa9;poser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2083"/>
        <source>Bon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2088"/>
        <source>Moyen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2093"/>
        <source>V&#xc3;&#xa9;tuste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2123"/>
        <source>Cl&#xc3;&#xa9;s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2128"/>
        <source>Triangle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2133"/>
        <source>Plat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2195"/>
        <source>Saillie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2200"/>
        <source>Encastr&#xc3;&#xa9;e</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2205"/>
        <source>Sur fa&#xc3;&#xa7;ade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2210"/>
        <source>Sur poteau de distribution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2215"/>
        <source>Autre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/report88/lumigis_dockwidget.ui" line="2970"/>
        <source>Technicien</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="14"/>
        <source>Lumigis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="392"/>
        <source>Prise(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="429"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nb&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="537"/>
        <source>Mat&#xc3;&#xa9;riel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1797"/>
        <source>Financeur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1842"/>
        <source>Depuis ...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="710"/>
        <source>Mise &#xc3;&#xa0; la terre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2012"/>
        <source>Finition</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="797"/>
        <source>Hauteur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="920"/>
        <source>Nom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1087"/>
        <source>Horloge :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1106"/>
        <source>Pos&#xc3;&#xa9;e le :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1227"/>
        <source>Phase(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1336"/>
        <source>Calibrage (A)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1375"/>
        <source>Intensit&#xc3;&#xa9;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1424"/>
        <source>Temporalit&#xc3;&#xa9;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1477"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dur&#xc3;&#xa9;e annuelle&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="1761"/>
        <source>Allumage/extinction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2158"/>
        <source>Puissance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2173"/>
        <source>Th&#xc3;&#xa9;orique (W)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2196"/>
        <source>Conso. (W)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2232"/>
        <source>Temp&#xc3;&#xa9;rature de couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/lumigis_dockwidget.ui" line="2325"/>
        <source>Pos&#xc3;&#xa9;e le</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LumigisDockWidgetProjet</name>
    <message>
        <location filename="../../gui/symbols_toolbox/symbols_toolbox_dockwidget.ui" line="14"/>
        <source>Project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/symbols_toolbox/symbols_toolbox_dockwidget.ui" line="25"/>
        <source>Armoire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/symbols_toolbox/symbols_toolbox_dockwidget.ui" line="2261"/>
        <source>&lt;p&gt;Cadre la carte sur tous les foyers command&#xc3;&#xa9;s par l&apos;armoire ou le d&#xc3;&#xa9;part courant, sinon centre la carte sur le mat&#xc3;&#xa9;riel courant&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/symbols_toolbox/symbols_toolbox_dockwidget.ui" line="264"/>
        <source>Support</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/symbols_toolbox/symbols_toolbox_dockwidget.ui" line="1112"/>
        <source>Foyer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/symbols_toolbox/symbols_toolbox_dockwidget.ui" line="1957"/>
        <source>Divers</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Lumigis_new_project</name>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="14"/>
        <source>New project</source>
        <translation>Nouveau projet</translation>
    </message>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="63"/>
        <source>New project folder :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="115"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="149"/>
        <source>Gaston Lagaffe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="168"/>
        <source>Address :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="187"/>
        <source>City :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="206"/>
        <source>Technician :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/sde22/new_project_dialog.ui" line="229"/>
        <source>project name</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>lumigis_about_form</name>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="14"/>
        <source>Lumigis - About</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="35"/>
        <source>Conception</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="47"/>
        <source>Lumigis plugin has been developed by Jean-Marie Arsac (Azimut). </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.sde22.fr/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2eb8e6;&quot;&gt;SDE22&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="122"/>
        <source>Evolution and contact</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contact &lt;a href=&quot;mailto:contact@azimut.fr?subject=[Lumigis]%20Ask%20aboutr%20the%20plugin&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;contact@azimut.fr&lt;/span&gt;&lt;/a&gt; for more details.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="63"/>
        <source>&lt;html&gt;&lt;a href=&quot;https://www.azimut.fr&quot;&gt;Azimut&lt;/a&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="95"/>
        <source>His funders are Syndicat D&#xc3;&#xa9;partemental d&apos;Electricit&#xc3;&#xa9; des Vosges and Syndicat D&#xc3;&#xa9;partemental d&apos;&#xc3;&#x89;nergie des C&#xc3;&#xb4;tes d&apos;Armor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/lumigis_about.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.sdev88.fr/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2eb8e6;&quot;&gt;SDEV&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
</context>
</TS>
