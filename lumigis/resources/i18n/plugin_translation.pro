FORMS        =  \
               ../../gui/dlg_settings.ui \
               ../../gui/lumigis_about.ui \
               ../../gui/sdev88/lumigis_dialog_csv.ui \
               ../../gui/sdev88/lumigis_dialog.ui \
               ../../gui/sdev88/lumigis_dialog_xdep88.ui \
               ../../gui/sdev88/lumigis_dockwidget.ui \
               ../../gui/report88/lumigis_dockwidget.ui \
               ../../gui/sde22/lumigis_dockwidget.ui \
               ../../gui/sde22/new_project_dialog.ui \
               ../../gui/symbols_toolbox/symbols_toolbox_dockwidget.ui \

SOURCES      =  \
               ../../lumigis.py \
               ../../csv_chantier.py \
               ../../gui/dlg_settings.py \
               ../../gui/lumigis_about.py \
               ../../gui/sdev88/lumigis_dialog_csv.py \
               ../../gui/sdev88/lumigis_dialog.py \
               ../../gui/sdev88/lumigis_dialog_xdep88.py \
               ../../gui/sdev88/lumigis_dockwidget.py \
               ../../gui/report88/lumigis_dockwidget.py \
               ../../gui/sde22/lumigis_dockwidget.py \
               ../../gui/sde22/new_project_dialog.py \
               ../../gui/splash/openproject_splash_dialog.py \
               ../../gui/symbols_toolbox/symbols_toolbox_dockwidget.py \
               ../../__about__.py \
               ../../csv_chantier.py \
               ../../db_privilege.py \
               ../../db_util.py \
               ../../dlg_util.py \
               ../../feature.py \
               ../../lumigis_layer.py \
               ../../lumigis_layers.py \
               ../../place_point_oriented_tool.py \
               ../../place_point_tool.py \
               ../../place_point_tool2.py \
               ../../zip_util.py \
               ../../__init__.py

TRANSLATIONS = lumigis_en.ts \
               lumigis_fr.ts
