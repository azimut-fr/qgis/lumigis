"""
/***************************************************************************
 CsvChantier

 Gestion des entêtes chantier
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import platform

from .toolbelt.log_handler import PlgLogger


class CsvChantier:
    """Classe decrivant les entêtes chantier des fichiers csv"""

    def __init__(
        self, filename, annee, n_lot, sinfoni, tmp_tablename="", tablename="chantiers"
    ):
        """Constructor.
        :param
        """
        self.nodename = platform.node().replace("-", "").replace(" ", "_")
        if not tmp_tablename:
            self.tmp_tablename = "tmp_chantier_{}".format(self.nodename)
        else:
            self.tmp_tablename = tmp_tablename
        self.tmp_points_tablename = "tmp_points_{}".format(self.nodename)
        self.tablename = tablename
        self.id = -1
        self.annee = annee
        self.n_lot = n_lot
        self.sinfoni = sinfoni
        self.fullfilename = filename
        self.filename = os.path.basename(filename).lower()
        self.entreprise_geo = ""
        self.operateur_geo = ""
        self.methode_leve = ""
        self.appareil_gps = ""
        self.appareil_station = ""
        self.entreprise_detection = ""
        self.operateur_detection = ""
        self.appareil_detection = ""
        self.technologie_mesure = ""
        self.nature_ouvrage = ""
        self.start_on = ""
        self.end_on = ""

    def create_tmp_tables(self, query):
        sql_string = (
            "CREATE UNLOGGED TABLE IF NOT EXISTS {} ("
            "nom_fichier text PRIMARY KEY,"
            "annee text,"
            "n_lot text,"
            "code_sinfoni text,"
            "entreprise_geo text,"
            "operateur_geo text,"
            "methode_leve text,"
            "appareil_gps text,"
            "appareil_station text,"
            "entreprise_detection text,"
            "operateur_detection text,"
            "appareil_detection text,"
            "technologie_mesure text,"
            "start_on date,"
            "end_on date)"
        ).format(self.tmp_tablename)
        if query.exec(sql_string):
            sql_string = "TRUNCATE TABLE {}".format(self.tmp_tablename)
            if query.exec(sql_string):
                sql_string = (
                    "CREATE UNLOGGED TABLE IF NOT EXISTS {} ("
                    "type_pt char(1),"
                    "nom text UNIQUE,"
                    "prof_leve real,"
                    "prof_calc real,"
                    "hdop real,"
                    "pdop real,"
                    "commentaire text,"
                    "surveyed_at timestamp with time zone,"
                    "sys_proj text,"
                    "methode_leve smallint,"
                    "geom geometry(PointZ, 3948))"
                ).format(self.tmp_points_tablename)
                query.exec(sql_string)

        error = query.lastError()
        if error.isValid():
            print(error.text())
            return False

        return True

    def insert_tmp_chantier(self, query):
        try:
            sql_string = (
                "INSERT INTO {} ("
                "nom_fichier,annee,n_lot,code_sinfoni,"
                "entreprise_geo,operateur_geo,"
                "methode_leve,appareil_gps,appareil_station,"
                "entreprise_detection,operateur_detection,"
                "appareil_detection,technologie_mesure"
                ") VALUES ("
                "?,?,?,?,?,?,?,?,?,?,?,?,?"
                ") ON CONFLICT (nom_fichier) DO UPDATE "
                "SET "
                "entreprise_geo=EXCLUDED.entreprise_geo,"
                "operateur_geo=EXCLUDED.operateur_geo,"
                "methode_leve=EXCLUDED.methode_leve,"
                "appareil_gps=EXCLUDED.appareil_gps,"
                "appareil_station=EXCLUDED.appareil_station,"
                "entreprise_detection=EXCLUDED.entreprise_detection,"
                "operateur_detection=EXCLUDED.operateur_detection,"
                "appareil_detection=EXCLUDED.appareil_detection,"
                "technologie_mesure=EXCLUDED.technologie_mesure "
            ).format(self.tmp_tablename)

            if query.prepare(sql_string):
                query.addBindValue(self.filename)
                query.addBindValue(self.annee)
                query.addBindValue(self.n_lot)
                query.addBindValue(self.sinfoni)
                query.addBindValue(self.entreprise_geo)
                query.addBindValue(self.operateur_geo)
                query.addBindValue(self.methode_leve)
                query.addBindValue(self.appareil_gps)
                query.addBindValue(self.appareil_station)
                query.addBindValue(self.entreprise_detection)
                query.addBindValue(self.operateur_detection)
                query.addBindValue(self.appareil_detection)
                query.addBindValue(self.technologie_mesure)

                query.exec()
                error = query.lastError()
                query.finish()
                if error.isValid():
                    print(error.text())

        except Exception as ex:
            print(str(ex))

    def reset_tmp_points(self, query):
        try:
            sql_string = "TRUNCATE TABLE {}".format(self.tmp_points_tablename)
            if query.exec(sql_string):
                return True
        except Exception as ex:
            print(str(ex))
        return False

    def insert_tmp_point(self, query, type_point, pt_fields):
        try:
            sql_string = (
                "INSERT INTO {} ("
                "type_pt,"
                "nom,"
                "prof_leve,"
                "surveyed_at,"
                "sys_proj,"
                "commentaire,"
                "pdop,"
                "hdop,"
                "methode_leve"
                ") VALUES ("
                "?,?,?,?,?,?,?,?,?"
                ") RETURNING nom"
            ).format(self.tmp_points_tablename)
            fields = pt_fields
            for i in range(len(pt_fields), 11):
                fields.append(None)

            for i in range(0, len(fields)):
                if fields[i] == "":
                    fields[i] = None

            if fields[4]:
                prof_leve = int(float(fields[4]) * 100.0)
            else:
                prof_leve = None

            hdop = fields[8]
            if hdop:
                hdop = hdop.replace(",", ".")
            pdop = fields[9]
            if pdop:
                pdop = hdop.replace(",", ".")
            methode_leve = fields[10] if fields[10] else "?"

            if query.prepare(sql_string):
                query.addBindValue(type_point)
                query.addBindValue(fields[0])
                query.addBindValue(prof_leve)
                query.addBindValue(fields[5])
                query.addBindValue(fields[6])
                query.addBindValue(fields[7])
                query.addBindValue(pdop)
                query.addBindValue(hdop)
                query.addBindValue(methode_leve)

                pt_nom = ""
                if query.exec():
                    if query.first():
                        pt_nom = query.value(0)
                error = query.lastError()
                query.finish()
                if error.isValid():
                    print(error.text())
                if pt_nom:
                    x = float(fields[1])
                    y = float(fields[2])
                    z = float(fields[3])
                    sql_string = (
                        "UPDATE {} SET geom = "
                        "ST_SetSRID("
                        "ST_MakePoint({}, {}, {}), 3948) "
                        "WHERE nom = '{}'"
                    ).format(
                        self.tmp_points_tablename,
                        fields[1],
                        fields[2],
                        fields[3],
                        pt_nom,
                    )
                    query.exec(sql_string)
                    error = query.lastError()
                    query.finish()
                    if error.isValid():
                        print(error.text())

        except Exception as ex:
            print(str(ex))

    def integrer_recolement(self, query, ecp_intervention_id, dlg):
        # metadonnées du chantier
        sql_string = (
            "INSERT INTO recol_chantier ("
            "nom_fichier,"
            "ecp_intervention_id,"
            "ent_topo,"
            "operateur,"
            "appareil,"
            "methode_leve,"
            "gps,"
            "appareil_detect,"
            "start_on,"
            "end_on,"
            "ent_detect,"
            "operateur_detect,"
            "technologie_mesure"
            ") SELECT "
            "? AS nom_fichier,"
            "? AS ecp_intervention_id,"
            "entreprise_geo,"
            "operateur_geo,"
            "appareil_station,"
            "methode_leve,"
            "appareil_gps,"
            "appareil_detection,"
            "start_on,"
            "end_on,"
            "entreprise_detection,"
            "operateur_detection,"
            "technologie_mesure "
            "FROM {} "
            "ON CONFLICT (nom_fichier) DO UPDATE "
            "SET "
            "ent_topo=EXCLUDED.ent_topo,"
            "operateur=EXCLUDED.operateur,"
            "appareil=EXCLUDED.appareil,"
            "methode_leve=EXCLUDED.methode_leve,"
            "gps=EXCLUDED.gps,"
            "appareil_detect=EXCLUDED.appareil_detect,"
            "start_on=EXCLUDED.start_on,"
            "end_on=EXCLUDED.end_on,"
            "ent_detect=EXCLUDED.ent_detect,"
            "operateur_detect=EXCLUDED.operateur_detect,"
            "technologie_mesure=EXCLUDED.technologie_mesure "
        ).format(self.tmp_tablename)
        id = 0
        if query.prepare(sql_string):
            query.addBindValue(self.filename)
            query.addBindValue(ecp_intervention_id)
            if query.exec():
                sql_string = "SELECT id FROM recol_chantier WHERE nom_fichier = ?"
                if query.prepare(sql_string):
                    query.addBindValue(self.filename)
                    if query.exec():
                        if query.next():
                            id = query.value(0)

        # purge des points du chantier
        if id:
            sql_string = "DELETE FROM recol_points WHERE recol_chantier_id = {}".format(
                id
            )
            query.exec(sql_string)
            sql_string = (
                "INSERT INTO recol_points ("
                "nom,"
                "recol_chantier_id,"
                "type_pt,"
                "prof_leve,"
                "hdop,"
                "pdop,"
                "surveyed_at,"
                "commentaire,"
                "methode_leve,"
                "geom"
                ") SELECT "
                "t.nom,"
                "{},"
                "t.type_pt,"
                "t.prof_leve,"
                "t.hdop,"
                "t.pdop,"
                "t.surveyed_at,"
                "t.commentaire,"
                "t.methode_leve,"
                "t.geom "
                "FROM {} t"
            ).format(id, self.tmp_points_tablename)
            nom = ""
            if query.exec(sql_string):
                sql_string = "Chantier {} intégré dans la BD".format(self.filename[:-4])
                PlgLogger.log(sql_string, log_level=3, push=True)
                if dlg:
                    dlg.label_point.setText(sql_string)
            error = query.lastError()
            query.finish()
            if error.isValid():
                print(error.text())
