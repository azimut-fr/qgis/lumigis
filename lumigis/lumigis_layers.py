"""
/***************************************************************************
 LumigisLayers
                                 A QGIS plugin
 Couches éclairage public
        begin                : 2022-12-04
        copyright            : (C) 2019 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsProject
from qgis.PyQt.QtCore import QRegularExpression
from qgis.PyQt.QtSql import *

from .lumigis_layer import LumigisLayer


class LumigisLayersConst:
    SUPPORT = 0
    ARMOIRE = 1
    DEPART = 2
    FOYER = 3
    SOURCE = 4
    CABLE = 5
    POINT = 6
    LIGNE = 7
    TEXTE = 8
    CERCLE = 9
    CONTOUR = 10
    # ------------
    FDP_POINT = 0
    FDP_LIGNE = 1
    FDP_TEXTE = 2
    FDP_CERCLE = 3
    FDP_CONTOUR = 4

    TREEVIEW_SUPPORTS = 0
    TREEVIEW_APPAREILS = 1


class LumigisLayers:
    """Classe decrivant les couches d'éclairage public"""

    def __init__(self, iface):
        """Constructor.
        :param iface: QGIS interface
        """
        self.iface = iface
        self._members = dict()
        self._luxLayers = []
        self._fdpLayers = []

    def append(self, layer_type, name, id):
        if layer_type == "LUX":
            self._luxLayers.append(LumigisLayer(name))
            layer = self._luxLayers[-1]
        elif layer_type == "FDP":
            self._fdpLayers.append(LumigisLayer(name))
            layer = self._fdpLayers[-1]
        layer.id = id
        return layer

    @property
    def lux_layers(self):
        return self._luxLayers

    @property
    def fdp_layers(self):
        return self._fdpLayers

    def filter_layers_with_insee(self, layers, insee_name, insee, refresh=True):
        for layer in layers:
            if layer is not None:
                LumigisLayer.filter_layer_with_insee(
                    layer, insee_name, insee, refresh, self.iface
                )
        if refresh:
            if not self.iface.mapCanvas().isCachingEnabled():
                self.iface.mapCanvas().refresh()

    def update_filter_with_insee(self, insee, refresh=True):
        layers_list_array = [self._luxLayers, self._fdpLayers]
        for layers_list in layers_list_array:
            for lumi_layer in layers_list:
                layer = None
                layers = QgsProject.instance().mapLayersByName(lumi_layer.name)
                if layers:
                    layer = layers[0]
                if layer is not None:
                    LumigisLayer.filter_layer_with_insee(
                        layer, lumi_layer.view_col_insee, insee, refresh, self.iface
                    )
        if refresh:
            if not self.iface.mapCanvas().isCachingEnabled():
                self.iface.mapCanvas().refresh()

    def active_layer_by_index(self, layers_type, layer_index):
        if layers_type == "LUX":
            lumigis_layers = self._luxLayers
        elif layers_type == "FDP":
            lumigis_layers = self._fdpLayers
        try:
            layers = QgsProject.instance().mapLayersByName(
                lumigis_layers[layer_index].name
            )
            if len(layers) == 1:
                layer = layers[0]
                if layer is not None:
                    self.iface.setActiveLayer(layer)
        except Exception:
            print(f"layer_type: {layers_type} layer_index: {layer_index}")
            print(f"size: {len(layers_type)}")
            pass

    def get_insee_from_lumi_layers_filter(self):
        insee = 0
        layers_list_array = [self._luxLayers, self._fdpLayers]
        for layers_list in layers_list_array:
            for lumi_layer in layers_list:
                layer = None
                layers = QgsProject.instance().mapLayersByName(lumi_layer.name)
                if layers:
                    layer = layers[0]
                    if layer is not None:
                        if layer.subsetString() is not None:
                            regex = QRegularExpression(
                                f"{lumi_layer.view_col_insee}=\\d{5}"
                            )
                            if regex.match(layer.subsetString()).hasMatch():
                                start_idx = len(lumi_layer.view_code_insee)
                                insee = regex.match(layer.subsetString()).captured()[
                                    start_idx : start_idx + 5 - 1
                                ]
        return insee

    def refresh_layer_by_name(self, layer_name):
        layer = QgsProject.instance().mapLayersByName(layer_name)[0]
        if layer is not None:
            # If caching is enabled, a simple canvas refresh might not be
            # sufficient to trigger a redraw and you must clear the cached
            # image for the layer
            if self.iface.mapCanvas().isCachingEnabled():
                layer.triggerRepaint()
            else:
                self.iface.mapCanvas().refresh()

    @staticmethod
    def layer_names(layer_type):
        if layer_type == "LUX":
            layer_array = [
                "armoires",
                "departs",
                "supports",
                "foyers",
                "sources",
                "cables",
                "points",
                "cercle",
                "lignes",
                "contour",
                "textes",
            ]
        elif layer_type == "FDP":
            layer_array = [
                "fdp_points",
                "fdp_cercles",
                "fdp_lignes",
                "fdp_contours",
                "fdp_textes",
            ]
        return layer_array

    def layers(self, layers_type):
        qgis_layers = []
        if layers_type == "LUX":
            lumigis_layers = self._luxLayers
        elif layers_type == "FDP":
            lumigis_layers = self._fdpLayers
        for lumi_layer in lumigis_layers:
            layers = QgsProject.instance().mapLayersByName(lumi_layer.name)
            if layers:
                qgis_layers.append(layers[0])
        return qgis_layers
