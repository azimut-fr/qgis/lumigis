"""
/***************************************************************************
 LumigisGlobal
                                 A QGIS plugin
 Variables globales Lumigis
        begin                : 2024-05-04
        copyright            : (C) 2024 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


class LumigisGlobal:
    def __init__(self):
        _event_filter = None

    @property
    def event_filter(self):
        return self._event_filter

    @event_filter.setter
    def event_filter(self, event_filter):
        self._event_filter = event_filter
