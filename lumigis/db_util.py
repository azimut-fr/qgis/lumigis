import configparser
import os

from qgis.core import Qgis, QgsDataSourceUri
from qgis.PyQt.QtCore import QRegularExpression, QVariant
from qgis.PyQt.QtSql import QSqlDatabase, QSqlQuery

from .db_privilege import DbPrivilege


class DbUtil:
    def __init__(self, iface, service_name: str = ""):
        """Constructor.
        :param iface: QGIS interface
        """
        self.iface = iface
        self._service_name = service_name
        if service_name[0:5] == "gpkg:":
            self._driver = "QSPATIALITE"
            self._dict_pgservices, self._dicoPgserviceConf = (None, None)
        else:
            self._driver = "QPSQL"
            # load pg_service.conf
            self._dicoPgserviceConf = {}
            if os.getenv("PGSYSCONFDIR"):
                self._dicoPgserviceConf = DbUtil.load_pgservice_conf(
                    os.path.join(os.getenv("PGSYSCONFDIR"), "pg_service.conf")
                )
                if self._dicoPgserviceConf:
                    self._dict_pgservices = self._dicoPgserviceConf.sections()
            else:
                self._dict_pgservices, self._dicoPgserviceConf = (None, None)
        self.db = QSqlDatabase(self._driver)
        self._schema = "public"
        self._query = None

    @property
    def pgservices(self):
        return self._dict_pgservices

    @property
    def pgservice_conf(self):
        return self._dicoPgserviceConf

    @property
    def query(self):
        return self._query

    @property
    def service_name(self):
        return self._service_name

    @service_name.setter
    def service_name(self, servicename):
        self._service_name = servicename

    @property
    def schema(self):
        return self._schema

    @schema.setter
    def schema(self, schema_name):
        self._schema = schema_name

    def geom_dimension(self, table_name, schema_name: str = None):
        coord_dimension = 0
        if self._driver == "QPSQL":
            sql_string = (
                "SELECT coord_dimension FROM geometry_columns "
                "WHERE f_table_name = ? AND f_table_schema = ?"
            )
            if self.query.prepare(sql_string):
                self.query.addBindValue(table_name)
                self.query.addBindValue(schema_name)
                if self._query.exec() and self.query.first():
                    coord_dimension = int(self.query.value(0))
                self.query.finish()
        elif self._driver == "QSPATIALITE":
            sql_string = (
                "SELECT z FROM gpkg_geometry_columns "
                f"WHERE table_name = '{table_name}"
            )
            if self.query.exec() and self.query.first():
                z = int(self.query.value(0))
                if z == 0:
                    coord_dimension = 2
                else:
                    coord_dimension = 3
            self.query.finish()

        return coord_dimension

    @staticmethod
    def load_pgservice_conf(fullfilepath):
        if os.path.exists(fullfilepath):
            dico_pgservice_conf = configparser.ConfigParser()
            dico_pgservice_conf.read(fullfilepath)
            return dico_pgservice_conf

    def id_by_insee_and_idnom(
        self, table_name, col_id, col_insee, insee, col_idnom, idnom
    ):
        if self.query.prepare(
            f"SELECT {col_id} FROM {table_name} "
            f" WHERE {col_insee} = ? AND {col_idnom} = ?"
        ):
            self.validate_and_add_bind_value(self.query, insee)
            self.validate_and_add_bind_value(self.query, idnom)
            if self.query.exec():
                if self.query.first():
                    if not (
                        isinstance(self.query.value(0), QVariant)
                        or self.query.value(0) is None
                    ):
                        return self.query.value(0)
        return 0

    def update_field(self, table_name, object_id, field_name, field_value):
        try:
            if self.query.prepare(
                f"UPDATE {table_name} SET {field_name} = ?" " WHERE numero = ?"
            ):
                self.validate_and_add_bind_value(self.query, field_value)
                self.validate_and_add_bind_value(self.query, object_id)
                return self.query.exec()
        except Exception as e:
            print(str(e))
            return False

    def update_geom(self, table_name, object_id, field_name, wkt, srid):
        try:
            sql_text = (
                f"UPDATE {table_name} SET {field_name} = "
                f"st_geomfromtext({wkt}, {srid}) "
                f"WHERE numero = {object_id}"
            )
            return self.query.exec(sql_text)
        except Exception as e:
            print(str(e))
            return False

    def db_service_name_from_layer(self, map_layer):
        try:
            qre = QRegularExpression("(service=')([a-z0-9_]+)(')")
            if qre.match(map_layer.publicSource()).hasMatch():
                my_match = qre.match(map_layer.publicSource())
                return my_match.captured(2)
        except Exception as ex:
            self.iface.messageBar().pushMessage(str(ex), "", Qgis.Warning)
            return ""

    def db_database_name(self, service_name: str = "smdev", map_layer=None):
        if service_name and self._dict_pgservices:
            if service_name in self._dict_pgservices:
                return self._dicoPgserviceConf[service_name]["dbname"]
        elif map_layer:
            qre = QRegularExpression("(dbname=')([a-z0-9_]+)(')")
            if qre.match(map_layer.publicSource()).hasMatch():
                my_match = qre.match(map_layer.publicSource())
                return my_match.captured(2)
        return ""

    def db_user_name(self, service_name: str = "smdev", map_layer=None):
        if service_name and self._dict_pgservices:
            if service_name in self._dict_pgservices:
                return self._dicoPgserviceConf[service_name]["user"]
        elif map_layer:
            qre = QRegularExpression("(user=')([a-z0-9_]+)(')")
            if qre.match(map_layer.publicSource()).hasMatch():
                my_match = qre.match(map_layer.publicSource())
                return my_match.captured(2)
        return ""

    @staticmethod
    def db_schema_name(map_layer):
        qre = QRegularExpression('(table=")([a-z0-9_]+)(".")([a-z0-9_]+)(")')
        if qre.match(map_layer.publicSource()).hasMatch():
            my_match = qre.match(map_layer.publicSource())
            return my_match.captured(2)
        return ""

    @staticmethod
    def db_table_name(map_layer):
        qre = QRegularExpression('(table=")([a-z0-9_]+)(".")([a-z0-9_]+)(")')
        if qre.match(map_layer.publicSource()).hasMatch():
            my_match = qre.match(map_layer.publicSource())
            return my_match.captured(4)
        return ""

    def close_connection_to_db(self):
        if self.db.isOpen():
            self.db.close()

    def connection_to_db(self):
        if self.db.isValid():
            if not self.db.isOpen():
                open_db = False
                try:
                    if self.pgservice_conf:
                        # string
                        self.db.setHostName(
                            self.pgservice_conf[self.service_name]["host"]
                        )
                        # string
                        self.db.setDatabaseName(
                            self.pgservice_conf[self.service_name]["dbname"]
                        )
                        # string
                        self.db.setUserName(
                            self.pgservice_conf[self.service_name]["user"]
                        )
                        # string
                        self.db.setPassword(
                            self.pgservice_conf[self.service_name]["password"]
                        )
                        # integer e.g. 5432
                        self.db.setPort(
                            int(self.pgservice_conf[self.service_name]["port"])
                        )
                        open_db = True
                    else:
                        self.db.setHostName("locahost")
                        service_name = self.service_name[5:]
                        uri = QgsDataSourceUri()
                        uri.setDatabase(service_name)
                        self.db.setDatabaseName(uri.database())
                        open_db = True

                except Exception as ex:
                    self.iface.messageBar().pushMessage(
                        "Service BD incorrect",
                        "Le service BD {} est introuvable ou mal défini".format(
                            str(ex)
                        ),
                        Qgis.Critical,
                    )
                    return False

                if open_db:
                    if self.db.open():
                        # set application_name with plugin name
                        self.db.exec("SET application_name = 'Lumigis'")
                    else:
                        error = self.db.lastError()
                        if error.isValid():
                            print(error.text())
                            ...

            # privilèges
            self.db_privilege = DbPrivilege(self.db)

            self._query = QSqlQuery(self.db)

            return True

    def insee(self):
        if self.db.databaseName()[-6] == ".gpkg":
            cog = self.db.databaseName()[-10:-5]
            return cog
        return 0

    def load_dictionary(self, dico, sql_string):
        if self.query and sql_string:
            if self.query.exec(sql_string):
                while self.query.next():
                    dico[self.query.value(0)] = self.query.value(1)
            else:
                print(self.query.lastError())

    def close(self):
        self.db.close()

    def validate_and_add_bind_value(self, query, value, empty_string=False):
        # print(type(value),value)
        if isinstance(value, str):
            if empty_string and value == "":
                query.addBindValue(value)
            elif value == "" or value == "NULL" or value is None:
                query.addBindValue(QVariant(QVariant.String))
            else:
                query.addBindValue(value)
        elif isinstance(value, bool):
            if value is None:
                query.addBindValue(QVariant(QVariant.String))
            else:
                if value:
                    query.addBindValue("oui")
                elif value:
                    query.addBindValue("non")
                else:
                    query.addBindValue(QVariant(QVariant.String))
        elif isinstance(value, int):
            if value is None:
                query.addBindValue(QVariant(QVariant.Int))
            else:
                query.addBindValue(value)
        elif isinstance(value, float):
            if value is None:
                query.addBindValue(QVariant(QVariant.Float))
            else:
                query.addBindValue(value)
        else:
            # print("TO DO ...",value)
            self.iface.messageBar().pushMessage(
                "dbutil.validate_and_add_bind_value", "TO DO... " + value, Qgis.Warning
            )
