#! python3  # noqa: E265
"""
/***************************************************************************
 export_utils.py
                                 A QGIS plugin

Utilities to export to geopackage
                              -------------------
        begin                : 2024-11-04
        git sha              : $Format:%H$
        copyright            : (C) 2024 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import os
import os.path
import shutil
from pathlib import Path

from qgis.core import Qgis, QgsMapLayerUtils, QgsProject, QgsVectorFileWriter
from qgis.PyQt.QtCore import QCoreApplication, QDir, Qt, QUrl
from qgis.PyQt.QtWidgets import QApplication

from .dlg_util import DlgUtil

# from .toolbelt.log_handler import PlgLogger
from .toolbelt.translator import PlgTranslator
from .utils import Utils

# from lumigis.toolbelt import PlgLogger, PlgOptionsManager


class ExportUtils:
    def __init__(self):
        pass

    @classmethod
    def export_visible_grouped_layers_to_geopackage(self, iface, group, export_path):
        self.iface = iface
        plg_translation_mngr = PlgTranslator()
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr
        if Utils.stringContainsVariable(export_path):
            root_path = Utils.expandVariablesInString(export_path)
        else:
            root_path = export_path
        mypath = Path(root_path)
        mypath.mkdir(parents=True, exist_ok=True)
        kit = group.name().lower()
        layers = []
        group_name = group.name()
        layer_list = [layer.name() for layer in group.children()]
        for name in layer_list:
            try:
                vLayer = QgsProject.instance().mapLayersByName(name)[0]
                layers.append(vLayer)
            except Exception as e:
                print(str(e))

        try:
            to_path = os.path.join(root_path, kit)
            if os.path.exists(to_path):
                ok = DlgUtil.show_alert(
                    self.dialog_xdep88,
                    self.tr("ALERT"),
                    self.tr(
                        "Directory already exists; its content will be "
                        "erased.\nDo you actually want to do it ?"
                    ),
                )
                if ok:
                    shutil.rmtree(to_path)
            else:
                ok = True
            if ok:
                QApplication.setOverrideCursor(Qt.BusyCursor)

                new_gpkg = os.path.join(root_path, f"{kit}.gpkg")
                context = QgsProject.instance().transformContext()
                options = QgsVectorFileWriter.SaveVectorOptions()
                options.driverName = "GPKG"
                options.includeZ = True
                if ExportUtils.features_are_selected_in_group(group):
                    options.onlySelectedFeatures = True
                else:
                    options.onlySelectedFeatures = False
                for lay in layers:
                    if lay.shortName():
                        options.layerName = lay.shortName()
                    else:
                        options.layerName = QgsMapLayerUtils.launderLayerName(
                            lay.name()
                        )
                    result = QgsVectorFileWriter.writeAsVectorFormatV3(
                        lay, new_gpkg, context, options
                    )
                    if result:
                        options.actionOnExistingFile = (
                            QgsVectorFileWriter.CreateOrOverwriteLayer
                        )

                # Connect to gpkg with ogr
                # md = QgsProviderRegistry.instance().providerMetadata("ogr")
                # conn = md.createConnection(new_gpkg, {})
                # md.disconnect()

                msg2 = self.tr(
                    ' in <a href="{}">{}</a>'.format(
                        QUrl.fromLocalFile(new_gpkg).toString(),
                        QDir.toNativeSeparators(new_gpkg),
                    ),
                    context="Lumigis",
                )
                self.iface.messageBar().pushMessage(
                    self.tr(
                        "Group {} exported to geopackage".format(group_name),
                        context="Lumigis",
                    ),
                    msg2,
                    Qgis.Success,
                )
            else:
                self.iface.messageBar().pushMessage(
                    self.tr(
                        "Group {} will not be exported to geopackage".format(
                            group_name
                        ),
                        context="Lumigis",
                    ),
                    Qgis.Warning,
                )

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr(
                    "Group {} will not be exported to geopackage".format(group_name),
                    context="Lumigis",
                ),
                str(e),
                Qgis.Warning,
            )

        while QApplication.overrideCursor() is not None:
            QApplication.restoreOverrideCursor()

    @classmethod
    def features_are_selected_in_group(cls, group):
        layers = []
        layer_names_list = [layer.name() for layer in group.children()]
        for name in layer_names_list:
            try:
                vLayer = QgsProject.instance().mapLayersByName(name)[0]
                layers.append(vLayer)
            except Exception as e:
                print(f"Error : {str(e)}")

        for lay in layers:
            if lay.selectedFeatureCount() > 0:
                return True
        return False
