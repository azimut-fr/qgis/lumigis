"""
/***************************************************************************
 DbPrivilege

 Gestion des privileges PostgreSQL
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.PyQt.QtSql import QSqlQuery


class DbPrivilege:
    """Classe decrivant les privilèges sur la BD"""

    def __init__(self, db):
        """Constructor.
        :param db: A QSqldatabase connection.
        """
        if db.isOpen():
            # Save conneciton to the database
            self.db = db

    def no_privilege(self):
        return "no_privilege"

    def delete_privilege(self):
        return "delete"

    def edit_privilege(self):
        return "edit"

    def view_privilege(self):
        return "view"

    def privilege(self, user_name, schema_name, table_name):
        if not self.db == None:
            query = QSqlQuery(self.db)
            sql_stmt = (
                "SELECT TRUE WHERE EXISTS (SELECT 1 FROM information_schema.table_privileges"
                " WHERE grantee = :user_name AND table_schema = :schema_name  AND table_name = :table_name and privilege_type LIKE 'DEL%')"
            )
            if query.prepare(sql_stmt):
                query.addBindValue(user_name)
                query.addBindValue(schema_name)
                query.addBindValue(table_name)
                if query.exec():
                    if query.first():
                        if query.value(0) == True:
                            return self.delete_privilege()

            sql_stmt = (
                "SELECT TRUE WHERE EXISTS (SELECT 1 FROM information_schema.table_privileges"
                " WHERE grantee = :user_name AND table_schema = :schema_name AND table_name = :table_name and privilege_type LIKE 'INS%')"
                " AND EXISTS (SELECT 1 FROM information_schema.table_privileges"
                " WHERE grantee = :user_name  AND table_schema = :schema_name AND table_name = :table_name and privilege_type LIKE 'UPD%')"
            )
            if query.prepare(sql_stmt):
                query.addBindValue(user_name)
                query.addBindValue(schema_name)
                query.addBindValue(table_name)
                query.addBindValue(user_name)
                query.addBindValue(schema_name)
                query.addBindValue(table_name)
                if query.exec():
                    if query.first():
                        if query.value(0) == True:
                            return self.edit_privilege()

            sql_stmt = (
                "SELECT TRUE WHERE EXISTS (SELECT 1 FROM information_schema.table_privileges"
                " WHERE grantee = :user_name  AND table_schema = :schema_name AND table_name = :table_name and privilege_type LIKE 'SEL%')"
            )
            if query.prepare(sql_stmt):
                query.addBindValue(user_name)
                query.addBindValue(schema_name)
                query.addBindValue(table_name)
                if query.exec():
                    if query.first():
                        if query.value(0) == True:
                            return self.view_privilege()

        return self.no_privilege()
