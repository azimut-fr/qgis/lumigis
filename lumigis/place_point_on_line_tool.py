"""
/***************************************************************************
 place_point_on_line.py

 PLace point tool class allows to place a symbol on map along a line


        begin                : 2023-04-02
        git sha              : $Format:%H$
        copyright            : (C) 2023 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

import math
import os

from qgis.core import QgsFeature, QgsGeometry, QgsPoint, QgsPointXY, QgsWkbTypes
from qgis.gui import QgsMapToolIdentify, QgsRubberBand
from qgis.PyQt.QtCore import Qt, QUuid
from qgis.PyQt.QtGui import QCursor, QPixmap
from qgis.PyQt.QtWidgets import QApplication

from lumigis.__about__ import DIR_PLUGIN_ROOT

# from .snapping_map_tool_emit_point import SnappingMapToolEmitPoint


class PlacePointOnLine(QgsMapToolIdentify):
    """
    La création d'un symbole aligné, se fait en 2 étapes:
    step 1: positionner le symbole sur une ligne
    step 2: accepter l'orientation du symbole (clic gauche) ou le retourner à 180° (clic droit)
    """

    def __init__(
        self,
        iface,
        dbutil,
        lumigislayers,
        object_type,
        table_name="",
        coord_dimension=3,
        rot90=False,
        symbol=None,
        line_layers=[],
    ):
        self.iface = iface
        self.db_util = dbutil
        self._lumigis_layers = lumigislayers
        self._layers = line_layers  # lumigislayers.layers("LUX")
        self.layer = iface.mapCanvas().currentLayer()
        self.currentTableName = table_name
        self.currentCoordDimension = coord_dimension
        self.currentObjectType = object_type
        self.rot90 = rot90
        self.symbol_name = symbol
        self.attr_values = dict()
        QgsMapToolIdentify.__init__(self, self.iface.mapCanvas())
        self.rubberBand = QgsRubberBand(
            self.iface.mapCanvas(), QgsWkbTypes.LineGeometry
        )
        self.rubberBand.setStrokeColor(Qt.red)
        self.rubberBand.setWidth(4)
        self.line_geometry = None
        if self.symbol_name:
            self.set_override_cursor_from_image(self.symbol_name)
        self.reset()

    def set_attr_value(self, key, value):
        self.attr_values[key] = value

    def set_attr_values_dict(self, values_dict):
        self.attr_values = values_dict

    def reset(self):
        self.startPoint = QgsPoint(0, 0, 0)
        self._z_rotation = 0
        self.step = 1

    def canvasReleaseEvent(self, event):
        if self.step == 1:
            if len(self._layers):
                results = self.identify(
                    event.x(), event.y(), self._layers, QgsMapToolIdentify.TopDownAll
                )
            else:
                results = self.identify(
                    event.x(), event.y(), [], QgsMapToolIdentify.TopDownAll
                )
            if results:
                for result in results:
                    layer = result.mLayer
                    if (
                        layer.name() == "Cables"
                        or layer.name() == "Fourreaux et reseaux divers"
                    ):
                        geom = result.mFeature.geometry()
                        closest_x = float(
                            result.mDerivedAttributes["X le plus proche"].replace(
                                ",", "."
                            )
                        )
                        closest_y = float(
                            result.mDerivedAttributes["Y le plus proche"].replace(
                                ",", "."
                            )
                        )
                        self.start_point = QgsPoint(closest_x, closest_y, 0)
                        distance = geom.lineLocatePoint(
                            QgsGeometry.fromPointXY(QgsPointXY(closest_x, closest_y))
                        )
                        rot_z = math.degrees(geom.interpolateAngle(distance))
                        if self.rot90:
                            rot_z -= 90
                            if rot_z < 0:
                                rot_z += 360
                        self._z_rotation = rot_z
                        rotation = (
                            self._z_rotation + 90 if self.rot90 else self._z_rotation
                        )
                        end_point = self.new_polar_point(self.start_point, 3, rotation)
                        self.showLine(self.start_point, end_point)
                        self.step = 2
                        break
        elif self.step == 2:
            if event.button() == Qt.RightButton:
                self._z_rotation += 180
                if self._z_rotation > 360:
                    self._z_rotation -= 360
                rotation = self._z_rotation + 90 if self.rot90 else self._z_rotation
                end_point = self.new_polar_point(self.start_point, 3, rotation)
                self.showLine(self.start_point, end_point)
            else:
                self.create_feature(self.start_point, True)
                self.rubberBand.reset()
                self.reset()
                self.iface.mapCanvas().unsetMapTool(self)

    def create_feature(self, point, refresh=False):
        ech_x = ech_y = ech_z = 1
        fea = QgsFeature(self.layer.fields())
        fea.setGeometry(point)
        # fea.setAttribute(0, 0)  # numero
        for key in self.attr_values.keys():
            fea.setAttribute(key, self.attr_values[key])

        fea.setAttribute("epaisseur", 0)  # epaisseur
        fea.setAttribute("couleur", 0)  # couleur
        fea.setAttribute("rot_z", self._z_rotation)
        fea.setAttribute("ech_x", ech_x)
        fea.setAttribute("ech_y", ech_y)
        fea.setAttribute("ech_z", ech_z)
        fea.setAttribute("etat", "Actif")  # etat
        fea.setAttribute("uid4", QUuid.createUuid().toString())
        provider = self.layer.dataProvider()
        provider.addFeatures([fea])
        if refresh:
            self._lumigis_layers.refresh_layer_by_name(self.layer.name())

    def set_override_cursor_from_image(self, symbol_name):
        icon_path = os.path.join(DIR_PLUGIN_ROOT, "resources/images", symbol_name)
        pixmap = QPixmap(icon_path)
        cursor = QCursor(pixmap.scaled(32, 32))
        QApplication.setOverrideCursor(cursor)

    def showLine(self, start_point, end_point):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)

        pt = QgsPointXY(start_point)
        self.rubberBand.addPoint(pt, False)
        # end point computing to draw a 3 m line
        pt = QgsPointXY(end_point)
        self.rubberBand.addPoint(pt, True)

        self.rubberBand.show()

    def new_polar_point(self, start_point, distance, azimuth):
        start_x, start_y = start_point.x(), start_point.y()
        azimuth_radians = math.radians(azimuth)
        x = start_x + distance * math.sin(azimuth_radians)
        y = start_y + distance * math.cos(azimuth_radians)
        return QgsPointXY(x, y)


"""
    def deactivate(self):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)
        QgsMapTool.deactivate(self)
        self.deactivated.emit()
"""
