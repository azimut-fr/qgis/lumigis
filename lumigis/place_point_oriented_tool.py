"""
/***************************************************************************
 point_oriented_tool.py

 Point oriented tool class allows to place a point with rotation on map


        begin                : 2022-12-22
        git sha              : $Format:%H$
        copyright            : (C) 2022 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

import math
import os

from qgis.core import QgsFeature, QgsGeometry, QgsPointXY, QgsWkbTypes
from qgis.gui import QgsMapToolEmitPoint, QgsRubberBand
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QCursor, QPixmap
from qgis.PyQt.QtWidgets import QApplication

from lumigis.__about__ import DIR_PLUGIN_ROOT


class PlacePointOriented(QgsMapToolEmitPoint):
    """
    Le placement d'un point orienté, se fait en 3 étapes:
    step 1: positionner le point
    step 2: orienter le point (clic droit place le point sans rotation)
    step 3: positionner définitivement le point
    """

    def __init__(
        self,
        iface,
        object_type,
        coord_dimension=2,
        rot90=False,
        symbol=None,
    ):
        self.iface = iface
        self.layer = iface.mapCanvas().currentLayer()
        self.currentCoordDimension = coord_dimension
        self.currentObjectType = object_type
        self.rot90 = rot90
        self.symbol_name = symbol
        self.attr_values = dict()
        QgsMapToolEmitPoint.__init__(self, self.iface.mapCanvas())
        self.rubberBand = QgsRubberBand(
            self.iface.mapCanvas(), QgsWkbTypes.LineGeometry
        )
        self.rubberBand.setStrokeColor(Qt.red)
        self.rubberBand.setWidth(4)
        self.line_geometry = None
        while QApplication.overrideCursor() is not None:
            QApplication.restoreOverrideCursor()
        if self.symbol_name:
            self.set_override_cursor_from_image(self.symbol_name)
        self.reset()

        self.reset()

    def set_attr_value(self, key, value):
        self.attr_values[key] = value

    def set_attr_values_dict(self, values_dict):
        self.attr_values = values_dict

    def reset(self):
        self.start_point = QgsPointXY(0, 0)
        self.isEmittingPoint = True
        self._z_rotation = 0
        self.step = 1

    # def canvasPressEvent(self, event):
    #    pass

    def canvasReleaseEvent(self, e):
        if e.button() == Qt.RightButton:
            self._z_rotation = 0
            if self.step == 1:
                self.iface.mapCanvas().unsetMapTool(self)
            elif self.step == 2:
                self.create_feature(self.start_point, True)
                self.rubberBand.reset()
                self.reset()
                self.iface.mapCanvas().unsetMapTool(self)
                # while QApplication.overrideCursor() is not None:
                #    QApplication.restoreOverrideCursor()
        else:
            if self.step == 1:
                self.start_point = self.toMapCoordinates(e.pos())
                self._z_rotation = 0
                self.step = 2
            elif self.step == 2:
                self._z_rotation = self.start_point.azimuth(
                    self.toMapCoordinates(e.pos())
                )
                self.create_feature(self.start_point, True)
                self.rubberBand.reset()
                self.reset()
                self.iface.mapCanvas().unsetMapTool(self)
                # while QApplication.overrideCursor() is not None:
                #    QApplication.restoreOverrideCursor()

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        if self.step == 2:
            self._z_rotation = self.start_point.azimuth(self.toMapCoordinates(e.pos()))
            self.showLine(self.start_point, self.toMapCoordinates(e.pos()))
        else:
            self.start_point = self.toMapCoordinates(e.pos())

    def create_feature(self, point, refresh=False):
        fea = QgsFeature(self.layer.fields())
        fea.setGeometry(QgsGeometry.fromPointXY(point))
        for key in self.attr_values.keys():
            fea.setAttribute(key, self.attr_values[key])

        fea.setAttribute("rot_z", self._z_rotation)
        provider = self.layer.dataProvider()
        provider.addFeatures([fea])
        if refresh:
            if self.iface.mapCanvas().isCachingEnabled():
                self.layer.triggerRepaint()
            else:
                self.iface.mapCanvas().refresh()

    def set_override_cursor_from_image(self, symbol_name):
        icon_path = os.path.join(DIR_PLUGIN_ROOT, "resources/images", symbol_name)
        pixmap = QPixmap(icon_path)
        cursor = QCursor(pixmap.scaled(32, 32))
        QApplication.setOverrideCursor(cursor)

    def showLine(self, start_point, end_point):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)

        pt = QgsPointXY(start_point)
        self.rubberBand.addPoint(pt, False)
        # end point computing to draw a 3 m line
        pt = QgsPointXY(end_point)
        self.rubberBand.addPoint(pt, True)

        self.rubberBand.show()

    def new_polar_point(self, start_point, distance, azimuth):
        start_x, start_y = start_point.x(), start_point.y()
        azimuth_radians = math.radians(azimuth)
        x = start_x + distance * math.sin(azimuth_radians)
        y = start_y + distance * math.cos(azimuth_radians)
        return QgsPointXY(x, y)

    def deactivate(self):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)
        while QApplication.overrideCursor() is not None:
            QApplication.restoreOverrideCursor()
        self.iface.mapCanvas().unsetMapTool(self)
        # QgsMapTool.deactivate(self)
        # self.deactivated.emit()
