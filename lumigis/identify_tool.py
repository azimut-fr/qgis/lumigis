"""
/***************************************************************************
 identify_tool.py

 Identify tool class allows to identify an entity


        begin                : 2023-04-02
        git sha              : $Format:%H$
        copyright            : (C) 2023 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

from qgis.core import QgsVectorLayer
from qgis.gui import QgsMapToolIdentify
from qgis.PyQt.QtCore import Qt

from .lumigis_config import LumigisConfig
from .lumigis_layers import LumigisLayersConst


class IdentifyTool(QgsMapToolIdentify):
    """
    identify feature
    """

    def __init__(self, iface, lumigislayers, index):
        super().__init__(iface.mapCanvas())
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.layer = self.iface.activeLayer()
        self._dialog_widget = None
        self._lumigis_layers = lumigislayers
        self._layers = lumigislayers.layers("LUX")
        self._index = index
        self._tree_widget = None
        self.iface.currentLayerChanged.connect(self.active_changed)

    @property
    def dialog_widget(self):
        return self._dialog_widget

    @dialog_widget.setter
    def dialog_widget(self, dialog):
        self._dialog_widget = dialog

    def active_changed(self, layer):
        if isinstance(layer, QgsVectorLayer) and layer.isSpatial():
            self.layer = layer

    def canvasReleaseEvent(self, event):
        results = None
        if len(self._layers):
            results = self.identify(
                event.x(), event.y(), self._layers, QgsMapToolIdentify.TopDownAll
            )
        else:
            results = self.identify(
                event.x(), event.y(), [], QgsMapToolIdentify.TopDownAll
            )
        if results:
            for result in results:
                idcolname = None
                self._tree_widget = None
                layer = result.mLayer
                if "ARMOIRE" in LumigisConfig.gis_config()["lux_layers"]:
                    if (
                        layer.name()
                        == LumigisConfig.gis_config()["lux_layers"]["ARMOIRE"]["name"]
                    ):
                        self._dialog_widget.tabWidgetSupportsAppareils.setCurrentIndex(
                            LumigisLayersConst.TREEVIEW_APPAREILS
                        )
                        self._tree_widget = self._dialog_widget.treeWidgetAppareils
                        idcolname = LumigisConfig.gis_config()["lux_layers"]["ARMOIRE"][
                            "view_col_idnom"
                        ]

                if (
                    not idcolname
                    and "SUPPORT" in LumigisConfig.gis_config()["lux_layers"]
                ):
                    if (
                        layer.name()
                        == LumigisConfig.gis_config()["lux_layers"]["SUPPORT"]["name"]
                    ):
                        self._dialog_widget.tabWidgetSupportsAppareils.setCurrentIndex(
                            LumigisLayersConst.TREEVIEW_SUPPORTS
                        )
                        self._tree_widget = self._dialog_widget.treeWidgetSupports
                        idcolname = LumigisConfig.gis_config()["lux_layers"]["SUPPORT"][
                            "view_col_idnom"
                        ]

                if (
                    not idcolname
                    and "FOYER" in LumigisConfig.gis_config()["lux_layers"]
                ):
                    if (
                        layer.name()
                        == LumigisConfig.gis_config()["lux_layers"]["FOYER"]["name"]
                    ):
                        self._dialog_widget.tabWidgetSupportsAppareils.setCurrentIndex(
                            LumigisLayersConst.TREEVIEW_APPAREILS
                        )
                        self._tree_widget = self._dialog_widget.treeWidgetAppareils
                        idcolname = LumigisConfig.gis_config()["lux_layers"]["FOYER"][
                            "view_col_idnom"
                        ]

                if self._tree_widget:
                    self._tree_widget.activateWindow()
                    self._tree_widget.setFocus()
                    idname = result.mFeature.attribute(idcolname)
                    items = self._tree_widget.findItems(
                        idname, Qt.MatchExactly or Qt.MatchRecursive
                    )

                    try:
                        self._tree_widget.setCurrentItem(items[0])
                        # self._dialog_widget.on_treewidgetappareils_expanded(
                        if items[0].parent():
                            items[0].parent().setExpanded(True)
                        else:
                            items[0].setExpanded(True)
                        # self.canvas.unsetMapTool(self)
                    except Exception as e:
                        print(str(e))
                        print("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv")
                        print(result.mLayer)
                        print(result.mDerivedAttributes)
                        print(
                            result.mFeature.id(),
                            result.mFeature.attribute(idcolname),
                        )
                        print(result.mFields)
                        print(result.mLabel)
                        print(result.mParams)
                        print(result.mAttributes)
                    else:
                        return
        return
