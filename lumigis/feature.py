"""
/***************************************************************************
 Feature
                                 A QGIS plugin
 Feature utils
        begin                : 2022-12-04
        copyright            : (C) 2022 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from qgis.core import QgsFeature, QgsVectorLayerUtils


class Feature:
    def __add_attribute_to_feature_dict(
        self, layer, feature_dict, dico, attribute_name
    ):
        if attribute_name in dico:
            i = layer.fields().indexFromName(attribute_name)
            if i >= 0:
                feature_dict.update({i: dico[attribute_name]})

    def create_feature_dict_attributes(self, layer, dico, pkey_name, field_names):
        feature_dict = dict()
        self.__add_attribute_to_feature_dict(layer, feature_dict, dico, pkey_name)
        for field_name in field_names:
            self.__add_attribute_to_feature_dict(layer, feature_dict, dico, field_name)
        return feature_dict

    def add_changed_attribute_to_feature_dict(
        self, layer, feature, feature_dict, old_dico, dico, attribute_name
    ):
        if attribute_name in dico:
            i = layer.fields().indexFromName(attribute_name)
            if i >= 0 and feature.attribute(i) != dico[attribute_name]:
                old_dico.update({i: feature.attribute(i)})
                feature_dict.update({i: dico[attribute_name]})

    def add_changed_attribute_value_to_feature_dict(
        self, layer, feature, feature_dict, old_dico, attribute_value, attribute_name
    ):
        i = layer.fields().indexFromName(attribute_name)
        if i >= 0 and feature.attribute(i) != attribute_value:
            old_dico.update({i: feature.attribute(i)})
            feature_dict.update({i: attribute_value})

    def create_feature_dict_changed_attributes(
        self, layer, feature, dico, pkey_name, field_names
    ):
        feature_dict = dict()
        old_values_dico = dict()
        self.__add_changed_attribute_to_feature_dict(
            layer, feature, feature_dict, old_values_dico, dico, pkey_name
        )
        for field_name in field_names:
            self.__add_changed_attribute_to_feature_dict(
                layer, feature, feature_dict, old_values_dico, dico, field_name
            )
        return feature_dict, old_values_dico

    def set_attribute_from_dictionary(self, feature, dico, attribute_name):
        if attribute_name in dico:
            feature.setAttribute(attribute_name, dico[attribute_name])

    def set_feature_attributes(self, feature, dico, field_names):
        for field_name in field_names:
            self.__set_attribute_from_dictionary(feature, dico, field_name)

    def add_or_update_en_cours_feature(
        self, layer, dico, geom, key_name, key_value, create: bool = True
    ):
        if key_value:
            i = layer.fields().indexFromName(key_name)
            if i >= 0:
                if QgsVectorLayerUtils.valueExists(layer, i, key_value):
                    fea_iter = layer.getFeatures(f"\"{key_name}\" = '{key_value}'")
                    if fea_iter.isValid():
                        feature = QgsFeature()
                        if fea_iter.nextFeature(feature):
                            self.update_feature(layer, feature, dico, geom)
                    fea_iter.close()
                else:
                    self.add_en_cours_feature(layer, dico, geom)

    def add_en_cours_feature(self, layer, dico, geom, refresh: bool = False):
        feature_dict = self.create_feature_dict_attributes(layer, dico)
        i = layer.fields().indexFromName("filename")
        if i >= 0:
            filename = os.path.basename(self.__dtdict.xml_filename)
            feature_dict.update({i: filename})
        i = layer.fields().indexFromName("login_name")
        if i >= 0:
            feature_dict.update({i: self.__login})
        i = layer.fields().indexFromName("t_r_actor_id")
        if i >= 0:
            feature_dict.update({i: self.__t_r_actor_id})
        feature = QgsVectorLayerUtils.createFeature(layer, geom, feature_dict)
        if True:
            (res, outFeats) = layer.dataProvider().addFeatures([feature])

        if refresh is True:
            # If caching is enabled, a simple canvas refresh might not be sufficient
            # to trigger a redraw and you must clear the cached image for the layer
            if self.iface.mapCanvas().isCachingEnabled():
                layer.triggerRepaint()
            else:
                self.iface.mapCanvas().refresh()

    def update_feature(self, layer, feature, dico, geom, refresh: bool = False):
        new_dico, old_dico = self.create_feature_dict_changed_attributes(
            layer, feature, dico
        )
        geom_changed = True if geom != feature.geometry() else False
        if len(new_dico) or geom_changed:
            layer.startEditing()
            if len(new_dico):
                layer.changeAttributeValues(feature.id(), new_dico, old_dico)
            if geom_changed:
                layer.changeGeometry(feature.id(), geom)
            layer.commitChanges()
            if refresh is True:
                # If caching is enabled, a simple canvas refresh might not be sufficient
                # to trigger a redraw and you must clear the cached image for the layer
                if self.iface.mapCanvas().isCachingEnabled():
                    layer.triggerRepaint()
                else:
                    self.iface.mapCanvas().refresh()
