"""
/***************************************************************************
 sheath_cable.py

 This class allows to convert a sheat to cable


        begin                : 2024-06-18
        git sha              : $Format:%H$
        copyright            : (C) 2024 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

from qgis.core import (
    Qgis,
    QgsExpressionContextUtils,
    QgsFeature,
    QgsPoint,
    QgsProject,
    edit,
)
from qgis.gui import QgsMapToolIdentify
from qgis.PyQt.QtCore import Qt, QUuid

# from .snapping_map_tool_emit_point import SnappingMapToolEmitPoint


class SheathCable(QgsMapToolIdentify):
    """
    La conversion d'un fourreau en câble (et réciproquement) se fait en 2 étapes:
    step 1: identifier une ligne "fourreau" (ou "cable")
    step 2: accepter la conversion du fourreau en câble (ou du câble en fourreau)
    """

    def __init__(
        self,
        iface,
        source_layers=[],
    ):
        self.iface = iface
        self._layers = source_layers
        self.layer = iface.mapCanvas().currentLayer()
        self.attr_values = dict()
        QgsMapToolIdentify.__init__(self, self.iface.mapCanvas())
        self.geometry = None
        self.reset()

    def set_attr_value(self, key, value):
        self.attr_values[key] = value

    def set_attr_values_dict(self, values_dict):
        self.attr_values = values_dict

    def reset(self):
        self.start_point = QgsPoint(0, 0, 0)
        self.step = 1

    def canvasReleaseEvent(self, event):
        if self.step == 1:
            if event.button() == Qt.RightButton:
                # TODO: restart action
                pass
            if len(self._layers):
                results = self.identify(
                    event.x(), event.y(), self._layers, QgsMapToolIdentify.TopDownAll
                )
            else:
                results = self.identify(
                    event.x(), event.y(), [], QgsMapToolIdentify.TopDownAll
                )
            if results:
                for result in results:
                    layer = result.mLayer
                    if (
                        layer.name() == "Cables"
                        or layer.name() == "Fourreaux et reseaux divers"
                    ):
                        if layer.name() == "Fourreaux et reseaux divers":
                            self.set_attr_value("type_pose", "Souterrain")
                        else:
                            layers = QgsProject.instance().mapLayersByName(
                                "Fourreaux et reseaux divers"
                            )
                            if len(layers) == 1:
                                self.layer = layers[0]
                                if self.layer is not None:
                                    self.iface.setActiveLayer(self.layer)

                        self.geometry = result.mFeature.geometry()
                        self.create_feature(self.geometry, True)
                        self.reset()
                        self.iface.mapCanvas().unsetMapTool(self)

    def create_feature(self, geom, refresh=False):
        with edit(self.layer):
            fea = QgsFeature(self.layer.fields())
            fea.setGeometry(geom)
            # fea.setAttribute(0, 0)  # numero
            try:
                for key in self.attr_values.keys():
                    fea.setAttribute(key, self.attr_values[key])
            except:
                pass

            fea.setAttribute("etat", "Actif")  # etat
            if self.layer.name() == "Cables":
                fea.setAttribute("uid4", QUuid.createUuid().toString())
                fea.setAttribute(
                    "acteur",
                    QgsExpressionContextUtils.globalScope().variable("user_full_name"),
                )
            else:
                fea.setAttribute("niveau", "ep_protection")
                fea.setAttribute("linestyle", "fourreau EP")
                fea.setAttribute(
                    "actor",
                    QgsExpressionContextUtils.globalScope().variable("user_full_name"),
                )
            (res, outFeats) = self.layer.dataProvider().addFeatures([fea])
            if res:
                fea = outFeats[0]
        try:
            with edit(self.layer):
                self.iface.openFeatureForm(self.layer, fea, True)
        except Exception as e:
            self.iface.messageBar().pushMessage(
                "Exception when opening form in sheath-cable tool",
                str(e),
                Qgis.Warning,
            )
        if refresh:
            if self.iface.mapCanvas().isCachingEnabled():
                self.layer.triggerRepaint()
            else:
                self.iface.mapCanvas().refresh()
        pass


"""
    def deactivate(self):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)
        QgsMapTool.deactivate(self)
        self.deactivated.emit()
"""
