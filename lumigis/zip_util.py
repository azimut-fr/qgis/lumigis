"""
/***************************************************************************
 ZipUtil
                                 A QGIS plugin
 Utils to zip file(s)/directory
        begin                : 2022-12-04
        copyright            : (C) 2022 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
from zipfile import ZipFile


class ZipUtil:
    """Classe decrivant une couche d'éclairage public"""

    @classmethod
    def is_file_to_clean(self, filename):
        if filename[-5:].lower() in [".xfdf"]:
            return True
        return False

    @classmethod
    def is_file_to_zip(self, filename):
        if filename[-4:].lower() in [
            ".cpg",
            ".csv",
            ".dbf",
            ".dgn",
            ".dxf",
            ".pdf",
            ".prj",
            ".qgs",
            ".qml",
            ".shp",
            ".shx",
            ".svg",
            ".txt",
        ]:
            return True
        if filename[-5:].lower() in [".gpkg"]:
            return True
        return False

    @classmethod
    def clean_files_in_dir(self, dir_name):
        try:
            # Iterate over all the files in directory
            for filename in os.listdir(dir_name):
                if self.is_file_to_clean(filename):
                    file_path = os.path.join(dir_name, filename)
                    os.remove(file_path)
        except Exception as e:
            print(str(e))

    @classmethod
    def zip_files_in_dir(
        self, dir_name, zip_file_name, del_zipped: bool = False, del_zip: bool = True
    ):
        if del_zip and os.path.exists(zip_file_name):
            os.remove(zip_file_name)
        with ZipFile(zip_file_name, "w") as zip_obj:
            wd = os.getcwd()
            os.chdir(dir_name)
            for root, dirs, files in os.walk(".\\"):
                for file in files:
                    if self.is_file_to_zip(file):
                        zip_obj.write(os.path.join(root, file))
                        if del_zipped:
                            os.remove(file)
                for directory in dirs:
                    zip_obj.write(os.path.join(root, directory))

            zip_obj.close()
            os.chdir(wd)
