"""
/***************************************************************************
 place_point_tool2.py

 PLace point tool class allows to place a symbol on map


        begin                : 2023-03-02
        git sha              : $Format:%H$
        copyright            : (C) 2023 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

import math
import os

from qgis.core import QgsGeometry, QgsPoint, QgsPointXY, QgsWkbTypes
from qgis.gui import QgsMapToolEmitPoint, QgsRubberBand
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QCursor, QPixmap
from qgis.PyQt.QtWidgets import QApplication

from lumigis.__about__ import DIR_PLUGIN_ROOT

from .fea_util import FeaUtil

# from .snapping_map_tool_emit_point import SnappingMapToolEmitPoint


class PlacePointTool(QgsMapToolEmitPoint):
    """
    La création d'un symbole orienté, se fait en 2 étapes:
    step 1: positionner le symbole
    step 2: orienter le symbole (clic droit place le symbole sans rotation)
    """

    def __init__(
        self,
        iface,
        dbutil,
        lumigislayers,
        object_type,
        table_name: str = "",
        coord_dimension: int = 3,
        object_id: int = 0,
        rot90: bool = False,
        symbol: str = None,
        refresh: bool = True,
    ):
        self.iface = iface
        self.db_util = dbutil
        self._lumigis_layers = lumigislayers
        self.layer = iface.mapCanvas().currentLayer()
        self.currentTableName = table_name
        self.currentCoordDimension = coord_dimension
        self.currentObjectType = object_type
        self.currentId = object_id
        self.currentFeature = self.layer.getFeature(object_id)
        self.rot90 = rot90
        self.symbol_name = symbol
        self.refresh = refresh
        self.attr_values = dict()
        QgsMapToolEmitPoint.__init__(self, self.iface.mapCanvas())
        self.rubberBand = QgsRubberBand(
            self.iface.mapCanvas(), QgsWkbTypes.LineGeometry
        )
        self.rubberBand.setStrokeColor(Qt.red)
        self.rubberBand.setWidth(4)
        self.startPoint = QgsPointXY(0, 0)
        self._z_rotation = 0.0
        self.line_geometry = None
        self.set_override_cursor_from_image(
            self._lumigis_layers.lux_layers[object_type].symbol_name
        )
        self.line_geometry = None
        if self.symbol_name:
            self.set_override_cursor_from_image(self.symbol_name)
        self.reset()

    def set_attr_value(self, key, value):
        self.attr_values[key] = value

    def set_attr_values_dict(self, values_dict):
        self.attr_values = values_dict

    def reset(self):
        self.startPoint = QgsPointXY(0, 0)
        self.isEmittingPoint = True
        self._z_rotation = 0
        self.step = 1

    def canvasReleaseEvent(self, e):
        if self.step == 1:
            self.startPoint = self.toMapCoordinates(e.pos())
            self._z_rotation = 0
            self.step = 2
            while QApplication.overrideCursor() is not None:
                QApplication.restoreOverrideCursor()
        elif self.step == 2:
            if e.button() == Qt.LeftButton:
                self._z_rotation = self.startPoint.azimuth(
                    self.toMapCoordinates(e.pos())
                )
            else:
                self._z_rotation = 0
            if self.currentCoordDimension == 3:
                geom = QgsGeometry(
                    QgsPoint(self.startPoint.x(), self.startPoint.y(), 0.0)
                )
            else:
                geom = QgsGeometry(QgsPointXY(self.startPoint))
            self.attr_values["rot_z"] = self._z_rotation
            FeaUtil.update_feature(
                self.iface.activeLayer(),
                self.currentFeature,
                self.attr_values,
                geom,
            )
            while QApplication.overrideCursor() is not None:
                QApplication.restoreOverrideCursor()
            self.rubberBand.reset()
            self.iface.mapCanvas().unsetMapTool(self)
            if self.refresh == True:
                # If caching is enabled, a simple canvas refresh might not be sufficient
                # to trigger a redraw and you must clear the cached image for the layer
                if self.iface.mapCanvas().isCachingEnabled():
                    self.iface.mapCanvas().currentLayer().triggerRepaint()
                else:
                    self.iface.mapCanvas().refresh()
        ...

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        if self.step == 2:
            end_point = self.toMapCoordinates(e.pos())
            self._z_rotation = self.startPoint.azimuth(end_point)
            self.showLine(self.startPoint, end_point)
        else:
            self.startPoint = self.toMapCoordinates(e.pos())

    def set_geom_from_point(self, point, button):
        pt = (
            self.iface.mapCanvas()
            .mapSettings()
            .mapToLayerCoordinates(self.iface.activeLayer(), point)
        )
        if self.currentCoordDimension == 3:
            self.currentFeature.setGeometry(QgsPoint(pt))
        else:
            self.currentFeature.setGeometry(pt)
        self.currentFeature.setAttribute("rot_z", self._z_rotation)

    def set_override_cursor_from_image(self, symbol_name):
        icon_path = os.path.join(DIR_PLUGIN_ROOT, "resources/images", symbol_name)
        pixmap = QPixmap(icon_path)
        cursor = QCursor(pixmap.scaled(32, 32))
        QApplication.setOverrideCursor(cursor)

    def showLine(self, start_point, end_point):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)

        pt = QgsPointXY(start_point)
        self.rubberBand.addPoint(pt, False)
        # end point computing to draw a 3 m line
        dx = end_point.x() - start_point.x()
        dy = end_point.y() - start_point.y()
        d2 = dx * dx + dy * dy
        k = math.sqrt(9.0 / d2)
        dx *= k
        dy *= k
        x = start_point.x() + dx
        y = start_point.y() + dy
        pt = QgsPointXY(x, y)
        self.rubberBand.addPoint(pt, True)

        self.rubberBand.show()


"""
    def deactivate(self):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)
        while QApplication.overrideCursor() is not None:
            QApplication.restoreOverrideCursor()
        self.iface.mapCanvas().unsetMapTool(self)
"""
