WITH rg AS (
SELECT
insee AS t_r_insee_commune_id,
id_foyer AS idnom,
f_idbyinseenom('ecp_supports',insee,id_support) AS ecp_support_id,
f_idbyinseenom('ecp_armoires',insee,id_armoire) AS ecp_armoire_id,
f_idbyinseenom('ecp_departs',insee,id_depart) AS ecp_depart_id,
f_refidbyall('t_r_etats',etat) AS t_r_etat_id,
marque,
type_vasque,
classe_electrique AS classe_elec,
degre_protection AS protection_ip,
resistance_choc AS resistance_ik,
ral,
f_refidbyall('t_r_cellule_foyers',cellule) AS t_r_cellule_foyer_id,
date_pose,
date_depose,
ecp_intervention_id,
remarque,
f_refidbyall('t_r_actors',acteur) AS t_r_actor_id,
hauteur_feu,
rot_z,
num_phase,
CASE WHEN varistance = 'oui' THEN true ELSE false END AS varistance,
uid4::uuid,
geom
 FROM {fdw_name}.foyers WHERE ecp_intervention_id = {ecp_intervention_id}
 )
 MERGE INTO ecp_foyers ecp
 USING rg ON rg.uid4 = ecp.uid4
 WHEN MATCHED THEN
 UPDATE SET
 (t_r_insee_commune_id,idnom,ecp_support_id,ecp_armoire_id,ecp_depart_id,t_r_etat_id,marque,type_vasque,classe_elec,protection_ip,resistance_ik,ral,t_r_cellule_foyer_id,date_pose,date_depose,ecp_intervention_id,remarque,t_r_actor_id,hauteur_feu,rot_z,num_phase,varistance,uid4,geom)
 =
 (rg.t_r_insee_commune_id,rg.idnom,rg.ecp_support_id,rg.ecp_armoire_id,rg.ecp_depart_id,rg.t_r_etat_id,rg.marque,rg.type_vasque,rg.classe_elec,rg.protection_ip,rg.resistance_ik,rg.ral,rg.t_r_cellule_foyer_id,rg.date_pose,rg.date_depose,rg.ecp_intervention_id,rg.remarque,rg.t_r_actor_id,rg.hauteur_feu,rg.rot_z,rg.num_phase,rg.varistance,rg.uid4,rg.geom)
 WHEN NOT MATCHED THEN
 INSERT
 (t_r_insee_commune_id,idnom,ecp_support_id,ecp_armoire_id,ecp_depart_id,t_r_etat_id,marque,type_vasque,classe_elec,protection_ip,resistance_ik,ral,t_r_cellule_foyer_id,date_pose,date_depose,ecp_intervention_id,remarque,t_r_actor_id,hauteur_feu,rot_z,num_phase,varistance,uid4,geom)
 VALUES
 (rg.t_r_insee_commune_id,rg.idnom,rg.ecp_support_id,rg.ecp_armoire_id,rg.ecp_depart_id,rg.t_r_etat_id,rg.marque,rg.type_vasque,rg.classe_elec,rg.protection_ip,rg.resistance_ik,rg.ral,rg.t_r_cellule_foyer_id,rg.date_pose,rg.date_depose,rg.ecp_intervention_id,rg.remarque,rg.t_r_actor_id,rg.hauteur_feu,rg.rot_z,rg.num_phase,rg.varistance,rg.uid4,rg.geom)
