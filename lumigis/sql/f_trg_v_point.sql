CREATE OR REPLACE FUNCTION sig.f_trg_update_vue_kit_points()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE

	v_id integer := NULL;

BEGIN

  IF (TG_OP = 'DELETE') THEN

    DELETE FROM sig_ecp.ecp_points WHERE id = OLD.numero;

    RETURN OLD;

  ELSIF (TG_OP = 'UPDATE') THEN

    UPDATE sig_ecp.ecp_points SET

      t_r_insee_commune_id = NEW.insee,

      cellule = NEW.cellule,

      niveau = NEW.niveau,

      epaisseur = NEW.epaisseur,

      couleur = NEW.couleur,

      rot_z = NEW.rot_z,

      ech_x = NEW.ech_x,

      ech_y = NEW.ech_y,

      ech_z = NEW.ech_z,

      gg = NEW.gg,

      remarque = NEW.remarque,

      photo = NEW.photo,

      t_r_etat_id = f_refidbyall('t_r_etats', NEW.etat),

      ecp_intervention_id = (SELECT id FROM ecp_interventions WHERE idnom = NEW.intervention),

      t_r_actor_id = f_refidbyall('t_r_actors', NEW.acteur),

      uid4 = NEW.uid4,

      geom = NEW.geom

	  WHERE id = NEW.numero;


    RETURN NEW;



  ELSIF (TG_OP = 'INSERT') THEN



    with ins as (

    INSERT INTO sig_ecp.ecp_points (

      t_r_insee_commune_id,

      cellule,

      t_r_etat_id,

      remarque,

      ecp_intervention_id,

      photo,

      rot_z,

      ech_x,

      ech_y,

      ech_z,

	    t_r_actor_id,

      uid4,

      geom

    ) values (

      NEW.insee,

      NEW.cellule,

      f_refidbyall('t_r_etats', NEW.etat),

      NEW.remarque,

      (SELECT id FROM ecp_interventions WHERE idnom = NEW.intervention),

      NEW.photo,

      NEW.rot_z,

      NEW.ech_x,

      NEW.ech_y,

      NEW.ech_z,

      f_refidbyall('t_r_actors', NEW.acteur),

      NEW.geom

    ) ON CONFLICT(id,numero) DO UPDATE SET

      t_r_insee_commune_id = EXCLUDED.t_r_insee_commune_id,

      cellule = EXCLUDED.cellule,

      t_r_etat_id = EXCLUDED.t_r_etat_id,

      remarque = EXCLUDED.remarque,

      ecp_intervention_id = EXCLUDED.ecp_intervention_id,

      photo = EXCLUDED.photo,

      rot_z = EXCLUDED.rot_z,

      ech_x = EXCLUDED.ech_x,

      ech_y = EXCLUDED.ech_y,

      ech_z = EXCLUDED.ech_z,

	    t_r_actor_id = EXCLUDED.t_r_actor_id,

      geom = EXCLUDED.geom

     returning id)

    select into v_id id from ins;


    RETURN NEW;



  END IF;



  RETURN NEW;

END;

$function$
