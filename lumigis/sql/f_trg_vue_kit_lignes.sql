CREATE OR REPLACE FUNCTION sig.f_trg_update_vue_kit_lignes()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE

        v_id integer := NULL;

BEGIN

  IF (TG_OP = 'DELETE') THEN

    DELETE FROM sig_ecp.ecp_lignes WHERE id = OLD.numero;

    RETURN OLD;

  ELSIF (TG_OP = 'UPDATE') THEN

    IF NEW.insee IS NULL THEN
        NEW.insee = OLD.insee;
    END IF;

    UPDATE sig_ecp.ecp_lignes SET

      t_r_insee_commune_id = NEW.insee,

      linestyle = NEW.linestyle,

      niveau = NEW.niveau,

      t_r_etat_id = f_refidbyall('t_r_etats', NEW.etat),

      ecp_intervention_id = (SELECT id FROM ecp_interventions WHERE idnom = NEW.intervention),

      t_r_actor_id = f_refidbyall('t_r_actors', NEW.actor),

      geom = NEW.geom

          WHERE id = NEW.numero;


    RETURN NEW;



  ELSIF (TG_OP = 'INSERT') THEN



    with ins as (

    INSERT INTO sig_ecp.ecp_lignes (

      t_r_insee_commune_id,

      linestyle,

      t_r_etat_id,

      niveau,

      ecp_intervention_id,

          t_r_actor_id,

      geom

    ) values (

      NEW.insee,

      NEW.linestyle,

      f_refidbyall('t_r_etats', NEW.etat),

      NEW.niveau,

      (SELECT id FROM ecp_interventions WHERE idnom = NEW.intervention),

      f_refidbyall('t_r_actors', NEW.actor),

      NEW.geom

    )  ON CONFLICT (t_r_insee_commune_id,niveau,linestyle,geom) DO UPDATE SET
    t_r_etat_id = f_refidbyall('t_r_etats', NEW.etat),
    ecp_intervention_id = (SELECT id FROM ecp_interventions WHERE idnom = NEW.intervention),
    t_r_actor_id = f_refidbyall('t_r_actors', NEW.actor)

    returning id)

    select into v_id id from ins;


    RETURN NEW;



  END IF;



  RETURN NEW;

END;

$function$
