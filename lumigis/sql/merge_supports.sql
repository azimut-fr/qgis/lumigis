WITH rg AS (SELECT
insee AS t_r_insee_commune_id,
id_support AS idnom,
f_refidbyall('t_r_etats',etat) AS t_r_etat_id,
nature,
terre,
CASE WHEN boitier = 'oui' THEN true ELSE false END AS boitier,
f_refidbyall('t_r_type_protections',type_protection) AS t_r_type_protection_id,
calibre_diff,
sensibilite_diff,
f_refidbyall('t_r_support_formes',forme) AS t_r_support_forme_id,
marque,
matiere,
hauteur_totale,
f_refidbyall('t_r_type_massifs',type_massif) AS t_r_type_massif_id,
f_refidbyall('t_r_entraxe_massifs',entraxe_massif) AS t_r_entraxe_massif_id,
ral_mat,
f_refidbyall('t_r_type_crosses',type_crosse) AS t_r_type_crosse_id,
ral_crosse,
CASE WHEN prise_illumination = 'oui' THEN true ELSE false END AS prise_illumination,
ral_illum,
f_refidbyall('t_r_cellule_supports',cellule) AS t_r_cellule_support_id,
ecp_intervention_id,
date_pose,
date_depose,
remarque,
f_refidbyall('t_r_actors', acteur) AS t_r_actor_id,
rot_z,
uid4::uuid,
geom
 FROM {fdw_name}.supports WHERE ecp_intervention_id = {ecp_intervention_id}
 )
 MERGE INTO ecp_supports ecp USING rg ON rg.uid4 = ecp.uid4
 WHEN MATCHED THEN
 UPDATE SET
 (t_r_insee_commune_id,idnom,t_r_etat_id,nature,terre,boitier,t_r_type_protection_id,calibre_diff,sensibilite_diff,t_r_support_forme_id,marque,matiere,hauteur_totale,t_r_type_massif_id,t_r_entraxe_massif_id,ral_mat, t_r_type_crosse_id,ral_crosse,prise_illumination,ral_illum,t_r_cellule_support_id,ecp_intervention_id,date_pose,date_depose,remarque,t_r_actor_id,rot_z,uid4,geom)
 =
 (rg.t_r_insee_commune_id,rg.idnom,rg.t_r_etat_id,rg.nature,rg.terre,rg.boitier,rg.t_r_type_protection_id,rg.calibre_diff,rg.sensibilite_diff,rg.t_r_support_forme_id,rg.marque,rg.matiere,rg.hauteur_totale,rg.t_r_type_massif_id,rg.t_r_entraxe_massif_id,rg.ral_mat,rg. t_r_type_crosse_id,rg.ral_crosse,rg.prise_illumination,rg.ral_illum,rg.t_r_cellule_support_id,rg.ecp_intervention_id,rg.date_pose,rg.date_depose,rg.remarque,rg.t_r_actor_id,rg.rot_z,rg.uid4,rg.geom)
 WHEN NOT MATCHED THEN
 INSERT
  (t_r_insee_commune_id,idnom,t_r_etat_id,nature,terre,boitier,t_r_type_protection_id,calibre_diff,sensibilite_diff,t_r_support_forme_id,marque,matiere,hauteur_totale,t_r_type_massif_id,t_r_entraxe_massif_id,ral_mat, t_r_type_crosse_id,ral_crosse,prise_illumination,ral_illum,t_r_cellule_support_id,ecp_intervention_id,date_pose,date_depose,remarque,t_r_actor_id,rot_z,uid4,geom)
 VALUES
 (rg.t_r_insee_commune_id,rg.idnom,rg.t_r_etat_id,rg.nature,rg.terre,rg.boitier,rg.t_r_type_protection_id,rg.calibre_diff,rg.sensibilite_diff,rg.t_r_support_forme_id,rg.marque,rg.matiere,rg.hauteur_totale,rg.t_r_type_massif_id,rg.t_r_entraxe_massif_id,rg.ral_mat,rg. t_r_type_crosse_id,rg.ral_crosse,rg.prise_illumination,rg.ral_illum,rg.t_r_cellule_support_id,rg.ecp_intervention_id,rg.date_pose,rg.date_depose,rg.remarque,rg.t_r_actor_id,rg.rot_z,rg.uid4,rg.geom)
