CREATE OR REPLACE FUNCTION sig.f_trg_update_vue_kit_armoires()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE

        v_id integer := NULL;

BEGIN

  IF (TG_OP = 'DELETE') THEN

    DELETE FROM sig_ecp.ecp_armoires WHERE id = OLD.numero;

    RETURN OLD;

  ELSIF (TG_OP = 'UPDATE') THEN

    IF NEW.insee IS NULL THEN
        NEW.insee = OLD.insee;
    END IF;
    UPDATE sig_ecp.ecp_armoires SET

      t_r_insee_commune_id = NEW.insee,

      idnom = NEW.id_armoire,

      numero_voie = NEW.numero_voie,

      t_r_type_voie_id = f_refidbyall('t_r_type_voies', NEW.type_voie),

      t_r_nom_voie_id = f_refidbyall('t_r_nom_voies', NEW.nom_voie),

      t_r_etat_id = f_refidbyall('t_r_etats', NEW.etat),

      t_r_vetuste_id = f_refidbyall('t_r_vetustes', NEW.vetuste),

      t_r_fermeture_id = f_refidbyall('t_r_fermetures', NEW.fermeture),

      poste = NEW.enveloppe_poste_amont,

      t_r_enveloppe_pose_id = f_refidbyall('t_r_enveloppe_poses', NEW.enveloppe_pose),

      nbre_depart = NEW.nbre_depart,

      t_r_cellule_armoire_id = f_refidbyall('t_r_cellule_armoires', NEW.cellule),

      date_pose = NEW.date_pose,

      date_depose = NEW.date_depose,

      date_intervention = NEW.date_action,

      remarque = NEW.remarque,

      mslink = NEW.mslink,

      mapid = NEW.mapid,

      ecp_intervention_id = NEW.ecp_intervention_id,

      created_at = NEW.created_at,

      updated_at = NEW.updated_at,

      photo = NEW.photo,

      schema_elec = NEW.schema_elec,

      rot_z = NEW.rot_z,

      t_r_actor_id = f_refidbyall('t_r_actors', NEW.acteur),

      geom = NEW.geom

          WHERE id = NEW.numero;



          IF NOT NEW.date_action IS NULL AND (NEW.date_action IS DISTINCT FROM OLD.date_action OR NEW.desc_action IS DISTINCT FROM OLD.desc_action) THEN

                  INSERT INTO ecp_actions

                  (tablename,id,date_action,desc_action)

                   VALUES

                  ('ecp_armoires',NEW.numero,NEW.date_action, NEW.desc_action);

          END IF;



    RETURN NEW;



  ELSIF (TG_OP = 'INSERT') THEN



    with ins as (

    INSERT INTO sig_ecp.ecp_armoires (

      t_r_insee_commune_id,

      idnom,

      numero_voie,

      t_r_type_voie_id,

      t_r_nom_voie_id,

      t_r_etat_id,

      t_r_vetuste_id,

      t_r_fermeture_id,

      poste,

      t_r_enveloppe_pose_id,

      nbre_depart,

      t_r_cellule_armoire_id,

      date_pose,

      date_depose,

      date_intervention,

      remarque,

      mslink,

      mapid,

      ecp_intervention_id,

      photo,

      schema_elec,

      rot_z,

          t_r_actor_id,

      geom,

      uid4

    ) values (

      NEW.insee,

      NEW.id_armoire,

      NEW.numero_voie,

      f_refidbyall('t_r_type_voies', NEW.type_voie),

      f_refidbyall('t_r_nom_voies', NEW.nom_voie),

      f_refidbyall('t_r_etats', NEW.etat),

      f_refidbyall('t_r_vetustes', NEW.vetuste),

      f_refidbyall('t_r_fermetures', NEW.fermeture),

      NEW.enveloppe_poste_amont,

      f_refidbyall('t_r_enveloppe_poses', NEW.enveloppe_pose),

      NEW.nbre_depart,

      f_refidbyall('t_r_cellule_armoires', NEW.cellule),

      NEW.date_pose,

      NEW.date_depose,

      NEW.date_action,

      NEW.remarque,

      NEW.mslink,

      NEW.mapid,

      NEW.ecp_intervention_id,

      NEW.photo,

      NEW.schema_elec,

      NEW.rot_z,

      f_refidbyall('t_r_actors', NEW.acteur),

      NEW.geom,

      NEW.uid4

    ) ON CONFLICT(t_r_insee_commune_id,idnom) DO UPDATE SET

      t_r_insee_commune_id = EXCLUDED.t_r_insee_commune_id,

      idnom = EXCLUDED.idnom,

      numero_voie = EXCLUDED.numero_voie,

      t_r_type_voie_id = EXCLUDED.t_r_type_voie_id,

      t_r_nom_voie_id = EXCLUDED.t_r_nom_voie_id,

      t_r_etat_id = EXCLUDED.t_r_etat_id,

      t_r_vetuste_id = EXCLUDED.t_r_vetuste_id,

      t_r_fermeture_id = EXCLUDED.t_r_fermeture_id,

      poste = EXCLUDED.poste,

      t_r_enveloppe_pose_id = EXCLUDED.t_r_enveloppe_pose_id,

      nbre_depart = EXCLUDED.nbre_depart,

      t_r_cellule_armoire_id = EXCLUDED.t_r_cellule_armoire_id,

      date_pose = EXCLUDED.date_pose,

      date_depose = EXCLUDED.date_depose,

      date_intervention = EXCLUDED.date_intervention,

      remarque = EXCLUDED.remarque,

      mslink = EXCLUDED.mslink,

      mapid = EXCLUDED.mapid,

      ecp_intervention_id = EXCLUDED.ecp_intervention_id,

      photo = EXCLUDED.photo,

      schema_elec = EXCLUDED.schema_elec,

      rot_z = EXCLUDED.rot_z,

            t_r_actor_id = EXCLUDED.t_r_actor_id,

      geom = EXCLUDED.geom

     returning id)

    select into v_id id from ins;



    if not v_id is null then

      IF NOT NEW.date_action IS NULL AND not NEW.desc_action IS NULL THEN

        INSERT INTO ecp_actions

        (tablename,id,date_action,desc_action)

         VALUES

        ('ecp_armoires',v_id,NEW.date_action, NEW.desc_action);

      END IF;

      NEW.numero := v_id;

    end if;



    RETURN NEW;



  END IF;



  RETURN NEW;

END;

$function$
