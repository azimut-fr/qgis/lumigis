CREATE OR REPLACE FUNCTION sig.f_trg_update_vue_kit_cables()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE

 v_id INTEGER := NULL;

BEGIN

  IF (TG_OP = 'DELETE') THEN



    DELETE FROM sig_ecp.ecp_cables WHERE id = OLD.numero;



    RETURN OLD;



  ELSIF (TG_OP = 'UPDATE') THEN

    IF NEW.insee IS NULL THEN
        NEW.insee = OLD.insee;
    END IF;
    UPDATE sig_ecp.ecp_cables SET

  t_r_insee_commune_id = NEW.insee,

  t_r_type_pose_id = f_refidbyall('t_r_type_poses', NEW.type_pose),

  t_r_type_cable_id = f_refidbyall('t_r_type_cables', NEW.type_cable),

  "section" = NEW.sect_cable,

  charge = NEW.charge,

  date_pose = NEW.date_pose,

  date_abandon = NEW.date_abandon,

  date_intervention = NEW.date_action,

  remarque = NEW.remarque,

  classe_precision = NEW.classe_precision,

  mapid = NEW.mapid,

  mslink = NEW.mslink,

  ecp_intervention_id = NEW.ecp_intervention_id,

  t_r_actor_id = f_refidbyall('t_r_actors', NEW.acteur),

  t_r_etat_cable_id = f_refidbyall('t_r_etat_cables', NEW.etat),

  geom = NEW.geom

  WHERE id = NEW.numero;



   IF NOT NEW.date_action IS NULL AND (NEW.date_action IS DISTINCT FROM OLD.date_action OR NEW.desc_action IS DISTINCT FROM OLD.desc_action) THEN

      INSERT INTO ecp_actions

      (tablename,id,date_action,desc_action)

       VALUES

      ('ecp_cables',NEW.numero,NEW.date_action, NEW.desc_action);

   END IF;



    RETURN NEW;



  ELSIF (TG_OP = 'INSERT') THEN



    with ins as (

 INSERT INTO sig_ecp.ecp_cables (

t_r_insee_commune_id,

  date_pose,

  t_r_type_pose_id,

  t_r_type_cable_id,

  "section",

  charge,

  date_pose,

  date_abandon,

  date_intervention,

  remarque,

  classe_precision,

  mapid,

  mslink,

  ecp_intervention_id,

  t_r_actor_id,

    t_r_etat_cable_id,

  geom

 ) values (

  NEW.insee,

  f_refidbyall('t_r_type_poses', NEW.type_pose),

  f_refidbyall('t_r_type_cables', NEW.type_cable),

  NEW.sect_cable,

  NEW.charge,

  NEW.date_pose,

  NEW.date_abandon,

  NEW.date_action,

  NEW.remarque,

  NEW.classe_precision,

  NEW.mapid,

  NEW.mslink,

  NEW.ecp_intervention_id,

  f_refidbyall('t_r_actors', NEW.acteur),

  f_refidbyall('t_r_etat_cables', NEW.etat),

  NEW.geom

 ) ON CONFLICT(t_r_insee_commune_id, "section", geom) DO UPDATE SET


  t_r_type_pose_id = f_refidbyall('t_r_type_poses', NEW.type_pose),

  t_r_type_cable_id = f_refidbyall('t_r_type_cables', NEW.type_cable),

  charge = NEW.charge,

  date_pose = NEW.date_pose,

  date_abandon = NEW.date_abandon,

  date_intervention = NEW.date_action,

  remarque = NEW.remarque,

  classe_precision = NEW.classe_precision,

  mapid = NEW.mapid,

  mslink = NEW.mslink,

  ecp_intervention_id = NEW.ecp_intervention_id,

  t_r_actor_id = f_refidbyall('t_r_actors', NEW.acteur),

  t_r_etat_cable_id = f_refidbyall('t_r_etat_cables', NEW.etat),

  geom = NEW.geom

  returning id)

    select into v_id id from ins;



    if not v_id is null then

      IF NOT NEW.date_action IS NULL AND not NEW.desc_action IS NULL THEN

        INSERT INTO ecp_actions

        (tablename,id,date_action,desc_action)

         VALUES

        ('ecp_cables',v_id,NEW.date_action, NEW.desc_action);

      END IF;

      NEW.numero := v_id;

    end if;



 RETURN NEW;



  END IF;



  RETURN NEW;

END;

$function$
