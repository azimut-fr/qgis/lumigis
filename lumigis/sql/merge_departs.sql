WITH rg AS (
SELECT
f_refidbyall('t_r_insee_communes',insee::text) AS t_r_insee_commune_id,
f_idbyinseenom('ecp_armoires',insee,id_armoire) AS ecp_armoire_id,
id_depart AS idnom,
f_refidbyall('t_r_designations',designation) AS t_r_designation_id,
f_refidbyall('t_r_nature_departs',nature_depart) AS t_r_nature_depart_id,
f_refidbyall('t_r_nature_protections',nature_protection) AS t_r_nature_protection_id,
calibrage_protection,
depart_sens_diff,
f_refidbyall('t_r_allumages',regime_allumage) AS t_r_allumage_id,
heure_allumage::time without time zone,
heure_extinction::time without time zone,
remarque,
ecp_intervention_id,
f_refidbyall('t_r_actors',acteur) AS t_r_actor_id,
iphase1,
iphase2,
iphase3,
uid4::uuid,
f_refidbyall('t_r_etats',etat) AS t_r_etat_id
 FROM recolement_gpkg.departs WHERE ecp_intervention_id = {ecp_intervention_id}
 )
 MERGE INTO ecp_departs ecp
USING rg ON rg.uid4 = ecp.uid4
WHEN MATCHED THEN
UPDATE SET
(t_r_insee_commune_id,ecp_armoire_id,idnom,t_r_designation_id,t_r_nature_depart_id,t_r_nature_protection_id,calibrage_protection,sensibilite_diff,t_r_allumage_id,heure_allumage,
heure_extinction,remarque,ecp_intervention_id,t_r_actor_id,iphase1,iphase2,iphase3,uid4,t_r_etat_id)
 =
(rg.t_r_insee_commune_id,rg.ecp_armoire_id,rg.idnom,rg.t_r_designation_id,rg.t_r_nature_depart_id,rg.t_r_nature_protection_id,rg.calibrage_protection,rg.depart_sens_diff,rg.t_r_allumage_id,rg.heure_allumage,rg.
heure_extinction,rg.remarque,rg.ecp_intervention_id,rg.t_r_actor_id,rg.iphase1,rg.iphase2,rg.iphase3,rg.uid4,rg.t_r_etat_id)
 WHEN NOT MATCHED THEN
 INSERT
 (t_r_insee_commune_id,ecp_armoire_id,idnom,t_r_designation_id,t_r_nature_depart_id,t_r_nature_protection_id,calibrage_protection,sensibilite_diff,t_r_allumage_id,heure_allumage,
heure_extinction,remarque,ecp_intervention_id,t_r_actor_id,iphase1,iphase2,iphase3,uid4,t_r_etat_id)
 VALUES
 (rg.t_r_insee_commune_id,rg.ecp_armoire_id,rg.idnom,rg.t_r_designation_id,rg.t_r_nature_depart_id,rg.t_r_nature_protection_id,rg.calibrage_protection,rg.depart_sens_diff,rg.t_r_allumage_id,rg.heure_allumage,rg.
heure_extinction,rg.remarque,rg.ecp_intervention_id,rg.t_r_actor_id,rg.iphase1,rg.iphase2,rg.iphase3,rg.uid4,rg.t_r_etat_id)
