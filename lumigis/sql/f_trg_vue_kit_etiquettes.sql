CREATE OR REPLACE FUNCTION sig.f_trg_update_vue_kit_etiquettes()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE

	v_id integer := NULL;

BEGIN

  IF (TG_OP = 'DELETE') THEN

    DELETE FROM sig_ecp.ecp_etiquettes WHERE id = OLD.id;

    RETURN OLD;

  ELSIF (TG_OP = 'UPDATE') THEN

    IF NEW.insee IS NULL THEN
        NEW.insee = OLD.insee;
    END IF;

    UPDATE sig_ecp.ecp_etiquettes SET

      t_r_insee_commune_id = NEW.insee,

      libelle = NEW.libelle,

      niveau = NEW.niveau,

      intervention = NEW.intervention,

      t_r_etat_id = f_refidbyall('t_r_etats', NEW.etat),

      ecp_intervention_id = (SELECT id FROM ecp_interventions WHERE idnom = NEW.intervention),

      t_r_actor_id = f_refidbyall('t_r_actors', NEW.acteur),

      geom = NEW.geom

	  WHERE id = NEW.id;


    RETURN NEW;



  ELSIF (TG_OP = 'INSERT') THEN



    with ins as (

    INSERT INTO sig_ecp.ecp_etiquettes (

      t_r_insee_commune_id,

      libelle,

      t_r_etat_id,

      niveau,

      ecp_intervention_id,

      intervention,

	    t_r_actor_id,

      geom

    ) values (

      NEW.insee,

      NEW.libelle,

      f_refidbyall('t_r_etats', NEW.etat),

      NEW.niveau,

      (SELECT id FROM ecp_interventions WHERE idnom = NEW.intervention),

      NEW.intervention,

      f_refidbyall('t_r_actors', NEW.acteur),

      NEW.geom

    )
    returning id)

    select into v_id id from ins;


    RETURN NEW;



  END IF;



  RETURN NEW;

END;

$function$
