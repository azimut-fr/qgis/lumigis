-- merge recolement_gpkg.armoires in ecp_armoires
WITH rg AS (
 SELECT
 insee AS t_r_insee_commune_id,
id_armoire AS idnom,
enveloppe_poste_amont AS poste,
f_refidbyall('t_r_etats',etat) AS t_r_etat_id,
f_refidbyall('t_r_vetustes',etat) AS t_r_vetuste_id,
f_refidbyall('t_r_fermetures',etat) AS t_r_fermeture_id,
f_refidbyall('t_r_cellule_armoires',cellule) AS t_r_cellule_armoire_id,
ecp_intervention_id,
date_pose,
date_depose,
remarque,
f_refidbyall('t_r_actors',acteur) AS t_r_actor_id,
rot_z,
uid4,
geom FROM {fdw_name}.armoires WHERE ecp_intervention_id = {ecp_intervention_id}
)
 MERGE INTO ecp_armoires ecp
 USING rg ON rg.uid4 = ecp.uid4::text
 WHEN MATCHED THEN
 UPDATE SET
 (t_r_insee_commune_id,idnom,poste,t_r_etat_id,t_r_vetuste_id,t_r_fermeture_id,t_r_cellule_armoire_id,ecp_intervention_id,date_pose,
date_depose,remarque,t_r_actor_id,rot_z,uid4,geom) =
 (rg.t_r_insee_commune_id,rg.idnom,rg.poste,rg.t_r_etat_id,rg.t_r_vetuste_id,rg.t_r_fermeture_id,rg.t_r_cellule_armoire_id,rg.ecp_intervention_id,rg.date_pose,
rg.date_depose,rg.remarque,rg.t_r_actor_id,rg.rot_z,rg.uid4::uuid,rg.geom)
 WHEN NOT MATCHED THEN
 INSERT
 (t_r_insee_commune_id,idnom,poste,t_r_etat_id,t_r_vetuste_id,t_r_fermeture_id,t_r_cellule_armoire_id,ecp_intervention_id,date_pose,
date_depose,remarque,t_r_actor_id,rot_z,uid4,geom) VALUES
(rg.t_r_insee_commune_id,rg.idnom,rg.poste,rg.t_r_etat_id,rg.t_r_vetuste_id,rg.t_r_fermeture_id,rg.t_r_cellule_armoire_id,rg.ecp_intervention_id,rg.date_pose,
rg.date_depose,rg.remarque,rg.t_r_actor_id,rg.rot_z,rg.uid4::uuid,rg.geom)
