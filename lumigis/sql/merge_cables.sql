WITH rg AS (
SELECT
insee AS t_r_insee_commune_id,
sect_cable AS section,
charge,
classe_precision,
ecp_intervention_id,
date_pose,
date_abandon,
remarque,
f_refidbyall('t_r_actors',acteur) AS t_r_actor_id,
f_refidbyall('t_r_type_cables',type_cable) AS t_r_type_cable_id,
f_refidbyall('t_r_type_poses',type_pose) AS t_r_type_pose_id,
f_refidbyall('t_r_etat_cables',etat) AS t_r_etat_cable_id,
uid4::uuid,
geom
 FROM {fdw_name}.cables WHERE ecp_intervention_id = {ecp_intervention_id}
 )
 MERGE INTO ecp_cables ecp
 USING rg ON rg.uid4 = ecp.uid4
 WHEN MATCHED THEN
 UPDATE SET
 (t_r_insee_commune_id,section,charge,classe_precision,ecp_intervention_id,date_pose,date_abandon,remarque,t_r_actor_id,t_r_type_cable_id,t_r_type_pose_id,t_r_etat_cable_id,uid4,geom)
 =
 (rg.t_r_insee_commune_id,rg.section,rg.charge,rg.classe_precision,rg.ecp_intervention_id,rg.date_pose,rg.date_abandon,rg.remarque,rg.t_r_actor_id,rg.t_r_type_cable_id,rg.t_r_type_pose_id,rg.t_r_etat_cable_id,rg.uid4,rg.geom)
 WHEN NOT MATCHED THEN
 INSERT
 (t_r_insee_commune_id,section,charge,classe_precision,ecp_intervention_id,date_pose,date_abandon,remarque,t_r_actor_id,t_r_type_cable_id,t_r_type_pose_id,t_r_etat_cable_id,uid4,geom)
 VALUES
 (rg.t_r_insee_commune_id,rg.section,rg.charge,rg.classe_precision,rg.ecp_intervention_id,rg.date_pose,rg.date_abandon,rg.remarque,rg.t_r_actor_id,rg.t_r_type_cable_id,rg.t_r_type_pose_id,rg.t_r_etat_cable_id,rg.uid4,rg.geom)
