import os

from qgis.core import Qgis
from qgis.gui import QgsMapToolEmitPoint
from qgis.PyQt.QtGui import QCursor, QPixmap
from qgis.PyQt.QtWidgets import QApplication

from lumigis.__about__ import DIR_PLUGIN_ROOT

# from .snapping_map_tool_emit_point import SnappingMapToolEmitPoint
from .lumigis_layers import LumigisLayersConst


class PlacePoint:
    """place point"""

    def __init__(self, iface, dbutil, lumigislayers):
        self.iface = iface
        self.db_util = dbutil
        self._lumigis_layers = lumigislayers

    def place_point_outil(
        self,
        object_type,
        table_name="",
        coord_dimension=3,
        object_id=0,
        set_state=False,
        mode="numerotation",
    ):
        if object_type in [
            LumigisLayersConst.ARMOIRE,
            LumigisLayersConst.SUPPORT,
            LumigisLayersConst.FOYER,
        ]:
            self.currentTableName = table_name
            self.currentCoordDimension = coord_dimension
            self.currentObjectType = object_type
            self.currentId = object_id
            self.set_state = set_state
            self.mode = mode
            # Create the map tool using the canvas reference
            self.pointTool = QgsMapToolEmitPoint(self.iface.mapCanvas())
            # connect signal that the canvas was clicked
            self.pointTool.canvasClicked.connect(self.set_geom_from_point)
            self.iface.mapCanvas().setMapTool(self.pointTool)
            self.set_override_cursor_from_image(
                self._lumigis_layers.lux_layers[object_type].symbol_name
            )

    def set_geom_from_point(self, point, button):
        while QApplication.overrideCursor() is not None:
            QApplication.restoreOverrideCursor()
        pt_cc48 = (
            self.iface.mapCanvas()
            .mapSettings()
            .mapToLayerCoordinates(self.iface.activeLayer(), point)
        )
        if self.currentCoordDimension == 3:
            wkt = "'POINT({} {} {})'".format(pt_cc48.x(), pt_cc48.y(), 0.0)
        else:
            wkt = "'POINT({} {})'".format(pt_cc48.x(), pt_cc48.y())
        # print(wkt,.table_name=',self.currentTableName,'id=', self.currentId)
        if self.currentTableName != "" and self.currentId != 0:
            postgis_srid = (
                self.iface.mapCanvas().mapSettings().destinationCrs().authid()
            )
            postgis_srid = postgis_srid[5:]
            if self.mode == "report":
                self.db_util.db.transaction()
            if self.db_util.update_geom(
                self.currentTableName, self.currentId, "geom", wkt, postgis_srid
            ):
                if self.set_state:
                    self.db_util.update_field(
                        self.currentTableName, self.currentId, "etat", "Actif"
                    )
                self.db_util.db.commit()
                self._lumigis_layers.refresh_layer_by_name(
                    self._lumigis_layers.lux_layers[self.currentObjectType].name
                )
            else:
                self.iface.messageBar().pushMessage(
                    "Positionnement",
                    "Impossible de mettre à jour la géométrie pour l'objet #{} "
                    "de la table {}".format(self.currentId, self.currentTableName),
                    Qgis.Critical,
                )
                self.db_util.db.rollback()

    def set_override_cursor_from_image(self, symbol_name):
        icon_path = os.path.join(DIR_PLUGIN_ROOT, "resources/images", symbol_name)
        pixmap = QPixmap(icon_path)
        cursor = QCursor(pixmap.scaled(32, 32))
        QApplication.setOverrideCursor(cursor)
