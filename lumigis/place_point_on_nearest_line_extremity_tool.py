"""
/***************************************************************************
 place_point_on_nearest_line_extremity.py

 PLace point tool class allows to place a symbol on nearest line extremity


        begin                : 2023-04-02
        git sha              : $Format:%H$
        copyright            : (C) 2023 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

import math
import os

from qgis.core import QgsFeature, QgsPointXY, QgsWkbTypes
from qgis.gui import QgsMapToolIdentify, QgsRubberBand
from qgis.PyQt.QtCore import Qt, QUuid
from qgis.PyQt.QtGui import QCursor, QPixmap
from qgis.PyQt.QtWidgets import QApplication

from lumigis.__about__ import DIR_PLUGIN_ROOT

# from .snapping_map_tool_emit_point import SnappingMapToolEmitPoint


class PlacePointOnNearestLineExtremity(QgsMapToolIdentify):
    """
    La création d'un symbole aligné sur l'extrémité d'une ligne, se fait en une étape:
    1- identifier la ligne au plus près de l'extrémité
    """

    def __init__(
        self,
        iface,
        dbutil,
        lumigislayers,
        object_type,
        table_name="",
        coord_dimension=3,
        distance=0,
        symbol=None,
        line_layers=[],
    ):
        self.iface = iface
        self.db_util = dbutil
        self._lumigis_layers = lumigislayers
        self._layers = line_layers  # lumigislayers.layers("LUX")
        self.layer = iface.mapCanvas().currentLayer()
        self.currentTableName = table_name
        self.currentCoordDimension = coord_dimension
        self.currentObjectType = object_type
        self.distance = distance
        self.symbol_name = symbol
        self.attr_values = dict()
        QgsMapToolIdentify.__init__(self, self.iface.mapCanvas())
        self.rubberBand = QgsRubberBand(
            self.iface.mapCanvas(), QgsWkbTypes.LineGeometry
        )
        self.rubberBand.setStrokeColor(Qt.red)
        self.rubberBand.setWidth(4)
        self.startPoint = QgsPointXY(0, 0)
        self._z_rotation = 0.0
        self.line_geometry = None
        if self.symbol_name:
            self.set_override_cursor_from_image(self.symbol_name)
        self.reset()

    def set_attr_value(self, key, value):
        self.attr_values[key] = value

    def set_attr_values_dict(self, values_dict):
        self.attr_values = values_dict

    def reset(self):
        self.startPoint = QgsPointXY(0, 0)
        self.isEmittingPoint = True
        self._z_rotation = 0
        self.step = 1

    # def canvasPressEvent(self, event):
    #    pass

    def canvasReleaseEvent(self, event):
        if self.step == 1:
            if len(self._layers):
                results = self.identify(
                    event.x(), event.y(), self._layers, QgsMapToolIdentify.TopDownAll
                )
            else:
                results = self.identify(
                    event.x(), event.y(), [], QgsMapToolIdentify.TopDownAll
                )
            if results:
                for result in results:
                    layer = result.mLayer
                    if layer.name() == "Cables":
                        """
                        print("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv")
                        print(result.mLayer)
                        print(result.mDerivedAttributes)
                        print(
                            result.mFeature.id(),
                            f"/{result.mFeature.attribute('insee')}/",
                            f"{result.mFeature.attribute('type_pose')}/",
                            f"{result.mFeature.attribute('type_cable')}",
                            f"{result.mFeature.attribute('sect_cable')}",
                        )
                        """
                        distance = self.distance
                        geom = result.mFeature.geometry()
                        rot_z = math.degrees(geom.interpolateAngle(distance))
                        start_x = float(
                            result.mDerivedAttributes["X en premier"].replace(",", ".")
                        )
                        start_y = float(
                            result.mDerivedAttributes["Y en premier"].replace(",", ".")
                        )
                        end_x = float(
                            result.mDerivedAttributes["X en dernier"].replace(",", ".")
                        )
                        end_y = float(
                            result.mDerivedAttributes["Y en dernier"].replace(",", ".")
                        )
                        closest_vertex_x = float(
                            result.mDerivedAttributes[
                                "X du sommet le plus proche"
                            ].replace(",", ".")
                        )
                        closest_vertex_y = float(
                            result.mDerivedAttributes[
                                "Y du sommet le plus proche"
                            ].replace(",", ".")
                        )
                        if closest_vertex_x == end_x and closest_vertex_y == end_y:
                            distance = geom.length() - distance
                            rot_z += 180
                            if rot_z > 360:
                                rot_z -= 360
                        point = geom.interpolate(distance)
                        self._z_rotation = rot_z
                        # if offset != 0:
                        #    dy = math.cos(math.radians(alpha)) * offset
                        #    dx = math.sin(math.radians(alpha)) * offset #12
                        self.create_feature(point, True)
                        self.rubberBand.reset()
                        self.reset()
                        self.iface.mapCanvas().unsetMapTool(self)
                        break

    # def canvasMoveEvent(self, event):
    #    pass

    def create_feature(self, point, refresh=False):
        ech_x = ech_y = ech_z = 1
        fea = QgsFeature(self.layer.fields())
        fea.setGeometry(point)
        # fea.setAttribute(0, 0)  # numero
        for key in self.attr_values.keys():
            fea.setAttribute(key, self.attr_values[key])

        fea.setAttribute("epaisseur", 0)  # epaisseur
        fea.setAttribute("couleur", 0)  # couleur
        fea.setAttribute("rot_z", self._z_rotation)
        fea.setAttribute("ech_x", ech_x)
        fea.setAttribute("ech_y", ech_y)
        fea.setAttribute("ech_z", ech_z)
        fea.setAttribute("etat", "Actif")  # etat
        fea.setAttribute("uid4", QUuid.createUuid().toString())
        provider = self.layer.dataProvider()
        provider.addFeatures([fea])
        if refresh:
            self._lumigis_layers.refresh_layer_by_name(self.layer.name())

    def set_override_cursor_from_image(self, symbol_name):
        icon_path = os.path.join(DIR_PLUGIN_ROOT, "resources/images", symbol_name)
        pixmap = QPixmap(icon_path)
        cursor = QCursor(pixmap.scaled(32, 32))
        QApplication.setOverrideCursor(cursor)

    def showLine(self, start_point, end_point):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)

        pt = QgsPointXY(start_point)
        self.rubberBand.addPoint(pt, False)
        # end point computing to draw a 3 m line
        dx = end_point.x() - start_point.x()
        dy = end_point.y() - start_point.y()
        d2 = dx * dx + dy * dy
        k = math.sqrt(9.0 / d2)
        dx *= k
        dy *= k
        x = start_point.x() + dx
        y = start_point.y() + dy
        pt = QgsPointXY(x, y)
        self.rubberBand.addPoint(pt, True)

        self.rubberBand.show()


"""
    def deactivate(self):
        self.rubberBand.reset(QgsWkbTypes.LineGeometry)
        QgsMapTool.deactivate(self)
        self.deactivated.emit()
"""
