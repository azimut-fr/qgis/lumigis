"""
/***************************************************************************
 LumigisLayer
                                 A QGIS plugin
 Couche éclairage public
        begin                : 2019-12-04
        copyright            : (C) 2019 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsVectorLayer
from qgis.PyQt.QtCore import QRegularExpression
from qgis.PyQt.QtSql import *


class LumigisLayer:
    """Classe decrivant une couche d'éclairage public"""

    def __init__(self, layer_name, layer_id: int = -1):
        """Constructor.
        :param layer_name: An Ecp layer name.
        """
        # Save reference to the QGIS interface
        self._layer_name = layer_name
        self._layer_id = layer_id
        self._layer_prefix = "undefined"
        self._table_name = "undefined"
        self._view_name = "undefined"
        self._col_id = "id"
        self._col_insee = "t_r_insee_commune_id"
        self._col_idnom = "idnom"
        self._view_col_id = "numero"
        self._view_col_insee = "insee"
        self._view_col_idnom = "undefined"
        self._regex = None
        self._child_name = "undefined"
        self._symbol_name = "undefined"
        self._coord_dimension = 3

    @property
    def id(self):
        return self._layer_id

    @id.setter
    def id(self, value):
        self._layer_id = value

    @property
    def name(self):
        return self._layer_name

    @property
    def prefix(self):
        return self._layer_prefix

    @prefix.setter
    def prefix(self, layer_prefix):
        self._layer_prefix = layer_prefix

    @property
    def table_name(self):
        return self._table_name

    @table_name.setter
    def table_name(self, tablename):
        self._table_name = tablename

    @property
    def view_name(self):
        return self._view_name

    @view_name.setter
    def view_name(self, viewname):
        self._view_name = viewname

    @property
    def col_id(self):
        return self._col_id

    @col_id.setter
    def col_id(self, value):
        self._col_id = value

    @property
    def col_idnom(self):
        return self._col_idnom

    @col_idnom.setter
    def col_idnom(self, value):
        self._col_idnom = value

    @property
    def col_insee(self):
        return self._col_insee

    @col_insee.setter
    def col_insee(self, value):
        self._col_insee = value

    @property
    def view_col_id(self):
        return self._view_col_id

    @view_col_id.setter
    def view_col_id(self, value):
        self._view_col_id = value

    @property
    def view_col_idnom(self):
        return self._view_col_idnom

    @view_col_idnom.setter
    def view_col_idnom(self, value):
        self._view_col_idnom = value

    @property
    def view_col_insee(self):
        return self._view_col_insee

    @view_col_insee.setter
    def view_col_insee(self, value):
        self._view_col_insee = value

    @property
    def regular_expression(self):
        return self._regex

    @regular_expression.setter
    def regular_expression(self, regex_string):
        self._regex = QRegularExpression(regex_string)

    @property
    def child_name(self):
        return self._child_name

    @child_name.setter
    def child_name(self, childname):
        self._child_name = childname

    @property
    def symbol_name(self):
        return self._symbol_name

    @symbol_name.setter
    def symbol_name(self, symbolname):
        self._symbol_name = symbolname

    def idnom(self, id):
        if type(id) == int:
            return self._layer_prefix + str(id)
        else:
            return self._layer_prefix + id

    @property
    def dimension(self):
        return self._coord_dimension

    @dimension.setter
    def dimension(self, coord_dimension):
        self._coord_dimension = coord_dimension

    @classmethod
    def filter_layer_with_insee(
        cls, layer: QgsVectorLayer, insee_name, insee, refresh, iface
    ):
        if layer is not None:
            layer.removeSelection()
            if insee:
                layer.setSubsetString(f"{insee_name}=" + str(insee))
            else:
                if layer.subsetString()[0 : len(insee_name) + 1] == f"{insee_name}=":
                    layer.setSubsetString("")
            if refresh:
                # If caching is enabled, a simple canvas refresh might not be
                # sufficient to trigger a redraw and you must clear the cached
                # image for the layer
                if iface.mapCanvas().isCachingEnabled():
                    layer.triggerRepaint()
