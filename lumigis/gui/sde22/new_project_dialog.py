"""
/***************************************************************************
 NewProjectDialog
                                 A QGIS plugin
 Lumigis
                             -------------------
        begin                : 2023-06-27
        git sha              : $Format:%H$
        copyright            : (C) 2023 by Jean-Marie ARSAC
        email                : jmarsac@arsac.wf
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import re

from qgis.core import QgsExpressionContextUtils, QgsProject
from qgis.PyQt import QtWidgets, uic

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "new_project_dialog.ui")
)


class NewProjectDialog(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(NewProjectDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self._db_util = None
        self._folder = None
        self._project_name = None
        self._fullpath = None
        self._technician = QgsExpressionContextUtils.projectScope(
            QgsProject.instance()
        ).variable("user_full_name")
        self._address = None
        self._city = None
        if not self._technician:
            self._technician = os.getlogin()

        self.toolButton.pressed.connect(self.showDialog)
        self.comboBox_city.currentIndexChanged.connect(
            self.on_combobox_city_currentindex_changed
        )
        self.lineEdit_address.textChanged.connect(self.on_lineedit_address_text_changed)
        self.label_technician.setText(self._technician)

    @property
    def folder(self):
        return self._folder

    @property
    def project_name(self):
        return self._project_name

    @property
    def fullpath(self):
        return self._fullpath

    @property
    def address(self):
        return self._address

    @property
    def technician(self):
        return self._technician

    @property
    def city(self):
        return self._city

    def set_db_util(self, db_util):
        self._db_util = db_util

    def unaccent(self, s):
        normalMap = {
            "À": "A",
            "Á": "A",
            "Â": "A",
            "Ã": "A",
            "Ä": "A",
            "à": "a",
            "á": "a",
            "â": "a",
            "ã": "a",
            "ä": "a",
            "ª": "A",
            "È": "E",
            "É": "E",
            "Ê": "E",
            "Ë": "E",
            "è": "e",
            "é": "e",
            "ê": "e",
            "ë": "e",
            "Í": "I",
            "Ì": "I",
            "Î": "I",
            "Ï": "I",
            "í": "i",
            "ì": "i",
            "î": "i",
            "ï": "i",
            "Ò": "O",
            "Ó": "O",
            "Ô": "O",
            "Õ": "O",
            "Ö": "O",
            "ò": "o",
            "ó": "o",
            "ô": "o",
            "õ": "o",
            "ö": "o",
            "º": "O",
            "Ù": "U",
            "Ú": "U",
            "Û": "U",
            "Ü": "U",
            "ù": "u",
            "ú": "u",
            "û": "u",
            "ü": "u",
            "Ñ": "N",
            "ñ": "n",
            "Ç": "C",
            "ç": "c",
            "§": "S",
            "³": "3",
            "²": "2",
            "¹": "1",
        }
        norma = str.maketrans(normalMap)
        unaccented = s.translate(norma)
        return unaccented

    def clean_path_text(self, path_text):
        s = re.sub(r"[^\w.-]+", "-", path_text)
        clean_text = s.replace("_", "-")
        return clean_text

    def showDialog(self):
        if self.lineEdit.text():
            dirname = QtWidgets.QFileDialog.getExistingDirectory(
                self, "", self.lineEdit.text()
            )
        else:
            dirname = QtWidgets.QFileDialog.getExistingDirectory(
                self, "", QgsProject.instance().homePath()
            )

        if dirname:
            self.lineEdit.setText(dirname)
            self._folder = dirname
            if self._project_name:
                self._fullpath = os.path.join(dirname, self._project_name)

    def on_combobox_city_currentindex_changed(self, current_index):
        try:
            if current_index >= 0:
                self._city = self.comboBox_city.currentText()
                query = self._db_util.query
                sql_string = f"SELECT anciencode FROM v_tous_adherents WHERE libelle = '{self._city}'"
                if query.exec(sql_string):
                    if query.next():
                        anciencode = query.value(0)
                        if anciencode:
                            directory = os.path.normpath(f"p:/{anciencode}/projet/tech")
                            print(directory)
                            if not os.path.exists(directory):
                                os.makedirs(directory)
                            self.lineEdit.setText(directory)
                            self._folder = directory
                            if self._project_name:
                                self._fullpath = os.path.join(
                                    directory, self._project_name
                                )
        except:
            pass

    def on_lineedit_address_text_changed(self):
        if self.lineEdit_address.text().strip():
            self._address = self.lineEdit_address.text()
            unaccented = self.unaccent(self._address)
            self._project_name = self.clean_path_text(unaccented).lower()
            self.label_project_name.setText(self._project_name)
            print(self._project_name)
            if self._folder:
                self._fullpath = os.path.join(self._folder, self._project_name)
                print(self._fullpath)
