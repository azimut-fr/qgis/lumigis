"""
/***************************************************************************
 LumigisAbout
                                 A QGIS plugin
 Lumigis
                             -------------------
        begin                : 2021-03-07
        git sha              : $Format:%H$
        copyright            : (C) 2020 by Jean-Marie Arsac
        email                : jmarsac@azimut.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import sys

from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QPixmap
from qgis.PyQt.QtWidgets import QDialog

sys.path.append(os.path.dirname(__file__))

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "lumigis_about.ui"), resource_suffix=""
)


class LumigisAbout(QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(LumigisAbout, self).__init__(parent)
        self.setupUi(self)

        self.rejected.connect(self.onReject)
        self.buttonBox.rejected.connect(self.onReject)
        self.buttonBox.accepted.connect(self.onAccept)

        self.init_logo(
            self.logo_azimut, ":/plugins/lumigis/resources/images/logo_azimut.png"
        )
        self.init_logo(
            self.logo_sdev, ":/plugins/lumigis/resources/images/logo_sdev.png"
        )

    def init_logo(self, label, logo):
        img = QPixmap(logo)
        img = img.scaled(150, 150, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        label.setPixmap(img)

    def onAccept(self):
        self.accept()

    def onReject(self):
        self.close()
