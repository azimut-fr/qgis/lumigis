import os

from qgis.core import Qgis, QgsExpressionContextUtils, QgsProject, QgsProviderRegistry
from qgis.PyQt import QtCore
from qgis.PyQt.QtWidgets import QDialog, QDialogButtonBox, QListWidget, QVBoxLayout
from qgis.utils import iface


def openProject(verbose: bool = False):
    try:
        message = QListWidget()
        to_show = False
        gpkg = os.path.basename(QgsProject.instance().fileName()).replace(
            ".qgs", ".gpkg"
        )
        new_gpkg = f"{QgsProject.instance().homePath()}/data/{gpkg}"
        md = QgsProviderRegistry.instance().providerMetadata("ogr")
        conn = md.createConnection(new_gpkg, {})
        result = conn.executeSql("SELECT * FROM metadata ORDER BY fid")
        for row in result:
            text_content = f"{row[1]} : {row[2]}"
            QgsExpressionContextUtils.setProjectVariable(
                QgsProject.instance(), f"lumigis_{row[1].lower()}", row[2]
            )
            message.addItem(text_content)
            to_show = True

        md.disconnect()

        if to_show:
            message.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            message.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            message.setFixedSize(
                message.sizeHintForColumn(0) + 2 * message.frameWidth(),
                message.sizeHintForRow(0) * message.count() + 2 * message.frameWidth(),
            )
            dlg = QDialog(iface.mainWindow())
            dlg.setModal(True)
            dlg.setWindowTitle("Lumigis - REPORT")
            QBtn = QDialogButtonBox.Ok
            dlg.buttonBox = QDialogButtonBox(QBtn)
            dlg.buttonBox.accepted.connect(dlg.accept)
            layout = QVBoxLayout()
            layout.addWidget(message)
            layout.addWidget(dlg.buttonBox)
            dlg.setLayout(layout)
            dlg.show()
    except Exception as e:
        if verbose:
            msgbar = iface.messageBar()
            msgbar.pushMessage("openproject: ", str(e), Qgis.Warning, 5)


def saveProject():
    pass


def closeProject():
    pass
